## About 881:

I reuploaded this abandoned project since my contributions were not going to get merged. I am not planning on hosting but planning to add Immersion to the game. Whether that means adding item spawn, npc spawns, dialogue,
bosses, slayer, or even clue scrolls. Whatever gets me to my goal is the focus for myself.

## Running

Location for cache: https://archive.openrs2.org/caches/runescape/984

Download the flat file 10gb.

Place the 881 cache inside `server/data/cache/`.

Use Java 8

Can set this in your Project Structure.

To start the server while developing, run `./gradlew server:run --args='1 false false false false'`
To start the client, run `./gradlew client:run`

## Credits

- Founded by Castiel
- Open Sourced by Summer
- Contributors: Castiel, Summer, Dylan, Pazaz


## Current Updates:

- Fixed General Stores in lumbridge to open up as a general store not the Varrock Sword shop.
- Added Lachtopher dialogue & Changed the examine to match RS3.
- Added Victoria dialogue & Changed the examine to match Rs3.
- Added Ring of Respawn with unlimited uses as QoL. I didn't see a point of charges when lodestone network exists.
- Added Fred the Farmer's Dialogue and Changed the examine to match RS3. I did leave out the quest lines as the quest isn't implemented.
- Added Lumbridge Cook's Post Quest Dialogue as it brings immersion not quest line.
- Added Duke Horacio's Dialogue with post quest dialogue.  He also gives the player unlimited anti-dragon shields when asked for a shield.
- Added Lumbridge Sage dialogue and added correct examine.
- Added Xenia's Dialogue after a tale of two cats dialogue since it was the least questy and changed her examine to match rs3.
- Added Zamorakian Herbalist ( Male )'s Dialogue, Changed his location to house adjacent to thieves guild as in rs3, and corrected the examine.
- Added Zamorakian Herbalist ( Wife )'s Dialogue, Changed her location to house adjacent to thieves guild as in rs3, and corrected the examine.
- Changed the Zamorakian Herbalist's son's location to house adjacent to thieves guild as in rs3, and corrected the examine.
- Added the Dialogue for Guardsman Peale in Lumbridge Castle and added the correct examine.
- Added the Dialogue for Guardsman Pazel in Lumbridge Castle and added the correct examine.
- Added the Dialogue for Guardsman Brawn in Lumbridge Castle and added the correct examine.
- Added the Dialogue for Guardsman DeShawn in Lumbridge Castle and added the correct examine.
- Added the Dialogue for Guardsman Dante in Lumbridge Castle and added the correct examine.
- Added the use herring on tree Monty python reference.
- Removed the subscribe button. There is no membership in open source, just love!
- Added Al The Camel's Dialogue and examine. It requires you to either wear or have in inventory a camulet.
- Added Donie's Dialogue and examine.
- Added Dialogue for Roddeck in Lumbridge and the correct examine.
- Added knife spawn behind Bob's Hatchet Shop.
- Added Knife on crate in Lumbridge Castle Kitchen.
- Added Empty Pot on table in Lumbridge Castle Kitchen.
- Added Bowl on table in Lumbridge Castle Kitchen.
- Added Jug on table in Lumbridge Castle Kitcehn.
- Added Mind Rune near the staircase on the West side of the Lumbridge Castle.
- Added 4 logs near the bank up on the top floor of the Lumbridge Castle.
- Added bronze dagger on dresser on the second floor of the Lumbridge Castle.
- Added Bronze Arrow near the staircase on the East side of the Lumbridge Castle.
- Added two bronze pickaxes on each side of the staircase in Lumbridge guard tower.
- Added Iron dagger in goblin house next to the crate.
- Added Pot on table in back of bar in Varrock.
- Added Leather top adjacent to the exterior door in abandoned house.
- Added x2 logs on both sides of the interior door leading to the main living area of abandoned hoouse.
- Added Jug on barrel at tea stall.
- Added cup of tea on table at tea stall.
- Added pie dish in Varrock Castle Kitchen.
- Added the "Search" haystacks with either a chance to get hurt ( 10 lp), receive nothing at all, or find a needle in the haystack.
- Pazaz updated Open881 to accept the flat file cache from OpenRS2 found here: https://archive.openrs2.org/caches/runescape/984
- Added Gerrant's Fish Business Shop, Dialogue, and Examine
- Added Dialogue & Examine for "The Face"
- Added Dialogue, Shop, & Examine for "Wydin"
- Added Dialogue, Shop, & Examine for "Grum"
- Added Dialogue, Shop, & Examine for "Betty"
- Added Dialogue, Shop, & Examine for "Brian"
- Moved Red Beard to correct location and Added Dialogue for "Red Beard"
- Added placeholder text for Explorer Jack. Player asks if he has any tasks, he responds with "No, not yet, but keep checking the update log for open881!"
- Correct xp rate for Giant Spiders
- Added more goblin spawns based off of this map: https://runescape.wiki/w/Goblin#mapFullscreen
- Removed all of the NPCs from the unpackedspawnlist and adding them back slowly without the null and without the adding Npcs without animations/dialogue/combat definitions.
- Added more accurate respawn delays for trees based off this: https://www.runehq.com/skill/woodcutting#respawntimes
- Added more accurate spawn delays and experience gained based off a similar historical article for mining.
- Added Dialogue, Spawn, and Examine for King Roald in Varrock.
- Added Dark Wizard spawns in Varrock ruins.
- Added all Varrock Guard spawns based off of this map: https://runescape.wiki/w/Guard_(Varrock)#mapFullscreen
- Added the full Asgarnian Ice Caves. Blurite is now protected!
- Added seagulls around Port Sarim
- Added the goblin spawns near lodestone in Port Sarim.
- Fishing Spots now move around based on a random timer.
- Added Rat Spawns in Lumbridge Swamp - https://runescape.wiki/w/Giant_rat#mapFullscreen
- Added Black Knight Spawn in between the "Swamp" and "Draynor". - https://runescape.wiki/w/Black_Knight#mapFullscreen
- Added Fishing Spots in Draynor.
- Added Jug of wine spawn in Catherby
- Added Insect Repellent spawn in Catherby.
- Added Man spawn in Catherby
- Added Unicorn spawn in Catherby.
- Added Guard spawn in Catherby.
- Added Wolf Spawns on White Wolf Mountain.
- Added the Fire Wizard spawn in Port Sarim.
- Added the Earth Wizard spawn in Port Sarim.
- Added the Water Wizard spawn in Port Sarim.
- Added the Air Wizard spawn in Port Sarim.
- Added Highwayman spawn in Port Sarim.
- Added Highwayman spawn in North East Falador.
- Added Highwayman spawn in Southwest Falador.
- Added Highwayman spawn in Seer's Village.
- Added Highwayman Spawn in Rimmington.
- Added Giant spider spawns in the Lumbridge Swamp.
- Added Green Dragon spawns in the Wilderness on both the East and West side. Reminder that the Duke gives out free anti-fire shields.
- Fixed Wolves not giving slayer experience, the counter not going down, and fixed the wolve's hp, xp given, and max hit.
- Added Blue Dragon spawns in Taverly Dungeon.
- Added Baby Blue Dragon spawns in Taverly Dungeon
- Added Black Demon spawns in Taverly Dungeon.
- Added Hill Giant spawns in Taverly Dungeon.
- Added Chaos Dwarf spawns in Taverly Dungeon.
- Added Poison Scorpion spawn in Taverly Dungeon.
- Added Rusty Key spawn in Taverly Dungeon.
- Added Lava Eel fishing with the use of oily rod.
- Added Skeleton spawns in Taverly Dungeon.
- Added Ghost spawns in Taverly Dungeon.
- Added Chaos Druid spawns in Taverly Dungeon.
- Added Lesser Demon spawns in Taverly Dungeon.
- Added Magic Axe spawns in Taverly Dungeon.
- Added Pompous Merchant spawn in Taverly.
- Made the Pompous Merchant Pickpocketable.
- Added Empty pot spawns in Taverly.
- Added Fishing bait spawns in Taverly.
- Added Cup of tea spawn in Taverly.
- Added Druid/Druidess spawns in Taverly.
- Added Fishing Spots in Lumbridge Swamp.
- Added Swamp tar spawns all over Lumbridge Swamp.
- Added Fish Flinger Npc Spawn ( He still needs dialogue and other ) in both Lumbridge Swamp & Draynor Village.
- Added Log spawns in Draynor Village.
- Added the Potter in Draynor Village.
- Added the Abyssal Knights for immersion ( no minigame )
- Added the Abyssal Banker ( Makes mining in Lumbridge Swamp pretty decent)
- Added Leather glove spawn in Father Urhney's house.
- Adjusted Tree Definitions to fit 2023 RS3 - lv 1 Regular, lv 10 Oak, lv 20 Willow, lv 30 Teak, lv 40 Maple, lv 50 Acadia, lv 60 Mahogany, lv 70 Yew, Lv 80 Magic, lv 90 Elder.
- Added Father Urhney to his house in the Lumbridge Swamps. It is post-quest dialogue, so he will give you a ghost speak amulet if you don't have one or lose yours.
- Added Fishing Spots in Catherby.
- Added Fish Flinger Npc Spawn ( He still needs dialogue and other ) in Catherby.
- Added Harry's Dialogue and Fishing shop in Catherby.
- Added Hickton's Dialogue and Archery shop in Catherby.
- Added Bucket of water spawn in Catherby.
- Added Burnt Fish spawn in Catherby.
- Added Knife spawn in Catherby.
- Added Pie Dish spawn in Catherby.
- Added Bucket spawn in Seer's Village.
- Added Bees! spawn in Seer's Village.
- Added Beekeeper and Dialogue in Seer's Village.
- Added Geoffrey and Dialogue in Seer's Village.
- Added Unicorn Spawns on the outskirts of Seer's Village.
- Added Squirrel Spawn on the outskirts of Seer's Village.
- Added Garlic spawn in Seer's village.
- Added Knife spawn in Seer's Village.
- Added Men spawn in Seer's Village pub.
- Added Women spawn in Seer's Village pub.
- Added the Poison Saleman and his dialogue in Seer's Village.
- Added Knife Spawn in the tower of the Scorpion Catcher quest.
- Added Glassblowing Pipe in Grandpa Jack's House near Fishing Contest Quest.
- Added Giant Bat Spawns West of Catherby.
- Added Renegade Knights Spawns in the Tower where you do Merlin's Crystal.
- Added Moss Giants Spawns West of the Fishing Guild.
- Added Warrior Woman Spawns south west of the Fishing Guild.
- Added Grandpa Jack's Dialogue and spawn.
- Added Morris at the gate of the Fishing Contest ( I still need to work on gates, doors, etc )
- Added Fishing Contest Fishing spots.
- Added Guard Dog spanws in Mcgrubor Woods.
- Added Candle Seller spawn and Dialogue.
- Added Merchant that yells about Arhein's pineapples.
- Deleted some rsps trash-like teleports from the game.
- Added Vanessa's spawn, Dialogue, and shop in Catherby.
- Added Captain Rovin's Dialogue in Varrock.
- Added Olivia's Shop & Dialogue in Draynor Village.
- Added Rabbit spawns in the Lumbridge Crater Area.
- Started to add a Shops constant to make it more appealing in code.

