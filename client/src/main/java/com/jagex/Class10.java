/* Class10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class10 implements Interface71
{
    int anInt68;
    int anInt69;
    int anInt70;
    int anInt71;
    int anInt72;
    int anInt73;
    boolean aBool74;
    public static int anInt75;
    
    public boolean method485(Interface71 interface71) {
	if (!(interface71 instanceof Class10))
	    return false;
	Class10 class10_0_ = (Class10) interface71;
	if (255874607 * class10_0_.anInt70 != 255874607 * anInt70)
	    return false;
	if (anInt69 * -1041026141 != class10_0_.anInt69 * -1041026141)
	    return false;
	if (class10_0_.anInt72 * 1927749167 != anInt72 * 1927749167)
	    return false;
	if (1824868691 * class10_0_.anInt71 != 1824868691 * anInt71)
	    return false;
	if (class10_0_.anInt73 * -369060565 != -369060565 * anInt73)
	    return false;
	if (-1395126681 * class10_0_.anInt68 != -1395126681 * anInt68)
	    return false;
	if (class10_0_.aBool74 != aBool74)
	    return false;
	return true;
    }
    
    public long method484() {
	long[] ls = Class534_Sub40.aLongArray10812;
	long l = -1L;
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (255874607 * anInt70)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1041026141 * anInt69 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt69 * -1041026141)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72 >> 24))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72 >> 16))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt72 * 1927749167 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1824868691 * anInt71)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565 >> 24))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (-369060565 * anInt73 >> 16))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt68 * -1395126681)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (aBool74 ? 1 : 0)) & 0xffL)];
	return l;
    }
    
    Class10() {
	/* empty */
    }
    
    public long method94() {
	long[] ls = Class534_Sub40.aLongArray10812;
	long l = -1L;
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (255874607 * anInt70)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1041026141 * anInt69 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt69 * -1041026141)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72 >> 24))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72 >> 16))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt72 * 1927749167 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1927749167 * anInt72)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (1824868691 * anInt71)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565 >> 24))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (-369060565 * anInt73 >> 16))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565 >> 8))
				& 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt73 * -369060565)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (anInt68 * -1395126681)) & 0xffL)];
	l = l >>> 8 ^ ls[(int) ((l ^ (long) (aBool74 ? 1 : 0)) & 0xffL)];
	return l;
    }
    
    public boolean method483(Interface71 interface71) {
	if (!(interface71 instanceof Class10))
	    return false;
	Class10 class10_1_ = (Class10) interface71;
	if (255874607 * class10_1_.anInt70 != 255874607 * anInt70)
	    return false;
	if (anInt69 * -1041026141 != class10_1_.anInt69 * -1041026141)
	    return false;
	if (class10_1_.anInt72 * 1927749167 != anInt72 * 1927749167)
	    return false;
	if (1824868691 * class10_1_.anInt71 != 1824868691 * anInt71)
	    return false;
	if (class10_1_.anInt73 * -369060565 != -369060565 * anInt73)
	    return false;
	if (-1395126681 * class10_1_.anInt68 != -1395126681 * anInt68)
	    return false;
	if (class10_1_.aBool74 != aBool74)
	    return false;
	return true;
    }
    
    public boolean method486(Interface71 interface71) {
	if (!(interface71 instanceof Class10))
	    return false;
	Class10 class10_2_ = (Class10) interface71;
	if (255874607 * class10_2_.anInt70 != 255874607 * anInt70)
	    return false;
	if (anInt69 * -1041026141 != class10_2_.anInt69 * -1041026141)
	    return false;
	if (class10_2_.anInt72 * 1927749167 != anInt72 * 1927749167)
	    return false;
	if (1824868691 * class10_2_.anInt71 != 1824868691 * anInt71)
	    return false;
	if (class10_2_.anInt73 * -369060565 != -369060565 * anInt73)
	    return false;
	if (-1395126681 * class10_2_.anInt68 != -1395126681 * anInt68)
	    return false;
	if (class10_2_.aBool74 != aBool74)
	    return false;
	return true;
    }
    
    public static Class393 method606(Class534_Sub40 class534_sub40, byte i) {
	Class401 class401 = (Class72.method1560(1926184431)
			     [class534_sub40.method16527(-896182661)]);
	Class391 class391 = (Class705.method14234(1470740124)
			     [class534_sub40.method16527(-1728726377)]);
	int i_3_ = class534_sub40.method16530((byte) -115);
	int i_4_ = class534_sub40.method16530((byte) -64);
	int i_5_ = class534_sub40.method16550((byte) 52);
	int i_6_ = class534_sub40.method16533(-258848859);
	int i_7_ = class534_sub40.method16527(-993996464);
	return new Class393(class401, class391, i_3_, i_4_, i_5_, i_6_, i_7_);
    }
    
    static int[] method607(Class534_Sub18_Sub7 class534_sub18_sub7, short i) {
	int[] is = null;
	if (Class698.method14124(-1986934021 * class534_sub18_sub7.anInt11706,
				 1024308122))
	    is = ((Class15) (Class531.aClass44_Sub7_7135.method91
			     ((int) (-7225575275964615095L
				     * class534_sub18_sub7.aLong11702),
			      -396269455))).anIntArray177;
	else if (-1603986365 * class534_sub18_sub7.anInt11698 != -1)
	    is = (((Class15)
		   Class531.aClass44_Sub7_7135.method91(((class534_sub18_sub7
							  .anInt11698)
							 * -1603986365),
							-181463108))
		  .anIntArray177);
	else if (Class534_Sub4.method16039((-1986934021
					    * class534_sub18_sub7.anInt11706),
					   321939350)) {
	    Class534_Sub6 class534_sub6
		= ((Class534_Sub6)
		   (client.aClass9_11331.method579
		    ((long) (int) (-7225575275964615095L
				   * class534_sub18_sub7.aLong11702))));
	    if (null != class534_sub6) {
		Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
		    = ((Class654_Sub1_Sub5_Sub1_Sub1)
		       class534_sub6.anObject10417);
		Class307 class307
		    = class654_sub1_sub5_sub1_sub1.aClass307_12204;
		if (null != class307.anIntArray3284)
		    class307 = class307.method5615(Class78.aClass103_825,
						   Class78.aClass103_825,
						   -1466068515);
		if (class307 != null)
		    is = class307.anIntArray3339;
	    }
	} else if (Class421.method6783((-1986934021
					* class534_sub18_sub7.anInt11706),
				       (byte) -17)) {
	    Class602 class602
		= ((Class602)
		   (client.aClass512_11100.method8428(-1486655428).method91
		    ((int) ((class534_sub18_sub7.aLong11702
			     * -7225575275964615095L) >>> 32
			    & 0x7fffffffL),
		     -1889526406)));
	    if (null != class602.anIntArray7943)
		class602
		    = class602.method9988(Class78.aClass103_825,
					  Class78.aClass103_825, -391142222);
	    if (null != class602)
		is = class602.anIntArray7958;
	}
	return is;
    }
    
    static final void method608(Class669 class669, int i) {
	int i_8_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4044(i_8_, 1481307617)
		  .method3962((byte) -95);
    }
    
    public static void method609(int i) {
	Class334.aBool3511 = true;
	Class534_Sub21.aString10536 = Class65.aString694;
	Class367.aString3856 = Class65.aString665;
	Class622.method10291(false, 833218919);
	Class235.method4403(2033412872);
	Class334.aClass294Array3506 = null;
	Class522.aClass467_7082 = null;
	Class673.method11110(8, -1392515061);
    }
}
