/* Class103 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class103 implements Interface20, Interface18, Interface73
{
    Class44_Sub11 aClass44_Sub11_1293;
    public Class612 aClass612_1294;
    Class110_Sub1_Sub1 aClass110_Sub1_Sub1_1295;
    Class626[] aClass626Array1296;
    
    public Class150 method108(Class453 class453, int i, int i_0_) {
	if (Class453.aClass453_4946 != class453)
	    return null;
	return (Class150) aClass110_Sub1_Sub1_1295.method91(i, -5011122);
    }
    
    public int method1906(int i, int i_1_) {
	return aClass626Array1296[i].method10363((client.aBool11157
						  ? Class681.aClass681_8671
						  : Class681.aClass681_8670),
						 -1724923633);
    }
    
    public int method1907(int i, int i_2_) {
	return aClass626Array1296[i].method10346(-1634641902);
    }
    
    public int method491(int i, byte i_3_) {
	return aClass626Array1296[i].method10332((client.aBool11157
						  ? Class681.aClass681_8671
						  : Class681.aClass681_8670),
						 (byte) -92);
    }
    
    public int method1908(int i, int i_4_) {
	return aClass626Array1296[i].method10333(-996428930);
    }
    
    public int method1909(int i, int i_5_) {
	return aClass626Array1296[i].method10336(708143525);
    }
    
    public int method1910(int i) {
	return aClass626Array1296[i].method10336(1648229793);
    }
    
    public Class318 method107(int i, int i_6_) {
	Class318 class318
	    = (Class318) aClass44_Sub11_1293.method91(i, 665800787);
	if (class318.aClass150_3392.aClass453_1695 != Class453.aClass453_4946)
	    return null;
	return class318;
    }
    
    public Class318 method110(int i) {
	Class318 class318
	    = (Class318) aClass44_Sub11_1293.method91(i, 626458192);
	if (class318.aClass150_3392.aClass453_1695 != Class453.aClass453_4946)
	    return null;
	return class318;
    }
    
    public int method119(Class318 class318, int i) {
	return aClass612_1294.method119(class318, -1837050483);
    }
    
    public int method1911(int i) {
	return aClass626Array1296[i].method10363((client.aBool11157
						  ? Class681.aClass681_8671
						  : Class681.aClass681_8670),
						 -1724923633);
    }
    
    public int method1912(int i) {
	return aClass626Array1296[i].method10336(1692330328);
    }
    
    public int method1913(int i) {
	return aClass626Array1296[i].method10346(-1634641902);
    }
    
    public int method123(Class318 class318) {
	return aClass612_1294.method119(class318, -1713341706);
    }
    
    public int method492(int i) {
	return aClass626Array1296[i].method10332((client.aBool11157
						  ? Class681.aClass681_8671
						  : Class681.aClass681_8670),
						 (byte) -44);
    }
    
    public int method1914(int i) {
	return aClass626Array1296[i].method10333(-1005209626);
    }
    
    public int method1915(int i) {
	return aClass626Array1296[i].method10333(-1399372563);
    }
    
    public int method1916(int i) {
	return aClass626Array1296[i].method10333(-1836703023);
    }
    
    public int method120(Class150 class150, byte i) {
	return aClass612_1294.method120(class150, (byte) -88);
    }
    
    public int method34(int i) {
	return aClass626Array1296[i].method10332((client.aBool11157
						  ? Class681.aClass681_8671
						  : Class681.aClass681_8670),
						 (byte) -57);
    }
    
    public Class150 method109(Class453 class453, int i) {
	if (Class453.aClass453_4946 != class453)
	    return null;
	return (Class150) aClass110_Sub1_Sub1_1295.method91(i, 1324054121);
    }
    
    public Class318 method111(int i) {
	Class318 class318
	    = (Class318) aClass44_Sub11_1293.method91(i, -2098416152);
	if (class318.aClass150_3392.aClass453_1695 != Class453.aClass453_4946)
	    return null;
	return class318;
    }
    
    public int method1917(int i) {
	return aClass626Array1296[i].method10346(-1634641902);
    }
    
    public Class103(Class110_Sub1_Sub1 class110_sub1_sub1,
		    Class44_Sub11 class44_sub11, int i) {
	aClass626Array1296 = new Class626[i];
	aClass612_1294 = new Class612();
	aClass110_Sub1_Sub1_1295 = class110_sub1_sub1;
	aClass44_Sub11_1293 = class44_sub11;
    }
    
    public int method117(Class150 class150) {
	return aClass612_1294.method120(class150, (byte) -97);
    }
    
    public int method136(Class150 class150) {
	return aClass612_1294.method120(class150, (byte) -30);
    }
    
    public Class318 method112(int i) {
	Class318 class318
	    = (Class318) aClass44_Sub11_1293.method91(i, -1815605271);
	if (class318.aClass150_3392.aClass453_1695 != Class453.aClass453_4946)
	    return null;
	return class318;
    }
    
    public int method135(Class318 class318) {
	return aClass612_1294.method119(class318, -1959754562);
    }
    
    public int method125(Class318 class318) {
	return aClass612_1294.method119(class318, -1694173325);
    }
    
    public int method126(Class318 class318) {
	return aClass612_1294.method119(class318, -1789647431);
    }
    
    static final void method1918
	(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1, byte i) {
	Class711 class711 = class654_sub1_sub5_sub1.aClass711_11948;
	if ((class654_sub1_sub5_sub1.anInt11957 * -1259938159
	     == client.anInt11101)
	    || !class711.method14338((byte) 0)
	    || class711.method14373(1, -594751363)) {
	    int i_7_ = (-1259938159 * class654_sub1_sub5_sub1.anInt11957
			- class654_sub1_sub5_sub1.anInt11956 * -1626386689);
	    int i_8_ = (client.anInt11101
			- -1626386689 * class654_sub1_sub5_sub1.anInt11956);
	    int i_9_ = (class654_sub1_sub5_sub1.anInt11943 * 1791130112
			+ class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    int i_10_
		= (660825600 * class654_sub1_sub5_sub1.anInt11945
		   + class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    int i_11_
		= (class654_sub1_sub5_sub1.anInt11951 * 1449754112
		   + class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    int i_12_
		= (class654_sub1_sub5_sub1.anInt11976 * -199475712
		   + class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    int i_13_ = ((i_7_ - i_8_) * i_9_ + i_11_ * i_8_) / i_7_;
	    int i_14_ = (i_10_ * (i_7_ - i_8_) + i_12_ * i_8_) / i_7_;
	    int i_15_ = Class247.method4595(i_13_, i_14_,
					    (class654_sub1_sub5_sub1.anInt11954
					     * -2033865703),
					    -1792426357);
	    int i_16_ = Class247.method4595(i_11_, i_12_,
					    (class654_sub1_sub5_sub1.anInt11955
					     * -1853533151),
					    382488024);
	    int i_17_ = (i_8_ * i_16_ + (i_7_ - i_8_) * i_15_) / i_7_;
	    class654_sub1_sub5_sub1.method10815((float) i_13_, (float) i_17_,
						(float) i_14_);
	}
	class654_sub1_sub5_sub1.anInt11947 = 0;
	class654_sub1_sub5_sub1.method18523((class654_sub1_sub5_sub1.anInt11958
					     * -565919971),
					    false, 835859038);
    }
    
    static final void method1919(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub2_10744
		  .method16833(-441280418);
    }
    
    static final void method1920(Class654_Sub1 class654_sub1,
				 Class669 class669, byte i) {
	boolean bool = false;
	int i_18_ = 0;
	int i_19_ = 0;
	int i_20_ = 0;
	int i_21_ = 0;
	if (class654_sub1.aClass194Array10852 != null) {
	    for (int i_22_ = 0;
		 i_22_ < class654_sub1.aClass194Array10852.length; i_22_++) {
		Class194 class194 = class654_sub1.aClass194Array10852[i_22_];
		if (class194.aBool2150) {
		    int i_23_;
		    int i_24_;
		    if (class194.anInt2152 < class194.anInt2148) {
			i_23_ = class194.anInt2152 - class194.anInt2147;
			i_24_ = class194.anInt2148 + class194.anInt2147;
		    } else {
			i_23_ = class194.anInt2148 - class194.anInt2147;
			i_24_ = class194.anInt2152 + class194.anInt2147;
		    }
		    int i_25_;
		    int i_26_;
		    if (class194.anInt2151 < class194.anInt2149) {
			i_25_ = class194.anInt2151 - class194.anInt2147;
			i_26_ = class194.anInt2149 + class194.anInt2147;
		    } else {
			i_25_ = class194.anInt2149 - class194.anInt2147;
			i_26_ = class194.anInt2151 + class194.anInt2147;
		    }
		    if (!bool || i_23_ < i_18_)
			i_18_ = i_23_;
		    if (!bool || i_25_ < i_19_)
			i_19_ = i_25_;
		    if (!bool || i_24_ > i_20_)
			i_20_ = i_24_;
		    if (!bool || i_26_ > i_21_)
			i_21_ = i_26_;
		    bool = true;
		}
	    }
	}
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = bool ? 1 : 0;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_18_;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_19_;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_20_;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_21_;
    }
}
