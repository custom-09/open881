/* Class105 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class105
{
    static Class391 aClass391_1299;
    static int anInt1300;
    static int anInt1301;
    static int anInt1302;
    static int anInt1303;
    static int anInt1304;
    static Class401 aClass401_1305;
    
    public static void method1932(Class401 class401, Class391 class391, int i,
				  int i_0_, int i_1_, int i_2_, int i_3_,
				  int i_4_, int i_5_, int i_6_, int i_7_) {
	aClass401_1305 = class401;
	aClass391_1299 = class391;
	anInt1300 = 1067872339 * i;
	anInt1302 = -1578885789 * i_0_;
	Class130.anInt1523 = -482153117 * i_1_;
	Class90.anInt891 = 1739452007 * i_2_;
	Class666.anInt8576 = i_3_ * 1150809657;
	anInt1301 = i_4_ * 1061662017;
	anInt1304 = i_5_ * 1949333389;
	Class690_Sub22.aClass169_10914 = null;
	Class199.aClass169_2176 = null;
	Class90.aClass169_892 = null;
	Class351.anInt3621 = 1559431319 * i_6_;
	anInt1303 = i_7_ * -509538137;
	Class473.method7752(-1937778115);
	Class499.aBool5603 = true;
    }
    
    Class105() throws Throwable {
	throw new Error();
    }
    
    public static void method1933(Class401 class401, Class391 class391, int i,
				  int i_8_, int i_9_, int i_10_, int i_11_,
				  int i_12_, int i_13_, int i_14_, int i_15_) {
	aClass401_1305 = class401;
	aClass391_1299 = class391;
	anInt1300 = 1067872339 * i;
	anInt1302 = -1578885789 * i_8_;
	Class130.anInt1523 = -482153117 * i_9_;
	Class90.anInt891 = 1739452007 * i_10_;
	Class666.anInt8576 = i_11_ * 1150809657;
	anInt1301 = i_12_ * 1061662017;
	anInt1304 = i_13_ * 1949333389;
	Class690_Sub22.aClass169_10914 = null;
	Class199.aClass169_2176 = null;
	Class90.aClass169_892 = null;
	Class351.anInt3621 = 1559431319 * i_14_;
	anInt1303 = i_15_ * -509538137;
	Class473.method7752(-1554611274);
	Class499.aBool5603 = true;
    }
    
    public static void method1934(Class401 class401, Class391 class391, int i,
				  int i_16_, int i_17_, int i_18_, int i_19_,
				  int i_20_, int i_21_, int i_22_, int i_23_) {
	aClass401_1305 = class401;
	aClass391_1299 = class391;
	anInt1300 = 1067872339 * i;
	anInt1302 = -1578885789 * i_16_;
	Class130.anInt1523 = -482153117 * i_17_;
	Class90.anInt891 = 1739452007 * i_18_;
	Class666.anInt8576 = i_19_ * 1150809657;
	anInt1301 = i_20_ * 1061662017;
	anInt1304 = i_21_ * 1949333389;
	Class690_Sub22.aClass169_10914 = null;
	Class199.aClass169_2176 = null;
	Class90.aClass169_892 = null;
	Class351.anInt3621 = 1559431319 * i_22_;
	anInt1303 = i_23_ * -509538137;
	Class473.method7752(-2090572789);
	Class499.aBool5603 = true;
    }
    
    public static void method1935(Class401 class401, Class391 class391, int i,
				  int i_24_, int i_25_, int i_26_, int i_27_,
				  int i_28_, int i_29_, int i_30_, int i_31_) {
	aClass401_1305 = class401;
	aClass391_1299 = class391;
	anInt1300 = 1067872339 * i;
	anInt1302 = -1578885789 * i_24_;
	Class130.anInt1523 = -482153117 * i_25_;
	Class90.anInt891 = 1739452007 * i_26_;
	Class666.anInt8576 = i_27_ * 1150809657;
	anInt1301 = i_28_ * 1061662017;
	anInt1304 = i_29_ * 1949333389;
	Class690_Sub22.aClass169_10914 = null;
	Class199.aClass169_2176 = null;
	Class90.aClass169_892 = null;
	Class351.anInt3621 = 1559431319 * i_30_;
	anInt1303 = i_31_ * -509538137;
	Class473.method7752(-1700189202);
	Class499.aBool5603 = true;
    }
    
    public static void method1936(String string, boolean bool,
				  Class185 class185, Class171 class171,
				  Class16 class16) {
	boolean bool_32_
	    = !Class499.aBool5603 || Class473.method7752(-1657724016);
	if (bool_32_) {
	    if (Class499.aBool5603 && bool_32_) {
		class16 = Class223.aClass16_2314;
		class171
		    = class185.method3325(class16,
					  Class192.aClass169_Sub2Array2146,
					  true);
		int i = class16.method747(string, 250, null, -1156053172);
		int i_33_ = class16.method737(string, 250,
					      class16.anInt187 * -1062735011,
					      null, 2112849964);
		int i_34_ = Class199.aClass169_2176.method2762();
		int i_35_ = 4 + i_34_;
		i += 2 * i_35_;
		i_33_ += i_35_ * 2;
		if (i < Class130.anInt1523 * 2090728523)
		    i = 2090728523 * Class130.anInt1523;
		if (i_33_ < Class90.anInt891 * 1207520599)
		    i_33_ = 1207520599 * Class90.anInt891;
		int i_36_
		    = (aClass401_1305.method6586(i, (client.anInt11047
						     * -321474631), -278876261)
		       + 111236059 * anInt1300);
		int i_37_
		    = (aClass391_1299.method6544(i_33_,
						 43072843 * client.anInt11192,
						 (byte) 17)
		       + -1939452853 * anInt1302);
		class185.method3279(Class90.aClass169_892, false).method2663
		    (i_36_ + Class690_Sub22.aClass169_10914.method2762(),
		     i_37_ + Class690_Sub22.aClass169_10914.method2763(),
		     i - Class690_Sub22.aClass169_10914.method2762() * 2,
		     i_33_ - Class690_Sub22.aClass169_10914.method2763() * 2,
		     1, -1, 0);
		class185.method3279(Class690_Sub22.aClass169_10914, true)
		    .method2656(i_36_, i_37_);
		Class690_Sub22.aClass169_10914.method2806();
		class185.method3279(Class690_Sub22.aClass169_10914, true)
		    .method2656(i + i_36_ - i_34_, i_37_);
		Class690_Sub22.aClass169_10914.method2779();
		class185.method3279(Class690_Sub22.aClass169_10914, true)
		    .method2656(i + i_36_ - i_34_, i_37_ + i_33_ - i_34_);
		Class690_Sub22.aClass169_10914.method2806();
		class185.method3279(Class690_Sub22.aClass169_10914, true)
		    .method2656(i_36_, i_37_ + i_33_ - i_34_);
		Class690_Sub22.aClass169_10914.method2779();
		class185.method3279(Class199.aClass169_2176, true).method2662
		    (i_36_,
		     i_37_ + Class690_Sub22.aClass169_10914.method2763(),
		     i_34_,
		     i_33_ - Class690_Sub22.aClass169_10914.method2763() * 2);
		Class199.aClass169_2176.method2771();
		class185.method3279(Class199.aClass169_2176, true).method2662
		    (i_36_ + Class690_Sub22.aClass169_10914.method2762(),
		     i_37_,
		     i - Class690_Sub22.aClass169_10914.method2762() * 2,
		     i_34_);
		Class199.aClass169_2176.method2771();
		class185.method3279(Class199.aClass169_2176, true).method2662
		    (i + i_36_ - i_34_,
		     i_37_ + Class690_Sub22.aClass169_10914.method2763(),
		     i_34_,
		     i_33_ - Class690_Sub22.aClass169_10914.method2763() * 2);
		Class199.aClass169_2176.method2771();
		class185.method3279(Class199.aClass169_2176, true).method2662
		    (i_36_ + Class690_Sub22.aClass169_10914.method2762(),
		     i_33_ + i_37_ - i_34_,
		     i - Class690_Sub22.aClass169_10914.method2762() * 2,
		     i_34_);
		Class199.aClass169_2176.method2771();
		class171.method2844(string, i_36_ + i_35_, i_37_ + i_35_,
				    i - 2 * i_35_, i_33_ - 2 * i_35_,
				    (2043737895 * Class351.anInt3621
				     | ~0xffffff),
				    -1, 1, 1, 0, null, null, null, 0, 0,
				    202025040);
		Class316.method5724(i_36_, i_37_, i, i_33_, (byte) 1);
	    } else {
		int i = class16.method747(string, 250, null, -2041488910);
		int i_38_
		    = class16.method733(string, 250, null, (byte) -123) * 13;
		int i_39_ = 4;
		int i_40_ = i_39_ + 6;
		int i_41_ = i_39_ + 6;
		class185.method3298(i_40_ - i_39_, i_41_ - i_39_,
				    i_39_ + i + i_39_, i_39_ + (i_39_ + i_38_),
				    -16777216, 0);
		class185.method3297(i_40_ - i_39_, i_41_ - i_39_,
				    i_39_ + (i + i_39_),
				    i_39_ + (i_39_ + i_38_), -1, 0);
		class171.method2844(string, i_40_, i_41_, i, i_38_, -1, -1, 1,
				    1, 0, null, null, null, 0, 0, 202025040);
		Class316.method5724(i_40_ - i_39_, i_41_ - i_39_,
				    i + i_39_ + i_39_, i_39_ + i_38_ + i_39_,
				    (byte) 1);
	    }
	    if (bool) {
		try {
		    class185.method3581();
		    class185.method3289(1665617290);
		} catch (Exception_Sub7 exception_sub7) {
		    /* empty */
		}
	    }
	}
    }
    
    static void method1937(int i, int i_42_) {
	if (4 == i)
	    throw new Error();
	if (i == 19)
	    throw new OutOfMemoryError();
	try {
	    if (i == 27)
		Class247.method4592(-1895257114);
	    else if (i == 14)
		Class73.method1567(new StringBuilder().append("").append
				       (1720947399 * Class498.anInt5554)
				       .toString(),
				   -1748063665);
	    else if (16 == i) {
		Class552 class552 = (client.aClass512_11100.method8424
				     ((byte) 9).aClass552_7427);
		class552.aBool7324 = !class552.aBool7324;
	    } else if (10 == i)
		client.aBool11262 = true;
	    else if (3 == i)
		client.aBool11262 = false;
	    else if (13 == i)
		client.aClass709_11212.method14283(-1107967299);
	    else if (7 == i) {
		Class171_Sub4.method15601((byte) 0);
		for (int i_43_ = 0; i_43_ < 10; i_43_++)
		    System.gc();
		Runtime runtime = Runtime.getRuntime();
		int i_44_
		    = (int) ((runtime.totalMemory() - runtime.freeMemory())
			     / 1024L);
		Class73.method1567(new StringBuilder().append("").append
				       (i_44_).toString(),
				   -1073793948);
	    } else if (i == 2) {
		Class171_Sub4.method15601((byte) 0);
		for (int i_45_ = 0; i_45_ < 10; i_45_++)
		    System.gc();
		Runtime runtime = Runtime.getRuntime();
		int i_46_
		    = (int) ((runtime.totalMemory() - runtime.freeMemory())
			     / 1024L);
		Class73.method1567(new StringBuilder().append("").append
				       (i_46_).toString(),
				   -2067695296);
		Class151.method2544((byte) 54);
		Class171_Sub4.method15601((byte) 0);
		for (int i_47_ = 0; i_47_ < 10; i_47_++)
		    System.gc();
		i_46_ = (int) ((runtime.totalMemory() - runtime.freeMemory())
			       / 1024L);
		Class73.method1567(new StringBuilder().append("").append
				       (i_46_).toString(),
				   -1448409170);
	    } else if (i == 20)
		Class73.method1567((Class632.aClass538_8269
					.method402((short) -19248)
				    ? "Success" : "Failure"),
				   -1743505062);
	    else if (9 == i)
		Class685.aClass23_8698.method818((byte) -108);
	    else if (i == 22)
		Class6.aClass450_56.method7341(1562205118);
	    else if (i == 29)
		Class6.aClass450_56.method7357(1907704907);
	    else if (1 == i)
		Class464.aCanvas5111.setLocation(50, 50);
	    else if (i == 15)
		Class464.aCanvas5111.setLocation((Class498.anInt5562
						  * -721302779),
						 (Class498.anInt5563
						  * -350211099));
	    else if (25 == i)
		Class55.method1213(-464678423);
	    else if (i == 26) {
		client.aClass512_11100.aLong5722
		    = Class250.method4604((byte) -32) * -7615436911730204861L;
		client.aClass512_11100.aBool5700 = true;
		Class55.method1213(-160321948);
	    } else if (6 == i) {
		Class438 class438
		    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method10807
		       ().aClass438_4885);
		Class73.method1567(new StringBuilder().append
				       ((int) class438.aFloat4864 >> 9).append
				       (" ").append
				       ((int) class438.aFloat4865 >> 9)
				       .toString(),
				   -1380375494);
	    } else if (i == 11) {
		Class438 class438
		    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method10807
		       ().aClass438_4885);
		Class73.method1567
		    (new StringBuilder().append("").append
			 (client.aClass512_11100.method8424((byte) 21)
			      .aClass151Array7432
			      [(Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
				.aByte10854)]
			      .method2491
			  ((int) class438.aFloat4864 >> 9,
			   (int) class438.aFloat4865 >> 9, -1472441977))
			 .toString(),
		     -816390576);
	    } else if (i == 21) {
		Class73.method1567(new StringBuilder().append
				       (Class247.aClass203_2552
					    .method3879(-1279247856))
				       .append
				       (" ").append
				       (Class247.aClass203_2552
					    .method3878(-950493403))
				       .toString(),
				   -756070577);
		Class73.method1567(new StringBuilder().append
				       (Class247.aClass203_2504
					    .method3879(1783654043))
				       .append
				       (" ").append
				       (Class247.aClass203_2504
					    .method3878(-670919342))
				       .toString(),
				   -1498739815);
	    } else if (i == 17)
		Class11.method611(false, -674901920);
	    else if (8 == i) {
		client.aBool11051 = !client.aBool11051;
		Class254.aClass185_2683.method3371(client.aBool11051);
		Class227.method4183((byte) 16);
	    } else if (i == 24) {
		client.anInt11060 = 0;
		client.aClass512_11100.method8441(1301971078);
	    } else if (28 == i) {
		client.anInt11060 = -1810851633;
		client.aClass512_11100.method8441(1280427890);
	    } else if (23 == i) {
		client.anInt11060 = 673264030;
		client.aClass512_11100.method8441(1324638997);
	    }
	} catch (Exception exception) {
	    Class73.method1567
		(Class58.aClass58_469.method1245(Class539.aClass672_7171,
						 (byte) -7),
		 -699271955);
	}
    }
    
    static final void method1938(Class247 class247, Class243 class243,
				 Class669 class669, byte i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2619
	    = Class99.method1859(string, class669, 1369715023);
	class247.aBool2561 = true;
    }
    
    static final void method1939(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    public static String method1940(CharSequence charsequence, byte i) {
	String string = Class556.method9396(Class666.method11019(charsequence,
								 2088438307));
	if (string == null)
	    string = "";
	return string;
    }
    
    static final void method1941(Class669 class669, byte i) {
	int i_48_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anInt8594 -= -1374580330;
	String string
	    = ((String)
	       class669.anObjectArray8593[class669.anInt8594 * 1485266147]);
	String string_49_ = (String) (class669.anObjectArray8593
				      [1 + class669.anInt8594 * 1485266147]);
	if (string.length() <= 500 && string_49_.length() <= 500)
	    Class410.method6715(i_48_, string, string_49_, 820386454);
    }
    
    static final void method1942(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	class669.anInt8600 -= 1853997378;
	class247.anInt2500 = -1840354219 * (class669.anIntArray8595
					    [2088438307 * class669.anInt8600]);
	class247.anInt2598
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600 + 1]
	       * 640385431);
	class247.anInt2502
	    = (-2066017421
	       * class669.anIntArray8595[2088438307 * class669.anInt8600 + 2]);
	class247.anInt2503
	    = (2087543833
	       * class669.anIntArray8595[2088438307 * class669.anInt8600 + 3]);
	class247.anInt2624
	    = (-1364343373
	       * class669.anIntArray8595[2088438307 * class669.anInt8600 + 4]);
	class247.anInt2508
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600 + 5]
	       * -2116337403);
	Class454.method7416(class247, -591389859);
	if (-1 == class247.anInt2580 * 1365669833 && !class243.aBool2413) {
	    Class551.method9052(class247.anInt2454 * -1278450723, -1614473260);
	    Class653.method10806(-1278450723 * class247.anInt2454,
				 -1840294843);
	}
    }
}
