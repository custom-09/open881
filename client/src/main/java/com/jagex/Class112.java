/* Class112 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;

public class Class112
{
    static final int anInt1351 = 100000;
    static final int anInt1352 = 101;
    public static int anInt1353;
    public static boolean aBool1354 = false;
    static final int anInt1355 = 10000;
    static final int anInt1356 = 501;
    static final int anInt1357 = 1004;
    static final float aFloat1358 = 1.3F;
    static final long aLong1359 = 60129613779L;
    static final int anInt1360 = 50000;
    public static boolean aBool1361 = false;
    static boolean aBool1362 = false;
    static final long aLong1363 = 64425238954L;
    static int anInt1364;
    
    public static void method1998() {
	Class24 class24 = null;
	try {
	    class24
		= Class606.method10050("", client.aClass675_11016.aString8640,
				       true, 2109258620);
	    Class534_Sub40 class534_sub40
		= Class44_Sub6.aClass534_Sub35_10989.method16436(2035312240);
	    class24.method844(class534_sub40.aByteArray10810, 0,
			      31645619 * class534_sub40.anInt10811, 454464553);
	} catch (Exception exception) {
	    /* empty */
	}
	try {
	    if (null != class24)
		class24.method832(1800635104);
	} catch (Exception exception) {
	    /* empty */
	}
    }
    
    public static int method1999() {
	Class52_Sub1 class52_sub1 = Class609.method10070(-1213435377);
	Class545.method8964(class52_sub1, -1966121501);
	return class52_sub1.method1182((short) 30775);
    }
    
    static void method2000(Class52_Sub1 class52_sub1) {
	class52_sub1.method16415(0, -1969092790);
	int i;
	if (721369631 * Class498.anInt5589 >= 96) {
	    int i_0_ = Class637.method10559((byte) 118);
	    if (i_0_ <= 101) {
		Class352.method6257(-2072906297);
		i = 4;
	    } else if (i_0_ <= 501) {
		Class465.method7571(2003013996);
		i = 3;
	    } else if (i_0_ <= 1004) {
		Class664.method10999(1746877648);
		i = 2;
	    } else {
		Class232.method4337(true, -1320739871);
		i = 1;
	    }
	    class52_sub1.method16409(0, i_0_, -2053584835);
	} else {
	    Class232.method4337(true, -1975983803);
	    i = 1;
	    class52_sub1.method16407(64, 1851149389);
	}
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		.method16935(-1807368365)
	    != 0) {
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10764, 0,
		 1866445334);
	    Class527.method8778(0, false, 2114810087);
	} else
	    Class44_Sub6.aClass534_Sub35_10989.method16439
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733, true,
		 (byte) 118);
	Class672.method11096((byte) 1);
	class52_sub1.method16411(i, 1194504499);
    }
    
    public static void method2001() {
	Class24 class24 = null;
	try {
	    class24
		= Class606.method10050("", client.aClass675_11016.aString8640,
				       true, 2081048747);
	    Class534_Sub40 class534_sub40
		= Class44_Sub6.aClass534_Sub35_10989.method16436(1976180811);
	    class24.method844(class534_sub40.aByteArray10810, 0,
			      31645619 * class534_sub40.anInt10811, 454464553);
	} catch (Exception exception) {
	    /* empty */
	}
	try {
	    if (null != class24)
		class24.method832(907877527);
	} catch (Exception exception) {
	    /* empty */
	}
    }
    
    static Class534_Sub35 method2002() {
	Class24 class24 = null;
	Class534_Sub35 class534_sub35
	    = new Class534_Sub35(client.aClass675_11016, 0);
	try {
	    class24
		= Class606.method10050("", client.aClass675_11016.aString8640,
				       false, 2105253122);
	    byte[] is = new byte[(int) class24.method831(261153999)];
	    int i;
	    for (int i_1_ = 0; i_1_ < is.length; i_1_ += i) {
		i = class24.method843(is, i_1_, is.length - i_1_, 2101582941);
		if (i == -1)
		    throw new IOException();
	    }
	    class534_sub35 = new Class534_Sub35(new Class534_Sub40(is),
						client.aClass675_11016, 0);
	} catch (Exception exception) {
	    /* empty */
	}
	try {
	    if (class24 != null)
		class24.method832(1847236583);
	} catch (Exception exception) {
	    /* empty */
	}
	return class534_sub35;
    }
    
    public static void method2003() {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 1,
	     562998642);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 1,
	     681106368);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     -377143730);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     2063179184);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 0,
	     1739901506);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       0, -1015178429);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       0, 1662032312);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       0, -1699232093);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 0,
	     1933149526);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 0,
	     560821078);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, -1129832308);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 0,
	     160131926);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     1443167520);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     805127453);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       0, 1356306371);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     1453209707 * Class302.aClass302_3246.anInt3244, -139851083);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     -810713696);
	if (Class254.aClass185_2683 != null
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 0,
	     1023033803);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 0,
	     1784953576);
	Class480.method7924(1574739790);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 2,
	     293364925);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 2,
	     928621158);
	Class635.method10538(-1601819208);
	client.aClass512_11100.method8501((byte) -22).method10157(690081376);
	client.aBool11059 = true;
    }
    
    public static int method2004() {
	Class52_Sub1 class52_sub1 = Class609.method10070(-1213435377);
	Class545.method8964(class52_sub1, -835542819);
	return class52_sub1.method1182((short) 21506);
    }
    
    public static int method2005() {
	Class52_Sub1 class52_sub1 = Class609.method10070(-1213435377);
	Class545.method8964(class52_sub1, -671219819);
	return class52_sub1.method1182((short) 10717);
    }
    
    public static void method2006() {
	Class24 class24 = null;
	try {
	    class24
		= Class606.method10050("", client.aClass675_11016.aString8640,
				       true, 2124844437);
	    Class534_Sub40 class534_sub40
		= Class44_Sub6.aClass534_Sub35_10989.method16436(2007759190);
	    class24.method844(class534_sub40.aByteArray10810, 0,
			      31645619 * class534_sub40.anInt10811, 454464553);
	} catch (Exception exception) {
	    /* empty */
	}
	try {
	    if (null != class24)
		class24.method832(1153528745);
	} catch (Exception exception) {
	    /* empty */
	}
    }
    
    public static int method2007() {
	Class52_Sub1 class52_sub1 = Class609.method10070(-1213435377);
	Class545.method8964(class52_sub1, -549234870);
	return class52_sub1.method1182((short) 18033);
    }
    
    static void method2008(Class52_Sub1 class52_sub1) {
	class52_sub1.method16415(0, -1838367156);
	int i;
	if (721369631 * Class498.anInt5589 >= 96) {
	    int i_2_ = Class637.method10559((byte) 4);
	    if (i_2_ <= 101) {
		Class352.method6257(42881674);
		i = 4;
	    } else if (i_2_ <= 501) {
		Class465.method7571(105177635);
		i = 3;
	    } else if (i_2_ <= 1004) {
		Class664.method10999(1946626176);
		i = 2;
	    } else {
		Class232.method4337(true, -2106270405);
		i = 1;
	    }
	    class52_sub1.method16409(0, i_2_, -2068786730);
	} else {
	    Class232.method4337(true, -2118203697);
	    i = 1;
	    class52_sub1.method16407(64, -1167206297);
	}
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		.method16935(-1807368365)
	    != 0) {
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10764, 0,
		 543768269);
	    Class527.method8778(0, false, 1948525828);
	} else
	    Class44_Sub6.aClass534_Sub35_10989.method16439
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733, true,
		 (byte) 107);
	Class672.method11096((byte) 1);
	class52_sub1.method16411(i, 1194504499);
    }
    
    static void method2009(Class52_Sub1 class52_sub1, int i, int i_3_) {
	class52_sub1.method16415(i, -1855617122);
	int i_4_;
	if (i_3_ > 100000) {
	    Class352.method6257(-1254866033);
	    i_4_ = 4;
	} else if (i_3_ > 50000) {
	    Class465.method7571(-679740769);
	    i_4_ = 3;
	} else if (i_3_ > 10000) {
	    Class664.method10999(1551481907);
	    i_4_ = 2;
	} else {
	    Class232.method4337(true, -1274508020);
	    i_4_ = 1;
	}
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		.method16935(-1807368365)
	    != i) {
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10764, i,
		 1258569684);
	    Class527.method8778(i, false, -557432790);
	} else
	    Class44_Sub6.aClass534_Sub35_10989.method16439
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733, true,
		 (byte) 19);
	Class672.method11096((byte) 1);
	class52_sub1.method16411(i_4_, 1194504499);
    }
    
    public static void method2010() {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 2,
	     1848699687);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 2,
	     -1647244323);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     -1595573731);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     -33274996);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 1,
	     475206825);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       1, 717911263);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       1, -2129539123);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 1,
	     -1584638078);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       2, -429407194);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 1,
	     1184241378);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       2, -1781854602);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 1,
	     -680579776);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     -981907139);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     867884986);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       2, 123932480);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     Class302.aClass302_3246.anInt3244 * 1453209707, 1156213604);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     1828919071);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 1,
	     -1366625460);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 1,
	     1755412451);
	Class480.method7924(-1061474652);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 0,
	     -913686631);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 4,
	     1912383117);
	Class635.method10538(-1952705849);
	client.aClass512_11100.method8501((byte) 71).method10157(901467140);
	client.aBool11059 = true;
    }
    
    public static void method2011() {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 2,
	     493772173);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 2,
	     811252903);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     70070429);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     -1080125481);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 1,
	     -384695168);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       1, 2049270375);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       1, 837824573);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 1,
	     -18213667);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       2, -54339013);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 1,
	     -71991159);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       2, 600501587);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 1,
	     788165394);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     809324257);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     1164346119);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       2, -660314674);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     Class302.aClass302_3246.anInt3244 * 1453209707, 1697191283);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     1027146408);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 1,
	     -1384436632);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 1,
	     984070313);
	Class480.method7924(-251953980);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 0,
	     -2062840174);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 4,
	     -470239277);
	Class635.method10538(-598881311);
	client.aClass512_11100.method8501((byte) -21).method10157(1232749751);
	client.aBool11059 = true;
    }
    
    public static void method2012() {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 2,
	     -1365518394);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 2,
	     -1885259790);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     -1185676656);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     52855991);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 1,
	     186317513);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       1, 1563146948);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       1, -1885615843);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 1,
	     -1471920710);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       1, -1350047832);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 1,
	     -764006580);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, -769634438);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 1,
	     2130202446);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     404213551);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     979987654);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       1, -1820706185);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     Class302.aClass302_3246.anInt3244 * 1453209707, -2038538246);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     -445840717);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 1,
	     -1980349056);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 1,
	     1498859638);
	Class480.method7924(862582267);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 1,
	     -1786457520);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 3,
	     1447327262);
	Class635.method10538(54376223);
	client.aClass512_11100.method8501((byte) 66).method10157(870739399);
	client.aBool11059 = true;
    }
    
    public static void method2013() {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 2,
	     -816588588);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 2,
	     -331648156);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     -172093705);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     -2070838807);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 1,
	     402007345);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       1, 190567108);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       1, 1097744033);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 1,
	     -1919949540);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       1, 1409604115);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 1,
	     478789032);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, -1371718635);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 1,
	     894699342);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     -820352506);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     -381178708);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       1, 294962725);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     Class302.aClass302_3246.anInt3244 * 1453209707, 526482090);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     118709413);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 1,
	     -1828365307);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 1,
	     735244728);
	Class480.method7924(1935806394);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 1,
	     1590672871);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 3,
	     -88743777);
	Class635.method10538(1810995591);
	client.aClass512_11100.method8501((byte) -38).method10157(1722258628);
	client.aBool11059 = true;
    }
    
    Class112() throws Throwable {
	throw new Error();
    }
    
    public static void method2014(boolean bool) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 1,
	     2038291623);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 1,
	     -1896940017);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 0,
	     851414137);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 0,
	     -16467822);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 0,
	     1020132612);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 0,
	     1859981284);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       0, -1577698809);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       0, 1110270166);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       0, 475099207);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 0,
	     84843423);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 0,
	     -168665964);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, 183923057);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     1323342819);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     971754247);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       0, 943647834);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     1453209707 * Class302.aClass302_3246.anInt3244, 1371674677);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     1387949919);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 0,
	     1179650165);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 0,
	     1748106408);
	Class480.method7924(1508851239);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 2,
	     877608064);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 1,
	     1852151374);
	Class635.method10538(1162412379);
	client.aClass512_11100.method8501((byte) -8).method10157(1432024190);
	client.aBool11059 = true;
    }
    
    public static void method2015(boolean bool) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 1,
	     467370195);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 1,
	     862916057);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 0,
	     2053185861);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 0,
	     1707469396);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 0,
	     1093854616);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 0,
	     -1985896614);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       0, 922306308);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       0, -1649531609);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       0, -642421043);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 0,
	     -1573387864);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 0,
	     774606284);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, -382348662);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     -114221814);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     468084815);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       0, -1600874688);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     1453209707 * Class302.aClass302_3246.anInt3244, 1562083924);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     -316851336);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 0,
	     247086059);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 0,
	     913991824);
	Class480.method7924(-282362376);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 2,
	     -965002650);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 1,
	     -840752689);
	Class635.method10538(-1607826528);
	client.aClass512_11100.method8501((byte) 19).method10157(1874054349);
	client.aBool11059 = true;
    }
    
    static void method2016(String string, String string_5_, int i, int i_6_,
			   int i_7_, long l, int i_8_, int i_9_, boolean bool,
			   boolean bool_10_, long l_11_, boolean bool_12_,
			   short i_13_) {
	if (!Class72.aBool758 && Class72.anInt765 * 324852453 < 504) {
	    i = i != -1 ? i : 846725895 * client.anInt11215;
	    Class534_Sub18_Sub7 class534_sub18_sub7
		= new Class534_Sub18_Sub7(string, string_5_, i, i_6_, i_7_, l,
					  i_8_, i_9_, bool, bool_10_, l_11_,
					  bool_12_);
	    Class517.method8636(class534_sub18_sub7, (byte) 0);
	}
    }
    
    public static Class247 method2017(int i, int i_14_) {
	int i_15_ = i >> 16;
	if (null == Class44_Sub11.aClass243Array11006[i_15_]
	    || (Class44_Sub11.aClass243Array11006[i_15_].method4476(i,
								    -101905411)
		== null)) {
	    boolean bool = Class180.method2978(i_15_, null, -1573189031);
	    if (!bool)
		return null;
	}
	return Class44_Sub11.aClass243Array11006[i_15_].method4476(i,
								   -299085166);
    }
    
    public static Interface60 method2018(int i) {
	if (Class510.anInterface60_5677 == null)
	    throw new IllegalStateException("");
	return Class510.anInterface60_5677;
    }
    
    public static int method2019(int i) {
	return (null == Class574.aTwitchWebcamDeviceArray7704 ? 0
		: Class574.aTwitchWebcamDeviceArray7704.length);
    }
}
