/* Class134_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaclib.memory.Buffer;

public class Class134_Sub1 extends Class134 implements Interface15
{
    int anInt8955;
    
    public void method97(int i, byte[] is, int i_0_) {
	method2318(is, i_0_);
	anInt8955 = i;
    }
    
    Class134_Sub1(Class185_Sub3 class185_sub3, int i, byte[] is, int i_1_,
		  boolean bool) {
	super(class185_sub3, 34962, is, i_1_, bool);
	anInt8955 = i;
    }
    
    public int method1() {
	return anInt1591;
    }
    
    public int method93() {
	return anInt8955;
    }
    
    public long method94() {
	return 0L;
    }
    
    void method2320() {
	aClass185_Sub3_1587.method15220(this);
    }
    
    public void method95(int i, byte[] is, int i_2_) {
	method2318(is, i_2_);
	anInt8955 = i;
    }
    
    Class134_Sub1(Class185_Sub3 class185_sub3, int i, Buffer buffer, int i_3_,
		  boolean bool) {
	super(class185_sub3, 34962, buffer, i_3_, bool);
	anInt8955 = i;
    }
    
    public int method53() {
	return anInt1591;
    }
    
    public int method88() {
	return anInt8955;
    }
    
    public int method8() {
	return anInt8955;
    }
    
    public int method85() {
	return anInt1591;
    }
    
    public void method99(int i, byte[] is, int i_4_) {
	method2318(is, i_4_);
	anInt8955 = i;
    }
    
    void method2322() {
	aClass185_Sub3_1587.method15220(this);
    }
    
    public void method98(int i, byte[] is, int i_5_) {
	method2318(is, i_5_);
	anInt8955 = i;
    }
    
    public void method92(int i, byte[] is, int i_6_) {
	method2318(is, i_6_);
	anInt8955 = i;
    }
    
    public void method100(int i, byte[] is, int i_7_) {
	method2318(is, i_7_);
	anInt8955 = i;
    }
    
    public long method96() {
	return 0L;
    }
}
