/* Class136_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaggl.OpenGL;

public class Class136_Sub5 extends Class136
{
    static final char aChar8951 = '\001';
    static final char aChar8952 = '\0';
    Class125 aClass125_8953;
    boolean aBool8954 = false;
    
    void method2329(boolean bool) {
	aClass185_Sub3_1600.method15232(8448, 7681);
    }
    
    boolean method2347() {
	return true;
    }
    
    void method2332(boolean bool) {
	aClass185_Sub3_1600.method15232(8448, 7681);
    }
    
    void method2348(Class141 class141, int i) {
	aClass185_Sub3_1600.method15231(class141);
	aClass185_Sub3_1600.method15214(i);
    }
    
    void method2327() {
	if (aBool8954) {
	    aClass125_8953.method2184('\001');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15230(0);
	} else
	    aClass185_Sub3_1600.method15239(0, 5890, 770);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aBool8954 = false;
    }
    
    void method2331(int i, int i_0_) {
	/* empty */
    }
    
    void method2341() {
	if (aBool8954) {
	    aClass125_8953.method2184('\001');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15230(0);
	} else
	    aClass185_Sub3_1600.method15239(0, 5890, 770);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aBool8954 = false;
    }
    
    void method2345() {
	if (aBool8954) {
	    aClass125_8953.method2184('\001');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15230(0);
	} else
	    aClass185_Sub3_1600.method15239(0, 5890, 770);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aBool8954 = false;
    }
    
    boolean method2333() {
	return true;
    }
    
    boolean method2334() {
	return true;
    }
    
    void method2335(boolean bool) {
	Class141_Sub1 class141_sub1 = aClass185_Sub3_1600.method15314();
	if (aClass125_8953 != null && class141_sub1 != null && bool) {
	    aClass125_8953.method2184('\0');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(class141_sub1);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadMatrixf((aClass185_Sub3_1600.aClass433_9604.method6869
				  (aClass185_Sub3_1600.aFloatArray9611)),
				 0);
	    OpenGL.glScalef(1.0F, -1.0F, -1.0F);
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aBool8954 = true;
	} else
	    aClass185_Sub3_1600.method15239(0, 34168, 770);
    }
    
    void method2344(boolean bool) {
	Class141_Sub1 class141_sub1 = aClass185_Sub3_1600.method15314();
	if (aClass125_8953 != null && class141_sub1 != null && bool) {
	    aClass125_8953.method2184('\0');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(class141_sub1);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadMatrixf((aClass185_Sub3_1600.aClass433_9604.method6869
				  (aClass185_Sub3_1600.aFloatArray9611)),
				 0);
	    OpenGL.glScalef(1.0F, -1.0F, -1.0F);
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aBool8954 = true;
	} else
	    aClass185_Sub3_1600.method15239(0, 34168, 770);
    }
    
    void method2337(boolean bool) {
	Class141_Sub1 class141_sub1 = aClass185_Sub3_1600.method15314();
	if (aClass125_8953 != null && class141_sub1 != null && bool) {
	    aClass125_8953.method2184('\0');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(class141_sub1);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadMatrixf((aClass185_Sub3_1600.aClass433_9604.method6869
				  (aClass185_Sub3_1600.aFloatArray9611)),
				 0);
	    OpenGL.glScalef(1.0F, -1.0F, -1.0F);
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aBool8954 = true;
	} else
	    aClass185_Sub3_1600.method15239(0, 34168, 770);
    }
    
    void method2338(boolean bool) {
	Class141_Sub1 class141_sub1 = aClass185_Sub3_1600.method15314();
	if (aClass125_8953 != null && class141_sub1 != null && bool) {
	    aClass125_8953.method2184('\0');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(class141_sub1);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadMatrixf((aClass185_Sub3_1600.aClass433_9604.method6869
				  (aClass185_Sub3_1600.aFloatArray9611)),
				 0);
	    OpenGL.glScalef(1.0F, -1.0F, -1.0F);
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aBool8954 = true;
	} else
	    aClass185_Sub3_1600.method15239(0, 34168, 770);
    }
    
    void method2339(boolean bool) {
	aClass185_Sub3_1600.method15232(8448, 7681);
    }
    
    Class136_Sub5(Class185_Sub3 class185_sub3) {
	super(class185_sub3);
	if (class185_sub3.aBool9694) {
	    aClass125_8953 = new Class125(class185_sub3, 2);
	    aClass125_8953.method2185(0);
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15232(34165, 7681);
	    aClass185_Sub3_1600.method15325(2, 34168, 770);
	    aClass185_Sub3_1600.method15239(0, 34167, 770);
	    OpenGL.glTexGeni(8192, 9472, 34066);
	    OpenGL.glTexGeni(8193, 9472, 34066);
	    OpenGL.glTexGeni(8194, 9472, 34066);
	    OpenGL.glEnable(3168);
	    OpenGL.glEnable(3169);
	    OpenGL.glEnable(3170);
	    aClass185_Sub3_1600.method15230(0);
	    aClass125_8953.method2186();
	    aClass125_8953.method2185(1);
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15232(8448, 8448);
	    aClass185_Sub3_1600.method15325(2, 34166, 770);
	    aClass185_Sub3_1600.method15239(0, 5890, 770);
	    OpenGL.glDisable(3168);
	    OpenGL.glDisable(3169);
	    OpenGL.glDisable(3170);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadIdentity();
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aClass125_8953.method2186();
	}
    }
    
    void method2340(boolean bool) {
	aClass185_Sub3_1600.method15232(8448, 7681);
    }
    
    boolean method2342() {
	return true;
    }
    
    void method2336(boolean bool) {
	aClass185_Sub3_1600.method15232(8448, 7681);
    }
    
    void method2326() {
	if (aBool8954) {
	    aClass125_8953.method2184('\001');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15230(0);
	} else
	    aClass185_Sub3_1600.method15239(0, 5890, 770);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aBool8954 = false;
    }
    
    void method2328(boolean bool) {
	Class141_Sub1 class141_sub1 = aClass185_Sub3_1600.method15314();
	if (aClass125_8953 != null && class141_sub1 != null && bool) {
	    aClass125_8953.method2184('\0');
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(class141_sub1);
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadMatrixf((aClass185_Sub3_1600.aClass433_9604.method6869
				  (aClass185_Sub3_1600.aFloatArray9611)),
				 0);
	    OpenGL.glScalef(1.0F, -1.0F, -1.0F);
	    OpenGL.glMatrixMode(5888);
	    aClass185_Sub3_1600.method15230(0);
	    aBool8954 = true;
	} else
	    aClass185_Sub3_1600.method15239(0, 34168, 770);
    }
    
    void method2346(int i, int i_1_) {
	/* empty */
    }
    
    void method2330(int i, int i_2_) {
	/* empty */
    }
    
    void method2343(Class141 class141, int i) {
	aClass185_Sub3_1600.method15231(class141);
	aClass185_Sub3_1600.method15214(i);
    }
}
