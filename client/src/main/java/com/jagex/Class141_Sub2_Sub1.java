/* Class141_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class141_Sub2_Sub1 extends Class141_Sub2
{
    float aFloat11378;
    int anInt11379;
    float aFloat11380;
    int anInt11381;
    boolean aBool11382;
    boolean aBool11383;
    
    static Class141_Sub2_Sub1 method17894(Class185_Sub3 class185_sub3, int i,
					  int i_0_, int i_1_, int i_2_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i_1_, 670859253)
		&& Class562.method9467(i_2_, 265601867)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_0_, i_1_,
					  i_2_, true);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_0_, i_1_,
					  i_2_, true);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_0_, i_1_, i_2_,
				      Class162.method2640(i_1_, (byte) 42),
				      Class162.method2640(i_2_, (byte) 116),
				      true);
    }
    
    static Class141_Sub2_Sub1 method17895(Class185_Sub3 class185_sub3, int i,
					  int i_3_, boolean bool, int[] is,
					  int i_4_, int i_5_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 1787081602)
		&& Class562.method9467(i_3_, -240783247)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_3_, bool,
					  is, i_4_, i_5_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_3_, bool,
					  is, i_4_, i_5_);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_3_,
				      Class162.method2640(i, (byte) 98),
				      Class162.method2640(i_3_, (byte) 120),
				      is);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, int i_6_, int i_7_,
		       boolean bool, int[] is, int i_8_, int i_9_) {
	super(class185_sub3, i, i_6_, i_7_, bool, is, i_8_, i_9_, true);
	anInt11379 = i_6_;
	anInt11381 = i_7_;
	if (anInt1628 == 34037) {
	    aFloat11380 = (float) i_7_;
	    aFloat11378 = (float) i_6_;
	    aBool11382 = false;
	} else {
	    aFloat11380 = 1.0F;
	    aFloat11378 = 1.0F;
	    aBool11382 = true;
	}
	aBool11383 = false;
    }
    
    static Class141_Sub2_Sub1 method17896(Class185_Sub3 class185_sub3, int i,
					  int i_10_, int i_11_, int i_12_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i_11_, 1095510251)
		&& Class562.method9467(i_12_, 578637564)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_10_, i_11_,
					  i_12_, true);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_10_,
					  i_11_, i_12_, true);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_10_, i_11_, i_12_,
				      Class162.method2640(i_11_, (byte) 21),
				      Class162.method2640(i_12_, (byte) 54),
				      true);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, Class181 class181,
		       Class173 class173, int i, int i_13_, int i_14_,
		       int i_15_) {
	super(class185_sub3, 3553, class181, class173, i_14_, i_15_);
	anInt11379 = i;
	anInt11381 = i_13_;
	aFloat11380 = (float) i_13_ / (float) i_15_;
	aFloat11378 = (float) i / (float) i_14_;
	aBool11382 = false;
	aBool11383 = true;
	method14454(false, false);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, int i_16_,
		       int i_17_, int i_18_, int i_19_, boolean bool) {
	super(class185_sub3, i, i_16_, i_17_, i_18_, i_19_);
	anInt11379 = i_18_;
	anInt11381 = i_19_;
	if (anInt1628 == 34037) {
	    aFloat11380 = (float) i_19_;
	    aFloat11378 = (float) i_18_;
	    aBool11382 = false;
	} else {
	    aFloat11380 = 1.0F;
	    aFloat11378 = 1.0F;
	    aBool11382 = true;
	}
	aBool11383 = false;
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, Class181 class181,
		       Class173 class173, int i_20_, int i_21_, boolean bool,
		       byte[] is, Class181 class181_22_) {
	super(class185_sub3, i, class181, class173, i_20_, i_21_, bool, is,
	      class181_22_, true);
	anInt11379 = i_20_;
	anInt11381 = i_21_;
	if (anInt1628 == 34037) {
	    aFloat11380 = (float) i_21_;
	    aFloat11378 = (float) i_20_;
	    aBool11382 = false;
	} else {
	    aFloat11380 = 1.0F;
	    aFloat11378 = 1.0F;
	    aBool11382 = true;
	}
	aBool11383 = false;
    }
    
    static Class141_Sub2_Sub1 method17897(Class185_Sub3 class185_sub3, int i,
					  int i_23_, boolean bool, int[] is,
					  int i_24_, int i_25_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 1739442663)
		&& Class562.method9467(i_23_, 1898230043)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_23_, bool,
					  is, i_24_, i_25_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_23_, bool,
					  is, i_24_, i_25_);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_23_,
				      Class162.method2640(i, (byte) 20),
				      Class162.method2640(i_23_, (byte) 127),
				      is);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, int i_26_,
		       int i_27_, int i_28_, int[] is) {
	super(class185_sub3, 3553, Class181.aClass181_1952,
	      Class173.aClass173_1830, i_27_, i_28_);
	anInt11379 = i;
	anInt11381 = i_26_;
	method14468(0, i_28_ - i_26_, i, i_26_, is, 0, 0, true);
	aFloat11380 = (float) i_26_ / (float) i_28_;
	aFloat11378 = (float) i / (float) i_27_;
	aBool11382 = false;
	aBool11383 = true;
	method14454(false, false);
    }
    
    void method2382(boolean bool) {
	super.method2382(bool && !aBool11383);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, Class181 class181,
		       Class173 class173, int i, int i_29_, int i_30_,
		       int i_31_, byte[] is, Class181 class181_32_) {
	super(class185_sub3, 3553, class181, class173, i_30_, i_31_);
	anInt11379 = i;
	anInt11381 = i_29_;
	method14450(0, i_31_ - i_29_, i, i_29_, is, class181_32_, 0, 0, true);
	aFloat11380 = (float) i_29_ / (float) i_31_;
	aFloat11378 = (float) i / (float) i_30_;
	aBool11382 = false;
	aBool11383 = true;
	method14454(false, false);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, int i_33_,
		       int i_34_, int i_35_, int i_36_, int i_37_,
		       boolean bool) {
	super(class185_sub3, 3553, i, i_33_, i_36_, i_37_);
	anInt11379 = i_34_;
	anInt11381 = i_35_;
	aFloat11380 = (float) i_35_ / (float) i_37_;
	aFloat11378 = (float) i_34_ / (float) i_36_;
	aBool11382 = false;
	aBool11383 = true;
	method14454(false, false);
    }
    
    static Class141_Sub2_Sub1 method17898(Class185_Sub3 class185_sub3,
					  Class181 class181, Class173 class173,
					  int i, int i_38_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 226241905)
		&& Class562.method9467(i_38_, 969829266)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_38_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_38_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_38_, Class162.method2640(i, (byte) 74),
				      Class162.method2640(i_38_, (byte) 95));
    }
    
    static Class141_Sub2_Sub1 method17899(Class185_Sub3 class185_sub3,
					  Class181 class181, Class173 class173,
					  int i, int i_39_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 102185514)
		&& Class562.method9467(i_39_, 926593742)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_39_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_39_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_39_, Class162.method2640(i, (byte) 40),
				      Class162.method2640(i_39_, (byte) 127));
    }
    
    static Class141_Sub2_Sub1 method17900(Class185_Sub3 class185_sub3,
					  Class181 class181, Class173 class173,
					  int i, int i_40_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 1027951151)
		&& Class562.method9467(i_40_, -1053824829)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_40_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_40_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_40_, Class162.method2640(i, (byte) 55),
				      Class162.method2640(i_40_, (byte) 116));
    }
    
    static Class141_Sub2_Sub1 method17901(Class185_Sub3 class185_sub3,
					  Class181 class181, Class173 class173,
					  int i, int i_41_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, -924381005)
		&& Class562.method9467(i_41_, 909152218)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_41_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_41_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_41_, Class162.method2640(i, (byte) 66),
				      Class162.method2640(i_41_, (byte) 74));
    }
    
    static Class141_Sub2_Sub1 method17902
	(Class185_Sub3 class185_sub3, Class181 class181, Class173 class173,
	 int i, int i_42_, boolean bool, byte[] is, Class181 class181_43_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, -1241686626)
		&& Class562.method9467(i_42_, 1525206726)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_42_, bool, is,
					  class181_43_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_42_, bool, is,
					  class181_43_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_42_, Class162.method2640(i, (byte) 74),
				      Class162.method2640(i_42_, (byte) 64),
				      is, class181_43_);
    }
    
    static Class141_Sub2_Sub1 method17903(Class185_Sub3 class185_sub3, int i,
					  int i_44_, boolean bool, int[] is,
					  int i_45_, int i_46_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, -812719265)
		&& Class562.method9467(i_44_, -1399199096)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_44_, bool,
					  is, i_45_, i_46_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_44_, bool,
					  is, i_45_, i_46_);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_44_,
				      Class162.method2640(i, (byte) 64),
				      Class162.method2640(i_44_, (byte) 26),
				      is);
    }
    
    static Class141_Sub2_Sub1 method17904(Class185_Sub3 class185_sub3, int i,
					  int i_47_, boolean bool, int[] is,
					  int i_48_, int i_49_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 410866079)
		&& Class562.method9467(i_47_, 765049359)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_47_, bool,
					  is, i_48_, i_49_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_47_, bool,
					  is, i_48_, i_49_);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_47_,
				      Class162.method2640(i, (byte) 66),
				      Class162.method2640(i_47_, (byte) 70),
				      is);
    }
    
    static Class141_Sub2_Sub1 method17905(Class185_Sub3 class185_sub3, int i,
					  int i_50_, boolean bool, int[] is,
					  int i_51_, int i_52_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, -323600548)
		&& Class562.method9467(i_50_, 1932932920)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_50_, bool,
					  is, i_51_, i_52_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_50_, bool,
					  is, i_51_, i_52_);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_50_,
				      Class162.method2640(i, (byte) 38),
				      Class162.method2640(i_50_, (byte) 41),
				      is);
    }
    
    static Class141_Sub2_Sub1 method17906
	(Class185_Sub3 class185_sub3, Class181 class181, Class173 class173,
	 int i, int i_53_, boolean bool, byte[] is, Class181 class181_54_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 63536194)
		&& Class562.method9467(i_53_, 782324233)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_53_, bool, is,
					  class181_54_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_53_, bool, is,
					  class181_54_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_53_, Class162.method2640(i, (byte) 61),
				      Class162.method2640(i_53_, (byte) 106),
				      is, class181_54_);
    }
    
    static Class141_Sub2_Sub1 method17907
	(Class185_Sub3 class185_sub3, Class181 class181, Class173 class173,
	 int i, int i_55_, boolean bool, byte[] is, Class181 class181_56_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i, 1318243373)
		&& Class562.method9467(i_55_, -838610777)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, class181,
					  class173, i, i_55_, bool, is,
					  class181_56_);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, class181,
					  class173, i, i_55_, bool, is,
					  class181_56_);
	return new Class141_Sub2_Sub1(class185_sub3, class181, class173, i,
				      i_55_, Class162.method2640(i, (byte) 86),
				      Class162.method2640(i_55_, (byte) 126),
				      is, class181_56_);
    }
    
    Class141_Sub2_Sub1(Class185_Sub3 class185_sub3, int i, Class181 class181,
		       Class173 class173, int i_57_, int i_58_) {
	super(class185_sub3, i, class181, class173, i_57_, i_58_);
	anInt11379 = i_57_;
	anInt11381 = i_58_;
	if (anInt1628 == 34037) {
	    aFloat11380 = (float) i_58_;
	    aFloat11378 = (float) i_57_;
	    aBool11382 = false;
	} else {
	    aFloat11380 = 1.0F;
	    aFloat11378 = 1.0F;
	    aBool11382 = true;
	}
	aBool11383 = false;
    }
    
    static Class141_Sub2_Sub1 method17908(Class185_Sub3 class185_sub3, int i,
					  int i_59_, int i_60_, int i_61_) {
	if (class185_sub3.aBool9698
	    || (Class562.method9467(i_60_, 1038144683)
		&& Class562.method9467(i_61_, -657885293)))
	    return new Class141_Sub2_Sub1(class185_sub3, 3553, i, i_59_, i_60_,
					  i_61_, true);
	if (class185_sub3.aBool9624)
	    return new Class141_Sub2_Sub1(class185_sub3, 34037, i, i_59_,
					  i_60_, i_61_, true);
	return new Class141_Sub2_Sub1(class185_sub3, i, i_59_, i_60_, i_61_,
				      Class162.method2640(i_60_, (byte) 28),
				      Class162.method2640(i_61_, (byte) 125),
				      true);
    }
    
    void method2385(boolean bool) {
	super.method2382(bool && !aBool11383);
    }
}
