/* Class155 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class155 implements Interface19
{
    public Interface4 anInterface4_1742;
    static int anInt1743;
    
    Class155(Class110_Sub1 class110_sub1, Class8 class8) {
	this(class110_sub1.aClass453_8978, class8);
    }
    
    public long method132(Class150 class150) {
	return anInterface4_1742.method26(class150.anInt1694 * -1270946121,
					  673320982);
    }
    
    public Class155(Class110_Sub1 class110_sub1) {
	this(class110_sub1, new Class8(class110_sub1));
    }
    
    public Class155(Class453 class453, Class8 class8) {
	anInterface4_1742 = class8;
    }
    
    public void method114(Class150 class150, int i, int i_0_) {
	anInterface4_1742.method28(-1270946121 * class150.anInt1694, i,
				   (byte) -34);
    }
    
    public long method127(Class150 class150, byte i) {
	return anInterface4_1742.method26(class150.anInt1694 * -1270946121,
					  673320982);
    }
    
    public void method116(Class150 class150, long l) {
	anInterface4_1742.method27(-1270946121 * class150.anInt1694, l);
    }
    
    public int method126(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -60),
				   1302178545);
    }
    
    public void method113(Class150 class150, long l) {
	anInterface4_1742.method27(-1270946121 * class150.anInt1694, l);
    }
    
    public int method119(Class318 class318, int i) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -122),
				   1928626410);
    }
    
    public void method122(Class318 class318, int i, byte i_1_)
	throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -3),
				      i, (byte) -7),
		  -523939547);
    }
    
    public int method117(Class150 class150) {
	return anInterface4_1742.method32(class150.anInt1694 * -1270946121,
					  -226408093);
    }
    
    public int method136(Class150 class150) {
	return anInterface4_1742.method32(class150.anInt1694 * -1270946121,
					  268555357);
    }
    
    public int method123(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -89),
				   2088921469);
    }
    
    public int method135(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -30),
				   1828806190);
    }
    
    public void method128(Class150 class150, int i) {
	anInterface4_1742.method28(-1270946121 * class150.anInt1694, i,
				   (byte) -118);
    }
    
    public void method115(Class150 class150, long l) {
	anInterface4_1742.method27(-1270946121 * class150.anInt1694, l);
    }
    
    public void method121(Class150 class150, int i) {
	anInterface4_1742.method28(-1270946121 * class150.anInt1694, i,
				   (byte) -63);
    }
    
    public Object method134(Class150 class150) {
	return anInterface4_1742.method45(-1270946121 * class150.anInt1694,
					  -2117491698);
    }
    
    public long method129(Class150 class150) {
	return anInterface4_1742.method26(class150.anInt1694 * -1270946121,
					  673320982);
    }
    
    public long method133(Class150 class150) {
	return anInterface4_1742.method26(class150.anInt1694 * -1270946121,
					  673320982);
    }
    
    public long method131(Class150 class150) {
	return anInterface4_1742.method26(class150.anInt1694 * -1270946121,
					  673320982);
    }
    
    public Object method124(Class150 class150, int i) {
	return anInterface4_1742.method45(-1270946121 * class150.anInt1694,
					  -1945492901);
    }
    
    public void method140(Class318 class318, int i) throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -92),
				      i, (byte) -64),
		  -2129938998);
    }
    
    public int method125(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -105),
				   1491672488);
    }
    
    public void method130(Class150 class150, long l) {
	anInterface4_1742.method27(-1270946121 * class150.anInt1694, l);
    }
    
    public void method118(Class150 class150, Object object, byte i) {
	anInterface4_1742.method29(-1270946121 * class150.anInt1694, object,
				   (short) -22092);
    }
    
    public Object method137(Class150 class150) {
	return anInterface4_1742.method45(-1270946121 * class150.anInt1694,
					  -1995605745);
    }
    
    public void method138(Class150 class150, Object object) {
	anInterface4_1742.method29(-1270946121 * class150.anInt1694, object,
				   (short) -32058);
    }
    
    public void method139(Class318 class318, int i) throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -9),
				      i, (byte) 43),
		  -348766102);
    }
    
    public int method120(Class150 class150, byte i) {
	return anInterface4_1742.method32(class150.anInt1694 * -1270946121,
					  1668322154);
    }
    
    static final void method2578(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	class669.anInt8600 -= -1204971666;
	class247.aBool2522 = true;
	class247.anInt2529
	    = Math.max(Math.min((class669.anIntArray8595
				 [2088438307 * class669.anInt8600]),
				2816),
		       0) * 827777951;
	class247.anInt2526
	    = Math.max(Math.min((class669.anIntArray8595
				 [class669.anInt8600 * 2088438307 + 1]),
				2816),
		       0) * -1717407349;
	class247.anInt2527
	    = Math.max(Math.min((class669.anIntArray8595
				 [class669.anInt8600 * 2088438307 + 2]),
				2816),
		       0) * 1995007491;
	int i_2_ = Math.max(Math.min((class669.anIntArray8595
				      [3 + 2088438307 * class669.anInt8600]),
				     255),
			    0);
	int i_3_ = Math.max(Math.min((class669.anIntArray8595
				      [4 + 2088438307 * class669.anInt8600]),
				     255),
			    0);
	int i_4_ = Math.max(Math.min((class669.anIntArray8595
				      [5 + 2088438307 * class669.anInt8600]),
				     255),
			    0);
	class247.anInt2511 = (i_2_ << 16 | i_3_ << 8 | i_4_) * -1401656177;
	class247.anInt2618
	    = (-1918420773
	       * class669.anIntArray8595[6 + 2088438307 * class669.anInt8600]);
	class247.anInt2559
	    = (-671112543
	       * class669.anIntArray8595[7 + class669.anInt8600 * 2088438307]);
	class247.anInt2608
	    = (class669.anIntArray8595[8 + class669.anInt8600 * 2088438307]
	       * -1970864823);
	class247.anInt2431
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600 + 9]
	       * 1621962113);
	Class454.method7416(class247, -1011821146);
    }
    
    static final int method2579
	(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1, int i) {
	if (class654_sub1_sub5_sub1.anInt11971 * -650254265 == 0)
	    return 0;
	if (-1 != 1409535459 * class654_sub1_sub5_sub1.anInt11944) {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1_5_ = null;
	    if (1409535459 * class654_sub1_sub5_sub1.anInt11944 < 32768) {
		Class534_Sub6 class534_sub6
		    = ((Class534_Sub6)
		       (client.aClass9_11331.method579
			((long) (1409535459
				 * class654_sub1_sub5_sub1.anInt11944))));
		if (class534_sub6 != null)
		    class654_sub1_sub5_sub1_5_ = ((Class654_Sub1_Sub5_Sub1)
						  class534_sub6.anObject10417);
	    } else if (class654_sub1_sub5_sub1.anInt11944 * 1409535459
		       >= 32768)
		class654_sub1_sub5_sub1_5_
		    = (client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279
		       [(class654_sub1_sub5_sub1.anInt11944 * 1409535459
			 - 32768)]);
	    if (null != class654_sub1_sub5_sub1_5_) {
		Class438 class438
		    = Class438.method7055((class654_sub1_sub5_sub1.method10807
					   ().aClass438_4885),
					  class654_sub1_sub5_sub1_5_
					      .method10807().aClass438_4885);
		int i_6_ = (int) class438.aFloat4864;
		int i_7_ = (int) class438.aFloat4865;
		if (0 != i_6_ || 0 != i_7_)
		    class654_sub1_sub5_sub1.method18524
			((int) (Math.atan2((double) i_6_, (double) i_7_)
				* 2607.5945876176133) & 0x3fff,
			 1294800092);
	    }
	}
	if (class654_sub1_sub5_sub1 instanceof Class654_Sub1_Sub5_Sub1_Sub2) {
	    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		= (Class654_Sub1_Sub5_Sub1_Sub2) class654_sub1_sub5_sub1;
	    if (class654_sub1_sub5_sub1_sub2.anInt12223 * -2025203045 != -1
		&& (0 == class654_sub1_sub5_sub1_sub2.anInt11980 * -1763707177
		    || (class654_sub1_sub5_sub1_sub2.anInt11947 * 1235744983
			> 0))) {
		class654_sub1_sub5_sub1_sub2.method18524
		    (class654_sub1_sub5_sub1_sub2.anInt12223 * -2025203045,
		     -436815365);
		class654_sub1_sub5_sub1_sub2.anInt12223 = 1021477997;
	    }
	} else if (class654_sub1_sub5_sub1
		   instanceof Class654_Sub1_Sub5_Sub1_Sub1) {
	    Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
		= (Class654_Sub1_Sub5_Sub1_Sub1) class654_sub1_sub5_sub1;
	    if (-1 != class654_sub1_sub5_sub1_sub1.anInt12193 * -1294025797
		&& (class654_sub1_sub5_sub1_sub1.anInt11980 * -1763707177 == 0
		    || (class654_sub1_sub5_sub1_sub1.anInt11947 * 1235744983
			> 0))) {
		Class438 class438 = (class654_sub1_sub5_sub1_sub1.method10807()
				     .aClass438_4885);
		Class597 class597
		    = client.aClass512_11100.method8416((byte) 37);
		int i_8_
		    = ((int) class438.aFloat4864
		       - (-558122240 * class654_sub1_sub5_sub1_sub1.anInt12193
			  - -1221009664 * class597.anInt7859
			  - -1221009664 * class597.anInt7859));
		int i_9_
		    = ((int) class438.aFloat4865
		       - (-1915040000 * class654_sub1_sub5_sub1_sub1.anInt12194
			  - 2077618944 * class597.anInt7861
			  - 2077618944 * class597.anInt7861));
		if (0 != i_8_ || 0 != i_9_)
		    class654_sub1_sub5_sub1_sub1.method18524
			((int) (Math.atan2((double) i_8_, (double) i_9_)
				* 2607.5945876176133) & 0x3fff,
			 -233411351);
		class654_sub1_sub5_sub1_sub1.anInt12193 = 433401485;
	    }
	}
	return class654_sub1_sub5_sub1.method18525(-464369596);
    }
    
    static final void method2580(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (string != null)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= string.length();
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
    }
}
