/* Class164 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaggl.OpenGL;

public class Class164 implements Interface17
{
    int anInt1759;
    Class141_Sub2 aClass141_Sub2_1760;
    
    public int method145() {
	return aClass141_Sub2_1760.anInt8919;
    }
    
    public void method65(int i) {
	OpenGL.glFramebufferTexture2DEXT(36160, i,
					 aClass141_Sub2_1760.anInt1628,
					 aClass141_Sub2_1760.anInt1633,
					 anInt1759);
    }
    
    public int method93() {
	return aClass141_Sub2_1760.anInt8919;
    }
    
    public void method142() {
	/* empty */
    }
    
    public void method144() {
	/* empty */
    }
    
    public int method1() {
	return aClass141_Sub2_1760.anInt8918;
    }
    
    public int method53() {
	return aClass141_Sub2_1760.anInt8918;
    }
    
    public void method143() {
	/* empty */
    }
    
    public int method88() {
	return aClass141_Sub2_1760.anInt8919;
    }
    
    public int method8() {
	return aClass141_Sub2_1760.anInt8919;
    }
    
    public int method9() {
	return aClass141_Sub2_1760.anInt8919;
    }
    
    public int method22() {
	return aClass141_Sub2_1760.anInt8918;
    }
    
    Class164(Class141_Sub2 class141_sub2, int i) {
	anInt1759 = i;
	aClass141_Sub2_1760 = class141_sub2;
    }
    
    public void method141() {
	/* empty */
    }
    
    public void method106(int i) {
	OpenGL.glFramebufferTexture2DEXT(36160, i,
					 aClass141_Sub2_1760.anInt1628,
					 aClass141_Sub2_1760.anInt1633,
					 anInt1759);
    }
    
    public int method85() {
	return aClass141_Sub2_1760.anInt8919;
    }
}
