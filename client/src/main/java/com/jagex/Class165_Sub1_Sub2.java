/* Class165_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class165_Sub1_Sub2 extends Class165_Sub1
{
    static int[][] anIntArrayArray11432 = new int[6][];
    Class141_Sub1 aClass141_Sub1_11433;
    int anInt11434;
    Class185_Sub3 aClass185_Sub3_11435;
    
    Class141_Sub1 method15071() {
	if (aClass141_Sub1_11433 == null) {
	    Interface25 interface25 = aClass185_Sub3_11435.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub3_11435.aClass177_2012.method2931(anInt11434,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11434,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11434,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-620520922);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_0_ = 0; i_0_ < 6; i_0_++) {
		anIntArrayArray11432[i_0_] = new int[i];
		System.arraycopy(is, i * i_0_, anIntArrayArray11432[i_0_], 0,
				 i);
	    }
	    aClass141_Sub1_11433
		= new Class141_Sub1(aClass185_Sub3_11435,
				    Class181.aClass181_1959,
				    Class173.aClass173_1830,
				    class186.anInt2046 * 1264459495,
				    class186.aByte2070 != 0,
				    anIntArrayArray11432);
	}
	return aClass141_Sub1_11433;
    }
    
    Class141_Sub1 method15068() {
	if (aClass141_Sub1_11433 == null) {
	    Interface25 interface25 = aClass185_Sub3_11435.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub3_11435.aClass177_2012.method2931(anInt11434,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11434,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11434,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-1325734078);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_1_ = 0; i_1_ < 6; i_1_++) {
		anIntArrayArray11432[i_1_] = new int[i];
		System.arraycopy(is, i * i_1_, anIntArrayArray11432[i_1_], 0,
				 i);
	    }
	    aClass141_Sub1_11433
		= new Class141_Sub1(aClass185_Sub3_11435,
				    Class181.aClass181_1959,
				    Class173.aClass173_1830,
				    class186.anInt2046 * 1264459495,
				    class186.aByte2070 != 0,
				    anIntArrayArray11432);
	}
	return aClass141_Sub1_11433;
    }
    
    Class141_Sub1 method15069() {
	if (aClass141_Sub1_11433 == null) {
	    Interface25 interface25 = aClass185_Sub3_11435.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub3_11435.aClass177_2012.method2931(anInt11434,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11434,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11434,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					1583660862);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_2_ = 0; i_2_ < 6; i_2_++) {
		anIntArrayArray11432[i_2_] = new int[i];
		System.arraycopy(is, i * i_2_, anIntArrayArray11432[i_2_], 0,
				 i);
	    }
	    aClass141_Sub1_11433
		= new Class141_Sub1(aClass185_Sub3_11435,
				    Class181.aClass181_1959,
				    Class173.aClass173_1830,
				    class186.anInt2046 * 1264459495,
				    class186.aByte2070 != 0,
				    anIntArrayArray11432);
	}
	return aClass141_Sub1_11433;
    }
    
    Class141_Sub1 method15067() {
	if (aClass141_Sub1_11433 == null) {
	    Interface25 interface25 = aClass185_Sub3_11435.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub3_11435.aClass177_2012.method2931(anInt11434,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11434,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11434,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-987376793);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_3_ = 0; i_3_ < 6; i_3_++) {
		anIntArrayArray11432[i_3_] = new int[i];
		System.arraycopy(is, i * i_3_, anIntArrayArray11432[i_3_], 0,
				 i);
	    }
	    aClass141_Sub1_11433
		= new Class141_Sub1(aClass185_Sub3_11435,
				    Class181.aClass181_1959,
				    Class173.aClass173_1830,
				    class186.anInt2046 * 1264459495,
				    class186.aByte2070 != 0,
				    anIntArrayArray11432);
	}
	return aClass141_Sub1_11433;
    }
    
    Class165_Sub1_Sub2(Class185_Sub3 class185_sub3, int i) {
	aClass185_Sub3_11435 = class185_sub3;
	anInt11434 = i;
    }
    
    Class141_Sub1 method15070() {
	if (aClass141_Sub1_11433 == null) {
	    Interface25 interface25 = aClass185_Sub3_11435.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub3_11435.aClass177_2012.method2931(anInt11434,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11434,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11434,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-1004036560);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_4_ = 0; i_4_ < 6; i_4_++) {
		anIntArrayArray11432[i_4_] = new int[i];
		System.arraycopy(is, i * i_4_, anIntArrayArray11432[i_4_], 0,
				 i);
	    }
	    aClass141_Sub1_11433
		= new Class141_Sub1(aClass185_Sub3_11435,
				    Class181.aClass181_1959,
				    Class173.aClass173_1830,
				    class186.anInt2046 * 1264459495,
				    class186.aByte2070 != 0,
				    anIntArrayArray11432);
	}
	return aClass141_Sub1_11433;
    }
}
