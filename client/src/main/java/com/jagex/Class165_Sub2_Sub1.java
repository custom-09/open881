/* Class165_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class165_Sub2_Sub1 extends Class165_Sub2
{
    Interface41 anInterface41_11549;
    int anInt11550;
    Class185_Sub1 aClass185_Sub1_11551;
    static int[][] anIntArrayArray11552 = new int[6][];
    
    Interface41 method15564() {
	if (anInterface41_11549 == null) {
	    Interface25 interface25 = aClass185_Sub1_11551.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub1_11551.aClass177_2012.method2931(anInt11550,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11550,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11550,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-1501134248);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_0_ = 0; i_0_ < 6; i_0_++) {
		anIntArrayArray11552[i_0_] = new int[i];
		System.arraycopy(is, i * i_0_, anIntArrayArray11552[i_0_], 0,
				 i);
	    }
	    anInterface41_11549
		= aClass185_Sub1_11551.method14665((class186.anInt2046
						    * 1264459495),
						   class186.aByte2070 != 0,
						   anIntArrayArray11552);
	}
	return anInterface41_11549;
    }
    
    Interface41 method15565() {
	if (anInterface41_11549 == null) {
	    Interface25 interface25 = aClass185_Sub1_11551.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub1_11551.aClass177_2012.method2931(anInt11550,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11550,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11550,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-623342488);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_1_ = 0; i_1_ < 6; i_1_++) {
		anIntArrayArray11552[i_1_] = new int[i];
		System.arraycopy(is, i * i_1_, anIntArrayArray11552[i_1_], 0,
				 i);
	    }
	    anInterface41_11549
		= aClass185_Sub1_11551.method14665((class186.anInt2046
						    * 1264459495),
						   class186.aByte2070 != 0,
						   anIntArrayArray11552);
	}
	return anInterface41_11549;
    }
    
    Interface41 method15562() {
	if (anInterface41_11549 == null) {
	    Interface25 interface25 = aClass185_Sub1_11551.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub1_11551.aClass177_2012.method2931(anInt11550,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11550,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11550,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					1566304362);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_2_ = 0; i_2_ < 6; i_2_++) {
		anIntArrayArray11552[i_2_] = new int[i];
		System.arraycopy(is, i * i_2_, anIntArrayArray11552[i_2_], 0,
				 i);
	    }
	    anInterface41_11549
		= aClass185_Sub1_11551.method14665((class186.anInt2046
						    * 1264459495),
						   class186.aByte2070 != 0,
						   anIntArrayArray11552);
	}
	return anInterface41_11549;
    }
    
    Interface41 method15561() {
	if (anInterface41_11549 == null) {
	    Interface25 interface25 = aClass185_Sub1_11551.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub1_11551.aClass177_2012.method2931(anInt11550,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11550,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11550,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					1694947729);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_3_ = 0; i_3_ < 6; i_3_++) {
		anIntArrayArray11552[i_3_] = new int[i];
		System.arraycopy(is, i * i_3_, anIntArrayArray11552[i_3_], 0,
				 i);
	    }
	    anInterface41_11549
		= aClass185_Sub1_11551.method14665((class186.anInt2046
						    * 1264459495),
						   class186.aByte2070 != 0,
						   anIntArrayArray11552);
	}
	return anInterface41_11549;
    }
    
    Class165_Sub2_Sub1(Class185_Sub1 class185_sub1, int i) {
	aClass185_Sub1_11551 = class185_sub1;
	anInt11550 = i;
    }
    
    Interface41 method15563() {
	if (anInterface41_11549 == null) {
	    Interface25 interface25 = aClass185_Sub1_11551.anInterface25_1997;
	    Class186 class186
		= aClass185_Sub1_11551.aClass177_2012.method2931(anInt11550,
								 (byte) 1);
	    if (class186 == null)
		return null;
	    if (!class186.aBool2045)
		return null;
	    if (!interface25.method155(Class598.aClass598_7864, anInt11550,
				       Class613.aClass613_8022, 1.0F,
				       class186.anInt2046 * 1264459495,
				       class186.anInt2046 * 1264459495, false,
				       -618218007))
		return null;
	    int[] is
		= interface25.method149(Class598.aClass598_7864, anInt11550,
					1.0F, class186.anInt2046 * 1264459495,
					class186.anInt2046 * 1264459495, false,
					-1246588704);
	    int i = (class186.anInt2046 * 1264459495
		     * (class186.anInt2046 * 1264459495));
	    if (is == null)
		return null;
	    for (int i_4_ = 0; i_4_ < 6; i_4_++) {
		anIntArrayArray11552[i_4_] = new int[i];
		System.arraycopy(is, i * i_4_, anIntArrayArray11552[i_4_], 0,
				 i);
	    }
	    anInterface41_11549
		= aClass185_Sub1_11551.method14665((class186.anInt2046
						    * 1264459495),
						   class186.aByte2070 != 0,
						   anIntArrayArray11552);
	}
	return anInterface41_11549;
    }
}
