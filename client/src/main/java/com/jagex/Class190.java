/* Class190 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class190
{
    int anInt2126;
    static Class190 aClass190_2127;
    public static Class190 aClass190_2128;
    static Class190 aClass190_2129;
    static Class190 aClass190_2130;
    static Class190 aClass190_2131;
    static Class190 aClass190_2132;
    static Class190 aClass190_2133;
    public static Class190 aClass190_2134 = new Class190(1);
    static Class190 aClass190_2135 = new Class190(6);
    static Class190 aClass190_2136;
    public static Class472 aClass472_2137;
    
    public int method3763(int i) {
	return 0x2000000 | anInt2126 * -766815435;
    }
    
    public int method3764() {
	return 0x2000000 | anInt2126 * -766815435;
    }
    
    Class190(int i) {
	anInt2126 = i * 2003871517;
    }
    
    public int method3765() {
	return 0x2000000 | anInt2126 * -766815435;
    }
    
    public int method3766() {
	return 0x2000000 | anInt2126 * -766815435;
    }
    
    static {
	aClass190_2128 = new Class190(5);
	aClass190_2129 = new Class190(2);
	aClass190_2130 = new Class190(7);
	aClass190_2131 = new Class190(9);
	aClass190_2132 = new Class190(8);
	aClass190_2133 = new Class190(4);
	aClass190_2127 = new Class190(0);
	aClass190_2136 = new Class190(3);
    }
    
    static final void method3767(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class277.method5161(class247, class243, class669, -1391066448);
    }
    
    static final void method3768(int i, int i_0_) {
	Class504.method8326(1148362895);
	int i_1_ = (((Class150_Sub2) Class562.aClass110_Sub1_Sub1_7560
					 .method91(i, -1786484280)).anInt8910
		    * 204778967);
	if (0 != i_1_) {
	    int i_2_ = (Class78.aClass103_825.method120
			((Class150) Class562.aClass110_Sub1_Sub1_7560
					.method91(i, 746034359),
			 (byte) -88));
	    if (i_1_ == 5)
		client.anInt11213 = i_2_ * -308071353;
	    if (6 == i_1_)
		client.anInt11225 = i_2_ * -28012801;
	}
    }
}
