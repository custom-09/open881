/* Class191 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

final class Class191 implements Interface20
{
    static Class567 aClass567_2138;
    
    public int method125(Class318 class318) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579(0x100000000L
					       | (long) (class318.anInt3396
							 * 206851703)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method119(class318, -2036182740);
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public int method120(Class150 class150, byte i) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579((long) (class150.anInt1694
						       * -1270946121)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method120(class150, (byte) -13);
	return class534_sub39.anInt10807 * -705967177;
    }
    
    public int method119(Class318 class318, int i) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579(0x100000000L
					       | (long) (class318.anInt3396
							 * 206851703)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method119(class318, -2139432733);
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public int method135(Class318 class318) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579(0x100000000L
					       | (long) (class318.anInt3396
							 * 206851703)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method119(class318, -2069543066);
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public int method136(Class150 class150) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579((long) (class150.anInt1694
						       * -1270946121)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method120(class150, (byte) -86);
	return class534_sub39.anInt10807 * -705967177;
    }
    
    public int method123(Class318 class318) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579(0x100000000L
					       | (long) (class318.anInt3396
							 * 206851703)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method119(class318, -1910230701);
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public int method117(Class150 class150) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579((long) (class150.anInt1694
						       * -1270946121)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method120(class150, (byte) -105);
	return class534_sub39.anInt10807 * -705967177;
    }
    
    public int method126(Class318 class318) {
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       Class201.aClass9_2180.method579(0x100000000L
					       | (long) (class318.anInt3396
							 * 206851703)));
	if (class534_sub39 == null)
	    return Class78.aClass103_825.method119(class318, -1190024794);
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public static Class607[] method3769(int i) {
	return (new Class607[]
		{ Class607.aClass607_7993, Class607.aClass607_7992,
		  Class607.aClass607_7990, Class607.aClass607_7989,
		  Class607.aClass607_7991, Class607.aClass607_7994 });
    }
    
    static final void method3770(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (Class599.aClass298_Sub1_7871.method5425(1983762570) == null ? -1
	       : (Class599.aClass298_Sub1_7871.method5425(1502386721).anInt3126
		  * -2097182691));
    }
}
