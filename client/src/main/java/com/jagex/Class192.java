/* Class192 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class192
{
    Class654_Sub1_Sub5_Sub1_Sub2 aClass654_Sub1_Sub5_Sub1_Sub2_2139;
    int anInt2140;
    public int anInt2141;
    public boolean aBool2142 = false;
    Class654_Sub1_Sub5_Sub1_Sub1 aClass654_Sub1_Sub5_Sub1_Sub1_2143 = null;
    static final int anInt2144 = 25;
    int[] anIntArray2145;
    static Class169_Sub2[] aClass169_Sub2Array2146;
    
    public Class654_Sub1_Sub5_Sub1 method3771() {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    return aClass654_Sub1_Sub5_Sub1_Sub1_2143;
	return aClass654_Sub1_Sub5_Sub1_Sub2_2139;
    }
    
    void method3772(int i, int i_0_, int i_1_, int i_2_, byte i_3_) {
	if (!aBool2142) {
	    aBool2142 = true;
	    if (anInt2141 * -849492911 >= 0) {
		aClass654_Sub1_Sub5_Sub1_Sub1_2143
		    = (new Class654_Sub1_Sub5_Sub1_Sub1
		       (client.aClass512_11100.method8424((byte) 73), 25));
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11967
		    = 1162675721 * client.anInt11101;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18827
		    (((Class307)
		      Class578.aClass44_Sub3_7743
			  .method91(-849492911 * anInt2141, 270441991)),
		     (byte) -2);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18535
		    ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
		      .anInt3328) * -1821838479,
		     -689189513);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11971
		    = ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
			.anInt3312) * -1373680215
		       << 3) * 1883849079;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    } else {
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    = (new Class654_Sub1_Sub5_Sub1_Sub2
		       (client.aClass512_11100.method8424((byte) 95), 25));
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    .method18865(Class712.aClass534_Sub40_8883, (byte) 95);
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11967
		    = client.anInt11101 * 1162675721;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    }
	}
	if (anInt2141 * -849492911 >= 0) {
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i_1_, i, i_0_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 -1063845314);
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18523(i_2_, true,
							   -1578830304);
	} else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i_1_;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i, i_0_,
							   -1860555296);
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18523(i_2_, true,
							   -168945108);
	}
    }
    
    void method3773(int i) {
	aClass654_Sub1_Sub5_Sub1_Sub1_2143 = null;
	aClass654_Sub1_Sub5_Sub1_Sub2_2139 = null;
	aBool2142 = false;
    }
    
    void method3774(int i, int i_4_, int i_5_, byte i_6_) {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i, i_4_, i_5_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 -1503579716);
	else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i_4_, i_5_,
							   -1484295027);
	}
    }
    
    public Class654_Sub1_Sub5_Sub1 method3775(int i) {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    return aClass654_Sub1_Sub5_Sub1_Sub1_2143;
	return aClass654_Sub1_Sub5_Sub1_Sub2_2139;
    }
    
    void method3776() {
	aClass654_Sub1_Sub5_Sub1_Sub1_2143 = null;
	aClass654_Sub1_Sub5_Sub1_Sub2_2139 = null;
	aBool2142 = false;
    }
    
    void method3777(int i, int i_7_, int i_8_, int i_9_) {
	if (!aBool2142) {
	    aBool2142 = true;
	    if (anInt2141 * -849492911 >= 0) {
		aClass654_Sub1_Sub5_Sub1_Sub1_2143
		    = (new Class654_Sub1_Sub5_Sub1_Sub1
		       (client.aClass512_11100.method8424((byte) 59), 25));
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11967
		    = 1162675721 * client.anInt11101;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18827
		    (((Class307)
		      Class578.aClass44_Sub3_7743
			  .method91(-849492911 * anInt2141, 146992912)),
		     (byte) -2);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18535
		    ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
		      .anInt3328) * -1821838479,
		     -57454320);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11971
		    = ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
			.anInt3312) * -1373680215
		       << 3) * 1883849079;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    } else {
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    = (new Class654_Sub1_Sub5_Sub1_Sub2
		       (client.aClass512_11100.method8424((byte) 21), 25));
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    .method18865(Class712.aClass534_Sub40_8883, (byte) 125);
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11967
		    = client.anInt11101 * 1162675721;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    }
	}
	if (anInt2141 * -849492911 >= 0) {
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i_8_, i, i_7_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 -851220085);
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18523(i_9_, true,
							   -1718227473);
	} else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i_8_;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i, i_7_,
							   -2123527065);
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18523(i_9_, true,
							   -888639171);
	}
    }
    
    void method3778(int i, int i_10_, int i_11_, int i_12_) {
	if (!aBool2142) {
	    aBool2142 = true;
	    if (anInt2141 * -849492911 >= 0) {
		aClass654_Sub1_Sub5_Sub1_Sub1_2143
		    = (new Class654_Sub1_Sub5_Sub1_Sub1
		       (client.aClass512_11100.method8424((byte) 91), 25));
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11967
		    = 1162675721 * client.anInt11101;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18827
		    (((Class307)
		      Class578.aClass44_Sub3_7743
			  .method91(-849492911 * anInt2141, -1664286206)),
		     (byte) -2);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18535
		    ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
		      .anInt3328) * -1821838479,
		     294724155);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11971
		    = ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
			.anInt3312) * -1373680215
		       << 3) * 1883849079;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    } else {
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    = (new Class654_Sub1_Sub5_Sub1_Sub2
		       (client.aClass512_11100.method8424((byte) 19), 25));
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    .method18865(Class712.aClass534_Sub40_8883, (byte) 46);
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11967
		    = client.anInt11101 * 1162675721;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    }
	}
	if (anInt2141 * -849492911 >= 0) {
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i_11_, i, i_10_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 -882908971);
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18523(i_12_, true,
							   1472813058);
	} else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i_11_;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i, i_10_,
							   -1916091193);
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18523(i_12_, true,
							   563311276);
	}
    }
    
    void method3779(int i, int i_13_, int i_14_, int i_15_) {
	if (!aBool2142) {
	    aBool2142 = true;
	    if (anInt2141 * -849492911 >= 0) {
		aClass654_Sub1_Sub5_Sub1_Sub1_2143
		    = (new Class654_Sub1_Sub5_Sub1_Sub1
		       (client.aClass512_11100.method8424((byte) 81), 25));
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11967
		    = 1162675721 * client.anInt11101;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18827
		    (((Class307)
		      Class578.aClass44_Sub3_7743
			  .method91(-849492911 * anInt2141, 585456115)),
		     (byte) -2);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18535
		    ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
		      .anInt3328) * -1821838479,
		     260269255);
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11971
		    = ((aClass654_Sub1_Sub5_Sub1_Sub1_2143.aClass307_12204
			.anInt3312) * -1373680215
		       << 3) * 1883849079;
		aClass654_Sub1_Sub5_Sub1_Sub1_2143.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    } else {
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    = (new Class654_Sub1_Sub5_Sub1_Sub2
		       (client.aClass512_11100.method8424((byte) 110), 25));
		aClass654_Sub1_Sub5_Sub1_Sub2_2139
		    .method18865(Class712.aClass534_Sub40_8883, (byte) 30);
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11922
		    = anInt2140 * -2076749011;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11967
		    = client.anInt11101 * 1162675721;
		aClass654_Sub1_Sub5_Sub1_Sub2_2139.anInt11931
		    = (((Class201.anInt2187 += -213444879) * 1294146065 - 1)
		       * 903417281);
	    }
	}
	if (anInt2141 * -849492911 >= 0) {
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i_14_, i, i_13_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 -672762591);
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18523(i_15_, true,
							   -2102253222);
	} else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i_14_;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i, i_13_,
							   -1793279866);
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18523(i_15_, true,
							   -1376617940);
	}
    }
    
    Class192(Class534_Sub40 class534_sub40, int i) {
	aClass654_Sub1_Sub5_Sub1_Sub2_2139 = null;
	anInt2140 = i * 1274430477;
	int i_16_ = class534_sub40.method16527(-92278779);
	switch (i_16_) {
	case 1:
	    anInt2141 = 1795554127;
	    break;
	default:
	    anInt2141 = 1795554127;
	    break;
	case 0:
	    anInt2141 = class534_sub40.method16550((byte) 50) * -1795554127;
	}
	class534_sub40.method16541((byte) -2);
    }
    
    void method3780() {
	aClass654_Sub1_Sub5_Sub1_Sub1_2143 = null;
	aClass654_Sub1_Sub5_Sub1_Sub2_2139 = null;
	aBool2142 = false;
    }
    
    void method3781(int i, int i_17_, int i_18_) {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18832
		(i, i_17_, i_18_, true,
		 aClass654_Sub1_Sub5_Sub1_Sub1_2143.method18545((byte) 1),
		 151430648);
	else {
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10854
		= aClass654_Sub1_Sub5_Sub1_Sub2_2139.aByte10853 = (byte) i;
	    aClass654_Sub1_Sub5_Sub1_Sub2_2139.method18871(i_17_, i_18_,
							   -1729325830);
	}
    }
    
    boolean method3782() {
	if (anInt2141 * -849492911 >= 0) {
	    Class307 class307
		= ((Class307)
		   Class578.aClass44_Sub3_7743.method91(anInt2141 * -849492911,
							1161264131));
	    boolean bool = class307.method5612(-1513758999);
	    if (anIntArray2145 == null) {
		Class570 class570
		    = ((Class570)
		       (Class200_Sub23.aClass44_Sub14_10041.method91
			(1789154529 * class307.anInt3310, 1339275741)));
		anIntArray2145 = class570.method9622((byte) 50);
	    }
	    int[] is = anIntArray2145;
	    for (int i = 0; i < is.length; i++) {
		int i_19_ = is[i];
		bool &= ((Class205) Class200_Sub12.aClass44_Sub1_9934
					.method91(i_19_, -865481194))
			    .method3913(-1968233903);
	    }
	    return bool;
	}
	return true;
    }
    
    public Class654_Sub1_Sub5_Sub1 method3783() {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    return aClass654_Sub1_Sub5_Sub1_Sub1_2143;
	return aClass654_Sub1_Sub5_Sub1_Sub2_2139;
    }
    
    void method3784() {
	aClass654_Sub1_Sub5_Sub1_Sub1_2143 = null;
	aClass654_Sub1_Sub5_Sub1_Sub2_2139 = null;
	aBool2142 = false;
    }
    
    public Class654_Sub1_Sub5_Sub1 method3785() {
	if (null != aClass654_Sub1_Sub5_Sub1_Sub1_2143)
	    return aClass654_Sub1_Sub5_Sub1_Sub1_2143;
	return aClass654_Sub1_Sub5_Sub1_Sub2_2139;
    }
    
    boolean method3786() {
	if (anInt2141 * -849492911 >= 0) {
	    Class307 class307
		= ((Class307)
		   Class578.aClass44_Sub3_7743.method91(anInt2141 * -849492911,
							-1182245503));
	    boolean bool = class307.method5612(-1513758999);
	    if (anIntArray2145 == null) {
		Class570 class570
		    = ((Class570)
		       (Class200_Sub23.aClass44_Sub14_10041.method91
			(1789154529 * class307.anInt3310, 807263165)));
		anIntArray2145 = class570.method9622((byte) 35);
	    }
	    int[] is = anIntArray2145;
	    for (int i = 0; i < is.length; i++) {
		int i_20_ = is[i];
		bool &= ((Class205) Class200_Sub12.aClass44_Sub1_9934
					.method91(i_20_, -1277301367))
			    .method3913(-285388803);
	    }
	    return bool;
	}
	return true;
    }
    
    boolean method3787(byte i) {
	if (anInt2141 * -849492911 >= 0) {
	    Class307 class307
		= ((Class307)
		   Class578.aClass44_Sub3_7743.method91(anInt2141 * -849492911,
							-293252079));
	    boolean bool = class307.method5612(-1513758999);
	    if (anIntArray2145 == null) {
		Class570 class570
		    = ((Class570)
		       (Class200_Sub23.aClass44_Sub14_10041.method91
			(1789154529 * class307.anInt3310, 368259553)));
		anIntArray2145 = class570.method9622((byte) -53);
	    }
	    int[] is = anIntArray2145;
	    for (int i_21_ = 0; i_21_ < is.length; i_21_++) {
		int i_22_ = is[i_21_];
		bool &= ((Class205)
			 Class200_Sub12.aClass44_Sub1_9934.method91(i_22_,
								    283899453))
			    .method3913(1402848874);
	    }
	    return bool;
	}
	return true;
    }
    
    public static int method3788(int i, int i_23_, int i_24_, byte i_25_) {
	int i_26_ = Class211.method3958(i_24_ - i_23_ + 1, (byte) 2);
	i_26_ <<= i_23_;
	i |= i_26_;
	return i;
    }
    
    public static boolean method3789(int i, int i_27_) {
	return i == 9 || i == 17 || i == 7 || i == 6;
    }
    
    static final void method3790(Class669 class669, short i) {
	int i_28_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_28_, 2015144132);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_28_ >> 16];
	Class534_Sub20.method16195(class247, class243, true, 0, class669,
				   -287618308);
    }
    
    static final void method3791(Class669 class669, int i) {
	Class504.method8327((String) (class669.anObjectArray8593
				      [((class669.anInt8594 -= 1460193483)
					* 1485266147)]),
			    false, false, 2096954598);
    }
}
