/* Class195 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.File;
import java.util.LinkedList;

public class Class195
{
    int anInt2153;
    Class595 aClass595_2154;
    int anInt2155;
    int anInt2156;
    int anInt2157;
    int anInt2158;
    static File aFile2159;
    
    Class195(Class534_Sub40 class534_sub40) {
	anInt2156 = class534_sub40.method16550((byte) 19) * 1508553065;
	aClass595_2154
	    = ((Class595)
	       Class448.method7319(Class650.method10724(-447621856),
				   class534_sub40.method16527(1480117275),
				   2088438307));
    }
    
    boolean method3802() {
	if (anInt2156 * -635375911 >= 0) {
	    Class602 class602
		= (Class602) Class175_Sub1.aClass44_Sub13_9427
				 .method91(anInt2156 * -635375911, 1001645006);
	    boolean bool
		= class602.method10000(aClass595_2154.anInt7852 * 847393323,
				       (byte) 66);
	    int[] is = class602.method10020(208406357);
	    if (null != is) {
		int[] is_0_ = is;
		for (int i = 0; i < is_0_.length; i++) {
		    int i_1_ = is_0_[i];
		    bool &= ((Class205) Class200_Sub12.aClass44_Sub1_9934
					    .method91(i_1_, 263342888))
				.method3913(-625845036);
		}
	    }
	    return bool;
	}
	return true;
    }
    
    void method3803(byte i) {
	Class482.method7930(-1762601189 * anInt2155, anInt2157 * -836779145,
			    anInt2153 * -1455419505,
			    aClass595_2154.anInt7853 * 403227023, -1,
			    aClass595_2154.anInt7852 * 847393323,
			    -1387240389 * anInt2158, -1044843463);
    }
    
    void method3804(int i, int i_2_, int i_3_, int i_4_, byte i_5_) {
	Class482.method7930(i, i_2_, i_3_,
			    aClass595_2154.anInt7853 * 403227023,
			    -635375911 * anInt2156,
			    847393323 * aClass595_2154.anInt7852, i_4_,
			    911736384);
	anInt2155 = 123972371 * i;
	anInt2157 = i_2_ * -314309049;
	anInt2153 = i_3_ * -255709329;
	anInt2158 = -1844390669 * i_4_;
    }
    
    void method3805(int i, int i_6_, int i_7_, int i_8_) {
	Class482.method7930(i, i_6_, i_7_,
			    aClass595_2154.anInt7853 * 403227023,
			    -635375911 * anInt2156,
			    847393323 * aClass595_2154.anInt7852, i_8_,
			    709972953);
	anInt2155 = 123972371 * i;
	anInt2157 = i_6_ * -314309049;
	anInt2153 = i_7_ * -255709329;
	anInt2158 = -1844390669 * i_8_;
    }
    
    void method3806() {
	Class482.method7930(-1762601189 * anInt2155, anInt2157 * -836779145,
			    anInt2153 * -1455419505,
			    aClass595_2154.anInt7853 * 403227023, -1,
			    aClass595_2154.anInt7852 * 847393323,
			    -1387240389 * anInt2158, -55150287);
    }
    
    boolean method3807(int i) {
	if (anInt2156 * -635375911 >= 0) {
	    Class602 class602
		= (Class602) Class175_Sub1.aClass44_Sub13_9427
				 .method91(anInt2156 * -635375911, 1370611075);
	    boolean bool
		= class602.method10000(aClass595_2154.anInt7852 * 847393323,
				       (byte) 64);
	    int[] is = class602.method10020(-504977787);
	    if (null != is) {
		int[] is_9_ = is;
		for (int i_10_ = 0; i_10_ < is_9_.length; i_10_++) {
		    int i_11_ = is_9_[i_10_];
		    bool &= ((Class205) Class200_Sub12.aClass44_Sub1_9934
					    .method91(i_11_, -426580364))
				.method3913(-522092273);
		}
	    }
	    return bool;
	}
	return true;
    }
    
    void method3808() {
	Class482.method7930(-1762601189 * anInt2155, anInt2157 * -836779145,
			    anInt2153 * -1455419505,
			    aClass595_2154.anInt7853 * 403227023, -1,
			    aClass595_2154.anInt7852 * 847393323,
			    -1387240389 * anInt2158, 1490210273);
    }
    
    void method3809() {
	Class482.method7930(-1762601189 * anInt2155, anInt2157 * -836779145,
			    anInt2153 * -1455419505,
			    aClass595_2154.anInt7853 * 403227023, -1,
			    aClass595_2154.anInt7852 * 847393323,
			    -1387240389 * anInt2158, -977230636);
    }
    
    boolean method3810() {
	if (anInt2156 * -635375911 >= 0) {
	    Class602 class602
		= (Class602) Class175_Sub1.aClass44_Sub13_9427
				 .method91(anInt2156 * -635375911, 553934993);
	    boolean bool
		= class602.method10000(aClass595_2154.anInt7852 * 847393323,
				       (byte) 74);
	    int[] is = class602.method10020(1006259686);
	    if (null != is) {
		int[] is_12_ = is;
		for (int i = 0; i < is_12_.length; i++) {
		    int i_13_ = is_12_[i];
		    bool &= ((Class205) Class200_Sub12.aClass44_Sub1_9934
					    .method91(i_13_, -559068855))
				.method3913(-1474407173);
		}
	    }
	    return bool;
	}
	return true;
    }
    
    void method3811() {
	Class482.method7930(-1762601189 * anInt2155, anInt2157 * -836779145,
			    anInt2153 * -1455419505,
			    aClass595_2154.anInt7853 * 403227023, -1,
			    aClass595_2154.anInt7852 * 847393323,
			    -1387240389 * anInt2158, 3425829);
    }
    
    public static void method3812(Class185 class185, long l, int i, int i_14_,
				  int i_15_) {
	do {
	    if (class185.method3269() && class185.method3273() > -1) {
		int i_16_ = class185.method3273();
		Class573 class573 = null;
		if (!Class574.aLinkedList7702.isEmpty())
		    class573 = (Class573) Class574.aLinkedList7702.getFirst();
		if (class573 == null
		    || i_16_ != 733600403 * class573.anInt7669) {
		    class185.method3656();
		    Class574.aLinkedList7702.clear();
		} else {
		    Class574.aLinkedList7702.removeFirst();
		    class185.method3559();
		    boolean bool = false;
		    int i_17_;
		    if (class573.aLinkedList7672.isEmpty()
			&& class185.method3396()) {
			long l_18_ = class185.method3355((Class387.anInt4050
							  * -777198511),
							 (Class503.anInt5629
							  * 1445010517));
			i_17_ = (Class523.aTwitchTV7088.SubmitFrame
				 (l_18_, class185.method3666(),
				  Class574.aBool7697, i, i_14_));
			class185.method3277(l_18_);
		    } else {
			class185.method3278(-777198511 * Class387.anInt4050,
					    1445010517 * Class503.anInt5629,
					    Class574.anIntArray7698,
					    Class574.anIntArray7699);
			Class654_Sub1_Sub5_Sub1_Sub1.method18862
			    (Class574.anIntArray7698, class573.aLinkedList7672,
			     1644634333 * class573.anInt7670,
			     class573.anInt7668 * -1875063907,
			     class573.aFloat7671, -1130537991);
			i_17_ = (Class523.aTwitchTV7088.SubmitFrameRaw
				 (Class574.anIntArray7698, Class574.aBool7697,
				  i, i_14_));
		    }
		    class185.method3446();
		    if (2072 == i_17_)
			Class574.aLong7693 = -3117649911290242775L * l;
		    else if (23 == i_17_)
			Class574.aLong7694 = l * 9074349051421350485L;
		    else if (-995 == i_17_ || i_17_ != 0)
			break;
		}
	    }
	} while (false);
	if (Class574.aBool7681) {
	    if (Class574.anIntArray7698 == null) {
		Class574.anIntArray7698
		    = new int[(Class574.aBool7697 ? i * i_14_
			       : (1445010517 * Class503.anInt5629
				  * (-777198511 * Class387.anInt4050)))];
		Class574.anIntArray7699
		    = new int[(Class574.aBool7697 ? i_14_ * i
			       : (Class387.anInt4050 * -777198511
				  * (Class503.anInt5629 * 1445010517)))];
	    }
	    if (!class185.method3269()) {
		class185.method3559();
		class185.method3278(Class387.anInt4050 * -777198511,
				    Class503.anInt5629 * 1445010517,
				    Class574.anIntArray7698,
				    Class574.anIntArray7699);
		Class654_Sub1_Sub5_Sub1_Sub1.method18862
		    (Class574.anIntArray7698, Class574.aLinkedList7701,
		     class185.anInt2018 * -1555714981,
		     -385311879 * class185.anInt2019, class185.aFloat2010,
		     -1578190742);
		int i_19_
		    = Class523.aTwitchTV7088.SubmitFrameRaw((Class574
							     .anIntArray7698),
							    Class574.aBool7697,
							    i, i_14_);
		class185.method3446();
		Class574.aLinkedList7701.clear();
		do {
		    if (i_19_ == 2072)
			Class574.aLong7693 = -3117649911290242775L * l;
		    else if (i_19_ == 23)
			Class574.aLong7694 = 9074349051421350485L * l;
		    else if (i_19_ == -995 || i_19_ != 0)
			break;
		} while (false);
	    } else {
		class185.method3275(client.anInt11101,
				    Class387.anInt4050 * -777198511,
				    Class503.anInt5629 * 1445010517);
		Class574.aLinkedList7702.add
		    (new Class573(client.anInt11101, Class574.aLinkedList7701,
				  -1555714981 * class185.anInt2018,
				  -385311879 * class185.anInt2019,
				  class185.aFloat2010));
		Class574.aLinkedList7701 = new LinkedList();
	    }
	    Class574.aBool7681 = false;
	}
    }
    
    static final void method3813(Class669 class669, byte i) {
	int i_20_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_20_, 929595268);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_20_ >> 16];
	Class329.method5833(class247, class243, class669, -2023974633);
    }
    
    static final void method3814(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class154.method2576(class247, class243, class669, (byte) -1);
    }
    
    static final void method3815(Class669 class669, int i) {
	throw new RuntimeException("");
    }
    
    static final void method3816(Class669 class669, int i) {
	int i_21_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751
		  .method14026(i_21_, -2024064741);
    }
}
