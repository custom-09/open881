/* Class200 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class200
{
    public int anInt2177;
    
    static Class200 method3842(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(-354743047);
	Class88 class88 = Class225.method4174(i, 378896264);
	Class200 class200 = null;
	switch (-1763082495 * class88.anInt860) {
	case 26:
	    class200 = new Class200_Sub4(class534_sub40);
	    break;
	case 6:
	    class200 = new Class200_Sub9(class534_sub40);
	    break;
	case 9:
	    class200 = new Class200_Sub3(class534_sub40);
	    break;
	case 1:
	    class200 = new Class200_Sub21(class534_sub40);
	    break;
	case 23:
	    class200 = new Class200_Sub20(class534_sub40);
	    break;
	case 12:
	    class200 = new Class200_Sub10(class534_sub40);
	    break;
	default:
	    break;
	case 2:
	    class200 = new Class200_Sub12_Sub2(class534_sub40);
	    break;
	case 17:
	    class200 = new Class200_Sub18(class534_sub40);
	    break;
	case 27:
	    class200 = new Class200_Sub6(class534_sub40);
	    break;
	case 20:
	    class200 = new Class200_Sub15(class534_sub40);
	    break;
	case 21:
	    class200 = new Class200_Sub14(class534_sub40);
	    break;
	case 18:
	    class200 = new Class200_Sub19(class534_sub40);
	    break;
	case 24:
	    class200 = new Class200_Sub8(class534_sub40);
	    break;
	case 14:
	    class200 = new Class200_Sub11(class534_sub40);
	    break;
	case 7:
	    class200 = new Class200_Sub1(class534_sub40);
	    break;
	case 4:
	    class200 = new Class200_Sub23(class534_sub40, 1, 1);
	    break;
	case 10:
	    class200 = new Class200_Sub7(class534_sub40);
	    break;
	case 8:
	    class200 = new Class200_Sub16(class534_sub40);
	    break;
	case 11:
	    class200 = new Class200_Sub13(class534_sub40, true);
	    break;
	case 5:
	    class200 = new Class200_Sub23(class534_sub40, 0, 0);
	    break;
	case 29:
	    class200 = new Class200_Sub13(class534_sub40, false);
	    break;
	case 16:
	    class200 = new Class200_Sub23(class534_sub40, 1, 0);
	    break;
	case 22:
	    class200 = new Class200_Sub2(class534_sub40);
	    break;
	case 3:
	    class200 = new Class200_Sub23(class534_sub40, 0, 1);
	    break;
	case 28:
	    class200 = new Class200_Sub22(class534_sub40);
	    break;
	case 13:
	    class200 = new Class200_Sub12_Sub1(class534_sub40);
	    break;
	case 15:
	    class200 = new Class200_Sub5(class534_sub40);
	    break;
	case 30:
	    class200 = new Class200_Sub17(class534_sub40);
	}
	return class200;
    }
    
    void method3843(byte i) {
	/* empty */
    }
    
    boolean method3844(int i) {
	return true;
    }
    
    public abstract void method3845(int i);
    
    public abstract void method3846();
    
    public abstract void method3847();
    
    boolean method3848() {
	return true;
    }
    
    boolean method3849() {
	return true;
    }
    
    void method3850() {
	/* empty */
    }
    
    static Class200 method3851(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(-1817315844);
	Class88 class88 = Class225.method4174(i, 1265543667);
	Class200 class200 = null;
	switch (-1763082495 * class88.anInt860) {
	case 26:
	    class200 = new Class200_Sub4(class534_sub40);
	    break;
	case 6:
	    class200 = new Class200_Sub9(class534_sub40);
	    break;
	case 9:
	    class200 = new Class200_Sub3(class534_sub40);
	    break;
	case 1:
	    class200 = new Class200_Sub21(class534_sub40);
	    break;
	case 23:
	    class200 = new Class200_Sub20(class534_sub40);
	    break;
	case 12:
	    class200 = new Class200_Sub10(class534_sub40);
	    break;
	default:
	    break;
	case 2:
	    class200 = new Class200_Sub12_Sub2(class534_sub40);
	    break;
	case 17:
	    class200 = new Class200_Sub18(class534_sub40);
	    break;
	case 27:
	    class200 = new Class200_Sub6(class534_sub40);
	    break;
	case 20:
	    class200 = new Class200_Sub15(class534_sub40);
	    break;
	case 21:
	    class200 = new Class200_Sub14(class534_sub40);
	    break;
	case 18:
	    class200 = new Class200_Sub19(class534_sub40);
	    break;
	case 24:
	    class200 = new Class200_Sub8(class534_sub40);
	    break;
	case 14:
	    class200 = new Class200_Sub11(class534_sub40);
	    break;
	case 7:
	    class200 = new Class200_Sub1(class534_sub40);
	    break;
	case 4:
	    class200 = new Class200_Sub23(class534_sub40, 1, 1);
	    break;
	case 10:
	    class200 = new Class200_Sub7(class534_sub40);
	    break;
	case 8:
	    class200 = new Class200_Sub16(class534_sub40);
	    break;
	case 11:
	    class200 = new Class200_Sub13(class534_sub40, true);
	    break;
	case 5:
	    class200 = new Class200_Sub23(class534_sub40, 0, 0);
	    break;
	case 29:
	    class200 = new Class200_Sub13(class534_sub40, false);
	    break;
	case 16:
	    class200 = new Class200_Sub23(class534_sub40, 1, 0);
	    break;
	case 22:
	    class200 = new Class200_Sub2(class534_sub40);
	    break;
	case 3:
	    class200 = new Class200_Sub23(class534_sub40, 0, 1);
	    break;
	case 28:
	    class200 = new Class200_Sub22(class534_sub40);
	    break;
	case 13:
	    class200 = new Class200_Sub12_Sub1(class534_sub40);
	    break;
	case 15:
	    class200 = new Class200_Sub5(class534_sub40);
	    break;
	case 30:
	    class200 = new Class200_Sub17(class534_sub40);
	}
	return class200;
    }
    
    Class200(Class534_Sub40 class534_sub40) {
	anInt2177 = class534_sub40.method16529((byte) 1) * -525321255;
    }
    
    static int method3852(int i, int i_0_, int i_1_) {
	if (9 == i_0_)
	    i = i + 1 & 0x3;
	if (10 == i_0_)
	    i = i + 3 & 0x3;
	if (11 == i_0_)
	    i = 3 + i & 0x3;
	return i;
    }
    
    static final void method3853(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (client.anInt11171 * -1050164879 == 2
	    && i_2_ < -1979292205 * client.anInt11324)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= client.aClass28Array11327[i_2_].aString254;
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
    }
    
    static final void method3854(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_3_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_4_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	Class15 class15
	    = (Class15) Class531.aClass44_Sub7_7135.method91(i_3_, 1391979461);
	if (i_4_ >= 1 && i_4_ <= 5
	    && class15.aStringArray133[i_4_ - 1] != null)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class15.aStringArray133[i_4_ - 1];
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
    }
    
    static final void method3855(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	long l = Class116.method2116(421874442);
	if (l == 0L)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 5;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= Class382.method6450(l, string, -2022026003);
    }
    
    public static final void method3856(byte i) {
	String string = (null != Class531.aString7137 ? Class531.aString7137
			 : Class286.method5266(-1528217822));
	Class468.method7622(string, false, client.aBool11032, 1411490484);
    }
    
    static final String[] method3857(String[] strings, byte i) {
	String[] strings_5_ = new String[5];
	for (int i_6_ = 0; i_6_ < 5; i_6_++) {
	    strings_5_[i_6_]
		= new StringBuilder().append(i_6_).append(" ").toString();
	    if (strings != null && null != strings[i_6_])
		strings_5_[i_6_]
		    = new StringBuilder().append(strings_5_[i_6_]).append
			  (strings[i_6_]).toString();
	}
	return strings_5_;
    }
}
