/* Class200_Sub10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub10 extends Class200
{
    int anInt9921;
    Class491 aClass491_9922;
    int anInt9923;
    int anInt9924;
    int anInt9925;
    public static Class44_Sub20 aClass44_Sub20_9926;
    static short aShort9927;
    
    void method3843(byte i) {
	if (null != aClass491_9922) {
	    aClass491_9922.method8014(50, 1962988118);
	    Class171_Sub4.aClass232_9944.method4234(aClass491_9922,
						    1914256505);
	    aClass491_9922 = null;
	}
    }
    
    public void method3845(int i) {
	if (null != aClass491_9922)
	    aClass491_9922.method8069(2032588897);
    }
    
    public void method3847() {
	if (null != aClass491_9922)
	    aClass491_9922.method8069(2032912697);
    }
    
    public void method3846() {
	if (null != aClass491_9922)
	    aClass491_9922.method8069(1957742006);
    }
    
    Class200_Sub10(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9925 = class534_sub40.method16529((byte) 1) * 2035251543;
	anInt9921 = class534_sub40.method16527(258027662) * 383236069;
	anInt9923 = class534_sub40.method16527(-1130085759) * -1626840205;
	anInt9924 = class534_sub40.method16527(1430442913) * -1556749807;
	aClass491_9922
	    = (Class171_Sub4.aClass232_9944.method4229
	       (Class211.aClass211_2255, this, anInt9925 * -1395096985,
		605376241 * anInt9924, anInt9921 * -56513043,
		Class190.aClass190_2134.method3763(-1387654841),
		Class207.aClass207_2235, 0.0F, 0.0F, null, 0,
		anInt9923 * -13993541, false, -1889477317));
	if (null != aClass491_9922)
	    aClass491_9922.method8012(-1720662844);
    }
    
    void method3850() {
	if (null != aClass491_9922) {
	    aClass491_9922.method8014(50, 1962988118);
	    Class171_Sub4.aClass232_9944.method4234(aClass491_9922, 435877823);
	    aClass491_9922 = null;
	}
    }
}
