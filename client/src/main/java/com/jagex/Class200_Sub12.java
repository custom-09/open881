/* Class200_Sub12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class200_Sub12 extends Class200
{
    int anInt9931;
    int anInt9932;
    int anInt9933;
    public static Class44_Sub1 aClass44_Sub1_9934;
    
    boolean method3844(int i) {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(1538714989 * anInt9933,
						  254061444));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       aClass44_Sub1_9934.method91(-811043807 * class684.anInt8688,
					   -1835988021));
	bool &= class205.method3913(331961295);
	return bool;
    }
    
    boolean method3849() {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(1538714989 * anInt9933,
						  926816796));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       aClass44_Sub1_9934.method91(-811043807 * class684.anInt8688,
					   -1480002310));
	bool &= class205.method3913(-1946244938);
	return bool;
    }
    
    boolean method3848() {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(1538714989 * anInt9933,
						  -315695554));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       aClass44_Sub1_9934.method91(-811043807 * class684.anInt8688,
					   38385660));
	bool &= class205.method3913(-214514997);
	return bool;
    }
    
    Class200_Sub12(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9933 = class534_sub40.method16529((byte) 1) * 596976741;
	anInt9931 = class534_sub40.method16529((byte) 1) * -362414937;
	anInt9932 = class534_sub40.method16527(-555834424) * -471861621;
    }
    
    static final void method15586(byte i) {
	client.anInt11023 = -1139557399;
	client.anInt11155 = -143767915;
	Class712.aClass534_Sub40_8883 = null;
	Class155_Sub1.method15473(-159845223);
    }
    
    static final void method15587(Class669 class669, int i) {
	class669.anInt8594 -= 85613153;
	class669.anInt8600 -= 617999126;
	Class657.method10896((String) (class669.anObjectArray8593
				       [class669.anInt8594 * 1485266147]),
			     (String) (class669.anObjectArray8593
				       [class669.anInt8594 * 1485266147 + 1]),
			     (class669.anIntArray8595
			      [2088438307 * class669.anInt8600]),
			     1 == (class669.anIntArray8595
				   [class669.anInt8600 * 2088438307 + 1]),
			     (String) (class669.anObjectArray8593
				       [2 + class669.anInt8594 * 1485266147]),
			     2138059739);
    }
}
