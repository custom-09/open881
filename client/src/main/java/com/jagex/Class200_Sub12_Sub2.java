/* Class200_Sub12_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub12_Sub2 extends Class200_Sub12
{
    int anInt11609;
    int anInt11610;
    int anInt11611;
    
    public void method3847() {
	int i = -1882020352 * anInt11610 + 256;
	int i_0_ = anInt11609 * -1129047552 + 256;
	int i_1_ = -392573345 * anInt11611;
	if (i_1_ < 3
	    && (client.aClass512_11100.method8552((byte) 0).method7612
		(anInt11610 * 935848275, anInt11609 * 1633573389, (byte) 0)))
	    i_1_++;
	Class654_Sub1_Sub5_Sub5 class654_sub1_sub5_sub5
	    = (new Class654_Sub1_Sub5_Sub5
	       (client.aClass512_11100.method8424((byte) 42),
		1538714989 * anInt9933, 0, -392573345 * anInt11611, i_1_, i,
		Class247.method4595(i, i_0_, anInt11611 * -392573345,
				    1101711042) - anInt9932 * 290996515,
		i_0_, 935848275 * anInt11610, 935848275 * anInt11610,
		1633573389 * anInt11609, anInt11609 * 1633573389,
		anInt9931 * 1055910679, false, 0));
	client.aClass9_11322.method581
	    (new Class534_Sub18_Sub5(class654_sub1_sub5_sub5),
	     (long) (anInt11610 * 935848275 << 16 | anInt11609 * 1633573389));
    }
    
    Class200_Sub12_Sub2(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	int i = class534_sub40.method16533(-258848859);
	anInt11610 = 207373019 * (i >>> 16);
	anInt11609 = -330398523 * (i & 0xffff);
	anInt11611 = class534_sub40.method16527(1823463802) * -876474977;
    }
    
    public void method3846() {
	int i = -1882020352 * anInt11610 + 256;
	int i_2_ = anInt11609 * -1129047552 + 256;
	int i_3_ = -392573345 * anInt11611;
	if (i_3_ < 3
	    && (client.aClass512_11100.method8552((byte) 0).method7612
		(anInt11610 * 935848275, anInt11609 * 1633573389, (byte) 0)))
	    i_3_++;
	Class654_Sub1_Sub5_Sub5 class654_sub1_sub5_sub5
	    = (new Class654_Sub1_Sub5_Sub5
	       (client.aClass512_11100.method8424((byte) 92),
		1538714989 * anInt9933, 0, -392573345 * anInt11611, i_3_, i,
		Class247.method4595(i, i_2_, anInt11611 * -392573345,
				    818448103) - anInt9932 * 290996515,
		i_2_, 935848275 * anInt11610, 935848275 * anInt11610,
		1633573389 * anInt11609, anInt11609 * 1633573389,
		anInt9931 * 1055910679, false, 0));
	client.aClass9_11322.method581
	    (new Class534_Sub18_Sub5(class654_sub1_sub5_sub5),
	     (long) (anInt11610 * 935848275 << 16 | anInt11609 * 1633573389));
    }
    
    public void method3845(int i) {
	int i_4_ = -1882020352 * anInt11610 + 256;
	int i_5_ = anInt11609 * -1129047552 + 256;
	int i_6_ = -392573345 * anInt11611;
	if (i_6_ < 3
	    && (client.aClass512_11100.method8552((byte) 0).method7612
		(anInt11610 * 935848275, anInt11609 * 1633573389, (byte) 0)))
	    i_6_++;
	Class654_Sub1_Sub5_Sub5 class654_sub1_sub5_sub5
	    = (new Class654_Sub1_Sub5_Sub5
	       (client.aClass512_11100.method8424((byte) 102),
		1538714989 * anInt9933, 0, -392573345 * anInt11611, i_6_, i_4_,
		Class247.method4595(i_4_, i_5_, anInt11611 * -392573345,
				    -1963035508) - anInt9932 * 290996515,
		i_5_, 935848275 * anInt11610, 935848275 * anInt11610,
		1633573389 * anInt11609, anInt11609 * 1633573389,
		anInt9931 * 1055910679, false, 0));
	client.aClass9_11322.method581
	    (new Class534_Sub18_Sub5(class654_sub1_sub5_sub5),
	     (long) (anInt11610 * 935848275 << 16 | anInt11609 * 1633573389));
    }
}
