/* Class200_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub20 extends Class200
{
    int anInt9981;
    int anInt9982;
    
    public void method3845(int i) {
	Class171_Sub4.aClass232_9944.method4248(anInt9981 * -387081723,
						-1444493289 * anInt9982,
						(byte) 60);
    }
    
    public void method3847() {
	Class171_Sub4.aClass232_9944.method4248(anInt9981 * -387081723,
						-1444493289 * anInt9982,
						(byte) -15);
    }
    
    public void method3846() {
	Class171_Sub4.aClass232_9944.method4248(anInt9981 * -387081723,
						-1444493289 * anInt9982,
						(byte) -60);
    }
    
    Class200_Sub20(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9981 = class534_sub40.method16529((byte) 1) * -707058995;
	anInt9982 = class534_sub40.method16527(-1012624281) * 2034954151;
    }
    
    public static Class534_Sub18_Sub14 method15635(int i, short i_0_) {
	return ((Class534_Sub18_Sub14)
		Class274.aClass9_3036.method579((long) i));
    }
    
    static final void method15636(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	int i_1_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_2_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	int i_3_
	    = class669.anIntArray8595[2 + 2088438307 * class669.anInt8600];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass352_8602.method6237(i_1_, i_2_, i_3_, -442966873);
    }
}
