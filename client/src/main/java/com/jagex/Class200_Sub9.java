/* Class200_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub9 extends Class200
{
    public static int anInt9916;
    
    public void method3845(int i) {
	Class118.method2128(true, 2021598882);
    }
    
    Class200_Sub9(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
    }
    
    public void method3846() {
	Class118.method2128(true, 1279758206);
    }
    
    public void method3847() {
	Class118.method2128(true, 1964137680);
    }
    
    public static int method15582(int i, int i_0_, int i_1_, boolean bool,
				  short i_2_) {
	Class318 class318
	    = (Class318) Class84.aClass44_Sub11_840.method91(i_1_, -752506471);
	Class150 class150 = class318.aClass150_3392;
	Class534_Sub5 class534_sub5 = Class269.method5023(i, bool, (byte) -4);
	int i_3_;
	if (class534_sub5 == null || i_0_ < 0
	    || i_0_ >= class534_sub5.anIntArray10415.length
	    || null == class534_sub5.aClass8Array10416
	    || null == class534_sub5.aClass8Array10416[i_0_])
	    i_3_ = ((Integer) class150.method2478(1706982101)).intValue();
	else
	    i_3_ = class534_sub5.aClass8Array10416[i_0_]
		       .method32(-1270946121 * class150.anInt1694, 104283526);
	return class318.method5750(i_3_, 1932461818);
    }
    
    static final void method15583(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method15584(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	if (class669.anIntArray8595[class669.anInt8600 * 2088438307]
	    == class669.anIntArray8595[1 + 2088438307 * class669.anInt8600])
	    class669.anInt8613
		+= (class669.anIntArray8591[662605117 * class669.anInt8613]
		    * -793595371);
    }
}
