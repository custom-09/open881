/* Class201 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class201
{
    static Class204[] aClass204Array2178;
    static int anInt2179;
    public static Class9 aClass9_2180;
    static short aShort2181;
    static int anInt2182;
    public static Class700 aClass700_2183 = new Class700();
    static boolean aBool2184;
    static int anInt2185;
    static int anInt2186;
    static int anInt2187;
    public static Interface20 anInterface20_2188;
    
    public static void method3858() {
	Class34.aShort279 = client.aShort11317;
	Class170.aShort1793 = client.aShort11318;
	Class200_Sub10.aShort9927 = client.aShort11315;
	aShort2181 = client.aShort11316;
	aBool2184 = true;
	anInt2186
	    = Class171_Sub4.aClass232_9944.method4268((byte) 56) * 683420367;
	if (0 != anInt2179 * 1103230549 && anInt2182 * 1532234787 != 0) {
	    client.aShort11315 = (short) 334;
	    client.aShort11316 = (short) 334;
	    client.aShort11318 = client.aShort11317
		= (short) (-2081641984 * anInt2179 / (1532234787 * anInt2182));
	}
    }
    
    static {
	aClass9_2180 = new Class9(32);
	anInt2187 = -213444879;
	anInt2182 = 0;
	anInt2179 = 0;
	aBool2184 = false;
	anInt2186 = -683420367;
	anInt2185 = -2070289217;
	anInterface20_2188 = new Class191();
    }
    
    public static void method3859() {
	aClass9_2180.method578((byte) -51);
	aClass700_2183.method14152(-1569776237);
	aClass204Array2178 = null;
	Class65.aClass192Array712 = null;
	Class539_Sub1.aClass195Array10326 = null;
	Class45.aClass209Array330 = null;
	Class700.aClass200Array8807 = null;
	anInt2187 = -213444879;
	anInt2182 = 0;
	anInt2179 = 0;
	Class665.aClass202_8557 = null;
	anInt2185 = -2070289217;
	anInt2186 = -683420367;
	if (aBool2184) {
	    client.aShort11317 = Class34.aShort279;
	    client.aShort11318 = Class170.aShort1793;
	    client.aShort11315 = Class200_Sub10.aShort9927;
	    client.aShort11316 = aShort2181;
	    aBool2184 = false;
	}
    }
    
    public static void method3860(boolean bool) {
	if (4 != client.anInt11155 * -1468443459
	    && 1 != client.anInt11155 * -1468443459) {
	    if (!bool) {
		if (Class700.aClass200Array8807 != null) {
		    Class200[] class200s = Class700.aClass200Array8807;
		    for (int i = 0; i < class200s.length; i++) {
			Class200 class200 = class200s[i];
			class200.method3843((byte) 101);
		    }
		}
		if (anInt2186 * -1908237265 != -1)
		    Class171_Sub4.aClass232_9944
			.method4244(anInt2186 * -1908237265, 255, (byte) 70);
	    }
	    client.anInt11155 = -575071660;
	    Class712.aClass534_Sub40_8883 = null;
	    client.aBool11109 = false;
	    if (1689023681 * anInt2185 > 0)
		Class680.method13862(Class583.aClass583_7790,
				     1689023681 * anInt2185, -1, (byte) 115);
	    Class155_Sub1.method15473(-370631255);
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4245,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(bool ? 1 : 0,
								  1249325997);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 30);
	}
    }
    
    public static void method3861() {
	Class34.aShort279 = client.aShort11317;
	Class170.aShort1793 = client.aShort11318;
	Class200_Sub10.aShort9927 = client.aShort11315;
	aShort2181 = client.aShort11316;
	aBool2184 = true;
	anInt2186
	    = Class171_Sub4.aClass232_9944.method4268((byte) 50) * 683420367;
	if (0 != anInt2179 * 1103230549 && anInt2182 * 1532234787 != 0) {
	    client.aShort11315 = (short) 334;
	    client.aShort11316 = (short) 334;
	    client.aShort11318 = client.aShort11317
		= (short) (-2081641984 * anInt2179 / (1532234787 * anInt2182));
	}
    }
    
    public static void method3862(boolean bool) {
	if (4 != client.anInt11155 * -1468443459
	    && 1 != client.anInt11155 * -1468443459) {
	    if (!bool) {
		if (Class700.aClass200Array8807 != null) {
		    Class200[] class200s = Class700.aClass200Array8807;
		    for (int i = 0; i < class200s.length; i++) {
			Class200 class200 = class200s[i];
			class200.method3843((byte) 17);
		    }
		}
		if (anInt2186 * -1908237265 != -1)
		    Class171_Sub4.aClass232_9944
			.method4244(anInt2186 * -1908237265, 255, (byte) -24);
	    }
	    client.anInt11155 = -575071660;
	    Class712.aClass534_Sub40_8883 = null;
	    client.aBool11109 = false;
	    if (1689023681 * anInt2185 > 0)
		Class680.method13862(Class583.aClass583_7790,
				     1689023681 * anInt2185, -1, (byte) 116);
	    Class155_Sub1.method15473(-1825682737);
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4245,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(bool ? 1 : 0,
								  1244238075);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 98);
	}
    }
    
    Class201() throws Throwable {
	throw new Error();
    }
    
    static final void method3863(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -488164841 * class247.anInt2478;
    }
    
    public static Class100 method3864(int i) {
	if (Class192.method3789(-1850530127 * client.anInt11039, -1613277638)
	    || 18 == client.anInt11039 * -1850530127)
	    return client.aClass100_11094;
	return client.aClass100_11264;
    }
}
