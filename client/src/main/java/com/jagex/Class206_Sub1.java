/* Class206_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class206_Sub1 extends Class206
{
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class205(i, this);
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class205(i, this);
    }
    
    public Class method59(short i) {
	return com.jagex.Class205.class;
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class205(i, this);
    }
    
    Class206_Sub1(Interface14 interface14, Class472 class472,
		  Class472 class472_1_) {
	super(interface14, class472, class472_1_);
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class205(i, this);
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class205(i, this);
    }
    
    public Class method61() {
	return com.jagex.Class205.class;
    }
    
    static final void method15705(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = new StringBuilder().append(string).append(i_2_).toString();
    }
    
    static final void method15706(Class247 class247, Class243 class243,
				  Class669 class669, byte i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2553
	    = Class99.method1859(string, class669, 519080705);
	class247.aBool2561 = true;
    }
    
    static final void method15707(Class247 class247, Class243 class243,
				  Class669 class669, byte i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2443
	    = Class99.method1859(string, class669, 682366271);
	class247.aBool2561 = true;
    }
}
