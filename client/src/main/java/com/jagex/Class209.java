/* Class209 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class209
{
    int[] anIntArray2242;
    static final int anInt2243 = 2;
    static final int anInt2244 = 0;
    int[] anIntArray2245;
    
    void method3947(Class192 class192, int i, int i_0_) {
	int i_1_ = anIntArray2245[0];
	class192.method3774(i, i_1_ >>> 16, i_1_ & 0xffff, (byte) -32);
	Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
	    = class192.method3775(2088438307);
	class654_sub1_sub5_sub1.anInt11980 = 0;
	for (int i_2_ = anIntArray2242.length - 1; i_2_ >= 0; i_2_--) {
	    int i_3_ = anIntArray2242[i_2_];
	    int i_4_ = anIntArray2245[i_2_];
	    class654_sub1_sub5_sub1.anIntArray11977
		[class654_sub1_sub5_sub1.anInt11980 * -1763707177]
		= i_4_ >> 16;
	    class654_sub1_sub5_sub1.anIntArray11978
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_4_ & 0xffff;
	    byte i_5_ = Class676.aClass676_8649.aByte8648;
	    if (i_3_ == 0)
		i_5_ = Class676.aClass676_8644.aByte8648;
	    else if (2 == i_3_)
		i_5_ = Class676.aClass676_8650.aByte8648;
	    class654_sub1_sub5_sub1.aByteArray11979
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_5_;
	    class654_sub1_sub5_sub1.anInt11980 += 990207207;
	}
    }
    
    Class209(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16546(-1706829710);
	anIntArray2242 = new int[i];
	anIntArray2245 = new int[i];
	for (int i_6_ = 0; i_6_ < i; i_6_++) {
	    int i_7_ = class534_sub40.method16527(-1794528333);
	    anIntArray2242[i_6_] = i_7_;
	    int i_8_ = class534_sub40.method16529((byte) 1);
	    int i_9_ = class534_sub40.method16529((byte) 1);
	    anIntArray2245[i_6_] = (i_8_ << 16) + i_9_;
	}
    }
    
    void method3948(Class192 class192, int i) {
	int i_10_ = anIntArray2245[0];
	class192.method3774(i, i_10_ >>> 16, i_10_ & 0xffff, (byte) -66);
	Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
	    = class192.method3775(2088438307);
	class654_sub1_sub5_sub1.anInt11980 = 0;
	for (int i_11_ = anIntArray2242.length - 1; i_11_ >= 0; i_11_--) {
	    int i_12_ = anIntArray2242[i_11_];
	    int i_13_ = anIntArray2245[i_11_];
	    class654_sub1_sub5_sub1.anIntArray11977
		[class654_sub1_sub5_sub1.anInt11980 * -1763707177]
		= i_13_ >> 16;
	    class654_sub1_sub5_sub1.anIntArray11978
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_13_ & 0xffff;
	    byte i_14_ = Class676.aClass676_8649.aByte8648;
	    if (i_12_ == 0)
		i_14_ = Class676.aClass676_8644.aByte8648;
	    else if (2 == i_12_)
		i_14_ = Class676.aClass676_8650.aByte8648;
	    class654_sub1_sub5_sub1.aByteArray11979
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_14_;
	    class654_sub1_sub5_sub1.anInt11980 += 990207207;
	}
    }
    
    void method3949(Class192 class192, int i) {
	int i_15_ = anIntArray2245[0];
	class192.method3774(i, i_15_ >>> 16, i_15_ & 0xffff, (byte) -21);
	Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
	    = class192.method3775(2088438307);
	class654_sub1_sub5_sub1.anInt11980 = 0;
	for (int i_16_ = anIntArray2242.length - 1; i_16_ >= 0; i_16_--) {
	    int i_17_ = anIntArray2242[i_16_];
	    int i_18_ = anIntArray2245[i_16_];
	    class654_sub1_sub5_sub1.anIntArray11977
		[class654_sub1_sub5_sub1.anInt11980 * -1763707177]
		= i_18_ >> 16;
	    class654_sub1_sub5_sub1.anIntArray11978
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_18_ & 0xffff;
	    byte i_19_ = Class676.aClass676_8649.aByte8648;
	    if (i_17_ == 0)
		i_19_ = Class676.aClass676_8644.aByte8648;
	    else if (2 == i_17_)
		i_19_ = Class676.aClass676_8650.aByte8648;
	    class654_sub1_sub5_sub1.aByteArray11979
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_19_;
	    class654_sub1_sub5_sub1.anInt11980 += 990207207;
	}
    }
    
    void method3950(Class192 class192, int i) {
	int i_20_ = anIntArray2245[0];
	class192.method3774(i, i_20_ >>> 16, i_20_ & 0xffff, (byte) -101);
	Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
	    = class192.method3775(2088438307);
	class654_sub1_sub5_sub1.anInt11980 = 0;
	for (int i_21_ = anIntArray2242.length - 1; i_21_ >= 0; i_21_--) {
	    int i_22_ = anIntArray2242[i_21_];
	    int i_23_ = anIntArray2245[i_21_];
	    class654_sub1_sub5_sub1.anIntArray11977
		[class654_sub1_sub5_sub1.anInt11980 * -1763707177]
		= i_23_ >> 16;
	    class654_sub1_sub5_sub1.anIntArray11978
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_23_ & 0xffff;
	    byte i_24_ = Class676.aClass676_8649.aByte8648;
	    if (i_22_ == 0)
		i_24_ = Class676.aClass676_8644.aByte8648;
	    else if (2 == i_22_)
		i_24_ = Class676.aClass676_8650.aByte8648;
	    class654_sub1_sub5_sub1.aByteArray11979
		[-1763707177 * class654_sub1_sub5_sub1.anInt11980]
		= i_24_;
	    class654_sub1_sub5_sub1.anInt11980 += 990207207;
	}
    }
    
    static final void method3951(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class619.method10256(class247, class243, class669, -148648999);
    }
    
    static final void method3952(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
}
