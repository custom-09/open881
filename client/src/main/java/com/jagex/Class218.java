/* Class218 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class218 implements Interface26
{
    Class213 this$0;
    Class225 aClass225_2303;
    
    public void method172(Class214 class214) {
	class214.method4040(aClass225_2303, (byte) 27);
    }
    
    public void method173(Class214 class214, int i) {
	class214.method4040(aClass225_2303, (byte) 0);
    }
    
    Class218(Class213 class213, Class534_Sub40 class534_sub40) {
	this$0 = class213;
	boolean bool = class534_sub40.method16527(-1957402655) != 255;
	if (bool)
	    class534_sub40.anInt10811 -= -1387468933;
	aClass225_2303 = new Class225(class534_sub40, bool, true);
    }
    
    public void method174(Class214 class214) {
	class214.method4040(aClass225_2303, (byte) -48);
    }
    
    static final void method4117(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 2083357573 * class247.anInt2634;
    }
    
    static void method4118(int i, int i_0_, int i_1_, int i_2_, byte i_3_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18121(-41192509);
	class534_sub18_sub6.anInt11666 = 517206857 * i_0_;
	class534_sub18_sub6.anInt11660 = i_1_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_2_ * -105177451;
    }
}
