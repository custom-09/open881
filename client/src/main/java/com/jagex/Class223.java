/* Class223 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

class Class223 implements Interface42
{
    Class232 this$0;
    static Class16 aClass16_2314;
    static boolean aBool2315;
    public static Class53_Sub2 aClass53_Sub2_2316;
    
    Class223(Class232 class232) {
	this$0 = class232;
    }
    
    public float method330(short i) {
	return ((float) Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub28_10786.method17131(-2020503522)
		/ 255.0F);
    }
    
    public float method331() {
	return ((float) Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub28_10786.method17131(-965111859)
		/ 255.0F);
    }
    
    public static Class699 method4156(byte i) {
	if (null == Class267.aClass699_2940)
	    return Class699.aClass699_8797;
	return Class267.aClass699_2940;
    }
    
    static void method4157(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class280.method5234(1216362536) ? 1 : 0;
    }
    
    static final void method4158(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class581.method9825(class247, class243, class669, 222411395);
    }
    
    static final void method4159(int i, byte i_0_) {
	client.anIntArray11177 = new int[i];
	client.anIntArray11123 = new int[i];
	client.anIntArray11179 = new int[i];
	client.anIntArray11135 = new int[i];
	client.anIntArray11181 = new int[i];
    }
    
    static final void method4160(int i, int i_1_, int i_2_, int i_3_,
				 byte i_4_) {
	Class706_Sub5.method17347(Class254.aClass185_2683, (byte) 0);
	Class544.method8962(i, i_1_, i_2_, i_3_, 256, 256, -2048935883);
	Class347.method6157(i, i_1_, i_2_, i_3_, 256, 256, (byte) -96);
	Class61.method1261(i, i_1_, i_2_, i_3_, 256, 256, (byte) -11);
	Class590.method9880(i, i_1_, i_2_, i_3_, 448328695);
    }
}
