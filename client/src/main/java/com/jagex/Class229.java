/* Class229 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.ArrayList;
import java.util.List;

public class Class229
{
    List aList2323;
    Class232 this$0;
    
    void method4192(Class491 class491, int i) {
	aList2323.add(class491);
    }
    
    Class229(Class232 class232) {
	this$0 = class232;
	aList2323 = new ArrayList();
    }
    
    void method4193(Class491 class491, int i) {
	aList2323.remove(class491);
    }
    
    boolean method4194(Class491 class491, int i) {
	return aList2323.contains(class491);
    }
    
    List method4195(int i) {
	return aList2323;
    }
    
    void method4196(Class491 class491) {
	aList2323.add(class491);
    }
    
    void method4197(Class491 class491) {
	aList2323.add(class491);
    }
    
    void method4198(Class491 class491) {
	aList2323.remove(class491);
    }
    
    List method4199() {
	return aList2323;
    }
    
    List method4200() {
	return aList2323;
    }
    
    boolean method4201(Class491 class491) {
	return aList2323.contains(class491);
    }
    
    static boolean method4202(String string, String string_0_, int i) {
	Class680.anInt8668 = -663357284;
	Class65.aClass100_658 = client.aClass100_11264;
	return Class593.method9900(false, false, string, string_0_, -1L);
    }
    
    public static void method4203(int i, int i_1_, int i_2_, int i_3_) {
	Class185.method3694
	    (new Class71_Sub1(i, i_1_, i_2_, client.anInt11074 * 1262399261,
			      1930739095 * Class6.aClass450_56.anInt4928,
			      Class6.aClass450_56.anInt4929 * 1871597189,
			      Class6.aClass450_56.method7334(375069447),
			      Class6.aClass450_56.method7340((byte) 24),
			      Class645.aClass463_8374.method7523(-374259247)),
	     -1600474947);
    }
    
    static final void method4204(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_4_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_5_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class662.method10990(i_4_, i_5_, -26100619) ? 1 : 0;
    }
    
    public static int method4205(int i) {
	if (-1 == 743618349 * Class50.anInt411) {
	    Class50[] class50s = Class24.method853(-224395528);
	    for (int i_6_ = 0; i_6_ < class50s.length; i_6_++) {
		Class50 class50 = class50s[i_6_];
		if (class50.anInt409 * -389811879
		    > Class50.anInt411 * 743618349)
		    Class50.anInt411 = class50.anInt409 * -1873494435;
	    }
	    Class50.anInt411 += -60444507;
	}
	return Class50.anInt411 * 743618349;
    }
    
    static final void method4206(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	if (class669.anIntArray8595[class669.anInt8600 * 2088438307]
	    <= class669.anIntArray8595[1 + 2088438307 * class669.anInt8600])
	    class669.anInt8613
		+= (-793595371
		    * class669.anIntArray8591[class669.anInt8613 * 662605117]);
    }
    
    static final void method4207(Class534_Sub4 class534_sub4, byte i) {
	Class556 class556 = client.aClass512_11100.method8424((byte) 96);
	if (class556 != null) {
	    Interface62 interface62 = null;
	    if (-1831835741 * class534_sub4.anInt10405 == 0)
		interface62
		    = (Interface62) (class556.method9258
				     (-511427777 * class534_sub4.anInt10402,
				      -1522052283 * class534_sub4.anInt10397,
				      -1246362377 * class534_sub4.anInt10400,
				      (byte) -38));
	    if (-1831835741 * class534_sub4.anInt10405 == 1)
		interface62
		    = (Interface62) (class556.method9357
				     (-511427777 * class534_sub4.anInt10402,
				      class534_sub4.anInt10397 * -1522052283,
				      class534_sub4.anInt10400 * -1246362377,
				      (byte) -1));
	    if (2 == class534_sub4.anInt10405 * -1831835741)
		interface62
		    = (Interface62) (class556.method9262
				     (-511427777 * class534_sub4.anInt10402,
				      class534_sub4.anInt10397 * -1522052283,
				      -1246362377 * class534_sub4.anInt10400,
				      client.anInterface64_11333, (byte) 47));
	    if (class534_sub4.anInt10405 * -1831835741 == 3)
		interface62
		    = (Interface62) (class556.method9264
				     (-511427777 * class534_sub4.anInt10402,
				      -1522052283 * class534_sub4.anInt10397,
				      -1246362377 * class534_sub4.anInt10400,
				      (byte) 59));
	    if (interface62 != null) {
		class534_sub4.anInt10395
		    = interface62.method56(-1169743794) * 1091872063;
		class534_sub4.anInt10401
		    = interface62.method409(-1422322907) * -1023683665;
		class534_sub4.anInt10396
		    = interface62.method410(1759648443) * -600477765;
	    } else {
		class534_sub4.anInt10395 = -1091872063;
		class534_sub4.anInt10401 = 0;
		class534_sub4.anInt10396 = 0;
	    }
	}
    }
    
    static void method4208(int i) {
	Class176.method2929(-2024655746);
	Class554_Sub1.aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	Class554_Sub1.aClass9_10686.method578((byte) -60);
	Class554_Sub1.aClass9_10687.method578((byte) 38);
	for (int i_7_ = 0; i_7_ < 3; i_7_++) {
	    for (int i_8_ = 0; i_8_ < 5; i_8_++) {
		Class554_Sub1.aClass171ArrayArray10678[i_7_][i_8_] = null;
		Class554_Sub1.aClass16ArrayArray10679[i_7_][i_8_] = null;
	    }
	}
    }
}
