/* Class251 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class251
{
    static Class534_Sub18_Sub7 aClass534_Sub18_Sub7_2653;
    
    static final void method4612(long l) {
	try {
	    Thread.sleep(l);
	} catch (InterruptedException interruptedexception) {
	    /* empty */
	}
    }
    
    Class251() throws Throwable {
	throw new Error();
    }
    
    public static final void method4613(long l) {
	if (l > 0L) {
	    if (0L == l % 10L) {
		Class106.method1943(l - 1L);
		Class106.method1943(1L);
	    } else
		Class106.method1943(l);
	}
    }
    
    public static final void method4614(long l) {
	if (l > 0L) {
	    if (0L == l % 10L) {
		Class106.method1943(l - 1L);
		Class106.method1943(1L);
	    } else
		Class106.method1943(l);
	}
    }
    
    public static final void method4615(long l) {
	if (l > 0L) {
	    if (0L == l % 10L) {
		Class106.method1943(l - 1L);
		Class106.method1943(1L);
	    } else
		Class106.method1943(l);
	}
    }
    
    static final void method4616(long l) {
	try {
	    Thread.sleep(l);
	} catch (InterruptedException interruptedexception) {
	    /* empty */
	}
    }
    
    public static final void method4617(long l) {
	if (l > 0L) {
	    if (0L == l % 10L) {
		Class106.method1943(l - 1L);
		Class106.method1943(1L);
	    } else
		Class106.method1943(l);
	}
    }
    
    public static Class419[] method4618(int i) {
	return (new Class419[]
		{ Class419.aClass419_4695, Class419.aClass419_4690,
		  Class419.aClass419_4691, Class419.aClass419_4692,
		  Class419.aClass419_4693, Class419.aClass419_4702,
		  Class419.aClass419_4700, Class419.aClass419_4696,
		  Class419.aClass419_4689, Class419.aClass419_4698,
		  Class419.aClass419_4699, Class419.aClass419_4697,
		  Class419.aClass419_4701, Class419.aClass419_4694,
		  Class419.aClass419_4703 });
    }
    
    static final void method4619(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 290091559 * class247.anInt2482;
    }
    
    static final void method4620(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub9_10748
		  .method16958(-1848453657) == 1 ? 1 : 0;
    }
    
    static final void method4621(Class669 class669, byte i) {
	class669.anInt8600 -= 1235998252;
	int i_0_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_1_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	int i_2_
	    = class669.anIntArray8595[2 + 2088438307 * class669.anInt8600];
	int i_3_
	    = class669.anIntArray8595[3 + class669.anInt8600 * 2088438307];
	int i_4_ = 256;
	Class171_Sub4.aClass232_9944.method4237(Class211.aClass211_2255, i_0_,
						i_1_, i_3_,
						Class190.aClass190_2134
						    .method3763(-1529868449),
						Class207.aClass207_2235, 0.0F,
						0.0F, null, 0, i_4_, i_2_,
						(byte) 10);
    }
    
    public static final void method4622(long l) {
	if (l > 0L) {
	    if (0L == l % 10L) {
		Class106.method1943(l - 1L);
		Class106.method1943(1L);
	    } else
		Class106.method1943(l);
	}
    }
}
