/* Class255 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class255 implements Interface76
{
    int anInt2684;
    static Class255 aClass255_2685 = new Class255(12, 100);
    static Class255 aClass255_2686;
    static Class255 aClass255_2687;
    static Class255 aClass255_2688;
    int anInt2689;
    static Class255 aClass255_2690;
    static Class255 aClass255_2691;
    static Class255 aClass255_2692;
    static Class255 aClass255_2693;
    static Class255 aClass255_2694;
    static Class255 aClass255_2695;
    static Class255 aClass255_2696;
    static Class255 aClass255_2697;
    static Class255 aClass255_2698;
    static Class255 aClass255_2699;
    static Class255 aClass255_2700;
    static Class255 aClass255_2701;
    static Class255 aClass255_2702 = new Class255(11, 101);
    static Class255 aClass255_2703;
    
    public int method93() {
	return anInt2689 * 571164647;
    }
    
    public int method22() {
	return anInt2689 * 571164647;
    }
    
    static {
	aClass255_2686 = new Class255(4, 102);
	aClass255_2687 = new Class255(5, 103);
	aClass255_2693 = new Class255(10, 104);
	aClass255_2703 = new Class255(6, 105);
	aClass255_2690 = new Class255(0, 106);
	aClass255_2691 = new Class255(2, 107);
	aClass255_2692 = new Class255(1, 108);
	aClass255_2699 = new Class255(17, 109);
	aClass255_2694 = new Class255(3, 110);
	aClass255_2688 = new Class255(14, 111);
	aClass255_2696 = new Class255(15, 112);
	aClass255_2697 = new Class255(16, 113);
	aClass255_2698 = new Class255(13, 114);
	aClass255_2695 = new Class255(8, 115);
	aClass255_2700 = new Class255(9, 116);
	aClass255_2701 = new Class255(7, 117);
    }
    
    public int method53() {
	return anInt2689 * 571164647;
    }
    
    Class255(int i, int i_0_) {
	anInt2684 = i * -539436657;
	anInt2689 = 1566366679 * i_0_;
    }
    
    static Class255[] method4643() {
	return (new Class255[]
		{ aClass255_2695, aClass255_2692, aClass255_2699,
		  aClass255_2700, aClass255_2691, aClass255_2690,
		  aClass255_2694, aClass255_2686, aClass255_2685,
		  aClass255_2687, aClass255_2696, aClass255_2698,
		  aClass255_2693, aClass255_2703, aClass255_2701,
		  aClass255_2697, aClass255_2702, aClass255_2688 });
    }
    
    static Class255[] method4644() {
	return (new Class255[]
		{ aClass255_2695, aClass255_2692, aClass255_2699,
		  aClass255_2700, aClass255_2691, aClass255_2690,
		  aClass255_2694, aClass255_2686, aClass255_2685,
		  aClass255_2687, aClass255_2696, aClass255_2698,
		  aClass255_2693, aClass255_2703, aClass255_2701,
		  aClass255_2697, aClass255_2702, aClass255_2688 });
    }
    
    public static Class293 method4645(int i, int i_1_) {
	if (-2097182691 * Class293.aClass293_3131.anInt3126 == i)
	    return Class293.aClass293_3131;
	if (-2097182691 * Class293.aClass293_3125.anInt3126 == i)
	    return Class293.aClass293_3125;
	if (i == -2097182691 * Class293.aClass293_3128.anInt3126)
	    return Class293.aClass293_3128;
	if (i == -2097182691 * Class293.aClass293_3124.anInt3126)
	    return Class293.aClass293_3124;
	if (i == Class293.aClass293_3127.anInt3126 * -2097182691)
	    return Class293.aClass293_3127;
	if (Class293.aClass293_3129.anInt3126 * -2097182691 == i)
	    return Class293.aClass293_3129;
	if (i == -2097182691 * Class293.aClass293_3130.anInt3126)
	    return Class293.aClass293_3130;
	return null;
    }
    
    public static int method4646(int i) {
	return Class277.aClass300_3049.method5519((byte) -90);
    }
    
    static final int method4647(int i, byte i_2_) {
	if (i < 4)
	    return 0;
	if (i < 10)
	    return i - 3;
	return i - 6;
    }
    
    public static boolean method4648(int i, byte i_3_) {
	return (i >= Class595.aClass595_7843.anInt7852 * 847393323
		&& i <= 847393323 * Class595.aClass595_7848.anInt7852);
    }
    
    static final void method4649(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	if (Class599.aClass298_Sub1_7871.method5425(1957526732)
	    != Class293.aClass293_3124)
	    throw new RuntimeException();
	((Class706_Sub3) Class599.aClass298_Sub1_7871.method5381(1420822791))
	    .method17273
	    (class669.anIntArray8595[class669.anInt8600 * 2088438307],
	     class669.anIntArray8595[1 + 2088438307 * class669.anInt8600],
	     class669.anIntArray8595[class669.anInt8600 * 2088438307 + 2],
	     (byte) 43);
    }
    
    public static void method4650(int i) {
	Class662.aClass199_8552.method3837(1074959591);
    }
    
    static boolean method4651(int i, int i_4_) {
	return (59 == i || i == 2 || 8 == i || i == 17 || 15 == i || 16 == i
		|| i == 58);
    }
    
    static final void method4652(Class669 class669, int i) {
	class669.anInt8600 -= 1853997378;
	Class534_Sub36 class534_sub36
	    = ((Class534_Sub36)
	       (class669.anObjectArray8593
		[(class669.anInt8594 -= 1460193483) * 1485266147]));
	if (Class599.aClass298_Sub1_7871.method5425(1861444914)
	    != Class293.aClass293_3124)
	    throw new RuntimeException();
	((Class706_Sub3) Class599.aClass298_Sub1_7871.method5381(1793072832))
	    .method17275
	    (class534_sub36.method16483(-1760184947),
	     class669.anIntArray8595[class669.anInt8600 * 2088438307],
	     class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1],
	     class669.anIntArray8595[class669.anInt8600 * 2088438307 + 2],
	     class669.anIntArray8595[3 + 2088438307 * class669.anInt8600],
	     class669.anIntArray8595[class669.anInt8600 * 2088438307 + 4],
	     class669.anIntArray8595[5 + class669.anInt8600 * 2088438307],
	     (byte) 1);
    }
    
    static final void method4653(Class247 class247, Class243 class243,
				 Class669 class669, byte i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int[] is = Class546.method8989(string, class669, -219410121);
	if (null != is)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2444
	    = Class99.method1859(string, class669, 1273306737);
	class247.anIntArray2581 = is;
	class247.aBool2561 = true;
    }
}
