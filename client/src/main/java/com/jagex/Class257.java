/* Class257 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class257
{
    public static Class257 method4670(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(-197384075);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_0_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_1_ = 0; i_1_ < i; i_1_++) {
	    int i_2_ = class534_sub40.method16527(1287292744);
	    if (i_2_ == 0)
		objects[i_1_]
		    = Class483.method7937
			  (java.lang.Integer.class, 1873719409)
			  .method75(class534_sub40, -1929265476);
	    else if (i_2_ == 1)
		objects[i_1_]
		    = Class483.method7937
			  (java.lang.String.class, 294344505)
			  .method75(class534_sub40, 827288864);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_2_).toString());
	}
	return new Class257(i_0_, objects);
    }
    
    public static Class257 method4671(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(55573774);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_3_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_4_ = 0; i_4_ < i; i_4_++) {
	    int i_5_ = class534_sub40.method16527(-1780406533);
	    if (i_5_ == 0)
		objects[i_4_]
		    = Class483.method7937
			  (java.lang.Integer.class, 1135282028)
			  .method75(class534_sub40, -1602071529);
	    else if (i_5_ == 1)
		objects[i_4_]
		    = Class483.method7937
			  (java.lang.String.class, 1101560665)
			  .method75(class534_sub40, 442299824);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_5_).toString());
	}
	return new Class257(i_3_, objects);
    }
    
    public static Class257 method4672(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(-576369614);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_6_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_7_ = 0; i_7_ < i; i_7_++) {
	    int i_8_ = class534_sub40.method16527(-1486127140);
	    if (i_8_ == 0)
		objects[i_7_]
		    = Class483.method7937
			  (java.lang.Integer.class, 1258360775)
			  .method75(class534_sub40, -1708598756);
	    else if (i_8_ == 1)
		objects[i_7_]
		    = Class483.method7937
			  (java.lang.String.class, 476594590)
			  .method75(class534_sub40, -60006201);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_8_).toString());
	}
	return new Class257(i_6_, objects);
    }
    
    public static Class257 method4673(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(466518312);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_9_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_10_ = 0; i_10_ < i; i_10_++) {
	    int i_11_ = class534_sub40.method16527(-1308510662);
	    if (i_11_ == 0)
		objects[i_10_]
		    = Class483.method7937
			  (java.lang.Integer.class, 1223253584)
			  .method75(class534_sub40, 681864652);
	    else if (i_11_ == 1)
		objects[i_10_]
		    = Class483.method7937
			  (java.lang.String.class, 1237366882)
			  .method75(class534_sub40, 523445630);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_11_).toString());
	}
	return new Class257(i_9_, objects);
    }
    
    public static Class257 method4674(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(1886583359);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_12_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_13_ = 0; i_13_ < i; i_13_++) {
	    int i_14_ = class534_sub40.method16527(1930918827);
	    if (i_14_ == 0)
		objects[i_13_]
		    = Class483.method7937
			  (java.lang.Integer.class, 963302487)
			  .method75(class534_sub40, -226515351);
	    else if (i_14_ == 1)
		objects[i_13_]
		    = Class483.method7937
			  (java.lang.String.class, 505561256)
			  .method75(class534_sub40, -1186143897);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_14_).toString());
	}
	return new Class257(i_12_, objects);
    }
    
    Class257(int i, Object[] objects) {
	/* empty */
    }
    
    public static Class257 method4675(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16527(987688560);
	if (0 == i)
	    return null;
	i--;
	class534_sub40.anInt10811 += -1387468933;
	int i_15_ = class534_sub40.method16533(-258848859);
	Object[] objects = new Object[i];
	for (int i_16_ = 0; i_16_ < i; i_16_++) {
	    int i_17_ = class534_sub40.method16527(-976358511);
	    if (i_17_ == 0)
		objects[i_16_]
		    = Class483.method7937
			  (java.lang.Integer.class, 491290452)
			  .method75(class534_sub40, -558390500);
	    else if (i_17_ == 1)
		objects[i_16_]
		    = Class483.method7937
			  (java.lang.String.class, 220094962)
			  .method75(class534_sub40, -1721311924);
	    else
		throw new IllegalStateException
			  (new StringBuilder().append
			       ("Unrecognised type ID in deserialise: ").append
			       (i_17_).toString());
	}
	return new Class257(i_15_, objects);
    }
    
    static void method4676(Class669 class669, int i) {
	class669.anIntArray8595[2088438307 * class669.anInt8600 - 2]
	    = (((Class273)
		Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						      [(class669.anInt8600
							* 2088438307) - 2]),
						     -465553934))
		   .method5081
	       (Class78.aClass103_825,
		class669.anIntArray8595[2088438307 * class669.anInt8600 - 1],
		(byte) 66)) ? 1 : 0;
	class669.anInt8600 -= 308999563;
    }
}
