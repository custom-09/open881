/* Class277 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class277
{
    public String aString3044;
    public Class276[] aClass276Array3045;
    public String aString3046;
    public Class276[] aClass276Array3047;
    public String aString3048;
    public static Class300 aClass300_3049;
    
    void method5159(Class263 class263) {
	aString3044 = class263.method4826(543578984);
	aString3048 = class263.method4826(1634705699);
	aString3046 = class263.method4826(385090825);
	int i = class263.method4830(2088438307);
	int i_0_ = class263.method4830(2088438307);
	aClass276Array3047 = 0 == i ? null : new Class276[i];
	aClass276Array3045 = i_0_ == 0 ? null : new Class276[i_0_];
	for (int i_1_ = 0; i_1_ < i; i_1_++) {
	    aClass276Array3047[i_1_] = new Class276();
	    aClass276Array3047[i_1_].method5153(class263, (short) 6491);
	}
	for (int i_2_ = 0; i_2_ < i_0_; i_2_++) {
	    aClass276Array3045[i_2_] = new Class276();
	    aClass276Array3045[i_2_].method5153(class263, (short) 28663);
	}
    }
    
    void method5160(Class263 class263, int i) {
	aString3044 = class263.method4826(-560246170);
	aString3048 = class263.method4826(-51391163);
	aString3046 = class263.method4826(1307029690);
	int i_3_ = class263.method4830(2088438307);
	int i_4_ = class263.method4830(2088438307);
	aClass276Array3047 = 0 == i_3_ ? null : new Class276[i_3_];
	aClass276Array3045 = i_4_ == 0 ? null : new Class276[i_4_];
	for (int i_5_ = 0; i_5_ < i_3_; i_5_++) {
	    aClass276Array3047[i_5_] = new Class276();
	    aClass276Array3047[i_5_].method5153(class263, (short) 12885);
	}
	for (int i_6_ = 0; i_6_ < i_4_; i_6_++) {
	    aClass276Array3045[i_6_] = new Class276();
	    aClass276Array3045[i_6_].method5153(class263, (short) 24859);
	}
    }
    
    Class277() {
	/* empty */
    }
    
    static final void method5161(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2517
	    = Class99.method1859(string, class669, 321989922);
	class247.aBool2561 = true;
    }
    
    static final void method5162(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aBool11050 ? 1 : 0;
    }
    
    public static void method5163(String string, int i) {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4155,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1443436946);
	    int i_7_ = (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			* 31645619);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -565780277);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(Class40.anIntArray309, i_7_,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i_7_,
		 -424113075);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 39);
	    Class262.aClass703_2800 = Class703.aClass703_8817;
	}
    }
    
    static void method5164(int i, String string, boolean bool, byte i_8_) {
	Class607.method10061(-697030303);
	Class305.method5605(1173290470);
	Class264.method4840((byte) 31);
	Class150.method2490(i, string, bool, (byte) -29);
	Class351.aClass406_3620.method6646(2106812598);
	Class351.aClass406_3620.method6645(client.anInterface52_11081,
					   -1482997381);
	Class351.aClass406_3620.method6652((byte) 1);
	Class78.method1620(Class254.aClass185_2683, -1551344217);
	Class603.method10028(Class254.aClass185_2683, Class464.aClass472_5113,
			     -47788837);
	Class265.method4856(969278507);
	Class422.method6785((byte) -61);
	Class635.method10538(-1052006576);
	if (client.anInt11039 * -1850530127 == 15)
	    Class673.method11110(10, -783619955);
	else if (9 == -1850530127 * client.anInt11039)
	    Class673.method11110(17, -56527528);
	else if (client.anInt11039 * -1850530127 == 7)
	    Class673.method11110(6, 449499814);
	else if (16 == client.anInt11039 * -1850530127)
	    Class673.method11110(3, 393433394);
	else if (11 == client.anInt11039 * -1850530127
		 || client.anInt11039 * -1850530127 == 4)
	    Class555.method9220(-106285826);
	else if (18 == client.anInt11039 * -1850530127)
	    Class673.method11110(0, -1724020065);
	else if (-1850530127 * client.anInt11039 == 3)
	    Class278.method5224(false, -1568446436);
    }
    
    public static void method5165(int i, short i_9_) {
	if (-1850530127 * client.anInt11039 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4177,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i,
								  1244327899);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 124);
	}
    }
}
