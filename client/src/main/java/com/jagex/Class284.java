/* Class284 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class284 implements Interface76
{
    public int anInt3069;
    public static Class284 aClass284_3070 = new Class284(2, 0);
    static Class284 aClass284_3071;
    public static Class284 aClass284_3072 = new Class284(1, 1);
    int anInt3073;
    
    static Class284[] method5252() {
	return (new Class284[]
		{ aClass284_3071, aClass284_3072, aClass284_3070 });
    }
    
    public int method93() {
	return 1588088039 * anInt3073;
    }
    
    public int method53() {
	return 1588088039 * anInt3073;
    }
    
    public int method22() {
	return 1588088039 * anInt3073;
    }
    
    Class284(int i, int i_0_) {
	anInt3069 = 1499465701 * i;
	anInt3073 = 353765079 * i_0_;
    }
    
    static Class284[] method5253() {
	return (new Class284[]
		{ aClass284_3071, aClass284_3072, aClass284_3070 });
    }
    
    static {
	aClass284_3071 = new Class284(0, 2);
    }
    
    static Class284[] method5254() {
	return (new Class284[]
		{ aClass284_3071, aClass284_3072, aClass284_3070 });
    }
    
    static Class640[] method5255(byte i) {
	return (new Class640[]
		{ Class640.aClass640_8319, Class640.aClass640_8326,
		  Class640.aClass640_8321, Class640.aClass640_8328,
		  Class640.aClass640_8329, Class640.aClass640_8320,
		  Class640.aClass640_8332, Class640.aClass640_8324,
		  Class640.aClass640_8322, Class640.aClass640_8331,
		  Class640.aClass640_8327, Class640.aClass640_8330,
		  Class640.aClass640_8323, Class640.aClass640_8325,
		  Class640.aClass640_8318 });
    }
}
