/* Class289 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class289
{
    static Class289 aClass289_3090;
    static Class289 aClass289_3091;
    static Class289 aClass289_3092;
    static Class289 aClass289_3093;
    static Class289 aClass289_3094;
    static Class289 aClass289_3095;
    static Class289 aClass289_3096;
    static Class289 aClass289_3097;
    static Class289 aClass289_3098;
    static Class289 aClass289_3099;
    static Class289 aClass289_3100;
    static Class289 aClass289_3101 = new Class289(0);
    static Class289 aClass289_3102;
    static Class289 aClass289_3103;
    static Class289 aClass289_3104;
    int anInt3105;
    public static Class472 aClass472_3106;
    
    Class289(int i) {
	anInt3105 = 734676979 * i;
    }
    
    static {
	aClass289_3091 = new Class289(1);
	aClass289_3092 = new Class289(2);
	aClass289_3093 = new Class289(3);
	aClass289_3094 = new Class289(4);
	aClass289_3090 = new Class289(5);
	aClass289_3096 = new Class289(6);
	aClass289_3097 = new Class289(7);
	aClass289_3102 = new Class289(8);
	aClass289_3099 = new Class289(9);
	aClass289_3098 = new Class289(10);
	aClass289_3100 = new Class289(11);
	aClass289_3103 = new Class289(12);
	aClass289_3095 = new Class289(13);
	aClass289_3104 = new Class289(14);
    }
    
    static final void method5280(Class669 class669, int i) {
	Class465.method7571(143547356);
	Class177.method2934((short) 21820);
	client.aClass512_11100.method8441(1087900906);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static final void method5281(int i, int i_0_, int i_1_, int i_2_, int i_3_,
				 int i_4_, int i_5_, byte i_6_) {
	if (-1468443459 * client.anInt11155 == 1) {
	    int i_7_ = -1843550713 * Class108.anInt1321;
	    int[] is = Class108.anIntArray1322;
	    for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
		Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		    = client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[is[i_8_]];
		if (null != class654_sub1_sub5_sub1_sub2)
		    class654_sub1_sub5_sub1_sub2.method18547(i, i_0_, i_1_,
							     i_2_, i_3_, i_4_,
							     i_5_, 530695901);
	    }
	    for (int i_9_ = 0; i_9_ < client.anInt11321 * -1125820437;
		 i_9_++) {
		int i_10_ = client.anIntArray11088[i_9_];
		Class534_Sub6 class534_sub6
		    = ((Class534_Sub6)
		       client.aClass9_11331.method579((long) i_10_));
		if (null != class534_sub6)
		    ((Class654_Sub1_Sub5_Sub1) class534_sub6.anObject10417)
			.method18547
			(i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, 275113829);
	    }
	}
    }
    
    static final void method5282(Class669 class669, int i) {
	int i_11_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_11_, 1655803357);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_11_ >> 16];
	Class534_Sub20.method16195(class247, class243, true, 1, class669,
				   223840164);
    }
    
    static Class280[] method5283(byte i) {
	return (new Class280[]
		{ Class280.aClass280_3054, Class280.aClass280_3055,
		  Class280.aClass280_3056 });
    }
}
