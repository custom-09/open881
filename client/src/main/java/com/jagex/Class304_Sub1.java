/* Class304_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class304_Sub1 extends Class304
{
    public Class method61() {
	return com.jagex.Class307.class;
    }
    
    Class304_Sub1(boolean bool, Class472 class472, Class672 class672,
		  Class675 class675) {
	super(bool, class472, class672, class675);
    }
    
    public Class method59(short i) {
	return com.jagex.Class307.class;
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class307(i, this, interface14);
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class307(i, this, interface14);
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class307(i, this, interface14);
    }
    
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class307(i, this, interface14);
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class307(i, this, interface14);
    }
}
