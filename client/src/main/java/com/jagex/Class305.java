/* Class305 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.applet.Applet;

public class Class305
{
    static Class305 aClass305_3264;
    public static Class305 aClass305_3265;
    public static Class305 aClass305_3266 = new Class305(0, false);
    static Class305 aClass305_3267;
    static Class305 aClass305_3268;
    public int anInt3269;
    boolean aBool3270;
    public static Applet anApplet3271;
    public static byte[][] aByteArrayArray3272;
    
    public static Class305 method5599(int i) {
	if (-1861794503 * aClass305_3266.anInt3269 == i)
	    return aClass305_3266;
	if (i == aClass305_3265.anInt3269 * -1861794503)
	    return aClass305_3265;
	if (-1861794503 * aClass305_3268.anInt3269 == i)
	    return aClass305_3268;
	if (-1861794503 * aClass305_3267.anInt3269 == i)
	    return aClass305_3267;
	if (aClass305_3264.anInt3269 * -1861794503 == i)
	    return aClass305_3264;
	return null;
    }
    
    public boolean method5600(int i) {
	return aBool3270;
    }
    
    static {
	aClass305_3265 = new Class305(1, false);
	aClass305_3268 = new Class305(2, true);
	aClass305_3267 = new Class305(3, true);
	aClass305_3264 = new Class305(4, true);
    }
    
    public static Class305 method5601(int i) {
	if (-1861794503 * aClass305_3266.anInt3269 == i)
	    return aClass305_3266;
	if (i == aClass305_3265.anInt3269 * -1861794503)
	    return aClass305_3265;
	if (-1861794503 * aClass305_3268.anInt3269 == i)
	    return aClass305_3268;
	if (-1861794503 * aClass305_3267.anInt3269 == i)
	    return aClass305_3267;
	if (aClass305_3264.anInt3269 * -1861794503 == i)
	    return aClass305_3264;
	return null;
    }
    
    public boolean method5602() {
	return aBool3270;
    }
    
    public boolean method5603() {
	return aBool3270;
    }
    
    Class305(int i, boolean bool) {
	anInt3269 = 1423710473 * i;
	aBool3270 = bool;
    }
    
    static final void method5604(int i) {
	Class65.aLong663 = 8972729624098644529L;
	Class65.aLong693 = 0L;
	Class65.anInt662 = -1651628635;
    }
    
    static void method5605(int i) {
	if (Class254.aClass185_2683 != null) {
	    client.aClass512_11100.method8455(2129664635);
	    Class561.method9444((byte) -125);
	    Class265.method4856(969278507);
	    Class469.method7636((byte) -67);
	    Class210.method3953((byte) 1);
	    client.aClass512_11100.method8437(true, 1296588206);
	    Class151.method2543(1791049254);
	    Class606.method10051(657955984);
	    Class11.method611(false, -1843002554);
	    Class538.method8922(2020004863);
	    Class316.method5725((byte) 3);
	    Class555.method9220(-106285826);
	    for (int i_0_ = 0; i_0_ < client.aClass530Array11054.length;
		 i_0_++) {
		if (client.aClass530Array11054[i_0_] != null)
		    client.aClass530Array11054[i_0_].aClass183_7127 = null;
	    }
	    for (int i_1_ = 0; i_1_ < 2048; i_1_++) {
		Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		    = client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[i_1_];
		if (null != class654_sub1_sub5_sub1_sub2) {
		    for (int i_2_ = 0;
			 i_2_ < (class654_sub1_sub5_sub1_sub2
				 .aClass183Array11986).length;
			 i_2_++)
			class654_sub1_sub5_sub1_sub2.aClass183Array11986[i_2_]
			    = null;
		}
	    }
	    for (int i_3_ = 0; i_3_ < client.anInt11148 * 759971875; i_3_++) {
		Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
		    = ((Class654_Sub1_Sub5_Sub1_Sub1)
		       client.aClass534_Sub6Array11085[i_3_].anObject10417);
		if (null != class654_sub1_sub5_sub1_sub1) {
		    for (int i_4_ = 0;
			 i_4_ < (class654_sub1_sub5_sub1_sub1
				 .aClass183Array11986).length;
			 i_4_++)
			class654_sub1_sub5_sub1_sub1.aClass183Array11986[i_4_]
			    = null;
		}
	    }
	    client.aClass9_11322.method578((byte) 91);
	    Class254.aClass185_2683.method3236(-568376843);
	    Class254.aClass185_2683 = null;
	}
    }
    
    static final void method5606(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_5_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_6_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	Class90 class90 = (Class90) Class534_Sub11_Sub13
					.aClass44_Sub22_11730
					.method91(i_6_, 1532628889);
	if (class90.method1718(-71384406))
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= ((Class602) client.aClass512_11100.method8428
				  (-1486655428).method91(i_5_, -222520709))
		      .method9990(i_6_, class90.aString889, (byte) 44);
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= (((Class602) client.aClass512_11100.method8428
				   (-1486655428).method91(i_5_, 1384029032))
		       .method9989
		   (i_6_, class90.anInt888 * 263946597, (byte) 66));
    }
}
