/* Class312_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class312_Sub2 extends Class312
{
    Class163 aClass163_10061;
    
    public boolean method204() {
	if (!super.method199(-1636650952))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -89);
    }
    
    public void method201(short i) {
	super.method201((short) -15202);
	aClass163_10061
	    = Class243.method4480(aClass472_3367,
				  (((Class394_Sub2) aClass394_3366).anInt10158
				   * 868775843),
				  2033833897);
    }
    
    void method5673(boolean bool, int i, int i_0_, byte i_1_) {
	int i_2_ = (method5674((byte) 97)
		    * (-1607607997 * aClass394_3366.anInt4097) / 10000);
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i, 2 + i_0_, i + i_2_,
					   (aClass394_3366.anInt4098
					    * -228886179) + i_0_);
	aClass163_10061.method2662(i, i_0_ + 2,
				   aClass394_3366.anInt4097 * -1607607997,
				   aClass394_3366.anInt4098 * -228886179);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    public boolean method207() {
	if (!super.method199(14192614))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -61);
    }
    
    public boolean method199(int i) {
	if (!super.method199(-513258112))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -26);
    }
    
    public void method203() {
	super.method201((short) -12307);
	aClass163_10061
	    = Class243.method4480(aClass472_3367,
				  (((Class394_Sub2) aClass394_3366).anInt10158
				   * 868775843),
				  -6895229);
    }
    
    void method5672(boolean bool, int i, int i_3_, short i_4_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_3_, -1607607997 * aClass394_3366.anInt4097 + 4,
	     2 + aClass394_3366.anInt4098 * -228886179,
	     -1419672847 * ((Class394_Sub2) aClass394_3366).anInt10159, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_3_ + 1, aClass394_3366.anInt4097 * -1607607997 + 2,
	     aClass394_3366.anInt4098 * -228886179, 0, 0);
    }
    
    public boolean method208() {
	if (!super.method199(817586382))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -119);
    }
    
    Class312_Sub2(Class472 class472, Class472 class472_5_,
		  Class394_Sub2 class394_sub2) {
	super(class472, class472_5_, (Class394) class394_sub2);
    }
    
    void method5676(boolean bool, int i, int i_6_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_6_, -1607607997 * aClass394_3366.anInt4097 + 4,
	     2 + aClass394_3366.anInt4098 * -228886179,
	     -1419672847 * ((Class394_Sub2) aClass394_3366).anInt10159, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_6_ + 1, aClass394_3366.anInt4097 * -1607607997 + 2,
	     aClass394_3366.anInt4098 * -228886179, 0, 0);
    }
    
    public boolean method206() {
	if (!super.method199(-1048820740))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -122);
    }
    
    public boolean method200() {
	if (!super.method199(-333863577))
	    return false;
	return aClass472_3367.method7670((((Class394_Sub2) aClass394_3366)
					  .anInt10158) * 868775843,
					 (byte) -91);
    }
    
    void method5671(boolean bool, int i, int i_7_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_7_, -1607607997 * aClass394_3366.anInt4097 + 4,
	     2 + aClass394_3366.anInt4098 * -228886179,
	     -1419672847 * ((Class394_Sub2) aClass394_3366).anInt10159, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_7_ + 1, aClass394_3366.anInt4097 * -1607607997 + 2,
	     aClass394_3366.anInt4098 * -228886179, 0, 0);
    }
    
    void method5677(boolean bool, int i, int i_8_) {
	int i_9_ = (method5674((byte) 18)
		    * (-1607607997 * aClass394_3366.anInt4097) / 10000);
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i, 2 + i_8_, i + i_9_,
					   (aClass394_3366.anInt4098
					    * -228886179) + i_8_);
	aClass163_10061.method2662(i, i_8_ + 2,
				   aClass394_3366.anInt4097 * -1607607997,
				   aClass394_3366.anInt4098 * -228886179);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    void method5678(boolean bool, int i, int i_10_) {
	int i_11_ = (method5674((byte) 45)
		     * (-1607607997 * aClass394_3366.anInt4097) / 10000);
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i, 2 + i_10_, i + i_11_,
					   (aClass394_3366.anInt4098
					    * -228886179) + i_10_);
	aClass163_10061.method2662(i, i_10_ + 2,
				   aClass394_3366.anInt4097 * -1607607997,
				   aClass394_3366.anInt4098 * -228886179);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    void method5675(boolean bool, int i, int i_12_) {
	int i_13_ = (method5674((byte) 39)
		     * (-1607607997 * aClass394_3366.anInt4097) / 10000);
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i, 2 + i_12_, i + i_13_,
					   (aClass394_3366.anInt4098
					    * -228886179) + i_12_);
	aClass163_10061.method2662(i, i_12_ + 2,
				   aClass394_3366.anInt4097 * -1607607997,
				   aClass394_3366.anInt4098 * -228886179);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    static final void method15670(Class669 class669, short i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class596.method9916(class247, class243, class669, (byte) 5);
    }
}
