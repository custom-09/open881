/* Class319 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class319 implements Interface32
{
    Class390 aClass390_3399;
    Class472 aClass472_3400;
    Class472 aClass472_3401;
    Class171 aClass171_3402;
    
    public void method203() {
	Class16 class16
	    = Class20.method804(aClass472_3401,
				aClass390_3399.anInt4070 * -835082531,
				Class351.aClass406_3620, -103210469);
	aClass171_3402
	    = (Class254.aClass185_2683.method3325
	       (class16,
		Class178.method2942(aClass472_3400,
				    -835082531 * aClass390_3399.anInt4070),
		true));
    }
    
    Class319(Class472 class472, Class472 class472_0_, Class390 class390) {
	aClass390_3399 = class390;
	aClass472_3400 = class472;
	aClass472_3401 = class472_0_;
    }
    
    public void method205(boolean bool, byte i) {
	if (bool) {
	    int i_1_ = ((aClass390_3399.aClass401_4061.method6586
			 (-124707271 * aClass390_3399.anInt4068,
			  -321474631 * client.anInt11047, -1879063956))
			+ aClass390_3399.anInt4065 * 102567979);
	    int i_2_ = ((aClass390_3399.aClass391_4062.method6544
			 (-408316691 * aClass390_3399.anInt4069,
			  client.anInt11192 * 43072843, (byte) 8))
			+ aClass390_3399.anInt4064 * -764465043);
	    aClass171_3402.method2844(aClass390_3399.aString4060, i_1_, i_2_,
				      -124707271 * aClass390_3399.anInt4068,
				      -408316691 * aClass390_3399.anInt4069,
				      462778561 * aClass390_3399.anInt4071,
				      -213434005 * aClass390_3399.anInt4072,
				      -331314539 * aClass390_3399.anInt4063,
				      524456983 * aClass390_3399.anInt4066,
				      -905608091 * aClass390_3399.anInt4067,
				      null, null, null, 0, 0, 202025040);
	}
    }
    
    public boolean method199(int i) {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -127))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -64))
	    bool = false;
	return bool;
    }
    
    public void method202(boolean bool) {
	if (bool) {
	    int i = ((aClass390_3399.aClass401_4061.method6586
		      (-124707271 * aClass390_3399.anInt4068,
		       -321474631 * client.anInt11047, -1232674754))
		     + aClass390_3399.anInt4065 * 102567979);
	    int i_3_ = ((aClass390_3399.aClass391_4062.method6544
			 (-408316691 * aClass390_3399.anInt4069,
			  client.anInt11192 * 43072843, (byte) 34))
			+ aClass390_3399.anInt4064 * -764465043);
	    aClass171_3402.method2844(aClass390_3399.aString4060, i, i_3_,
				      -124707271 * aClass390_3399.anInt4068,
				      -408316691 * aClass390_3399.anInt4069,
				      462778561 * aClass390_3399.anInt4071,
				      -213434005 * aClass390_3399.anInt4072,
				      -331314539 * aClass390_3399.anInt4063,
				      524456983 * aClass390_3399.anInt4066,
				      -905608091 * aClass390_3399.anInt4067,
				      null, null, null, 0, 0, 202025040);
	}
    }
    
    public boolean method207() {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -71))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -89))
	    bool = false;
	return bool;
    }
    
    public boolean method204() {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -119))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -59))
	    bool = false;
	return bool;
    }
    
    public boolean method208() {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -52))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -43))
	    bool = false;
	return bool;
    }
    
    public void method201(short i) {
	Class16 class16
	    = Class20.method804(aClass472_3401,
				aClass390_3399.anInt4070 * -835082531,
				Class351.aClass406_3620, -103210469);
	aClass171_3402
	    = (Class254.aClass185_2683.method3325
	       (class16,
		Class178.method2942(aClass472_3400,
				    -835082531 * aClass390_3399.anInt4070),
		true));
    }
    
    public boolean method200() {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -76))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -95))
	    bool = false;
	return bool;
    }
    
    public boolean method206() {
	boolean bool = true;
	if (!aClass472_3400.method7670(aClass390_3399.anInt4070 * -835082531,
				       (byte) -94))
	    bool = false;
	if (!aClass472_3401.method7670(-835082531 * aClass390_3399.anInt4070,
				       (byte) -15))
	    bool = false;
	return bool;
    }
    
    static final void method5759(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class247.aBool2486 = 1 == i_4_;
	Class454.method7416(class247, -1152466908);
    }
    
    static final void method5760(Class669 class669, int i) {
	int i_5_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_5_, 1739012784);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_5_ >> 16];
	Class690_Sub1.method16830(class247, class243, class669, -555127875);
    }
    
    static final void method5761(Class669 class669, int i) {
	int i_6_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_6_, 438505115);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1033849275 * class247.anInt2502;
    }
    
    static final void method5762(Class669 class669, int i) {
	client.aBool11015 = true;
	Class306.method5607((byte) -58);
    }
    
    static final void method5763(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1816697759 * Class221.anInt2310;
    }
}
