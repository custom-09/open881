/* Class320 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class320
{
    public Interface38 anInterface38_3403;
    public int anInt3404;
    public Interface38 anInterface38_3405;
    public Class433 aClass433_3406;
    protected Class185_Sub1 aClass185_Sub1_3407;
    public Class433 aClass433_3408;
    public Class433 aClass433_3409 = new Class433();
    public Interface37 anInterface37_3410;
    public Class349 aClass349_3411;
    
    public abstract void method5764();
    
    public abstract void method5765(int i, int i_0_);
    
    public abstract void method5766(int i, int i_1_);
    
    public abstract void method5767();
    
    public abstract void method5768();
    
    public abstract void method5769();
    
    public abstract void method5770();
    
    Class320(Class185_Sub1 class185_sub1) {
	aClass433_3406 = new Class433();
	aClass433_3408 = new Class433();
	aClass185_Sub1_3407 = class185_sub1;
    }
    
    public abstract void method5771(int i, int i_2_);
}
