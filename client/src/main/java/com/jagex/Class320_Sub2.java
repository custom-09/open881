/* Class320_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class320_Sub2 extends Class320
{
    void method15708() {
	aClass185_Sub1_3407.method14739(aClass433_3409);
	aClass185_Sub1_3407.method14894(Class433.aClass433_4854,
					Class433.aClass433_4854,
					aClass433_3409);
	aClass185_Sub1_3407.method14668(0);
	aClass185_Sub1_3407.method14669(anInterface38_3403);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14599(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3406);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
    }
    
    void method15709() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    void method15710() {
	aClass185_Sub1_3407.method14739(aClass433_3409);
	aClass185_Sub1_3407.method14894(Class433.aClass433_4854,
					Class433.aClass433_4854,
					aClass433_3409);
	aClass185_Sub1_3407.method14668(0);
	aClass185_Sub1_3407.method14669(anInterface38_3403);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14599(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3406);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
    }
    
    public void method5765(int i, int i_0_) {
	aClass185_Sub1_3407.method14671(i);
	aClass185_Sub1_3407.method14677(i_0_);
    }
    
    void method15711() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    void method15712() {
	aClass185_Sub1_3407.method14739(aClass433_3409);
	aClass185_Sub1_3407.method14894(Class433.aClass433_4854,
					Class433.aClass433_4854,
					aClass433_3409);
	aClass185_Sub1_3407.method14668(0);
	aClass185_Sub1_3407.method14669(anInterface38_3403);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14599(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3406);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
    }
    
    public void method5767() {
	method15709();
    }
    
    void method15713() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    void method15714() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    public void method5766(int i, int i_1_) {
	aClass185_Sub1_3407.method14671(i);
	aClass185_Sub1_3407.method14677(i_1_);
    }
    
    void method15715() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    public void method5770() {
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14669(anInterface38_3405);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3408);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
	aClass185_Sub1_3407.method14672(Class378.aClass378_3920,
					Class378.aClass378_3916);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3893);
	method15709();
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14670();
    }
    
    void method15716() {
	aClass185_Sub1_3407.method14711();
	method15710();
	aClass185_Sub1_3407.method14733(0, anInterface37_3410);
	aClass185_Sub1_3407.method14700(aClass349_3411);
	aClass185_Sub1_3407.method14708(Class374.aClass374_3895, anInt3404, 2);
    }
    
    public Class320_Sub2(Class185_Sub1 class185_sub1) {
	super(class185_sub1);
    }
    
    public void method5768() {
	method15709();
    }
    
    public void method5769() {
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14669(anInterface38_3405);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3408);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
	aClass185_Sub1_3407.method14672(Class378.aClass378_3920,
					Class378.aClass378_3916);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3893);
	method15709();
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14670();
    }
    
    public void method5764() {
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14669(anInterface38_3405);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3408);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
	aClass185_Sub1_3407.method14672(Class378.aClass378_3920,
					Class378.aClass378_3916);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3893);
	method15709();
	aClass185_Sub1_3407.method14668(1);
	aClass185_Sub1_3407.method14670();
    }
    
    public void method5771(int i, int i_2_) {
	aClass185_Sub1_3407.method14671(i);
	aClass185_Sub1_3407.method14677(i_2_);
    }
    
    void method15717() {
	aClass185_Sub1_3407.method14739(aClass433_3409);
	aClass185_Sub1_3407.method14894(Class433.aClass433_4854,
					Class433.aClass433_4854,
					aClass433_3409);
	aClass185_Sub1_3407.method14668(0);
	aClass185_Sub1_3407.method14669(anInterface38_3403);
	aClass185_Sub1_3407.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3407.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14599(1, Class373.aClass373_3892);
	aClass185_Sub1_3407.method14679().method6842(aClass433_3406);
	aClass185_Sub1_3407.method14871(Class364.aClass364_3724);
    }
}
