/* Class323 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class323
{
    public float aFloat3421;
    protected Class185_Sub1 aClass185_Sub1_3422;
    public Class433 aClass433_3423 = new Class433();
    public Class438 aClass438_3424;
    public Class441 aClass441_3425 = new Class441();
    public Interface38 anInterface38_3426;
    public int anInt3427;
    
    public abstract void method5782(Class433 class433);
    
    public abstract void method5783(int i, boolean bool);
    
    public abstract void method5784(Class433 class433);
    
    public abstract void method5785(boolean bool);
    
    public abstract void method5786(int i, boolean bool);
    
    public abstract void method5787(Class433 class433);
    
    public abstract void method5788(Class433 class433);
    
    public abstract void method5789(int i, boolean bool);
    
    public abstract void method5790(int i, boolean bool);
    
    Class323(Class185_Sub1 class185_sub1) {
	aClass438_3424 = new Class438();
	aFloat3421 = 0.0F;
	aClass185_Sub1_3422 = class185_sub1;
    }
    
    public abstract void method5791(boolean bool);
}
