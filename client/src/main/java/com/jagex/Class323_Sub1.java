/* Class323_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class323_Sub1 extends Class323
{
    public void method5791(boolean bool) {
	aClass185_Sub1_3422.method14677(anInt3427);
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3892);
	method15672();
	aClass185_Sub1_3422.method14706();
    }
    
    public void method5784(Class433 class433) {
	aClass185_Sub1_3422.method14894(class433,
					aClass185_Sub1_3422.aClass433_9142,
					aClass185_Sub1_3422.aClass433_9202);
    }
    
    public void method5786(int i, boolean bool) {
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3890);
	method15672();
	aClass185_Sub1_3422.method14710(Class374.aClass374_3898, 0, i * 4, 0,
					i * 2);
    }
    
    void method15671() {
	aClass185_Sub1_3422.method14668(0);
	aClass185_Sub1_3422.method14669(anInterface38_3426);
	aClass185_Sub1_3422.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3422.method14679().method6842(aClass433_3423);
	aClass185_Sub1_3422.method14871(Class364.aClass364_3724);
    }
    
    void method15672() {
	aClass185_Sub1_3422.method14668(0);
	aClass185_Sub1_3422.method14669(anInterface38_3426);
	aClass185_Sub1_3422.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3422.method14679().method6842(aClass433_3423);
	aClass185_Sub1_3422.method14871(Class364.aClass364_3724);
    }
    
    public void method5789(int i, boolean bool) {
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3890);
	method15672();
	aClass185_Sub1_3422.method14710(Class374.aClass374_3898, 0, i * 4, 0,
					i * 2);
    }
    
    public void method5782(Class433 class433) {
	aClass185_Sub1_3422.method14894(class433,
					aClass185_Sub1_3422.aClass433_9142,
					aClass185_Sub1_3422.aClass433_9202);
    }
    
    public void method5788(Class433 class433) {
	aClass185_Sub1_3422.method14894(class433,
					aClass185_Sub1_3422.aClass433_9142,
					aClass185_Sub1_3422.aClass433_9202);
    }
    
    public void method5785(boolean bool) {
	aClass185_Sub1_3422.method14677(anInt3427);
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3892);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3892);
	method15672();
	aClass185_Sub1_3422.method14706();
    }
    
    public void method5790(int i, boolean bool) {
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3890);
	method15672();
	aClass185_Sub1_3422.method14710(Class374.aClass374_3898, 0, i * 4, 0,
					i * 2);
    }
    
    public void method5783(int i, boolean bool) {
	aClass185_Sub1_3422.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3422.method14599(1, Class373.aClass373_3890);
	method15672();
	aClass185_Sub1_3422.method14710(Class374.aClass374_3898, 0, i * 4, 0,
					i * 2);
    }
    
    public Class323_Sub1(Class185_Sub1 class185_sub1) {
	super(class185_sub1);
    }
    
    void method15673() {
	aClass185_Sub1_3422.method14668(0);
	aClass185_Sub1_3422.method14669(anInterface38_3426);
	aClass185_Sub1_3422.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3422.method14679().method6842(aClass433_3423);
	aClass185_Sub1_3422.method14871(Class364.aClass364_3724);
    }
    
    public void method5787(Class433 class433) {
	aClass185_Sub1_3422.method14894(class433,
					aClass185_Sub1_3422.aClass433_9142,
					aClass185_Sub1_3422.aClass433_9202);
    }
    
    void method15674() {
	aClass185_Sub1_3422.method14668(0);
	aClass185_Sub1_3422.method14669(anInterface38_3426);
	aClass185_Sub1_3422.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3422.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3422.method14679().method6842(aClass433_3423);
	aClass185_Sub1_3422.method14871(Class364.aClass364_3724);
    }
}
