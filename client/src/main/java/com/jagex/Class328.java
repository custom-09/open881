/* Class328 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class328 implements Interface76
{
    static Class328 aClass328_3461;
    static Class328 aClass328_3462;
    static Class328 aClass328_3463;
    static Class328 aClass328_3464;
    static Class328 aClass328_3465;
    static Class328 aClass328_3466;
    static Class328 aClass328_3467;
    static Class328 aClass328_3468;
    static Class328 aClass328_3469;
    static Class328 aClass328_3470 = new Class328(13, 1);
    int anInt3471;
    static Class328 aClass328_3472;
    static Class328 aClass328_3473;
    static Class328 aClass328_3474;
    static Class328 aClass328_3475;
    static Class328 aClass328_3476;
    int anInt3477;
    public static int anInt3478;
    public static int anInt3479;
    
    static Class328[] method5827() {
	return (new Class328[]
		{ aClass328_3473, aClass328_3464, aClass328_3463,
		  aClass328_3470, aClass328_3466, aClass328_3475,
		  aClass328_3462, aClass328_3468, aClass328_3467,
		  aClass328_3476, aClass328_3472, aClass328_3469,
		  aClass328_3474, aClass328_3465, aClass328_3461 });
    }
    
    public int method93() {
	return anInt3477 * -404993037;
    }
    
    static {
	aClass328_3469 = new Class328(11, 2);
	aClass328_3463 = new Class328(14, 3);
	aClass328_3464 = new Class328(12, 4);
	aClass328_3465 = new Class328(5, 5);
	aClass328_3466 = new Class328(3, 6);
	aClass328_3467 = new Class328(7, 7);
	aClass328_3473 = new Class328(8, 8);
	aClass328_3461 = new Class328(10, 9);
	aClass328_3476 = new Class328(1, 10);
	aClass328_3468 = new Class328(6, 11);
	aClass328_3472 = new Class328(0, 12);
	aClass328_3462 = new Class328(4, 13);
	aClass328_3474 = new Class328(9, 14);
	aClass328_3475 = new Class328(2, 15);
    }
    
    public int method22() {
	return anInt3477 * -404993037;
    }
    
    public int method53() {
	return anInt3477 * -404993037;
    }
    
    static Class328[] method5828() {
	return (new Class328[]
		{ aClass328_3473, aClass328_3464, aClass328_3463,
		  aClass328_3470, aClass328_3466, aClass328_3475,
		  aClass328_3462, aClass328_3468, aClass328_3467,
		  aClass328_3476, aClass328_3472, aClass328_3469,
		  aClass328_3474, aClass328_3465, aClass328_3461 });
    }
    
    Class328(int i, int i_0_) {
	anInt3471 = 1900699263 * i;
	anInt3477 = 1270236475 * i_0_;
    }
    
    static final void method5829(Class669 class669, int i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class534_Sub37 class534_sub37
	    = (Class534_Sub37) client.aClass9_11224.method579((long) i_1_);
	if (class534_sub37 != null)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 1;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
    }
    
    static final void method5830(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (int) ((Class250.method4604((byte) -61)
		      - client.aClass214_11359.method4032((byte) -17)
		      - 656147693700925901L * Class212.aLong2274)
		     / 1000L);
    }
    
    static final void method5831
	(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1, boolean bool,
	 int i) {
	Class570 class570 = class654_sub1_sub5_sub1.method18531((byte) -74);
	if (class654_sub1_sub5_sub1.anInt11980 * -1763707177 == 0) {
	    class654_sub1_sub5_sub1.anInt11947 = 0;
	    Class652.anInt8483 = Class676.aClass676_8646.aByte8648 * 182925375;
	    Class179_Sub1.anInt9332 = 0;
	} else {
	    if (class654_sub1_sub5_sub1.aClass711_11948.method14338((byte) 0)
		&& !class654_sub1_sub5_sub1.aClass711_11948
			.method14336(1485740368)) {
		Class205 class205 = class654_sub1_sub5_sub1.aClass711_11948
					.method14382(-533154673);
		if (class654_sub1_sub5_sub1.anInt11937 * 86173187 > 0
		    && 716389381 * class205.anInt2221 == 0) {
		    class654_sub1_sub5_sub1.anInt11947 += -1763149081;
		    Class652.anInt8483
			= Class676.aClass676_8646.aByte8648 * 182925375;
		    Class179_Sub1.anInt9332 = 0;
		    return;
		}
		if (86173187 * class654_sub1_sub5_sub1.anInt11937 <= 0
		    && -492433165 * class205.anInt2218 == 0) {
		    class654_sub1_sub5_sub1.anInt11947 += -1763149081;
		    Class652.anInt8483
			= 182925375 * Class676.aClass676_8646.aByte8648;
		    Class179_Sub1.anInt9332 = 0;
		    return;
		}
	    }
	    for (int i_2_ = 0;
		 i_2_ < class654_sub1_sub5_sub1.aClass529Array11949.length;
		 i_2_++) {
		if (-1183861629 * (class654_sub1_sub5_sub1.aClass529Array11949
				   [i_2_].anInt7123) != -1
		    && class654_sub1_sub5_sub1.aClass529Array11949[i_2_]
			   .aClass711_7121.method14336(1308248859)) {
		    Class684 class684
			= (Class684) (Class55.aClass44_Sub4_447.method91
				      (-1183861629 * (class654_sub1_sub5_sub1
						      .aClass529Array11949
						      [i_2_].anInt7123),
				       264831046));
		    if (class684.aBool8691
			&& -811043807 * class684.anInt8688 != -1) {
			Class205 class205
			    = ((Class205)
			       (Class200_Sub12.aClass44_Sub1_9934.method91
				(class684.anInt8688 * -811043807,
				 -2036379457)));
			if (86173187 * class654_sub1_sub5_sub1.anInt11937 > 0
			    && 0 == 716389381 * class205.anInt2221) {
			    class654_sub1_sub5_sub1.anInt11947 += -1763149081;
			    Class652.anInt8483
				= (182925375
				   * Class676.aClass676_8646.aByte8648);
			    Class179_Sub1.anInt9332 = 0;
			    return;
			}
			if (86173187 * class654_sub1_sub5_sub1.anInt11937 <= 0
			    && 0 == class205.anInt2218 * -492433165) {
			    class654_sub1_sub5_sub1.anInt11947 += -1763149081;
			    Class652.anInt8483
				= (182925375
				   * Class676.aClass676_8646.aByte8648);
			    Class179_Sub1.anInt9332 = 0;
			    return;
			}
		    }
		}
	    }
	    Class438 class438
		= Class438.method6994(class654_sub1_sub5_sub1.method10807()
				      .aClass438_4885);
	    int i_3_ = (int) class438.aFloat4864;
	    int i_4_ = (int) class438.aFloat4865;
	    int i_5_
		= (((class654_sub1_sub5_sub1.anIntArray11977
		     [class654_sub1_sub5_sub1.anInt11980 * -1763707177 - 1])
		    * 512)
		   + class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    int i_6_
		= (((class654_sub1_sub5_sub1.anIntArray11978
		     [class654_sub1_sub5_sub1.anInt11980 * -1763707177 - 1])
		    * 512)
		   + class654_sub1_sub5_sub1.method18545((byte) 1) * 256);
	    if (i_3_ < i_5_) {
		if (i_4_ < i_6_)
		    class654_sub1_sub5_sub1.method18524(10240, -1220057966);
		else if (i_4_ > i_6_)
		    class654_sub1_sub5_sub1.method18524(14336, 236809091);
		else
		    class654_sub1_sub5_sub1.method18524(12288, -1432630231);
	    } else if (i_3_ > i_5_) {
		if (i_4_ < i_6_)
		    class654_sub1_sub5_sub1.method18524(6144, 332994436);
		else if (i_4_ > i_6_)
		    class654_sub1_sub5_sub1.method18524(2048, 1279674434);
		else
		    class654_sub1_sub5_sub1.method18524(4096, 1734083456);
	    } else if (i_4_ < i_6_)
		class654_sub1_sub5_sub1.method18524(8192, -805922903);
	    else if (i_4_ > i_6_)
		class654_sub1_sub5_sub1.method18524(0, 1119229232);
	    int i_7_
		= (class654_sub1_sub5_sub1.aByteArray11979
		   [class654_sub1_sub5_sub1.anInt11980 * -1763707177 - 1]);
	    if (!bool && (i_5_ - i_3_ > 1024 || i_5_ - i_3_ < -1024
			  || i_6_ - i_4_ > 1024 || i_6_ - i_4_ < -1024)) {
		class654_sub1_sub5_sub1.method10815((float) i_5_,
						    class438.aFloat4863,
						    (float) i_6_);
		class654_sub1_sub5_sub1.method18523((class654_sub1_sub5_sub1
						     .anInt11972) * 1306061223,
						    false, 824060257);
		class654_sub1_sub5_sub1.anInt11980 -= 990207207;
		if (86173187 * class654_sub1_sub5_sub1.anInt11937 > 0)
		    class654_sub1_sub5_sub1.anInt11937 -= 645756075;
		Class652.anInt8483
		    = Class676.aClass676_8646.aByte8648 * 182925375;
		Class179_Sub1.anInt9332 = 0;
		class438.method7074();
	    } else {
		int i_8_ = 16;
		boolean bool_9_ = true;
		if (class654_sub1_sub5_sub1
		    instanceof Class654_Sub1_Sub5_Sub1_Sub1)
		    bool_9_
			= ((Class654_Sub1_Sub5_Sub1_Sub1)
			   class654_sub1_sub5_sub1).aClass307_12204.aBool3317;
		if (bool_9_) {
		    int i_10_
			= (class654_sub1_sub5_sub1.anInt11972 * 1306061223
			   - 949937137 * (class654_sub1_sub5_sub1
					  .aClass57_11973.anInt457));
		    if (0 != i_10_
			&& (class654_sub1_sub5_sub1.anInt11944 * 1409535459
			    == -1)
			&& 0 != (-650254265
				 * class654_sub1_sub5_sub1.anInt11971))
			i_8_ = 8;
		    if (!bool
			&& (-1763707177 * class654_sub1_sub5_sub1.anInt11980
			    > 2))
			i_8_ = 24;
		    if (!bool
			&& (class654_sub1_sub5_sub1.anInt11980 * -1763707177
			    > 3))
			i_8_ = 32;
		} else {
		    if (!bool
			&& (class654_sub1_sub5_sub1.anInt11980 * -1763707177
			    > 1))
			i_8_ = 24;
		    if (!bool
			&& (-1763707177 * class654_sub1_sub5_sub1.anInt11980
			    > 2))
			i_8_ = 32;
		}
		if (1235744983 * class654_sub1_sub5_sub1.anInt11947 > 0
		    && -1763707177 * class654_sub1_sub5_sub1.anInt11980 > 1) {
		    i_8_ = 32;
		    class654_sub1_sub5_sub1.anInt11947 -= -1763149081;
		}
		if (Class676.aClass676_8650.aByte8648 == i_7_)
		    i_8_ <<= 1;
		else if (i_7_ == Class676.aClass676_8644.aByte8648)
		    i_8_ >>= 1;
		if (1545308287 * class570.anInt7661 != -1) {
		    i_8_ <<= 9;
		    if (1
			== class654_sub1_sub5_sub1.anInt11980 * -1763707177) {
			int i_11_
			    = (class654_sub1_sub5_sub1.anInt11982 * -1778719267
			       * (-1778719267
				  * class654_sub1_sub5_sub1.anInt11982));
			int i_12_ = (((int) class438.aFloat4864 > i_5_
				      ? (int) class438.aFloat4864 - i_5_
				      : i_5_ - (int) class438.aFloat4864)
				     << 9);
			int i_13_ = (((int) class438.aFloat4865 > i_6_
				      ? (int) class438.aFloat4865 - i_6_
				      : i_6_ - (int) class438.aFloat4865)
				     << 9);
			int i_14_ = i_12_ > i_13_ ? i_12_ : i_13_;
			int i_15_ = i_14_ * (-1204350722 * class570.anInt7661);
			if (i_11_ > i_15_) {
			    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1_16_;
			    (class654_sub1_sub5_sub1_16_
			     = class654_sub1_sub5_sub1).anInt11982
				= (-1335453067
				   * (-1778719267
				      * class654_sub1_sub5_sub1_16_.anInt11982
				      / 2));
			} else if (i_11_ / 2 > i_14_) {
			    class654_sub1_sub5_sub1.anInt11982
				-= class570.anInt7661 * 1531693067;
			    if ((class654_sub1_sub5_sub1.anInt11982
				 * -1778719267)
				< 0)
				class654_sub1_sub5_sub1.anInt11982 = 0;
			} else if ((class654_sub1_sub5_sub1.anInt11982
				    * -1778719267)
				   < i_8_) {
			    class654_sub1_sub5_sub1.anInt11982
				+= class570.anInt7661 * 1531693067;
			    if ((class654_sub1_sub5_sub1.anInt11982
				 * -1778719267)
				> i_8_)
				class654_sub1_sub5_sub1.anInt11982
				    = i_8_ * -1335453067;
			}
		    } else if (-1778719267 * class654_sub1_sub5_sub1.anInt11982
			       < i_8_) {
			class654_sub1_sub5_sub1.anInt11982
			    += 1531693067 * class570.anInt7661;
			if (class654_sub1_sub5_sub1.anInt11982 * -1778719267
			    > i_8_)
			    class654_sub1_sub5_sub1.anInt11982
				= i_8_ * -1335453067;
		    } else if (class654_sub1_sub5_sub1.anInt11982 * -1778719267
			       > 0) {
			class654_sub1_sub5_sub1.anInt11982
			    -= class570.anInt7661 * 1531693067;
			if (-1778719267 * class654_sub1_sub5_sub1.anInt11982
			    < 0)
			    class654_sub1_sub5_sub1.anInt11982 = 0;
		    }
		    i_8_ = (class654_sub1_sub5_sub1.anInt11982 * -1778719267
			    >> 9);
		    if (i_8_ < 1)
			i_8_ = 1;
		}
		Class179_Sub1.anInt9332 = 0;
		if (i_3_ != i_5_ || i_6_ != i_4_) {
		    if (i_3_ < i_5_) {
			class438.aFloat4864 += (float) i_8_;
			Class179_Sub1.anInt9332
			    = ((808625503 * Class179_Sub1.anInt9332 | 0x4)
			       * -878774625);
			if (class438.aFloat4864 > (float) i_5_)
			    class438.aFloat4864 = (float) i_5_;
		    } else if (i_3_ > i_5_) {
			class438.aFloat4864 -= (float) i_8_;
			Class179_Sub1.anInt9332
			    = -878774625 * (808625503 * Class179_Sub1.anInt9332
					    | 0x8);
			if (class438.aFloat4864 < (float) i_5_)
			    class438.aFloat4864 = (float) i_5_;
		    }
		    if (i_4_ < i_6_) {
			class438.aFloat4865 += (float) i_8_;
			Class179_Sub1.anInt9332
			    = ((Class179_Sub1.anInt9332 * 808625503 | 0x1)
			       * -878774625);
			if (class438.aFloat4865 > (float) i_6_)
			    class438.aFloat4865 = (float) i_6_;
		    } else if (i_4_ > i_6_) {
			class438.aFloat4865 -= (float) i_8_;
			Class179_Sub1.anInt9332
			    = -878774625 * (808625503 * Class179_Sub1.anInt9332
					    | 0x2);
			if (class438.aFloat4865 < (float) i_6_)
			    class438.aFloat4865 = (float) i_6_;
		    }
		    class654_sub1_sub5_sub1.method10809(class438);
		    if (i_8_ >= 32)
			Class652.anInt8483
			    = 182925375 * Class676.aClass676_8650.aByte8648;
		    else
			Class652.anInt8483 = i_7_ * 182925375;
		} else
		    Class652.anInt8483
			= Class676.aClass676_8646.aByte8648 * 182925375;
		if ((int) class438.aFloat4864 == i_5_
		    && (int) class438.aFloat4865 == i_6_) {
		    class654_sub1_sub5_sub1.anInt11980 -= 990207207;
		    if (86173187 * class654_sub1_sub5_sub1.anInt11937 > 0)
			class654_sub1_sub5_sub1.anInt11937 -= 645756075;
		}
		class438.method7074();
	    }
	}
    }
}
