/* Class343_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class343_Sub2 extends Class343
{
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_10225;
    Class278 aClass278_10226;
    public static float aFloat10227;
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_10228;
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_10229;
    boolean aBool10230;
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_10231;
    public static float aFloat10232 = 1.0F;
    public static float aFloat10233 = 0.0F;
    Class269 aClass269_10234;
    public static float aFloat10235;
    public static float aFloat10236;
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_10237;
    
    int method6059() {
	return 1;
    }
    
    boolean method6073() {
	return (aClass185_Sub1_3556.aBool9271
		&& aClass185_Sub1_3556.method14747());
    }
    
    static {
	aFloat10227 = 1.0F;
	aFloat10235 = 0.0F;
	aFloat10236 = 1.0F;
    }
    
    boolean method6061() {
	if (method6073()) {
	    aClass269_10234 = aClass185_Sub1_3556.method14589("FilterLevels");
	    if (aClass269_10234 == null)
		return false;
	    try {
		aClass534_Sub12_Sub1_10225
		    = aClass269_10234.method4909("sceneTex", 467686460);
		aClass534_Sub12_Sub1_10228
		    = aClass269_10234.method4909("paramsGamma", 13563673);
		aClass534_Sub12_Sub1_10237
		    = aClass269_10234.method4909("paramsRanges", 1904568714);
		aClass534_Sub12_Sub1_10229
		    = aClass269_10234.method4909("pixelOffset", -136993641);
		aClass534_Sub12_Sub1_10231
		    = aClass269_10234.method4909("PosAndTexCoords",
						 1427634583);
		aClass278_10226
		    = aClass269_10234.method4914("techAdjust", -1418674653);
	    } catch (Exception_Sub4_Sub1 exception_sub4_sub1) {
		return false;
	    } catch (Exception_Sub4_Sub2 exception_sub4_sub2) {
		return false;
	    }
	    if (!aClass278_10226.method5182())
		return false;
	    aBool10230 = true;
	    return true;
	}
	return false;
    }
    
    public Class343_Sub2(Class185_Sub1 class185_sub1) {
	super(class185_sub1);
    }
    
    void method6106() {
	/* empty */
    }
    
    int method6082() {
	return 1;
    }
    
    void method6064(int i, int i_0_) {
	/* empty */
    }
    
    boolean method6095() {
	return (aFloat10232 == 1.0F && aFloat10233 == 0.0F
		&& aFloat10227 == 1.0F && aFloat10235 == 0.0F
		&& aFloat10236 == 1.0F);
    }
    
    boolean method6098() {
	return aBool10230;
    }
    
    int method6068() {
	return 1;
    }
    
    boolean method6072() {
	return (aFloat10232 == 1.0F && aFloat10233 == 0.0F
		&& aFloat10227 == 1.0F && aFloat10235 == 0.0F
		&& aFloat10236 == 1.0F);
    }
    
    int method6069() {
	return 0;
    }
    
    boolean method6099() {
	return (aClass185_Sub1_3556.aBool9271
		&& aClass185_Sub1_3556.method14747());
    }
    
    int method6083() {
	return 1;
    }
    
    boolean method6093() {
	if (method6073()) {
	    aClass269_10234 = aClass185_Sub1_3556.method14589("FilterLevels");
	    if (aClass269_10234 == null)
		return false;
	    try {
		aClass534_Sub12_Sub1_10225
		    = aClass269_10234.method4909("sceneTex", 773012804);
		aClass534_Sub12_Sub1_10228
		    = aClass269_10234.method4909("paramsGamma", 992910490);
		aClass534_Sub12_Sub1_10237
		    = aClass269_10234.method4909("paramsRanges", 407880971);
		aClass534_Sub12_Sub1_10229
		    = aClass269_10234.method4909("pixelOffset", 1016795128);
		aClass534_Sub12_Sub1_10231
		    = aClass269_10234.method4909("PosAndTexCoords", -913713);
		aClass278_10226
		    = aClass269_10234.method4914("techAdjust", -1549981554);
	    } catch (Exception_Sub4_Sub1 exception_sub4_sub1) {
		return false;
	    } catch (Exception_Sub4_Sub2 exception_sub4_sub2) {
		return false;
	    }
	    if (!aClass278_10226.method5182())
		return false;
	    aBool10230 = true;
	    return true;
	}
	return false;
    }
    
    boolean method6076() {
	if (method6073()) {
	    aClass269_10234 = aClass185_Sub1_3556.method14589("FilterLevels");
	    if (aClass269_10234 == null)
		return false;
	    try {
		aClass534_Sub12_Sub1_10225
		    = aClass269_10234.method4909("sceneTex", 563956982);
		aClass534_Sub12_Sub1_10228
		    = aClass269_10234.method4909("paramsGamma", 1622885459);
		aClass534_Sub12_Sub1_10237
		    = aClass269_10234.method4909("paramsRanges", 742224143);
		aClass534_Sub12_Sub1_10229
		    = aClass269_10234.method4909("pixelOffset", 903530580);
		aClass534_Sub12_Sub1_10231
		    = aClass269_10234.method4909("PosAndTexCoords",
						 2108621177);
		aClass278_10226
		    = aClass269_10234.method4914("techAdjust", -1887069865);
	    } catch (Exception_Sub4_Sub1 exception_sub4_sub1) {
		return false;
	    } catch (Exception_Sub4_Sub2 exception_sub4_sub2) {
		return false;
	    }
	    if (!aClass278_10226.method5182())
		return false;
	    aBool10230 = true;
	    return true;
	}
	return false;
    }
    
    boolean method6077() {
	if (method6073()) {
	    aClass269_10234 = aClass185_Sub1_3556.method14589("FilterLevels");
	    if (aClass269_10234 == null)
		return false;
	    try {
		aClass534_Sub12_Sub1_10225
		    = aClass269_10234.method4909("sceneTex", 1885523736);
		aClass534_Sub12_Sub1_10228
		    = aClass269_10234.method4909("paramsGamma", 431618668);
		aClass534_Sub12_Sub1_10237
		    = aClass269_10234.method4909("paramsRanges", 1679829363);
		aClass534_Sub12_Sub1_10229
		    = aClass269_10234.method4909("pixelOffset", 747622995);
		aClass534_Sub12_Sub1_10231
		    = aClass269_10234.method4909("PosAndTexCoords",
						 1491816678);
		aClass278_10226
		    = aClass269_10234.method4914("techAdjust", -1715009991);
	    } catch (Exception_Sub4_Sub1 exception_sub4_sub1) {
		return false;
	    } catch (Exception_Sub4_Sub2 exception_sub4_sub2) {
		return false;
	    }
	    if (!aClass278_10226.method5182())
		return false;
	    aBool10230 = true;
	    return true;
	}
	return false;
    }
    
    void method6107(int i) {
	aClass269_10234.method4910();
    }
    
    void method6080(int i, Class175_Sub1 class175_sub1,
		    Interface38 interface38, Interface21 interface21,
		    Interface38 interface38_1_, boolean bool) {
	float f = aClass185_Sub1_3556.method14715();
	float f_2_ = (float) class175_sub1.method2910();
	float f_3_ = (float) class175_sub1.method2911();
	float f_4_ = f * 2.0F / f_2_;
	float f_5_ = -f * 2.0F / f_3_;
	float[] fs = { -1.0F + f_4_, 1.0F + f_5_, 0.0F, 0.0F, -1.0F + f_4_,
		       -3.0F + f_5_, 0.0F, 2.0F, 3.0F + f_4_, 1.0F + f_5_,
		       2.0F, 0.0F };
	int i_6_ = (int) f_2_;
	int i_7_ = (int) f_3_;
	int i_8_
	    = (bool ? aClass185_Sub1_3556.method3254(-194092797).method2910()
	       : i_6_);
	int i_9_
	    = (bool ? aClass185_Sub1_3556.method3254(330681943).method2911()
	       : i_7_);
	aClass269_10234.method4919(aClass278_10226);
	aClass269_10234.method4913();
	float f_10_ = (float) i_6_ / f_2_;
	float f_11_ = (float) i_7_ / f_3_;
	float f_12_ = (float) i_8_ / f_2_;
	float f_13_ = (float) i_9_ / f_3_;
	fs[8] = (fs[8] + 1.0F) * f_10_ - 1.0F;
	fs[5] = (fs[5] - 1.0F) * f_11_ + 1.0F;
	fs[10] *= f_12_;
	fs[7] *= f_13_;
	aClass269_10234.method4929(aClass534_Sub12_Sub1_10231, fs,
				   -2137565352);
	aClass269_10234.method4933(aClass534_Sub12_Sub1_10225, 0, interface38,
				   382616211);
	aClass269_10234.method4923(aClass534_Sub12_Sub1_10228, aFloat10232,
				   (byte) 8);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10237, aFloat10233,
				   aFloat10227, aFloat10235, aFloat10236,
				   -782422212);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10229, 0.0F, 0.0F,
				   0.0F, 0.0F, -19637316);
	aClass185_Sub1_3556.method3318(0, 0, i_6_, i_7_);
    }
    
    void method6081(int i, Class175_Sub1 class175_sub1,
		    Interface38 interface38, Interface21 interface21,
		    Interface38 interface38_14_, boolean bool) {
	float f = aClass185_Sub1_3556.method14715();
	float f_15_ = (float) class175_sub1.method2910();
	float f_16_ = (float) class175_sub1.method2911();
	float f_17_ = f * 2.0F / f_15_;
	float f_18_ = -f * 2.0F / f_16_;
	float[] fs = { -1.0F + f_17_, 1.0F + f_18_, 0.0F, 0.0F, -1.0F + f_17_,
		       -3.0F + f_18_, 0.0F, 2.0F, 3.0F + f_17_, 1.0F + f_18_,
		       2.0F, 0.0F };
	int i_19_ = (int) f_15_;
	int i_20_ = (int) f_16_;
	int i_21_
	    = (bool ? aClass185_Sub1_3556.method3254(818369271).method2910()
	       : i_19_);
	int i_22_
	    = (bool ? aClass185_Sub1_3556.method3254(-1981116878).method2911()
	       : i_20_);
	aClass269_10234.method4919(aClass278_10226);
	aClass269_10234.method4913();
	float f_23_ = (float) i_19_ / f_15_;
	float f_24_ = (float) i_20_ / f_16_;
	float f_25_ = (float) i_21_ / f_15_;
	float f_26_ = (float) i_22_ / f_16_;
	fs[8] = (fs[8] + 1.0F) * f_23_ - 1.0F;
	fs[5] = (fs[5] - 1.0F) * f_24_ + 1.0F;
	fs[10] *= f_25_;
	fs[7] *= f_26_;
	aClass269_10234.method4929(aClass534_Sub12_Sub1_10231, fs,
				   -1437861206);
	aClass269_10234.method4933(aClass534_Sub12_Sub1_10225, 0, interface38,
				   -981337281);
	aClass269_10234.method4923(aClass534_Sub12_Sub1_10228, aFloat10232,
				   (byte) -8);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10237, aFloat10233,
				   aFloat10227, aFloat10235, aFloat10236,
				   -693618217);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10229, 0.0F, 0.0F,
				   0.0F, 0.0F, -1623150502);
	aClass185_Sub1_3556.method3318(0, 0, i_19_, i_20_);
    }
    
    int method6085() {
	return 1;
    }
    
    void method6066(int i) {
	aClass269_10234.method4910();
    }
    
    boolean method6097() {
	if (method6073()) {
	    aClass269_10234 = aClass185_Sub1_3556.method14589("FilterLevels");
	    if (aClass269_10234 == null)
		return false;
	    try {
		aClass534_Sub12_Sub1_10225
		    = aClass269_10234.method4909("sceneTex", -207241675);
		aClass534_Sub12_Sub1_10228
		    = aClass269_10234.method4909("paramsGamma", 437820877);
		aClass534_Sub12_Sub1_10237
		    = aClass269_10234.method4909("paramsRanges", 321514186);
		aClass534_Sub12_Sub1_10229
		    = aClass269_10234.method4909("pixelOffset", 1738884645);
		aClass534_Sub12_Sub1_10231
		    = aClass269_10234.method4909("PosAndTexCoords",
						 2014225490);
		aClass278_10226
		    = aClass269_10234.method4914("techAdjust", -1382006389);
	    } catch (Exception_Sub4_Sub1 exception_sub4_sub1) {
		return false;
	    } catch (Exception_Sub4_Sub2 exception_sub4_sub2) {
		return false;
	    }
	    if (!aClass278_10226.method5182())
		return false;
	    aBool10230 = true;
	    return true;
	}
	return false;
    }
    
    void method6065(int i, Class175_Sub1 class175_sub1,
		    Interface38 interface38, Interface21 interface21,
		    Interface38 interface38_27_, boolean bool) {
	float f = aClass185_Sub1_3556.method14715();
	float f_28_ = (float) class175_sub1.method2910();
	float f_29_ = (float) class175_sub1.method2911();
	float f_30_ = f * 2.0F / f_28_;
	float f_31_ = -f * 2.0F / f_29_;
	float[] fs = { -1.0F + f_30_, 1.0F + f_31_, 0.0F, 0.0F, -1.0F + f_30_,
		       -3.0F + f_31_, 0.0F, 2.0F, 3.0F + f_30_, 1.0F + f_31_,
		       2.0F, 0.0F };
	int i_32_ = (int) f_28_;
	int i_33_ = (int) f_29_;
	int i_34_
	    = (bool ? aClass185_Sub1_3556.method3254(-1382157864).method2910()
	       : i_32_);
	int i_35_
	    = (bool ? aClass185_Sub1_3556.method3254(-1305398587).method2911()
	       : i_33_);
	aClass269_10234.method4919(aClass278_10226);
	aClass269_10234.method4913();
	float f_36_ = (float) i_32_ / f_28_;
	float f_37_ = (float) i_33_ / f_29_;
	float f_38_ = (float) i_34_ / f_28_;
	float f_39_ = (float) i_35_ / f_29_;
	fs[8] = (fs[8] + 1.0F) * f_36_ - 1.0F;
	fs[5] = (fs[5] - 1.0F) * f_37_ + 1.0F;
	fs[10] *= f_38_;
	fs[7] *= f_39_;
	aClass269_10234.method4929(aClass534_Sub12_Sub1_10231, fs, 1592586640);
	aClass269_10234.method4933(aClass534_Sub12_Sub1_10225, 0, interface38,
				   -1381913613);
	aClass269_10234.method4923(aClass534_Sub12_Sub1_10228, aFloat10232,
				   (byte) -10);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10237, aFloat10233,
				   aFloat10227, aFloat10235, aFloat10236,
				   -527630514);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10229, 0.0F, 0.0F,
				   0.0F, 0.0F, 279038329);
	aClass185_Sub1_3556.method3318(0, 0, i_32_, i_33_);
    }
    
    int method6086() {
	return 1;
    }
    
    int method6058() {
	return 0;
    }
    
    boolean method6060() {
	return (aFloat10232 == 1.0F && aFloat10233 == 0.0F
		&& aFloat10227 == 1.0F && aFloat10235 == 0.0F
		&& aFloat10236 == 1.0F);
    }
    
    boolean method6094() {
	return (aFloat10232 == 1.0F && aFloat10233 == 0.0F
		&& aFloat10227 == 1.0F && aFloat10235 == 0.0F
		&& aFloat10236 == 1.0F);
    }
    
    boolean method6063() {
	return aBool10230;
    }
    
    boolean method6096() {
	return (aFloat10232 == 1.0F && aFloat10233 == 0.0F
		&& aFloat10227 == 1.0F && aFloat10235 == 0.0F
		&& aFloat10236 == 1.0F);
    }
    
    void method6079(int i, Class175_Sub1 class175_sub1,
		    Interface38 interface38, Interface21 interface21,
		    Interface38 interface38_40_, boolean bool) {
	float f = aClass185_Sub1_3556.method14715();
	float f_41_ = (float) class175_sub1.method2910();
	float f_42_ = (float) class175_sub1.method2911();
	float f_43_ = f * 2.0F / f_41_;
	float f_44_ = -f * 2.0F / f_42_;
	float[] fs = { -1.0F + f_43_, 1.0F + f_44_, 0.0F, 0.0F, -1.0F + f_43_,
		       -3.0F + f_44_, 0.0F, 2.0F, 3.0F + f_43_, 1.0F + f_44_,
		       2.0F, 0.0F };
	int i_45_ = (int) f_41_;
	int i_46_ = (int) f_42_;
	int i_47_
	    = (bool ? aClass185_Sub1_3556.method3254(-2139233681).method2910()
	       : i_45_);
	int i_48_
	    = (bool ? aClass185_Sub1_3556.method3254(-2090342427).method2911()
	       : i_46_);
	aClass269_10234.method4919(aClass278_10226);
	aClass269_10234.method4913();
	float f_49_ = (float) i_45_ / f_41_;
	float f_50_ = (float) i_46_ / f_42_;
	float f_51_ = (float) i_47_ / f_41_;
	float f_52_ = (float) i_48_ / f_42_;
	fs[8] = (fs[8] + 1.0F) * f_49_ - 1.0F;
	fs[5] = (fs[5] - 1.0F) * f_50_ + 1.0F;
	fs[10] *= f_51_;
	fs[7] *= f_52_;
	aClass269_10234.method4929(aClass534_Sub12_Sub1_10231, fs, 164792473);
	aClass269_10234.method4933(aClass534_Sub12_Sub1_10225, 0, interface38,
				   -1000198545);
	aClass269_10234.method4923(aClass534_Sub12_Sub1_10228, aFloat10232,
				   (byte) 23);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10237, aFloat10233,
				   aFloat10227, aFloat10235, aFloat10236,
				   -1218966927);
	aClass269_10234.method4926(aClass534_Sub12_Sub1_10229, 0.0F, 0.0F,
				   0.0F, 0.0F, -1293231653);
	aClass185_Sub1_3556.method3318(0, 0, i_45_, i_46_);
    }
    
    boolean method6084() {
	return aBool10230;
    }
    
    void method6067(int i, int i_53_) {
	/* empty */
    }
    
    void method6100(int i, int i_54_) {
	/* empty */
    }
    
    int method6074() {
	return 1;
    }
    
    void method6102(int i, int i_55_) {
	/* empty */
    }
    
    void method6103() {
	/* empty */
    }
    
    void method6104() {
	/* empty */
    }
    
    void method6105() {
	/* empty */
    }
    
    void method6092(int i) {
	aClass269_10234.method4910();
    }
    
    void method6087(int i) {
	aClass269_10234.method4910();
    }
    
    void method6075(int i, int i_56_) {
	/* empty */
    }
    
    void method6108(int i) {
	aClass269_10234.method4910();
    }
}
