/* Class347_Sub3_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class347_Sub3_Sub2 extends Class347_Sub3
{
    void method15858(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    float method15853(float f, float f_0_, float f_1_, int i) {
	float f_2_ = f - aFloat10251;
	if (aClass298_3589.method5393(-1096720309).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_0_ = aClass298_3589.method5396(-1398059573).method7012();
	else {
	    float f_3_
		= f_0_ / aClass298_3589.method5393(-47790474).method7012();
	    float f_4_ = f_0_ / 2.0F * f_3_;
	    if (f_4_ > f_2_) {
		f_0_ -= (aClass298_3589.method5393(-109966152).method7012()
			 * f_1_);
		if (f_0_ < 0.0F)
		    f_0_ = 0.0F;
	    } else if (f_0_
		       < aClass298_3589.method5396(82136548).method7012()) {
		f_0_ += (aClass298_3589.method5393(-476073698).method7012()
			 * f_1_);
		if (f_0_ > aClass298_3589.method5396(-1585449009).method7012())
		    f_0_ = aClass298_3589.method5396(433178187).method7012();
	    }
	}
	return f_0_;
    }
    
    public Class347_Sub3_Sub2(Class298 class298) {
	super(class298);
    }
    
    float method15862(float f, float f_5_, float f_6_) {
	float f_7_ = f - aFloat10251;
	if (aClass298_3589.method5393(-438760497).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_5_ = aClass298_3589.method5396(1259276667).method7012();
	else {
	    float f_8_
		= f_5_ / aClass298_3589.method5393(-288777375).method7012();
	    float f_9_ = f_5_ / 2.0F * f_8_;
	    if (f_9_ > f_7_) {
		f_5_ -= (aClass298_3589.method5393(-143956601).method7012()
			 * f_6_);
		if (f_5_ < 0.0F)
		    f_5_ = 0.0F;
	    } else if (f_5_
		       < aClass298_3589.method5396(-369695959).method7012()) {
		f_5_ += (aClass298_3589.method5393(-175574776).method7012()
			 * f_6_);
		if (f_5_ > aClass298_3589.method5396(-1145698096).method7012())
		    f_5_ = aClass298_3589.method5396(1164419217).method7012();
	    }
	}
	return f_5_;
    }
    
    void method15855() {
	/* empty */
    }
    
    void method15856() {
	/* empty */
    }
    
    void method15860(Class534_Sub40 class534_sub40, int i, int i_10_) {
	/* empty */
    }
    
    void method15857(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    void method15866(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    void method15854(byte i) {
	/* empty */
    }
    
    float method15863(float f, float f_11_, float f_12_) {
	float f_13_ = f - aFloat10251;
	if (aClass298_3589.method5393(-700881776).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_11_ = aClass298_3589.method5396(-907837537).method7012();
	else {
	    float f_14_
		= f_11_ / aClass298_3589.method5393(-1027503982).method7012();
	    float f_15_ = f_11_ / 2.0F * f_14_;
	    if (f_15_ > f_13_) {
		f_11_ -= (aClass298_3589.method5393(-1934497774).method7012()
			  * f_12_);
		if (f_11_ < 0.0F)
		    f_11_ = 0.0F;
	    } else if (f_11_
		       < aClass298_3589.method5396(-271851144).method7012()) {
		f_11_ += (aClass298_3589.method5393(-2144828567).method7012()
			  * f_12_);
		if (f_11_ > aClass298_3589.method5396(-611077814).method7012())
		    f_11_ = aClass298_3589.method5396(-28006395).method7012();
	    }
	}
	return f_11_;
    }
}
