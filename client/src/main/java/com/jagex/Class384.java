/* Class384 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class384
{
    public long aLong3944;
    public int anInt3945;
    static final int anInt3946 = 0;
    public static final int anInt3947 = 1;
    public static final int anInt3948 = 2;
    public static final int anInt3949 = 0;
    static final int anInt3950 = 1;
    public static final int anInt3951 = 0;
    static final int anInt3952 = 1;
    public int anInt3953;
    public int anInt3954;
    public static final int anInt3955 = 1;
    public static final int anInt3956 = 2;
    public int anInt3957;
    public int anInt3958;
    public int anInt3959;
    public int anInt3960;
    public int anInt3961 = 0;
    public int anInt3962 = 0;
    boolean aBool3963 = false;
    public int anInt3964;
    public int anInt3965;
    public int anInt3966;
    public static Class472 aClass472_3967;
    
    void method6462() {
	anInt3965 = (1563207367
		     * Class427.anIntArray4805[anInt3958 * 1148988209 << 3]);
	long l = (long) (anInt3945 * -1058410147);
	long l_0_ = (long) (anInt3957 * 829166933);
	long l_1_ = (long) (894885741 * anInt3953);
	anInt3964
	    = ((int) Math.sqrt((double) (l_1_ * l_1_ + (l_0_ * l_0_ + l * l)))
	       * 1327430267);
	if (0 == anInt3960 * -533022681)
	    anInt3960 = -857000041;
	if (0 == anInt3959 * 256549879)
	    aLong3944 = 1600628302371635205L;
	else if (256549879 * anInt3959 == 1) {
	    aLong3944
		= -5214239248492544005L * (long) (anInt3964 * -1352751720
						  / (anInt3960 * -533022681));
	    aLong3944 *= aLong3944 * -2285738288332836045L;
	} else if (2 == anInt3959 * 256549879)
	    aLong3944
		= -5214239248492544005L * (long) (-1352751720 * anInt3964
						  / (anInt3960 * -533022681));
	if (aBool3963)
	    anInt3964 *= -1;
    }
    
    void method6463(Class534_Sub40 class534_sub40, int i) {
	for (;;) {
	    int i_2_ = class534_sub40.method16527(-2111057799);
	    if (i_2_ == 0)
		break;
	    method6464(class534_sub40, i_2_, (byte) -60);
	}
    }
    
    void method6464(Class534_Sub40 class534_sub40, int i, byte i_3_) {
	if (i == 1)
	    anInt3958 = class534_sub40.method16529((byte) 1) * -653046319;
	else if (i == 2)
	    class534_sub40.method16527(-205442829);
	else if (i == 3) {
	    anInt3945 = class534_sub40.method16533(-258848859) * -694031627;
	    anInt3957 = class534_sub40.method16533(-258848859) * 322125821;
	    anInt3953 = class534_sub40.method16533(-258848859) * -150535579;
	} else if (i == 4) {
	    anInt3959 = class534_sub40.method16527(91870699) * -1160704569;
	    anInt3960 = class534_sub40.method16533(-258848859) * -857000041;
	} else if (6 == i)
	    anInt3954 = class534_sub40.method16527(1864470069) * -1433207405;
	else if (8 == i)
	    anInt3961 = -1840829347;
	else if (i == 9)
	    anInt3962 = 31584171;
	else if (10 == i)
	    aBool3963 = true;
    }
    
    void method6465() {
	anInt3965 = (1563207367
		     * Class427.anIntArray4805[anInt3958 * 1148988209 << 3]);
	long l = (long) (anInt3945 * -1058410147);
	long l_4_ = (long) (anInt3957 * 829166933);
	long l_5_ = (long) (894885741 * anInt3953);
	anInt3964
	    = ((int) Math.sqrt((double) (l_5_ * l_5_ + (l_4_ * l_4_ + l * l)))
	       * 1327430267);
	if (0 == anInt3960 * -533022681)
	    anInt3960 = -857000041;
	if (0 == anInt3959 * 256549879)
	    aLong3944 = 1600628302371635205L;
	else if (256549879 * anInt3959 == 1) {
	    aLong3944
		= -5214239248492544005L * (long) (anInt3964 * -1352751720
						  / (anInt3960 * -533022681));
	    aLong3944 *= aLong3944 * -2285738288332836045L;
	} else if (2 == anInt3959 * 256549879)
	    aLong3944
		= -5214239248492544005L * (long) (-1352751720 * anInt3964
						  / (anInt3960 * -533022681));
	if (aBool3963)
	    anInt3964 *= -1;
    }
    
    void method6466(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1637877019);
	    if (i == 0)
		break;
	    method6464(class534_sub40, i, (byte) -44);
	}
    }
    
    void method6467(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-400398744);
	    if (i == 0)
		break;
	    method6464(class534_sub40, i, (byte) -24);
	}
    }
    
    void method6468(byte i) {
	anInt3965 = (1563207367
		     * Class427.anIntArray4805[anInt3958 * 1148988209 << 3]);
	long l = (long) (anInt3945 * -1058410147);
	long l_6_ = (long) (anInt3957 * 829166933);
	long l_7_ = (long) (894885741 * anInt3953);
	anInt3964
	    = ((int) Math.sqrt((double) (l_7_ * l_7_ + (l_6_ * l_6_ + l * l)))
	       * 1327430267);
	if (0 == anInt3960 * -533022681)
	    anInt3960 = -857000041;
	if (0 == anInt3959 * 256549879)
	    aLong3944 = 1600628302371635205L;
	else if (256549879 * anInt3959 == 1) {
	    aLong3944
		= -5214239248492544005L * (long) (anInt3964 * -1352751720
						  / (anInt3960 * -533022681));
	    aLong3944 *= aLong3944 * -2285738288332836045L;
	} else if (2 == anInt3959 * 256549879)
	    aLong3944
		= -5214239248492544005L * (long) (-1352751720 * anInt3964
						  / (anInt3960 * -533022681));
	if (aBool3963)
	    anInt3964 *= -1;
    }
    
    void method6469(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    anInt3958 = class534_sub40.method16529((byte) 1) * -653046319;
	else if (i == 2)
	    class534_sub40.method16527(2076211377);
	else if (i == 3) {
	    anInt3945 = class534_sub40.method16533(-258848859) * -694031627;
	    anInt3957 = class534_sub40.method16533(-258848859) * 322125821;
	    anInt3953 = class534_sub40.method16533(-258848859) * -150535579;
	} else if (i == 4) {
	    anInt3959 = class534_sub40.method16527(633237838) * -1160704569;
	    anInt3960 = class534_sub40.method16533(-258848859) * -857000041;
	} else if (6 == i)
	    anInt3954 = class534_sub40.method16527(-75927989) * -1433207405;
	else if (8 == i)
	    anInt3961 = -1840829347;
	else if (i == 9)
	    anInt3962 = 31584171;
	else if (10 == i)
	    aBool3963 = true;
    }
    
    void method6470(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    anInt3958 = class534_sub40.method16529((byte) 1) * -653046319;
	else if (i == 2)
	    class534_sub40.method16527(-1622593180);
	else if (i == 3) {
	    anInt3945 = class534_sub40.method16533(-258848859) * -694031627;
	    anInt3957 = class534_sub40.method16533(-258848859) * 322125821;
	    anInt3953 = class534_sub40.method16533(-258848859) * -150535579;
	} else if (i == 4) {
	    anInt3959 = class534_sub40.method16527(181683927) * -1160704569;
	    anInt3960 = class534_sub40.method16533(-258848859) * -857000041;
	} else if (6 == i)
	    anInt3954 = class534_sub40.method16527(-138848502) * -1433207405;
	else if (8 == i)
	    anInt3961 = -1840829347;
	else if (i == 9)
	    anInt3962 = 31584171;
	else if (10 == i)
	    aBool3963 = true;
    }
    
    void method6471() {
	anInt3965 = (1563207367
		     * Class427.anIntArray4805[anInt3958 * 1148988209 << 3]);
	long l = (long) (anInt3945 * -1058410147);
	long l_8_ = (long) (anInt3957 * 829166933);
	long l_9_ = (long) (894885741 * anInt3953);
	anInt3964
	    = ((int) Math.sqrt((double) (l_9_ * l_9_ + (l_8_ * l_8_ + l * l)))
	       * 1327430267);
	if (0 == anInt3960 * -533022681)
	    anInt3960 = -857000041;
	if (0 == anInt3959 * 256549879)
	    aLong3944 = 1600628302371635205L;
	else if (256549879 * anInt3959 == 1) {
	    aLong3944
		= -5214239248492544005L * (long) (anInt3964 * -1352751720
						  / (anInt3960 * -533022681));
	    aLong3944 *= aLong3944 * -2285738288332836045L;
	} else if (2 == anInt3959 * 256549879)
	    aLong3944
		= -5214239248492544005L * (long) (-1352751720 * anInt3964
						  / (anInt3960 * -533022681));
	if (aBool3963)
	    anInt3964 *= -1;
    }
    
    void method6472() {
	anInt3965 = (1563207367
		     * Class427.anIntArray4805[anInt3958 * 1148988209 << 3]);
	long l = (long) (anInt3945 * -1058410147);
	long l_10_ = (long) (anInt3957 * 829166933);
	long l_11_ = (long) (894885741 * anInt3953);
	anInt3964 = ((int) Math.sqrt((double) (l_11_ * l_11_
					       + (l_10_ * l_10_ + l * l)))
		     * 1327430267);
	if (0 == anInt3960 * -533022681)
	    anInt3960 = -857000041;
	if (0 == anInt3959 * 256549879)
	    aLong3944 = 1600628302371635205L;
	else if (256549879 * anInt3959 == 1) {
	    aLong3944
		= -5214239248492544005L * (long) (anInt3964 * -1352751720
						  / (anInt3960 * -533022681));
	    aLong3944 *= aLong3944 * -2285738288332836045L;
	} else if (2 == anInt3959 * 256549879)
	    aLong3944
		= -5214239248492544005L * (long) (-1352751720 * anInt3964
						  / (anInt3960 * -533022681));
	if (aBool3963)
	    anInt3964 *= -1;
    }
    
    Class384() {
	/* empty */
    }
    
    void method6473(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(159978422);
	    if (i == 0)
		break;
	    method6464(class534_sub40, i, (byte) -20);
	}
    }
    
    static final void method6474(Class669 class669, int i) {
	int i_12_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_12_, 1624318036);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_12_ >> 16];
	Class267.method4868(class247, class243, class669, -1250555387);
    }
    
    static final void method6475(Class669 class669, byte i) {
	class669.anInt8600 -= 1544997815;
    }
    
    static final void method6476(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.anInt11037 * -643758853;
    }
    
    public static Class599[] method6477(int i) {
	return (new Class599[]
		{ Class599.aClass599_7868, Class599.aClass599_7870,
		  Class599.aClass599_7869 });
    }
    
    static void method6478(Class247 class247, int i) {
	if (5 == class247.anInt2438 * -1960530827
	    && class247.anInt2606 * 403396513 != -1)
	    Class440.method7103(Class254.aClass185_2683, class247, (byte) -94);
    }
}
