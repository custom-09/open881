/* Class394 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class394 implements Interface47
{
    public int anInt4093;
    public int anInt4094;
    public Class391 aClass391_4095;
    public Class401 aClass401_4096;
    public int anInt4097;
    public int anInt4098;
    public int anInt4099;
    public int anInt4100;
    public int anInt4101;
    
    public Class397 method348(int i) {
	return null;
    }
    
    Class394(Class401 class401, Class391 class391, int i, int i_0_, int i_1_,
	     int i_2_, int i_3_, int i_4_, int i_5_) {
	aClass401_4096 = class401;
	aClass391_4095 = class391;
	anInt4101 = i * 1028805713;
	anInt4093 = i_0_ * 28068005;
	anInt4097 = i_1_ * -2012120213;
	anInt4098 = 399024885 * i_2_;
	anInt4099 = -1930743129 * i_3_;
	anInt4100 = i_4_ * -1927661395;
	anInt4094 = i_5_ * -820848567;
    }
    
    public Class397 method349() {
	return null;
    }
    
    public Class397 method350() {
	return null;
    }
    
    public Class397 method351() {
	return null;
    }
    
    static Class394 method6558(Class534_Sub40 class534_sub40) {
	Class401 class401 = (Class72.method1560(1922310691)
			     [class534_sub40.method16527(1818395367)]);
	Class391 class391 = (Class705.method14234(-585382644)
			     [class534_sub40.method16527(-900437445)]);
	int i = class534_sub40.method16530((byte) -66);
	int i_6_ = class534_sub40.method16530((byte) -17);
	int i_7_ = class534_sub40.method16529((byte) 1);
	int i_8_ = class534_sub40.method16529((byte) 1);
	int i_9_ = class534_sub40.method16530((byte) -62);
	int i_10_ = class534_sub40.method16550((byte) -101);
	int i_11_ = class534_sub40.method16533(-258848859);
	return new Class394(class401, class391, i, i_6_, i_7_, i_8_, i_9_,
			    i_10_, i_11_);
    }
    
    static Class394 method6559(Class534_Sub40 class534_sub40) {
	Class401 class401 = (Class72.method1560(1981497599)
			     [class534_sub40.method16527(-159592895)]);
	Class391 class391 = (Class705.method14234(1666673584)
			     [class534_sub40.method16527(708442687)]);
	int i = class534_sub40.method16530((byte) -118);
	int i_12_ = class534_sub40.method16530((byte) -90);
	int i_13_ = class534_sub40.method16529((byte) 1);
	int i_14_ = class534_sub40.method16529((byte) 1);
	int i_15_ = class534_sub40.method16530((byte) -120);
	int i_16_ = class534_sub40.method16550((byte) 8);
	int i_17_ = class534_sub40.method16533(-258848859);
	return new Class394(class401, class391, i, i_12_, i_13_, i_14_, i_15_,
			    i_16_, i_17_);
    }
    
    static final void method6560(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int[] is = Class546.method8989(string, class669, -219410121);
	if (is != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2505
	    = Class99.method1859(string, class669, -1622263525);
	class247.anIntArray2575 = is;
	class247.aBool2561 = true;
    }
    
    static final void method6561(Class669 class669, int i)
	throws Exception_Sub2 {
	Class599.aClass298_Sub1_7871.method5363(-1483748639);
    }
    
    public static Class699[] method6562(int i) {
	return (new Class699[]
		{ Class699.aClass699_8798, Class699.aClass699_8793,
		  Class699.aClass699_8797, Class699.aClass699_8792,
		  Class699.aClass699_8801, Class699.aClass699_8800,
		  Class699.aClass699_8799, Class699.aClass699_8796,
		  Class699.aClass699_8795 });
    }
}
