/* Class394_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class394_Sub1_Sub1 extends Class394_Sub1
{
    public int anInt11633;
    
    public static Class394 method18107(Class534_Sub40 class534_sub40) {
	Class394_Sub1 class394_sub1
	    = (Class394_Sub1) Class44.method1100(class534_sub40, 1843656164);
	int i = class534_sub40.method16530((byte) -87);
	return new Class394_Sub1_Sub1(class394_sub1.aClass401_4096,
				      class394_sub1.aClass391_4095,
				      -2127596367 * class394_sub1.anInt4101,
				      class394_sub1.anInt4093 * -1055236307,
				      class394_sub1.anInt4097 * -1607607997,
				      -228886179 * class394_sub1.anInt4098,
				      class394_sub1.anInt4099 * -81046249,
				      class394_sub1.anInt4100 * -120853723,
				      class394_sub1.anInt4094 * 1210620409,
				      1333388775 * class394_sub1.anInt10143,
				      class394_sub1.anInt10142 * -2081665769,
				      class394_sub1.anInt10147 * 845449995,
				      class394_sub1.anInt10145 * -773478983,
				      -1674677163 * class394_sub1.anInt10146,
				      2123496001 * class394_sub1.anInt10144,
				      i);
    }
    
    public Class397 method351() {
	return Class397.aClass397_4117;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4117;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4117;
    }
    
    Class394_Sub1_Sub1(Class401 class401, Class391 class391, int i, int i_0_,
		       int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
		       int i_6_, int i_7_, int i_8_, int i_9_, int i_10_,
		       int i_11_, int i_12_) {
	super(class401, class391, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_,
	      i_7_, i_8_, i_9_, i_10_, i_11_);
	anInt11633 = 1494146871 * i_12_;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4117;
    }
    
    public static Class394 method18108(Class534_Sub40 class534_sub40) {
	Class394_Sub1 class394_sub1
	    = (Class394_Sub1) Class44.method1100(class534_sub40, 1741621672);
	int i = class534_sub40.method16530((byte) -103);
	return new Class394_Sub1_Sub1(class394_sub1.aClass401_4096,
				      class394_sub1.aClass391_4095,
				      -2127596367 * class394_sub1.anInt4101,
				      class394_sub1.anInt4093 * -1055236307,
				      class394_sub1.anInt4097 * -1607607997,
				      -228886179 * class394_sub1.anInt4098,
				      class394_sub1.anInt4099 * -81046249,
				      class394_sub1.anInt4100 * -120853723,
				      class394_sub1.anInt4094 * 1210620409,
				      1333388775 * class394_sub1.anInt10143,
				      class394_sub1.anInt10142 * -2081665769,
				      class394_sub1.anInt10147 * 845449995,
				      class394_sub1.anInt10145 * -773478983,
				      -1674677163 * class394_sub1.anInt10146,
				      2123496001 * class394_sub1.anInt10144,
				      i);
    }
}
