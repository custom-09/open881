/* Class394_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;

public class Class394_Sub2 extends Class394
{
    public int anInt10158;
    public int anInt10159;
    
    public Class397 method350() {
	return Class397.aClass397_4110;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4110;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4110;
    }
    
    public static Class394 method15781(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, 2047699384);
	int i = class534_sub40.method16533(-258848859);
	int i_0_ = class534_sub40.method16533(-258848859);
	int i_1_ = class534_sub40.method16550((byte) 41);
	return new Class394_Sub2(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 class394.anInt4093 * -1055236307,
				 class394.anInt4097 * -1607607997,
				 -228886179 * class394.anInt4098,
				 -81046249 * class394.anInt4099,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_0_,
				 i_1_);
    }
    
    Class394_Sub2(Class401 class401, Class391 class391, int i, int i_2_,
		  int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_,
		  int i_9_, int i_10_) {
	super(class401, class391, i, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_);
	anInt10159 = 2062495249 * i_9_;
	anInt10158 = i_10_ * -1242598389;
    }
    
    public Class397 method351() {
	return Class397.aClass397_4110;
    }
    
    public static Class394 method15782(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, 426551727);
	int i = class534_sub40.method16533(-258848859);
	int i_11_ = class534_sub40.method16533(-258848859);
	int i_12_ = class534_sub40.method16550((byte) -39);
	return new Class394_Sub2(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 class394.anInt4093 * -1055236307,
				 class394.anInt4097 * -1607607997,
				 -228886179 * class394.anInt4098,
				 -81046249 * class394.anInt4099,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_11_,
				 i_12_);
    }
    
    public static Class394 method15783(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -1301989387);
	int i = class534_sub40.method16533(-258848859);
	int i_13_ = class534_sub40.method16533(-258848859);
	int i_14_ = class534_sub40.method16550((byte) 4);
	return new Class394_Sub2(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 class394.anInt4093 * -1055236307,
				 class394.anInt4097 * -1607607997,
				 -228886179 * class394.anInt4098,
				 -81046249 * class394.anInt4099,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_13_,
				 i_14_);
    }
    
    public static Class394 method15784(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -742042248);
	int i = class534_sub40.method16533(-258848859);
	int i_15_ = class534_sub40.method16533(-258848859);
	int i_16_ = class534_sub40.method16550((byte) 14);
	return new Class394_Sub2(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 class394.anInt4093 * -1055236307,
				 class394.anInt4097 * -1607607997,
				 -228886179 * class394.anInt4098,
				 -81046249 * class394.anInt4099,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_15_,
				 i_16_);
    }
    
    static final void method15785(int i, int i_17_) {
	client.anInt11092 = 0;
	client.anInt11089 = 0;
	client.anInt11091 += 1875053687;
	Class471.method7659(-704328937);
	Class464.method7565(i, 1789933777);
	Class468.method7627((byte) 53);
	boolean bool = false;
	for (int i_18_ = 0; i_18_ < client.anInt11092 * 289244867; i_18_++) {
	    int i_19_ = client.anIntArray11093[i_18_];
	    Class534_Sub6 class534_sub6
		= (Class534_Sub6) client.aClass9_11331.method579((long) i_19_);
	    Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
		= (Class654_Sub1_Sub5_Sub1_Sub1) class534_sub6.anObject10417;
	    if (class654_sub1_sub5_sub1_sub1.anInt11967 * 1438017593
		!= client.anInt11091 * 1237749063) {
		if (Class72.aBool758 && Class410.method6714(i_19_, 1709639294))
		    Class44_Sub16.method17357((byte) 5);
		if (class654_sub1_sub5_sub1_sub1.aClass307_12204
			.method5638(-488045315))
		    Class694.method14069(class654_sub1_sub5_sub1_sub1,
					 -836779145);
		class654_sub1_sub5_sub1_sub1.method18827(null, (byte) -2);
		class534_sub6.method8892((byte) 1);
		bool = true;
	    }
	}
	if (bool) {
	    int i_20_ = 759971875 * client.anInt11148;
	    client.anInt11148
		= client.aClass9_11331.method600(1780021600) * 1085173643;
	    int i_21_ = 0;
	    Iterator iterator = client.aClass9_11331.iterator();
	    while (iterator.hasNext()) {
		Class534_Sub6 class534_sub6 = (Class534_Sub6) iterator.next();
		client.aClass534_Sub6Array11085[i_21_++] = class534_sub6;
	    }
	    for (int i_22_ = 759971875 * client.anInt11148; i_22_ < i_20_;
		 i_22_++)
		client.aClass534_Sub6Array11085[i_22_] = null;
	    Class305 class305
		= Class599.aClass298_Sub1_7871.method5388((byte) 122);
	    if (class305 == Class305.aClass305_3265) {
		Class347_Sub1 class347_sub1
		    = ((Class347_Sub1)
		       Class599.aClass298_Sub1_7871.method5380((byte) -95));
		class347_sub1.method15745((byte) 41);
	    }
	    Class293 class293
		= Class599.aClass298_Sub1_7871.method5425(1645868862);
	    if (class293 == Class293.aClass293_3125) {
		Class706_Sub4 class706_sub4
		    = ((Class706_Sub4)
		       Class599.aClass298_Sub1_7871.method5381(1742966092));
		class706_sub4.method17298(-1172483249);
	    }
	}
	if ((client.aClass100_11264.aClass534_Sub40_Sub1_1179.anInt10811
	     * 31645619)
	    != -1013636781 * client.aClass100_11264.anInt1197)
	    throw new RuntimeException(new StringBuilder().append
					   (31645619
					    * (client.aClass100_11264
					       .aClass534_Sub40_Sub1_1179
					       .anInt10811))
					   .append
					   (" ").append
					   (-1013636781
					    * client.aClass100_11264.anInt1197)
					   .toString());
	for (int i_23_ = 0; i_23_ < client.anInt11321 * -1125820437; i_23_++) {
	    if (client.aClass9_11331
		    .method579((long) client.anIntArray11088[i_23_])
		== null)
		throw new RuntimeException(new StringBuilder().append
					       (i_23_).append
					       (" ").append
					       (-1125820437
						* client.anInt11321)
					       .toString());
	}
	if (0
	    != 759971875 * client.anInt11148 - client.anInt11321 * -1125820437)
	    throw new RuntimeException(new StringBuilder().append("").append
					   (client.anInt11148 * 759971875
					    - -1125820437 * client.anInt11321)
					   .toString());
	for (int i_24_ = 0; i_24_ < client.anInt11148 * 759971875; i_24_++) {
	    if (client.anInt11091 * 1237749063
		!= (((Class654_Sub1_Sub5_Sub1)
		     client.aClass534_Sub6Array11085[i_24_].anObject10417)
		    .anInt11967) * 1438017593)
		throw new RuntimeException(new StringBuilder().append("")
					       .append
					       ((((Class654_Sub1_Sub5_Sub1)
						  (client
						   .aClass534_Sub6Array11085
						   [i_24_].anObject10417))
						 .anInt11922) * 1126388985)
					       .toString());
	}
    }
    
    static String method15786(Class534_Sub40 class534_sub40, int i,
			      int i_25_) {
	String string;
	try {
	    int i_26_ = class534_sub40.method16546(-1706829710);
	    if (i_26_ > i)
		i_26_ = i;
	    byte[] is = new byte[i_26_];
	    class534_sub40.anInt10811
		+= (Class283.aClass267_3067.method4861((class534_sub40
							.aByteArray10810),
						       (31645619
							* (class534_sub40
							   .anInt10811)),
						       is, 0, i_26_, 143908383)
		    * -1387468933);
	    String string_27_ = Class376.method6418(is, 0, i_26_, -2133936639);
	    string = string_27_;
	} catch (Exception exception) {
	    return "Cabbage";
	}
	return string;
    }
}
