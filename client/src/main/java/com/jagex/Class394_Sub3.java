/* Class394_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class394_Sub3 extends Class394
{
    public int anInt10245;
    public int anInt10246;
    public static Class692 aClass692_10247;
    
    public Class397 method351() {
	return Class397.aClass397_4109;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4109;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4109;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4109;
    }
    
    Class394_Sub3(Class401 class401, Class391 class391, int i, int i_0_,
		  int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_,
		  int i_7_) {
	super(class401, class391, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_);
	anInt10245 = 621965201 * i_6_;
	anInt10246 = 1494775033 * i_7_;
    }
    
    public static Class394 method15845(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, 559573421);
	int i = class534_sub40.method16533(-258848859);
	int i_8_ = class534_sub40.method16533(-258848859);
	return new Class394_Sub3(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 -228886179 * class394.anInt4098,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_8_);
    }
    
    public static Class394 method15846(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -1997790489);
	int i = class534_sub40.method16533(-258848859);
	int i_9_ = class534_sub40.method16533(-258848859);
	return new Class394_Sub3(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 -228886179 * class394.anInt4098,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_9_);
    }
    
    public static Class394 method15847(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -1094919813);
	int i = class534_sub40.method16533(-258848859);
	int i_10_ = class534_sub40.method16533(-258848859);
	return new Class394_Sub3(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 -228886179 * class394.anInt4098,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_10_);
    }
    
    public static Class394 method15848(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, 962657708);
	int i = class534_sub40.method16533(-258848859);
	int i_11_ = class534_sub40.method16533(-258848859);
	return new Class394_Sub3(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 -228886179 * class394.anInt4098,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_11_);
    }
    
    static final void method15849(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class105.method1942(class247, class243, class669, -717456726);
    }
    
    static final void method15850(Class669 class669, int i) {
	int i_12_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_12_, -369239537);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_12_ >> 16];
	Class263.method4831(class247, class243, class669,
			    Class253.aClass253_2664, (byte) 41);
    }
}
