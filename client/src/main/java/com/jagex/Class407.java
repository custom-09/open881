/* Class407 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;

public class Class407 implements Interface76
{
    static Class407 aClass407_4299;
    static Class407 aClass407_4300;
    static Class407 aClass407_4301;
    static Class407 aClass407_4302 = new Class407(0, -1, true, false, true);
    static Class407 aClass407_4303;
    public int anInt4304;
    static Class407 aClass407_4305 = new Class407(1, 0, true, true, true);
    static Class407 aClass407_4306;
    static Class407 aClass407_4307;
    int anInt4308;
    static Class407 aClass407_4309;
    public boolean aBool4310;
    public boolean aBool4311;
    public static Class110_Sub1_Sub2 aClass110_Sub1_Sub2_4312;
    
    public static Class407[] method6683() {
	return (new Class407[]
		{ aClass407_4309, aClass407_4303, aClass407_4299,
		  aClass407_4307, aClass407_4301, aClass407_4305,
		  aClass407_4306, aClass407_4302, aClass407_4300 });
    }
    
    Class407(int i, int i_0_, boolean bool, boolean bool_1_, boolean bool_2_) {
	anInt4308 = 317879063 * i;
	anInt4304 = i_0_ * 881215973;
	aBool4310 = bool_1_;
	aBool4311 = bool_2_;
    }
    
    public static Class407[] method6684(int i) {
	return (new Class407[]
		{ aClass407_4309, aClass407_4303, aClass407_4299,
		  aClass407_4307, aClass407_4301, aClass407_4305,
		  aClass407_4306, aClass407_4302, aClass407_4300 });
    }
    
    public int method93() {
	return -31198041 * anInt4308;
    }
    
    public int method22() {
	return -31198041 * anInt4308;
    }
    
    public int method53() {
	return -31198041 * anInt4308;
    }
    
    static {
	aClass407_4301 = new Class407(2, 1, true, true, false);
	aClass407_4309 = new Class407(3, 8, false, true, true);
	aClass407_4303 = new Class407(4, 9, false, false, true);
	aClass407_4300 = new Class407(5, 10, false, true, true);
	aClass407_4299 = new Class407(6, 11, false, false, true);
	aClass407_4306 = new Class407(7, 12, false, false, true);
	aClass407_4307 = new Class407(8, 13, false, false, true);
    }
    
    public static Class407[] method6685() {
	return (new Class407[]
		{ aClass407_4309, aClass407_4303, aClass407_4299,
		  aClass407_4307, aClass407_4301, aClass407_4305,
		  aClass407_4306, aClass407_4302, aClass407_4300 });
    }
    
    static void method6686(Class185 class185, byte i) {
	if (Class104.aClass700_1298.method14141((byte) 6) != 0) {
	    if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		    .method16935(-1807368365)
		== 0) {
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    Class104.aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8
			 = ((Class534_Sub8)
			    Class104.aClass700_1298.method14139(1896311559))) {
		    Class531.aClass44_Sub7_7135.method17307
			(class185, class185,
			 class534_sub8.anInt10420 * -2069726317,
			 -863343329 * class534_sub8.anInt10419,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 class534_sub8.anInt10421 * 1740682347,
			 Class219.aClass171_2307,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -1972350681);
		    class534_sub8.method8892((byte) 1);
		}
		Class422.method6785((byte) -74);
	    } else {
		if (null == Class104.aClass185_1297) {
		    Canvas canvas = new Canvas();
		    canvas.setSize(36, 32);
		    Class104.aClass185_1297
			= Class321.method5777(0, canvas,
					      Class656.aClass177_8524,
					      Class534.anInterface25_7160,
					      Class334.aClass402_3513,
					      Class269.aClass396_2956,
					      Class48.aClass387_363,
					      Class295.aClass472_3161, 0,
					      2143776392);
		    Class662.aClass171_8551
			= (Class104.aClass185_1297.method3325
			   (Class711.method14414(Class606.aClass472_7988,
						 (Class67.anInt715
						  * -1643399711),
						 0, 324312856),
			    Class178.method2939(Class464.aClass472_5113,
						Class67.anInt715 * -1643399711,
						0),
			    true));
		}
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    Class104.aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8
			 = ((Class534_Sub8)
			    Class104.aClass700_1298.method14139(1269321988))) {
		    Class531.aClass44_Sub7_7135.method17307
			(Class104.aClass185_1297, class185,
			 -2069726317 * class534_sub8.anInt10420,
			 class534_sub8.anInt10419 * -863343329,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 1740682347 * class534_sub8.anInt10421,
			 Class662.aClass171_8551,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -2082127440);
		    class534_sub8.method8892((byte) 1);
		}
	    }
	}
    }
    
    static void method6687(int i) {
	Class24 class24
	    = Class606.method10050("2", client.aClass675_11016.aString8640,
				   false, 2126878446);
	Class77.aClass155_Sub1_819.method15460(class24, (byte) 1);
    }
    
    static void method6688(Class534_Sub19 class534_sub19, int i, int i_3_,
			   int i_4_, byte i_5_) {
	class534_sub19.aClass534_Sub40_Sub1_10513.method16510(i, -1091403111);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16569(i_3_,
							      1453055933);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16569(i_4_, 803094596);
    }
    
    static void method6689(int i, short i_6_) {
	Class49.anInt365 = i * 365309251;
	synchronized (Class49.aClass203_364) {
	    Class49.aClass203_364.method3877(-1213708984);
	}
    }
}
