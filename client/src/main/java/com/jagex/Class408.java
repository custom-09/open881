/* Class408 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.LinkedList;

public class Class408
{
    static Class408 aClass408_4313;
    static Class408 aClass408_4314 = new Class408("6", "6");
    static Class408 aClass408_4315 = new Class408("41", "41");
    static Class408 aClass408_4316;
    static Class408 aClass408_4317;
    static Class408 aClass408_4318;
    static Class408 aClass408_4319;
    static Class408 aClass408_4320;
    static Class408 aClass408_4321;
    static Class408 aClass408_4322;
    static Class408 aClass408_4323;
    static Class408 aClass408_4324;
    static Class408 aClass408_4325;
    static Class408 aClass408_4326;
    static Class408 aClass408_4327;
    static Class408 aClass408_4328;
    static Class408 aClass408_4329;
    static Class408 aClass408_4330;
    static Class408 aClass408_4331;
    static Class408 aClass408_4332;
    static Class408 aClass408_4333;
    static Class408 aClass408_4334;
    static Class408 aClass408_4335;
    static Class408 aClass408_4336;
    public static Class408 aClass408_4337;
    public static Class408 aClass408_4338;
    static Class408 aClass408_4339;
    static Class408 aClass408_4340;
    static Class408 aClass408_4341;
    static Class408 aClass408_4342;
    static Class408 aClass408_4343;
    static Class408 aClass408_4344;
    static Class408 aClass408_4345;
    static Class408 aClass408_4346;
    static Class408 aClass408_4347;
    static Class408 aClass408_4348;
    static Class408 aClass408_4349;
    static Class408 aClass408_4350;
    static Class408 aClass408_4351;
    static Class408 aClass408_4352;
    static Class408 aClass408_4353;
    static Class408 aClass408_4354;
    static Class408 aClass408_4355 = new Class408("26", "26");
    static Class408 aClass408_4356;
    static Class408 aClass408_4357;
    static Class408 aClass408_4358;
    static Class408 aClass408_4359;
    static Class408 aClass408_4360;
    static Class408 aClass408_4361;
    static Class408 aClass408_4362;
    static Class408 aClass408_4363;
    static Class408 aClass408_4364;
    public String aString4365;
    
    static {
	aClass408_4316 = new Class408("37", "37");
	aClass408_4317 = new Class408("16", "16");
	aClass408_4333 = new Class408("35", "35");
	aClass408_4319 = new Class408("28", "28");
	aClass408_4320 = new Class408("51", "51");
	aClass408_4356 = new Class408("50", "50");
	aClass408_4331 = new Class408("31", "31");
	aClass408_4351 = new Class408("43", "43");
	aClass408_4324 = new Class408("11", "11");
	aClass408_4325 = new Class408("38", "38");
	aClass408_4326 = new Class408("46", "46");
	aClass408_4327 = new Class408("8", "8");
	aClass408_4328 = new Class408("34", "34");
	aClass408_4339 = new Class408("47", "47");
	aClass408_4330 = new Class408("32", "32");
	aClass408_4350 = new Class408("21", "21");
	aClass408_4332 = new Class408("44", "44");
	aClass408_4329 = new Class408("3", "3");
	aClass408_4334 = new Class408("45", "45");
	aClass408_4335 = new Class408("14", "14");
	aClass408_4336 = new Class408("2", "2");
	aClass408_4337 = new Class408("48", "48");
	aClass408_4338 = new Class408("12", "12");
	aClass408_4318 = new Class408("9", "9");
	aClass408_4347 = new Class408("49", "49");
	aClass408_4341 = new Class408("23", "23");
	aClass408_4357 = new Class408("29", "29");
	aClass408_4343 = new Class408("20", "20");
	aClass408_4344 = new Class408("7", "7");
	aClass408_4345 = new Class408("24", "24");
	aClass408_4321 = new Class408("30", "30");
	aClass408_4359 = new Class408("33", "33");
	aClass408_4323 = new Class408("10", "10");
	aClass408_4346 = new Class408("27", "27");
	aClass408_4348 = new Class408("19", "19");
	aClass408_4322 = new Class408("52", "52");
	aClass408_4352 = new Class408("17", "17");
	aClass408_4353 = new Class408("36", "36");
	aClass408_4354 = new Class408("40", "40");
	aClass408_4342 = new Class408("25", "25");
	aClass408_4349 = new Class408("1", "1");
	aClass408_4340 = new Class408("42", "42");
	aClass408_4313 = new Class408("39", "39");
	aClass408_4360 = new Class408("5", "5");
	aClass408_4358 = new Class408("13", "13");
	aClass408_4361 = new Class408("15", "15");
	aClass408_4362 = new Class408("22", "22");
	aClass408_4363 = new Class408("18", "18");
	aClass408_4364 = new Class408("4", "4");
    }
    
    public static Class408[] method6690() {
	return new Class408[] { aClass408_4338, aClass408_4322, aClass408_4337,
				aClass408_4327, aClass408_4362, aClass408_4363,
				aClass408_4340, aClass408_4359, aClass408_4364,
				aClass408_4326, aClass408_4325, aClass408_4361,
				aClass408_4351, aClass408_4344, aClass408_4352,
				aClass408_4323, aClass408_4324, aClass408_4331,
				aClass408_4319, aClass408_4333, aClass408_4341,
				aClass408_4353, aClass408_4355, aClass408_4317,
				aClass408_4357, aClass408_4336, aClass408_4321,
				aClass408_4349, aClass408_4356, aClass408_4350,
				aClass408_4348, aClass408_4318, aClass408_4315,
				aClass408_4342, aClass408_4334, aClass408_4343,
				aClass408_4335, aClass408_4316, aClass408_4345,
				aClass408_4332, aClass408_4330, aClass408_4346,
				aClass408_4328, aClass408_4314, aClass408_4339,
				aClass408_4354, aClass408_4329, aClass408_4360,
				aClass408_4320, aClass408_4347, aClass408_4313,
				aClass408_4358 };
    }
    
    Class408(String string, String string_0_) {
	aString4365 = string_0_;
    }
    
    public static int method6691(int i, int i_1_, int i_2_, int i_3_, int i_4_,
				 int i_5_, int i_6_) {
	if (1 == (i_5_ & 0x1)) {
	    int i_7_ = i_3_;
	    i_3_ = i_4_;
	    i_4_ = i_7_;
	}
	i_2_ &= 0x3;
	if (0 == i_2_)
	    return i;
	if (1 == i_2_)
	    return i_1_;
	if (2 == i_2_)
	    return 7 - i - (i_3_ - 1);
	return 7 - i_1_ - (i_4_ - 1);
    }
    
    public static void method6692(Interface48 interface48,
				  Interface46 interface46, byte i) {
	Class625.anInt8153 = 0;
	Class625.anInt8157 = 0;
	Class625.aList8154 = new LinkedList();
	Class625.aClass536_Sub2_Sub1_Sub1Array8152
	    = new Class536_Sub2_Sub1_Sub1[1024];
	OutputStream_Sub1.aClass629Array11013
	    = new Class629[(Class184.anIntArray1982
			    [-1429983313 * Class625.anInt8164]) + 1];
	Class625.anInt8165 = 0;
	Class625.anInt8156 = 0;
	Class625.anInterface46_8159 = interface46;
    }
    
    static void method6693(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class33.method899(string, 1610550490);
    }
    
    static final void method6694(Class669 class669, byte i) {
	class669.anInt8594 -= 85613153;
	Class563.method9508((String) (class669.anObjectArray8593
				      [class669.anInt8594 * 1485266147]),
			    (String) (class669.anObjectArray8593
				      [1 + 1485266147 * class669.anInt8594]),
			    (String) (class669.anObjectArray8593
				      [class669.anInt8594 * 1485266147 + 2]),
			    ((class669.anIntArray8595
			      [(class669.anInt8600 -= 308999563) * 2088438307])
			     == 1),
			    true, 2126406187);
    }
    
    static final void method6695(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_8_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_9_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_8_ < i_9_ ? i_8_ : i_9_;
    }
    
    static Class654_Sub1_Sub5_Sub1 method6696(int i, int i_10_, int i_11_,
					      byte i_12_) {
	Class568 class568 = (client.aClass512_11100.method8424((byte) 35)
			     .aClass568ArrayArrayArray7431[i][i_10_][i_11_]);
	if (null == class568)
	    return null;
	Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1 = null;
	int i_13_ = -1;
	for (Class559 class559 = class568.aClass559_7604; class559 != null;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if (class654_sub1_sub5 instanceof Class654_Sub1_Sub5_Sub1) {
		Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1_14_
		    = (Class654_Sub1_Sub5_Sub1) class654_sub1_sub5;
		int i_15_ = ((class654_sub1_sub5_sub1_14_.method18545((byte) 1)
			      - 1) * 256
			     + 252);
		Class438 class438
		    = class654_sub1_sub5_sub1_14_.method10807().aClass438_4885;
		int i_16_ = (int) class438.aFloat4864 - i_15_ >> 9;
		int i_17_ = (int) class438.aFloat4865 - i_15_ >> 9;
		int i_18_ = i_15_ + (int) class438.aFloat4864 >> 9;
		int i_19_ = i_15_ + (int) class438.aFloat4865 >> 9;
		if (i_16_ <= i_10_ && i_17_ <= i_11_ && i_18_ >= i_10_
		    && i_19_ >= i_11_) {
		    int i_20_ = (1 + i_18_ - i_10_) * (i_19_ + 1 - i_11_);
		    if (i_20_ > i_13_) {
			class654_sub1_sub5_sub1 = class654_sub1_sub5_sub1_14_;
			i_13_ = i_20_;
		    }
		}
	    }
	}
	return class654_sub1_sub5_sub1;
    }
    
    static final void method6697(Class669 class669, int i) {
	int i_21_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_21_, 1504272055);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_21_ >> 16];
	Class275.method5150(class247, class243, class669, 2141837149);
    }
    
    static void method6698(Class669 class669, short i) {
	class669.anIntArray8595[class669.anInt8600 * 2088438307 - 2]
	    = (((Class273)
		Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						      [((2088438307
							 * class669.anInt8600)
							- 2)]),
						     1195754948))
	       .anIntArray3024
	       [class669.anIntArray8595[class669.anInt8600 * 2088438307 - 1]]);
	class669.anInt8600 -= 308999563;
    }
}
