/* Class417 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class417
{
    static final int anInt4673 = 3;
    static final int anInt4674 = 9;
    static final int anInt4675 = 1;
    static final int anInt4676 = 5;
    static final int anInt4677 = 2;
    static final int anInt4678 = 12;
    static final int anInt4679 = 4;
    static final int anInt4680 = 7;
    static final int anInt4681 = 6;
    static final int anInt4682 = 8;
    static final int anInt4683 = 13;
    static final int anInt4684 = 11;
    static final int anInt4685 = 10;
    
    Class417() throws Throwable {
	throw new Error();
    }
    
    static final void method6753(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	int i_1_ = i_0_ >> 14 & 0x3fff;
	int i_2_ = i_0_ & 0x3fff;
	Class597 class597 = client.aClass512_11100.method8416((byte) 8);
	i_1_ -= -424199969 * class597.anInt7859;
	if (i_1_ < 0)
	    i_1_ = 0;
	else if (i_1_ >= client.aClass512_11100.method8417(392885427))
	    i_1_ = client.aClass512_11100.method8417(559930588);
	i_2_ -= -1166289421 * class597.anInt7861;
	if (i_2_ < 0)
	    i_2_ = 0;
	else if (i_2_ >= client.aClass512_11100.method8418(-1533611049))
	    i_2_ = client.aClass512_11100.method8418(-1533611049);
	client.anInt11137 = 711878257 * ((i_1_ << 9) + 256);
	client.anInt11138 = (256 + (i_2_ << 9)) * -1949606641;
	Class10.anInt75 = -520252003;
	Class106.anInt1312 = 309821991;
	Class93.anInt901 = 1899572639;
	client.aBool11147 = true;
    }
    
    static void method6754(Class185 class185, byte i) {
	int i_3_ = -10660793;
	Class214.method4099(class185, Class627.anInt8175 * -620506573,
			    -260575397 * Class72.anInt782,
			    Class641.anInt8340 * -1739196959,
			    Class327_Sub1.anInt9991 * -577412881, i_3_,
			    -16777216, (byte) 34);
	Class231.aClass171_2325.method2828
	    (Class58.aClass58_468.method1245(Class539.aClass672_7171,
					     (byte) -31),
	     3 + Class627.anInt8175 * -620506573,
	     14 + -260575397 * Class72.anInt782, i_3_, -1, (byte) 94);
	int i_4_ = Class81.aClass563_834.method9493(-1875782898);
	int i_5_ = Class81.aClass563_834.method9477(1390294839);
	if (!Class72.aBool766) {
	    int i_6_ = 0;
	    for (Class534_Sub18_Sub7 class534_sub18_sub7
		     = ((Class534_Sub18_Sub7)
			Class72.aClass700_771.method14135((byte) -1));
		 null != class534_sub18_sub7;
		 class534_sub18_sub7
		     = ((Class534_Sub18_Sub7)
			Class72.aClass700_771.method14139(594075358))) {
		int i_7_ = (((Class72.anInt765 * 324852453 - 1 - i_6_)
			     * (-238165825 * Class72.anInt754))
			    + (31 + Class72.anInt782 * -260575397));
		Class87.method1709(i_4_, i_5_, Class627.anInt8175 * -620506573,
				   Class72.anInt782 * -260575397,
				   -1739196959 * Class641.anInt8340,
				   Class327_Sub1.anInt9991 * -577412881, i_7_,
				   class534_sub18_sub7,
				   Class231.aClass171_2325,
				   Class322.aClass16_3420, -1, -256, (byte) 0);
		i_6_++;
	    }
	} else {
	    int i_8_ = 0;
	    for (Class534_Sub18_Sub11 class534_sub18_sub11
		     = ((Class534_Sub18_Sub11)
			Class72.aClass696_772.method14078(1221951837));
		 class534_sub18_sub11 != null;
		 class534_sub18_sub11
		     = ((Class534_Sub18_Sub11)
			Class72.aClass696_772.method14080((byte) 57))) {
		int i_9_ = (31 + Class72.anInt782 * -260575397
			    + -238165825 * Class72.anInt754 * i_8_);
		if (1 == class534_sub18_sub11.anInt11795 * -475442105)
		    Class87.method1709(i_4_, i_5_,
				       -620506573 * Class627.anInt8175,
				       Class72.anInt782 * -260575397,
				       -1739196959 * Class641.anInt8340,
				       -577412881 * Class327_Sub1.anInt9991,
				       i_9_,
				       ((Class534_Sub18_Sub7)
					(class534_sub18_sub11.aClass696_11794
					 .aClass534_Sub18_8785
					 .aClass534_Sub18_10510)),
				       Class231.aClass171_2325,
				       Class322.aClass16_3420, -1, -256,
				       (byte) 0);
		else
		    Class536_Sub4.method15994(i_4_, i_5_,
					      -620506573 * Class627.anInt8175,
					      Class72.anInt782 * -260575397,
					      Class641.anInt8340 * -1739196959,
					      (-577412881
					       * Class327_Sub1.anInt9991),
					      i_9_, class534_sub18_sub11,
					      Class231.aClass171_2325,
					      Class322.aClass16_3420, -1, -256,
					      (byte) 46);
		i_8_++;
	    }
	    if (null != Class72.aClass534_Sub18_Sub11_760) {
		Class214.method4099(class185, Class112.anInt1364 * -1803884121,
				    Class150.anInt1699 * 892411561,
				    Class536_Sub4.anInt10366 * -2123561997,
				    Class281.anInt3062 * -417346889, i_3_,
				    -16777216, (byte) -38);
		Class231.aClass171_2325.method2828
		    (Class72.aClass534_Sub18_Sub11_760.aString11793,
		     Class112.anInt1364 * -1803884121 + 3,
		     Class150.anInt1699 * 892411561 + 14, i_3_, -1, (byte) 27);
		i_8_ = 0;
		for (Class534_Sub18_Sub7 class534_sub18_sub7
			 = ((Class534_Sub18_Sub7)
			    Class72.aClass534_Sub18_Sub11_760
				.aClass696_11794.method14078(1221951837));
		     class534_sub18_sub7 != null;
		     class534_sub18_sub7
			 = ((Class534_Sub18_Sub7)
			    Class72.aClass534_Sub18_Sub11_760
				.aClass696_11794.method14080((byte) 3))) {
		    int i_10_ = (-238165825 * Class72.anInt754 * i_8_
				 + (Class150.anInt1699 * 892411561 + 31));
		    Class87.method1709(i_4_, i_5_,
				       Class112.anInt1364 * -1803884121,
				       Class150.anInt1699 * 892411561,
				       Class536_Sub4.anInt10366 * -2123561997,
				       Class281.anInt3062 * -417346889, i_10_,
				       class534_sub18_sub7,
				       Class231.aClass171_2325,
				       Class322.aClass16_3420, -1, -256,
				       (byte) 0);
		    i_8_++;
		}
	    }
	}
    }
    
    public static void method6755(long[] ls, int[] is, int i) {
	IsaacCipher.method650(ls, is, 0, ls.length - 1, -1113871958);
    }
    
    static final void method6756(Class669 class669, byte i) {
	Class247 class247
	    = Class112.method2017(class669.anIntArray8595[((class669.anInt8600
							    -= 308999563)
							   * 2088438307)],
				  1461927049);
	if (null == class247.aClass247Array2620)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	else {
	    int i_11_ = class247.aClass247Array2620.length;
	    for (int i_12_ = 0; i_12_ < class247.aClass247Array2620.length;
		 i_12_++) {
		if (null == class247.aClass247Array2620[i_12_]) {
		    i_11_ = i_12_;
		    break;
		}
	    }
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= i_11_;
	}
    }
    
    static final void method6757(Class669 class669, byte i) {
	if (Class211.anIntArray2260 == null
	    || (Class352.anInt3653 * -472703619
		>= Class211.anIntArray2260.length))
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= ((Class211.anIntArray2260
		    [(Class352.anInt3653 += 1476395989) * -472703619 - 1])
		   & 0xffff);
    }
}
