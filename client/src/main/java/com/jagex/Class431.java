/* Class431 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class431
{
    public static final int anInt4824 = 3;
    public static final int anInt4825 = 11;
    public static final int anInt4826 = 8;
    public static final int anInt4827 = 4;
    public static final int anInt4828 = 10;
    public static final int anInt4829 = 15;
    public static final int anInt4830 = 12;
    public static final int anInt4831 = 9;
    public static final int anInt4832 = 17;
    public static final int anInt4833 = 7;
    public static final int anInt4834 = 6;
    public static final int anInt4835 = 16;
    public static final int anInt4836 = 13;
    public static final int anInt4837 = 14;
    public static final int anInt4838 = 5;
    public static final int anInt4839 = 1;
    public static final int anInt4840 = 2;
    public static final int anInt4841 = 19;
    public static final int anInt4842 = 18;
    public static final int anInt4843 = 0;
    
    Class431() throws Throwable {
	throw new Error();
    }
    
    public static boolean method6834(int i, byte i_0_) {
	return i == 0 || 2 == i;
    }
    
    static void method6835(Class699 class699, byte i) {
	Class267.aClass699_2940 = class699;
    }
    
    static final void method6836(Class669 class669, byte i) {
	Class664.method10999(1739281693);
	Class177.method2934((short) 14612);
	client.aClass512_11100.method8441(1853752489);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    public static final int method6837(String string, int i) {
	if (string == null)
	    return -1;
	for (int i_1_ = 0; i_1_ < client.anInt11324 * -1979292205; i_1_++) {
	    if (string.equalsIgnoreCase(client.aClass28Array11327[i_1_]
					.aString257))
		return i_1_;
	}
	return -1;
    }
}
