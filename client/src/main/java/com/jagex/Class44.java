/* Class44 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;

public class Class44 implements Interface14
{
    Class203 aClass203_325;
    protected Interface6 anInterface6_326;
    public int anInt327;
    Class649 aClass649_328;
    Class472 aClass472_329;
    
    public void method1076() {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(84097269);
	}
    }
    
    public void method1077() {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(-1002533490);
	}
    }
    
    public void method1078() {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(-1829258979);
	}
    }
    
    public int method90(byte i) {
	return anInt327 * 888398261;
    }
    
    public void method1079(int i, int i_0_) {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(-1410318337);
	    aClass203_325 = new Class203(i);
	}
    }
    
    public void method1080(int i) {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(1609773697);
	}
    }
    
    public void method1081(int i, short i_1_) {
	synchronized (aClass203_325) {
	    aClass203_325.method3876(i, (byte) 0);
	}
    }
    
    public void method1082(int i) {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(-1542074802);
	    aClass203_325 = new Class203(i);
	}
    }
    
    public Iterator iterator() {
	return new Class46(this);
    }
    
    public Interface13 method87(int i) {
	Interface13 interface13;
	synchronized (aClass203_325) {
	    interface13 = (Interface13) aClass203_325.method3871((long) i);
	}
	if (interface13 != null)
	    return interface13;
	interface13 = method1086(i, 1244029428);
	synchronized (aClass203_325) {
	    aClass203_325.method3893(interface13, (long) i);
	}
	return interface13;
    }
    
    public Interface13 method86(int i) {
	Interface13 interface13;
	synchronized (aClass203_325) {
	    interface13 = (Interface13) aClass203_325.method3871((long) i);
	}
	if (interface13 != null)
	    return interface13;
	interface13 = method1086(i, -418985364);
	synchronized (aClass203_325) {
	    aClass203_325.method3893(interface13, (long) i);
	}
	return interface13;
    }
    
    public Interface13 method89(int i) {
	Interface13 interface13;
	synchronized (aClass203_325) {
	    interface13 = (Interface13) aClass203_325.method3871((long) i);
	}
	if (interface13 != null)
	    return interface13;
	interface13 = method1086(i, -736083338);
	synchronized (aClass203_325) {
	    aClass203_325.method3893(interface13, (long) i);
	}
	return interface13;
    }
    
    public int method85() {
	return anInt327 * 888398261;
    }
    
    Interface13 method1083(int i) {
	byte[] is;
	synchronized (aClass472_329) {
	    is = Class600.method9950(aClass472_329, aClass649_328, i,
				     (short) -18841);
	}
	Interface13 interface13
	    = anInterface6_326.method58(i, this, (byte) -1);
	if (null != is)
	    interface13.method79(new Class534_Sub40(is), (byte) 3);
	interface13.method82(-1757440304);
	return interface13;
    }
    
    public void method1084(int i) {
	synchronized (aClass203_325) {
	    aClass203_325.method3876(i, (byte) 0);
	}
    }
    
    public Class44(Class675 class675, Class672 class672, Class472 class472,
		   Class649 class649, int i, Interface6 interface6) {
	aClass472_329 = class472;
	aClass649_328 = class649;
	anInterface6_326 = interface6;
	anInt327 = (Class26.method858(aClass472_329, aClass649_328, -897742574)
		    * -1692351331);
	aClass203_325 = new Class203(i);
    }
    
    public void method1085(int i) {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -54);
	}
    }
    
    public Interface13 method91(int i, int i_2_) {
	Interface13 interface13;
	synchronized (aClass203_325) {
	    interface13 = (Interface13) aClass203_325.method3871((long) i);
	}
	if (interface13 != null)
	    return interface13;
	interface13 = method1086(i, -119241408);
	synchronized (aClass203_325) {
	    aClass203_325.method3893(interface13, (long) i);
	}
	return interface13;
    }
    
    public int method88() {
	return anInt327 * 888398261;
    }
    
    Interface13 method1086(int i, int i_3_) {
	byte[] is;
	synchronized (aClass472_329) {
	    is = Class600.method9950(aClass472_329, aClass649_328, i,
				     (short) -3221);
	}
	Interface13 interface13
	    = anInterface6_326.method58(i, this, (byte) -1);
	if (null != is)
	    interface13.method79(new Class534_Sub40(is), (byte) 3);
	interface13.method82(-1757440304);
	return interface13;
    }
    
    public void method1087() {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -54);
	}
    }
    
    public void method1088() {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -14);
	}
    }
    
    public void method1089() {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -95);
	}
    }
    
    public void method1090() {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -19);
	}
    }
    
    public void method1091() {
	synchronized (aClass203_325) {
	    aClass203_325.method3884((byte) -73);
	}
    }
    
    public void method1092(int i) {
	synchronized (aClass203_325) {
	    aClass203_325.method3876(i, (byte) 0);
	}
    }
    
    public Iterator method1093() {
	return new Class46(this);
    }
    
    Interface13 method1094(int i) {
	byte[] is;
	synchronized (aClass472_329) {
	    is = Class600.method9950(aClass472_329, aClass649_328, i,
				     (short) -5811);
	}
	Interface13 interface13
	    = anInterface6_326.method58(i, this, (byte) -1);
	if (null != is)
	    interface13.method79(new Class534_Sub40(is), (byte) 3);
	interface13.method82(-1757440304);
	return interface13;
    }
    
    Interface13 method1095(int i) {
	byte[] is;
	synchronized (aClass472_329) {
	    is = Class600.method9950(aClass472_329, aClass649_328, i,
				     (short) -17987);
	}
	Interface13 interface13
	    = anInterface6_326.method58(i, this, (byte) -1);
	if (null != is)
	    interface13.method79(new Class534_Sub40(is), (byte) 3);
	interface13.method82(-1757440304);
	return interface13;
    }
    
    public Iterator method1096() {
	return new Class46(this);
    }
    
    Interface13 method1097(int i) {
	byte[] is;
	synchronized (aClass472_329) {
	    is = Class600.method9950(aClass472_329, aClass649_328, i,
				     (short) -27789);
	}
	Interface13 interface13
	    = anInterface6_326.method58(i, this, (byte) -1);
	if (null != is)
	    interface13.method79(new Class534_Sub40(is), (byte) 3);
	interface13.method82(-1757440304);
	return interface13;
    }
    
    public void method1098() {
	synchronized (aClass203_325) {
	    aClass203_325.method3877(-741277870);
	}
    }
    
    static final void method1099(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class65.anInt708 * -241239087;
    }
    
    public static Class394 method1100(Class534_Sub40 class534_sub40, int i) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -1152272777);
	int i_4_ = class534_sub40.method16550((byte) 64);
	int i_5_ = class534_sub40.method16550((byte) -69);
	int i_6_ = class534_sub40.method16550((byte) -18);
	int i_7_ = class534_sub40.method16550((byte) 52);
	int i_8_ = class534_sub40.method16550((byte) -12);
	int i_9_ = class534_sub40.method16550((byte) -81);
	return new Class394_Sub1(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 class394.anInt4098 * -228886179,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i_4_, i_5_,
				 i_6_, i_7_, i_8_, i_9_);
    }
}
