/* Class444 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class444
{
    public Class438 aClass438_4885;
    public Class443 aClass443_4886;
    
    public final void method7218() {
	aClass443_4886.method7182();
	aClass438_4885.method7088();
	aClass438_4885.method7021(aClass443_4886);
    }
    
    public Class444(Class444 class444_0_) {
	aClass443_4886 = new Class443();
	aClass438_4885 = new Class438();
	method7219(class444_0_);
    }
    
    public void method7219(Class444 class444_1_) {
	aClass443_4886.method7145(class444_1_.aClass443_4886);
	aClass438_4885.method6992(class444_1_.aClass438_4885);
    }
    
    public final void method7220() {
	aClass443_4886.method7182();
	aClass438_4885.method7088();
	aClass438_4885.method7021(aClass443_4886);
    }
    
    public Class444() {
	aClass443_4886 = new Class443();
	aClass438_4885 = new Class438();
    }
    
    public final void method7221(Class444 class444_2_) {
	aClass443_4886.method7158(class444_2_.aClass443_4886);
	aClass438_4885.method7021(class444_2_.aClass443_4886);
	aClass438_4885.method7026(class444_2_.aClass438_4885);
    }
    
    public String method7222() {
	return new StringBuilder().append("[").append
		   (aClass443_4886.toString()).append
		   ("|").append
		   (aClass438_4885.toString()).append
		   ("]").toString();
    }
    
    public String method7223() {
	return new StringBuilder().append("[").append
		   (aClass443_4886.toString()).append
		   ("|").append
		   (aClass438_4885.toString()).append
		   ("]").toString();
    }
    
    public void method7224(Class444 class444_3_) {
	aClass443_4886.method7145(class444_3_.aClass443_4886);
	aClass438_4885.method6992(class444_3_.aClass438_4885);
    }
    
    public final void method7225() {
	aClass443_4886.method7182();
	aClass438_4885.method7088();
	aClass438_4885.method7021(aClass443_4886);
    }
    
    public void method7226(Class444 class444_4_) {
	aClass443_4886.method7145(class444_4_.aClass443_4886);
	aClass438_4885.method6992(class444_4_.aClass438_4885);
    }
    
    public void method7227(Class444 class444_5_) {
	aClass443_4886.method7145(class444_5_.aClass443_4886);
	aClass438_4885.method6992(class444_5_.aClass438_4885);
    }
    
    public final void method7228() {
	aClass443_4886.method7182();
	aClass438_4885.method7088();
	aClass438_4885.method7021(aClass443_4886);
    }
    
    public final void method7229() {
	aClass443_4886.method7182();
	aClass438_4885.method7088();
	aClass438_4885.method7021(aClass443_4886);
    }
    
    public final void method7230(Class444 class444_6_) {
	aClass443_4886.method7158(class444_6_.aClass443_4886);
	aClass438_4885.method7021(class444_6_.aClass443_4886);
	aClass438_4885.method7026(class444_6_.aClass438_4885);
    }
    
    public String toString() {
	return new StringBuilder().append("[").append
		   (aClass443_4886.toString()).append
		   ("|").append
		   (aClass438_4885.toString()).append
		   ("]").toString();
    }
    
    public final void method7231(Class444 class444_7_) {
	aClass443_4886.method7158(class444_7_.aClass443_4886);
	aClass438_4885.method7021(class444_7_.aClass443_4886);
	aClass438_4885.method7026(class444_7_.aClass438_4885);
    }
}
