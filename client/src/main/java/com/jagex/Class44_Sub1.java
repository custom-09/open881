/* Class44_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub1 extends Class44
{
    public void method1092(int i) {
	super.method1081(i, (short) 12004);
	((Class206) anInterface6_326).method3928(i, 924455323);
    }
    
    public Class534_Sub18_Sub17 method17241(int i, int i_0_) {
	return ((Class206) anInterface6_326).method3926(i, -1468149322);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class206) anInterface6_326).method3931((byte) 88);
    }
    
    public void method1081(int i, short i_1_) {
	super.method1081(i, (short) 14214);
	((Class206) anInterface6_326).method3928(i, 1092081670);
    }
    
    public void method1085(int i) {
	super.method1085(2112585664);
	((Class206) anInterface6_326).method3929(753676691);
    }
    
    public Class534_Sub18_Sub17 method17242(int i) {
	return ((Class206) anInterface6_326).method3926(i, 616667447);
    }
    
    public void method1091() {
	super.method1085(2112576887);
	((Class206) anInterface6_326).method3929(-104738731);
    }
    
    public void method1088() {
	super.method1085(1970928568);
	((Class206) anInterface6_326).method3929(1175445557);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class206) anInterface6_326).method3931((byte) 12);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class206) anInterface6_326).method3931((byte) -114);
    }
    
    public Class44_Sub1(Class675 class675, Class672 class672,
			Class472 class472, Class472 class472_2_,
			Class472 class472_3_, Class44_Sub8 class44_sub8) {
	super(class675, class672, class472, Class649.aClass649_8415, 64,
	      new Class206_Sub1(class44_sub8, class472_2_, class472_3_));
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class206) anInterface6_326).method3931((byte) 21);
    }
    
    public Class534_Sub18_Sub17 method17243(int i) {
	return ((Class206) anInterface6_326).method3926(i, 32802449);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 25862);
	((Class206) anInterface6_326).method3928(i, 764465959);
    }
    
    public Class534_Sub18_Sub17 method17244(int i) {
	return ((Class206) anInterface6_326).method3926(i, -265684029);
    }
    
    public void method1087() {
	super.method1085(1948999115);
	((Class206) anInterface6_326).method3929(1197077576);
    }
    
    public Class534_Sub18_Sub17 method17245(int i) {
	return ((Class206) anInterface6_326).method3926(i, 996015930);
    }
    
    public void method1089() {
	super.method1085(2014698385);
	((Class206) anInterface6_326).method3929(1826649623);
    }
    
    public void method1090() {
	super.method1085(1899671871);
	((Class206) anInterface6_326).method3929(996127955);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class206) anInterface6_326).method3931((byte) -39);
    }
}
