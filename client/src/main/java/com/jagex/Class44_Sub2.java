/* Class44_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub2 extends Class44
{
    public void method1081(int i, short i_0_) {
	super.method1081(i, (short) 5125);
	((Class586) anInterface6_326).method9850(i, 1957940911);
    }
    
    public void method1090() {
	super.method1085(2108499609);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class586) anInterface6_326).method9854(2080130977);
    }
    
    public void method1085(int i) {
	super.method1085(2142452322);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class586) anInterface6_326).method9854(1951559014);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class586) anInterface6_326).method9854(2144278886);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class586) anInterface6_326).method9854(1948559700);
    }
    
    public Class44_Sub2(Class675 class675, Class672 class672,
			Class472 class472, Class472 class472_1_) {
	super(class675, class672, class472, Class649.aClass649_8424, 64,
	      new Class586_Sub1(class472_1_));
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 808);
	((Class586) anInterface6_326).method9850(i, 1267815297);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 14807);
	((Class586) anInterface6_326).method9850(i, 1212649825);
    }
    
    public void method1087() {
	super.method1085(1954149021);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    public void method1088() {
	super.method1085(2013443906);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    public void method1089() {
	super.method1085(1963379442);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class586) anInterface6_326).method9854(2143832817);
    }
    
    public void method1091() {
	super.method1085(2010174177);
	((Class586) anInterface6_326).method9855((short) 155);
    }
    
    static final void method17246(Class669 class669, int i) {
	int i_2_
	    = class669.anIntArray8591[class669.anInt8613 * 662605117] >> 16;
	int i_3_
	    = class669.anIntArray8591[class669.anInt8613 * 662605117] & 0xffff;
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (i_4_ < 0 || i_4_ > 5000)
	    throw new RuntimeException();
	class669.anIntArray8589[i_2_] = i_4_;
	int i_5_ = -1;
	if (i_3_ == Class493.aClass493_5451.method93())
	    i_5_ = 0;
	for (int i_6_ = 0; i_6_ < i_4_; i_6_++)
	    class669.anIntArrayArray8590[i_2_][i_6_] = i_5_;
    }
    
    static final void method17247(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (Class599.aClass298_Sub1_7871.method5401(1199579521).anInt3352
	       * 964723235);
    }
    
    static final void method17248(Class669 class669, int i) {
	int i_7_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = class669.aClass352_8602.aStringArray3646[i_7_];
    }
}
