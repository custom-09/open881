/* Class44_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub3 extends Class44
{
    public Class44_Sub3(Class675 class675, Class672 class672, boolean bool,
			Class472 class472, Class472 class472_0_) {
	super(class675, class672, class472, Class649.aClass649_8387, 64,
	      new Class304_Sub1(bool, class472_0_, class672, class675));
    }
    
    public void method17249(boolean bool, byte i) {
	((Class304) anInterface6_326).method5579(bool, 910933751);
	super.method1080(65280);
    }
    
    public void method17250(int i, byte i_1_) {
	((Class304) anInterface6_326).method5580(i, (byte) 34);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class304) anInterface6_326).method5592(-921450818);
    }
    
    public void method1081(int i, short i_2_) {
	super.method1081(i, (short) 16578);
	((Class304) anInterface6_326).method5582(i, (byte) 111);
    }
    
    public void method1085(int i) {
	super.method1085(1916700627);
	((Class304) anInterface6_326).method5583(-1270448339);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 29002);
	((Class304) anInterface6_326).method5582(i, (byte) 7);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class304) anInterface6_326).method5592(-484790323);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class304) anInterface6_326).method5592(-234512115);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class304) anInterface6_326).method5592(-1601994917);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class304) anInterface6_326).method5592(-1062160436);
    }
    
    public void method1087() {
	super.method1085(1980267948);
	((Class304) anInterface6_326).method5583(-2073707083);
    }
    
    public void method1088() {
	super.method1085(2115475927);
	((Class304) anInterface6_326).method5583(164629360);
    }
    
    public void method17251(boolean bool) {
	((Class304) anInterface6_326).method5579(bool, -1770545780);
	super.method1080(65280);
    }
    
    public void method1089() {
	super.method1085(2051728621);
	((Class304) anInterface6_326).method5583(-1846681467);
    }
    
    public void method17252(int i) {
	((Class304) anInterface6_326).method5580(i, (byte) 15);
    }
    
    public void method1091() {
	super.method1085(2081720680);
	((Class304) anInterface6_326).method5583(1483702896);
    }
    
    public void method17253(boolean bool) {
	((Class304) anInterface6_326).method5579(bool, -75320787);
	super.method1080(65280);
    }
    
    public void method17254(boolean bool) {
	((Class304) anInterface6_326).method5579(bool, -1552839749);
	super.method1080(65280);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 10372);
	((Class304) anInterface6_326).method5582(i, (byte) 87);
    }
    
    public void method17255(int i) {
	((Class304) anInterface6_326).method5580(i, (byte) -94);
    }
    
    public void method1090() {
	super.method1085(2112805602);
	((Class304) anInterface6_326).method5583(-38505181);
    }
    
    static final void method17256(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class43.method1074(class247, class669, -2071984500);
    }
}
