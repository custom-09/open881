/* Class450 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class450
{
    Class696 aClass696_4914;
    static final int anInt4915 = 500;
    static final int anInt4916 = 500;
    static final int anInt4917 = 102400;
    static final int anInt4918 = 5;
    static final int anInt4919 = 5;
    Class696 aClass696_4920 = new Class696();
    static final int anInt4921 = 4;
    Class696 aClass696_4922;
    Class696 aClass696_4923;
    public volatile int anInt4924;
    int anInt4925;
    long aLong4926;
    byte aByte4927;
    public volatile int anInt4928;
    public volatile int anInt4929;
    public volatile int anInt4930;
    Class534_Sub18_Sub18_Sub2 aClass534_Sub18_Sub18_Sub2_4931;
    Class534_Sub40 aClass534_Sub40_4932;
    Class534_Sub40 aClass534_Sub40_4933;
    int anInt4934;
    Class534_Sub40 aClass534_Sub40_4935;
    
    public abstract void method7331();
    
    Class534_Sub18_Sub18_Sub2 method7332(int i, int i_0_, byte i_1_,
					 boolean bool, int i_2_) {
	long l = (long) i_0_ + ((long) i << 32);
	Class534_Sub18_Sub18_Sub2 class534_sub18_sub18_sub2
	    = new Class534_Sub18_Sub18_Sub2();
	class534_sub18_sub18_sub2.aLong10509 = l * 84410810002887935L;
	class534_sub18_sub18_sub2.aByte12133 = i_1_;
	class534_sub18_sub18_sub2.aBool11886 = bool;
	if (bool) {
	    if (method7336(1312605679) >= 500)
		throw new RuntimeException();
	    aClass696_4920.method14076(class534_sub18_sub18_sub2, (byte) 54);
	} else {
	    if (method7344(-1624674312) >= 500)
		throw new RuntimeException();
	    aClass696_4922.method14076(class534_sub18_sub18_sub2, (byte) 84);
	}
	return class534_sub18_sub18_sub2;
    }
    
    abstract void method7333(int i, int i_3_);
    
    public boolean method7334(int i) {
	return method7336(-1362838901) >= 500;
    }
    
    public abstract void method7335(Object object, boolean bool, byte i);
    
    public int method7336(int i) {
	return (aClass696_4920.method14081((byte) 1)
		+ aClass696_4914.method14081((byte) 1));
    }
    
    Class450() {
	aClass696_4914 = new Class696();
	aClass696_4922 = new Class696();
	aClass696_4923 = new Class696();
	aClass534_Sub40_4935 = new Class534_Sub40(6);
	aByte4927 = (byte) 0;
	anInt4928 = 0;
	anInt4929 = 0;
	anInt4930 = -624355545;
	anInt4924 = 1826868259;
	aClass534_Sub40_4932 = new Class534_Sub40(5);
	aClass534_Sub40_4933 = new Class534_Sub40(5);
	anInt4934 = 0;
	aClass534_Sub18_Sub18_Sub2_4931 = null;
    }
    
    public abstract boolean method7337(int i);
    
    public abstract void method7338(boolean bool, int i);
    
    int method7339() {
	return (aClass696_4922.method14081((byte) 1)
		+ aClass696_4923.method14081((byte) 1));
    }
    
    public boolean method7340(byte i) {
	return method7344(-475155123) >= 500;
    }
    
    public abstract void method7341(int i);
    
    public abstract void method7342(byte i);
    
    Class534_Sub18_Sub18_Sub2 method7343(int i, int i_4_, byte i_5_,
					 boolean bool) {
	long l = (long) i_4_ + ((long) i << 32);
	Class534_Sub18_Sub18_Sub2 class534_sub18_sub18_sub2
	    = new Class534_Sub18_Sub18_Sub2();
	class534_sub18_sub18_sub2.aLong10509 = l * 84410810002887935L;
	class534_sub18_sub18_sub2.aByte12133 = i_5_;
	class534_sub18_sub18_sub2.aBool11886 = bool;
	if (bool) {
	    if (method7336(1055275417) >= 500)
		throw new RuntimeException();
	    aClass696_4920.method14076(class534_sub18_sub18_sub2, (byte) 66);
	} else {
	    if (method7344(-2096185168) >= 500)
		throw new RuntimeException();
	    aClass696_4922.method14076(class534_sub18_sub18_sub2, (byte) 98);
	}
	return class534_sub18_sub18_sub2;
    }
    
    int method7344(int i) {
	return (aClass696_4922.method14081((byte) 1)
		+ aClass696_4923.method14081((byte) 1));
    }
    
    public boolean method7345() {
	return method7344(-693109739) >= 500;
    }
    
    public boolean method7346() {
	return method7344(-1123332187) >= 500;
    }
    
    public boolean method7347() {
	return method7336(-1439917082) >= 500;
    }
    
    public boolean method7348() {
	return method7336(792001808) >= 500;
    }
    
    int method7349() {
	return (aClass696_4922.method14081((byte) 1)
		+ aClass696_4923.method14081((byte) 1));
    }
    
    public abstract boolean method7350();
    
    public int method7351() {
	return (aClass696_4920.method14081((byte) 1)
		+ aClass696_4914.method14081((byte) 1));
    }
    
    public abstract void method7352();
    
    public abstract void method7353(boolean bool);
    
    abstract void method7354(int i, int i_6_, byte i_7_);
    
    public abstract void method7355(Object object, boolean bool);
    
    public abstract void method7356(Object object, boolean bool);
    
    public abstract void method7357(int i);
    
    public abstract void method7358();
    
    public abstract void method7359();
    
    public abstract void method7360();
    
    public abstract void method7361();
    
    public abstract void method7362();
    
    public abstract void method7363();
    
    public boolean method7364() {
	return method7344(-494113630) >= 500;
    }
    
    public abstract void method7365(Object object, boolean bool);
    
    public abstract void method7366();
    
    public abstract void method7367();
    
    public abstract void method7368();
    
    static final void method7369(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub3_10767
		  .method16837((byte) 110) ? 1 : 0;
    }
    
    public static Class534_Sub18_Sub14 method7370(int i, int i_8_, int i_9_) {
	Class270 class270
	    = (Class270) Class274.aMap3037.get(Integer.valueOf(i));
	return class270.method5029(i_8_, 330642735);
    }
}
