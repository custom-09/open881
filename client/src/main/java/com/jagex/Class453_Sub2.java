/* Class453_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

final class Class453_Sub2 extends Class453
{
    static int anInt10347;
    
    Object method7398(Class150 class150) {
	if (Class493.aClass493_5345 == class150.aClass493_1696)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 32);
    }
    
    Class453_Sub2(Class649 class649, int i, boolean bool, boolean bool_0_) {
	super(class649, i, bool, bool_0_);
    }
    
    Object method7395(Class150 class150, int i) {
	if (Class493.aClass493_5345 == class150.aClass493_1696)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 25);
    }
    
    Object method7400(Class150 class150) {
	if (Class493.aClass493_5345 == class150.aClass493_1696)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 1);
    }
    
    static final void method15972(Class669 class669, byte i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub1_10762,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]) == 1 ? 1 : 0,
	     156616325);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
}
