/* Class453_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

final class Class453_Sub3 extends Class453
{
    Class453_Sub3(Class649 class649, int i, boolean bool, boolean bool_0_) {
	super(class649, i, bool, bool_0_);
    }
    
    Object method7400(Class150 class150) {
	if (class150.aClass493_1696 == Class493.aClass493_5345)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 71);
    }
    
    Object method7395(Class150 class150, int i) {
	if (class150.aClass493_1696 == Class493.aClass493_5345)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 99);
    }
    
    Object method7398(Class150 class150) {
	if (class150.aClass493_1696 == Class493.aClass493_5345)
	    return Integer.valueOf(-1);
	return class150.aClass493_1696.method8104((byte) 72);
    }
    
    static void method15985(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method15986(byte i) {
	Class99.aClass203_1168.method3884((byte) -55);
    }
    
    static void method15987(Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class296.method5330(1975799748);
    }
}
