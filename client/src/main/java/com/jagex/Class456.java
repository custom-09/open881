/* Class456 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class456
{
    public static final int anInt4964 = 4;
    public static final int anInt4965 = 5;
    public static final int anInt4966 = 2;
    public static final int anInt4967 = 3;
    public static final int anInt4968 = 6;
    public static final int anInt4969 = 1;
    
    Class456() throws Throwable {
	throw new Error();
    }
    
    public static void method7425(int i, int i_0_) {
	Class175.method2925(i, 1089546520);
    }
    
    public static void method7426(boolean bool, boolean bool_1_, int i) {
	if (bool) {
	    Class661.anInt8547 += 194044063;
	    Class504.method8319(-1480439696);
	}
	if (bool_1_) {
	    Class661.anInt8550 += 555410307;
	    Class655.method10852((byte) 68);
	}
    }
    
    static final void method7427(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class386.method6494(class247, class669, -1482226719);
    }
}
