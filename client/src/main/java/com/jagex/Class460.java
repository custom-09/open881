/* Class460 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class460
{
    public boolean aBool4981;
    Class44_Sub12 aClass44_Sub12_4982;
    protected Class468 aClass468_4983;
    protected Class556 aClass556_4984;
    static int[][] anIntArrayArray4985;
    public int anInt4986 = 0;
    int[] anIntArray4987;
    byte[][][] aByteArrayArrayArray4988;
    protected byte[][][] aByteArrayArrayArray4989;
    public boolean aBool4990;
    public int anInt4991;
    protected int anInt4992;
    int[] anIntArray4993;
    static int[][] anIntArrayArray4994;
    public int[][][] anIntArrayArrayArray4995;
    Class44_Sub16 aClass44_Sub16_4996;
    protected boolean aBool4997;
    public boolean aBool4998;
    int[] anIntArray4999;
    short[][][] aShortArrayArrayArray5000;
    short[][][] aShortArrayArrayArray5001;
    public boolean aBool5002;
    int[] anIntArray5003;
    int[] anIntArray5004;
    int anInt5005;
    static final int anInt5006 = 0;
    int[] anIntArray5007;
    static final int anInt5008 = 0;
    static final int anInt5009 = 1;
    static final int anInt5010 = 2;
    static boolean[][] aBoolArrayArray5011;
    static final int anInt5012 = 1;
    static final int anInt5013 = 2;
    static final int anInt5014 = 4;
    static final int anInt5015 = 8;
    static final int anInt5016 = 3;
    static final int anInt5017 = 32;
    static final int anInt5018 = 64;
    static final int anInt5019 = 128;
    static final int anInt5020 = 256;
    static boolean[][] aBoolArrayArray5021;
    static int[] anIntArray5022
	= { 2, 1, 1, 1, 2, 2, 2, 1, 3, 3, 3, 2, 0, 4, 0 };
    static int[] anIntArray5023
	= { 0, 1, 2, 2, 1, 1, 2, 3, 1, 3, 3, 4, 2, 0, 4 };
    static int[] anIntArray5024 = { 4, 2, 1, 1, 2, 2, 3, 1, 3, 3, 3, 2, 0 };
    static int[][] anIntArrayArray5025;
    static int[] anIntArray5026;
    int anInt5027;
    static int[] anIntArray5028;
    static int[] anIntArray5029;
    static int[][] anIntArrayArray5030;
    static boolean[][] aBoolArrayArray5031;
    static int[][] anIntArrayArray5032;
    int[] anIntArray5033;
    static int[][] anIntArrayArray5034;
    int[] anIntArray5035;
    static int[][] anIntArrayArray5036;
    static int[][] anIntArrayArray5037;
    static int[][] anIntArrayArray5038;
    static int[] anIntArray5039 = { 0, 2, 2, 2, 1, 1, 3, 3, 1, 3, 3, 4, 4 };
    static boolean[][] aBoolArrayArray5040;
    static int[][] anIntArrayArray5041;
    static int[][] anIntArrayArray5042;
    public boolean aBool5043 = false;
    int[] anIntArray5044;
    static final int anInt5045 = 16;
    int anInt5046;
    int[] anIntArray5047;
    static final int anInt5048 = 512;
    boolean aBool5049;
    int[] anIntArray5050;
    protected int anInt5051;
    int anInt5052;
    int anInt5053;
    int[] anIntArray5054;
    int anInt5055;
    int anInt5056;
    int anInt5057;
    byte[][][] aByteArrayArrayArray5058;
    static int[] anIntArray5059;
    boolean aBool5060;
    boolean aBool5061;
    boolean aBool5062;
    int[] anIntArray5063;
    int anInt5064;
    int[] anIntArray5065;
    int[] anIntArray5066;
    public byte[][][] aByteArrayArrayArray5067;
    int[] anIntArray5068;
    static int anInt5069;
    
    public final void method7445(Class534_Sub40 class534_sub40, int i,
				 int i_0_, int i_1_, int i_2_) {
	int i_3_ = i + i_1_;
	int i_4_ = i_2_ + i_0_;
	for (int i_5_ = 0; i_5_ < -692901467 * anInt4991; i_5_++) {
	    for (int i_6_ = 0; i_6_ < 64; i_6_++) {
		for (int i_7_ = 0; i_7_ < 64; i_7_++)
		    method7493(class534_sub40, i_5_, i + i_6_, i_0_ + i_7_, 0,
			       0, i_6_ + i_3_, i_4_ + i_7_, 0, false,
			       414042331);
	    }
	}
    }
    
    public void method7446(int i) {
	aBool4997 = true;
    }
    
    void method7447(Class185 class185, Class651 class651, Class1 class1, int i,
		    int i_8_, byte[][] is, byte[][] is_9_, short[][] is_10_,
		    boolean[] bools) {
	boolean[] bools_11_ = (null != class651 && class651.aBool8474
			       ? aBoolArrayArray5040[anInt5052 * -648787169]
			       : aBoolArrayArray5021[-648787169 * anInt5052]);
	method7464(class185, class651, class1, i, i_8_, -60640777 * anInt4992,
		   anInt5051 * -1584311401, is_10_, is, is_9_, bools,
		   -464661091);
	aBool5062 = class651 != null && (class651.anInt8467 * -2044484027
					 != class651.anInt8470 * 33386845);
	if (!aBool5062) {
	    for (int i_12_ = 0; i_12_ < 8; i_12_++) {
		if (anIntArray5050[i_12_] >= 0
		    && anIntArray5047[i_12_] != anIntArray5033[i_12_]) {
		    aBool5062 = true;
		    break;
		}
	    }
	}
	if (!bools_11_[anInt5053 * -1693107127 + 1 & 0x3]) {
	    boolean[] bools_13_ = bools;
	    int i_14_ = 1;
	    bools_13_[i_14_] = bools_13_[i_14_] | 0 == (anIntArray4993[2]
							& anIntArray4993[4]);
	}
	if (!bools_11_[anInt5053 * -1693107127 + 3 & 0x3]) {
	    boolean[] bools_15_ = bools;
	    int i_16_ = 3;
	    bools_15_[i_16_] = bools_15_[i_16_] | 0 == (anIntArray4993[6]
							& anIntArray4993[0]);
	}
	if (!bools_11_[-1693107127 * anInt5053 + 0 & 0x3]) {
	    boolean[] bools_17_ = bools;
	    int i_18_ = 0;
	    bools_17_[i_18_] = bools_17_[i_18_] | 0 == (anIntArray4993[0]
							& anIntArray4993[2]);
	}
	if (!bools_11_[-1693107127 * anInt5053 + 2 & 0x3]) {
	    boolean[] bools_19_ = bools;
	    int i_20_ = 2;
	    bools_19_[i_20_] = bools_19_[i_20_] | (anIntArray4993[4]
						   & anIntArray4993[6]) == 0;
	}
	if (!aBool5060
	    && (-648787169 * anInt5052 == 0 || anInt5052 * -648787169 == 12)) {
	    if (bools[0] && !bools[1] && !bools[2] && bools[3]) {
		boolean[] bools_21_ = bools;
		bools[3] = false;
		bools_21_[0] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = 0;
	    } else if (bools[0] && bools[1] && !bools[2] && !bools[3]) {
		boolean[] bools_22_ = bools;
		bools[1] = false;
		bools_22_[0] = false;
		anInt5052
		    = (-648787169 * anInt5052 == 0 ? 13 : 14) * 1068200159;
		anInt5053 = 911892971;
	    } else if (!bools[0] && bools[1] && bools[2] && !bools[3]) {
		boolean[] bools_23_ = bools;
		bools[2] = false;
		bools_23_[1] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = -823727118;
	    } else if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
		boolean[] bools_24_ = bools;
		bools[3] = false;
		bools_24_[2] = false;
		anInt5052
		    = 1068200159 * (0 == -648787169 * anInt5052 ? 13 : 14);
		anInt5053 = 1735620089;
	    }
	}
    }
    
    public final void method7448(int i, int i_25_, int i_26_, int i_27_,
				 byte i_28_) {
	for (int i_29_ = 0; i_29_ < -692901467 * anInt4991; i_29_++)
	    method7449(i_29_, i, i_25_, i_26_, i_27_, -904088808);
    }
    
    public final void method7449(int i, int i_30_, int i_31_, int i_32_,
				 int i_33_, int i_34_) {
	for (int i_35_ = i_31_; i_35_ < i_33_ + i_31_; i_35_++) {
	    for (int i_36_ = i_30_; i_36_ < i_32_ + i_30_; i_36_++) {
		if (i_36_ >= 0 && i_36_ < -60640777 * anInt4992 && i_35_ >= 0
		    && i_35_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_36_][i_35_]
			= i > 0 ? (anIntArrayArrayArray4995[i - 1][i_36_]
				   [i_35_]) - 960 : 0;
	    }
	}
	if (i_30_ > 0 && i_30_ < anInt4992 * -60640777) {
	    for (int i_37_ = 1 + i_31_; i_37_ < i_33_ + i_31_; i_37_++) {
		if (i_37_ >= 0 && i_37_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_30_][i_37_]
			= anIntArrayArrayArray4995[i][i_30_ - 1][i_37_];
	    }
	}
	if (i_31_ > 0 && i_31_ < anInt5051 * -1584311401) {
	    for (int i_38_ = 1 + i_30_; i_38_ < i_30_ + i_32_; i_38_++) {
		if (i_38_ >= 0 && i_38_ < -60640777 * anInt4992)
		    anIntArrayArrayArray4995[i][i_38_][i_31_]
			= anIntArrayArrayArray4995[i][i_38_][i_31_ - 1];
	    }
	}
	if (i_30_ >= 0 && i_31_ >= 0 && i_30_ < -60640777 * anInt4992
	    && i_31_ < anInt5051 * -1584311401) {
	    if (i == 0) {
		if (i_30_ > 0
		    && 0 != anIntArrayArrayArray4995[i][i_30_ - 1][i_31_])
		    anIntArrayArrayArray4995[i][i_30_][i_31_]
			= anIntArrayArrayArray4995[i][i_30_ - 1][i_31_];
		else if (i_31_ > 0
			 && 0 != anIntArrayArrayArray4995[i][i_30_][i_31_ - 1])
		    anIntArrayArrayArray4995[i][i_30_][i_31_]
			= anIntArrayArrayArray4995[i][i_30_][i_31_ - 1];
		else if (i_30_ > 0 && i_31_ > 0
			 && (anIntArrayArrayArray4995[i][i_30_ - 1][i_31_ - 1]
			     != 0))
		    anIntArrayArrayArray4995[i][i_30_][i_31_]
			= anIntArrayArrayArray4995[i][i_30_ - 1][i_31_ - 1];
	    } else if (i_30_ > 0
		       && (anIntArrayArrayArray4995[i - 1][i_30_ - 1][i_31_]
			   != anIntArrayArrayArray4995[i][i_30_ - 1][i_31_]))
		anIntArrayArrayArray4995[i][i_30_][i_31_]
		    = anIntArrayArrayArray4995[i][i_30_ - 1][i_31_];
	    else if (i_31_ > 0
		     && (anIntArrayArrayArray4995[i][i_30_][i_31_ - 1]
			 != anIntArrayArrayArray4995[i - 1][i_30_][i_31_ - 1]))
		anIntArrayArrayArray4995[i][i_30_][i_31_]
		    = anIntArrayArrayArray4995[i][i_30_][i_31_ - 1];
	    else if (i_30_ > 0 && i_31_ > 0
		     && (anIntArrayArrayArray4995[i - 1][i_30_ - 1][i_31_ - 1]
			 != anIntArrayArrayArray4995[i][i_30_ - 1][i_31_ - 1]))
		anIntArrayArrayArray4995[i][i_30_][i_31_]
		    = anIntArrayArrayArray4995[i][i_30_ - 1][i_31_ - 1];
	}
    }
    
    public final void method7450(Class534_Sub40 class534_sub40, int i,
				 int i_39_, int i_40_, int i_41_, int i_42_) {
	int i_43_ = i + i_40_;
	int i_44_ = i_41_ + i_39_;
	for (int i_45_ = 0; i_45_ < -692901467 * anInt4991; i_45_++) {
	    for (int i_46_ = 0; i_46_ < 64; i_46_++) {
		for (int i_47_ = 0; i_47_ < 64; i_47_++)
		    method7493(class534_sub40, i_45_, i + i_46_, i_39_ + i_47_,
			       0, 0, i_46_ + i_43_, i_44_ + i_47_, 0, false,
			       414042331);
	    }
	}
    }
    
    public final void method7451(Class534_Sub40 class534_sub40, int i,
				 int i_48_, int i_49_, int i_50_, int i_51_,
				 int i_52_, int i_53_, int i_54_) {
	int i_55_ = (i_51_ & 0x7) * 8;
	int i_56_ = 8 * (i_52_ & 0x7);
	int i_57_ = (i_51_ & ~0x7) << 3;
	int i_58_ = (i_52_ & ~0x7) << 3;
	int i_59_ = 0;
	int i_60_ = 0;
	if (1 == i_53_)
	    i_60_ = 1;
	else if (2 == i_53_) {
	    i_59_ = 1;
	    i_60_ = 1;
	} else if (i_53_ == 3)
	    i_59_ = 1;
	for (int i_61_ = 0; i_61_ < -692901467 * anInt4991; i_61_++) {
	    for (int i_62_ = 0; i_62_ < 64; i_62_++) {
		for (int i_63_ = 0; i_63_ < 64; i_63_++) {
		    if (i_61_ == i_50_ && i_62_ >= i_55_ && i_62_ <= 8 + i_55_
			&& i_63_ >= i_56_ && i_63_ <= i_56_ + 8) {
			int i_64_;
			int i_65_;
			if (i_62_ == i_55_ + 8 || i_63_ == 8 + i_56_) {
			    if (0 == i_53_) {
				i_64_ = i_62_ - i_55_ + i_48_;
				i_65_ = i_49_ + (i_63_ - i_56_);
			    } else if (1 == i_53_) {
				i_64_ = i_48_ + (i_63_ - i_56_);
				i_65_ = i_49_ + 8 - (i_62_ - i_55_);
			    } else if (i_53_ == 2) {
				i_64_ = i_48_ + 8 - (i_62_ - i_55_);
				i_65_ = 8 + i_49_ - (i_63_ - i_56_);
			    } else {
				i_64_ = i_48_ + 8 - (i_63_ - i_56_);
				i_65_ = i_49_ + (i_62_ - i_55_);
			    }
			    method7493(class534_sub40, i, i_64_, i_65_, 0, 0,
				       i_62_ + i_57_, i_58_ + i_63_, 0, true,
				       414042331);
			} else {
			    i_64_ = i_48_ + Class644.method10681(i_62_ & 0x7,
								 i_63_ & 0x7,
								 i_53_,
								 (byte) 28);
			    i_65_ = i_49_ + Class47.method1128(i_62_ & 0x7,
							       i_63_ & 0x7,
							       i_53_,
							       1238663606);
			    method7493(class534_sub40, i, i_64_, i_65_, i_59_,
				       i_60_, i_62_ + i_57_, i_58_ + i_63_,
				       i_53_, false, 414042331);
			}
			if (i_62_ == 63 || i_63_ == 63) {
			    int i_66_ = 1;
			    if (i_62_ == 63 && 63 == i_63_)
				i_66_ = 3;
			    for (int i_67_ = 0; i_67_ < i_66_; i_67_++) {
				int i_68_ = i_62_;
				int i_69_ = i_63_;
				if (i_67_ == 0) {
				    i_68_ = i_62_ == 63 ? 64 : i_62_;
				    i_69_ = 63 == i_63_ ? 64 : i_63_;
				} else if (1 == i_67_)
				    i_68_ = 64;
				else if (2 == i_67_)
				    i_69_ = 64;
				int i_70_;
				int i_71_;
				if (0 == i_53_) {
				    i_70_ = i_68_ - i_55_ + i_48_;
				    i_71_ = i_69_ - i_56_ + i_49_;
				} else if (1 == i_53_) {
				    i_70_ = i_48_ + (i_69_ - i_56_);
				    i_71_ = i_49_ + 8 - (i_68_ - i_55_);
				} else if (2 == i_53_) {
				    i_70_ = 8 + i_48_ - (i_68_ - i_55_);
				    i_71_ = 8 + i_49_ - (i_69_ - i_56_);
				} else {
				    i_70_ = i_48_ + 8 - (i_69_ - i_56_);
				    i_71_ = i_49_ + (i_68_ - i_55_);
				}
				if (i_70_ >= 0 && i_70_ < anInt4992 * -60640777
				    && i_71_ >= 0
				    && i_71_ < anInt5051 * -1584311401)
				    anIntArrayArrayArray4995[i][i_70_][i_71_]
					= (anIntArrayArrayArray4995[i]
					   [i_59_ + i_64_][i_60_ + i_65_]);
			    }
			}
		    } else
			method7493(class534_sub40, 0, -1, -1, 0, 0, 0, 0, 0,
				   false, 414042331);
		}
	    }
	}
    }
    
    public final void method7452(int i, int i_72_, int i_73_, int i_74_) {
	for (int i_75_ = 0; i_75_ < -692901467 * anInt4991; i_75_++)
	    method7449(i_75_, i, i_72_, i_73_, i_74_, -904088808);
    }
    
    static final int method7453(int i, int i_76_) {
	int i_77_ = i_76_ * 57 + i;
	i_77_ = i_77_ << 13 ^ i_77_;
	int i_78_ = (1376312589 + i_77_ * (i_77_ * i_77_ * 15731 + 789221)
		     & 0x7fffffff);
	return i_78_ >> 19 & 0xff;
    }
    
    public void method7454(Class185 class185, int[][][] is, int i) {
	for (int i_79_ = 0; i_79_ < -692901467 * anInt4991; i_79_++) {
	    int i_80_ = 0;
	    int i_81_ = 0;
	    if (!aBool5002) {
		if (aBool5043)
		    i_81_ |= 0x8;
		if (aBool4998)
		    i_80_ |= 0x2;
		if (1733778175 * anInt4986 != 0) {
		    i_80_ |= 0x1;
		    i_81_ |= 0x10;
		}
	    }
	    if (aBool4998)
		i_81_ |= 0x7;
	    if (!aBool4990)
		i_81_ |= 0x20;
	    int[][] is_82_ = (null != is && i_79_ < is.length ? is[i_79_]
			      : anIntArrayArrayArray4995[i_79_]);
	    aClass556_4984.method9309
		(i_79_,
		 class185.method3332(-60640777 * anInt4992,
				     -1584311401 * anInt5051,
				     anIntArrayArrayArray4995[i_79_], is_82_,
				     512, i_80_, i_81_),
		 1158240888);
	}
    }
    
    public final void method7455(Class185 class185, Class151 class151,
				 Class151 class151_83_, byte i) {
	int[][] is = new int[-60640777 * anInt4992][-1584311401 * anInt5051];
	if (null == anIntArray5003
	    || -1584311401 * anInt5051 != anIntArray5003.length) {
	    anIntArray5003 = new int[anInt5051 * -1584311401];
	    anIntArray5004 = new int[anInt5051 * -1584311401];
	    anIntArray4999 = new int[-1584311401 * anInt5051];
	    anIntArray5035 = new int[anInt5051 * -1584311401];
	    anIntArray5063 = new int[anInt5051 * -1584311401];
	}
	for (int i_84_ = 0; i_84_ < anInt4991 * -692901467; i_84_++) {
	    for (int i_85_ = 0; i_85_ < -1584311401 * anInt5051; i_85_++) {
		anIntArray5003[i_85_] = 0;
		anIntArray5004[i_85_] = 0;
		anIntArray4999[i_85_] = 0;
		anIntArray5035[i_85_] = 0;
		anIntArray5063[i_85_] = 0;
	    }
	    for (int i_86_ = -5; i_86_ < anInt4992 * -60640777; i_86_++) {
		for (int i_87_ = 0; i_87_ < -1584311401 * anInt5051; i_87_++) {
		    int i_88_ = 5 + i_86_;
		    if (i_88_ < -60640777 * anInt4992) {
			int i_89_
			    = (aShortArrayArrayArray5000[i_84_][i_88_][i_87_]
			       & 0x7fff);
			if (i_89_ > 0) {
			    Class1 class1
				= ((Class1)
				   aClass44_Sub12_4982.method91(i_89_ - 1,
								-1785597925));
			    anIntArray5003[i_87_]
				+= 1782063805 * class1.anInt11;
			    anIntArray5004[i_87_]
				+= class1.anInt14 * 1312863965;
			    anIntArray4999[i_87_]
				+= 1484599057 * class1.anInt16;
			    anIntArray5035[i_87_]
				+= class1.anInt17 * -1721639561;
			    anIntArray5063[i_87_]++;
			}
		    }
		    int i_90_ = i_86_ - 5;
		    if (i_90_ >= 0) {
			int i_91_
			    = (aShortArrayArrayArray5000[i_84_][i_90_][i_87_]
			       & 0x7fff);
			if (i_91_ > 0) {
			    Class1 class1
				= ((Class1)
				   aClass44_Sub12_4982.method91(i_91_ - 1,
								1098453917));
			    anIntArray5003[i_87_]
				-= 1782063805 * class1.anInt11;
			    anIntArray5004[i_87_]
				-= class1.anInt14 * 1312863965;
			    anIntArray4999[i_87_]
				-= 1484599057 * class1.anInt16;
			    anIntArray5035[i_87_]
				-= -1721639561 * class1.anInt17;
			    anIntArray5063[i_87_]--;
			}
		    }
		}
		if (i_86_ >= 0) {
		    int i_92_ = 0;
		    int i_93_ = 0;
		    int i_94_ = 0;
		    int i_95_ = 0;
		    int i_96_ = 0;
		    for (int i_97_ = -5; i_97_ < anInt5051 * -1584311401;
			 i_97_++) {
			int i_98_ = 5 + i_97_;
			if (i_98_ < -1584311401 * anInt5051) {
			    i_92_ += anIntArray5003[i_98_];
			    i_93_ += anIntArray5004[i_98_];
			    i_94_ += anIntArray4999[i_98_];
			    i_95_ += anIntArray5035[i_98_];
			    i_96_ += anIntArray5063[i_98_];
			}
			int i_99_ = i_97_ - 5;
			if (i_99_ >= 0) {
			    i_92_ -= anIntArray5003[i_99_];
			    i_93_ -= anIntArray5004[i_99_];
			    i_94_ -= anIntArray4999[i_99_];
			    i_95_ -= anIntArray5035[i_99_];
			    i_96_ -= anIntArray5063[i_99_];
			}
			if (i_97_ >= 0 && i_95_ > 0 && i_96_ > 0)
			    is[i_86_][i_97_]
				= Class200_Sub23.method15662((i_92_ * 256
							      / i_95_),
							     i_93_ / i_96_,
							     i_94_ / i_96_,
							     (byte) 3);
		    }
		}
	    }
	    if (aBool4981)
		method7461(class185, aClass556_4984.aClass151Array7432[i_84_],
			   i_84_, is, 0 == i_84_ ? class151 : null,
			   i_84_ == 0 ? class151_83_ : null, (byte) -57);
	    else
		method7456(class185, aClass556_4984.aClass151Array7432[i_84_],
			   i_84_, is, i_84_ == 0 ? class151 : null,
			   i_84_ == 0 ? class151_83_ : null, (byte) 8);
	    aShortArrayArrayArray5000[i_84_] = null;
	    aShortArrayArrayArray5001[i_84_] = null;
	    aByteArrayArrayArray5058[i_84_] = null;
	    aByteArrayArrayArray4988[i_84_] = null;
	}
	if (!aBool5002) {
	    if (anInt4986 * 1733778175 != 0)
		aClass556_4984.method9278(-40099963);
	    if (aBool4998)
		aClass556_4984.method9280((byte) -27);
	}
	for (int i_100_ = 0; i_100_ < -692901467 * anInt4991; i_100_++)
	    aClass556_4984.aClass151Array7432[i_100_].method2495();
    }
    
    void method7456(Class185 class185, Class151 class151, int i, int[][] is,
		    Class151 class151_101_, Class151 class151_102_,
		    byte i_103_) {
	for (int i_104_ = 0; i_104_ < anInt4992 * -60640777; i_104_++) {
	    for (int i_105_ = 0; i_105_ < anInt5051 * -1584311401; i_105_++) {
		byte i_106_ = aByteArrayArrayArray5058[i][i_104_][i_105_];
		byte i_107_ = aByteArrayArrayArray4988[i][i_104_][i_105_];
		int i_108_
		    = aShortArrayArrayArray5001[i][i_104_][i_105_] & 0x7fff;
		int i_109_
		    = aShortArrayArrayArray5000[i][i_104_][i_105_] & 0x7fff;
		Class651 class651
		    = ((Class651)
		       (i_108_ != 0
			? aClass44_Sub16_4996.method91(i_108_ - 1, 959073715)
			: null));
		Class1 class1
		    = ((Class1)
		       (i_109_ != 0
			? aClass44_Sub12_4982.method91(i_109_ - 1, -1770922818)
			: null));
		if (i_106_ == 0 && class651 == null)
		    i_106_ = (byte) 12;
		Class651 class651_110_ = class651;
		if (class651 != null && -1 == class651.anInt8467 * -2044484027
		    && -1 == class651.anInt8470 * 33386845) {
		    class651_110_ = class651;
		    class651 = null;
		}
		if (null != class651 || class1 != null) {
		    anInt5027 = anIntArray5023[i_106_] * -926932567;
		    anInt5064 = anIntArray5022[i_106_] * 106151493;
		    int i_111_
			= ((null != class1 ? anInt5027 * -1457646951 : 0)
			   + (null != class651 ? -1278434163 * anInt5064 : 0));
		    int i_112_ = 0;
		    anInt5053 = 0;
		    anInt5056 = -586067143 * (null != class651
					      ? class651.anInt8468 * 1884378951
					      : -1);
		    int i_113_
			= null != class1 ? 587899173 * class1.anInt10 : -1;
		    int[] is_114_ = new int[i_111_];
		    int[] is_115_ = new int[i_111_];
		    int[] is_116_ = new int[i_111_];
		    int[] is_117_ = new int[i_111_];
		    int[] is_118_ = new int[i_111_];
		    int[] is_119_ = new int[i_111_];
		    int[] is_120_ = ((class651 != null
				      && -1 != 33386845 * class651.anInt8470)
				     ? new int[i_111_] : null);
		    if (null != class651) {
			for (int i_121_ = 0; i_121_ < -1278434163 * anInt5064;
			     i_121_++) {
			    is_114_[i_112_] = (anIntArrayArray4985[i_106_]
					       [anInt5053 * -1693107127]);
			    is_115_[i_112_] = (anIntArrayArray4994[i_106_]
					       [-1693107127 * anInt5053]);
			    is_116_[i_112_] = (anIntArrayArray5034[i_106_]
					       [-1693107127 * anInt5053]);
			    is_118_[i_112_] = anInt5056 * -717903095;
			    is_119_[i_112_] = class651.anInt8471 * 431890869;
			    is_117_[i_112_] = class651.anInt8467 * -2044484027;
			    if (null != is_120_)
				is_120_[i_112_]
				    = 33386845 * class651.anInt8470;
			    i_112_++;
			    anInt5053 += 1735620089;
			}
			if (!aBool5002 && 0 == i)
			    aClass556_4984.method9261
				(i_104_, i_105_,
				 class651.anInt8475 * -1652506135,
				 class651.anInt8476 * 390419480,
				 class651.anInt8466 * -1594141697,
				 -672944661 * class651.anInt8465,
				 1112938727 * class651.anInt8479,
				 class651.anInt8480 * 1597465863, -500988450);
		    } else
			anInt5053 += 261586469 * anInt5064;
		    if (class1 != null) {
			for (int i_122_ = 0; i_122_ < -1457646951 * anInt5027;
			     i_122_++) {
			    is_114_[i_112_] = (anIntArrayArray4985[i_106_]
					       [-1693107127 * anInt5053]);
			    is_115_[i_112_] = (anIntArrayArray4994[i_106_]
					       [anInt5053 * -1693107127]);
			    is_116_[i_112_] = (anIntArrayArray5034[i_106_]
					       [anInt5053 * -1693107127]);
			    is_118_[i_112_] = i_113_;
			    is_119_[i_112_] = 161079521 * class1.anInt15;
			    is_117_[i_112_] = is[i_104_][i_105_];
			    if (is_120_ != null)
				is_120_[i_112_] = is_117_[i_112_];
			    i_112_++;
			    anInt5053 += 1735620089;
			}
		    }
		    int i_123_ = anIntArray5028.length;
		    int[] is_124_ = new int[i_123_];
		    int[] is_125_ = new int[i_123_];
		    int[] is_126_
			= null != class151_102_ ? new int[i_123_] : null;
		    int[] is_127_
			= (null != class151_102_ || class151_101_ != null
			   ? new int[i_123_] : null);
		    for (int i_128_ = 0; i_128_ < i_123_; i_128_++) {
			int i_129_ = anIntArray5028[i_128_];
			int i_130_ = anIntArray5029[i_128_];
			if (i_107_ == 0) {
			    is_124_[i_128_] = i_129_;
			    is_125_[i_128_] = i_130_;
			} else if (1 == i_107_) {
			    int i_131_ = i_129_;
			    is_124_[i_128_] = i_130_;
			    is_125_[i_128_] = 512 - i_131_;
			} else if (i_107_ == 2) {
			    is_124_[i_128_] = 512 - i_129_;
			    is_125_[i_128_] = 512 - i_130_;
			} else if (3 == i_107_) {
			    int i_132_ = i_129_;
			    is_124_[i_128_] = 512 - i_130_;
			    is_125_[i_128_] = i_132_;
			}
			if (is_126_ != null
			    && aBoolArrayArray5011[i_106_][i_128_]) {
			    int i_133_ = (i_104_ << 9) + is_124_[i_128_];
			    int i_134_ = is_125_[i_128_] + (i_105_ << 9);
			    is_126_[i_128_]
				= (class151_102_.method2498(i_133_, i_134_,
							    243870907)
				   - class151.method2498(i_133_, i_134_,
							 -787379836));
			}
			if (is_127_ != null) {
			    if (null != class151_102_
				&& !aBoolArrayArray5011[i_106_][i_128_]) {
				int i_135_ = (i_104_ << 9) + is_124_[i_128_];
				int i_136_ = is_125_[i_128_] + (i_105_ << 9);
				is_127_[i_128_]
				    = (class151.method2498(i_135_, i_136_,
							   -182432906)
				       - class151_102_.method2498(i_135_,
								  i_136_,
								  640231053));
			    } else if (class151_101_ != null
				       && !(aBoolArrayArray5031[i_106_]
					    [i_128_])) {
				int i_137_ = is_124_[i_128_] + (i_104_ << 9);
				int i_138_ = is_125_[i_128_] + (i_105_ << 9);
				is_127_[i_128_]
				    = (class151_101_.method2498(i_137_, i_138_,
								2133683326)
				       - class151.method2498(i_137_, i_138_,
							     31165436));
			    }
			}
		    }
		    int i_139_
			= class151.method2491(i_104_, i_105_, -280610960);
		    int i_140_
			= class151.method2491(i_104_ + 1, i_105_, 911901679);
		    int i_141_ = class151.method2491(1 + i_104_, 1 + i_105_,
						     1184820388);
		    int i_142_
			= class151.method2491(i_104_, i_105_ + 1, -1868646930);
		    boolean bool
			= aClass468_4983.method7612(i_104_, i_105_, (byte) 0);
		    if (bool && i > 1 || !bool && i > 0) {
			boolean bool_143_ = true;
			if (null != class1 && !class1.aBool13)
			    bool_143_ = false;
			else if (i_109_ == 0 && i_106_ != 0)
			    bool_143_ = false;
			else if (i_108_ > 0 && null != class651_110_
				 && !class651_110_.aBool8469)
			    bool_143_ = false;
			if (bool_143_ && i_139_ == i_140_ && i_139_ == i_141_
			    && i_142_ == i_139_)
			    aByteArrayArrayArray4989[i][i_104_][i_105_] |= 0x4;
		    }
		    Class166 class166 = new Class166();
		    if (aBool5002) {
			class166.anInt1763
			    = (aClass556_4984.method9305(i_104_, i_105_,
							 1508325175)
			       * 929497037);
			class166.anInt1762
			    = aClass556_4984.method9283(i_104_, i_105_,
							(byte) 2) * 1063224577;
			class166.anInt1764
			    = (aClass556_4984.method9331(i_104_, i_105_,
							 1940346961)
			       * -1101271945);
			class166.anInt1765
			    = (aClass556_4984.method9238(i_104_, i_105_,
							 (byte) -27)
			       * -1961234339);
			class166.anInt1766
			    = (aClass556_4984.method9239(i_104_, i_105_,
							 158258554)
			       * -1338809385);
			class166.anInt1767
			    = (aClass556_4984.method9310(i_104_, i_105_,
							 (short) -10455)
			       * -2041662327);
		    }
		    class151.method2494(i_104_, i_105_, is_124_, is_126_,
					is_125_, is_127_, is_114_, is_115_,
					is_116_, is_117_, is_120_, is_118_,
					is_119_, class166, false);
		    aClass556_4984.method9367(i, i_104_, i_105_, -1876172263);
		}
	    }
	}
    }
    
    public final void method7457(Class534_Sub40 class534_sub40, int i,
				 int i_144_, int i_145_, int i_146_) {
	int i_147_ = i + i_145_;
	int i_148_ = i_146_ + i_144_;
	for (int i_149_ = 0; i_149_ < -692901467 * anInt4991; i_149_++) {
	    for (int i_150_ = 0; i_150_ < 64; i_150_++) {
		for (int i_151_ = 0; i_151_ < 64; i_151_++)
		    method7493(class534_sub40, i_149_, i + i_150_,
			       i_144_ + i_151_, 0, 0, i_150_ + i_147_,
			       i_148_ + i_151_, 0, false, 414042331);
	    }
	}
    }
    
    void method7458(Class185 class185, int i, int i_152_, int i_153_,
		    Class651 class651, boolean[] bools, int[] is,
		    int[] is_154_, int[] is_155_, int[] is_156_, int[] is_157_,
		    int[] is_158_, int[] is_159_, int[] is_160_,
		    Class151 class151, Class151 class151_161_,
		    Class151 class151_162_) {
	anInt5046 = 1053793721;
	anInt5056 = 586067143;
	anInt5057 = 1487773952;
	if (class651 != null) {
	    anInt5046 = 192513827 * class651.anInt8467;
	    anInt5056 = 2015327183 * class651.anInt8468;
	    anInt5057 = 1989313493 * class651.anInt8471;
	    int i_163_ = Class689.method14014(class185, class651, -1922263784);
	    for (int i_164_ = 0; i_164_ < anInt5064 * -1278434163; i_164_++) {
		boolean bool = false;
		int i_165_;
		if (bools[0 - anInt5053 * -1693107127 & 0x3]
		    && anIntArray5068[0] == anInt5005 * 1299058195) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 1;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 1;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_165_ = 6;
		} else if (bools[2 - anInt5053 * -1693107127 & 0x3]
			   && anIntArray5068[2] == 1299058195 * anInt5005) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 5;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 5;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_165_ = 6;
		} else if (bools[1 - anInt5053 * -1693107127 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[1]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 3;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 3;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_165_ = 6;
		} else if (bools[3 - -1693107127 * anInt5053 & 0x3]
			   && anInt5005 * 1299058195 == anIntArray5068[3]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 7;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 7;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_165_ = 6;
		} else {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    i_165_ = 3;
		}
		for (int i_166_ = 0; i_166_ < i_165_; i_166_++) {
		    int i_167_ = anIntArray5044[i_166_];
		    int i_168_ = i_167_ - 908753042 * anInt5053 & 0x7;
		    int i_169_ = anIntArray5028[i_167_];
		    int i_170_ = anIntArray5029[i_167_];
		    int i_171_;
		    int i_172_;
		    if (anInt5053 * -1693107127 == 1) {
			i_171_ = i_170_;
			i_172_ = 512 - i_169_;
		    } else if (anInt5053 * -1693107127 == 2) {
			i_171_ = 512 - i_169_;
			i_172_ = 512 - i_170_;
		    } else if (-1693107127 * anInt5053 == 3) {
			i_171_ = 512 - i_170_;
			i_172_ = i_169_;
		    } else {
			i_171_ = i_169_;
			i_172_ = i_170_;
		    }
		    is_154_[anInt5055 * 551530869] = i_171_;
		    is_155_[551530869 * anInt5055] = i_172_;
		    if (null != is_159_
			&& (aBoolArrayArray5011[anInt5052 * -648787169]
			    [i_167_])) {
			int i_173_ = i_171_ + (i_152_ << 9);
			int i_174_ = (i_153_ << 9) + i_172_;
			is_159_[551530869 * anInt5055]
			    = (class151_161_.method2498(i_173_, i_174_,
							1191550795)
			       - class151.method2498(i_173_, i_174_,
						     1654132786));
		    }
		    if (is_160_ != null) {
			if (null != class151_161_
			    && !(aBoolArrayArray5011[anInt5052 * -648787169]
				 [i_167_])) {
			    int i_175_ = i_171_ + (i_152_ << 9);
			    int i_176_ = i_172_ + (i_153_ << 9);
			    is_160_[anInt5055 * 551530869]
				= (class151.method2498(i_175_, i_176_,
						       -1805092151)
				   - class151_161_.method2498(i_175_, i_176_,
							      198587506));
			} else if (null != class151_162_
				   && !(aBoolArrayArray5031
					[anInt5052 * -648787169][i_167_])) {
			    int i_177_ = i_171_ + (i_152_ << 9);
			    int i_178_ = i_172_ + (i_153_ << 9);
			    is_160_[551530869 * anInt5055]
				= (class151_162_.method2498(i_177_, i_178_,
							    400930947)
				   - class151.method2498(i_177_, i_178_,
							 1505875950));
			}
		    }
		    if (i_167_ < 8 && (anIntArray5050[i_168_]
				       > class651.anInt8473 * 589238839)) {
			if (null != is)
			    is[551530869 * anInt5055] = anIntArray5047[i_168_];
			is_158_[551530869 * anInt5055]
			    = anIntArray5054[i_168_];
			is_157_[551530869 * anInt5055]
			    = anIntArray4987[i_168_];
			is_156_[anInt5055 * 551530869]
			    = anIntArray5033[i_168_];
		    } else {
			if (is != null)
			    is[551530869 * anInt5055] = i_163_;
			is_157_[551530869 * anInt5055]
			    = class651.anInt8468 * 1884378951;
			is_158_[551530869 * anInt5055]
			    = 431890869 * class651.anInt8471;
			is_156_[551530869 * anInt5055] = anInt5046 * 124430199;
		    }
		    anInt5055 += -1596139811;
		}
		anInt5005 += -817239013;
	    }
	    if (!aBool5002 && 0 == i)
		aClass556_4984.method9261(i_152_, i_153_,
					  -1652506135 * class651.anInt8475,
					  390419480 * class651.anInt8476,
					  class651.anInt8466 * -1594141697,
					  class651.anInt8465 * -672944661,
					  1112938727 * class651.anInt8479,
					  1597465863 * class651.anInt8480,
					  -500988450);
	    if (12 != -648787169 * anInt5052
		&& -1 != -2044484027 * class651.anInt8467
		&& class651.aBool8472)
		aBool5049 = true;
	} else if (aBool5061)
	    anInt5005 += anIntArray5022[anInt5052 * -648787169] * -817239013;
	else if (aBool5060)
	    anInt5005 += anIntArray5026[-648787169 * anInt5052] * -817239013;
	else
	    anInt5005 += -817239013 * anIntArray5024[-648787169 * anInt5052];
    }
    
    public final void method7459(int i, int[][] is, byte i_179_) {
	int[][] is_180_ = anIntArrayArrayArray4995[i];
	for (int i_181_ = 0; i_181_ < -60640777 * anInt4992 + 1; i_181_++) {
	    for (int i_182_ = 0; i_182_ < 1 + -1584311401 * anInt5051;
		 i_182_++)
		is_180_[i_181_][i_182_] += is[i_181_][i_182_];
	}
    }
    
    void method7460(Class651 class651, Class1 class1, int i) {
	if (aBool5061) {
	    anIntArray5065 = anIntArrayArray4985[anInt5052 * -648787169];
	    anIntArray5066 = anIntArrayArray4994[-648787169 * anInt5052];
	    anIntArray5007 = anIntArrayArray5034[-648787169 * anInt5052];
	    anInt5064
		= (null != class651 ? anIntArray5022[anInt5052 * -648787169]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (null != class1
				? anIntArray5023[-648787169 * anInt5052] : 0);
	} else if (aBool5060) {
	    anIntArray5065 = anIntArrayArray5042[-648787169 * anInt5052];
	    anIntArray5066 = anIntArrayArray5025[anInt5052 * -648787169];
	    anIntArray5007 = anIntArrayArray5032[-648787169 * anInt5052];
	    anInt5064
		= (class651 != null ? anIntArray5026[anInt5052 * -648787169]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (class1 != null
				? anIntArray5059[anInt5052 * -648787169] : 0);
	    anIntArray5068 = anIntArrayArray5041[-648787169 * anInt5052];
	} else {
	    anIntArray5065 = anIntArrayArray5037[anInt5052 * -648787169];
	    anIntArray5066 = anIntArrayArray5038[anInt5052 * -648787169];
	    anIntArray5007 = anIntArrayArray5030[anInt5052 * -648787169];
	    anInt5064
		= (class651 != null ? anIntArray5024[-648787169 * anInt5052]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (class1 != null
				? anIntArray5039[anInt5052 * -648787169] : 0);
	    anIntArray5068 = anIntArrayArray5036[anInt5052 * -648787169];
	}
    }
    
    void method7461(Class185 class185, Class151 class151, int i, int[][] is,
		    Class151 class151_183_, Class151 class151_184_,
		    byte i_185_) {
	byte[][] is_186_ = aByteArrayArrayArray5058[i];
	byte[][] is_187_ = aByteArrayArrayArray4988[i];
	short[][] is_188_ = aShortArrayArrayArray5000[i];
	short[][] is_189_ = aShortArrayArrayArray5001[i];
	boolean[] bools = new boolean[4];
	for (int i_190_ = 0; i_190_ < -60640777 * anInt4992; i_190_++) {
	    int i_191_
		= i_190_ < anInt4992 * -60640777 - 1 ? i_190_ + 1 : i_190_;
	    for (int i_192_ = 0; i_192_ < -1584311401 * anInt5051; i_192_++) {
		int i_193_ = (i_192_ < anInt5051 * -1584311401 - 1 ? i_192_ + 1
			      : i_192_);
		anInt5052 = is_186_[i_190_][i_192_] * 1068200159;
		anInt5053 = is_187_[i_190_][i_192_] * 1735620089;
		int i_194_ = is_189_[i_190_][i_192_] & 0x7fff;
		int i_195_ = is_188_[i_190_][i_192_] & 0x7fff;
		if (0 != i_194_ || i_195_ != 0) {
		    Class651 class651
			= ((Class651)
			   (0 != i_194_
			    ? aClass44_Sub16_4996.method91(i_194_ - 1,
							   -670450632)
			    : null));
		    Class1 class1
			= (Class1) (0 != i_195_
				    ? aClass44_Sub12_4982.method91(i_195_ - 1,
								   327675218)
				    : null);
		    if (-648787169 * anInt5052 == 0 && class651 == null)
			anInt5052 = -66499980;
		    aBool5049 = false;
		    aBool5060 = false;
		    boolean[] bools_196_ = bools;
		    boolean[] bools_197_ = bools;
		    boolean[] bools_198_ = bools;
		    bools[3] = false;
		    bools_198_[2] = false;
		    bools_197_[1] = false;
		    bools_196_[0] = false;
		    Class651 class651_199_ = class651;
		    if (class651 != null) {
			if (-1 == -2044484027 * class651.anInt8467
			    && 33386845 * class651.anInt8470 == -1) {
			    class651_199_ = class651;
			    class651 = null;
			} else if (null != class1
				   && anInt5052 * -648787169 != 0)
			    aBool5060 = class651.aBool8474;
		    }
		    anInt5053 = method7508(i_195_, i_190_, i_192_, i_191_,
					   i_193_, class151, is_188_,
					   -2032936883) * 1735620089;
		    for (int i_200_ = 0; i_200_ < 13; i_200_++) {
			anIntArray5050[i_200_] = -1;
			anIntArray4993[i_200_] = 1;
		    }
		    method7503(class185, class651, class1, i_190_, i_192_,
			       is_186_, is_187_, is_189_, bools, (byte) 0);
		    aBool5061 = (!aBool5060 && !bools[0] && !bools[2]
				 && !bools[1] && !bools[3]);
		    method7460(class651, class1, 35040);
		    int i_201_
			= anInt5027 * -1457646951 + anInt5064 * -1278434163;
		    if (i_201_ <= 0)
			aClass556_4984.method9367(i, i_190_, i_192_,
						  -887931878);
		    else {
			if (bools[0])
			    i_201_++;
			if (bools[2])
			    i_201_++;
			if (bools[1])
			    i_201_++;
			if (bools[3])
			    i_201_++;
			anInt5005 = 0;
			anInt5055 = 0;
			int i_202_ = 3 * i_201_;
			int[] is_203_ = aBool5062 ? new int[i_202_] : null;
			int[] is_204_ = new int[i_202_];
			int[] is_205_ = new int[i_202_];
			int[] is_206_ = new int[i_202_];
			int[] is_207_ = new int[i_202_];
			int[] is_208_ = new int[i_202_];
			int[] is_209_
			    = class151_184_ != null ? new int[i_202_] : null;
			int[] is_210_
			    = (null != class151_184_ || class151_183_ != null
			       ? new int[i_202_] : null);
			for (int i_211_ = 0; i_211_ < i_202_; i_211_++)
			    is_207_[i_211_] = -1;
			method7472(class185, i, i_190_, i_192_, class651,
				   bools, is_203_, is_204_, is_205_, is_206_,
				   is_207_, is_208_, is_209_, is_210_,
				   class151, class151_184_, class151_183_,
				   1474432706);
			int i_212_ = is_188_[i_190_][i_193_] & 0x7fff;
			int i_213_ = is_188_[i_191_][i_193_] & 0x7fff;
			int i_214_ = is_188_[i_191_][i_192_] & 0x7fff;
			method7462(class185, i, i_190_, i_192_, i_191_, i_193_,
				   class1, i_195_, i_212_, i_213_, i_214_,
				   bools, is_203_, is_204_, is_205_, is_206_,
				   is_207_, is_208_, is_209_, is_210_, is,
				   class151, class151_184_, class151_183_,
				   -273856224);
			method7463(class151, class1, class651_199_, i, i_190_,
				   i_192_, i_191_, i_193_, i_195_, i_194_,
				   (byte) 27);
			Class166 class166 = new Class166();
			if (aBool5002) {
			    class166.anInt1763
				= (aClass556_4984.method9305(i_190_, i_192_,
							     -498069121)
				   * 929497037);
			    class166.anInt1762
				= (aClass556_4984.method9283(i_190_, i_192_,
							     (byte) 0)
				   * 1063224577);
			    class166.anInt1764
				= (aClass556_4984.method9331(i_190_, i_192_,
							     -1041130287)
				   * -1101271945);
			    class166.anInt1765
				= (aClass556_4984.method9238(i_190_, i_192_,
							     (byte) -114)
				   * -1961234339);
			    class166.anInt1766
				= (aClass556_4984.method9239(i_190_, i_192_,
							     1607326976)
				   * -1338809385);
			    class166.anInt1767
				= (aClass556_4984.method9310(i_190_, i_192_,
							     (short) -22599)
				   * -2041662327);
			}
			class151.method2493(i_190_, i_192_, is_204_, is_209_,
					    is_205_, is_210_, is_206_, is_203_,
					    is_207_, is_208_, class166,
					    aBool5049);
			aClass556_4984.method9367(i, i_190_, i_192_,
						  -1102578511);
		    }
		}
	    }
	}
    }
    
    void method7462(Class185 class185, int i, int i_215_, int i_216_,
		    int i_217_, int i_218_, Class1 class1, int i_219_,
		    int i_220_, int i_221_, int i_222_, boolean[] bools,
		    int[] is, int[] is_223_, int[] is_224_, int[] is_225_,
		    int[] is_226_, int[] is_227_, int[] is_228_, int[] is_229_,
		    int[][] is_230_, Class151 class151, Class151 class151_231_,
		    Class151 class151_232_, int i_233_) {
	if (null != class1) {
	    if (0 == i_220_)
		i_220_ = i_219_;
	    if (i_221_ == 0)
		i_221_ = i_219_;
	    if (i_222_ == 0)
		i_222_ = i_219_;
	    Class1 class1_234_
		= ((Class1)
		   aClass44_Sub12_4982.method91(i_219_ - 1, -1600367489));
	    Class1 class1_235_
		= ((Class1)
		   aClass44_Sub12_4982.method91(i_220_ - 1, -276118979));
	    Class1 class1_236_
		= ((Class1)
		   aClass44_Sub12_4982.method91(i_221_ - 1, -1113362452));
	    Class1 class1_237_
		= (Class1) aClass44_Sub12_4982.method91(i_222_ - 1, 477898064);
	    for (int i_238_ = 0; i_238_ < anInt5027 * -1457646951; i_238_++) {
		boolean bool = false;
		int i_239_;
		if (bools[0 - anInt5053 * -1693107127 & 0x3]
		    && anInt5005 * 1299058195 == anIntArray5068[0]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 1;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 1;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_239_ = 6;
		} else if (bools[2 - -1693107127 * anInt5053 & 0x3]
			   && anInt5005 * 1299058195 == anIntArray5068[2]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 5;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 5;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_239_ = 6;
		} else if (bools[1 - -1693107127 * anInt5053 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[1]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 3;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 3;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_239_ = 6;
		} else if (bools[3 - anInt5053 * -1693107127 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[3]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 7;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 7;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_239_ = 6;
		} else {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    i_239_ = 3;
		}
		for (int i_240_ = 0; i_240_ < i_239_; i_240_++) {
		    int i_241_ = anIntArray5044[i_240_];
		    int i_242_ = i_241_ - 908753042 * anInt5053 & 0x7;
		    int i_243_ = anIntArray5028[i_241_];
		    int i_244_ = anIntArray5029[i_241_];
		    int i_245_;
		    int i_246_;
		    if (-1693107127 * anInt5053 == 1) {
			i_245_ = i_244_;
			i_246_ = 512 - i_243_;
		    } else if (anInt5053 * -1693107127 == 2) {
			i_245_ = 512 - i_243_;
			i_246_ = 512 - i_244_;
		    } else if (3 == anInt5053 * -1693107127) {
			i_245_ = 512 - i_244_;
			i_246_ = i_243_;
		    } else {
			i_245_ = i_243_;
			i_246_ = i_244_;
		    }
		    is_223_[anInt5055 * 551530869] = i_245_;
		    is_224_[anInt5055 * 551530869] = i_246_;
		    if (null != is_228_
			&& (aBoolArrayArray5011[-648787169 * anInt5052]
			    [i_241_])) {
			int i_247_ = (i_215_ << 9) + i_245_;
			int i_248_ = (i_216_ << 9) + i_246_;
			is_228_[551530869 * anInt5055]
			    = (class151_231_.method2498(i_247_, i_248_,
							-1249652006)
			       - class151.method2498(i_247_, i_248_,
						     -1707467151));
		    }
		    if (null != is_229_) {
			if (null != class151_231_
			    && !(aBoolArrayArray5011[anInt5052 * -648787169]
				 [i_241_])) {
			    int i_249_ = (i_215_ << 9) + i_245_;
			    int i_250_ = (i_216_ << 9) + i_246_;
			    is_229_[551530869 * anInt5055]
				= (class151.method2498(i_249_, i_250_,
						       1951986173)
				   - class151_231_.method2498(i_249_, i_250_,
							      820193545));
			} else if (null != class151_232_
				   && !(aBoolArrayArray5031
					[anInt5052 * -648787169][i_241_])) {
			    int i_251_ = i_245_ + (i_215_ << 9);
			    int i_252_ = i_246_ + (i_216_ << 9);
			    is_229_[551530869 * anInt5055]
				= (class151_232_.method2498(i_251_, i_252_,
							    -1700470183)
				   - class151.method2498(i_251_, i_252_,
							 -1790630311));
			}
		    }
		    if (i_241_ < 8 && anIntArray5050[i_242_] >= 0) {
			if (is != null)
			    is[anInt5055 * 551530869] = anIntArray5047[i_242_];
			is_227_[anInt5055 * 551530869]
			    = anIntArray5054[i_242_];
			is_226_[anInt5055 * 551530869]
			    = anIntArray4987[i_242_];
			is_225_[551530869 * anInt5055]
			    = anIntArray5033[i_242_];
		    } else {
			if (aBool5060 && (aBoolArrayArray5011
					  [-648787169 * anInt5052][i_241_])) {
			    is_226_[551530869 * anInt5055]
				= anInt5056 * -717903095;
			    is_227_[anInt5055 * 551530869]
				= anInt5057 * 782136929;
			    is_225_[551530869 * anInt5055]
				= 124430199 * anInt5046;
			} else if (i_245_ == 0 && i_246_ == 0) {
			    is_225_[551530869 * anInt5055]
				= is_230_[i_215_][i_216_];
			    is_226_[551530869 * anInt5055]
				= 587899173 * class1_234_.anInt10;
			    is_227_[anInt5055 * 551530869]
				= 161079521 * class1_234_.anInt15;
			} else if (i_245_ == 0 && 512 == i_246_) {
			    is_225_[anInt5055 * 551530869]
				= is_230_[i_215_][i_218_];
			    is_226_[551530869 * anInt5055]
				= 587899173 * class1_235_.anInt10;
			    is_227_[anInt5055 * 551530869]
				= class1_235_.anInt15 * 161079521;
			} else if (512 == i_245_ && 512 == i_246_) {
			    is_225_[551530869 * anInt5055]
				= is_230_[i_217_][i_218_];
			    is_226_[anInt5055 * 551530869]
				= class1_236_.anInt10 * 587899173;
			    is_227_[551530869 * anInt5055]
				= class1_236_.anInt15 * 161079521;
			} else if (512 == i_245_ && 0 == i_246_) {
			    is_225_[anInt5055 * 551530869]
				= is_230_[i_217_][i_216_];
			    is_226_[551530869 * anInt5055]
				= 587899173 * class1_237_.anInt10;
			    is_227_[551530869 * anInt5055]
				= 161079521 * class1_237_.anInt15;
			} else {
			    if (i_245_ < 256) {
				if (i_246_ < 256) {
				    is_226_[anInt5055 * 551530869]
					= class1_234_.anInt10 * 587899173;
				    is_227_[551530869 * anInt5055]
					= class1_234_.anInt15 * 161079521;
				} else {
				    is_226_[anInt5055 * 551530869]
					= 587899173 * class1_235_.anInt10;
				    is_227_[551530869 * anInt5055]
					= 161079521 * class1_235_.anInt15;
				}
			    } else if (i_246_ < 256) {
				is_226_[551530869 * anInt5055]
				    = class1_237_.anInt10 * 587899173;
				is_227_[anInt5055 * 551530869]
				    = class1_237_.anInt15 * 161079521;
			    } else {
				is_226_[anInt5055 * 551530869]
				    = class1_236_.anInt10 * 587899173;
				is_227_[551530869 * anInt5055]
				    = class1_236_.anInt15 * 161079521;
			    }
			    int i_253_
				= Class437.method6986(is_230_[i_215_][i_216_],
						      is_230_[i_217_][i_216_],
						      i_245_ << 7 >> 9,
						      -1456718944);
			    int i_254_
				= Class437.method6986(is_230_[i_215_][i_218_],
						      is_230_[i_217_][i_218_],
						      i_245_ << 7 >> 9,
						      -1745052100);
			    is_225_[551530869 * anInt5055]
				= Class437.method6986(i_253_, i_254_,
						      i_246_ << 7 >> 9,
						      -1917265223);
			}
			if (null != is)
			    is[551530869 * anInt5055]
				= is_225_[551530869 * anInt5055];
		    }
		    anInt5055 += -1596139811;
		}
		anInt5005 += -817239013;
	    }
	    if (-648787169 * anInt5052 != 0 && class1.aBool12)
		aBool5049 = true;
	}
    }
    
    void method7463(Class151 class151, Class1 class1, Class651 class651, int i,
		    int i_255_, int i_256_, int i_257_, int i_258_, int i_259_,
		    int i_260_, byte i_261_) {
	int i_262_ = class151.method2491(i_255_, i_256_, -1761555108);
	int i_263_ = class151.method2491(i_257_, i_256_, -859050715);
	int i_264_ = class151.method2491(i_257_, i_258_, -344413583);
	int i_265_ = class151.method2491(i_255_, i_258_, 1469696959);
	boolean bool = aClass468_4983.method7612(i_255_, i_256_, (byte) 0);
	if (bool && i > 1 || !bool && i > 0) {
	    boolean bool_266_ = true;
	    if (null != class1 && !class1.aBool13)
		bool_266_ = false;
	    else if (0 == i_259_ && -648787169 * anInt5052 != 0)
		bool_266_ = false;
	    else if (i_260_ > 0 && null != class651 && !class651.aBool8469)
		bool_266_ = false;
	    if (bool_266_ && i_263_ == i_262_ && i_262_ == i_264_
		&& i_265_ == i_262_)
		aByteArrayArrayArray4989[i][i_255_][i_256_] |= 0x4;
	}
    }
    
    final void method7464(Class185 class185, Class651 class651, Class1 class1,
			  int i, int i_267_, int i_268_, int i_269_,
			  short[][] is, byte[][] is_270_, byte[][] is_271_,
			  boolean[] bools, int i_272_) {
	boolean[] bools_273_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[-648787169 * anInt5052]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	if (i_267_ > 0) {
	    if (i > 0) {
		int i_274_ = is[i - 1][i_267_ - 1] & 0x7fff;
		if (i_274_ > 0) {
		    Class651 class651_275_
			= (Class651) aClass44_Sub16_4996.method91(i_274_ - 1,
								  1116160797);
		    if (-1 != class651_275_.anInt8467 * -2044484027
			&& class651_275_.aBool8474) {
			byte i_276_ = is_270_[i - 1][i_267_ - 1];
			int i_277_ = 2 * is_271_[i - 1][i_267_ - 1] + 4 & 0x7;
			int i_278_
			    = Class689.method14014(class185, class651_275_,
						   -2118471183);
			if (aBoolArrayArray5011[i_276_][i_277_]) {
			    anIntArray5033[0]
				= -2044484027 * class651_275_.anInt8467;
			    anIntArray5047[0] = i_278_;
			    anIntArray4987[0]
				= 1884378951 * class651_275_.anInt8468;
			    anIntArray5054[0]
				= class651_275_.anInt8471 * 431890869;
			    anIntArray5050[0]
				= class651_275_.anInt8473 * 589238839;
			    anIntArray4993[0] = 256;
			}
		    }
		}
	    }
	    if (i < i_268_ - 1) {
		int i_279_ = is[1 + i][i_267_ - 1] & 0x7fff;
		if (i_279_ > 0) {
		    Class651 class651_280_
			= (Class651) aClass44_Sub16_4996.method91(i_279_ - 1,
								  860821273);
		    if (-2044484027 * class651_280_.anInt8467 != -1
			&& class651_280_.aBool8474) {
			byte i_281_ = is_270_[1 + i][i_267_ - 1];
			int i_282_ = 2 * is_271_[i + 1][i_267_ - 1] + 6 & 0x7;
			int i_283_
			    = Class689.method14014(class185, class651_280_,
						   -2019615543);
			if (aBoolArrayArray5011[i_281_][i_282_]) {
			    anIntArray5033[2]
				= class651_280_.anInt8467 * -2044484027;
			    anIntArray5047[2] = i_283_;
			    anIntArray4987[2]
				= class651_280_.anInt8468 * 1884378951;
			    anIntArray5054[2]
				= 431890869 * class651_280_.anInt8471;
			    anIntArray5050[2]
				= 589238839 * class651_280_.anInt8473;
			    anIntArray4993[2] = 512;
			}
		    }
		}
	    }
	}
	if (i_267_ < i_269_ - 1) {
	    if (i > 0) {
		int i_284_ = is[i - 1][1 + i_267_] & 0x7fff;
		if (i_284_ > 0) {
		    Class651 class651_285_
			= (Class651) aClass44_Sub16_4996.method91(i_284_ - 1,
								  592212798);
		    if (-1 != class651_285_.anInt8467 * -2044484027
			&& class651_285_.aBool8474) {
			byte i_286_ = is_270_[i - 1][1 + i_267_];
			int i_287_ = 2 + is_271_[i - 1][i_267_ + 1] * 2 & 0x7;
			int i_288_
			    = Class689.method14014(class185, class651_285_,
						   -1979755557);
			if (aBoolArrayArray5011[i_286_][i_287_]) {
			    anIntArray5033[6]
				= class651_285_.anInt8467 * -2044484027;
			    anIntArray5047[6] = i_288_;
			    anIntArray4987[6]
				= 1884378951 * class651_285_.anInt8468;
			    anIntArray5054[6]
				= class651_285_.anInt8471 * 431890869;
			    anIntArray5050[6]
				= class651_285_.anInt8473 * 589238839;
			    anIntArray4993[6] = 64;
			}
		    }
		}
	    }
	    if (i < i_268_ - 1) {
		int i_289_ = is[1 + i][1 + i_267_] & 0x7fff;
		if (i_289_ > 0) {
		    Class651 class651_290_
			= (Class651) aClass44_Sub16_4996.method91(i_289_ - 1,
								  -832312735);
		    if (-1 != class651_290_.anInt8467 * -2044484027
			&& class651_290_.aBool8474) {
			byte i_291_ = is_270_[i + 1][i_267_ + 1];
			int i_292_ = 2 * is_271_[1 + i][1 + i_267_] + 0 & 0x7;
			int i_293_
			    = Class689.method14014(class185, class651_290_,
						   -2105703471);
			if (aBoolArrayArray5011[i_291_][i_292_]) {
			    anIntArray5033[4]
				= -2044484027 * class651_290_.anInt8467;
			    anIntArray5047[4] = i_293_;
			    anIntArray4987[4]
				= class651_290_.anInt8468 * 1884378951;
			    anIntArray5054[4]
				= 431890869 * class651_290_.anInt8471;
			    anIntArray5050[4]
				= 589238839 * class651_290_.anInt8473;
			    anIntArray4993[4] = 128;
			}
		    }
		}
	    }
	}
	if (i_267_ > 0) {
	    int i_294_ = is[i][i_267_ - 1] & 0x7fff;
	    if (i_294_ > 0) {
		Class651 class651_295_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_294_ - 1, 1235893507));
		if (-1 != class651_295_.anInt8467 * -2044484027) {
		    byte i_296_ = is_270_[i][i_267_ - 1];
		    int i_297_ = is_271_[i][i_267_ - 1];
		    if (class651_295_.aBool8474) {
			int i_298_ = 2;
			int i_299_ = 2 * i_297_ + 4;
			int i_300_
			    = Class689.method14014(class185, class651_295_,
						   -1895491392);
			for (int i_301_ = 0; i_301_ < 3; i_301_++) {
			    i_299_ &= 0x7;
			    i_298_ &= 0x7;
			    if (aBoolArrayArray5011[i_296_][i_299_]
				&& (anIntArray5050[i_298_]
				    <= class651_295_.anInt8473 * 589238839)) {
				anIntArray5033[i_298_]
				    = -2044484027 * class651_295_.anInt8467;
				anIntArray5047[i_298_] = i_300_;
				anIntArray4987[i_298_]
				    = 1884378951 * class651_295_.anInt8468;
				anIntArray5054[i_298_]
				    = 431890869 * class651_295_.anInt8471;
				if (anIntArray5050[i_298_]
				    == 589238839 * class651_295_.anInt8473)
				    anIntArray4993[i_298_] |= 0x20;
				else
				    anIntArray4993[i_298_] = 32;
				anIntArray5050[i_298_]
				    = class651_295_.anInt8473 * 589238839;
			    }
			    i_299_++;
			    i_298_--;
			}
			if (!bools_273_[-1693107127 * anInt5053 + 0 & 0x3])
			    bools[0] = (aBoolArrayArray5040[i_296_]
					[i_297_ + 2 & 0x3]);
		    } else if (!bools_273_[0 + -1693107127 * anInt5053 & 0x3])
			bools[0]
			    = aBoolArrayArray5021[i_296_][i_297_ + 2 & 0x3];
		}
	    }
	}
	if (i_267_ < i_269_ - 1) {
	    int i_302_ = is[i][i_267_ + 1] & 0x7fff;
	    if (i_302_ > 0) {
		Class651 class651_303_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_302_ - 1, -2118274083));
		if (class651_303_.anInt8467 * -2044484027 != -1) {
		    byte i_304_ = is_270_[i][i_267_ + 1];
		    int i_305_ = is_271_[i][1 + i_267_];
		    if (class651_303_.aBool8474) {
			int i_306_ = 4;
			int i_307_ = i_305_ * 2 + 2;
			int i_308_
			    = Class689.method14014(class185, class651_303_,
						   -1905007934);
			for (int i_309_ = 0; i_309_ < 3; i_309_++) {
			    i_307_ &= 0x7;
			    i_306_ &= 0x7;
			    if (aBoolArrayArray5011[i_304_][i_307_]
				&& (anIntArray5050[i_306_]
				    <= class651_303_.anInt8473 * 589238839)) {
				anIntArray5033[i_306_]
				    = -2044484027 * class651_303_.anInt8467;
				anIntArray5047[i_306_] = i_308_;
				anIntArray4987[i_306_]
				    = 1884378951 * class651_303_.anInt8468;
				anIntArray5054[i_306_]
				    = 431890869 * class651_303_.anInt8471;
				if (class651_303_.anInt8473 * 589238839
				    == anIntArray5050[i_306_])
				    anIntArray4993[i_306_] |= 0x10;
				else
				    anIntArray4993[i_306_] = 16;
				anIntArray5050[i_306_]
				    = class651_303_.anInt8473 * 589238839;
			    }
			    i_307_--;
			    i_306_++;
			}
			if (!bools_273_[anInt5053 * -1693107127 + 2 & 0x3])
			    bools[2] = (aBoolArrayArray5040[i_304_]
					[i_305_ + 0 & 0x3]);
		    } else if (!bools_273_[anInt5053 * -1693107127 + 2 & 0x3])
			bools[2]
			    = aBoolArrayArray5021[i_304_][i_305_ + 0 & 0x3];
		}
	    }
	}
	if (i > 0) {
	    int i_310_ = is[i - 1][i_267_] & 0x7fff;
	    if (i_310_ > 0) {
		Class651 class651_311_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_310_ - 1, -733053408));
		if (-2044484027 * class651_311_.anInt8467 != -1) {
		    byte i_312_ = is_270_[i - 1][i_267_];
		    int i_313_ = is_271_[i - 1][i_267_];
		    if (class651_311_.aBool8474) {
			int i_314_ = 6;
			int i_315_ = i_313_ * 2 + 4;
			int i_316_
			    = Class689.method14014(class185, class651_311_,
						   -2097753318);
			for (int i_317_ = 0; i_317_ < 3; i_317_++) {
			    i_315_ &= 0x7;
			    i_314_ &= 0x7;
			    if (aBoolArrayArray5011[i_312_][i_315_]
				&& (anIntArray5050[i_314_]
				    <= 589238839 * class651_311_.anInt8473)) {
				anIntArray5033[i_314_]
				    = class651_311_.anInt8467 * -2044484027;
				anIntArray5047[i_314_] = i_316_;
				anIntArray4987[i_314_]
				    = class651_311_.anInt8468 * 1884378951;
				anIntArray5054[i_314_]
				    = 431890869 * class651_311_.anInt8471;
				if (589238839 * class651_311_.anInt8473
				    == anIntArray5050[i_314_])
				    anIntArray4993[i_314_] |= 0x8;
				else
				    anIntArray4993[i_314_] = 8;
				anIntArray5050[i_314_]
				    = 589238839 * class651_311_.anInt8473;
			    }
			    i_315_--;
			    i_314_++;
			}
			if (!bools_273_[anInt5053 * -1693107127 + 3 & 0x3])
			    bools[3] = (aBoolArrayArray5040[i_312_]
					[1 + i_313_ & 0x3]);
		    } else if (!bools_273_[-1693107127 * anInt5053 + 3 & 0x3])
			bools[3]
			    = aBoolArrayArray5021[i_312_][i_313_ + 1 & 0x3];
		}
	    }
	}
	if (i < i_268_ - 1) {
	    int i_318_ = is[1 + i][i_267_] & 0x7fff;
	    if (i_318_ > 0) {
		Class651 class651_319_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_318_ - 1, 175920397));
		if (class651_319_.anInt8467 * -2044484027 != -1) {
		    byte i_320_ = is_270_[1 + i][i_267_];
		    int i_321_ = is_271_[i + 1][i_267_];
		    if (class651_319_.aBool8474) {
			int i_322_ = 4;
			int i_323_ = 6 + 2 * i_321_;
			int i_324_
			    = Class689.method14014(class185, class651_319_,
						   -1929186205);
			for (int i_325_ = 0; i_325_ < 3; i_325_++) {
			    i_323_ &= 0x7;
			    i_322_ &= 0x7;
			    if (aBoolArrayArray5011[i_320_][i_323_]
				&& (anIntArray5050[i_322_]
				    <= 589238839 * class651_319_.anInt8473)) {
				anIntArray5033[i_322_]
				    = class651_319_.anInt8467 * -2044484027;
				anIntArray5047[i_322_] = i_324_;
				anIntArray4987[i_322_]
				    = class651_319_.anInt8468 * 1884378951;
				anIntArray5054[i_322_]
				    = 431890869 * class651_319_.anInt8471;
				if (anIntArray5050[i_322_]
				    == 589238839 * class651_319_.anInt8473)
				    anIntArray4993[i_322_] |= 0x4;
				else
				    anIntArray4993[i_322_] = 4;
				anIntArray5050[i_322_]
				    = 589238839 * class651_319_.anInt8473;
			    }
			    i_323_++;
			    i_322_--;
			}
			if (!bools_273_[-1693107127 * anInt5053 + 1 & 0x3])
			    bools[1] = (aBoolArrayArray5040[i_320_]
					[i_321_ + 3 & 0x3]);
		    } else if (!bools_273_[1 + -1693107127 * anInt5053 & 0x3])
			bools[1]
			    = aBoolArrayArray5021[i_320_][3 + i_321_ & 0x3];
		}
	    }
	}
	if (class651 != null && class651.aBool8474) {
	    int i_326_ = Class689.method14014(class185, class651, -2131374486);
	    for (int i_327_ = 0; i_327_ < 8; i_327_++) {
		int i_328_ = i_327_ - anInt5053 * 908753042 & 0x7;
		if (aBoolArrayArray5011[anInt5052 * -648787169][i_327_]
		    && (anIntArray5050[i_328_]
			<= 589238839 * class651.anInt8473)) {
		    anIntArray5033[i_328_] = class651.anInt8467 * -2044484027;
		    anIntArray5047[i_328_] = i_326_;
		    anIntArray4987[i_328_] = class651.anInt8468 * 1884378951;
		    anIntArray5054[i_328_] = class651.anInt8471 * 431890869;
		    if (anIntArray5050[i_328_]
			== class651.anInt8473 * 589238839)
			anIntArray4993[i_328_] |= 0x2;
		    else
			anIntArray4993[i_328_] = 2;
		    anIntArray5050[i_328_] = 589238839 * class651.anInt8473;
		}
	    }
	}
    }
    
    static {
	anIntArray5026 = new int[] { 4, 2, 1, 1, 2, 2, 3, 1, 3, 3, 3, 2, 0 };
	anIntArray5059 = new int[] { 0, 4, 3, 3, 1, 1, 3, 5, 1, 5, 3, 6, 4 };
	anIntArray5028 = new int[] { 0, 256, 512, 512, 512, 256, 0, 0, 128,
				     256, 128, 384, 256 };
	anIntArray5029 = new int[] { 0, 0, 0, 256, 512, 512, 512, 256, 256,
				     384, 128, 128, 256 };
	aBoolArrayArray5011
	    = (new boolean[][]
	       { { true, true, true, true, true, true, true, true, true, true,
		   true, true, true },
		 { true, true, true, false, false, false, true, true, false,
		   false, false, false, true },
		 { true, false, false, false, false, true, true, true, false,
		   false, false, false, false },
		 { false, false, true, true, true, true, false, false, false,
		   false, false, false, false },
		 { true, true, true, true, true, true, false, false, false,
		   false, false, false, false },
		 { true, true, true, false, false, true, true, true, false,
		   false, false, false, false },
		 { true, true, false, false, false, true, true, true, false,
		   false, false, false, true },
		 { true, true, false, false, false, false, false, true, false,
		   false, false, false, false },
		 { false, true, true, true, true, true, true, true, false,
		   false, false, false, false },
		 { true, false, false, false, true, true, true, true, true,
		   true, false, false, false },
		 { true, true, true, true, true, false, false, false, true,
		   true, false, false, false },
		 { true, true, true, false, false, false, false, false, false,
		   false, true, true, false },
		 { false, false, false, false, false, false, false, false,
		   false, false, false, false, false },
		 { true, true, true, true, true, true, true, true, true, true,
		   true, true, true },
		 { false, false, false, false, false, false, false, false,
		   false, false, false, false, false } });
	aBoolArrayArray5031
	    = (new boolean[][]
	       { { false, false, false, false, false, false, false, false,
		   false, false, false, false, false },
		 { false, false, true, true, true, true, true, false, false,
		   false, false, false, true },
		 { true, true, true, true, true, true, false, false, false,
		   false, false, false, false },
		 { true, true, true, false, false, true, true, true, false,
		   false, false, false, false },
		 { true, false, false, false, false, true, true, true, false,
		   false, false, false, false },
		 { false, false, true, true, true, true, false, false, false,
		   false, false, false, false },
		 { false, true, true, true, true, true, false, false, false,
		   false, false, false, true },
		 { false, true, true, true, true, true, true, true, false,
		   false, false, false, true },
		 { true, true, false, false, false, false, false, true, false,
		   false, false, false, false },
		 { true, true, true, true, true, false, false, false, true,
		   true, false, false, false },
		 { true, false, false, false, true, true, true, true, true,
		   true, false, false, false },
		 { true, false, true, true, true, true, true, true, false,
		   false, true, true, false },
		 { true, true, true, true, true, true, true, true, true, true,
		   true, true, true },
		 { false, false, false, false, false, false, false, false,
		   false, false, false, false, false },
		 { true, true, true, true, true, true, true, true, true, true,
		   true, true, true } });
	anIntArrayArray4985
	    = new int[][] { { 0, 2 }, { 0, 2 }, { 0, 0, 2 }, { 2, 0, 0 },
			    { 0, 2, 0 }, { 0, 0, 2 }, { 0, 5, 1, 4 },
			    { 0, 4, 4, 4 }, { 4, 4, 4, 0 },
			    { 6, 6, 6, 2, 2, 2 }, { 2, 2, 2, 6, 6, 6 },
			    { 0, 11, 6, 6, 6, 4 }, { 0, 2 }, { 0, 4, 4, 4 },
			    { 0, 4, 4, 4 } };
	anIntArrayArray4994
	    = new int[][] { { 2, 4 }, { 2, 4 }, { 5, 2, 4 }, { 4, 5, 2 },
			    { 2, 4, 5 }, { 5, 2, 4 }, { 1, 6, 2, 5 },
			    { 1, 6, 7, 1 }, { 6, 7, 1, 1 },
			    { 0, 8, 9, 8, 9, 4 }, { 8, 9, 4, 0, 8, 9 },
			    { 2, 10, 0, 10, 11, 11 }, { 2, 4 }, { 1, 6, 7, 1 },
			    { 1, 6, 7, 1 } };
	anIntArrayArray5034
	    = new int[][] { { 6, 6 }, { 6, 6 }, { 6, 5, 5 }, { 5, 6, 5 },
			    { 5, 5, 6 }, { 6, 5, 5 }, { 5, 0, 4, 1 },
			    { 7, 7, 1, 2 }, { 7, 1, 2, 7 },
			    { 8, 9, 4, 0, 8, 9 }, { 0, 8, 9, 8, 9, 4 },
			    { 11, 0, 10, 11, 4, 2 }, { 6, 6 }, { 7, 7, 1, 2 },
			    { 7, 7, 1, 2 } };
	aBoolArrayArray5021
	    = (new boolean[][]
	       { { false, false, false, false },
		 { false, false, false, false }, { false, false, true, false },
		 { false, false, true, false }, { false, false, true, false },
		 { false, false, true, false }, { true, false, true, false },
		 { true, false, false, true }, { true, false, false, true },
		 { false, false, false, false },
		 { false, false, false, false },
		 { false, false, false, false },
		 { false, false, false, false } });
	anIntArrayArray5036
	    = new int[][] { { 0, 1, 2, 3 }, { 1, 2, 3, 0 }, { 1, 2, -1, 0 },
			    { 2, 0, -1, 1 }, { 0, 1, -1, 2 }, { 1, 2, -1, 0 },
			    { -1, 4, -1, 1 }, { -1, 1, 3, -1 },
			    { -1, 0, 2, -1 }, { 3, 5, 2, 0 }, { 0, 2, 5, 3 },
			    { 0, 2, 3, 5 }, { 0, 1, 2, 3 } };
	anIntArrayArray5037
	    = (new int[][]
	       { { 0, 2, 4, 6 }, { 6, 0, 2, 4 }, { 6, 0, 2 }, { 2, 6, 0 },
		 { 0, 2, 6 }, { 6, 0, 2 }, { 5, 6, 0, 1, 2, 4 },
		 { 7, 2, 4, 4 }, { 2, 4, 4, 7 }, { 6, 6, 4, 0, 2, 2 },
		 { 0, 2, 2, 6, 6, 4 }, { 0, 2, 2, 4, 6, 6 }, { 0, 2, 4, 6 } });
	anIntArrayArray5038
	    = new int[][] { { 2, 4, 6, 0 }, { 0, 2, 4, 6 }, { 0, 2, 4 },
			    { 4, 0, 2 }, { 2, 4, 0 }, { 0, 2, 4 },
			    { 6, 0, 1, 2, 4, 5 }, { 0, 4, 7, 6 },
			    { 4, 7, 6, 0 }, { 0, 8, 6, 2, 9, 4 },
			    { 2, 9, 4, 0, 8, 6 }, { 2, 11, 4, 6, 10, 0 },
			    { 2, 4, 6, 0 } };
	anIntArrayArray5030
	    = new int[][] { { 12, 12, 12, 12 }, { 12, 12, 12, 12 },
			    { 5, 5, 5 }, { 5, 5, 5 }, { 5, 5, 5 }, { 5, 5, 5 },
			    { 12, 12, 12, 12, 12, 12 }, { 1, 1, 1, 7 },
			    { 1, 1, 7, 1 }, { 8, 9, 9, 8, 8, 9 },
			    { 8, 8, 9, 8, 9, 9 }, { 10, 10, 11, 11, 11, 10 },
			    { 12, 12, 12, 12 } };
	aBoolArrayArray5040
	    = (new boolean[][]
	       { { false, false, false, false }, { false, true, true, false },
		 { true, false, true, false }, { true, false, true, false },
		 { false, false, true, false }, { false, false, true, false },
		 { true, false, true, false }, { true, false, false, true },
		 { true, false, false, true }, { true, true, false, false },
		 { false, false, false, false }, { false, true, false, true },
		 { false, false, false, false } });
	anIntArrayArray5041
	    = new int[][] { { 0, 1, 2, 3 }, { 1, -1, -1, 0 }, { -1, 2, -1, 0 },
			    { -1, 0, -1, 2 }, { 0, 1, -1, 2 }, { 1, 2, -1, 0 },
			    { -1, 4, -1, 1 }, { -1, 3, 4, -1 },
			    { -1, 0, 2, -1 }, { -1, -1, 2, 0 }, { 0, 2, 5, 3 },
			    { 0, -1, 6, -1 }, { 0, 1, 2, 3 } };
	anIntArrayArray5042
	    = new int[][] { { 0, 2, 4, 6 }, { 6, 0, 2, 3, 5, 3 },
			    { 6, 0, 2, 4 }, { 2, 5, 6, 1 }, { 0, 2, 6 },
			    { 6, 0, 2 }, { 5, 6, 0, 1, 2, 4 },
			    { 7, 7, 1, 2, 4, 6 }, { 2, 4, 4, 7 },
			    { 6, 6, 4, 0, 1, 1, 3, 3 }, { 0, 2, 2, 6, 6, 4 },
			    { 0, 2, 2, 3, 7, 0, 4, 3 }, { 0, 2, 4, 6 } };
	anIntArrayArray5025
	    = new int[][] { { 2, 4, 6, 0 }, { 0, 2, 3, 5, 6, 4 },
			    { 0, 1, 4, 5 }, { 4, 6, 0, 2 }, { 2, 4, 0 },
			    { 0, 2, 4 }, { 6, 0, 1, 2, 4, 5 },
			    { 0, 1, 2, 4, 6, 7 }, { 4, 7, 6, 0 },
			    { 0, 8, 6, 1, 9, 2, 9, 4 }, { 2, 9, 4, 0, 8, 6 },
			    { 2, 11, 3, 7, 10, 10, 6, 6 }, { 2, 4, 6, 0 } };
	anIntArrayArray5032
	    = new int[][] { { 12, 12, 12, 12 }, { 12, 12, 12, 12, 12, 5 },
			    { 5, 5, 1, 1 }, { 5, 1, 1, 5 }, { 5, 5, 5 },
			    { 5, 5, 5 }, { 12, 12, 12, 12, 12, 12 },
			    { 1, 12, 12, 12, 12, 12 }, { 1, 1, 7, 1 },
			    { 8, 9, 9, 8, 8, 3, 1, 9 }, { 8, 8, 9, 8, 9, 9 },
			    { 10, 10, 11, 11, 11, 7, 3, 7 },
			    { 12, 12, 12, 12 } };
    }
    
    public void method7465() {
	aBool4997 = true;
    }
    
    public final void method7466(Class534_Sub40 class534_sub40, int i,
				 int i_329_, int i_330_, int i_331_) {
	int i_332_ = i + i_330_;
	int i_333_ = i_331_ + i_329_;
	for (int i_334_ = 0; i_334_ < -692901467 * anInt4991; i_334_++) {
	    for (int i_335_ = 0; i_335_ < 64; i_335_++) {
		for (int i_336_ = 0; i_336_ < 64; i_336_++)
		    method7493(class534_sub40, i_334_, i + i_335_,
			       i_329_ + i_336_, 0, 0, i_335_ + i_332_,
			       i_333_ + i_336_, 0, false, 414042331);
	    }
	}
    }
    
    public final void method7467(int i, int i_337_, int i_338_, int i_339_) {
	for (int i_340_ = 0; i_340_ < -692901467 * anInt4991; i_340_++)
	    method7449(i_340_, i, i_337_, i_338_, i_339_, -904088808);
    }
    
    public final void method7468(Class534_Sub40 class534_sub40, int i,
				 int i_341_, int i_342_, int i_343_) {
	int i_344_ = i + i_342_;
	int i_345_ = i_343_ + i_341_;
	for (int i_346_ = 0; i_346_ < -692901467 * anInt4991; i_346_++) {
	    for (int i_347_ = 0; i_347_ < 64; i_347_++) {
		for (int i_348_ = 0; i_348_ < 64; i_348_++)
		    method7493(class534_sub40, i_346_, i + i_347_,
			       i_341_ + i_348_, 0, 0, i_347_ + i_344_,
			       i_345_ + i_348_, 0, false, 414042331);
	    }
	}
    }
    
    public final void method7469(int i, int i_349_, int i_350_, int i_351_,
				 int i_352_) {
	for (int i_353_ = i_350_; i_353_ < i_352_ + i_350_; i_353_++) {
	    for (int i_354_ = i_349_; i_354_ < i_351_ + i_349_; i_354_++) {
		if (i_354_ >= 0 && i_354_ < -60640777 * anInt4992
		    && i_353_ >= 0 && i_353_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_354_][i_353_]
			= i > 0 ? (anIntArrayArrayArray4995[i - 1][i_354_]
				   [i_353_]) - 960 : 0;
	    }
	}
	if (i_349_ > 0 && i_349_ < anInt4992 * -60640777) {
	    for (int i_355_ = 1 + i_350_; i_355_ < i_352_ + i_350_; i_355_++) {
		if (i_355_ >= 0 && i_355_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_349_][i_355_]
			= anIntArrayArrayArray4995[i][i_349_ - 1][i_355_];
	    }
	}
	if (i_350_ > 0 && i_350_ < anInt5051 * -1584311401) {
	    for (int i_356_ = 1 + i_349_; i_356_ < i_349_ + i_351_; i_356_++) {
		if (i_356_ >= 0 && i_356_ < -60640777 * anInt4992)
		    anIntArrayArrayArray4995[i][i_356_][i_350_]
			= anIntArrayArrayArray4995[i][i_356_][i_350_ - 1];
	    }
	}
	if (i_349_ >= 0 && i_350_ >= 0 && i_349_ < -60640777 * anInt4992
	    && i_350_ < anInt5051 * -1584311401) {
	    if (i == 0) {
		if (i_349_ > 0
		    && 0 != anIntArrayArrayArray4995[i][i_349_ - 1][i_350_])
		    anIntArrayArrayArray4995[i][i_349_][i_350_]
			= anIntArrayArrayArray4995[i][i_349_ - 1][i_350_];
		else if (i_350_ > 0 && 0 != (anIntArrayArrayArray4995[i]
					     [i_349_][i_350_ - 1]))
		    anIntArrayArrayArray4995[i][i_349_][i_350_]
			= anIntArrayArrayArray4995[i][i_349_][i_350_ - 1];
		else if (i_349_ > 0 && i_350_ > 0
			 && (anIntArrayArrayArray4995[i][i_349_ - 1]
			     [i_350_ - 1]) != 0)
		    anIntArrayArrayArray4995[i][i_349_][i_350_]
			= anIntArrayArrayArray4995[i][i_349_ - 1][i_350_ - 1];
	    } else if (i_349_ > 0
		       && (anIntArrayArrayArray4995[i - 1][i_349_ - 1][i_350_]
			   != anIntArrayArrayArray4995[i][i_349_ - 1][i_350_]))
		anIntArrayArrayArray4995[i][i_349_][i_350_]
		    = anIntArrayArrayArray4995[i][i_349_ - 1][i_350_];
	    else if (i_350_ > 0
		     && (anIntArrayArrayArray4995[i][i_349_][i_350_ - 1]
			 != (anIntArrayArrayArray4995[i - 1][i_349_]
			     [i_350_ - 1])))
		anIntArrayArrayArray4995[i][i_349_][i_350_]
		    = anIntArrayArrayArray4995[i][i_349_][i_350_ - 1];
	    else if (i_349_ > 0 && i_350_ > 0
		     && ((anIntArrayArrayArray4995[i - 1][i_349_ - 1]
			  [i_350_ - 1])
			 != (anIntArrayArrayArray4995[i][i_349_ - 1]
			     [i_350_ - 1])))
		anIntArrayArrayArray4995[i][i_349_][i_350_]
		    = anIntArrayArrayArray4995[i][i_349_ - 1][i_350_ - 1];
	}
    }
    
    public void method7470(int i) {
	anIntArray5003 = null;
	anIntArray5004 = null;
	anIntArray4999 = null;
	anIntArray5035 = null;
	anIntArray5063 = null;
	aBool4997 = false;
    }
    
    public final void method7471(Class534_Sub40 class534_sub40, int i,
				 int i_357_, int i_358_, int i_359_) {
	int i_360_ = i + i_358_;
	int i_361_ = i_359_ + i_357_;
	for (int i_362_ = 0; i_362_ < -692901467 * anInt4991; i_362_++) {
	    for (int i_363_ = 0; i_363_ < 64; i_363_++) {
		for (int i_364_ = 0; i_364_ < 64; i_364_++)
		    method7493(class534_sub40, i_362_, i + i_363_,
			       i_357_ + i_364_, 0, 0, i_363_ + i_360_,
			       i_361_ + i_364_, 0, false, 414042331);
	    }
	}
    }
    
    void method7472(Class185 class185, int i, int i_365_, int i_366_,
		    Class651 class651, boolean[] bools, int[] is,
		    int[] is_367_, int[] is_368_, int[] is_369_, int[] is_370_,
		    int[] is_371_, int[] is_372_, int[] is_373_,
		    Class151 class151, Class151 class151_374_,
		    Class151 class151_375_, int i_376_) {
	anInt5046 = 1053793721;
	anInt5056 = 586067143;
	anInt5057 = 1487773952;
	if (class651 != null) {
	    anInt5046 = 192513827 * class651.anInt8467;
	    anInt5056 = 2015327183 * class651.anInt8468;
	    anInt5057 = 1989313493 * class651.anInt8471;
	    int i_377_ = Class689.method14014(class185, class651, -1981677815);
	    for (int i_378_ = 0; i_378_ < anInt5064 * -1278434163; i_378_++) {
		boolean bool = false;
		int i_379_;
		if (bools[0 - anInt5053 * -1693107127 & 0x3]
		    && anIntArray5068[0] == anInt5005 * 1299058195) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 1;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 1;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_379_ = 6;
		} else if (bools[2 - anInt5053 * -1693107127 & 0x3]
			   && anIntArray5068[2] == 1299058195 * anInt5005) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 5;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 5;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_379_ = 6;
		} else if (bools[1 - anInt5053 * -1693107127 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[1]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 3;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 3;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_379_ = 6;
		} else if (bools[3 - -1693107127 * anInt5053 & 0x3]
			   && anInt5005 * 1299058195 == anIntArray5068[3]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 7;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 7;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_379_ = 6;
		} else {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    i_379_ = 3;
		}
		for (int i_380_ = 0; i_380_ < i_379_; i_380_++) {
		    int i_381_ = anIntArray5044[i_380_];
		    int i_382_ = i_381_ - 908753042 * anInt5053 & 0x7;
		    int i_383_ = anIntArray5028[i_381_];
		    int i_384_ = anIntArray5029[i_381_];
		    int i_385_;
		    int i_386_;
		    if (anInt5053 * -1693107127 == 1) {
			i_385_ = i_384_;
			i_386_ = 512 - i_383_;
		    } else if (anInt5053 * -1693107127 == 2) {
			i_385_ = 512 - i_383_;
			i_386_ = 512 - i_384_;
		    } else if (-1693107127 * anInt5053 == 3) {
			i_385_ = 512 - i_384_;
			i_386_ = i_383_;
		    } else {
			i_385_ = i_383_;
			i_386_ = i_384_;
		    }
		    is_367_[anInt5055 * 551530869] = i_385_;
		    is_368_[551530869 * anInt5055] = i_386_;
		    if (null != is_372_
			&& (aBoolArrayArray5011[anInt5052 * -648787169]
			    [i_381_])) {
			int i_387_ = i_385_ + (i_365_ << 9);
			int i_388_ = (i_366_ << 9) + i_386_;
			is_372_[551530869 * anInt5055]
			    = (class151_374_.method2498(i_387_, i_388_,
							-299721340)
			       - class151.method2498(i_387_, i_388_,
						     -647542304));
		    }
		    if (is_373_ != null) {
			if (null != class151_374_
			    && !(aBoolArrayArray5011[anInt5052 * -648787169]
				 [i_381_])) {
			    int i_389_ = i_385_ + (i_365_ << 9);
			    int i_390_ = i_386_ + (i_366_ << 9);
			    is_373_[anInt5055 * 551530869]
				= (class151.method2498(i_389_, i_390_,
						       -538475643)
				   - class151_374_.method2498(i_389_, i_390_,
							      -2029059877));
			} else if (null != class151_375_
				   && !(aBoolArrayArray5031
					[anInt5052 * -648787169][i_381_])) {
			    int i_391_ = i_385_ + (i_365_ << 9);
			    int i_392_ = i_386_ + (i_366_ << 9);
			    is_373_[551530869 * anInt5055]
				= (class151_375_.method2498(i_391_, i_392_,
							    -2053872992)
				   - class151.method2498(i_391_, i_392_,
							 958248567));
			}
		    }
		    if (i_381_ < 8 && (anIntArray5050[i_382_]
				       > class651.anInt8473 * 589238839)) {
			if (null != is)
			    is[551530869 * anInt5055] = anIntArray5047[i_382_];
			is_371_[551530869 * anInt5055]
			    = anIntArray5054[i_382_];
			is_370_[551530869 * anInt5055]
			    = anIntArray4987[i_382_];
			is_369_[anInt5055 * 551530869]
			    = anIntArray5033[i_382_];
		    } else {
			if (is != null)
			    is[551530869 * anInt5055] = i_377_;
			is_370_[551530869 * anInt5055]
			    = class651.anInt8468 * 1884378951;
			is_371_[551530869 * anInt5055]
			    = 431890869 * class651.anInt8471;
			is_369_[551530869 * anInt5055] = anInt5046 * 124430199;
		    }
		    anInt5055 += -1596139811;
		}
		anInt5005 += -817239013;
	    }
	    if (!aBool5002 && 0 == i)
		aClass556_4984.method9261(i_365_, i_366_,
					  -1652506135 * class651.anInt8475,
					  390419480 * class651.anInt8476,
					  class651.anInt8466 * -1594141697,
					  class651.anInt8465 * -672944661,
					  1112938727 * class651.anInt8479,
					  1597465863 * class651.anInt8480,
					  -500988450);
	    if (12 != -648787169 * anInt5052
		&& -1 != -2044484027 * class651.anInt8467
		&& class651.aBool8472)
		aBool5049 = true;
	} else if (aBool5061)
	    anInt5005 += anIntArray5022[anInt5052 * -648787169] * -817239013;
	else if (aBool5060)
	    anInt5005 += anIntArray5026[-648787169 * anInt5052] * -817239013;
	else
	    anInt5005 += -817239013 * anIntArray5024[-648787169 * anInt5052];
    }
    
    static final int method7473(int i, int i_393_) {
	int i_394_
	    = (Class419.method6770(45365 + i, 91923 + i_393_, 4, 1748207138)
	       - 128
	       + (Class419.method6770(10294 + i, 37821 + i_393_, 2,
				      1748207138) - 128
		  >> 1)
	       + (Class419.method6770(i, i_393_, 1, 1748207138) - 128 >> 2));
	i_394_ = 35 + (int) (0.3 * (double) i_394_);
	if (i_394_ < 10)
	    i_394_ = 10;
	else if (i_394_ > 60)
	    i_394_ = 60;
	return i_394_;
    }
    
    public final void method7474(int i, int i_395_, int i_396_, int i_397_,
				 int i_398_) {
	for (int i_399_ = i_396_; i_399_ < i_398_ + i_396_; i_399_++) {
	    for (int i_400_ = i_395_; i_400_ < i_397_ + i_395_; i_400_++) {
		if (i_400_ >= 0 && i_400_ < -60640777 * anInt4992
		    && i_399_ >= 0 && i_399_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_400_][i_399_]
			= i > 0 ? (anIntArrayArrayArray4995[i - 1][i_400_]
				   [i_399_]) - 960 : 0;
	    }
	}
	if (i_395_ > 0 && i_395_ < anInt4992 * -60640777) {
	    for (int i_401_ = 1 + i_396_; i_401_ < i_398_ + i_396_; i_401_++) {
		if (i_401_ >= 0 && i_401_ < anInt5051 * -1584311401)
		    anIntArrayArrayArray4995[i][i_395_][i_401_]
			= anIntArrayArrayArray4995[i][i_395_ - 1][i_401_];
	    }
	}
	if (i_396_ > 0 && i_396_ < anInt5051 * -1584311401) {
	    for (int i_402_ = 1 + i_395_; i_402_ < i_395_ + i_397_; i_402_++) {
		if (i_402_ >= 0 && i_402_ < -60640777 * anInt4992)
		    anIntArrayArrayArray4995[i][i_402_][i_396_]
			= anIntArrayArrayArray4995[i][i_402_][i_396_ - 1];
	    }
	}
	if (i_395_ >= 0 && i_396_ >= 0 && i_395_ < -60640777 * anInt4992
	    && i_396_ < anInt5051 * -1584311401) {
	    if (i == 0) {
		if (i_395_ > 0
		    && 0 != anIntArrayArrayArray4995[i][i_395_ - 1][i_396_])
		    anIntArrayArrayArray4995[i][i_395_][i_396_]
			= anIntArrayArrayArray4995[i][i_395_ - 1][i_396_];
		else if (i_396_ > 0 && 0 != (anIntArrayArrayArray4995[i]
					     [i_395_][i_396_ - 1]))
		    anIntArrayArrayArray4995[i][i_395_][i_396_]
			= anIntArrayArrayArray4995[i][i_395_][i_396_ - 1];
		else if (i_395_ > 0 && i_396_ > 0
			 && (anIntArrayArrayArray4995[i][i_395_ - 1]
			     [i_396_ - 1]) != 0)
		    anIntArrayArrayArray4995[i][i_395_][i_396_]
			= anIntArrayArrayArray4995[i][i_395_ - 1][i_396_ - 1];
	    } else if (i_395_ > 0
		       && (anIntArrayArrayArray4995[i - 1][i_395_ - 1][i_396_]
			   != anIntArrayArrayArray4995[i][i_395_ - 1][i_396_]))
		anIntArrayArrayArray4995[i][i_395_][i_396_]
		    = anIntArrayArrayArray4995[i][i_395_ - 1][i_396_];
	    else if (i_396_ > 0
		     && (anIntArrayArrayArray4995[i][i_395_][i_396_ - 1]
			 != (anIntArrayArrayArray4995[i - 1][i_395_]
			     [i_396_ - 1])))
		anIntArrayArrayArray4995[i][i_395_][i_396_]
		    = anIntArrayArrayArray4995[i][i_395_][i_396_ - 1];
	    else if (i_395_ > 0 && i_396_ > 0
		     && ((anIntArrayArrayArray4995[i - 1][i_395_ - 1]
			  [i_396_ - 1])
			 != (anIntArrayArrayArray4995[i][i_395_ - 1]
			     [i_396_ - 1])))
		anIntArrayArrayArray4995[i][i_395_][i_396_]
		    = anIntArrayArrayArray4995[i][i_395_ - 1][i_396_ - 1];
	}
    }
    
    public final void method7475(int i, int[][] is) {
	int[][] is_403_ = anIntArrayArrayArray4995[i];
	for (int i_404_ = 0; i_404_ < -60640777 * anInt4992 + 1; i_404_++) {
	    for (int i_405_ = 0; i_405_ < 1 + -1584311401 * anInt5051;
		 i_405_++)
		is_403_[i_404_][i_405_] += is[i_404_][i_405_];
	}
    }
    
    static final int method7476(int i, int i_406_) {
	int i_407_
	    = (Class419.method6770(45365 + i, 91923 + i_406_, 4, 1748207138)
	       - 128
	       + (Class419.method6770(10294 + i, 37821 + i_406_, 2,
				      1748207138) - 128
		  >> 1)
	       + (Class419.method6770(i, i_406_, 1, 1748207138) - 128 >> 2));
	i_407_ = 35 + (int) (0.3 * (double) i_407_);
	if (i_407_ < 10)
	    i_407_ = 10;
	else if (i_407_ > 60)
	    i_407_ = 60;
	return i_407_;
    }
    
    public final void method7477(Class534_Sub40 class534_sub40, int i,
				 int i_408_, int i_409_, int i_410_,
				 int i_411_, int i_412_, int i_413_) {
	int i_414_ = (i_411_ & 0x7) * 8;
	int i_415_ = 8 * (i_412_ & 0x7);
	int i_416_ = (i_411_ & ~0x7) << 3;
	int i_417_ = (i_412_ & ~0x7) << 3;
	int i_418_ = 0;
	int i_419_ = 0;
	if (1 == i_413_)
	    i_419_ = 1;
	else if (2 == i_413_) {
	    i_418_ = 1;
	    i_419_ = 1;
	} else if (i_413_ == 3)
	    i_418_ = 1;
	for (int i_420_ = 0; i_420_ < -692901467 * anInt4991; i_420_++) {
	    for (int i_421_ = 0; i_421_ < 64; i_421_++) {
		for (int i_422_ = 0; i_422_ < 64; i_422_++) {
		    if (i_420_ == i_410_ && i_421_ >= i_414_
			&& i_421_ <= 8 + i_414_ && i_422_ >= i_415_
			&& i_422_ <= i_415_ + 8) {
			int i_423_;
			int i_424_;
			if (i_421_ == i_414_ + 8 || i_422_ == 8 + i_415_) {
			    if (0 == i_413_) {
				i_423_ = i_421_ - i_414_ + i_408_;
				i_424_ = i_409_ + (i_422_ - i_415_);
			    } else if (1 == i_413_) {
				i_423_ = i_408_ + (i_422_ - i_415_);
				i_424_ = i_409_ + 8 - (i_421_ - i_414_);
			    } else if (i_413_ == 2) {
				i_423_ = i_408_ + 8 - (i_421_ - i_414_);
				i_424_ = 8 + i_409_ - (i_422_ - i_415_);
			    } else {
				i_423_ = i_408_ + 8 - (i_422_ - i_415_);
				i_424_ = i_409_ + (i_421_ - i_414_);
			    }
			    method7493(class534_sub40, i, i_423_, i_424_, 0, 0,
				       i_421_ + i_416_, i_417_ + i_422_, 0,
				       true, 414042331);
			} else {
			    i_423_
				= i_408_ + Class644.method10681(i_421_ & 0x7,
								i_422_ & 0x7,
								i_413_,
								(byte) 27);
			    i_424_ = i_409_ + Class47.method1128(i_421_ & 0x7,
								 i_422_ & 0x7,
								 i_413_,
								 -458797615);
			    method7493(class534_sub40, i, i_423_, i_424_,
				       i_418_, i_419_, i_421_ + i_416_,
				       i_417_ + i_422_, i_413_, false,
				       414042331);
			}
			if (i_421_ == 63 || i_422_ == 63) {
			    int i_425_ = 1;
			    if (i_421_ == 63 && 63 == i_422_)
				i_425_ = 3;
			    for (int i_426_ = 0; i_426_ < i_425_; i_426_++) {
				int i_427_ = i_421_;
				int i_428_ = i_422_;
				if (i_426_ == 0) {
				    i_427_ = i_421_ == 63 ? 64 : i_421_;
				    i_428_ = 63 == i_422_ ? 64 : i_422_;
				} else if (1 == i_426_)
				    i_427_ = 64;
				else if (2 == i_426_)
				    i_428_ = 64;
				int i_429_;
				int i_430_;
				if (0 == i_413_) {
				    i_429_ = i_427_ - i_414_ + i_408_;
				    i_430_ = i_428_ - i_415_ + i_409_;
				} else if (1 == i_413_) {
				    i_429_ = i_408_ + (i_428_ - i_415_);
				    i_430_ = i_409_ + 8 - (i_427_ - i_414_);
				} else if (2 == i_413_) {
				    i_429_ = 8 + i_408_ - (i_427_ - i_414_);
				    i_430_ = 8 + i_409_ - (i_428_ - i_415_);
				} else {
				    i_429_ = i_408_ + 8 - (i_428_ - i_415_);
				    i_430_ = i_409_ + (i_427_ - i_414_);
				}
				if (i_429_ >= 0
				    && i_429_ < anInt4992 * -60640777
				    && i_430_ >= 0
				    && i_430_ < anInt5051 * -1584311401)
				    anIntArrayArrayArray4995[i][i_429_][i_430_]
					= (anIntArrayArrayArray4995[i]
					   [i_418_ + i_423_][i_419_ + i_424_]);
			    }
			}
		    } else
			method7493(class534_sub40, 0, -1, -1, 0, 0, 0, 0, 0,
				   false, 414042331);
		}
	    }
	}
    }
    
    public final void method7478(Class534_Sub40 class534_sub40, int i,
				 int i_431_, int i_432_, int i_433_,
				 int i_434_, int i_435_, int i_436_) {
	int i_437_ = (i_434_ & 0x7) * 8;
	int i_438_ = 8 * (i_435_ & 0x7);
	int i_439_ = (i_434_ & ~0x7) << 3;
	int i_440_ = (i_435_ & ~0x7) << 3;
	int i_441_ = 0;
	int i_442_ = 0;
	if (1 == i_436_)
	    i_442_ = 1;
	else if (2 == i_436_) {
	    i_441_ = 1;
	    i_442_ = 1;
	} else if (i_436_ == 3)
	    i_441_ = 1;
	for (int i_443_ = 0; i_443_ < -692901467 * anInt4991; i_443_++) {
	    for (int i_444_ = 0; i_444_ < 64; i_444_++) {
		for (int i_445_ = 0; i_445_ < 64; i_445_++) {
		    if (i_443_ == i_433_ && i_444_ >= i_437_
			&& i_444_ <= 8 + i_437_ && i_445_ >= i_438_
			&& i_445_ <= i_438_ + 8) {
			int i_446_;
			int i_447_;
			if (i_444_ == i_437_ + 8 || i_445_ == 8 + i_438_) {
			    if (0 == i_436_) {
				i_446_ = i_444_ - i_437_ + i_431_;
				i_447_ = i_432_ + (i_445_ - i_438_);
			    } else if (1 == i_436_) {
				i_446_ = i_431_ + (i_445_ - i_438_);
				i_447_ = i_432_ + 8 - (i_444_ - i_437_);
			    } else if (i_436_ == 2) {
				i_446_ = i_431_ + 8 - (i_444_ - i_437_);
				i_447_ = 8 + i_432_ - (i_445_ - i_438_);
			    } else {
				i_446_ = i_431_ + 8 - (i_445_ - i_438_);
				i_447_ = i_432_ + (i_444_ - i_437_);
			    }
			    method7493(class534_sub40, i, i_446_, i_447_, 0, 0,
				       i_444_ + i_439_, i_440_ + i_445_, 0,
				       true, 414042331);
			} else {
			    i_446_
				= i_431_ + Class644.method10681(i_444_ & 0x7,
								i_445_ & 0x7,
								i_436_,
								(byte) -16);
			    i_447_ = i_432_ + Class47.method1128(i_444_ & 0x7,
								 i_445_ & 0x7,
								 i_436_,
								 -1011150156);
			    method7493(class534_sub40, i, i_446_, i_447_,
				       i_441_, i_442_, i_444_ + i_439_,
				       i_440_ + i_445_, i_436_, false,
				       414042331);
			}
			if (i_444_ == 63 || i_445_ == 63) {
			    int i_448_ = 1;
			    if (i_444_ == 63 && 63 == i_445_)
				i_448_ = 3;
			    for (int i_449_ = 0; i_449_ < i_448_; i_449_++) {
				int i_450_ = i_444_;
				int i_451_ = i_445_;
				if (i_449_ == 0) {
				    i_450_ = i_444_ == 63 ? 64 : i_444_;
				    i_451_ = 63 == i_445_ ? 64 : i_445_;
				} else if (1 == i_449_)
				    i_450_ = 64;
				else if (2 == i_449_)
				    i_451_ = 64;
				int i_452_;
				int i_453_;
				if (0 == i_436_) {
				    i_452_ = i_450_ - i_437_ + i_431_;
				    i_453_ = i_451_ - i_438_ + i_432_;
				} else if (1 == i_436_) {
				    i_452_ = i_431_ + (i_451_ - i_438_);
				    i_453_ = i_432_ + 8 - (i_450_ - i_437_);
				} else if (2 == i_436_) {
				    i_452_ = 8 + i_431_ - (i_450_ - i_437_);
				    i_453_ = 8 + i_432_ - (i_451_ - i_438_);
				} else {
				    i_452_ = i_431_ + 8 - (i_451_ - i_438_);
				    i_453_ = i_432_ + (i_450_ - i_437_);
				}
				if (i_452_ >= 0
				    && i_452_ < anInt4992 * -60640777
				    && i_453_ >= 0
				    && i_453_ < anInt5051 * -1584311401)
				    anIntArrayArrayArray4995[i][i_452_][i_453_]
					= (anIntArrayArrayArray4995[i]
					   [i_441_ + i_446_][i_442_ + i_447_]);
			    }
			}
		    } else
			method7493(class534_sub40, 0, -1, -1, 0, 0, 0, 0, 0,
				   false, 414042331);
		}
	    }
	}
    }
    
    final void method7479(Class534_Sub40 class534_sub40, int i, int i_454_,
			  int i_455_, int i_456_, int i_457_, int i_458_,
			  int i_459_, int i_460_, boolean bool) {
	if (1 == i_460_)
	    i_457_ = 1;
	else if (2 == i_460_) {
	    i_456_ = 1;
	    i_457_ = 1;
	} else if (3 == i_460_)
	    i_456_ = 1;
	if (i_454_ >= 0 && i_454_ < -60640777 * anInt4992 && i_455_ >= 0
	    && i_455_ < -1584311401 * anInt5051) {
	    if (!aBool5002 && !bool)
		aClass468_4983.aByteArrayArrayArray5145[i][i_454_][i_455_]
		    = (byte) 0;
	    int i_461_ = class534_sub40.method16527(-2147143623);
	    if (0 != (i_461_ & 0x1)) {
		if (bool) {
		    class534_sub40.method16527(955416704);
		    class534_sub40.method16546(-1706829710);
		} else {
		    int i_462_ = class534_sub40.method16527(-145211622);
		    aShortArrayArrayArray5001[i][i_454_][i_455_]
			= (short) class534_sub40.method16546(-1706829710);
		    aByteArrayArrayArray5058[i][i_454_][i_455_]
			= (byte) (i_462_ >> 2);
		    aByteArrayArrayArray4988[i][i_454_][i_455_]
			= (byte) (i_462_ + i_460_ & 0x3);
		}
	    }
	    if ((i_461_ & 0x2) != 0) {
		if (!aBool5002 && !bool)
		    aClass468_4983.aByteArrayArrayArray5145[i][i_454_][i_455_]
			= class534_sub40.method16586((byte) 1);
		else
		    class534_sub40.anInt10811 += -1387468933;
	    }
	    if ((i_461_ & 0x4) != 0) {
		if (bool)
		    class534_sub40.method16546(-1706829710);
		else
		    aShortArrayArrayArray5000[i][i_454_][i_455_]
			= (short) class534_sub40.method16546(-1706829710);
	    }
	    if ((i_461_ & 0x8) != 0) {
		int i_463_ = class534_sub40.method16527(53276561);
		if (!aBool5002) {
		    if (i_463_ == 1)
			i_463_ = 0;
		    if (0 == i)
			anIntArrayArrayArray4995[0][i_456_ + i_454_]
			    [i_455_ + i_457_]
			    = 8 * -i_463_ << 2;
		    else
			anIntArrayArrayArray4995[i][i_454_ + i_456_]
			    [i_457_ + i_455_]
			    = (anIntArrayArrayArray4995[i - 1][i_456_ + i_454_]
			       [i_455_ + i_457_]) - (8 * i_463_ << 2);
		} else
		    anIntArrayArrayArray4995[0][i_456_ + i_454_][(i_457_
								  + i_455_)]
			= 8 * i_463_ << 2;
	    } else if (aBool5002)
		anIntArrayArrayArray4995[0][i_454_ + i_456_][i_457_ + i_455_]
		    = 0;
	    else if (0 == i)
		anIntArrayArrayArray4995[0][i_456_ + i_454_][i_457_ + i_455_]
		    = -Class224.method4161(i_458_ + 932731, i_459_ + 556238,
					   1674702540) * 8 << 2;
	    else
		anIntArrayArrayArray4995[i][i_456_ + i_454_][i_457_ + i_455_]
		    = (anIntArrayArrayArray4995[i - 1][i_456_ + i_454_]
		       [i_455_ + i_457_]) - 960;
	} else {
	    int i_464_ = class534_sub40.method16527(-1507806877);
	    if (0 != (i_464_ & 0x1)) {
		class534_sub40.method16527(1416238788);
		class534_sub40.method16546(-1706829710);
	    }
	    if (0 != (i_464_ & 0x2))
		class534_sub40.anInt10811 += -1387468933;
	    if ((i_464_ & 0x4) != 0)
		class534_sub40.method16546(-1706829710);
	    if ((i_464_ & 0x8) != 0)
		class534_sub40.method16527(484304955);
	}
    }
    
    final void method7480(Class534_Sub40 class534_sub40, int i, int i_465_,
			  int i_466_, int i_467_, int i_468_, int i_469_,
			  int i_470_, int i_471_, boolean bool) {
	if (1 == i_471_)
	    i_468_ = 1;
	else if (2 == i_471_) {
	    i_467_ = 1;
	    i_468_ = 1;
	} else if (3 == i_471_)
	    i_467_ = 1;
	if (i_465_ >= 0 && i_465_ < -60640777 * anInt4992 && i_466_ >= 0
	    && i_466_ < -1584311401 * anInt5051) {
	    if (!aBool5002 && !bool)
		aClass468_4983.aByteArrayArrayArray5145[i][i_465_][i_466_]
		    = (byte) 0;
	    int i_472_ = class534_sub40.method16527(595743526);
	    if (0 != (i_472_ & 0x1)) {
		if (bool) {
		    class534_sub40.method16527(-428966299);
		    class534_sub40.method16546(-1706829710);
		} else {
		    int i_473_ = class534_sub40.method16527(-813906650);
		    aShortArrayArrayArray5001[i][i_465_][i_466_]
			= (short) class534_sub40.method16546(-1706829710);
		    aByteArrayArrayArray5058[i][i_465_][i_466_]
			= (byte) (i_473_ >> 2);
		    aByteArrayArrayArray4988[i][i_465_][i_466_]
			= (byte) (i_473_ + i_471_ & 0x3);
		}
	    }
	    if ((i_472_ & 0x2) != 0) {
		if (!aBool5002 && !bool)
		    aClass468_4983.aByteArrayArrayArray5145[i][i_465_][i_466_]
			= class534_sub40.method16586((byte) 1);
		else
		    class534_sub40.anInt10811 += -1387468933;
	    }
	    if ((i_472_ & 0x4) != 0) {
		if (bool)
		    class534_sub40.method16546(-1706829710);
		else
		    aShortArrayArrayArray5000[i][i_465_][i_466_]
			= (short) class534_sub40.method16546(-1706829710);
	    }
	    if ((i_472_ & 0x8) != 0) {
		int i_474_ = class534_sub40.method16527(1717642516);
		if (!aBool5002) {
		    if (i_474_ == 1)
			i_474_ = 0;
		    if (0 == i)
			anIntArrayArrayArray4995[0][i_467_ + i_465_]
			    [i_466_ + i_468_]
			    = 8 * -i_474_ << 2;
		    else
			anIntArrayArrayArray4995[i][i_465_ + i_467_]
			    [i_468_ + i_466_]
			    = (anIntArrayArrayArray4995[i - 1][i_467_ + i_465_]
			       [i_466_ + i_468_]) - (8 * i_474_ << 2);
		} else
		    anIntArrayArrayArray4995[0][i_467_ + i_465_][(i_468_
								  + i_466_)]
			= 8 * i_474_ << 2;
	    } else if (aBool5002)
		anIntArrayArrayArray4995[0][i_465_ + i_467_][i_468_ + i_466_]
		    = 0;
	    else if (0 == i)
		anIntArrayArrayArray4995[0][i_467_ + i_465_][i_468_ + i_466_]
		    = -Class224.method4161(i_469_ + 932731, i_470_ + 556238,
					   1674702540) * 8 << 2;
	    else
		anIntArrayArrayArray4995[i][i_467_ + i_465_][i_468_ + i_466_]
		    = (anIntArrayArrayArray4995[i - 1][i_467_ + i_465_]
		       [i_466_ + i_468_]) - 960;
	} else {
	    int i_475_ = class534_sub40.method16527(-2057698837);
	    if (0 != (i_475_ & 0x1)) {
		class534_sub40.method16527(1107590580);
		class534_sub40.method16546(-1706829710);
	    }
	    if (0 != (i_475_ & 0x2))
		class534_sub40.anInt10811 += -1387468933;
	    if ((i_475_ & 0x4) != 0)
		class534_sub40.method16546(-1706829710);
	    if ((i_475_ & 0x8) != 0)
		class534_sub40.method16527(-468285026);
	}
    }
    
    public final void method7481(int i, int[][] is) {
	int[][] is_476_ = anIntArrayArrayArray4995[i];
	for (int i_477_ = 0; i_477_ < -60640777 * anInt4992 + 1; i_477_++) {
	    for (int i_478_ = 0; i_478_ < 1 + -1584311401 * anInt5051;
		 i_478_++)
		is_476_[i_477_][i_478_] += is[i_477_][i_478_];
	}
    }
    
    int method7482(int i, int i_479_, int i_480_, int i_481_, int i_482_,
		   Class151 class151, short[][] is) {
	if ((0 == anInt5052 * -648787169 || 12 == anInt5052 * -648787169)
	    && i_479_ > 0 && i_480_ > 0 && i_479_ < -60640777 * anInt4992
	    && i_480_ < -1584311401 * anInt5051) {
	    int i_483_ = 0;
	    int i_484_ = 0;
	    int i_485_ = 0;
	    int i_486_ = 0;
	    i_483_ = i_483_ + (i == is[i_479_ - 1][i_480_ - 1] ? 1 : -1);
	    i_484_ = i_484_ + (i == is[i_481_][i_480_ - 1] ? 1 : -1);
	    i_485_ = i_485_ + (is[i_481_][i_482_] == i ? 1 : -1);
	    i_486_ = i_486_ + (is[i_479_ - 1][i_482_] == i ? 1 : -1);
	    if (is[i_479_][i_480_ - 1] == i) {
		i_483_++;
		i_484_++;
	    } else {
		i_483_--;
		i_484_--;
	    }
	    if (is[i_481_][i_480_] == i) {
		i_484_++;
		i_485_++;
	    } else {
		i_484_--;
		i_485_--;
	    }
	    if (i == is[i_479_][i_482_]) {
		i_485_++;
		i_486_++;
	    } else {
		i_485_--;
		i_486_--;
	    }
	    if (is[i_479_ - 1][i_480_] == i) {
		i_486_++;
		i_483_++;
	    } else {
		i_486_--;
		i_483_--;
	    }
	    int i_487_ = i_483_ - i_485_;
	    if (i_487_ < 0)
		i_487_ = -i_487_;
	    int i_488_ = i_484_ - i_486_;
	    if (i_488_ < 0)
		i_488_ = -i_488_;
	    if (i_488_ == i_487_) {
		i_487_ = (class151.method2491(i_479_, i_480_, -1042904419)
			  - class151.method2491(i_481_, i_482_, 1070970609));
		if (i_487_ < 0)
		    i_487_ = -i_487_;
		i_488_ = (class151.method2491(i_481_, i_480_, -1941940225)
			  - class151.method2491(i_479_, i_482_, -305142714));
		if (i_488_ < 0)
		    i_488_ = -i_488_;
	    }
	    return i_487_ < i_488_ ? 1 : 0;
	}
	return anInt5053 * -1693107127;
    }
    
    void method7483(Class185 class185, int i, int i_489_, int i_490_,
		    Class651 class651, boolean[] bools, int[] is,
		    int[] is_491_, int[] is_492_, int[] is_493_, int[] is_494_,
		    int[] is_495_, int[] is_496_, int[] is_497_,
		    Class151 class151, Class151 class151_498_,
		    Class151 class151_499_) {
	anInt5046 = 1053793721;
	anInt5056 = 586067143;
	anInt5057 = 1487773952;
	if (class651 != null) {
	    anInt5046 = 192513827 * class651.anInt8467;
	    anInt5056 = 2015327183 * class651.anInt8468;
	    anInt5057 = 1989313493 * class651.anInt8471;
	    int i_500_ = Class689.method14014(class185, class651, -2043821582);
	    for (int i_501_ = 0; i_501_ < anInt5064 * -1278434163; i_501_++) {
		boolean bool = false;
		int i_502_;
		if (bools[0 - anInt5053 * -1693107127 & 0x3]
		    && anIntArray5068[0] == anInt5005 * 1299058195) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 1;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 1;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_502_ = 6;
		} else if (bools[2 - anInt5053 * -1693107127 & 0x3]
			   && anIntArray5068[2] == 1299058195 * anInt5005) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 5;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 5;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_502_ = 6;
		} else if (bools[1 - anInt5053 * -1693107127 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[1]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 3;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 3;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_502_ = 6;
		} else if (bools[3 - -1693107127 * anInt5053 & 0x3]
			   && anInt5005 * 1299058195 == anIntArray5068[3]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 7;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 7;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_502_ = 6;
		} else {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    i_502_ = 3;
		}
		for (int i_503_ = 0; i_503_ < i_502_; i_503_++) {
		    int i_504_ = anIntArray5044[i_503_];
		    int i_505_ = i_504_ - 908753042 * anInt5053 & 0x7;
		    int i_506_ = anIntArray5028[i_504_];
		    int i_507_ = anIntArray5029[i_504_];
		    int i_508_;
		    int i_509_;
		    if (anInt5053 * -1693107127 == 1) {
			i_508_ = i_507_;
			i_509_ = 512 - i_506_;
		    } else if (anInt5053 * -1693107127 == 2) {
			i_508_ = 512 - i_506_;
			i_509_ = 512 - i_507_;
		    } else if (-1693107127 * anInt5053 == 3) {
			i_508_ = 512 - i_507_;
			i_509_ = i_506_;
		    } else {
			i_508_ = i_506_;
			i_509_ = i_507_;
		    }
		    is_491_[anInt5055 * 551530869] = i_508_;
		    is_492_[551530869 * anInt5055] = i_509_;
		    if (null != is_496_
			&& (aBoolArrayArray5011[anInt5052 * -648787169]
			    [i_504_])) {
			int i_510_ = i_508_ + (i_489_ << 9);
			int i_511_ = (i_490_ << 9) + i_509_;
			is_496_[551530869 * anInt5055]
			    = (class151_498_.method2498(i_510_, i_511_,
							-1162647905)
			       - class151.method2498(i_510_, i_511_,
						     1580977820));
		    }
		    if (is_497_ != null) {
			if (null != class151_498_
			    && !(aBoolArrayArray5011[anInt5052 * -648787169]
				 [i_504_])) {
			    int i_512_ = i_508_ + (i_489_ << 9);
			    int i_513_ = i_509_ + (i_490_ << 9);
			    is_497_[anInt5055 * 551530869]
				= (class151.method2498(i_512_, i_513_,
						       -927765845)
				   - class151_498_.method2498(i_512_, i_513_,
							      1028292485));
			} else if (null != class151_499_
				   && !(aBoolArrayArray5031
					[anInt5052 * -648787169][i_504_])) {
			    int i_514_ = i_508_ + (i_489_ << 9);
			    int i_515_ = i_509_ + (i_490_ << 9);
			    is_497_[551530869 * anInt5055]
				= (class151_499_.method2498(i_514_, i_515_,
							    -2081601192)
				   - class151.method2498(i_514_, i_515_,
							 -1298596216));
			}
		    }
		    if (i_504_ < 8 && (anIntArray5050[i_505_]
				       > class651.anInt8473 * 589238839)) {
			if (null != is)
			    is[551530869 * anInt5055] = anIntArray5047[i_505_];
			is_495_[551530869 * anInt5055]
			    = anIntArray5054[i_505_];
			is_494_[551530869 * anInt5055]
			    = anIntArray4987[i_505_];
			is_493_[anInt5055 * 551530869]
			    = anIntArray5033[i_505_];
		    } else {
			if (is != null)
			    is[551530869 * anInt5055] = i_500_;
			is_494_[551530869 * anInt5055]
			    = class651.anInt8468 * 1884378951;
			is_495_[551530869 * anInt5055]
			    = 431890869 * class651.anInt8471;
			is_493_[551530869 * anInt5055] = anInt5046 * 124430199;
		    }
		    anInt5055 += -1596139811;
		}
		anInt5005 += -817239013;
	    }
	    if (!aBool5002 && 0 == i)
		aClass556_4984.method9261(i_489_, i_490_,
					  -1652506135 * class651.anInt8475,
					  390419480 * class651.anInt8476,
					  class651.anInt8466 * -1594141697,
					  class651.anInt8465 * -672944661,
					  1112938727 * class651.anInt8479,
					  1597465863 * class651.anInt8480,
					  -500988450);
	    if (12 != -648787169 * anInt5052
		&& -1 != -2044484027 * class651.anInt8467
		&& class651.aBool8472)
		aBool5049 = true;
	} else if (aBool5061)
	    anInt5005 += anIntArray5022[anInt5052 * -648787169] * -817239013;
	else if (aBool5060)
	    anInt5005 += anIntArray5026[-648787169 * anInt5052] * -817239013;
	else
	    anInt5005 += -817239013 * anIntArray5024[-648787169 * anInt5052];
    }
    
    void method7484(Class185 class185, Class151 class151, int i, int[][] is,
		    Class151 class151_516_, Class151 class151_517_) {
	byte[][] is_518_ = aByteArrayArrayArray5058[i];
	byte[][] is_519_ = aByteArrayArrayArray4988[i];
	short[][] is_520_ = aShortArrayArrayArray5000[i];
	short[][] is_521_ = aShortArrayArrayArray5001[i];
	boolean[] bools = new boolean[4];
	for (int i_522_ = 0; i_522_ < -60640777 * anInt4992; i_522_++) {
	    int i_523_
		= i_522_ < anInt4992 * -60640777 - 1 ? i_522_ + 1 : i_522_;
	    for (int i_524_ = 0; i_524_ < -1584311401 * anInt5051; i_524_++) {
		int i_525_ = (i_524_ < anInt5051 * -1584311401 - 1 ? i_524_ + 1
			      : i_524_);
		anInt5052 = is_518_[i_522_][i_524_] * 1068200159;
		anInt5053 = is_519_[i_522_][i_524_] * 1735620089;
		int i_526_ = is_521_[i_522_][i_524_] & 0x7fff;
		int i_527_ = is_520_[i_522_][i_524_] & 0x7fff;
		if (0 != i_526_ || i_527_ != 0) {
		    Class651 class651
			= ((Class651)
			   (0 != i_526_
			    ? aClass44_Sub16_4996.method91(i_526_ - 1,
							   316308539)
			    : null));
		    Class1 class1
			= (Class1) (0 != i_527_
				    ? aClass44_Sub12_4982.method91(i_527_ - 1,
								   -313744292)
				    : null);
		    if (-648787169 * anInt5052 == 0 && class651 == null)
			anInt5052 = -66499980;
		    aBool5049 = false;
		    aBool5060 = false;
		    boolean[] bools_528_ = bools;
		    boolean[] bools_529_ = bools;
		    boolean[] bools_530_ = bools;
		    bools[3] = false;
		    bools_530_[2] = false;
		    bools_529_[1] = false;
		    bools_528_[0] = false;
		    Class651 class651_531_ = class651;
		    if (class651 != null) {
			if (-1 == -2044484027 * class651.anInt8467
			    && 33386845 * class651.anInt8470 == -1) {
			    class651_531_ = class651;
			    class651 = null;
			} else if (null != class1
				   && anInt5052 * -648787169 != 0)
			    aBool5060 = class651.aBool8474;
		    }
		    anInt5053 = method7508(i_527_, i_522_, i_524_, i_523_,
					   i_525_, class151, is_520_,
					   -879941123) * 1735620089;
		    for (int i_532_ = 0; i_532_ < 13; i_532_++) {
			anIntArray5050[i_532_] = -1;
			anIntArray4993[i_532_] = 1;
		    }
		    method7503(class185, class651, class1, i_522_, i_524_,
			       is_518_, is_519_, is_521_, bools, (byte) 0);
		    aBool5061 = (!aBool5060 && !bools[0] && !bools[2]
				 && !bools[1] && !bools[3]);
		    method7460(class651, class1, 35040);
		    int i_533_
			= anInt5027 * -1457646951 + anInt5064 * -1278434163;
		    if (i_533_ <= 0)
			aClass556_4984.method9367(i, i_522_, i_524_,
						  -1515840006);
		    else {
			if (bools[0])
			    i_533_++;
			if (bools[2])
			    i_533_++;
			if (bools[1])
			    i_533_++;
			if (bools[3])
			    i_533_++;
			anInt5005 = 0;
			anInt5055 = 0;
			int i_534_ = 3 * i_533_;
			int[] is_535_ = aBool5062 ? new int[i_534_] : null;
			int[] is_536_ = new int[i_534_];
			int[] is_537_ = new int[i_534_];
			int[] is_538_ = new int[i_534_];
			int[] is_539_ = new int[i_534_];
			int[] is_540_ = new int[i_534_];
			int[] is_541_
			    = class151_517_ != null ? new int[i_534_] : null;
			int[] is_542_
			    = (null != class151_517_ || class151_516_ != null
			       ? new int[i_534_] : null);
			for (int i_543_ = 0; i_543_ < i_534_; i_543_++)
			    is_539_[i_543_] = -1;
			method7472(class185, i, i_522_, i_524_, class651,
				   bools, is_535_, is_536_, is_537_, is_538_,
				   is_539_, is_540_, is_541_, is_542_,
				   class151, class151_517_, class151_516_,
				   1169540771);
			int i_544_ = is_520_[i_522_][i_525_] & 0x7fff;
			int i_545_ = is_520_[i_523_][i_525_] & 0x7fff;
			int i_546_ = is_520_[i_523_][i_524_] & 0x7fff;
			method7462(class185, i, i_522_, i_524_, i_523_, i_525_,
				   class1, i_527_, i_544_, i_545_, i_546_,
				   bools, is_535_, is_536_, is_537_, is_538_,
				   is_539_, is_540_, is_541_, is_542_, is,
				   class151, class151_517_, class151_516_,
				   1513753474);
			method7463(class151, class1, class651_531_, i, i_522_,
				   i_524_, i_523_, i_525_, i_527_, i_526_,
				   (byte) 4);
			Class166 class166 = new Class166();
			if (aBool5002) {
			    class166.anInt1763
				= (aClass556_4984.method9305(i_522_, i_524_,
							     698475051)
				   * 929497037);
			    class166.anInt1762
				= (aClass556_4984.method9283(i_522_, i_524_,
							     (byte) -21)
				   * 1063224577);
			    class166.anInt1764
				= (aClass556_4984.method9331(i_522_, i_524_,
							     1110637120)
				   * -1101271945);
			    class166.anInt1765
				= (aClass556_4984.method9238(i_522_, i_524_,
							     (byte) -81)
				   * -1961234339);
			    class166.anInt1766
				= (aClass556_4984.method9239(i_522_, i_524_,
							     510034777)
				   * -1338809385);
			    class166.anInt1767
				= (aClass556_4984.method9310(i_522_, i_524_,
							     (short) -1501)
				   * -2041662327);
			}
			class151.method2493(i_522_, i_524_, is_536_, is_541_,
					    is_537_, is_542_, is_538_, is_535_,
					    is_539_, is_540_, class166,
					    aBool5049);
			aClass556_4984.method9367(i, i_522_, i_524_,
						  -1698507349);
		    }
		}
	    }
	}
    }
    
    static final int method7485(int i, int i_547_) {
	int i_548_
	    = (Class419.method6770(45365 + i, 91923 + i_547_, 4, 1748207138)
	       - 128
	       + (Class419.method6770(10294 + i, 37821 + i_547_, 2,
				      1748207138) - 128
		  >> 1)
	       + (Class419.method6770(i, i_547_, 1, 1748207138) - 128 >> 2));
	i_548_ = 35 + (int) (0.3 * (double) i_548_);
	if (i_548_ < 10)
	    i_548_ = 10;
	else if (i_548_ > 60)
	    i_548_ = 60;
	return i_548_;
    }
    
    public final void method7486(int i, int i_549_, int i_550_, int i_551_) {
	for (int i_552_ = 0; i_552_ < -692901467 * anInt4991; i_552_++)
	    method7449(i_552_, i, i_549_, i_550_, i_551_, -904088808);
    }
    
    int method7487(int i, int i_553_, int i_554_, int i_555_, int i_556_,
		   Class151 class151, short[][] is) {
	if ((0 == anInt5052 * -648787169 || 12 == anInt5052 * -648787169)
	    && i_553_ > 0 && i_554_ > 0 && i_553_ < -60640777 * anInt4992
	    && i_554_ < -1584311401 * anInt5051) {
	    int i_557_ = 0;
	    int i_558_ = 0;
	    int i_559_ = 0;
	    int i_560_ = 0;
	    i_557_ = i_557_ + (i == is[i_553_ - 1][i_554_ - 1] ? 1 : -1);
	    i_558_ = i_558_ + (i == is[i_555_][i_554_ - 1] ? 1 : -1);
	    i_559_ = i_559_ + (is[i_555_][i_556_] == i ? 1 : -1);
	    i_560_ = i_560_ + (is[i_553_ - 1][i_556_] == i ? 1 : -1);
	    if (is[i_553_][i_554_ - 1] == i) {
		i_557_++;
		i_558_++;
	    } else {
		i_557_--;
		i_558_--;
	    }
	    if (is[i_555_][i_554_] == i) {
		i_558_++;
		i_559_++;
	    } else {
		i_558_--;
		i_559_--;
	    }
	    if (i == is[i_553_][i_556_]) {
		i_559_++;
		i_560_++;
	    } else {
		i_559_--;
		i_560_--;
	    }
	    if (is[i_553_ - 1][i_554_] == i) {
		i_560_++;
		i_557_++;
	    } else {
		i_560_--;
		i_557_--;
	    }
	    int i_561_ = i_557_ - i_559_;
	    if (i_561_ < 0)
		i_561_ = -i_561_;
	    int i_562_ = i_558_ - i_560_;
	    if (i_562_ < 0)
		i_562_ = -i_562_;
	    if (i_562_ == i_561_) {
		i_561_ = (class151.method2491(i_553_, i_554_, -1249796535)
			  - class151.method2491(i_555_, i_556_, -1711824529));
		if (i_561_ < 0)
		    i_561_ = -i_561_;
		i_562_ = (class151.method2491(i_555_, i_554_, -1426591978)
			  - class151.method2491(i_553_, i_556_, 2025560320));
		if (i_562_ < 0)
		    i_562_ = -i_562_;
	    }
	    return i_561_ < i_562_ ? 1 : 0;
	}
	return anInt5053 * -1693107127;
    }
    
    void method7488(Class185 class185, Class651 class651, Class1 class1, int i,
		    int i_563_, byte[][] is, byte[][] is_564_,
		    short[][] is_565_, boolean[] bools) {
	boolean[] bools_566_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[anInt5052 * -648787169]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	method7464(class185, class651, class1, i, i_563_,
		   -60640777 * anInt4992, anInt5051 * -1584311401, is_565_, is,
		   is_564_, bools, 447514599);
	aBool5062 = class651 != null && (class651.anInt8467 * -2044484027
					 != class651.anInt8470 * 33386845);
	if (!aBool5062) {
	    for (int i_567_ = 0; i_567_ < 8; i_567_++) {
		if (anIntArray5050[i_567_] >= 0
		    && anIntArray5047[i_567_] != anIntArray5033[i_567_]) {
		    aBool5062 = true;
		    break;
		}
	    }
	}
	if (!bools_566_[anInt5053 * -1693107127 + 1 & 0x3]) {
	    boolean[] bools_568_ = bools;
	    int i_569_ = 1;
	    bools_568_[i_569_]
		= bools_568_[i_569_] | 0 == (anIntArray4993[2]
					     & anIntArray4993[4]);
	}
	if (!bools_566_[anInt5053 * -1693107127 + 3 & 0x3]) {
	    boolean[] bools_570_ = bools;
	    int i_571_ = 3;
	    bools_570_[i_571_]
		= bools_570_[i_571_] | 0 == (anIntArray4993[6]
					     & anIntArray4993[0]);
	}
	if (!bools_566_[-1693107127 * anInt5053 + 0 & 0x3]) {
	    boolean[] bools_572_ = bools;
	    int i_573_ = 0;
	    bools_572_[i_573_]
		= bools_572_[i_573_] | 0 == (anIntArray4993[0]
					     & anIntArray4993[2]);
	}
	if (!bools_566_[-1693107127 * anInt5053 + 2 & 0x3]) {
	    boolean[] bools_574_ = bools;
	    int i_575_ = 2;
	    bools_574_[i_575_]
		= (bools_574_[i_575_]
		   | (anIntArray4993[4] & anIntArray4993[6]) == 0);
	}
	if (!aBool5060
	    && (-648787169 * anInt5052 == 0 || anInt5052 * -648787169 == 12)) {
	    if (bools[0] && !bools[1] && !bools[2] && bools[3]) {
		boolean[] bools_576_ = bools;
		bools[3] = false;
		bools_576_[0] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = 0;
	    } else if (bools[0] && bools[1] && !bools[2] && !bools[3]) {
		boolean[] bools_577_ = bools;
		bools[1] = false;
		bools_577_[0] = false;
		anInt5052
		    = (-648787169 * anInt5052 == 0 ? 13 : 14) * 1068200159;
		anInt5053 = 911892971;
	    } else if (!bools[0] && bools[1] && bools[2] && !bools[3]) {
		boolean[] bools_578_ = bools;
		bools[2] = false;
		bools_578_[1] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = -823727118;
	    } else if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
		boolean[] bools_579_ = bools;
		bools[3] = false;
		bools_579_[2] = false;
		anInt5052
		    = 1068200159 * (0 == -648787169 * anInt5052 ? 13 : 14);
		anInt5053 = 1735620089;
	    }
	}
    }
    
    void method7489(Class185 class185, Class651 class651, Class1 class1, int i,
		    int i_580_, byte[][] is, byte[][] is_581_,
		    short[][] is_582_, boolean[] bools) {
	boolean[] bools_583_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[anInt5052 * -648787169]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	method7464(class185, class651, class1, i, i_580_,
		   -60640777 * anInt4992, anInt5051 * -1584311401, is_582_, is,
		   is_581_, bools, -29557574);
	aBool5062 = class651 != null && (class651.anInt8467 * -2044484027
					 != class651.anInt8470 * 33386845);
	if (!aBool5062) {
	    for (int i_584_ = 0; i_584_ < 8; i_584_++) {
		if (anIntArray5050[i_584_] >= 0
		    && anIntArray5047[i_584_] != anIntArray5033[i_584_]) {
		    aBool5062 = true;
		    break;
		}
	    }
	}
	if (!bools_583_[anInt5053 * -1693107127 + 1 & 0x3]) {
	    boolean[] bools_585_ = bools;
	    int i_586_ = 1;
	    bools_585_[i_586_]
		= bools_585_[i_586_] | 0 == (anIntArray4993[2]
					     & anIntArray4993[4]);
	}
	if (!bools_583_[anInt5053 * -1693107127 + 3 & 0x3]) {
	    boolean[] bools_587_ = bools;
	    int i_588_ = 3;
	    bools_587_[i_588_]
		= bools_587_[i_588_] | 0 == (anIntArray4993[6]
					     & anIntArray4993[0]);
	}
	if (!bools_583_[-1693107127 * anInt5053 + 0 & 0x3]) {
	    boolean[] bools_589_ = bools;
	    int i_590_ = 0;
	    bools_589_[i_590_]
		= bools_589_[i_590_] | 0 == (anIntArray4993[0]
					     & anIntArray4993[2]);
	}
	if (!bools_583_[-1693107127 * anInt5053 + 2 & 0x3]) {
	    boolean[] bools_591_ = bools;
	    int i_592_ = 2;
	    bools_591_[i_592_]
		= (bools_591_[i_592_]
		   | (anIntArray4993[4] & anIntArray4993[6]) == 0);
	}
	if (!aBool5060
	    && (-648787169 * anInt5052 == 0 || anInt5052 * -648787169 == 12)) {
	    if (bools[0] && !bools[1] && !bools[2] && bools[3]) {
		boolean[] bools_593_ = bools;
		bools[3] = false;
		bools_593_[0] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = 0;
	    } else if (bools[0] && bools[1] && !bools[2] && !bools[3]) {
		boolean[] bools_594_ = bools;
		bools[1] = false;
		bools_594_[0] = false;
		anInt5052
		    = (-648787169 * anInt5052 == 0 ? 13 : 14) * 1068200159;
		anInt5053 = 911892971;
	    } else if (!bools[0] && bools[1] && bools[2] && !bools[3]) {
		boolean[] bools_595_ = bools;
		bools[2] = false;
		bools_595_[1] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = -823727118;
	    } else if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
		boolean[] bools_596_ = bools;
		bools[3] = false;
		bools_596_[2] = false;
		anInt5052
		    = 1068200159 * (0 == -648787169 * anInt5052 ? 13 : 14);
		anInt5053 = 1735620089;
	    }
	}
    }
    
    Class460(Class556 class556, int i, int i_597_, int i_598_, boolean bool,
	     Class44_Sub16 class44_sub16, Class44_Sub12 class44_sub12,
	     Class468 class468) {
	aBool4998 = false;
	aBool4981 = false;
	aBool4990 = false;
	anIntArray5044 = new int[6];
	anIntArray5033 = new int[13];
	anIntArray5047 = new int[13];
	anIntArray4987 = new int[13];
	anIntArray5054 = new int[13];
	anIntArray5050 = new int[13];
	anIntArray4993 = new int[13];
	anInt5005 = 0;
	anInt5055 = 0;
	anIntArray5068 = null;
	aClass556_4984 = class556;
	anInt4991 = 2126485037 * i;
	anInt4992 = i_597_ * 1533763527;
	anInt5051 = -1342284761 * i_598_;
	aBool5002 = bool;
	aClass44_Sub16_4996 = class44_sub16;
	aClass44_Sub12_4982 = class44_sub12;
	aClass468_4983 = class468;
	aShortArrayArrayArray5000
	    = (new short[-692901467 * anInt4991][-60640777 * anInt4992]
	       [-1584311401 * anInt5051]);
	aShortArrayArrayArray5001
	    = (new short[anInt4991 * -692901467][anInt4992 * -60640777]
	       [-1584311401 * anInt5051]);
	aByteArrayArrayArray5058
	    = (new byte[anInt4991 * -692901467][anInt4992 * -60640777]
	       [anInt5051 * -1584311401]);
	aByteArrayArrayArray4988
	    = (new byte[anInt4991 * -692901467][-60640777 * anInt4992]
	       [-1584311401 * anInt5051]);
	anIntArrayArrayArray4995
	    = (new int[anInt4991 * -692901467][1 + anInt4992 * -60640777]
	       [1 + anInt5051 * -1584311401]);
	aByteArrayArrayArray4989
	    = (new byte[anInt4991 * -692901467][anInt4992 * -60640777 + 1]
	       [-1584311401 * anInt5051 + 1]);
    }
    
    public final void method7490(Class534_Sub40 class534_sub40, int i,
				 int i_599_, int i_600_, int i_601_,
				 int i_602_, int i_603_, int i_604_) {
	int i_605_ = (i_602_ & 0x7) * 8;
	int i_606_ = 8 * (i_603_ & 0x7);
	int i_607_ = (i_602_ & ~0x7) << 3;
	int i_608_ = (i_603_ & ~0x7) << 3;
	int i_609_ = 0;
	int i_610_ = 0;
	if (1 == i_604_)
	    i_610_ = 1;
	else if (2 == i_604_) {
	    i_609_ = 1;
	    i_610_ = 1;
	} else if (i_604_ == 3)
	    i_609_ = 1;
	for (int i_611_ = 0; i_611_ < -692901467 * anInt4991; i_611_++) {
	    for (int i_612_ = 0; i_612_ < 64; i_612_++) {
		for (int i_613_ = 0; i_613_ < 64; i_613_++) {
		    if (i_611_ == i_601_ && i_612_ >= i_605_
			&& i_612_ <= 8 + i_605_ && i_613_ >= i_606_
			&& i_613_ <= i_606_ + 8) {
			int i_614_;
			int i_615_;
			if (i_612_ == i_605_ + 8 || i_613_ == 8 + i_606_) {
			    if (0 == i_604_) {
				i_614_ = i_612_ - i_605_ + i_599_;
				i_615_ = i_600_ + (i_613_ - i_606_);
			    } else if (1 == i_604_) {
				i_614_ = i_599_ + (i_613_ - i_606_);
				i_615_ = i_600_ + 8 - (i_612_ - i_605_);
			    } else if (i_604_ == 2) {
				i_614_ = i_599_ + 8 - (i_612_ - i_605_);
				i_615_ = 8 + i_600_ - (i_613_ - i_606_);
			    } else {
				i_614_ = i_599_ + 8 - (i_613_ - i_606_);
				i_615_ = i_600_ + (i_612_ - i_605_);
			    }
			    method7493(class534_sub40, i, i_614_, i_615_, 0, 0,
				       i_612_ + i_607_, i_608_ + i_613_, 0,
				       true, 414042331);
			} else {
			    i_614_
				= i_599_ + Class644.method10681(i_612_ & 0x7,
								i_613_ & 0x7,
								i_604_,
								(byte) 75);
			    i_615_ = i_600_ + Class47.method1128(i_612_ & 0x7,
								 i_613_ & 0x7,
								 i_604_,
								 1506762594);
			    method7493(class534_sub40, i, i_614_, i_615_,
				       i_609_, i_610_, i_612_ + i_607_,
				       i_608_ + i_613_, i_604_, false,
				       414042331);
			}
			if (i_612_ == 63 || i_613_ == 63) {
			    int i_616_ = 1;
			    if (i_612_ == 63 && 63 == i_613_)
				i_616_ = 3;
			    for (int i_617_ = 0; i_617_ < i_616_; i_617_++) {
				int i_618_ = i_612_;
				int i_619_ = i_613_;
				if (i_617_ == 0) {
				    i_618_ = i_612_ == 63 ? 64 : i_612_;
				    i_619_ = 63 == i_613_ ? 64 : i_613_;
				} else if (1 == i_617_)
				    i_618_ = 64;
				else if (2 == i_617_)
				    i_619_ = 64;
				int i_620_;
				int i_621_;
				if (0 == i_604_) {
				    i_620_ = i_618_ - i_605_ + i_599_;
				    i_621_ = i_619_ - i_606_ + i_600_;
				} else if (1 == i_604_) {
				    i_620_ = i_599_ + (i_619_ - i_606_);
				    i_621_ = i_600_ + 8 - (i_618_ - i_605_);
				} else if (2 == i_604_) {
				    i_620_ = 8 + i_599_ - (i_618_ - i_605_);
				    i_621_ = 8 + i_600_ - (i_619_ - i_606_);
				} else {
				    i_620_ = i_599_ + 8 - (i_619_ - i_606_);
				    i_621_ = i_600_ + (i_618_ - i_605_);
				}
				if (i_620_ >= 0
				    && i_620_ < anInt4992 * -60640777
				    && i_621_ >= 0
				    && i_621_ < anInt5051 * -1584311401)
				    anIntArrayArrayArray4995[i][i_620_][i_621_]
					= (anIntArrayArrayArray4995[i]
					   [i_609_ + i_614_][i_610_ + i_615_]);
			    }
			}
		    } else
			method7493(class534_sub40, 0, -1, -1, 0, 0, 0, 0, 0,
				   false, 414042331);
		}
	    }
	}
    }
    
    void method7491(Class651 class651, Class1 class1) {
	if (aBool5061) {
	    anIntArray5065 = anIntArrayArray4985[anInt5052 * -648787169];
	    anIntArray5066 = anIntArrayArray4994[-648787169 * anInt5052];
	    anIntArray5007 = anIntArrayArray5034[-648787169 * anInt5052];
	    anInt5064
		= (null != class651 ? anIntArray5022[anInt5052 * -648787169]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (null != class1
				? anIntArray5023[-648787169 * anInt5052] : 0);
	} else if (aBool5060) {
	    anIntArray5065 = anIntArrayArray5042[-648787169 * anInt5052];
	    anIntArray5066 = anIntArrayArray5025[anInt5052 * -648787169];
	    anIntArray5007 = anIntArrayArray5032[-648787169 * anInt5052];
	    anInt5064
		= (class651 != null ? anIntArray5026[anInt5052 * -648787169]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (class1 != null
				? anIntArray5059[anInt5052 * -648787169] : 0);
	    anIntArray5068 = anIntArrayArray5041[-648787169 * anInt5052];
	} else {
	    anIntArray5065 = anIntArrayArray5037[anInt5052 * -648787169];
	    anIntArray5066 = anIntArrayArray5038[anInt5052 * -648787169];
	    anIntArray5007 = anIntArrayArray5030[anInt5052 * -648787169];
	    anInt5064
		= (class651 != null ? anIntArray5024[-648787169 * anInt5052]
		   : 0) * 106151493;
	    anInt5027
		= -926932567 * (class1 != null
				? anIntArray5039[anInt5052 * -648787169] : 0);
	    anIntArray5068 = anIntArrayArray5036[anInt5052 * -648787169];
	}
    }
    
    void method7492(Class185 class185, Class151 class151, int i, int[][] is,
		    Class151 class151_622_, Class151 class151_623_) {
	byte[][] is_624_ = aByteArrayArrayArray5058[i];
	byte[][] is_625_ = aByteArrayArrayArray4988[i];
	short[][] is_626_ = aShortArrayArrayArray5000[i];
	short[][] is_627_ = aShortArrayArrayArray5001[i];
	boolean[] bools = new boolean[4];
	for (int i_628_ = 0; i_628_ < -60640777 * anInt4992; i_628_++) {
	    int i_629_
		= i_628_ < anInt4992 * -60640777 - 1 ? i_628_ + 1 : i_628_;
	    for (int i_630_ = 0; i_630_ < -1584311401 * anInt5051; i_630_++) {
		int i_631_ = (i_630_ < anInt5051 * -1584311401 - 1 ? i_630_ + 1
			      : i_630_);
		anInt5052 = is_624_[i_628_][i_630_] * 1068200159;
		anInt5053 = is_625_[i_628_][i_630_] * 1735620089;
		int i_632_ = is_627_[i_628_][i_630_] & 0x7fff;
		int i_633_ = is_626_[i_628_][i_630_] & 0x7fff;
		if (0 != i_632_ || i_633_ != 0) {
		    Class651 class651
			= ((Class651)
			   (0 != i_632_
			    ? aClass44_Sub16_4996.method91(i_632_ - 1,
							   1290038165)
			    : null));
		    Class1 class1
			= (Class1) (0 != i_633_
				    ? aClass44_Sub12_4982.method91(i_633_ - 1,
								   -491265491)
				    : null);
		    if (-648787169 * anInt5052 == 0 && class651 == null)
			anInt5052 = -66499980;
		    aBool5049 = false;
		    aBool5060 = false;
		    boolean[] bools_634_ = bools;
		    boolean[] bools_635_ = bools;
		    boolean[] bools_636_ = bools;
		    bools[3] = false;
		    bools_636_[2] = false;
		    bools_635_[1] = false;
		    bools_634_[0] = false;
		    Class651 class651_637_ = class651;
		    if (class651 != null) {
			if (-1 == -2044484027 * class651.anInt8467
			    && 33386845 * class651.anInt8470 == -1) {
			    class651_637_ = class651;
			    class651 = null;
			} else if (null != class1
				   && anInt5052 * -648787169 != 0)
			    aBool5060 = class651.aBool8474;
		    }
		    anInt5053 = method7508(i_633_, i_628_, i_630_, i_629_,
					   i_631_, class151, is_626_,
					   -99810560) * 1735620089;
		    for (int i_638_ = 0; i_638_ < 13; i_638_++) {
			anIntArray5050[i_638_] = -1;
			anIntArray4993[i_638_] = 1;
		    }
		    method7503(class185, class651, class1, i_628_, i_630_,
			       is_624_, is_625_, is_627_, bools, (byte) 0);
		    aBool5061 = (!aBool5060 && !bools[0] && !bools[2]
				 && !bools[1] && !bools[3]);
		    method7460(class651, class1, 35040);
		    int i_639_
			= anInt5027 * -1457646951 + anInt5064 * -1278434163;
		    if (i_639_ <= 0)
			aClass556_4984.method9367(i, i_628_, i_630_,
						  -1168687060);
		    else {
			if (bools[0])
			    i_639_++;
			if (bools[2])
			    i_639_++;
			if (bools[1])
			    i_639_++;
			if (bools[3])
			    i_639_++;
			anInt5005 = 0;
			anInt5055 = 0;
			int i_640_ = 3 * i_639_;
			int[] is_641_ = aBool5062 ? new int[i_640_] : null;
			int[] is_642_ = new int[i_640_];
			int[] is_643_ = new int[i_640_];
			int[] is_644_ = new int[i_640_];
			int[] is_645_ = new int[i_640_];
			int[] is_646_ = new int[i_640_];
			int[] is_647_
			    = class151_623_ != null ? new int[i_640_] : null;
			int[] is_648_
			    = (null != class151_623_ || class151_622_ != null
			       ? new int[i_640_] : null);
			for (int i_649_ = 0; i_649_ < i_640_; i_649_++)
			    is_645_[i_649_] = -1;
			method7472(class185, i, i_628_, i_630_, class651,
				   bools, is_641_, is_642_, is_643_, is_644_,
				   is_645_, is_646_, is_647_, is_648_,
				   class151, class151_623_, class151_622_,
				   1290118874);
			int i_650_ = is_626_[i_628_][i_631_] & 0x7fff;
			int i_651_ = is_626_[i_629_][i_631_] & 0x7fff;
			int i_652_ = is_626_[i_629_][i_630_] & 0x7fff;
			method7462(class185, i, i_628_, i_630_, i_629_, i_631_,
				   class1, i_633_, i_650_, i_651_, i_652_,
				   bools, is_641_, is_642_, is_643_, is_644_,
				   is_645_, is_646_, is_647_, is_648_, is,
				   class151, class151_623_, class151_622_,
				   2129376787);
			method7463(class151, class1, class651_637_, i, i_628_,
				   i_630_, i_629_, i_631_, i_633_, i_632_,
				   (byte) 70);
			Class166 class166 = new Class166();
			if (aBool5002) {
			    class166.anInt1763
				= (aClass556_4984.method9305(i_628_, i_630_,
							     550020184)
				   * 929497037);
			    class166.anInt1762
				= (aClass556_4984.method9283(i_628_, i_630_,
							     (byte) -48)
				   * 1063224577);
			    class166.anInt1764
				= (aClass556_4984.method9331(i_628_, i_630_,
							     173856216)
				   * -1101271945);
			    class166.anInt1765
				= (aClass556_4984.method9238(i_628_, i_630_,
							     (byte) -43)
				   * -1961234339);
			    class166.anInt1766
				= (aClass556_4984.method9239(i_628_, i_630_,
							     383871388)
				   * -1338809385);
			    class166.anInt1767
				= (aClass556_4984.method9310(i_628_, i_630_,
							     (short) -23062)
				   * -2041662327);
			}
			class151.method2493(i_628_, i_630_, is_642_, is_647_,
					    is_643_, is_648_, is_644_, is_641_,
					    is_645_, is_646_, class166,
					    aBool5049);
			aClass556_4984.method9367(i, i_628_, i_630_,
						  -1552899806);
		    }
		}
	    }
	}
    }
    
    final void method7493(Class534_Sub40 class534_sub40, int i, int i_653_,
			  int i_654_, int i_655_, int i_656_, int i_657_,
			  int i_658_, int i_659_, boolean bool, int i_660_) {
	if (1 == i_659_)
	    i_656_ = 1;
	else if (2 == i_659_) {
	    i_655_ = 1;
	    i_656_ = 1;
	} else if (3 == i_659_)
	    i_655_ = 1;
	if (i_653_ >= 0 && i_653_ < -60640777 * anInt4992 && i_654_ >= 0
	    && i_654_ < -1584311401 * anInt5051) {
	    if (!aBool5002 && !bool)
		aClass468_4983.aByteArrayArrayArray5145[i][i_653_][i_654_]
		    = (byte) 0;
	    int i_661_ = class534_sub40.method16527(1875034632);
	    if (0 != (i_661_ & 0x1)) {
		if (bool) {
		    class534_sub40.method16527(-508806233);
		    class534_sub40.method16546(-1706829710);
		} else {
		    int i_662_ = class534_sub40.method16527(975181065);
		    aShortArrayArrayArray5001[i][i_653_][i_654_]
			= (short) class534_sub40.method16546(-1706829710);
		    aByteArrayArrayArray5058[i][i_653_][i_654_]
			= (byte) (i_662_ >> 2);
		    aByteArrayArrayArray4988[i][i_653_][i_654_]
			= (byte) (i_662_ + i_659_ & 0x3);
		}
	    }
	    if ((i_661_ & 0x2) != 0) {
		if (!aBool5002 && !bool)
		    aClass468_4983.aByteArrayArrayArray5145[i][i_653_][i_654_]
			= class534_sub40.method16586((byte) 1);
		else
		    class534_sub40.anInt10811 += -1387468933;
	    }
	    if ((i_661_ & 0x4) != 0) {
		if (bool)
		    class534_sub40.method16546(-1706829710);
		else
		    aShortArrayArrayArray5000[i][i_653_][i_654_]
			= (short) class534_sub40.method16546(-1706829710);
	    }
	    if ((i_661_ & 0x8) != 0) {
		int i_663_ = class534_sub40.method16527(1920996934);
		if (!aBool5002) {
		    if (i_663_ == 1)
			i_663_ = 0;
		    if (0 == i)
			anIntArrayArrayArray4995[0][i_655_ + i_653_]
			    [i_654_ + i_656_]
			    = 8 * -i_663_ << 2;
		    else
			anIntArrayArrayArray4995[i][i_653_ + i_655_]
			    [i_656_ + i_654_]
			    = (anIntArrayArrayArray4995[i - 1][i_655_ + i_653_]
			       [i_654_ + i_656_]) - (8 * i_663_ << 2);
		} else
		    anIntArrayArrayArray4995[0][i_655_ + i_653_][(i_656_
								  + i_654_)]
			= 8 * i_663_ << 2;
	    } else if (aBool5002)
		anIntArrayArrayArray4995[0][i_653_ + i_655_][i_656_ + i_654_]
		    = 0;
	    else if (0 == i)
		anIntArrayArrayArray4995[0][i_655_ + i_653_][i_656_ + i_654_]
		    = -Class224.method4161(i_657_ + 932731, i_658_ + 556238,
					   1674702540) * 8 << 2;
	    else
		anIntArrayArrayArray4995[i][i_655_ + i_653_][i_656_ + i_654_]
		    = (anIntArrayArrayArray4995[i - 1][i_655_ + i_653_]
		       [i_654_ + i_656_]) - 960;
	} else {
	    int i_664_ = class534_sub40.method16527(-1102650701);
	    if (0 != (i_664_ & 0x1)) {
		class534_sub40.method16527(-1422938777);
		class534_sub40.method16546(-1706829710);
	    }
	    if (0 != (i_664_ & 0x2))
		class534_sub40.anInt10811 += -1387468933;
	    if ((i_664_ & 0x4) != 0)
		class534_sub40.method16546(-1706829710);
	    if ((i_664_ & 0x8) != 0)
		class534_sub40.method16527(1169945500);
	}
    }
    
    void method7494(Class185 class185, int i, int i_665_, int i_666_,
		    int i_667_, int i_668_, Class1 class1, int i_669_,
		    int i_670_, int i_671_, int i_672_, boolean[] bools,
		    int[] is, int[] is_673_, int[] is_674_, int[] is_675_,
		    int[] is_676_, int[] is_677_, int[] is_678_, int[] is_679_,
		    int[][] is_680_, Class151 class151, Class151 class151_681_,
		    Class151 class151_682_) {
	if (null != class1) {
	    if (0 == i_670_)
		i_670_ = i_669_;
	    if (i_671_ == 0)
		i_671_ = i_669_;
	    if (i_672_ == 0)
		i_672_ = i_669_;
	    Class1 class1_683_
		= (Class1) aClass44_Sub12_4982.method91(i_669_ - 1, 887922381);
	    Class1 class1_684_
		= ((Class1)
		   aClass44_Sub12_4982.method91(i_670_ - 1, -2113533506));
	    Class1 class1_685_
		= (Class1) aClass44_Sub12_4982.method91(i_671_ - 1, 955475585);
	    Class1 class1_686_
		= ((Class1)
		   aClass44_Sub12_4982.method91(i_672_ - 1, -1118974793));
	    for (int i_687_ = 0; i_687_ < anInt5027 * -1457646951; i_687_++) {
		boolean bool = false;
		int i_688_;
		if (bools[0 - anInt5053 * -1693107127 & 0x3]
		    && anInt5005 * 1299058195 == anIntArray5068[0]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 1;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 1;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[1299058195 * anInt5005];
		    i_688_ = 6;
		} else if (bools[2 - -1693107127 * anInt5053 & 0x3]
			   && anInt5005 * 1299058195 == anIntArray5068[2]) {
		    anIntArray5044[0] = anIntArray5065[1299058195 * anInt5005];
		    anIntArray5044[1] = 5;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 5;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_688_ = 6;
		} else if (bools[1 - -1693107127 * anInt5053 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[1]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 3;
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    anIntArray5044[3] = 3;
		    anIntArray5044[4] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_688_ = 6;
		} else if (bools[3 - anInt5053 * -1693107127 & 0x3]
			   && 1299058195 * anInt5005 == anIntArray5068[3]) {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = 7;
		    anIntArray5044[2] = anIntArray5007[anInt5005 * 1299058195];
		    anIntArray5044[3] = 7;
		    anIntArray5044[4] = anIntArray5066[anInt5005 * 1299058195];
		    anIntArray5044[5] = anIntArray5007[anInt5005 * 1299058195];
		    i_688_ = 6;
		} else {
		    anIntArray5044[0] = anIntArray5065[anInt5005 * 1299058195];
		    anIntArray5044[1] = anIntArray5066[1299058195 * anInt5005];
		    anIntArray5044[2] = anIntArray5007[1299058195 * anInt5005];
		    i_688_ = 3;
		}
		for (int i_689_ = 0; i_689_ < i_688_; i_689_++) {
		    int i_690_ = anIntArray5044[i_689_];
		    int i_691_ = i_690_ - 908753042 * anInt5053 & 0x7;
		    int i_692_ = anIntArray5028[i_690_];
		    int i_693_ = anIntArray5029[i_690_];
		    int i_694_;
		    int i_695_;
		    if (-1693107127 * anInt5053 == 1) {
			i_694_ = i_693_;
			i_695_ = 512 - i_692_;
		    } else if (anInt5053 * -1693107127 == 2) {
			i_694_ = 512 - i_692_;
			i_695_ = 512 - i_693_;
		    } else if (3 == anInt5053 * -1693107127) {
			i_694_ = 512 - i_693_;
			i_695_ = i_692_;
		    } else {
			i_694_ = i_692_;
			i_695_ = i_693_;
		    }
		    is_673_[anInt5055 * 551530869] = i_694_;
		    is_674_[anInt5055 * 551530869] = i_695_;
		    if (null != is_678_
			&& (aBoolArrayArray5011[-648787169 * anInt5052]
			    [i_690_])) {
			int i_696_ = (i_665_ << 9) + i_694_;
			int i_697_ = (i_666_ << 9) + i_695_;
			is_678_[551530869 * anInt5055]
			    = (class151_681_.method2498(i_696_, i_697_,
							1816419687)
			       - class151.method2498(i_696_, i_697_,
						     -1650767280));
		    }
		    if (null != is_679_) {
			if (null != class151_681_
			    && !(aBoolArrayArray5011[anInt5052 * -648787169]
				 [i_690_])) {
			    int i_698_ = (i_665_ << 9) + i_694_;
			    int i_699_ = (i_666_ << 9) + i_695_;
			    is_679_[551530869 * anInt5055]
				= (class151.method2498(i_698_, i_699_,
						       136297839)
				   - class151_681_.method2498(i_698_, i_699_,
							      1722665368));
			} else if (null != class151_682_
				   && !(aBoolArrayArray5031
					[anInt5052 * -648787169][i_690_])) {
			    int i_700_ = i_694_ + (i_665_ << 9);
			    int i_701_ = i_695_ + (i_666_ << 9);
			    is_679_[551530869 * anInt5055]
				= (class151_682_.method2498(i_700_, i_701_,
							    1706966899)
				   - class151.method2498(i_700_, i_701_,
							 -351538582));
			}
		    }
		    if (i_690_ < 8 && anIntArray5050[i_691_] >= 0) {
			if (is != null)
			    is[anInt5055 * 551530869] = anIntArray5047[i_691_];
			is_677_[anInt5055 * 551530869]
			    = anIntArray5054[i_691_];
			is_676_[anInt5055 * 551530869]
			    = anIntArray4987[i_691_];
			is_675_[551530869 * anInt5055]
			    = anIntArray5033[i_691_];
		    } else {
			if (aBool5060 && (aBoolArrayArray5011
					  [-648787169 * anInt5052][i_690_])) {
			    is_676_[551530869 * anInt5055]
				= anInt5056 * -717903095;
			    is_677_[anInt5055 * 551530869]
				= anInt5057 * 782136929;
			    is_675_[551530869 * anInt5055]
				= 124430199 * anInt5046;
			} else if (i_694_ == 0 && i_695_ == 0) {
			    is_675_[551530869 * anInt5055]
				= is_680_[i_665_][i_666_];
			    is_676_[551530869 * anInt5055]
				= 587899173 * class1_683_.anInt10;
			    is_677_[anInt5055 * 551530869]
				= 161079521 * class1_683_.anInt15;
			} else if (i_694_ == 0 && 512 == i_695_) {
			    is_675_[anInt5055 * 551530869]
				= is_680_[i_665_][i_668_];
			    is_676_[551530869 * anInt5055]
				= 587899173 * class1_684_.anInt10;
			    is_677_[anInt5055 * 551530869]
				= class1_684_.anInt15 * 161079521;
			} else if (512 == i_694_ && 512 == i_695_) {
			    is_675_[551530869 * anInt5055]
				= is_680_[i_667_][i_668_];
			    is_676_[anInt5055 * 551530869]
				= class1_685_.anInt10 * 587899173;
			    is_677_[551530869 * anInt5055]
				= class1_685_.anInt15 * 161079521;
			} else if (512 == i_694_ && 0 == i_695_) {
			    is_675_[anInt5055 * 551530869]
				= is_680_[i_667_][i_666_];
			    is_676_[551530869 * anInt5055]
				= 587899173 * class1_686_.anInt10;
			    is_677_[551530869 * anInt5055]
				= 161079521 * class1_686_.anInt15;
			} else {
			    if (i_694_ < 256) {
				if (i_695_ < 256) {
				    is_676_[anInt5055 * 551530869]
					= class1_683_.anInt10 * 587899173;
				    is_677_[551530869 * anInt5055]
					= class1_683_.anInt15 * 161079521;
				} else {
				    is_676_[anInt5055 * 551530869]
					= 587899173 * class1_684_.anInt10;
				    is_677_[551530869 * anInt5055]
					= 161079521 * class1_684_.anInt15;
				}
			    } else if (i_695_ < 256) {
				is_676_[551530869 * anInt5055]
				    = class1_686_.anInt10 * 587899173;
				is_677_[anInt5055 * 551530869]
				    = class1_686_.anInt15 * 161079521;
			    } else {
				is_676_[anInt5055 * 551530869]
				    = class1_685_.anInt10 * 587899173;
				is_677_[551530869 * anInt5055]
				    = class1_685_.anInt15 * 161079521;
			    }
			    int i_702_
				= Class437.method6986(is_680_[i_665_][i_666_],
						      is_680_[i_667_][i_666_],
						      i_694_ << 7 >> 9,
						      -312753493);
			    int i_703_
				= Class437.method6986(is_680_[i_665_][i_668_],
						      is_680_[i_667_][i_668_],
						      i_694_ << 7 >> 9,
						      -911240850);
			    is_675_[551530869 * anInt5055]
				= Class437.method6986(i_702_, i_703_,
						      i_695_ << 7 >> 9,
						      868468105);
			}
			if (null != is)
			    is[551530869 * anInt5055]
				= is_675_[551530869 * anInt5055];
		    }
		    anInt5055 += -1596139811;
		}
		anInt5005 += -817239013;
	    }
	    if (-648787169 * anInt5052 != 0 && class1.aBool12)
		aBool5049 = true;
	}
    }
    
    void method7495(Class151 class151, Class1 class1, Class651 class651, int i,
		    int i_704_, int i_705_, int i_706_, int i_707_, int i_708_,
		    int i_709_) {
	int i_710_ = class151.method2491(i_704_, i_705_, -1405142225);
	int i_711_ = class151.method2491(i_706_, i_705_, 1639421010);
	int i_712_ = class151.method2491(i_706_, i_707_, 344486123);
	int i_713_ = class151.method2491(i_704_, i_707_, -462779833);
	boolean bool = aClass468_4983.method7612(i_704_, i_705_, (byte) 0);
	if (bool && i > 1 || !bool && i > 0) {
	    boolean bool_714_ = true;
	    if (null != class1 && !class1.aBool13)
		bool_714_ = false;
	    else if (0 == i_708_ && -648787169 * anInt5052 != 0)
		bool_714_ = false;
	    else if (i_709_ > 0 && null != class651 && !class651.aBool8469)
		bool_714_ = false;
	    if (bool_714_ && i_711_ == i_710_ && i_710_ == i_712_
		&& i_713_ == i_710_)
		aByteArrayArrayArray4989[i][i_704_][i_705_] |= 0x4;
	}
    }
    
    final void method7496(Class185 class185, Class651 class651, Class1 class1,
			  int i, int i_715_, int i_716_, int i_717_,
			  short[][] is, byte[][] is_718_, byte[][] is_719_,
			  boolean[] bools) {
	boolean[] bools_720_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[-648787169 * anInt5052]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	if (i_715_ > 0) {
	    if (i > 0) {
		int i_721_ = is[i - 1][i_715_ - 1] & 0x7fff;
		if (i_721_ > 0) {
		    Class651 class651_722_
			= (Class651) aClass44_Sub16_4996.method91(i_721_ - 1,
								  401530024);
		    if (-1 != class651_722_.anInt8467 * -2044484027
			&& class651_722_.aBool8474) {
			byte i_723_ = is_718_[i - 1][i_715_ - 1];
			int i_724_ = 2 * is_719_[i - 1][i_715_ - 1] + 4 & 0x7;
			int i_725_
			    = Class689.method14014(class185, class651_722_,
						   -2117979082);
			if (aBoolArrayArray5011[i_723_][i_724_]) {
			    anIntArray5033[0]
				= -2044484027 * class651_722_.anInt8467;
			    anIntArray5047[0] = i_725_;
			    anIntArray4987[0]
				= 1884378951 * class651_722_.anInt8468;
			    anIntArray5054[0]
				= class651_722_.anInt8471 * 431890869;
			    anIntArray5050[0]
				= class651_722_.anInt8473 * 589238839;
			    anIntArray4993[0] = 256;
			}
		    }
		}
	    }
	    if (i < i_716_ - 1) {
		int i_726_ = is[1 + i][i_715_ - 1] & 0x7fff;
		if (i_726_ > 0) {
		    Class651 class651_727_
			= (Class651) aClass44_Sub16_4996.method91(i_726_ - 1,
								  -1243469440);
		    if (-2044484027 * class651_727_.anInt8467 != -1
			&& class651_727_.aBool8474) {
			byte i_728_ = is_718_[1 + i][i_715_ - 1];
			int i_729_ = 2 * is_719_[i + 1][i_715_ - 1] + 6 & 0x7;
			int i_730_
			    = Class689.method14014(class185, class651_727_,
						   -2096229650);
			if (aBoolArrayArray5011[i_728_][i_729_]) {
			    anIntArray5033[2]
				= class651_727_.anInt8467 * -2044484027;
			    anIntArray5047[2] = i_730_;
			    anIntArray4987[2]
				= class651_727_.anInt8468 * 1884378951;
			    anIntArray5054[2]
				= 431890869 * class651_727_.anInt8471;
			    anIntArray5050[2]
				= 589238839 * class651_727_.anInt8473;
			    anIntArray4993[2] = 512;
			}
		    }
		}
	    }
	}
	if (i_715_ < i_717_ - 1) {
	    if (i > 0) {
		int i_731_ = is[i - 1][1 + i_715_] & 0x7fff;
		if (i_731_ > 0) {
		    Class651 class651_732_
			= (Class651) aClass44_Sub16_4996.method91(i_731_ - 1,
								  495203343);
		    if (-1 != class651_732_.anInt8467 * -2044484027
			&& class651_732_.aBool8474) {
			byte i_733_ = is_718_[i - 1][1 + i_715_];
			int i_734_ = 2 + is_719_[i - 1][i_715_ + 1] * 2 & 0x7;
			int i_735_
			    = Class689.method14014(class185, class651_732_,
						   -2055715018);
			if (aBoolArrayArray5011[i_733_][i_734_]) {
			    anIntArray5033[6]
				= class651_732_.anInt8467 * -2044484027;
			    anIntArray5047[6] = i_735_;
			    anIntArray4987[6]
				= 1884378951 * class651_732_.anInt8468;
			    anIntArray5054[6]
				= class651_732_.anInt8471 * 431890869;
			    anIntArray5050[6]
				= class651_732_.anInt8473 * 589238839;
			    anIntArray4993[6] = 64;
			}
		    }
		}
	    }
	    if (i < i_716_ - 1) {
		int i_736_ = is[1 + i][1 + i_715_] & 0x7fff;
		if (i_736_ > 0) {
		    Class651 class651_737_
			= (Class651) aClass44_Sub16_4996.method91(i_736_ - 1,
								  -714702499);
		    if (-1 != class651_737_.anInt8467 * -2044484027
			&& class651_737_.aBool8474) {
			byte i_738_ = is_718_[i + 1][i_715_ + 1];
			int i_739_ = 2 * is_719_[1 + i][1 + i_715_] + 0 & 0x7;
			int i_740_
			    = Class689.method14014(class185, class651_737_,
						   -1977663179);
			if (aBoolArrayArray5011[i_738_][i_739_]) {
			    anIntArray5033[4]
				= -2044484027 * class651_737_.anInt8467;
			    anIntArray5047[4] = i_740_;
			    anIntArray4987[4]
				= class651_737_.anInt8468 * 1884378951;
			    anIntArray5054[4]
				= 431890869 * class651_737_.anInt8471;
			    anIntArray5050[4]
				= 589238839 * class651_737_.anInt8473;
			    anIntArray4993[4] = 128;
			}
		    }
		}
	    }
	}
	if (i_715_ > 0) {
	    int i_741_ = is[i][i_715_ - 1] & 0x7fff;
	    if (i_741_ > 0) {
		Class651 class651_742_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_741_ - 1, -1835347023));
		if (-1 != class651_742_.anInt8467 * -2044484027) {
		    byte i_743_ = is_718_[i][i_715_ - 1];
		    int i_744_ = is_719_[i][i_715_ - 1];
		    if (class651_742_.aBool8474) {
			int i_745_ = 2;
			int i_746_ = 2 * i_744_ + 4;
			int i_747_
			    = Class689.method14014(class185, class651_742_,
						   -2085409944);
			for (int i_748_ = 0; i_748_ < 3; i_748_++) {
			    i_746_ &= 0x7;
			    i_745_ &= 0x7;
			    if (aBoolArrayArray5011[i_743_][i_746_]
				&& (anIntArray5050[i_745_]
				    <= class651_742_.anInt8473 * 589238839)) {
				anIntArray5033[i_745_]
				    = -2044484027 * class651_742_.anInt8467;
				anIntArray5047[i_745_] = i_747_;
				anIntArray4987[i_745_]
				    = 1884378951 * class651_742_.anInt8468;
				anIntArray5054[i_745_]
				    = 431890869 * class651_742_.anInt8471;
				if (anIntArray5050[i_745_]
				    == 589238839 * class651_742_.anInt8473)
				    anIntArray4993[i_745_] |= 0x20;
				else
				    anIntArray4993[i_745_] = 32;
				anIntArray5050[i_745_]
				    = class651_742_.anInt8473 * 589238839;
			    }
			    i_746_++;
			    i_745_--;
			}
			if (!bools_720_[-1693107127 * anInt5053 + 0 & 0x3])
			    bools[0] = (aBoolArrayArray5040[i_743_]
					[i_744_ + 2 & 0x3]);
		    } else if (!bools_720_[0 + -1693107127 * anInt5053 & 0x3])
			bools[0]
			    = aBoolArrayArray5021[i_743_][i_744_ + 2 & 0x3];
		}
	    }
	}
	if (i_715_ < i_717_ - 1) {
	    int i_749_ = is[i][i_715_ + 1] & 0x7fff;
	    if (i_749_ > 0) {
		Class651 class651_750_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_749_ - 1, 771856474));
		if (class651_750_.anInt8467 * -2044484027 != -1) {
		    byte i_751_ = is_718_[i][i_715_ + 1];
		    int i_752_ = is_719_[i][1 + i_715_];
		    if (class651_750_.aBool8474) {
			int i_753_ = 4;
			int i_754_ = i_752_ * 2 + 2;
			int i_755_
			    = Class689.method14014(class185, class651_750_,
						   -2122870404);
			for (int i_756_ = 0; i_756_ < 3; i_756_++) {
			    i_754_ &= 0x7;
			    i_753_ &= 0x7;
			    if (aBoolArrayArray5011[i_751_][i_754_]
				&& (anIntArray5050[i_753_]
				    <= class651_750_.anInt8473 * 589238839)) {
				anIntArray5033[i_753_]
				    = -2044484027 * class651_750_.anInt8467;
				anIntArray5047[i_753_] = i_755_;
				anIntArray4987[i_753_]
				    = 1884378951 * class651_750_.anInt8468;
				anIntArray5054[i_753_]
				    = 431890869 * class651_750_.anInt8471;
				if (class651_750_.anInt8473 * 589238839
				    == anIntArray5050[i_753_])
				    anIntArray4993[i_753_] |= 0x10;
				else
				    anIntArray4993[i_753_] = 16;
				anIntArray5050[i_753_]
				    = class651_750_.anInt8473 * 589238839;
			    }
			    i_754_--;
			    i_753_++;
			}
			if (!bools_720_[anInt5053 * -1693107127 + 2 & 0x3])
			    bools[2] = (aBoolArrayArray5040[i_751_]
					[i_752_ + 0 & 0x3]);
		    } else if (!bools_720_[anInt5053 * -1693107127 + 2 & 0x3])
			bools[2]
			    = aBoolArrayArray5021[i_751_][i_752_ + 0 & 0x3];
		}
	    }
	}
	if (i > 0) {
	    int i_757_ = is[i - 1][i_715_] & 0x7fff;
	    if (i_757_ > 0) {
		Class651 class651_758_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_757_ - 1, -2053617565));
		if (-2044484027 * class651_758_.anInt8467 != -1) {
		    byte i_759_ = is_718_[i - 1][i_715_];
		    int i_760_ = is_719_[i - 1][i_715_];
		    if (class651_758_.aBool8474) {
			int i_761_ = 6;
			int i_762_ = i_760_ * 2 + 4;
			int i_763_
			    = Class689.method14014(class185, class651_758_,
						   -2009160655);
			for (int i_764_ = 0; i_764_ < 3; i_764_++) {
			    i_762_ &= 0x7;
			    i_761_ &= 0x7;
			    if (aBoolArrayArray5011[i_759_][i_762_]
				&& (anIntArray5050[i_761_]
				    <= 589238839 * class651_758_.anInt8473)) {
				anIntArray5033[i_761_]
				    = class651_758_.anInt8467 * -2044484027;
				anIntArray5047[i_761_] = i_763_;
				anIntArray4987[i_761_]
				    = class651_758_.anInt8468 * 1884378951;
				anIntArray5054[i_761_]
				    = 431890869 * class651_758_.anInt8471;
				if (589238839 * class651_758_.anInt8473
				    == anIntArray5050[i_761_])
				    anIntArray4993[i_761_] |= 0x8;
				else
				    anIntArray4993[i_761_] = 8;
				anIntArray5050[i_761_]
				    = 589238839 * class651_758_.anInt8473;
			    }
			    i_762_--;
			    i_761_++;
			}
			if (!bools_720_[anInt5053 * -1693107127 + 3 & 0x3])
			    bools[3] = (aBoolArrayArray5040[i_759_]
					[1 + i_760_ & 0x3]);
		    } else if (!bools_720_[-1693107127 * anInt5053 + 3 & 0x3])
			bools[3]
			    = aBoolArrayArray5021[i_759_][i_760_ + 1 & 0x3];
		}
	    }
	}
	if (i < i_716_ - 1) {
	    int i_765_ = is[1 + i][i_715_] & 0x7fff;
	    if (i_765_ > 0) {
		Class651 class651_766_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_765_ - 1, 202052436));
		if (class651_766_.anInt8467 * -2044484027 != -1) {
		    byte i_767_ = is_718_[1 + i][i_715_];
		    int i_768_ = is_719_[i + 1][i_715_];
		    if (class651_766_.aBool8474) {
			int i_769_ = 4;
			int i_770_ = 6 + 2 * i_768_;
			int i_771_
			    = Class689.method14014(class185, class651_766_,
						   -2112263389);
			for (int i_772_ = 0; i_772_ < 3; i_772_++) {
			    i_770_ &= 0x7;
			    i_769_ &= 0x7;
			    if (aBoolArrayArray5011[i_767_][i_770_]
				&& (anIntArray5050[i_769_]
				    <= 589238839 * class651_766_.anInt8473)) {
				anIntArray5033[i_769_]
				    = class651_766_.anInt8467 * -2044484027;
				anIntArray5047[i_769_] = i_771_;
				anIntArray4987[i_769_]
				    = class651_766_.anInt8468 * 1884378951;
				anIntArray5054[i_769_]
				    = 431890869 * class651_766_.anInt8471;
				if (anIntArray5050[i_769_]
				    == 589238839 * class651_766_.anInt8473)
				    anIntArray4993[i_769_] |= 0x4;
				else
				    anIntArray4993[i_769_] = 4;
				anIntArray5050[i_769_]
				    = 589238839 * class651_766_.anInt8473;
			    }
			    i_770_++;
			    i_769_--;
			}
			if (!bools_720_[-1693107127 * anInt5053 + 1 & 0x3])
			    bools[1] = (aBoolArrayArray5040[i_767_]
					[i_768_ + 3 & 0x3]);
		    } else if (!bools_720_[1 + -1693107127 * anInt5053 & 0x3])
			bools[1]
			    = aBoolArrayArray5021[i_767_][3 + i_768_ & 0x3];
		}
	    }
	}
	if (class651 != null && class651.aBool8474) {
	    int i_773_ = Class689.method14014(class185, class651, -2135841290);
	    for (int i_774_ = 0; i_774_ < 8; i_774_++) {
		int i_775_ = i_774_ - anInt5053 * 908753042 & 0x7;
		if (aBoolArrayArray5011[anInt5052 * -648787169][i_774_]
		    && (anIntArray5050[i_775_]
			<= 589238839 * class651.anInt8473)) {
		    anIntArray5033[i_775_] = class651.anInt8467 * -2044484027;
		    anIntArray5047[i_775_] = i_773_;
		    anIntArray4987[i_775_] = class651.anInt8468 * 1884378951;
		    anIntArray5054[i_775_] = class651.anInt8471 * 431890869;
		    if (anIntArray5050[i_775_]
			== class651.anInt8473 * 589238839)
			anIntArray4993[i_775_] |= 0x2;
		    else
			anIntArray4993[i_775_] = 2;
		    anIntArray5050[i_775_] = 589238839 * class651.anInt8473;
		}
	    }
	}
    }
    
    final void method7497(Class185 class185, Class651 class651, Class1 class1,
			  int i, int i_776_, int i_777_, int i_778_,
			  short[][] is, byte[][] is_779_, byte[][] is_780_,
			  boolean[] bools) {
	boolean[] bools_781_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[-648787169 * anInt5052]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	if (i_776_ > 0) {
	    if (i > 0) {
		int i_782_ = is[i - 1][i_776_ - 1] & 0x7fff;
		if (i_782_ > 0) {
		    Class651 class651_783_
			= (Class651) aClass44_Sub16_4996.method91(i_782_ - 1,
								  874039474);
		    if (-1 != class651_783_.anInt8467 * -2044484027
			&& class651_783_.aBool8474) {
			byte i_784_ = is_779_[i - 1][i_776_ - 1];
			int i_785_ = 2 * is_780_[i - 1][i_776_ - 1] + 4 & 0x7;
			int i_786_
			    = Class689.method14014(class185, class651_783_,
						   -2055921869);
			if (aBoolArrayArray5011[i_784_][i_785_]) {
			    anIntArray5033[0]
				= -2044484027 * class651_783_.anInt8467;
			    anIntArray5047[0] = i_786_;
			    anIntArray4987[0]
				= 1884378951 * class651_783_.anInt8468;
			    anIntArray5054[0]
				= class651_783_.anInt8471 * 431890869;
			    anIntArray5050[0]
				= class651_783_.anInt8473 * 589238839;
			    anIntArray4993[0] = 256;
			}
		    }
		}
	    }
	    if (i < i_777_ - 1) {
		int i_787_ = is[1 + i][i_776_ - 1] & 0x7fff;
		if (i_787_ > 0) {
		    Class651 class651_788_
			= (Class651) aClass44_Sub16_4996.method91(i_787_ - 1,
								  892364856);
		    if (-2044484027 * class651_788_.anInt8467 != -1
			&& class651_788_.aBool8474) {
			byte i_789_ = is_779_[1 + i][i_776_ - 1];
			int i_790_ = 2 * is_780_[i + 1][i_776_ - 1] + 6 & 0x7;
			int i_791_
			    = Class689.method14014(class185, class651_788_,
						   -2074482507);
			if (aBoolArrayArray5011[i_789_][i_790_]) {
			    anIntArray5033[2]
				= class651_788_.anInt8467 * -2044484027;
			    anIntArray5047[2] = i_791_;
			    anIntArray4987[2]
				= class651_788_.anInt8468 * 1884378951;
			    anIntArray5054[2]
				= 431890869 * class651_788_.anInt8471;
			    anIntArray5050[2]
				= 589238839 * class651_788_.anInt8473;
			    anIntArray4993[2] = 512;
			}
		    }
		}
	    }
	}
	if (i_776_ < i_778_ - 1) {
	    if (i > 0) {
		int i_792_ = is[i - 1][1 + i_776_] & 0x7fff;
		if (i_792_ > 0) {
		    Class651 class651_793_
			= (Class651) aClass44_Sub16_4996.method91(i_792_ - 1,
								  663303164);
		    if (-1 != class651_793_.anInt8467 * -2044484027
			&& class651_793_.aBool8474) {
			byte i_794_ = is_779_[i - 1][1 + i_776_];
			int i_795_ = 2 + is_780_[i - 1][i_776_ + 1] * 2 & 0x7;
			int i_796_
			    = Class689.method14014(class185, class651_793_,
						   -1978128859);
			if (aBoolArrayArray5011[i_794_][i_795_]) {
			    anIntArray5033[6]
				= class651_793_.anInt8467 * -2044484027;
			    anIntArray5047[6] = i_796_;
			    anIntArray4987[6]
				= 1884378951 * class651_793_.anInt8468;
			    anIntArray5054[6]
				= class651_793_.anInt8471 * 431890869;
			    anIntArray5050[6]
				= class651_793_.anInt8473 * 589238839;
			    anIntArray4993[6] = 64;
			}
		    }
		}
	    }
	    if (i < i_777_ - 1) {
		int i_797_ = is[1 + i][1 + i_776_] & 0x7fff;
		if (i_797_ > 0) {
		    Class651 class651_798_
			= (Class651) aClass44_Sub16_4996.method91(i_797_ - 1,
								  -1295150094);
		    if (-1 != class651_798_.anInt8467 * -2044484027
			&& class651_798_.aBool8474) {
			byte i_799_ = is_779_[i + 1][i_776_ + 1];
			int i_800_ = 2 * is_780_[1 + i][1 + i_776_] + 0 & 0x7;
			int i_801_
			    = Class689.method14014(class185, class651_798_,
						   -1933289442);
			if (aBoolArrayArray5011[i_799_][i_800_]) {
			    anIntArray5033[4]
				= -2044484027 * class651_798_.anInt8467;
			    anIntArray5047[4] = i_801_;
			    anIntArray4987[4]
				= class651_798_.anInt8468 * 1884378951;
			    anIntArray5054[4]
				= 431890869 * class651_798_.anInt8471;
			    anIntArray5050[4]
				= 589238839 * class651_798_.anInt8473;
			    anIntArray4993[4] = 128;
			}
		    }
		}
	    }
	}
	if (i_776_ > 0) {
	    int i_802_ = is[i][i_776_ - 1] & 0x7fff;
	    if (i_802_ > 0) {
		Class651 class651_803_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_802_ - 1, -656929264));
		if (-1 != class651_803_.anInt8467 * -2044484027) {
		    byte i_804_ = is_779_[i][i_776_ - 1];
		    int i_805_ = is_780_[i][i_776_ - 1];
		    if (class651_803_.aBool8474) {
			int i_806_ = 2;
			int i_807_ = 2 * i_805_ + 4;
			int i_808_
			    = Class689.method14014(class185, class651_803_,
						   -1895579610);
			for (int i_809_ = 0; i_809_ < 3; i_809_++) {
			    i_807_ &= 0x7;
			    i_806_ &= 0x7;
			    if (aBoolArrayArray5011[i_804_][i_807_]
				&& (anIntArray5050[i_806_]
				    <= class651_803_.anInt8473 * 589238839)) {
				anIntArray5033[i_806_]
				    = -2044484027 * class651_803_.anInt8467;
				anIntArray5047[i_806_] = i_808_;
				anIntArray4987[i_806_]
				    = 1884378951 * class651_803_.anInt8468;
				anIntArray5054[i_806_]
				    = 431890869 * class651_803_.anInt8471;
				if (anIntArray5050[i_806_]
				    == 589238839 * class651_803_.anInt8473)
				    anIntArray4993[i_806_] |= 0x20;
				else
				    anIntArray4993[i_806_] = 32;
				anIntArray5050[i_806_]
				    = class651_803_.anInt8473 * 589238839;
			    }
			    i_807_++;
			    i_806_--;
			}
			if (!bools_781_[-1693107127 * anInt5053 + 0 & 0x3])
			    bools[0] = (aBoolArrayArray5040[i_804_]
					[i_805_ + 2 & 0x3]);
		    } else if (!bools_781_[0 + -1693107127 * anInt5053 & 0x3])
			bools[0]
			    = aBoolArrayArray5021[i_804_][i_805_ + 2 & 0x3];
		}
	    }
	}
	if (i_776_ < i_778_ - 1) {
	    int i_810_ = is[i][i_776_ + 1] & 0x7fff;
	    if (i_810_ > 0) {
		Class651 class651_811_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_810_ - 1, 281621737));
		if (class651_811_.anInt8467 * -2044484027 != -1) {
		    byte i_812_ = is_779_[i][i_776_ + 1];
		    int i_813_ = is_780_[i][1 + i_776_];
		    if (class651_811_.aBool8474) {
			int i_814_ = 4;
			int i_815_ = i_813_ * 2 + 2;
			int i_816_
			    = Class689.method14014(class185, class651_811_,
						   -1961322649);
			for (int i_817_ = 0; i_817_ < 3; i_817_++) {
			    i_815_ &= 0x7;
			    i_814_ &= 0x7;
			    if (aBoolArrayArray5011[i_812_][i_815_]
				&& (anIntArray5050[i_814_]
				    <= class651_811_.anInt8473 * 589238839)) {
				anIntArray5033[i_814_]
				    = -2044484027 * class651_811_.anInt8467;
				anIntArray5047[i_814_] = i_816_;
				anIntArray4987[i_814_]
				    = 1884378951 * class651_811_.anInt8468;
				anIntArray5054[i_814_]
				    = 431890869 * class651_811_.anInt8471;
				if (class651_811_.anInt8473 * 589238839
				    == anIntArray5050[i_814_])
				    anIntArray4993[i_814_] |= 0x10;
				else
				    anIntArray4993[i_814_] = 16;
				anIntArray5050[i_814_]
				    = class651_811_.anInt8473 * 589238839;
			    }
			    i_815_--;
			    i_814_++;
			}
			if (!bools_781_[anInt5053 * -1693107127 + 2 & 0x3])
			    bools[2] = (aBoolArrayArray5040[i_812_]
					[i_813_ + 0 & 0x3]);
		    } else if (!bools_781_[anInt5053 * -1693107127 + 2 & 0x3])
			bools[2]
			    = aBoolArrayArray5021[i_812_][i_813_ + 0 & 0x3];
		}
	    }
	}
	if (i > 0) {
	    int i_818_ = is[i - 1][i_776_] & 0x7fff;
	    if (i_818_ > 0) {
		Class651 class651_819_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_818_ - 1, -1695537619));
		if (-2044484027 * class651_819_.anInt8467 != -1) {
		    byte i_820_ = is_779_[i - 1][i_776_];
		    int i_821_ = is_780_[i - 1][i_776_];
		    if (class651_819_.aBool8474) {
			int i_822_ = 6;
			int i_823_ = i_821_ * 2 + 4;
			int i_824_
			    = Class689.method14014(class185, class651_819_,
						   -2040398938);
			for (int i_825_ = 0; i_825_ < 3; i_825_++) {
			    i_823_ &= 0x7;
			    i_822_ &= 0x7;
			    if (aBoolArrayArray5011[i_820_][i_823_]
				&& (anIntArray5050[i_822_]
				    <= 589238839 * class651_819_.anInt8473)) {
				anIntArray5033[i_822_]
				    = class651_819_.anInt8467 * -2044484027;
				anIntArray5047[i_822_] = i_824_;
				anIntArray4987[i_822_]
				    = class651_819_.anInt8468 * 1884378951;
				anIntArray5054[i_822_]
				    = 431890869 * class651_819_.anInt8471;
				if (589238839 * class651_819_.anInt8473
				    == anIntArray5050[i_822_])
				    anIntArray4993[i_822_] |= 0x8;
				else
				    anIntArray4993[i_822_] = 8;
				anIntArray5050[i_822_]
				    = 589238839 * class651_819_.anInt8473;
			    }
			    i_823_--;
			    i_822_++;
			}
			if (!bools_781_[anInt5053 * -1693107127 + 3 & 0x3])
			    bools[3] = (aBoolArrayArray5040[i_820_]
					[1 + i_821_ & 0x3]);
		    } else if (!bools_781_[-1693107127 * anInt5053 + 3 & 0x3])
			bools[3]
			    = aBoolArrayArray5021[i_820_][i_821_ + 1 & 0x3];
		}
	    }
	}
	if (i < i_777_ - 1) {
	    int i_826_ = is[1 + i][i_776_] & 0x7fff;
	    if (i_826_ > 0) {
		Class651 class651_827_
		    = ((Class651)
		       aClass44_Sub16_4996.method91(i_826_ - 1, -1908752846));
		if (class651_827_.anInt8467 * -2044484027 != -1) {
		    byte i_828_ = is_779_[1 + i][i_776_];
		    int i_829_ = is_780_[i + 1][i_776_];
		    if (class651_827_.aBool8474) {
			int i_830_ = 4;
			int i_831_ = 6 + 2 * i_829_;
			int i_832_
			    = Class689.method14014(class185, class651_827_,
						   -2065963848);
			for (int i_833_ = 0; i_833_ < 3; i_833_++) {
			    i_831_ &= 0x7;
			    i_830_ &= 0x7;
			    if (aBoolArrayArray5011[i_828_][i_831_]
				&& (anIntArray5050[i_830_]
				    <= 589238839 * class651_827_.anInt8473)) {
				anIntArray5033[i_830_]
				    = class651_827_.anInt8467 * -2044484027;
				anIntArray5047[i_830_] = i_832_;
				anIntArray4987[i_830_]
				    = class651_827_.anInt8468 * 1884378951;
				anIntArray5054[i_830_]
				    = 431890869 * class651_827_.anInt8471;
				if (anIntArray5050[i_830_]
				    == 589238839 * class651_827_.anInt8473)
				    anIntArray4993[i_830_] |= 0x4;
				else
				    anIntArray4993[i_830_] = 4;
				anIntArray5050[i_830_]
				    = 589238839 * class651_827_.anInt8473;
			    }
			    i_831_++;
			    i_830_--;
			}
			if (!bools_781_[-1693107127 * anInt5053 + 1 & 0x3])
			    bools[1] = (aBoolArrayArray5040[i_828_]
					[i_829_ + 3 & 0x3]);
		    } else if (!bools_781_[1 + -1693107127 * anInt5053 & 0x3])
			bools[1]
			    = aBoolArrayArray5021[i_828_][3 + i_829_ & 0x3];
		}
	    }
	}
	if (class651 != null && class651.aBool8474) {
	    int i_834_ = Class689.method14014(class185, class651, -1982321539);
	    for (int i_835_ = 0; i_835_ < 8; i_835_++) {
		int i_836_ = i_835_ - anInt5053 * 908753042 & 0x7;
		if (aBoolArrayArray5011[anInt5052 * -648787169][i_835_]
		    && (anIntArray5050[i_836_]
			<= 589238839 * class651.anInt8473)) {
		    anIntArray5033[i_836_] = class651.anInt8467 * -2044484027;
		    anIntArray5047[i_836_] = i_834_;
		    anIntArray4987[i_836_] = class651.anInt8468 * 1884378951;
		    anIntArray5054[i_836_] = class651.anInt8471 * 431890869;
		    if (anIntArray5050[i_836_]
			== class651.anInt8473 * 589238839)
			anIntArray4993[i_836_] |= 0x2;
		    else
			anIntArray4993[i_836_] = 2;
		    anIntArray5050[i_836_] = 589238839 * class651.anInt8473;
		}
	    }
	}
    }
    
    static final int method7498(int i, int i_837_, int i_838_) {
	if (i == i_837_)
	    return i;
	int i_839_ = 128 - i_838_;
	int i_840_ = i_839_ * (i & 0x7f) + (i_837_ & 0x7f) * i_838_ >> 7;
	int i_841_ = (i & 0x380) * i_839_ + (i_837_ & 0x380) * i_838_ >> 7;
	int i_842_ = i_839_ * (i & 0xfc00) + (i_837_ & 0xfc00) * i_838_ >> 7;
	return i_842_ & 0xfc00 | i_841_ & 0x380 | i_840_ & 0x7f;
    }
    
    static final int method7499(int i, int i_843_, int i_844_) {
	if (i == i_843_)
	    return i;
	int i_845_ = 128 - i_844_;
	int i_846_ = i_845_ * (i & 0x7f) + (i_843_ & 0x7f) * i_844_ >> 7;
	int i_847_ = (i & 0x380) * i_845_ + (i_843_ & 0x380) * i_844_ >> 7;
	int i_848_ = i_845_ * (i & 0xfc00) + (i_843_ & 0xfc00) * i_844_ >> 7;
	return i_848_ & 0xfc00 | i_847_ & 0x380 | i_846_ & 0x7f;
    }
    
    static final int method7500(int i, int i_849_, int i_850_) {
	if (i == i_849_)
	    return i;
	int i_851_ = 128 - i_850_;
	int i_852_ = i_851_ * (i & 0x7f) + (i_849_ & 0x7f) * i_850_ >> 7;
	int i_853_ = (i & 0x380) * i_851_ + (i_849_ & 0x380) * i_850_ >> 7;
	int i_854_ = i_851_ * (i & 0xfc00) + (i_849_ & 0xfc00) * i_850_ >> 7;
	return i_854_ & 0xfc00 | i_853_ & 0x380 | i_852_ & 0x7f;
    }
    
    void method7501(Class185 class185, Class651 class651, Class1 class1, int i,
		    int i_855_, byte[][] is, byte[][] is_856_,
		    short[][] is_857_, boolean[] bools) {
	boolean[] bools_858_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[anInt5052 * -648787169]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	method7464(class185, class651, class1, i, i_855_,
		   -60640777 * anInt4992, anInt5051 * -1584311401, is_857_, is,
		   is_856_, bools, 227571664);
	aBool5062 = class651 != null && (class651.anInt8467 * -2044484027
					 != class651.anInt8470 * 33386845);
	if (!aBool5062) {
	    for (int i_859_ = 0; i_859_ < 8; i_859_++) {
		if (anIntArray5050[i_859_] >= 0
		    && anIntArray5047[i_859_] != anIntArray5033[i_859_]) {
		    aBool5062 = true;
		    break;
		}
	    }
	}
	if (!bools_858_[anInt5053 * -1693107127 + 1 & 0x3]) {
	    boolean[] bools_860_ = bools;
	    int i_861_ = 1;
	    bools_860_[i_861_]
		= bools_860_[i_861_] | 0 == (anIntArray4993[2]
					     & anIntArray4993[4]);
	}
	if (!bools_858_[anInt5053 * -1693107127 + 3 & 0x3]) {
	    boolean[] bools_862_ = bools;
	    int i_863_ = 3;
	    bools_862_[i_863_]
		= bools_862_[i_863_] | 0 == (anIntArray4993[6]
					     & anIntArray4993[0]);
	}
	if (!bools_858_[-1693107127 * anInt5053 + 0 & 0x3]) {
	    boolean[] bools_864_ = bools;
	    int i_865_ = 0;
	    bools_864_[i_865_]
		= bools_864_[i_865_] | 0 == (anIntArray4993[0]
					     & anIntArray4993[2]);
	}
	if (!bools_858_[-1693107127 * anInt5053 + 2 & 0x3]) {
	    boolean[] bools_866_ = bools;
	    int i_867_ = 2;
	    bools_866_[i_867_]
		= (bools_866_[i_867_]
		   | (anIntArray4993[4] & anIntArray4993[6]) == 0);
	}
	if (!aBool5060
	    && (-648787169 * anInt5052 == 0 || anInt5052 * -648787169 == 12)) {
	    if (bools[0] && !bools[1] && !bools[2] && bools[3]) {
		boolean[] bools_868_ = bools;
		bools[3] = false;
		bools_868_[0] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = 0;
	    } else if (bools[0] && bools[1] && !bools[2] && !bools[3]) {
		boolean[] bools_869_ = bools;
		bools[1] = false;
		bools_869_[0] = false;
		anInt5052
		    = (-648787169 * anInt5052 == 0 ? 13 : 14) * 1068200159;
		anInt5053 = 911892971;
	    } else if (!bools[0] && bools[1] && bools[2] && !bools[3]) {
		boolean[] bools_870_ = bools;
		bools[2] = false;
		bools_870_[1] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = -823727118;
	    } else if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
		boolean[] bools_871_ = bools;
		bools[3] = false;
		bools_871_[2] = false;
		anInt5052
		    = 1068200159 * (0 == -648787169 * anInt5052 ? 13 : 14);
		anInt5053 = 1735620089;
	    }
	}
    }
    
    public final void method7502(Class185 class185, Class151 class151,
				 Class151 class151_872_) {
	int[][] is = new int[-60640777 * anInt4992][-1584311401 * anInt5051];
	if (null == anIntArray5003
	    || -1584311401 * anInt5051 != anIntArray5003.length) {
	    anIntArray5003 = new int[anInt5051 * -1584311401];
	    anIntArray5004 = new int[anInt5051 * -1584311401];
	    anIntArray4999 = new int[-1584311401 * anInt5051];
	    anIntArray5035 = new int[anInt5051 * -1584311401];
	    anIntArray5063 = new int[anInt5051 * -1584311401];
	}
	for (int i = 0; i < anInt4991 * -692901467; i++) {
	    for (int i_873_ = 0; i_873_ < -1584311401 * anInt5051; i_873_++) {
		anIntArray5003[i_873_] = 0;
		anIntArray5004[i_873_] = 0;
		anIntArray4999[i_873_] = 0;
		anIntArray5035[i_873_] = 0;
		anIntArray5063[i_873_] = 0;
	    }
	    for (int i_874_ = -5; i_874_ < anInt4992 * -60640777; i_874_++) {
		for (int i_875_ = 0; i_875_ < -1584311401 * anInt5051;
		     i_875_++) {
		    int i_876_ = 5 + i_874_;
		    if (i_876_ < -60640777 * anInt4992) {
			int i_877_
			    = (aShortArrayArrayArray5000[i][i_876_][i_875_]
			       & 0x7fff);
			if (i_877_ > 0) {
			    Class1 class1
				= ((Class1)
				   aClass44_Sub12_4982.method91(i_877_ - 1,
								13693661));
			    anIntArray5003[i_875_]
				+= 1782063805 * class1.anInt11;
			    anIntArray5004[i_875_]
				+= class1.anInt14 * 1312863965;
			    anIntArray4999[i_875_]
				+= 1484599057 * class1.anInt16;
			    anIntArray5035[i_875_]
				+= class1.anInt17 * -1721639561;
			    anIntArray5063[i_875_]++;
			}
		    }
		    int i_878_ = i_874_ - 5;
		    if (i_878_ >= 0) {
			int i_879_
			    = (aShortArrayArrayArray5000[i][i_878_][i_875_]
			       & 0x7fff);
			if (i_879_ > 0) {
			    Class1 class1
				= ((Class1)
				   aClass44_Sub12_4982.method91(i_879_ - 1,
								36038055));
			    anIntArray5003[i_875_]
				-= 1782063805 * class1.anInt11;
			    anIntArray5004[i_875_]
				-= class1.anInt14 * 1312863965;
			    anIntArray4999[i_875_]
				-= 1484599057 * class1.anInt16;
			    anIntArray5035[i_875_]
				-= -1721639561 * class1.anInt17;
			    anIntArray5063[i_875_]--;
			}
		    }
		}
		if (i_874_ >= 0) {
		    int i_880_ = 0;
		    int i_881_ = 0;
		    int i_882_ = 0;
		    int i_883_ = 0;
		    int i_884_ = 0;
		    for (int i_885_ = -5; i_885_ < anInt5051 * -1584311401;
			 i_885_++) {
			int i_886_ = 5 + i_885_;
			if (i_886_ < -1584311401 * anInt5051) {
			    i_880_ += anIntArray5003[i_886_];
			    i_881_ += anIntArray5004[i_886_];
			    i_882_ += anIntArray4999[i_886_];
			    i_883_ += anIntArray5035[i_886_];
			    i_884_ += anIntArray5063[i_886_];
			}
			int i_887_ = i_885_ - 5;
			if (i_887_ >= 0) {
			    i_880_ -= anIntArray5003[i_887_];
			    i_881_ -= anIntArray5004[i_887_];
			    i_882_ -= anIntArray4999[i_887_];
			    i_883_ -= anIntArray5035[i_887_];
			    i_884_ -= anIntArray5063[i_887_];
			}
			if (i_885_ >= 0 && i_883_ > 0 && i_884_ > 0)
			    is[i_874_][i_885_]
				= Class200_Sub23.method15662((i_880_ * 256
							      / i_883_),
							     i_881_ / i_884_,
							     i_882_ / i_884_,
							     (byte) 87);
		    }
		}
	    }
	    if (aBool4981)
		method7461(class185, aClass556_4984.aClass151Array7432[i], i,
			   is, 0 == i ? class151 : null,
			   i == 0 ? class151_872_ : null, (byte) 2);
	    else
		method7456(class185, aClass556_4984.aClass151Array7432[i], i,
			   is, i == 0 ? class151 : null,
			   i == 0 ? class151_872_ : null, (byte) 8);
	    aShortArrayArrayArray5000[i] = null;
	    aShortArrayArrayArray5001[i] = null;
	    aByteArrayArrayArray5058[i] = null;
	    aByteArrayArrayArray4988[i] = null;
	}
	if (!aBool5002) {
	    if (anInt4986 * 1733778175 != 0)
		aClass556_4984.method9278(50228915);
	    if (aBool4998)
		aClass556_4984.method9280((byte) -79);
	}
	for (int i = 0; i < -692901467 * anInt4991; i++)
	    aClass556_4984.aClass151Array7432[i].method2495();
    }
    
    void method7503(Class185 class185, Class651 class651, Class1 class1, int i,
		    int i_888_, byte[][] is, byte[][] is_889_,
		    short[][] is_890_, boolean[] bools, byte i_891_) {
	boolean[] bools_892_ = (null != class651 && class651.aBool8474
				? aBoolArrayArray5040[anInt5052 * -648787169]
				: aBoolArrayArray5021[-648787169 * anInt5052]);
	method7464(class185, class651, class1, i, i_888_,
		   -60640777 * anInt4992, anInt5051 * -1584311401, is_890_, is,
		   is_889_, bools, -790916004);
	aBool5062 = class651 != null && (class651.anInt8467 * -2044484027
					 != class651.anInt8470 * 33386845);
	if (!aBool5062) {
	    for (int i_893_ = 0; i_893_ < 8; i_893_++) {
		if (anIntArray5050[i_893_] >= 0
		    && anIntArray5047[i_893_] != anIntArray5033[i_893_]) {
		    aBool5062 = true;
		    break;
		}
	    }
	}
	if (!bools_892_[anInt5053 * -1693107127 + 1 & 0x3]) {
	    boolean[] bools_894_ = bools;
	    int i_895_ = 1;
	    bools_894_[i_895_]
		= bools_894_[i_895_] | 0 == (anIntArray4993[2]
					     & anIntArray4993[4]);
	}
	if (!bools_892_[anInt5053 * -1693107127 + 3 & 0x3]) {
	    boolean[] bools_896_ = bools;
	    int i_897_ = 3;
	    bools_896_[i_897_]
		= bools_896_[i_897_] | 0 == (anIntArray4993[6]
					     & anIntArray4993[0]);
	}
	if (!bools_892_[-1693107127 * anInt5053 + 0 & 0x3]) {
	    boolean[] bools_898_ = bools;
	    int i_899_ = 0;
	    bools_898_[i_899_]
		= bools_898_[i_899_] | 0 == (anIntArray4993[0]
					     & anIntArray4993[2]);
	}
	if (!bools_892_[-1693107127 * anInt5053 + 2 & 0x3]) {
	    boolean[] bools_900_ = bools;
	    int i_901_ = 2;
	    bools_900_[i_901_]
		= (bools_900_[i_901_]
		   | (anIntArray4993[4] & anIntArray4993[6]) == 0);
	}
	if (!aBool5060
	    && (-648787169 * anInt5052 == 0 || anInt5052 * -648787169 == 12)) {
	    if (bools[0] && !bools[1] && !bools[2] && bools[3]) {
		boolean[] bools_902_ = bools;
		bools[3] = false;
		bools_902_[0] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = 0;
	    } else if (bools[0] && bools[1] && !bools[2] && !bools[3]) {
		boolean[] bools_903_ = bools;
		bools[1] = false;
		bools_903_[0] = false;
		anInt5052
		    = (-648787169 * anInt5052 == 0 ? 13 : 14) * 1068200159;
		anInt5053 = 911892971;
	    } else if (!bools[0] && bools[1] && bools[2] && !bools[3]) {
		boolean[] bools_904_ = bools;
		bools[2] = false;
		bools_904_[1] = false;
		anInt5052
		    = (0 == -648787169 * anInt5052 ? 13 : 14) * 1068200159;
		anInt5053 = -823727118;
	    } else if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
		boolean[] bools_905_ = bools;
		bools[3] = false;
		bools_905_[2] = false;
		anInt5052
		    = 1068200159 * (0 == -648787169 * anInt5052 ? 13 : 14);
		anInt5053 = 1735620089;
	    }
	}
    }
    
    static final int method7504(int i, int i_906_) {
	int i_907_
	    = (Class419.method6770(45365 + i, 91923 + i_906_, 4, 1748207138)
	       - 128
	       + (Class419.method6770(10294 + i, 37821 + i_906_, 2,
				      1748207138) - 128
		  >> 1)
	       + (Class419.method6770(i, i_906_, 1, 1748207138) - 128 >> 2));
	i_907_ = 35 + (int) (0.3 * (double) i_907_);
	if (i_907_ < 10)
	    i_907_ = 10;
	else if (i_907_ > 60)
	    i_907_ = 60;
	return i_907_;
    }
    
    static final int method7505(int i, int i_908_, int i_909_) {
	int i_910_ = i / i_909_;
	int i_911_ = i & i_909_ - 1;
	int i_912_ = i_908_ / i_909_;
	int i_913_ = i_908_ & i_909_ - 1;
	int i_914_ = Class110_Sub1.method14506(i_910_, i_912_, -1753716178);
	int i_915_ = Class110_Sub1.method14506(i_910_ + 1, i_912_, -978238638);
	int i_916_ = Class110_Sub1.method14506(i_910_, 1 + i_912_, -833606966);
	int i_917_
	    = Class110_Sub1.method14506(1 + i_910_, 1 + i_912_, -1515567252);
	int i_918_
	    = Class97.method1827(i_914_, i_915_, i_911_, i_909_, (byte) -20);
	int i_919_
	    = Class97.method1827(i_916_, i_917_, i_911_, i_909_, (byte) -36);
	return Class97.method1827(i_918_, i_919_, i_913_, i_909_, (byte) 40);
    }
    
    static final int method7506(int i, int i_920_, int i_921_, int i_922_) {
	int i_923_
	    = 65536 - Class427.anIntArray4805[8192 * i_921_ / i_922_] >> 1;
	return (i_920_ * i_923_ >> 16) + (i * (65536 - i_923_) >> 16);
    }
    
    static final int method7507(int i, int i_924_) {
	int i_925_ = i_924_ * 57 + i;
	i_925_ = i_925_ << 13 ^ i_925_;
	int i_926_ = (1376312589 + i_925_ * (i_925_ * i_925_ * 15731 + 789221)
		      & 0x7fffffff);
	return i_926_ >> 19 & 0xff;
    }
    
    int method7508(int i, int i_927_, int i_928_, int i_929_, int i_930_,
		   Class151 class151, short[][] is, int i_931_) {
	if ((0 == anInt5052 * -648787169 || 12 == anInt5052 * -648787169)
	    && i_927_ > 0 && i_928_ > 0 && i_927_ < -60640777 * anInt4992
	    && i_928_ < -1584311401 * anInt5051) {
	    int i_932_ = 0;
	    int i_933_ = 0;
	    int i_934_ = 0;
	    int i_935_ = 0;
	    i_932_ = i_932_ + (i == is[i_927_ - 1][i_928_ - 1] ? 1 : -1);
	    i_933_ = i_933_ + (i == is[i_929_][i_928_ - 1] ? 1 : -1);
	    i_934_ = i_934_ + (is[i_929_][i_930_] == i ? 1 : -1);
	    i_935_ = i_935_ + (is[i_927_ - 1][i_930_] == i ? 1 : -1);
	    if (is[i_927_][i_928_ - 1] == i) {
		i_932_++;
		i_933_++;
	    } else {
		i_932_--;
		i_933_--;
	    }
	    if (is[i_929_][i_928_] == i) {
		i_933_++;
		i_934_++;
	    } else {
		i_933_--;
		i_934_--;
	    }
	    if (i == is[i_927_][i_930_]) {
		i_934_++;
		i_935_++;
	    } else {
		i_934_--;
		i_935_--;
	    }
	    if (is[i_927_ - 1][i_928_] == i) {
		i_935_++;
		i_932_++;
	    } else {
		i_935_--;
		i_932_--;
	    }
	    int i_936_ = i_932_ - i_934_;
	    if (i_936_ < 0)
		i_936_ = -i_936_;
	    int i_937_ = i_933_ - i_935_;
	    if (i_937_ < 0)
		i_937_ = -i_937_;
	    if (i_937_ == i_936_) {
		i_936_ = (class151.method2491(i_927_, i_928_, -1129876217)
			  - class151.method2491(i_929_, i_930_, -377469397));
		if (i_936_ < 0)
		    i_936_ = -i_936_;
		i_937_ = (class151.method2491(i_929_, i_928_, -102962300)
			  - class151.method2491(i_927_, i_930_, 393897440));
		if (i_937_ < 0)
		    i_937_ = -i_937_;
	    }
	    return i_936_ < i_937_ ? 1 : 0;
	}
	return anInt5053 * -1693107127;
    }
    
    static final void method7509(Class247 class247, Class669 class669,
				 byte i) {
	class669.anInt8600 -= 308999563;
	int i_938_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307] - 1;
	if (i_938_ < 0 || i_938_ > 9)
	    throw new RuntimeException();
	Class63.method1281(class247, i_938_, class669, 1913020013);
    }
    
    static final void method7510(Class669 class669, byte i) {
	Class530.method8847(class669, 1236736994);
    }
}
