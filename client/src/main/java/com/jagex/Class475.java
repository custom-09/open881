/* Class475 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class475 implements Interface76
{
    static Class475 aClass475_5176;
    static Class475 aClass475_5177 = new Class475(0);
    static Class475 aClass475_5178 = new Class475(1);
    int anInt5179;
    
    public int method93() {
	return 931544555 * anInt5179;
    }
    
    public static Class475[] method7764() {
	return (new Class475[]
		{ aClass475_5176, aClass475_5177, aClass475_5178 });
    }
    
    static {
	aClass475_5176 = new Class475(2);
    }
    
    Class475(int i) {
	anInt5179 = i * -1324809533;
    }
    
    public int method53() {
	return 931544555 * anInt5179;
    }
    
    public int method22() {
	return 931544555 * anInt5179;
    }
    
    static final void method7765(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, 363933598);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >>> 16];
	Class247 class247_1_
	    = Class578.method9799(class243, class247, -1495101509);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class247_1_ == null ? -1 : -1278450723 * class247_1_.anInt2454;
    }
    
    public static Class17 method7766(Class15 class15,
				     Class534_Sub40 class534_sub40, int i) {
	return Class550.method9029(class15, class534_sub40, 2, -1640014010);
    }
    
    public static void method7767(int i, int i_2_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(23, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
}
