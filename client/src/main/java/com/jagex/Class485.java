/* Class485 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class485
{
    static Class485 aClass485_5281;
    public static Class485 aClass485_5282;
    int anInt5283;
    public static Class485 aClass485_5284;
    public static Class485 aClass485_5285;
    static Class485 aClass485_5286;
    public static Class485 aClass485_5287;
    static Class485 aClass485_5288;
    static Class485 aClass485_5289;
    static Class485 aClass485_5290 = new Class485(0);
    
    static {
	aClass485_5287 = new Class485(1);
	aClass485_5281 = new Class485(2);
	aClass485_5284 = new Class485(3);
	aClass485_5285 = new Class485(4);
	aClass485_5282 = new Class485(5);
	aClass485_5286 = new Class485(6);
	aClass485_5288 = new Class485(7);
	aClass485_5289 = new Class485(8);
    }
    
    Class485(int i) {
	anInt5283 = i * 1891043161;
    }
    
    static final void method7960(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	class247.aString2560
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
    }
    
    static final void method7961(Class669 class669, byte i) {
	Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
	    = ((Class654_Sub1_Sub5_Sub1_Sub1)
	       class669.aClass654_Sub1_Sub5_Sub1_8604);
	Class307 class307 = class654_sub1_sub5_sub1_sub1.aClass307_12204;
	if (null != class307.anIntArray3284)
	    class307 = class307.method5615(Class78.aClass103_825,
					   Class78.aClass103_825, -1466068515);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class307 != null ? 1 : 0;
    }
    
    static final void method7962(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class247.anInt2484 * 1943720095;
    }
    
    public static void method7963(Class536 class536, Class536 class536_0_,
				  byte i) {
	if (class536.aClass536_7163 != null)
	    class536.method8900(710418947);
	class536.aClass536_7163 = class536_0_;
	class536.aClass536_7164 = class536_0_.aClass536_7164;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
}
