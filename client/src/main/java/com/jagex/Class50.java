/* Class50 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class50
{
    public static Class50 aClass50_366;
    public static Class50 aClass50_367;
    public static Class50 aClass50_368;
    public static Class50 aClass50_369;
    public static Class50 aClass50_370;
    public static Class50 aClass50_371;
    static Class50 aClass50_372;
    static Class50 aClass50_373;
    public static Class50 aClass50_374;
    public static Class50 aClass50_375;
    public static Class50 aClass50_376;
    public static Class50 aClass50_377;
    public static Class50 aClass50_378
	= new Class50(0, "", false, false, false);
    static Class50 aClass50_379;
    public static Class50 aClass50_380;
    public static Class50 aClass50_381;
    public static Class50 aClass50_382;
    static Class50 aClass50_383;
    public static Class50 aClass50_384;
    public static Class50 aClass50_385;
    public static Class50 aClass50_386;
    public static Class50 aClass50_387;
    public static Class50 aClass50_388;
    public static Class50 aClass50_389;
    public static Class50 aClass50_390;
    public static Class50 aClass50_391;
    public static Class50 aClass50_392;
    public static Class50 aClass50_393;
    public static Class50 aClass50_394;
    public static Class50 aClass50_395;
    public static Class50 aClass50_396;
    public static Class50 aClass50_397;
    public static Class50 aClass50_398;
    static Class50 aClass50_399;
    public static Class50 aClass50_400;
    public static Class50 aClass50_401;
    static Class50 aClass50_402;
    public static Class50 aClass50_403;
    static Class50 aClass50_404;
    public static Class50 aClass50_405;
    public static Class50 aClass50_406;
    public static Class50 aClass50_407;
    public static Class50 aClass50_408;
    int anInt409;
    boolean aBool410;
    static int anInt411;
    
    public boolean method1154(int i) {
	return aBool410;
    }
    
    public int method1155(int i) {
	return -389811879 * anInt409;
    }
    
    Class50(int i, String string, boolean bool, boolean bool_0_,
	    boolean bool_1_) {
	anInt409 = i * 1453541097;
	aBool410 = bool_1_;
    }
    
    static {
	aClass50_367 = new Class50(1, "", false, false, false);
	aClass50_368 = new Class50(2, "", true, false, false);
	aClass50_369 = new Class50(3, "", true, false, false);
	aClass50_370 = new Class50(5, "", false, false, false);
	aClass50_371 = new Class50(7, "", false, false, false);
	aClass50_389 = new Class50(8, "", false, false, false);
	aClass50_403 = new Class50(10, "", false, false, false);
	aClass50_374 = new Class50(12, "", true, false, false);
	aClass50_376 = new Class50(13, "", false, false, false);
	aClass50_375 = new Class50(14, "", false, false, false);
	aClass50_377 = new Class50(16, "", true, false, false);
	aClass50_382 = new Class50(17, "", true, false, false);
	aClass50_366 = new Class50(18, "", true, false, false);
	aClass50_380 = new Class50(19, "", true, false, false);
	aClass50_381 = new Class50(20, "", true, false, false);
	aClass50_392 = new Class50(21, "", true, false, false);
	aClass50_390 = new Class50(22, "", true, false, false);
	aClass50_384 = new Class50(23, "", true, false, false);
	aClass50_385 = new Class50(24, "", true, false, false);
	aClass50_386 = new Class50(25, "", true, false, false);
	aClass50_387 = new Class50(26, "", false, false, false);
	aClass50_388 = new Class50(27, "", false, false, false);
	aClass50_405 = new Class50(28, "", false, false, false);
	aClass50_396 = new Class50(29, "", false, false, false);
	aClass50_391 = new Class50(30, "", false, false, false);
	aClass50_408 = new Class50(31, "", false, false, false);
	aClass50_393 = new Class50(32, "", false, false, false);
	aClass50_394 = new Class50(33, "", true, false, false);
	aClass50_395 = new Class50(34, "", false, false, false);
	aClass50_400 = new Class50(35, "", true, false, false);
	aClass50_397 = new Class50(40, "", false, false, true);
	aClass50_398 = new Class50(41, "", false, false, false);
	aClass50_399 = new Class50(42, "", false, false, false);
	aClass50_407 = new Class50(43, "", false, false, false);
	aClass50_401 = new Class50(44, "", false, false, false);
	aClass50_402 = new Class50(45, "", false, true, false);
	aClass50_379 = new Class50(46, "", false, true, false);
	aClass50_372 = new Class50(47, "", false, true, false);
	aClass50_404 = new Class50(48, "", false, true, false);
	aClass50_406 = new Class50(49, "", true, true, false);
	aClass50_383 = new Class50(50, "", false, true, false);
	aClass50_373 = new Class50(51, "", false, true, false);
	anInt411 = 60444507;
    }
    
    public static Class50[] method1156() {
	return (new Class50[]
		{ aClass50_378, aClass50_367, aClass50_368, aClass50_369,
		  aClass50_370, aClass50_371, aClass50_389, aClass50_403,
		  aClass50_374, aClass50_376, aClass50_375, aClass50_377,
		  aClass50_382, aClass50_366, aClass50_380, aClass50_381,
		  aClass50_392, aClass50_390, aClass50_384, aClass50_385,
		  aClass50_386, aClass50_387, aClass50_388, aClass50_405,
		  aClass50_396, aClass50_391, aClass50_408, aClass50_393,
		  aClass50_394, aClass50_395, aClass50_400, aClass50_397,
		  aClass50_398, aClass50_399, aClass50_407, aClass50_401,
		  aClass50_402, aClass50_379, aClass50_372, aClass50_404,
		  aClass50_406, aClass50_383, aClass50_373 });
    }
    
    public int method1157() {
	return -389811879 * anInt409;
    }
    
    public int method1158() {
	return -389811879 * anInt409;
    }
    
    public int method1159() {
	return -389811879 * anInt409;
    }
    
    public boolean method1160() {
	return aBool410;
    }
    
    public static int method1161() {
	if (-1 == 743618349 * anInt411) {
	    Class50[] class50s = Class24.method853(1183944145);
	    for (int i = 0; i < class50s.length; i++) {
		Class50 class50 = class50s[i];
		if (class50.anInt409 * -389811879 > anInt411 * 743618349)
		    anInt411 = class50.anInt409 * -1873494435;
	    }
	    anInt411 += -60444507;
	}
	return anInt411 * 743618349;
    }
    
    static final void method1162(Class669 class669, byte i) {
	IsaacCipher.method649((class669.anIntArray8595
			   [(class669.anInt8600 -= 308999563) * 2088438307]),
			  -1448978638);
    }
    
    public static void method1163(int i, int i_2_, int i_3_, int i_4_,
				  int i_5_, int i_6_, int i_7_, int i_8_,
				  byte i_9_) {
	if (i_2_ >= 0 && i_3_ >= 0
	    && i_2_ < client.aClass512_11100.method8417(784985477) - 1
	    && i_3_ < client.aClass512_11100.method8418(-1533611049) - 1) {
	    if (client.aClass512_11100.method8424((byte) 33) != null) {
		if (i_4_ == 0) {
		    Interface62 interface62
			= ((Interface62)
			   client.aClass512_11100.method8424((byte) 81)
			       .method9258(i, i_2_, i_3_, (byte) 59));
		    Interface62 interface62_10_
			= ((Interface62)
			   client.aClass512_11100.method8424((byte) 26)
			       .method9259(i, i_2_, i_3_, 924596252));
		    if (interface62 != null && 2 != i_5_) {
			if (interface62 instanceof Class654_Sub1_Sub1_Sub2)
			    ((Class654_Sub1_Sub1_Sub2) interface62)
				.aClass528_12019
				.method8807(i_7_, i_8_, -761774311);
			else
			    Class567.method9567(i, i_4_, i_2_, i_3_,
						interface62
						    .method56(647729286),
						i_6_, i_5_, i_7_, i_8_,
						(byte) 1);
		    }
		    if (interface62_10_ != null) {
			if (interface62_10_ instanceof Class654_Sub1_Sub1_Sub2)
			    ((Class654_Sub1_Sub1_Sub2) interface62_10_)
				.aClass528_12019
				.method8807(i_7_, i_8_, -761774311);
			else
			    Class567.method9567(i, i_4_, i_2_, i_3_,
						interface62_10_
						    .method56(-3868099),
						i_6_, i_5_, i_7_, i_8_,
						(byte) 1);
		    }
		} else if (1 == i_4_) {
		    Interface62 interface62
			= ((Interface62)
			   client.aClass512_11100.method8424((byte) 87)
			       .method9357(i, i_2_, i_3_, (byte) -4));
		    if (null != interface62) {
			if (interface62 instanceof Class654_Sub1_Sub3_Sub2)
			    ((Class654_Sub1_Sub3_Sub2) interface62)
				.aClass528_12036
				.method8807(i_7_, i_8_, -761774311);
			else {
			    int i_11_ = interface62.method56(1261186662);
			    if (i_5_ == 4 || i_5_ == 5)
				Class567.method9567(i, i_4_, i_2_, i_3_, i_11_,
						    i_6_, 4, i_7_, i_8_,
						    (byte) 1);
			    else if (i_5_ == 6)
				Class567.method9567(i, i_4_, i_2_, i_3_, i_11_,
						    i_6_ + 4, 4, i_7_, i_8_,
						    (byte) 1);
			    else if (7 == i_5_)
				Class567.method9567(i, i_4_, i_2_, i_3_, i_11_,
						    (i_6_ + 2 & 0x3) + 4, 4,
						    i_7_, i_8_, (byte) 1);
			    else if (8 == i_5_) {
				Class567.method9567(i, i_4_, i_2_, i_3_, i_11_,
						    i_6_ + 4, 4, i_7_, i_8_,
						    (byte) 1);
				Class567.method9567(i, i_4_, i_2_, i_3_, i_11_,
						    4 + (i_6_ + 2 & 0x3), 4,
						    i_7_, i_8_, (byte) 1);
			    }
			}
		    }
		} else if (2 == i_4_) {
		    Interface62 interface62
			= ((Interface62)
			   (client.aClass512_11100.method8424((byte) 18)
				.method9262
			    (i, i_2_, i_3_, client.anInterface64_11333,
			     (byte) -107)));
		    if (null != interface62) {
			if (i_5_ == 11)
			    i_5_ = 10;
			if (interface62 instanceof Class654_Sub1_Sub5_Sub3)
			    ((Class654_Sub1_Sub5_Sub3) interface62)
				.aClass528_12038
				.method8807(i_7_, i_8_, -761774311);
			else
			    Class567.method9567(i, i_4_, i_2_, i_3_,
						interface62
						    .method56(235192784),
						i_6_, i_5_, i_7_, i_8_,
						(byte) 1);
		    }
		} else if (i_4_ == 3) {
		    Interface62 interface62
			= ((Interface62)
			   client.aClass512_11100.method8424((byte) 52)
			       .method9264(i, i_2_, i_3_, (byte) 73));
		    if (interface62 != null) {
			if (interface62 instanceof Class654_Sub1_Sub2_Sub2)
			    ((Class654_Sub1_Sub2_Sub2) interface62)
				.aClass528_12062
				.method8807(i_7_, i_8_, -761774311);
			else
			    Class567.method9567(i, i_4_, i_2_, i_3_,
						interface62
						    .method56(-1704735678),
						i_6_, i_5_, i_7_, i_8_,
						(byte) 1);
		    }
		}
	    }
	}
    }
    
    static final void method1164(Class534_Sub40_Sub1 class534_sub40_sub1,
				 int i, int i_12_) {
	Class108.anInt1326 = 0;
	Class494.method8128(class534_sub40_sub1, 561537457);
	Class480.method7920(class534_sub40_sub1, 2145560035);
	if (class534_sub40_sub1.anInt10811 * 31645619 != i)
	    throw new RuntimeException(new StringBuilder().append
					   (class534_sub40_sub1.anInt10811
					    * 31645619)
					   .append
					   (" ").append
					   (i).toString());
    }
}
