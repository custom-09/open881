/* Class501 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class501
{
    public static final int anInt5604 = 29;
    public static final int anInt5605 = -3;
    public static final int anInt5606 = -4;
    public static final int anInt5607 = -5;
    public static final int anInt5608 = 48;
    public static final int anInt5609 = 2;
    public static final int anInt5610 = 3;
    public static final int anInt5611 = 6;
    public static final int anInt5612 = 9;
    public static final int anInt5613 = 15;
    public static final int anInt5614 = 23;
    public static final int anInt5615 = 21;
    public static final int anInt5616 = 1;
    public static final int anInt5617 = 52;
    public static final int anInt5618 = 35;
    public static final int anInt5619 = 49;
    public static final int anInt5620 = 45;
    public static final int anInt5621 = -2;
    public static final int anInt5622 = 7;
    public static final int anInt5623 = 42;
    public static final int anInt5624 = 53;
    
    Class501() throws Throwable {
	throw new Error();
    }
    
    static final void method8277(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class489.method8007(class247, class243, class669, -1349004573);
    }
    
    public static final void method8278(String string, int i) {
	if (Class168.aClass98Array1792 != null) {
	    Class100 class100 = Class201.method3864(2095398292);
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4217,
				      class100.aClass13_1183, 1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
		(Class668.method11029(string, (byte) 0), 1501317512);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -166355842);
	    class100.method1863(class534_sub19, (byte) 32);
	}
    }
}
