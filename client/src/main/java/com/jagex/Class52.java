/* Class52 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class52
{
    public static final int anInt415 = 2048;
    public static final int anInt416 = 32768;
    public static final int anInt417 = 16;
    public static final int anInt418 = 64;
    public static final int anInt419 = 4;
    public static final int anInt420 = 32;
    public static final int anInt421 = 512;
    public static final int anInt422 = 256;
    public static final int anInt423 = 4096;
    public static final int anInt424 = 8192;
    public static final int anInt425 = 16384;
    public static final int anInt426 = 2;
    protected int anInt427 = -581203021;
    protected int anInt428 = 211221557;
    protected int anInt429 = 2003428493;
    protected int anInt430 = -1031958597;
    protected int anInt431 = 406105807;
    protected int anInt432;
    protected int anInt433;
    protected int anInt434;
    public static Class472 aClass472_435;
    static Class641 aClass641_436;
    
    public int method1181() {
	return anInt434 * -164430629;
    }
    
    public int method1182(short i) {
	return anInt434 * -164430629;
    }
    
    public int method1183() {
	return anInt434 * -164430629;
    }
    
    public int method1184() {
	return anInt434 * -164430629;
    }
    
    public int method1185() {
	return anInt434 * -164430629;
    }
    
    Class52() {
	/* empty */
    }
    
    static final void method1186(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (client.aString11300 != null
	    && i_0_ < Class455.anInt4963 * -217094943)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 1829245767 * Class168.aClass98Array1792[i_0_].anInt1164;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
    }
}
