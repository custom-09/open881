/* Class52_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class52_Sub1 extends Class52 implements Interface68
{
    public void method203() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4219,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt431 * -1260886063, 395633313);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(anInt432 * -564260043, 1201715149);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt428 * -1924232733, -2077287083);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16724(anInt429 * -131628613, (byte) -17);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(anInt433 * -1817792963, 1373572980);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt430 * -592239987, -1427457258);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-1234705275 * anInt427, -218457523);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(-164430629 * anInt434, 1739588782);
	client.aClass100_11264.method1863(class534_sub19, (byte) 23);
    }
    
    public void method356(int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4219,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt431 * -1260886063, -1893286161);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(anInt432 * -564260043, -1945899220);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt428 * -1924232733, -832340693);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16724(anInt429 * -131628613, (byte) 23);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(anInt433 * -1817792963, 1121169902);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt430 * -592239987, -1708821959);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-1234705275 * anInt427, -1509260983);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(-164430629 * anInt434, 1797767758);
	client.aClass100_11264.method1863(class534_sub19, (byte) 21);
    }
    
    public void method16406(int i) {
	anInt434 = i * 1560584019;
    }
    
    public void method16407(int i, int i_0_) {
	anInt432 = (-564260043 * anInt432 | i) * 1901586205;
    }
    
    public void method16408(int i) {
	anInt432 = (-564260043 * anInt432 | i) * 1901586205;
    }
    
    public void method16409(int i, int i_1_, int i_2_) {
	if (0 == i && i_1_ > 32767)
	    i_1_ = 32767;
	else if (i_1_ > 8388607)
	    i_1_ = 8388607;
	switch (i) {
	case 5:
	    anInt431 = -406105807 * i_1_;
	    break;
	case 0:
	    anInt427 = i_1_ * 581203021;
	    break;
	case 3:
	    anInt430 = i_1_ * 1031958597;
	    break;
	case 1:
	    anInt429 = i_1_ * -2003428493;
	    break;
	}
    }
    
    public void method286() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4219,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt431 * -1260886063, -590744207);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(anInt432 * -564260043, -610586745);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt428 * -1924232733, -299453038);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16724(anInt429 * -131628613, (byte) 6);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(anInt433 * -1817792963, 887285264);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt430 * -592239987, -1360846638);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-1234705275 * anInt427, 34952091);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(-164430629 * anInt434, 1322505989);
	client.aClass100_11264.method1863(class534_sub19, (byte) 6);
    }
    
    public void method142() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4219,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt431 * -1260886063, -437887318);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(anInt432 * -564260043, -806606221);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt428 * -1924232733, -1869266900);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16724(anInt429 * -131628613, (byte) -48);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(anInt433 * -1817792963, 1114309234);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt430 * -592239987, -1326267911);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-1234705275 * anInt427, -599300737);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(-164430629 * anInt434, 1250059697);
	client.aClass100_11264.method1863(class534_sub19, (byte) 127);
    }
    
    public void method439() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4219,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt431 * -1260886063, -548226126);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(anInt432 * -564260043, 494109912);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt428 * -1924232733, -792438438);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16724(anInt429 * -131628613, (byte) -66);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(anInt433 * -1817792963, 2082824521);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16509(anInt430 * -592239987, -132401671);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-1234705275 * anInt427, 119010462);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(-164430629 * anInt434, 1839922928);
	client.aClass100_11264.method1863(class534_sub19, (byte) 16);
    }
    
    public void method16410(int i) {
	anInt432 = (-564260043 * anInt432 | i) * 1901586205;
    }
    
    public void method16411(int i, int i_3_) {
	anInt434 = i * 1560584019;
    }
    
    public void method16412(int i) {
	anInt434 = i * 1560584019;
    }
    
    public void method16413(int i) {
	anInt434 = i * 1560584019;
    }
    
    public void method16414(int i) {
	anInt434 = i * 1560584019;
    }
    
    public void method16415(int i, int i_4_) {
	anInt433 = -330639083 * i;
    }
    
    public void method16416(int i) {
	anInt433 = -330639083 * i;
    }
    
    public void method16417(int i) {
	anInt433 = -330639083 * i;
    }
    
    public void method16418(int i, int i_5_) {
	if (0 == i && i_5_ > 32767)
	    i_5_ = 32767;
	else if (i_5_ > 8388607)
	    i_5_ = 8388607;
	switch (i) {
	case 5:
	    anInt431 = -406105807 * i_5_;
	    break;
	case 0:
	    anInt427 = i_5_ * 581203021;
	    break;
	case 3:
	    anInt430 = i_5_ * 1031958597;
	    break;
	case 1:
	    anInt429 = i_5_ * -2003428493;
	    break;
	}
    }
}
