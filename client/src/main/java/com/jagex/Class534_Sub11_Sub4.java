/* Class534_Sub11_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub11_Sub4 extends Class534_Sub11
{
    int anInt11654;
    String aString11655;
    long aLong11656;
    Class346 this$0;
    
    void method16131(Class352 class352) {
	class352.method6198(aLong11656 * -6118778694071463293L, aString11655,
			    -1982464077 * anInt11654, 1421237380);
    }
    
    void method16128(Class534_Sub40 class534_sub40, int i) {
	if (class534_sub40.method16527(1951469648) != 255) {
	    class534_sub40.anInt10811 -= -1387468933;
	    aLong11656 = (class534_sub40.method16537(1359621443)
			  * 3582335655677004843L);
	}
	aString11655 = class534_sub40.method16540(76978635);
	anInt11654 = class534_sub40.method16529((byte) 1) * 2096023419;
    }
    
    void method16129(Class352 class352, byte i) {
	class352.method6198(aLong11656 * -6118778694071463293L, aString11655,
			    -1982464077 * anInt11654, -929996571);
    }
    
    void method16130(Class534_Sub40 class534_sub40) {
	if (class534_sub40.method16527(2037635326) != 255) {
	    class534_sub40.anInt10811 -= -1387468933;
	    aLong11656 = (class534_sub40.method16537(1359621443)
			  * 3582335655677004843L);
	}
	aString11655 = class534_sub40.method16540(76978635);
	anInt11654 = class534_sub40.method16529((byte) 1) * 2096023419;
    }
    
    Class534_Sub11_Sub4(Class346 class346) {
	this$0 = class346;
	aLong11656 = -3582335655677004843L;
	aString11655 = null;
	anInt11654 = 0;
    }
    
    void method16127(Class352 class352) {
	class352.method6198(aLong11656 * -6118778694071463293L, aString11655,
			    -1982464077 * anInt11654, 1312533638);
    }
    
    void method16132(Class352 class352) {
	class352.method6198(aLong11656 * -6118778694071463293L, aString11655,
			    -1982464077 * anInt11654, -519194650);
    }
}
