/* Class534_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub17 extends Class534
{
    public int anInt10499;
    public int anInt10500;
    public int anInt10501;
    public int anInt10502;
    public int anInt10503;
    public int anInt10504;
    public int anInt10505;
    public int anInt10506;
    public int anInt10507;
    
    Class534_Sub17(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16533(-258848859);
	anInt10505 = (i >>> 28) * -1087455833;
	anInt10500 = (i >>> 14 & 0x3fff) * 52449129;
	anInt10501 = (i & 0x3fff) * -21357985;
	anInt10504 = class534_sub40.method16527(1202943500) * 1089743733;
	anInt10506 = class534_sub40.method16527(-1981828016) * -2056302421;
	anInt10502 = class534_sub40.method16527(-29899304) * 682144289;
	anInt10507 = class534_sub40.method16527(1536145165) * 1920524889;
	anInt10499 = class534_sub40.method16527(507856131) * -764612805;
	anInt10503 = class534_sub40.method16527(-1893419233) * 174675339;
    }
}
