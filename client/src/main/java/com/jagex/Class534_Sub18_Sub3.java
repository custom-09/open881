/* Class534_Sub18_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub3 extends Class534_Sub18
    implements Interface13, Interface7
{
    Class9 aClass9_11374;
    static int anInt11375;
    
    public String method17868(int i, String string) {
	if (aClass9_11374 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_11374.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(199470873);
	    if (0 == i_0_)
		break;
	    method17869(class534_sub40, i_0_, (byte) -78);
	}
    }
    
    void method17869(Class534_Sub40 class534_sub40, int i, byte i_1_) {
	if (249 == i) {
	    int i_2_ = class534_sub40.method16527(-1241541051);
	    if (aClass9_11374 == null) {
		int i_3_ = Class162.method2640(i_2_, (byte) 72);
		aClass9_11374 = new Class9(i_3_);
	    }
	    for (int i_4_ = 0; i_4_ < i_2_; i_4_++) {
		boolean bool = class534_sub40.method16527(-801570160) == 1;
		int i_5_ = class534_sub40.method16531(1258844190);
		Class534 class534;
		if (bool)
		    class534 = new Class534_Sub6(class534_sub40
						     .method16541((byte) -93));
		else
		    class534
			= new Class534_Sub39(class534_sub40
						 .method16533(-258848859));
		aClass9_11374.method581(class534, (long) i_5_);
	    }
	}
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1030523878);
	    if (0 == i)
		break;
	    method17869(class534_sub40, i, (byte) -126);
	}
    }
    
    public String method17870(int i, String string) {
	if (aClass9_11374 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_11374.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    void method17871(Class534_Sub40 class534_sub40, int i) {
	if (249 == i) {
	    int i_6_ = class534_sub40.method16527(1486357876);
	    if (aClass9_11374 == null) {
		int i_7_ = Class162.method2640(i_6_, (byte) 69);
		aClass9_11374 = new Class9(i_7_);
	    }
	    for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
		boolean bool = class534_sub40.method16527(-37631154) == 1;
		int i_9_ = class534_sub40.method16531(1287524453);
		Class534 class534;
		if (bool)
		    class534 = new Class534_Sub6(class534_sub40
						     .method16541((byte) -54));
		else
		    class534
			= new Class534_Sub39(class534_sub40
						 .method16533(-258848859));
		aClass9_11374.method581(class534, (long) i_9_);
	    }
	}
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1654616502);
	    if (0 == i)
		break;
	    method17869(class534_sub40, i, (byte) -84);
	}
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(85358115);
	    if (0 == i)
		break;
	    method17869(class534_sub40, i, (byte) -60);
	}
    }
    
    public void method67(int i, int i_10_) {
	/* empty */
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    public void method66(int i) {
	/* empty */
    }
    
    public void method65(int i) {
	/* empty */
    }
    
    void method17872(Class534_Sub40 class534_sub40, int i) {
	if (249 == i) {
	    int i_11_ = class534_sub40.method16527(207333830);
	    if (aClass9_11374 == null) {
		int i_12_ = Class162.method2640(i_11_, (byte) 60);
		aClass9_11374 = new Class9(i_12_);
	    }
	    for (int i_13_ = 0; i_13_ < i_11_; i_13_++) {
		boolean bool = class534_sub40.method16527(-279330855) == 1;
		int i_14_ = class534_sub40.method16531(139249429);
		Class534 class534;
		if (bool)
		    class534
			= new Class534_Sub6(class534_sub40
						.method16541((byte) -111));
		else
		    class534
			= new Class534_Sub39(class534_sub40
						 .method16533(-258848859));
		aClass9_11374.method581(class534, (long) i_14_);
	    }
	}
    }
    
    public String method17873(int i, String string, int i_15_) {
	if (aClass9_11374 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_11374.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public int method17874(int i, int i_16_, byte i_17_) {
	if (null == aClass9_11374)
	    return i_16_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_11374.method579((long) i);
	if (null == class534_sub39)
	    return i_16_;
	return class534_sub39.anInt10807 * -705967177;
    }
    
    Class534_Sub18_Sub3() {
	/* empty */
    }
    
    public String method17875(int i, String string) {
	if (aClass9_11374 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_11374.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public String method17876(int i, String string) {
	if (aClass9_11374 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_11374.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    static final void method17877(Class669 class669, int i) {
	Class625.method10330(class669, class669.aClass654_Sub1_Sub5_Sub1_8604,
			     1161105126);
    }
    
    public static Class606_Sub1 method17878(int i) {
	Class5.anInt48 = 0;
	return Class498.method8261(-1146074661);
    }
}
