/* Class534_Sub18_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub7 extends Class534_Sub18
{
    int anInt11698;
    String aString11699;
    int anInt11700;
    boolean aBool11701;
    long aLong11702;
    int anInt11703;
    int anInt11704;
    boolean aBool11705;
    int anInt11706;
    boolean aBool11707;
    String aString11708;
    long aLong11709;
    String aString11710;
    
    public long method18225() {
	return -6387465159951953483L * aLong11709;
    }
    
    Class534_Sub18_Sub7(String string, String string_0_, int i, int i_1_,
			int i_2_, long l, int i_3_, int i_4_, boolean bool,
			boolean bool_5_, long l_6_, boolean bool_7_) {
	aString11708 = string_0_;
	aString11699 = string;
	anInt11700 = i * -1012672397;
	anInt11706 = i_1_ * -390549453;
	anInt11698 = i_2_ * 420679787;
	aLong11702 = l * 4198517094207527929L;
	anInt11703 = i_3_ * -172997041;
	anInt11704 = -533383379 * i_4_;
	aBool11705 = bool;
	aBool11701 = bool_5_;
	aLong11709 = 7996350968569255069L * l_6_;
	aBool11707 = bool_7_;
    }
    
    public long method18226(byte i) {
	return aLong11702 * -7225575275964615095L;
    }
    
    public long method18227() {
	return aLong11702 * -7225575275964615095L;
    }
    
    public long method18228(byte i) {
	return -6387465159951953483L * aLong11709;
    }
}
