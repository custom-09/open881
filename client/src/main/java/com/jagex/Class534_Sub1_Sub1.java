/* Class534_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub1_Sub1 extends Class534_Sub1
{
    int anInt11690;
    byte aByte11691;
    int anInt11692;
    String aString11693;
    Class351 this$0;
    
    void method16014(Class534_Sub40 class534_sub40) {
	class534_sub40.method16527(272502143);
	anInt11692 = class534_sub40.method16529((byte) 1) * 1684728147;
	aByte11691 = class534_sub40.method16586((byte) 1);
	anInt11690 = class534_sub40.method16529((byte) 1) * 864609147;
	class534_sub40.method16537(1359621443);
	aString11693 = class534_sub40.method16541((byte) -74);
	class534_sub40.method16527(-538053227);
    }
    
    void method16012(Class534_Sub40 class534_sub40, int i) {
	class534_sub40.method16527(1127917641);
	anInt11692 = class534_sub40.method16529((byte) 1) * 1684728147;
	aByte11691 = class534_sub40.method16586((byte) 1);
	anInt11690 = class534_sub40.method16529((byte) 1) * 864609147;
	class534_sub40.method16537(1359621443);
	aString11693 = class534_sub40.method16541((byte) -46);
	class534_sub40.method16527(1963226120);
    }
    
    Class534_Sub1_Sub1(Class351 class351) {
	this$0 = class351;
	anInt11692 = -1684728147;
    }
    
    void method16011(Class534_Sub26 class534_sub26, int i) {
	Class337 class337
	    = class534_sub26.aClass337Array10579[850477787 * anInt11692];
	class337.aByte3521 = aByte11691;
	class337.anInt3522 = -1034456163 * anInt11690;
	class337.aString3523 = aString11693;
    }
    
    void method16015(Class534_Sub26 class534_sub26) {
	Class337 class337
	    = class534_sub26.aClass337Array10579[850477787 * anInt11692];
	class337.aByte3521 = aByte11691;
	class337.anInt3522 = -1034456163 * anInt11690;
	class337.aString3523 = aString11693;
    }
    
    void method16013(Class534_Sub26 class534_sub26) {
	Class337 class337
	    = class534_sub26.aClass337Array10579[850477787 * anInt11692];
	class337.aByte3521 = aByte11691;
	class337.anInt3522 = -1034456163 * anInt11690;
	class337.aString3523 = aString11693;
    }
    
    static final void method18224(Class669 class669, byte i) {
	Class534_Sub18_Sub9 class534_sub18_sub9
	    = Class232.method4339(429960120);
	if (null != class534_sub18_sub9) {
	    boolean bool
		= (class534_sub18_sub9.method18264
		   (Class554.anInt7368 + 204700261 * Class151.anInt1705,
		    Class328.anInt3479 * -1636630007 + Class554.anInt7369,
		    Class679.anIntArray8659, -1630345302));
	    if (bool) {
		class669.anIntArray8595
		    [(class669.anInt8600 += 308999563) * 2088438307 - 1]
		    = Class679.anIntArray8659[1];
		class669.anIntArray8595
		    [(class669.anInt8600 += 308999563) * 2088438307 - 1]
		    = Class679.anIntArray8659[2];
	    } else {
		class669.anIntArray8595
		    [(class669.anInt8600 += 308999563) * 2088438307 - 1]
		    = -1;
		class669.anIntArray8595
		    [(class669.anInt8600 += 308999563) * 2088438307 - 1]
		    = -1;
	    }
	} else {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	}
    }
}
