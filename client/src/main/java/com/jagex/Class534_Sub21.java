/* Class534_Sub21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub21 extends Class534
{
    int anInt10526;
    int anInt10527;
    int anInt10528;
    public Class438 aClass438_10529 = new Class438();
    float aFloat10530;
    int anInt10531;
    int anInt10532;
    int anInt10533;
    long aLong10534;
    int anInt10535;
    static String aString10536;
    
    public final int method16196() {
	return (int) aClass438_10529.aFloat4865;
    }
    
    public final int method16197(int i) {
	return (int) aClass438_10529.aFloat4864;
    }
    
    public void method16198(int i) {
	anInt10532 = i * -2068541819;
    }
    
    public final int method16199(byte i) {
	return (int) aClass438_10529.aFloat4865;
    }
    
    public final int method16200() {
	return -33867187 * anInt10532;
    }
    
    public final int method16201(byte i) {
	return -33867187 * anInt10532;
    }
    
    public final float method16202(int i) {
	return aFloat10530;
    }
    
    public void method16203(float f, byte i) {
	aFloat10530 = f;
    }
    
    public void method16204(int i, int i_0_, int i_1_, int i_2_) {
	aClass438_10529.method6997((float) i, (float) i_0_, (float) i_1_);
    }
    
    public final int method16205() {
	return (int) aClass438_10529.aFloat4865;
    }
    
    public int method16206(byte i) {
	return anInt10533 * -1408190947;
    }
    
    public void method16207(int i, int i_3_) {
	anInt10532 = i * -2068541819;
    }
    
    public final int method16208() {
	return (int) aClass438_10529.aFloat4863;
    }
    
    public void method16209(int i) {
	if (anInt10526 * -1108222799 != -33867187 * anInt10532) {
	    long l = Class250.method4604((byte) -102);
	    long l_4_ = l - -2219571627323776075L * aLong10534;
	    anInt10533 -= l_4_ * 410106933L;
	    if (anInt10533 * -1408190947 > 0)
		anInt10532
		    = Class69.method1396(-722080191 * anInt10527,
					 -1108222799 * anInt10526,
					 ((float) (anInt10535 * -1598616773
						   - anInt10533 * -1408190947)
					  / (float) (anInt10535 * -1598616773)
					  * 255.0F),
					 (byte) -31) * -2068541819;
	    else
		anInt10532 = anInt10526 * 1199953397;
	    aLong10534 = -5417107606381694307L * l;
	}
    }
    
    public final int method16210() {
	return (int) aClass438_10529.aFloat4864;
    }
    
    public final int method16211() {
	return (int) aClass438_10529.aFloat4864;
    }
    
    public final int method16212() {
	return (int) aClass438_10529.aFloat4864;
    }
    
    public final int method16213() {
	return (int) aClass438_10529.aFloat4864;
    }
    
    public int method16214() {
	return anInt10533 * -1408190947;
    }
    
    public void method16215(int i, int i_5_, int i_6_) {
	aClass438_10529.method6997((float) i, (float) i_5_, (float) i_6_);
    }
    
    public void method16216(float f) {
	aFloat10530 = f;
    }
    
    public final int method16217() {
	return (int) aClass438_10529.aFloat4865;
    }
    
    public final int method16218() {
	return -33867187 * anInt10532;
    }
    
    public final int method16219() {
	return -33867187 * anInt10532;
    }
    
    public void method16220() {
	if (anInt10526 * -1108222799 != -33867187 * anInt10532) {
	    long l = Class250.method4604((byte) -25);
	    long l_7_ = l - -2219571627323776075L * aLong10534;
	    anInt10533 -= l_7_ * 410106933L;
	    if (anInt10533 * -1408190947 > 0)
		anInt10532
		    = Class69.method1396(-722080191 * anInt10527,
					 -1108222799 * anInt10526,
					 ((float) (anInt10535 * -1598616773
						   - anInt10533 * -1408190947)
					  / (float) (anInt10535 * -1598616773)
					  * 255.0F),
					 (byte) 104) * -2068541819;
	    else
		anInt10532 = anInt10526 * 1199953397;
	    aLong10534 = -5417107606381694307L * l;
	}
    }
    
    public final int method16221() {
	return -33867187 * anInt10532;
    }
    
    public final float method16222() {
	return aFloat10530;
    }
    
    public final float method16223() {
	return aFloat10530;
    }
    
    public void method16224(float f) {
	aFloat10530 = f;
    }
    
    public int method16225() {
	return anInt10526 * -1108222799;
    }
    
    public final int method16226(int i) {
	return (int) aClass438_10529.aFloat4863;
    }
    
    Class534_Sub21(int i, int i_8_, int i_9_, int i_10_, int i_11_, float f) {
	aClass438_10529.method6997((float) i, (float) i_8_, (float) i_9_);
	anInt10531 = -146399327 * i_10_;
	anInt10528 = i_11_ * -1126680375;
	anInt10532 = anInt10528 * -77807139;
	anInt10526 = anInt10528 * 1954636617;
	aFloat10530 = f;
    }
    
    public void method16227(int i, int i_12_, int i_13_) {
	aClass438_10529.method6997((float) i, (float) i_12_, (float) i_13_);
    }
    
    public void method16228(int i, int i_14_, int i_15_) {
	aClass438_10529.method6997((float) i, (float) i_14_, (float) i_15_);
    }
    
    public int method16229() {
	return anInt10526 * -1108222799;
    }
    
    public int method16230(byte i) {
	return anInt10526 * -1108222799;
    }
    
    public int method16231() {
	return anInt10533 * -1408190947;
    }
    
    public void method16232(float f) {
	aFloat10530 = f;
    }
    
    public final int method16233(int i) {
	return 1841267809 * anInt10531;
    }
    
    public void method16234(int i, int i_16_) {
	anInt10526 = i * 686222417;
	anInt10527 = anInt10532 * 1331159309;
	anInt10535 = 1756730867 * i_16_;
	if (-1 == anInt10526 * -1108222799)
	    anInt10526 = anInt10528 * 1954636617;
	anInt10533 = 410106933 * i_16_;
	aLong10534 = Class250.method4604((byte) -70) * -5417107606381694307L;
    }
    
    public void method16235(int i, int i_17_, byte i_18_) {
	anInt10526 = i * 686222417;
	anInt10527 = anInt10532 * 1331159309;
	anInt10535 = 1756730867 * i_17_;
	if (-1 == anInt10526 * -1108222799)
	    anInt10526 = anInt10528 * 1954636617;
	anInt10533 = 410106933 * i_17_;
	aLong10534 = Class250.method4604((byte) -111) * -5417107606381694307L;
    }
    
    public void method16236() {
	if (anInt10526 * -1108222799 != -33867187 * anInt10532) {
	    long l = Class250.method4604((byte) -53);
	    long l_19_ = l - -2219571627323776075L * aLong10534;
	    anInt10533 -= l_19_ * 410106933L;
	    if (anInt10533 * -1408190947 > 0)
		anInt10532
		    = Class69.method1396(-722080191 * anInt10527,
					 -1108222799 * anInt10526,
					 ((float) (anInt10535 * -1598616773
						   - anInt10533 * -1408190947)
					  / (float) (anInt10535 * -1598616773)
					  * 255.0F),
					 (byte) 35) * -2068541819;
	    else
		anInt10532 = anInt10526 * 1199953397;
	    aLong10534 = -5417107606381694307L * l;
	}
    }
    
    public void method16237() {
	if (anInt10526 * -1108222799 != -33867187 * anInt10532) {
	    long l = Class250.method4604((byte) -105);
	    long l_20_ = l - -2219571627323776075L * aLong10534;
	    anInt10533 -= l_20_ * 410106933L;
	    if (anInt10533 * -1408190947 > 0)
		anInt10532
		    = Class69.method1396(-722080191 * anInt10527,
					 -1108222799 * anInt10526,
					 ((float) (anInt10535 * -1598616773
						   - anInt10533 * -1408190947)
					  / (float) (anInt10535 * -1598616773)
					  * 255.0F),
					 (byte) -47) * -2068541819;
	    else
		anInt10532 = anInt10526 * 1199953397;
	    aLong10534 = -5417107606381694307L * l;
	}
    }
    
    public void method16238() {
	if (anInt10526 * -1108222799 != -33867187 * anInt10532) {
	    long l = Class250.method4604((byte) -30);
	    long l_21_ = l - -2219571627323776075L * aLong10534;
	    anInt10533 -= l_21_ * 410106933L;
	    if (anInt10533 * -1408190947 > 0)
		anInt10532
		    = Class69.method1396(-722080191 * anInt10527,
					 -1108222799 * anInt10526,
					 ((float) (anInt10535 * -1598616773
						   - anInt10533 * -1408190947)
					  / (float) (anInt10535 * -1598616773)
					  * 255.0F),
					 (byte) 14) * -2068541819;
	    else
		anInt10532 = anInt10526 * 1199953397;
	    aLong10534 = -5417107606381694307L * l;
	}
    }
    
    static final void method16239(Class669 class669, short i) {
	Class235.method4408(Class200_Sub5.method15573((byte) 44), (byte) 83);
    }
    
    static final void method16240(Class669 class669, int i) {
	int i_22_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (2 == client.anInt11171 * -1050164879
	    && i_22_ < client.anInt11324 * -1979292205)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= client.aClass28Array11327[i_22_].aString249;
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
    }
}
