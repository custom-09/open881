/* Class534_Sub25 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import com.jagex.twitchtv.TwitchEventLiveStreams;

public final class Class534_Sub25 extends Class534
{
    public static final int anInt10566 = 2;
    public static final int anInt10567 = 1;
    public static final int anInt10568 = 8;
    public static final int anInt10569 = 64;
    public static final int anInt10570 = 4;
    public static final int anInt10571 = 32;
    public static final int anInt10572 = 16;
    static Class534_Sub25 aClass534_Sub25_10573 = new Class534_Sub25(0, -1);
    public int anInt10574;
    public int anInt10575;
    public static TwitchEventLiveStreams aTwitchEventLiveStreams10576;
    
    public final int method16262() {
	return 1837782131 * anInt10574 >> 18 & 0x7;
    }
    
    public final int method16263() {
	return Class386.method6497(1837782131 * anInt10574, -992824565);
    }
    
    public final boolean method16264(int i, byte i_0_) {
	return (anInt10574 * 1837782131 >> i + 1 & 0x1) != 0;
    }
    
    public Class534_Sub25(int i, int i_1_) {
	anInt10574 = -831098693 * i;
	anInt10575 = i_1_ * -636688699;
    }
    
    public final int method16265(short i) {
	return 1837782131 * anInt10574 >> 18 & 0x7;
    }
    
    public final boolean method16266(short i) {
	return (anInt10574 * 1837782131 >> 21 & 0x1) != 0;
    }
    
    public final boolean method16267(int i) {
	return 0 != (1837782131 * anInt10574 >> 22 & 0x1);
    }
    
    public final boolean method16268(int i) {
	return 0 != (anInt10574 * 1837782131 >> 23 & 0x1);
    }
    
    public final boolean method16269() {
	return (1837782131 * anInt10574 & 0x1) != 0;
    }
    
    public final boolean method16270(int i) {
	return (anInt10574 * 1837782131 >> i + 1 & 0x1) != 0;
    }
    
    public final int method16271(byte i) {
	return Class386.method6497(1837782131 * anInt10574, 408014496);
    }
    
    public final boolean method16272(short i) {
	return (1837782131 * anInt10574 & 0x1) != 0;
    }
    
    public final int method16273() {
	return Class386.method6497(1837782131 * anInt10574, -370256094);
    }
    
    static final int method16274(int i) {
	return i >> 11 & 0x7f;
    }
    
    public final boolean method16275() {
	return 0 != (1837782131 * anInt10574 >> 22 & 0x1);
    }
    
    public final boolean method16276() {
	return (anInt10574 * 1837782131 >> 21 & 0x1) != 0;
    }
    
    public final boolean method16277() {
	return (anInt10574 * 1837782131 >> 21 & 0x1) != 0;
    }
    
    public final boolean method16278() {
	return (anInt10574 * 1837782131 >> 21 & 0x1) != 0;
    }
    
    static final int method16279(int i) {
	return i >> 11 & 0x7f;
    }
    
    public final boolean method16280() {
	return 0 != (1837782131 * anInt10574 >> 22 & 0x1);
    }
    
    public final boolean method16281() {
	return 0 != (anInt10574 * 1837782131 >> 23 & 0x1);
    }
}
