/* Class534_Sub32 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub32 extends Class534
{
    int anInt10696;
    float aFloat10697;
    int anInt10698;
    int anInt10699;
    Class384 aClass384_10700;
    float aFloat10701;
    Class158 aClass158_10702;
    static float[] aFloatArray10703 = new float[3];
    
    Class534_Sub32(Class158 class158, Class629 class629) {
	aClass158_10702 = class158;
	aClass384_10700
	    = aClass158_10702.method2590(Class625.anInterface46_8159,
					 (byte) 84);
	method16388(-155818650);
    }
    
    void method16388(int i) {
	anInt10698 = 1492208541 * aClass158_10702.anInt1750;
	anInt10699 = aClass158_10702.anInt1751 * -884278269;
	anInt10696 = aClass158_10702.anInt1749 * 837938373;
	if (aClass158_10702.aClass433_1747 != null)
	    aClass158_10702.aClass433_1747.method6852
		((float) (-1058410147 * aClass384_10700.anInt3945),
		 (float) (aClass384_10700.anInt3957 * 829166933),
		 (float) (894885741 * aClass384_10700.anInt3953),
		 aFloatArray10703);
	aFloat10701 = aFloatArray10703[0];
	aFloat10697 = aFloatArray10703[2];
    }
    
    void method16389() {
	anInt10698 = 1492208541 * aClass158_10702.anInt1750;
	anInt10699 = aClass158_10702.anInt1751 * -884278269;
	anInt10696 = aClass158_10702.anInt1749 * 837938373;
	if (aClass158_10702.aClass433_1747 != null)
	    aClass158_10702.aClass433_1747.method6852
		((float) (-1058410147 * aClass384_10700.anInt3945),
		 (float) (aClass384_10700.anInt3957 * 829166933),
		 (float) (894885741 * aClass384_10700.anInt3953),
		 aFloatArray10703);
	aFloat10701 = aFloatArray10703[0];
	aFloat10697 = aFloatArray10703[2];
    }
    
    void method16390() {
	anInt10698 = 1492208541 * aClass158_10702.anInt1750;
	anInt10699 = aClass158_10702.anInt1751 * -884278269;
	anInt10696 = aClass158_10702.anInt1749 * 837938373;
	if (aClass158_10702.aClass433_1747 != null)
	    aClass158_10702.aClass433_1747.method6852
		((float) (-1058410147 * aClass384_10700.anInt3945),
		 (float) (aClass384_10700.anInt3957 * 829166933),
		 (float) (894885741 * aClass384_10700.anInt3953),
		 aFloatArray10703);
	aFloat10701 = aFloatArray10703[0];
	aFloat10697 = aFloatArray10703[2];
    }
    
    static final void method16391(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	class669.anInt8600 -= 617999126;
	int i_0_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_1_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = string.substring(i_0_, i_1_);
    }
    
    static final void method16392(Class669 class669, byte i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub31_10785,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]),
	     -959021878);
	Class672.method11096((byte) 1);
    }
}
