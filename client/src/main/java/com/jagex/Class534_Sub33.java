/* Class534_Sub33 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class534_Sub33 extends Class534 implements Interface63
{
    char aChar10717;
    static final int anInt10718 = -1;
    int anInt10719;
    int anInt10720;
    long aLong10721;
    int anInt10722;
    
    public char method427() {
	return aChar10717;
    }
    
    public long method365() {
	return -9064052320171598525L * aLong10721;
    }
    
    public int method190() {
	return anInt10722 * -923010969;
    }
    
    public int method431(byte i) {
	return anInt10720 * -387225769;
    }
    
    public long method425(int i) {
	return -9064052320171598525L * aLong10721;
    }
    
    public int method426(byte i) {
	return anInt10722 * -923010969;
    }
    
    public int method85() {
	return anInt10719 * 1926402485;
    }
    
    public int method56(int i) {
	return anInt10719 * 1926402485;
    }
    
    public char method429() {
	return aChar10717;
    }
    
    public char method428() {
	return aChar10717;
    }
    
    public int method430() {
	return anInt10722 * -923010969;
    }
    
    public int method181() {
	return anInt10720 * -387225769;
    }
    
    public int method145() {
	return anInt10720 * -387225769;
    }
    
    public char method424(byte i) {
	return aChar10717;
    }
    
    Class534_Sub33() {
	/* empty */
    }
    
    public static Class534_Sub36 method16405(int i, int i_0_, int i_1_,
					     int i_2_, int i_3_) {
	synchronized (Class534_Sub36.aClass534_Sub36Array10794) {
	    if (0 == 2450901 * Class534_Sub36.anInt10795) {
		Class534_Sub36 class534_sub36
		    = new Class534_Sub36(i, i_0_, i_1_, i_2_);
		return class534_sub36;
	    }
	    Class534_Sub36.aClass534_Sub36Array10794
		[(Class534_Sub36.anInt10795 -= 1809361789) * 2450901]
		.method16465(i, i_0_, i_1_, i_2_, (byte) 49);
	    Class534_Sub36 class534_sub36
		= (Class534_Sub36.aClass534_Sub36Array10794
		   [2450901 * Class534_Sub36.anInt10795]);
	    return class534_sub36;
	}
    }
}
