/* Class534_Sub37_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub37_Sub4 extends Class534_Sub37
{
    Class677 aClass677_11889;
    
    public boolean method16499(byte i) {
	Class654_Sub1_Sub4_Sub1 class654_sub1_sub4_sub1
	    = aClass677_11889.method11136((byte) -2);
	if (null != class654_sub1_sub4_sub1) {
	    Class262.method4823(Class583.aClass583_7789,
				anInt10803 * 1225863589, -1, aClass677_11889,
				class654_sub1_sub4_sub1, -1929497052);
	    return true;
	}
	return false;
    }
    
    public Class534_Sub37_Sub4(int i, int i_0_, Class677 class677) {
	super(i, i_0_);
	aClass677_11889 = class677;
    }
    
    public boolean method16500() {
	Class654_Sub1_Sub4_Sub1 class654_sub1_sub4_sub1
	    = aClass677_11889.method11136((byte) -26);
	if (null != class654_sub1_sub4_sub1) {
	    Class262.method4823(Class583.aClass583_7789,
				anInt10803 * 1225863589, -1, aClass677_11889,
				class654_sub1_sub4_sub1, -1929497052);
	    return true;
	}
	return false;
    }
}
