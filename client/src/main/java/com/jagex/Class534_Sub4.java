/* Class534_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class534_Sub4 extends Class534
{
    int anInt10395;
    int anInt10396;
    public int anInt10397;
    public int anInt10398;
    Class596 aClass596_10399;
    public int anInt10400;
    int anInt10401;
    int anInt10402;
    public int anInt10403;
    int anInt10404;
    int anInt10405;
    boolean aBool10406 = true;
    boolean aBool10407 = false;
    public static Class700 aClass700_10408 = new Class700();
    public static Class700 aClass700_10409 = new Class700();
    static long aLong10410 = 4551746311500396985L;
    
    static final void method16026(int i, int i_0_, int i_1_, int i_2_,
				  int i_3_, int i_4_, int i_5_, int i_6_,
				  int i_7_) {
	if (i_1_ >= 1 && i_2_ >= 1
	    && i_1_ <= client.aClass512_11100.method8417(1171759705) - 2
	    && i_2_ <= client.aClass512_11100.method8418(-1533611049) - 2) {
	    int i_8_ = i;
	    if (i_8_ < 3 && client.aClass512_11100.method8552((byte) 0)
				.method7612(i_1_, i_2_, (byte) 0))
		i_8_++;
	    if (client.aClass512_11100.method8424((byte) 114) != null) {
		client.aClass512_11100.method8493(-648138732).method15968
		    (Class254.aClass185_2683, i, i_0_, i_1_, i_2_, 447242637);
		if (i_3_ >= 0) {
		    int i_9_
			= Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub10_10751.method16970((byte) 12);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 1, -1775117436);
		    client.aClass512_11100.method8493(-776234674).method15942
			(Class254.aClass185_2683, i_8_, i, i_1_, i_2_, i_3_,
			 i_4_, i_5_, i_6_, i_7_, 2087835778);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 i_9_, 1012867115);
		}
		Class690_Sub27.method17128((Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854),
					   1854318782);
	    }
	}
    }
    
    static final void method16027(Class534_Sub4 class534_sub4, boolean bool) {
	if (class534_sub4.aBool10407) {
	    if (class534_sub4.anInt10395 * 311151295 < 0
		|| (Class690_Sub29.method17139
		    (client.aClass512_11100.method8428(-1486655428),
		     class534_sub4.anInt10395 * 311151295,
		     665019215 * class534_sub4.anInt10401, (byte) -19))) {
		if (!bool)
		    Class567.method9567(-511427777 * class534_sub4.anInt10402,
					class534_sub4.anInt10405 * -1831835741,
					class534_sub4.anInt10397 * -1522052283,
					class534_sub4.anInt10400 * -1246362377,
					311151295 * class534_sub4.anInt10395,
					class534_sub4.anInt10396 * 352976243,
					665019215 * class534_sub4.anInt10401,
					-1, 0, (byte) 1);
		else
		    Class281_Sub1.method15657
			(-511427777 * class534_sub4.anInt10402,
			 class534_sub4.anInt10405 * -1831835741,
			 class534_sub4.anInt10397 * -1522052283,
			 class534_sub4.anInt10400 * -1246362377, null,
			 -1893352756);
		class534_sub4.method8892((byte) 1);
	    }
	} else if (class534_sub4.aBool10406
		   && class534_sub4.anInt10397 * -1522052283 >= 1
		   && class534_sub4.anInt10400 * -1246362377 >= 1
		   && (class534_sub4.anInt10397 * -1522052283
		       <= client.aClass512_11100.method8417(1777974644) - 2)
		   && (class534_sub4.anInt10400 * -1246362377
		       <= client.aClass512_11100.method8418(-1533611049) - 2)
		   && (class534_sub4.anInt10398 * -1375582077 < 0
		       || (Class690_Sub29.method17139
			   (client.aClass512_11100.method8428(-1486655428),
			    class534_sub4.anInt10398 * -1375582077,
			    -1945323711 * class534_sub4.anInt10404,
			    (byte) -51)))) {
	    if (!bool)
		Class567.method9567(class534_sub4.anInt10402 * -511427777,
				    class534_sub4.anInt10405 * -1831835741,
				    class534_sub4.anInt10397 * -1522052283,
				    class534_sub4.anInt10400 * -1246362377,
				    class534_sub4.anInt10398 * -1375582077,
				    class534_sub4.anInt10403 * 220446523,
				    -1945323711 * class534_sub4.anInt10404, -1,
				    0, (byte) 1);
	    else
		Class281_Sub1.method15657
		    (-511427777 * class534_sub4.anInt10402,
		     -1831835741 * class534_sub4.anInt10405,
		     -1522052283 * class534_sub4.anInt10397,
		     class534_sub4.anInt10400 * -1246362377,
		     class534_sub4.aClass596_10399, -723812348);
	    class534_sub4.aBool10406 = false;
	    if (!bool
		&& (class534_sub4.anInt10395 * 311151295
		    == -1375582077 * class534_sub4.anInt10398)
		&& 311151295 * class534_sub4.anInt10395 == -1)
		class534_sub4.method8892((byte) 1);
	    else if (!bool
		     && (-1375582077 * class534_sub4.anInt10398
			 == 311151295 * class534_sub4.anInt10395)
		     && (class534_sub4.anInt10403 * 220446523
			 == class534_sub4.anInt10396 * 352976243)
		     && (class534_sub4.anInt10404 * -1945323711
			 == 665019215 * class534_sub4.anInt10401))
		class534_sub4.method8892((byte) 1);
	}
    }
    
    public static void method16028(int i, int i_10_, int i_11_, int i_12_,
				   int i_13_, int i_14_, int i_15_) {
	Class534_Sub4 class534_sub4 = null;
	for (Class534_Sub4 class534_sub4_16_
		 = (Class534_Sub4) aClass700_10408.method14135((byte) -1);
	     class534_sub4_16_ != null;
	     class534_sub4_16_
		 = (Class534_Sub4) aClass700_10408.method14139(2017817432)) {
	    if (class534_sub4_16_.anInt10402 * -511427777 == i
		&& class534_sub4_16_.anInt10397 * -1522052283 == i_10_
		&& -1246362377 * class534_sub4_16_.anInt10400 == i_11_
		&& i_12_ == class534_sub4_16_.anInt10405 * -1831835741) {
		class534_sub4 = class534_sub4_16_;
		break;
	    }
	}
	if (class534_sub4 == null) {
	    class534_sub4 = new Class534_Sub4();
	    class534_sub4.anInt10402 = i * -453954369;
	    class534_sub4.anInt10405 = i_12_ * 2108621835;
	    class534_sub4.anInt10397 = i_10_ * 1402161037;
	    class534_sub4.anInt10400 = 2107243719 * i_11_;
	    if (i_10_ >= 0 && i_11_ >= 0
		&& i_10_ < client.aClass512_11100.method8417(882457023)
		&& i_11_ < client.aClass512_11100.method8418(-1533611049))
		Class229.method4207(class534_sub4, (byte) 0);
	    aClass700_10408.method14131(class534_sub4, (short) 789);
	}
	class534_sub4.anInt10398 = -1796323797 * i_13_;
	class534_sub4.anInt10404 = i_14_ * -804529983;
	class534_sub4.anInt10403 = -917323277 * i_15_;
	class534_sub4.aBool10406 = true;
	class534_sub4.aBool10407 = false;
    }
    
    static void method16029(int i, int i_17_, int i_18_, int i_19_, int i_20_,
			    int i_21_, Class596 class596) {
	Class534_Sub4 class534_sub4 = null;
	for (Class534_Sub4 class534_sub4_22_
		 = (Class534_Sub4) aClass700_10409.method14135((byte) -1);
	     class534_sub4_22_ != null;
	     class534_sub4_22_
		 = (Class534_Sub4) aClass700_10409.method14139(1400562465)) {
	    if (i == class534_sub4_22_.anInt10402 * -511427777
		&& -1522052283 * class534_sub4_22_.anInt10397 == i_17_
		&& -1246362377 * class534_sub4_22_.anInt10400 == i_18_
		&& i_19_ == -1831835741 * class534_sub4_22_.anInt10405) {
		class534_sub4 = class534_sub4_22_;
		break;
	    }
	}
	if (null == class534_sub4) {
	    class534_sub4 = new Class534_Sub4();
	    class534_sub4.anInt10402 = -453954369 * i;
	    class534_sub4.anInt10405 = 2108621835 * i_19_;
	    class534_sub4.anInt10397 = 1402161037 * i_17_;
	    class534_sub4.anInt10400 = i_18_ * 2107243719;
	    aClass700_10409.method14131(class534_sub4, (short) 789);
	}
	class534_sub4.anInt10398 = -1796323797 * i_20_;
	class534_sub4.anInt10404 = -804529983 * i_21_;
	class534_sub4.aClass596_10399 = class596;
	class534_sub4.aBool10406 = true;
	class534_sub4.aBool10407 = false;
    }
    
    static void method16030(int i, int i_23_, int i_24_, int i_25_, int i_26_,
			    int i_27_, Class596 class596) {
	Class534_Sub4 class534_sub4 = null;
	for (Class534_Sub4 class534_sub4_28_
		 = (Class534_Sub4) aClass700_10409.method14135((byte) -1);
	     class534_sub4_28_ != null;
	     class534_sub4_28_
		 = (Class534_Sub4) aClass700_10409.method14139(1555683116)) {
	    if (i == class534_sub4_28_.anInt10402 * -511427777
		&& -1522052283 * class534_sub4_28_.anInt10397 == i_23_
		&& -1246362377 * class534_sub4_28_.anInt10400 == i_24_
		&& i_25_ == -1831835741 * class534_sub4_28_.anInt10405) {
		class534_sub4 = class534_sub4_28_;
		break;
	    }
	}
	if (null == class534_sub4) {
	    class534_sub4 = new Class534_Sub4();
	    class534_sub4.anInt10402 = -453954369 * i;
	    class534_sub4.anInt10405 = 2108621835 * i_25_;
	    class534_sub4.anInt10397 = 1402161037 * i_23_;
	    class534_sub4.anInt10400 = i_24_ * 2107243719;
	    aClass700_10409.method14131(class534_sub4, (short) 789);
	}
	class534_sub4.anInt10398 = -1796323797 * i_26_;
	class534_sub4.anInt10404 = -804529983 * i_27_;
	class534_sub4.aClass596_10399 = class596;
	class534_sub4.aBool10406 = true;
	class534_sub4.aBool10407 = false;
    }
    
    static final void method16031() {
	for (Class534_Sub4 class534_sub4
		 = (Class534_Sub4) aClass700_10408.method14135((byte) -1);
	     class534_sub4 != null;
	     class534_sub4
		 = (Class534_Sub4) aClass700_10408.method14139(1389075367))
	    Class403.method6616(class534_sub4, false, 46213627);
	for (Class534_Sub4 class534_sub4
		 = (Class534_Sub4) aClass700_10409.method14135((byte) -1);
	     null != class534_sub4;
	     class534_sub4
		 = (Class534_Sub4) aClass700_10409.method14139(1093258078))
	    Class403.method6616(class534_sub4, true, -1024141840);
    }
    
    static final void method16032() {
	for (Class534_Sub4 class534_sub4
		 = (Class534_Sub4) aClass700_10408.method14135((byte) -1);
	     class534_sub4 != null;
	     class534_sub4
		 = (Class534_Sub4) aClass700_10408.method14139(1225857391))
	    Class403.method6616(class534_sub4, false, 1308119271);
	for (Class534_Sub4 class534_sub4
		 = (Class534_Sub4) aClass700_10409.method14135((byte) -1);
	     null != class534_sub4;
	     class534_sub4
		 = (Class534_Sub4) aClass700_10409.method14139(946461345))
	    Class403.method6616(class534_sub4, true, 1083226803);
    }
    
    static final void method16033(Class534_Sub4 class534_sub4, boolean bool) {
	if (class534_sub4.aBool10407) {
	    if (class534_sub4.anInt10395 * 311151295 < 0
		|| (Class690_Sub29.method17139
		    (client.aClass512_11100.method8428(-1486655428),
		     class534_sub4.anInt10395 * 311151295,
		     665019215 * class534_sub4.anInt10401, (byte) -23))) {
		if (!bool)
		    Class567.method9567(-511427777 * class534_sub4.anInt10402,
					class534_sub4.anInt10405 * -1831835741,
					class534_sub4.anInt10397 * -1522052283,
					class534_sub4.anInt10400 * -1246362377,
					311151295 * class534_sub4.anInt10395,
					class534_sub4.anInt10396 * 352976243,
					665019215 * class534_sub4.anInt10401,
					-1, 0, (byte) 1);
		else
		    Class281_Sub1.method15657
			(-511427777 * class534_sub4.anInt10402,
			 class534_sub4.anInt10405 * -1831835741,
			 class534_sub4.anInt10397 * -1522052283,
			 class534_sub4.anInt10400 * -1246362377, null,
			 -1036960602);
		class534_sub4.method8892((byte) 1);
	    }
	} else if (class534_sub4.aBool10406
		   && class534_sub4.anInt10397 * -1522052283 >= 1
		   && class534_sub4.anInt10400 * -1246362377 >= 1
		   && (class534_sub4.anInt10397 * -1522052283
		       <= client.aClass512_11100.method8417(-405877712) - 2)
		   && (class534_sub4.anInt10400 * -1246362377
		       <= client.aClass512_11100.method8418(-1533611049) - 2)
		   && (class534_sub4.anInt10398 * -1375582077 < 0
		       || (Class690_Sub29.method17139
			   (client.aClass512_11100.method8428(-1486655428),
			    class534_sub4.anInt10398 * -1375582077,
			    -1945323711 * class534_sub4.anInt10404,
			    (byte) -39)))) {
	    if (!bool)
		Class567.method9567(class534_sub4.anInt10402 * -511427777,
				    class534_sub4.anInt10405 * -1831835741,
				    class534_sub4.anInt10397 * -1522052283,
				    class534_sub4.anInt10400 * -1246362377,
				    class534_sub4.anInt10398 * -1375582077,
				    class534_sub4.anInt10403 * 220446523,
				    -1945323711 * class534_sub4.anInt10404, -1,
				    0, (byte) 1);
	    else
		Class281_Sub1.method15657
		    (-511427777 * class534_sub4.anInt10402,
		     -1831835741 * class534_sub4.anInt10405,
		     -1522052283 * class534_sub4.anInt10397,
		     class534_sub4.anInt10400 * -1246362377,
		     class534_sub4.aClass596_10399, 861909584);
	    class534_sub4.aBool10406 = false;
	    if (!bool
		&& (class534_sub4.anInt10395 * 311151295
		    == -1375582077 * class534_sub4.anInt10398)
		&& 311151295 * class534_sub4.anInt10395 == -1)
		class534_sub4.method8892((byte) 1);
	    else if (!bool
		     && (-1375582077 * class534_sub4.anInt10398
			 == 311151295 * class534_sub4.anInt10395)
		     && (class534_sub4.anInt10403 * 220446523
			 == class534_sub4.anInt10396 * 352976243)
		     && (class534_sub4.anInt10404 * -1945323711
			 == 665019215 * class534_sub4.anInt10401))
		class534_sub4.method8892((byte) 1);
	}
    }
    
    Class534_Sub4() {
	/* empty */
    }
    
    static final void method16034(int i, int i_29_, int i_30_, int i_31_,
				  int i_32_, int i_33_, int i_34_, int i_35_,
				  int i_36_) {
	if (i_30_ >= 1 && i_31_ >= 1
	    && i_30_ <= client.aClass512_11100.method8417(285084340) - 2
	    && i_31_ <= client.aClass512_11100.method8418(-1533611049) - 2) {
	    int i_37_ = i;
	    if (i_37_ < 3 && client.aClass512_11100.method8552((byte) 0)
				 .method7612(i_30_, i_31_, (byte) 0))
		i_37_++;
	    if (client.aClass512_11100.method8424((byte) 3) != null) {
		client.aClass512_11100.method8493(71465822).method15968
		    (Class254.aClass185_2683, i, i_29_, i_30_, i_31_,
		     -934051987);
		if (i_32_ >= 0) {
		    int i_38_
			= Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub10_10751.method16970((byte) 12);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 1, 903103467);
		    client.aClass512_11100.method8493(-849980386).method15942
			(Class254.aClass185_2683, i_37_, i, i_30_, i_31_,
			 i_32_, i_33_, i_34_, i_35_, i_36_, -547635356);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 i_38_, -1253929514);
		}
		Class690_Sub27.method17128((Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854),
					   1205594175);
	    }
	}
    }
    
    static final void method16035(int i, int i_39_, int i_40_, int i_41_,
				  int i_42_, int i_43_, int i_44_, int i_45_,
				  int i_46_) {
	if (i_40_ >= 1 && i_41_ >= 1
	    && i_40_ <= client.aClass512_11100.method8417(512536669) - 2
	    && i_41_ <= client.aClass512_11100.method8418(-1533611049) - 2) {
	    int i_47_ = i;
	    if (i_47_ < 3 && client.aClass512_11100.method8552((byte) 0)
				 .method7612(i_40_, i_41_, (byte) 0))
		i_47_++;
	    if (client.aClass512_11100.method8424((byte) 38) != null) {
		client.aClass512_11100.method8493(-858104423).method15968
		    (Class254.aClass185_2683, i, i_39_, i_40_, i_41_,
		     -2068950377);
		if (i_42_ >= 0) {
		    int i_48_
			= Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub10_10751.method16970((byte) 12);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 1, 438100097);
		    client.aClass512_11100.method8493(494466802).method15942
			(Class254.aClass185_2683, i_47_, i, i_40_, i_41_,
			 i_42_, i_43_, i_44_, i_45_, i_46_, 862708247);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 i_48_, -1137211504);
		}
		Class690_Sub27.method17128((Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854),
					   975141293);
	    }
	}
    }
    
    static final void method16036(int i, int i_49_, int i_50_, int i_51_,
				  int i_52_, int i_53_, int i_54_, int i_55_,
				  int i_56_) {
	if (i_50_ >= 1 && i_51_ >= 1
	    && i_50_ <= client.aClass512_11100.method8417(-168458183) - 2
	    && i_51_ <= client.aClass512_11100.method8418(-1533611049) - 2) {
	    int i_57_ = i;
	    if (i_57_ < 3 && client.aClass512_11100.method8552((byte) 0)
				 .method7612(i_50_, i_51_, (byte) 0))
		i_57_++;
	    if (client.aClass512_11100.method8424((byte) 110) != null) {
		client.aClass512_11100.method8493(-216738953).method15968
		    (Class254.aClass185_2683, i, i_49_, i_50_, i_51_,
		     906797057);
		if (i_52_ >= 0) {
		    int i_58_
			= Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub10_10751.method16970((byte) 12);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 1, -939711099);
		    client.aClass512_11100.method8493(1120815786).method15942
			(Class254.aClass185_2683, i_57_, i, i_50_, i_51_,
			 i_52_, i_53_, i_54_, i_55_, i_56_, -944567149);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub10_10751),
			 i_58_, -973823271);
		}
		Class690_Sub27.method17128((Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854),
					   1317515753);
	    }
	}
    }
    
    static final void method16037(Class534_Sub4 class534_sub4, boolean bool) {
	if (class534_sub4.aBool10407) {
	    if (class534_sub4.anInt10395 * 311151295 < 0
		|| (Class690_Sub29.method17139
		    (client.aClass512_11100.method8428(-1486655428),
		     class534_sub4.anInt10395 * 311151295,
		     665019215 * class534_sub4.anInt10401, (byte) -120))) {
		if (!bool)
		    Class567.method9567(-511427777 * class534_sub4.anInt10402,
					class534_sub4.anInt10405 * -1831835741,
					class534_sub4.anInt10397 * -1522052283,
					class534_sub4.anInt10400 * -1246362377,
					311151295 * class534_sub4.anInt10395,
					class534_sub4.anInt10396 * 352976243,
					665019215 * class534_sub4.anInt10401,
					-1, 0, (byte) 1);
		else
		    Class281_Sub1.method15657
			(-511427777 * class534_sub4.anInt10402,
			 class534_sub4.anInt10405 * -1831835741,
			 class534_sub4.anInt10397 * -1522052283,
			 class534_sub4.anInt10400 * -1246362377, null,
			 1437576014);
		class534_sub4.method8892((byte) 1);
	    }
	} else if (class534_sub4.aBool10406
		   && class534_sub4.anInt10397 * -1522052283 >= 1
		   && class534_sub4.anInt10400 * -1246362377 >= 1
		   && (class534_sub4.anInt10397 * -1522052283
		       <= client.aClass512_11100.method8417(1173763614) - 2)
		   && (class534_sub4.anInt10400 * -1246362377
		       <= client.aClass512_11100.method8418(-1533611049) - 2)
		   && (class534_sub4.anInt10398 * -1375582077 < 0
		       || (Class690_Sub29.method17139
			   (client.aClass512_11100.method8428(-1486655428),
			    class534_sub4.anInt10398 * -1375582077,
			    -1945323711 * class534_sub4.anInt10404,
			    (byte) -55)))) {
	    if (!bool)
		Class567.method9567(class534_sub4.anInt10402 * -511427777,
				    class534_sub4.anInt10405 * -1831835741,
				    class534_sub4.anInt10397 * -1522052283,
				    class534_sub4.anInt10400 * -1246362377,
				    class534_sub4.anInt10398 * -1375582077,
				    class534_sub4.anInt10403 * 220446523,
				    -1945323711 * class534_sub4.anInt10404, -1,
				    0, (byte) 1);
	    else
		Class281_Sub1.method15657
		    (-511427777 * class534_sub4.anInt10402,
		     -1831835741 * class534_sub4.anInt10405,
		     -1522052283 * class534_sub4.anInt10397,
		     class534_sub4.anInt10400 * -1246362377,
		     class534_sub4.aClass596_10399, -298335530);
	    class534_sub4.aBool10406 = false;
	    if (!bool
		&& (class534_sub4.anInt10395 * 311151295
		    == -1375582077 * class534_sub4.anInt10398)
		&& 311151295 * class534_sub4.anInt10395 == -1)
		class534_sub4.method8892((byte) 1);
	    else if (!bool
		     && (-1375582077 * class534_sub4.anInt10398
			 == 311151295 * class534_sub4.anInt10395)
		     && (class534_sub4.anInt10403 * 220446523
			 == class534_sub4.anInt10396 * 352976243)
		     && (class534_sub4.anInt10404 * -1945323711
			 == 665019215 * class534_sub4.anInt10401))
		class534_sub4.method8892((byte) 1);
	}
    }
    
    static final void method16038(Class669 class669, byte i) {
	if (null != client.aString11300)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -217094943 * Class455.anInt4963;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
    }
    
    static boolean method16039(int i, int i_59_) {
	if (9 == i || 10 == i || i == 11 || 12 == i || 13 == i || 1003 == i)
	    return true;
	if (i == 8)
	    return true;
	return false;
    }
}
