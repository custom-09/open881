/* Class534_Sub42_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Point;

public class Class534_Sub42_Sub1 extends Class534_Sub42
{
    static final int anInt11826 = 160;
    static final int anInt11827 = 161;
    static final int anInt11828 = 162;
    static final int anInt11829 = 174;
    static final int anInt11830 = 164;
    static final int anInt11831 = 165;
    static final int anInt11832 = 166;
    static final int anInt11833 = 163;
    static final int anInt11834 = 168;
    static final int anInt11835 = 169;
    static final int anInt11836 = 522;
    static final int anInt11837 = 171;
    static final int anInt11838 = 172;
    static final int anInt11839 = 173;
    static final int anInt11840 = 521;
    static final int anInt11841 = 512;
    static final int anInt11842 = 513;
    static final int anInt11843 = 515;
    static final int anInt11844 = 514;
    static final int anInt11845 = 516;
    static final int anInt11846 = 517;
    static final int anInt11847 = 518;
    static final int anInt11848 = 519;
    static final int anInt11849 = 520;
    int anInt11850;
    static final int anInt11851 = 170;
    static final int anInt11852 = 525;
    static final int anInt11853 = 672;
    static final int anInt11854 = 674;
    static Class534_Sub42_Sub1[] aClass534_Sub42_Sub1Array11855
	= new Class534_Sub42_Sub1[0];
    static int anInt11856;
    int anInt11857;
    int anInt11858;
    long aLong11859;
    int anInt11860;
    static final int anInt11861 = 167;
    
    public void method16812() {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    if (1045872731 * anInt11856 < Class4.anInt39 * 1605015273 - 1)
		aClass534_Sub42_Sub1Array11855
		    [(anInt11856 += 1321758675) * 1045872731 - 1]
		    = this;
	}
    }
    
    public void method16803(byte i) {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    if (1045872731 * anInt11856 < Class4.anInt39 * 1605015273 - 1)
		aClass534_Sub42_Sub1Array11855
		    [(anInt11856 += 1321758675) * 1045872731 - 1]
		    = this;
	}
    }
    
    public static Class534_Sub42_Sub1 method18407(int i, int i_0_, int i_1_,
						  int i_2_) {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    Class534_Sub42_Sub1 class534_sub42_sub1;
	    if (0 == anInt11856 * 1045872731)
		class534_sub42_sub1 = new Class534_Sub42_Sub1();
	    else
		class534_sub42_sub1
		    = (aClass534_Sub42_Sub1Array11855
		       [(anInt11856 -= 1321758675) * 1045872731]);
	    class534_sub42_sub1.anInt11860 = -466592867 * i;
	    class534_sub42_sub1.anInt11850 = i_0_ * -1991833173;
	    class534_sub42_sub1.anInt11857 = i_1_ * -934260093;
	    class534_sub42_sub1.anInt11858 = i_2_ * -220200763;
	    class534_sub42_sub1.aLong11859
		= Class250.method4604((byte) -41) * 747553093241954833L;
	    Class534_Sub42_Sub1 class534_sub42_sub1_3_ = class534_sub42_sub1;
	    return class534_sub42_sub1_3_;
	}
    }
    
    public int method18408(int i) {
	return anInt11857 * -41153493;
    }
    
    public int method16806() {
	return 760981763 * anInt11850;
    }
    
    public int method16798(byte i) {
	switch (anInt11857 * -41153493) {
	case 167:
	case 169:
	case 519:
	case 521:
	    return 1;
	case 161:
	case 163:
	case 513:
	case 515:
	    return 0;
	case 165:
	case 517:
	    return 5;
	case 168:
	case 520:
	    return 4;
	case 162:
	case 514:
	    return 3;
	case 164:
	case 166:
	case 516:
	case 518:
	    return 2;
	case 160:
	case 512:
	    return -1;
	case 170:
	case 522:
	    return 6;
	default:
	    return -2;
	}
    }
    
    public void method18409(Point point, int i) {
	anInt11860 -= point.x * -466592867;
	anInt11850 -= point.y * -1991833173;
    }
    
    public long method16802(int i) {
	return 8600395610999017201L * aLong11859;
    }
    
    public int method16799(byte i) {
	return anInt11860 * -570948939;
    }
    
    public int method16800(int i) {
	return 760981763 * anInt11850;
    }
    
    public int method16808() {
	return anInt11860 * -570948939;
    }
    
    public int method18410() {
	return anInt11857 * -41153493;
    }
    
    public int method16805() {
	switch (anInt11857 * -41153493) {
	case 167:
	case 169:
	case 519:
	case 521:
	    return 1;
	case 161:
	case 163:
	case 513:
	case 515:
	    return 0;
	case 165:
	case 517:
	    return 5;
	case 168:
	case 520:
	    return 4;
	case 162:
	case 514:
	    return 3;
	case 164:
	case 166:
	case 516:
	case 518:
	    return 2;
	case 160:
	case 512:
	    return -1;
	case 170:
	case 522:
	    return 6;
	default:
	    return -2;
	}
    }
    
    public int method18411() {
	return anInt11857 * -41153493;
    }
    
    public int method16807() {
	return 760981763 * anInt11850;
    }
    
    public int method16809() {
	return 760981763 * anInt11850;
    }
    
    public long method16813() {
	return 8600395610999017201L * aLong11859;
    }
    
    public int method16818() {
	switch (-41153493 * anInt11857) {
	case 160:
	case 512:
	    return 0;
	default:
	    return 1;
	case 163:
	case 166:
	case 169:
	case 173:
	case 515:
	case 518:
	case 521:
	case 525:
	    return 2;
	}
    }
    
    public void method16811() {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    if (1045872731 * anInt11856 < Class4.anInt39 * 1605015273 - 1)
		aClass534_Sub42_Sub1Array11855
		    [(anInt11856 += 1321758675) * 1045872731 - 1]
		    = this;
	}
    }
    
    public int method16804() {
	switch (anInt11857 * -41153493) {
	case 167:
	case 169:
	case 519:
	case 521:
	    return 1;
	case 161:
	case 163:
	case 513:
	case 515:
	    return 0;
	case 165:
	case 517:
	    return 5;
	case 168:
	case 520:
	    return 4;
	case 162:
	case 514:
	    return 3;
	case 164:
	case 166:
	case 516:
	case 518:
	    return 2;
	case 160:
	case 512:
	    return -1;
	case 170:
	case 522:
	    return 6;
	default:
	    return -2;
	}
    }
    
    public int method18412() {
	return 1731427853 * anInt11858;
    }
    
    public boolean method18413(int i) {
	switch (anInt11857 * -41153493) {
	default:
	    return false;
	case 160:
	case 161:
	case 162:
	case 163:
	case 164:
	case 165:
	case 166:
	case 167:
	case 168:
	case 169:
	case 170:
	case 171:
	case 172:
	case 173:
	case 174:
	case 672:
	case 674:
	    return true;
	}
    }
    
    Class534_Sub42_Sub1() {
	/* empty */
    }
    
    public static void method18414(int i) {
	Class4.anInt39 = 48341337 * i;
	aClass534_Sub42_Sub1Array11855 = new Class534_Sub42_Sub1[i];
	anInt11856 = 0;
    }
    
    public static Class534_Sub42_Sub1 method18415(int i, int i_4_, int i_5_,
						  int i_6_) {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    Class534_Sub42_Sub1 class534_sub42_sub1;
	    if (0 == anInt11856 * 1045872731)
		class534_sub42_sub1 = new Class534_Sub42_Sub1();
	    else
		class534_sub42_sub1
		    = (aClass534_Sub42_Sub1Array11855
		       [(anInt11856 -= 1321758675) * 1045872731]);
	    class534_sub42_sub1.anInt11860 = -466592867 * i;
	    class534_sub42_sub1.anInt11850 = i_4_ * -1991833173;
	    class534_sub42_sub1.anInt11857 = i_5_ * -934260093;
	    class534_sub42_sub1.anInt11858 = i_6_ * -220200763;
	    class534_sub42_sub1.aLong11859
		= Class250.method4604((byte) -40) * 747553093241954833L;
	    Class534_Sub42_Sub1 class534_sub42_sub1_7_ = class534_sub42_sub1;
	    return class534_sub42_sub1_7_;
	}
    }
    
    public int method16817(int i) {
	switch (-41153493 * anInt11857) {
	case 160:
	case 512:
	    return 0;
	default:
	    return 1;
	case 163:
	case 166:
	case 169:
	case 173:
	case 515:
	case 518:
	case 521:
	case 525:
	    return 2;
	}
    }
    
    public void method16810() {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    if (1045872731 * anInt11856 < Class4.anInt39 * 1605015273 - 1)
		aClass534_Sub42_Sub1Array11855
		    [(anInt11856 += 1321758675) * 1045872731 - 1]
		    = this;
	}
    }
    
    public int method18416() {
	return anInt11857 * -41153493;
    }
    
    public int method18417(int i) {
	return 1731427853 * anInt11858;
    }
    
    public int method18418() {
	return anInt11857 * -41153493;
    }
    
    public static Class534_Sub42_Sub1 method18419(int i, int i_8_, int i_9_,
						  int i_10_) {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    Class534_Sub42_Sub1 class534_sub42_sub1;
	    if (0 == anInt11856 * 1045872731)
		class534_sub42_sub1 = new Class534_Sub42_Sub1();
	    else
		class534_sub42_sub1
		    = (aClass534_Sub42_Sub1Array11855
		       [(anInt11856 -= 1321758675) * 1045872731]);
	    class534_sub42_sub1.anInt11860 = -466592867 * i;
	    class534_sub42_sub1.anInt11850 = i_8_ * -1991833173;
	    class534_sub42_sub1.anInt11857 = i_9_ * -934260093;
	    class534_sub42_sub1.anInt11858 = i_10_ * -220200763;
	    class534_sub42_sub1.aLong11859
		= Class250.method4604((byte) -79) * 747553093241954833L;
	    Class534_Sub42_Sub1 class534_sub42_sub1_11_ = class534_sub42_sub1;
	    return class534_sub42_sub1_11_;
	}
    }
    
    public void method16801() {
	synchronized (aClass534_Sub42_Sub1Array11855) {
	    if (1045872731 * anInt11856 < Class4.anInt39 * 1605015273 - 1)
		aClass534_Sub42_Sub1Array11855
		    [(anInt11856 += 1321758675) * 1045872731 - 1]
		    = this;
	}
    }
    
    public int method18420() {
	return anInt11857 * -41153493;
    }
    
    public void method18421(Point point) {
	anInt11860 -= point.x * -466592867;
	anInt11850 -= point.y * -1991833173;
    }
    
    public static void method18422(int i) {
	Class4.anInt39 = 48341337 * i;
	aClass534_Sub42_Sub1Array11855 = new Class534_Sub42_Sub1[i];
	anInt11856 = 0;
    }
    
    static void method18423(int i, int i_12_, Class247 class247,
			    Class145 class145, int i_13_, int i_14_,
			    int i_15_) {
	int i_16_ = -1843550713 * Class108.anInt1321;
	int[] is = Class108.anIntArray1322;
	for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
	    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		= client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[is[i_17_]];
	    if (null != class654_sub1_sub5_sub1_sub2
		&& class654_sub1_sub5_sub1_sub2.method18876(-441117355)
		&& !class654_sub1_sub5_sub1_sub2.aClass618_12216
			.method10242((byte) -39)
		&& (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
		    != class654_sub1_sub5_sub1_sub2)
		&& (class654_sub1_sub5_sub1_sub2.aByte10854
		    == (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			.aByte10854))) {
		Class438 class438 = (class654_sub1_sub5_sub1_sub2.method10807()
				     .aClass438_4885);
		int i_18_ = (int) class438.aFloat4864 / 128 - i / 128;
		int i_19_ = (int) class438.aFloat4865 / 128 - i_12_ / 128;
		boolean bool = false;
		for (int i_20_ = 0; i_20_ < -1979292205 * client.anInt11324;
		     i_20_++) {
		    Class28 class28 = client.aClass28Array11327[i_20_];
		    if (class654_sub1_sub5_sub1_sub2.aString12211
			    .equals(class28.aString257)
			&& -721928209 * class28.anInt251 != 0) {
			bool = true;
			break;
		    }
		}
		boolean bool_21_ = false;
		for (int i_22_ = 0; i_22_ < -217094943 * Class455.anInt4963;
		     i_22_++) {
		    if (class654_sub1_sub5_sub1_sub2.aString12211.equals
			(Class168.aClass98Array1792[i_22_].aString1162)) {
			bool_21_ = true;
			break;
		    }
		}
		boolean bool_23_ = false;
		if ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.anInt12224
		     * 676699829) != 0
		    && class654_sub1_sub5_sub1_sub2.anInt12224 * 676699829 != 0
		    && ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.anInt12224
			 * 676699829)
			== (class654_sub1_sub5_sub1_sub2.anInt12224
			    * 676699829)))
		    bool_23_ = true;
		if (null != class654_sub1_sub5_sub1_sub2.aClass631_12226
		    && 1568742735 * (class654_sub1_sub5_sub1_sub2
				     .aClass631_12226.anInt8212) != -1
		    && ((Class307) (Class578.aClass44_Sub3_7743.method91
				    ((class654_sub1_sub5_sub1_sub2
				      .aClass631_12226.anInt8212) * 1568742735,
				     1283547173))).aBool3348)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[1],
					1313730702);
		else if (class654_sub1_sub5_sub1_sub2.aClass252_12232
			 == Class252.aClass252_2655)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[8],
					1313730702);
		else if (Class252.aClass252_2656
			 == class654_sub1_sub5_sub1_sub2.aClass252_12232)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[6],
					1313730702);
		else if (bool_23_)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[4],
					1313730702);
		else if (class654_sub1_sub5_sub1_sub2.aBool12234)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[7],
					1313730702);
		else if (bool)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[3],
					1313730702);
		else if (bool_21_)
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[5],
					1313730702);
		else
		    Class246.method4509(class247, class145, i_13_, i_14_,
					i_18_, i_19_,
					Class67.aClass163Array721[2],
					1313730702);
	    }
	}
    }
}
