/* Class534_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub8 extends Class534
{
    int anInt10419;
    int anInt10420;
    int anInt10421;
    int anInt10422;
    int anInt10423;
    boolean aBool10424;
    static Class534_Sub40 aClass534_Sub40_10425;
    
    Class534_Sub8(int i, int i_0_, int i_1_, int i_2_, int i_3_,
		  boolean bool) {
	anInt10420 = 1082972827 * i;
	anInt10419 = -1058746657 * i_0_;
	anInt10423 = i_1_ * -261912427;
	anInt10422 = 1051515433 * i_2_;
	anInt10421 = i_3_ * -1834723261;
	aBool10424 = bool;
    }
    
    static final void method16068(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class114.method2108(class247, class243, class669, (byte) 8);
    }
}
