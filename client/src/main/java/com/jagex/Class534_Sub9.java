/* Class534_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class534_Sub9 extends Class534
{
    boolean aBool10426;
    Class185_Sub3 aClass185_Sub3_10427;
    
    abstract boolean method16069();
    
    abstract boolean method16070();
    
    abstract boolean method16071();
    
    abstract void method16072(int i, int i_0_);
    
    abstract void method16073(int i);
    
    abstract boolean method16074();
    
    abstract void method16075(int i, Class141_Sub2 class141_sub2,
			      Class141_Sub2 class141_sub2_1_, int i_2_,
			      int i_3_);
    
    int method16076() {
	return 1;
    }
    
    abstract int method16077();
    
    boolean method16078() {
	return false;
    }
    
    Class173 method16079() {
	return Class173.aClass173_1830;
    }
    
    abstract int method16080();
    
    abstract boolean method16081();
    
    abstract boolean method16082();
    
    abstract boolean method16083();
    
    abstract boolean method16084();
    
    Class534_Sub9(Class185_Sub3 class185_sub3) {
	aClass185_Sub3_10427 = class185_sub3;
    }
    
    abstract boolean method16085();
    
    abstract boolean method16086();
    
    int method16087() {
	return 1;
    }
    
    abstract boolean method16088();
    
    abstract void method16089();
    
    abstract void method16090();
    
    abstract void method16091(int i, int i_4_);
    
    Class173 method16092() {
	return Class173.aClass173_1830;
    }
    
    abstract void method16093(int i, int i_5_);
    
    abstract void method16094(int i, int i_6_);
    
    abstract void method16095(int i);
    
    Class173 method16096() {
	return Class173.aClass173_1830;
    }
    
    abstract int method16097();
    
    abstract void method16098(int i, int i_7_);
    
    abstract int method16099();
    
    abstract int method16100();
    
    boolean method16101() {
	return false;
    }
    
    boolean method16102() {
	return false;
    }
    
    boolean method16103() {
	return false;
    }
    
    abstract void method16104();
    
    abstract void method16105(int i, Class141_Sub2 class141_sub2,
			      Class141_Sub2 class141_sub2_8_, int i_9_,
			      int i_10_);
    
    Class173 method16106() {
	return Class173.aClass173_1830;
    }
    
    Class173 method16107() {
	return Class173.aClass173_1830;
    }
    
    boolean method16108() {
	return aBool10426;
    }
    
    boolean method16109() {
	return aBool10426;
    }
    
    abstract boolean method16110();
    
    abstract boolean method16111();
    
    abstract void method16112(int i, Class141_Sub2 class141_sub2,
			      Class141_Sub2 class141_sub2_11_, int i_12_,
			      int i_13_);
    
    abstract boolean method16113();
    
    abstract void method16114(int i, Class141_Sub2 class141_sub2,
			      Class141_Sub2 class141_sub2_14_, int i_15_,
			      int i_16_);
}
