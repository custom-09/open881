/* Class537 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class537
{
    Class544 this$0;
    
    Class537(Class544 class544) {
	this$0 = class544;
    }
    
    static final void method8909(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, 1298457526);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class162.method2642(class247, class243, class669, -632751994);
    }
    
    static void method8910(int i) {
	if (-1850530127 * client.anInt11039 == 1)
	    Class513.method8582(648829258);
	client.aClass100_11094.method1866((byte) -89);
	method8913(-1739893072);
	Class534_Sub36.aBool10800 = true;
	Class653.method10802((byte) 68);
	for (int i_1_ = 0; i_1_ < client.aClass99Array11053.length; i_1_++)
	    client.aClass99Array11053[i_1_] = null;
	client.aBool11218 = false;
	client.anInt11042 = ((int) (Math.random() * 100.0) - 50) * 249179819;
	client.anInt11125 = ((int) (Math.random() * 110.0) - 55) * 1002553817;
	client.anInt11127 = ((int) (Math.random() * 80.0) - 40) * -503740369;
	client.anInt11189 = ((int) (Math.random() * 120.0) - 60) * -1536818227;
	client.anInt11022 = ((int) (Math.random() * 30.0) - 20) * -1403451729;
	client.aFloat11140
	    = (float) ((int) (Math.random() * 160.0) - 80 & 0x3fff);
	Class599.method9947(-1962955800);
	for (int i_2_ = 0; i_2_ < 2048; i_2_++)
	    client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[i_2_] = null;
	Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419 = null;
	client.anInt11321 = 0;
	client.anInt11148 = 0;
	client.aClass9_11331.method578((byte) 45);
	client.aClass700_11210.method14152(-1748164253);
	client.aClass9_11322.method578((byte) 58);
	client.aClass709_11212.method14283(-1107967299);
	client.aClass9_11209.method578((byte) -94);
	Class534_Sub4.aClass700_10408 = new Class700();
	Class534_Sub4.aClass700_10409 = new Class700();
	Class200_Sub8.anInt9909 = 0;
	Class200_Sub9.anInt9916 = 0;
	Class453_Sub2.anInt10347 = 0;
	Class42.anInt321 = 0;
	Class110.anInt1345 = 0;
	Class652.anInt8484 = 0;
	Class609.anInt8007 = 0;
	Class341.anInt3554 = 0;
	Class77.anInt820 = 0;
	Class221.anInt2311 = 0;
	if (-1 != -993629849 * client.anInt11185)
	    Class219.method4143(client.anInt11185 * -993629849, -549422413);
	for (Class534_Sub37 class534_sub37
		 = ((Class534_Sub37)
		    client.aClass9_11224.method583(-2138315608));
	     null != class534_sub37;
	     class534_sub37 = ((Class534_Sub37)
			       client.aClass9_11224.method584((byte) -114))) {
	    if (!class534_sub37.method8889(917485312)) {
		class534_sub37 = ((Class534_Sub37)
				  client.aClass9_11224.method583(-1911851069));
		if (class534_sub37 == null)
		    break;
	    }
	    Class534_Sub41.method16766(class534_sub37, true, false,
				       -501970604);
	}
	client.anInt11185 = 1328962985;
	client.aClass9_11224 = new Class9(8);
	Class701.method14213(1748798423);
	client.aClass247_11119 = null;
	for (int i_3_ = 0; i_3_ < 8; i_3_++) {
	    client.aStringArray11206[i_3_] = null;
	    client.aBoolArray11207[i_3_] = false;
	    client.anIntArray11205[i_3_] = -1;
	}
	Class686.method13968(-1171256250);
	client.aBool11352 = true;
	for (int i_4_ = 0; i_4_ < 108; i_4_++)
	    client.aBoolArray11180[i_4_] = true;
	for (int i_5_ = 0; i_5_ < 3; i_5_++) {
	    for (int i_6_ = 0; i_6_ < 8; i_6_++)
		client.aClass492ArrayArray11027[i_5_][i_6_] = new Class492();
	}
	client.aClass512_11100.method8501((byte) -103).method10157(2109698207);
	client.aBool11147 = true;
	Class602.aShortArray7886 = Class307.aShortArray3285
	    = Class15.aShortArray151 = new short[256];
	Class106.aString1311
	    = Class58.aClass58_593.method1245(Class539.aClass672_7171,
					      (byte) -6);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759,
	     Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758
		 .method17035((byte) 2),
	     -65118950);
	client.anInt11144 = 0;
	Class489.method8005(-141541576);
	Class52.aClass641_436 = null;
	Class36.aLong288 = 0L;
    }
    
    static final boolean method8911(Class568[][][] class568s, int i, int i_7_,
				    int i_8_, boolean bool, byte i_9_) {
	byte[][][] is = client.aClass512_11100.method8411((byte) -107);
	byte i_10_
	    = bool ? (byte) 1 : (byte) (1373322351 * client.anInt11161 & 0xff);
	if (is[Class674.anInt8633 * -878424575][i_7_][i_8_] == i_10_)
	    return false;
	Class468 class468 = client.aClass512_11100.method8552((byte) 0);
	if (0 == ((class468.aByteArrayArrayArray5145
		   [Class674.anInt8633 * -878424575][i_7_][i_8_])
		  & 0x4))
	    return false;
	int i_11_ = 0;
	int i_12_ = 0;
	client.anIntArray11120[i_11_] = i_7_;
	client.anIntArray11121[i_11_++] = i_8_;
	is[-878424575 * Class674.anInt8633][i_7_][i_8_] = i_10_;
	while (i_11_ != i_12_) {
	    int i_13_ = client.anIntArray11120[i_12_] & 0xffff;
	    int i_14_ = client.anIntArray11120[i_12_] >> 16 & 0xff;
	    int i_15_ = client.anIntArray11120[i_12_] >> 24 & 0xff;
	    int i_16_ = client.anIntArray11121[i_12_] & 0xffff;
	    int i_17_ = client.anIntArray11121[i_12_] >> 16 & 0xff;
	    i_12_ = 1 + i_12_ & 0xfff;
	    boolean bool_18_ = false;
	    if (0 == ((class468.aByteArrayArrayArray5145
		       [-878424575 * Class674.anInt8633][i_13_][i_16_])
		      & 0x4))
		bool_18_ = true;
	    boolean bool_19_ = false;
	    if (null != class568s) {
		int i_20_ = -878424575 * Class674.anInt8633 + 1;
	    while_60_:
		for (/**/; i_20_ <= 3; i_20_++) {
		    if (null != class568s[i_20_]
			&& 0 == ((class468.aByteArrayArrayArray5145[i_20_]
				  [i_13_][i_16_])
				 & 0x8)) {
			if (bool_18_
			    && null != class568s[i_20_][i_13_][i_16_]) {
			    if (null != (class568s[i_20_][i_13_][i_16_]
					 .aClass654_Sub1_Sub1_7598)) {
				int i_21_
				    = Class479.method7918(i_14_, 1108739759);
				if (i_21_ == (class568s[i_20_][i_13_][i_16_]
					      .aClass654_Sub1_Sub1_7598
					      .aShort11823)
				    || ((class568s[i_20_][i_13_][i_16_]
					 .aClass654_Sub1_Sub1_7597) != null
					&& (class568s[i_20_][i_13_][i_16_]
					    .aClass654_Sub1_Sub1_7597
					    .aShort11823) == i_21_))
				    continue;
				if (i_15_ != 0) {
				    int i_22_ = Class479.method7918(i_15_,
								    315520502);
				    if (i_22_ == (class568s[i_20_][i_13_]
						  [i_16_]
						  .aClass654_Sub1_Sub1_7598
						  .aShort11823)
					|| (null != (class568s[i_20_][i_13_]
						     [i_16_]
						     .aClass654_Sub1_Sub1_7597)
					    && (i_22_
						== (class568s[i_20_][i_13_]
						    [i_16_]
						    .aClass654_Sub1_Sub1_7597
						    .aShort11823))))
					continue;
				}
				if (i_17_ != 0) {
				    int i_23_ = Class479.method7918(i_17_,
								    909264703);
				    if ((class568s[i_20_][i_13_][i_16_]
					 .aClass654_Sub1_Sub1_7598
					 .aShort11823) == i_23_
					|| (null != (class568s[i_20_][i_13_]
						     [i_16_]
						     .aClass654_Sub1_Sub1_7597)
					    && (class568s[i_20_][i_13_][i_16_]
						.aClass654_Sub1_Sub1_7597
						.aShort11823) == i_23_))
					continue;
				}
			    }
			    Class568 class568 = class568s[i_20_][i_13_][i_16_];
			    if (null != class568.aClass559_7604) {
				for (Class559 class559
					 = class568.aClass559_7604;
				     class559 != null;
				     class559 = class559.aClass559_7497) {
				    Class654_Sub1_Sub5 class654_sub1_sub5
					= class559.aClass654_Sub1_Sub5_7500;
				    if (class654_sub1_sub5
					instanceof Interface62) {
					Interface62 interface62
					    = (Interface62) class654_sub1_sub5;
					int i_24_
					    = interface62
						  .method409(-1876302151);
					int i_25_
					    = interface62
						  .method410(-1351449161);
					if (i_24_ == 21)
					    i_24_ = 19;
					int i_26_ = i_24_ | i_25_ << 6;
					if (i_14_ == i_26_
					    || i_15_ != 0 && i_15_ == i_26_
					    || 0 != i_17_ && i_26_ == i_17_)
					    continue while_60_;
				    }
				}
			    }
			}
			Class568 class568 = class568s[i_20_][i_13_][i_16_];
			if (class568 != null
			    && class568.aClass559_7604 != null) {
			    for (Class559 class559 = class568.aClass559_7604;
				 class559 != null;
				 class559 = class559.aClass559_7497) {
				Class654_Sub1_Sub5 class654_sub1_sub5
				    = class559.aClass654_Sub1_Sub5_7500;
				if ((class654_sub1_sub5.aShort11896
				     != class654_sub1_sub5.aShort11900)
				    || (class654_sub1_sub5.aShort11901
					!= class654_sub1_sub5.aShort11898)) {
				    short i_27_
					= class654_sub1_sub5.aShort11900;
				    short i_28_
					= class654_sub1_sub5.aShort11896;
				    short i_29_
					= class654_sub1_sub5.aShort11901;
				    short i_30_
					= class654_sub1_sub5.aShort11898;
				    int i_31_
					= Math.max(0,
						   Math.min(i_27_,
							    (is[i_20_].length
							     - 1)));
				    int i_32_
					= Math.max(0,
						   Math.min(i_29_,
							    (is[i_20_]
							     [0]).length - 1));
				    int i_33_
					= Math.max(0,
						   Math.min(i_28_,
							    (is[i_20_].length
							     - 1)));
				    int i_34_
					= Math.max(0,
						   Math.min(i_30_,
							    (is[i_20_]
							     [0]).length - 1));
				    for (/**/; i_31_ <= i_33_; i_31_++) {
					for (/**/; i_32_ <= i_34_; i_32_++)
					    is[i_20_][i_31_][i_32_] = i_10_;
				    }
				}
			    }
			}
			is[i_20_][i_13_][i_16_] = i_10_;
			bool_19_ = true;
		    }
		}
	    }
	    if (bool_19_) {
		int i_35_ = client.aClass512_11100.method8424((byte) 104)
				.aClass151Array7432
				[1 + -878424575 * Class674.anInt8633]
				.method2491(i_13_, i_16_, 904885192);
		if (client.anIntArray11177[i] < i_35_)
		    client.anIntArray11177[i] = i_35_;
		int i_36_ = i_13_ << 9;
		int i_37_ = i_16_ << 9;
		if (client.anIntArray11123[i] > i_36_)
		    client.anIntArray11123[i] = i_36_;
		else if (client.anIntArray11179[i] < i_36_)
		    client.anIntArray11179[i] = i_36_;
		if (client.anIntArray11181[i] > i_37_)
		    client.anIntArray11181[i] = i_37_;
		else if (client.anIntArray11135[i] < i_37_)
		    client.anIntArray11135[i] = i_37_;
	    }
	    if (!bool_18_) {
		if (i_13_ >= 1 && i_10_ != (is[-878424575 * Class674.anInt8633]
					    [i_13_ - 1][i_16_])) {
		    client.anIntArray11120[i_11_]
			= i_13_ - 1 | 0x120000 | ~0x2cffffff;
		    client.anIntArray11121[i_11_] = i_16_ | 0x130000;
		    i_11_ = i_11_ + 1 & 0xfff;
		    is[-878424575 * Class674.anInt8633][i_13_ - 1][i_16_]
			= i_10_;
		}
		if (++i_16_ < client.aClass512_11100.method8418(-1533611049)) {
		    if (i_13_ - 1 >= 0
			&& (is[Class674.anInt8633 * -878424575][i_13_ - 1]
			    [i_16_]) != i_10_
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [Class674.anInt8633 * -878424575][i_13_]
				  [i_16_])
				 & 0x4)
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [-878424575 * Class674.anInt8633][i_13_ - 1]
				  [i_16_ - 1])
				 & 0x4)) {
			client.anIntArray11120[i_11_]
			    = i_13_ - 1 | 0x120000 | 0x52000000;
			client.anIntArray11121[i_11_] = i_16_ | 0x130000;
			i_11_ = 1 + i_11_ & 0xfff;
			is[Class674.anInt8633 * -878424575][i_13_ - 1][i_16_]
			    = i_10_;
		    }
		    if (is[Class674.anInt8633 * -878424575][i_13_][i_16_]
			!= i_10_) {
			client.anIntArray11120[i_11_]
			    = i_13_ | 0x520000 | 0x13000000;
			client.anIntArray11121[i_11_] = i_16_ | 0x530000;
			i_11_ = 1 + i_11_ & 0xfff;
			is[Class674.anInt8633 * -878424575][i_13_][i_16_]
			    = i_10_;
		    }
		    if (1 + i_13_ < client.aClass512_11100.method8417(54124303)
			&& (is[-878424575 * Class674.anInt8633][1 + i_13_]
			    [i_16_]) != i_10_
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [Class674.anInt8633 * -878424575][i_13_]
				  [i_16_])
				 & 0x4)
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [-878424575 * Class674.anInt8633][i_13_ + 1]
				  [i_16_ - 1])
				 & 0x4)) {
			client.anIntArray11120[i_11_]
			    = 1 + i_13_ | 0x520000 | ~0x6dffffff;
			client.anIntArray11121[i_11_] = i_16_ | 0x530000;
			i_11_ = i_11_ + 1 & 0xfff;
			is[-878424575 * Class674.anInt8633][i_13_ + 1][i_16_]
			    = i_10_;
		    }
		}
		i_16_--;
		if (i_13_ + 1 < client.aClass512_11100.method8417(1355221649)
		    && (is[Class674.anInt8633 * -878424575][1 + i_13_][i_16_]
			!= i_10_)) {
		    client.anIntArray11120[i_11_]
			= i_13_ + 1 | 0x920000 | 0x53000000;
		    client.anIntArray11121[i_11_] = i_16_ | 0x930000;
		    i_11_ = 1 + i_11_ & 0xfff;
		    is[-878424575 * Class674.anInt8633][i_13_ + 1][i_16_]
			= i_10_;
		}
		if (--i_16_ >= 0) {
		    if (i_13_ - 1 >= 0
			&& i_10_ != (is[Class674.anInt8633 * -878424575]
				     [i_13_ - 1][i_16_])
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [Class674.anInt8633 * -878424575][i_13_]
				  [i_16_])
				 & 0x4)
			&& ((class468.aByteArrayArrayArray5145
			     [Class674.anInt8633 * -878424575][i_13_ - 1]
			     [1 + i_16_])
			    & 0x4) == 0) {
			client.anIntArray11120[i_11_]
			    = i_13_ - 1 | 0xd20000 | 0x12000000;
			client.anIntArray11121[i_11_] = i_16_ | 0xd30000;
			i_11_ = 1 + i_11_ & 0xfff;
			is[Class674.anInt8633 * -878424575][i_13_ - 1][i_16_]
			    = i_10_;
		    }
		    if (is[-878424575 * Class674.anInt8633][i_13_][i_16_]
			!= i_10_) {
			client.anIntArray11120[i_11_]
			    = i_13_ | 0xd20000 | ~0x6cffffff;
			client.anIntArray11121[i_11_] = i_16_ | 0xd30000;
			i_11_ = 1 + i_11_ & 0xfff;
			is[-878424575 * Class674.anInt8633][i_13_][i_16_]
			    = i_10_;
		    }
		    if ((1 + i_13_
			 < client.aClass512_11100.method8417(430573661))
			&& (is[-878424575 * Class674.anInt8633][i_13_ + 1]
			    [i_16_]) != i_10_
			&& 0 == ((class468.aByteArrayArrayArray5145
				  [-878424575 * Class674.anInt8633][i_13_]
				  [i_16_])
				 & 0x4)
			&& ((class468.aByteArrayArrayArray5145
			     [-878424575 * Class674.anInt8633][1 + i_13_]
			     [1 + i_16_])
			    & 0x4) == 0) {
			client.anIntArray11120[i_11_]
			    = i_13_ + 1 | 0x920000 | ~0x2dffffff;
			client.anIntArray11121[i_11_] = i_16_ | 0x930000;
			i_11_ = i_11_ + 1 & 0xfff;
			is[-878424575 * Class674.anInt8633][i_13_ + 1][i_16_]
			    = i_10_;
		    }
		}
	    }
	}
	if (-1000000 != client.anIntArray11177[i]) {
	    client.anIntArray11177[i] += 40;
	    client.anIntArray11123[i] -= 512;
	    client.anIntArray11179[i] += 512;
	    client.anIntArray11135[i] += 512;
	    client.anIntArray11181[i] -= 512;
	}
	return true;
    }
    
    static final void method8912(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class375.method6411(class247, class243, class669, (short) 4903);
    }
    
    static void method8913(int i) {
	Class65.aClass100_658.method1874(-1735976454);
	Class65.aClass100_658.aClass534_Sub40_Sub1_1179.anInt10811 = 0;
	Class65.aClass100_658.aClass409_1199 = null;
	Class65.aClass100_658.aClass409_1196 = null;
	Class65.aClass100_658.aClass409_1195 = null;
	Class65.aClass100_658.anInt1189 = 0;
	client.anInt11231 = 0;
	client.anInt11171 = 0;
	client.anInt11324 = 0;
	client.anInt11329 = 0;
	client.aString11300 = null;
	Class455.anInt4963 = 0;
	Class168.aClass98Array1792 = null;
	Class19.aClass352_211 = null;
	Class162.aClass352_1758 = null;
	client.aBool11339 = true;
	Class540.method8935((byte) 4);
	for (int i_38_ = 0;
	     i_38_ < Class78.aClass103_825.aClass626Array1296.length;
	     i_38_++) {
	    Class626 class626
		= new Class626(Class542.aClass623_7184
				   .method10297(i_38_, -1475330168),
			       false);
	    class626.method10343(0, 1902938399);
	    class626.method10338(0, -1095140607);
	    Class78.aClass103_825.aClass626Array1296[i_38_] = class626;
	}
	Class78.aClass103_825.aClass612_1294.method10098(-821532329);
	Class690_Sub21.method17075((short) 1729);
	Class66.method1361(Class65.aClass100_658, 243151006);
    }
}
