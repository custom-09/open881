/* Class53_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class53_Sub1 extends Class53
{
    public Class53_Sub1(Class675 class675, Class672 class672,
			Class472 class472, boolean bool) {
	super(class675, class672, Class649.aClass649_8404, class472,
	      new Class61(com.jagex.Class534_Sub18_Sub3.class), bool);
    }
    
    public static Class390 method17361(Class534_Sub40 class534_sub40, int i) {
	String string = class534_sub40.method16541((byte) -52);
	Class401 class401 = (Class72.method1560(2096927378)
			     [class534_sub40.method16527(-967491631)]);
	Class391 class391 = (Class705.method14234(622224665)
			     [class534_sub40.method16527(1336883786)]);
	int i_0_ = class534_sub40.method16530((byte) -24);
	int i_1_ = class534_sub40.method16530((byte) -118);
	int i_2_ = class534_sub40.method16527(-417723189);
	int i_3_ = class534_sub40.method16527(1741119300);
	int i_4_ = class534_sub40.method16527(523042602);
	int i_5_ = class534_sub40.method16529((byte) 1);
	int i_6_ = class534_sub40.method16529((byte) 1);
	int i_7_ = class534_sub40.method16550((byte) 2);
	int i_8_ = class534_sub40.method16533(-258848859);
	int i_9_ = class534_sub40.method16533(-258848859);
	return new Class390(string, class401, class391, i_0_, i_1_, i_2_, i_3_,
			    i_4_, i_5_, i_6_, i_7_, i_8_, i_9_);
    }
}
