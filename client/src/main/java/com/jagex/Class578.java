/* Class578 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class578 implements Interface13
{
    int anInt7714;
    public int anInt7715;
    public int[] anIntArray7716;
    public static final int anInt7717 = 0;
    String aString7718;
    Interface14 anInterface14_7719;
    static final int anInt7720 = 70;
    public boolean aBool7721 = false;
    int anInt7722;
    public int anInt7723;
    public int anInt7724;
    Class586 aClass586_7725;
    public static final int anInt7726 = 1;
    int anInt7727;
    int anInt7728;
    public int anInt7729;
    public static final int anInt7730 = -1;
    public int anInt7731;
    static final int anInt7732 = 16777215;
    public int anInt7733;
    public int anInt7734;
    public int anInt7735 = 184437089;
    public int anInt7736;
    public int anInt7737;
    int anInt7738;
    int anInt7739;
    int anInt7740;
    int anInt7741;
    public static int anInt7742;
    public static Class44_Sub3 aClass44_Sub3_7743;
    public static int anInt7744;
    
    public Class163 method9774(Class185 class185) {
	if (anInt7722 * 84693715 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7722
								* 84693715)));
	if (null == class163) {
	    method9783(class185, 551228243);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (84693715 * anInt7722));
	}
	return class163;
    }
    
    public Class163 method9775(Class185 class185, int i) {
	if (anInt7722 * 84693715 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7722
								* 84693715)));
	if (null == class163) {
	    method9783(class185, -1183104965);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (84693715 * anInt7722));
	}
	return class163;
    }
    
    void method9776(Class534_Sub40 class534_sub40, int i, int i_0_) {
	if (i == 1)
	    anInt7735 = class534_sub40.method16550((byte) -10) * -184437089;
	else if (i == 2) {
	    anInt7723 = class534_sub40.method16531(590697300) * 1257612201;
	    aBool7721 = true;
	} else if (3 == i)
	    anInt7714 = class534_sub40.method16550((byte) 65) * -1213463151;
	else if (i == 4)
	    anInt7727 = class534_sub40.method16550((byte) -46) * 603706763;
	else if (i == 5)
	    anInt7722 = class534_sub40.method16550((byte) 59) * 978834779;
	else if (6 == i)
	    anInt7728 = class534_sub40.method16550((byte) -24) * -1421348389;
	else if (7 == i)
	    anInt7729 = class534_sub40.method16530((byte) -52) * -1915218899;
	else if (8 == i)
	    aString7718 = class534_sub40.method16523(-1343882499);
	else if (9 == i)
	    anInt7724 = class534_sub40.method16529((byte) 1) * -1875395463;
	else if (i == 10)
	    anInt7715 = class534_sub40.method16530((byte) -48) * 653431865;
	else if (i == 11)
	    anInt7731 = 0;
	else if (12 == i)
	    anInt7733 = class534_sub40.method16527(965155758) * -1185770227;
	else if (13 == i)
	    anInt7734 = class534_sub40.method16530((byte) -92) * 1066318293;
	else if (14 == i)
	    anInt7731 = class534_sub40.method16529((byte) 1) * -1256435453;
	else if (i == 16) {
	    anInt7737 = class534_sub40.method16530((byte) -35) * -1721496031;
	    anInt7736 = class534_sub40.method16530((byte) -25) * 475879979;
	} else if (17 == i || i == 18) {
	    anInt7738 = class534_sub40.method16529((byte) 1) * 2105446301;
	    if (56323765 * anInt7738 == 65535)
		anInt7738 = -2105446301;
	    anInt7739 = class534_sub40.method16529((byte) 1) * -525250797;
	    if (anInt7739 * -1235168485 == 65535)
		anInt7739 = 525250797;
	    int i_1_ = -1;
	    if (18 == i) {
		i_1_ = class534_sub40.method16529((byte) 1);
		if (i_1_ == 65535)
		    i_1_ = -1;
	    }
	    int i_2_ = class534_sub40.method16527(245273365);
	    anIntArray7716 = new int[i_2_ + 2];
	    for (int i_3_ = 0; i_3_ <= i_2_; i_3_++) {
		anIntArray7716[i_3_] = class534_sub40.method16529((byte) 1);
		if (65535 == anIntArray7716[i_3_])
		    anIntArray7716[i_3_] = -1;
	    }
	    anIntArray7716[1 + i_2_] = i_1_;
	} else if (i == 19)
	    anInt7740 = class534_sub40.method16529((byte) 1) * -1925512347;
	else if (i == 20)
	    anInt7741 = class534_sub40.method16529((byte) 1) * -1889864929;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_4_ = class534_sub40.method16527(528697469);
	    if (i_4_ == 0)
		break;
	    method9776(class534_sub40, i_4_, -1217904898);
	}
    }
    
    public String method9777(int i, byte i_5_) {
	String string = aString7718;
	i = i * (-308054419 * anInt7740) / (anInt7741 * -1184184097);
	for (;;) {
	    int i_6_ = string.indexOf("%1");
	    if (i_6_ < 0)
		break;
	    string
		= new StringBuilder().append(string.substring(0, i_6_)).append
		      (Class316.method5723(i, false, (byte) 42)).append
		      (string.substring(i_6_ + 2)).toString();
	}
	return string;
    }
    
    public Class163 method9778(Class185 class185, int i) {
	if (237460337 * anInt7714 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7714
								* 237460337)));
	if (null == class163) {
	    method9783(class185, 656374343);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (237460337 * anInt7714));
	}
	return class163;
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    public Class163 method9779(Class185 class185, int i) {
	if (901458467 * anInt7727 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7727
								* 901458467)));
	if (class163 == null) {
	    method9783(class185, 1847911800);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (901458467 * anInt7727));
	}
	return class163;
    }
    
    public Class163 method9780(Class185 class185) {
	if (901458467 * anInt7727 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7727
								* 901458467)));
	if (class163 == null) {
	    method9783(class185, 150093111);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (901458467 * anInt7727));
	}
	return class163;
    }
    
    public final Class578 method9781(Interface18 interface18,
				     Interface20 interface20, byte i) {
	int i_7_ = -1;
	if (null == anIntArray7716)
	    return this;
	if (interface18 == null || interface20 == null)
	    return null;
	if (-1 != 56323765 * anInt7738) {
	    Class318 class318
		= interface18.method107(56323765 * anInt7738, 1504047109);
	    if (class318 != null)
		i_7_ = interface20.method119(class318, -1475231115);
	} else if (-1235168485 * anInt7739 != -1) {
	    Class150 class150
		= interface18.method108(Class453.aClass453_4946,
					anInt7739 * -1235168485, 1717622439);
	    if (class150 != null)
		i_7_ = interface20.method120(class150, (byte) -45);
	}
	if (i_7_ < 0 || i_7_ >= anIntArray7716.length - 1) {
	    int i_8_ = anIntArray7716[anIntArray7716.length - 1];
	    if (-1 != i_8_)
		return ((Class578)
			anInterface14_7719.method91(i_8_, 1462175555));
	    return null;
	}
	if (anIntArray7716[i_7_] == -1)
	    return null;
	return ((Class578)
		anInterface14_7719.method91(anIntArray7716[i_7_], -279259130));
    }
    
    public Class163 method9782(Class185 class185, int i) {
	if (anInt7728 * 1138795091 < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass586_7725.aClass203_7801
			     .method3871((long) (anInt7728 * 1138795091));
	if (null == class163) {
	    method9783(class185, 953619356);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (1138795091 * anInt7728));
	}
	return class163;
    }
    
    void method9783(Class185 class185, int i) {
	Class472 class472 = aClass586_7725.aClass472_7800;
	if (237460337 * anInt7714 >= 0
	    && (aClass586_7725.aClass203_7801.method3871((long) (anInt7714
								 * 237460337))
		== null)
	    && class472.method7670(237460337 * anInt7714, (byte) -54)) {
	    Class169 class169
		= Class178.method2945(class472, anInt7714 * 237460337);
	    aClass586_7725.aClass203_7801.method3893
		(class185.method3279(class169, true),
		 (long) (237460337 * anInt7714));
	}
	if (anInt7722 * 84693715 >= 0
	    && (aClass586_7725.aClass203_7801.method3871((long) (84693715
								 * anInt7722))
		== null)
	    && class472.method7670(84693715 * anInt7722, (byte) -55)) {
	    Class169 class169
		= Class178.method2945(class472, anInt7722 * 84693715);
	    aClass586_7725.aClass203_7801.method3893
		(class185.method3279(class169, true),
		 (long) (84693715 * anInt7722));
	}
	if (anInt7727 * 901458467 >= 0
	    && (aClass586_7725.aClass203_7801.method3871((long) (901458467
								 * anInt7727))
		== null)
	    && class472.method7670(anInt7727 * 901458467, (byte) -77)) {
	    Class169 class169
		= Class178.method2945(class472, 901458467 * anInt7727);
	    aClass586_7725.aClass203_7801.method3893
		(class185.method3279(class169, true),
		 (long) (anInt7727 * 901458467));
	}
	if (anInt7728 * 1138795091 >= 0
	    && (aClass586_7725.aClass203_7801.method3871((long) (anInt7728
								 * 1138795091))
		== null)
	    && class472.method7670(anInt7728 * 1138795091, (byte) 0)) {
	    Class169 class169
		= Class178.method2945(class472, anInt7728 * 1138795091);
	    aClass586_7725.aClass203_7801.method3893
		(class185.method3279(class169, true),
		 (long) (1138795091 * anInt7728));
	}
    }
    
    public String method9784(int i) {
	String string = aString7718;
	i = i * (-308054419 * anInt7740) / (anInt7741 * -1184184097);
	for (;;) {
	    int i_9_ = string.indexOf("%1");
	    if (i_9_ < 0)
		break;
	    string
		= new StringBuilder().append(string.substring(0, i_9_)).append
		      (Class316.method5723(i, false, (byte) 102)).append
		      (string.substring(i_9_ + 2)).toString();
	}
	return string;
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(61524309);
	    if (i == 0)
		break;
	    method9776(class534_sub40, i, -1366039996);
	}
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    public final Class578 method9785(Interface18 interface18,
				     Interface20 interface20) {
	int i = -1;
	if (null == anIntArray7716)
	    return this;
	if (interface18 == null || interface20 == null)
	    return null;
	if (-1 != 56323765 * anInt7738) {
	    Class318 class318
		= interface18.method107(56323765 * anInt7738, 1504047109);
	    if (class318 != null)
		i = interface20.method119(class318, -1688623167);
	} else if (-1235168485 * anInt7739 != -1) {
	    Class150 class150
		= interface18.method108(Class453.aClass453_4946,
					anInt7739 * -1235168485, 1869033499);
	    if (class150 != null)
		i = interface20.method120(class150, (byte) -101);
	}
	if (i < 0 || i >= anIntArray7716.length - 1) {
	    int i_10_ = anIntArray7716[anIntArray7716.length - 1];
	    if (-1 != i_10_)
		return ((Class578)
			anInterface14_7719.method91(i_10_, -1944256929));
	    return null;
	}
	if (anIntArray7716[i] == -1)
	    return null;
	return ((Class578)
		anInterface14_7719.method91(anIntArray7716[i], 548422322));
    }
    
    public final Class578 method9786(Interface18 interface18,
				     Interface20 interface20) {
	int i = -1;
	if (null == anIntArray7716)
	    return this;
	if (interface18 == null || interface20 == null)
	    return null;
	if (-1 != 56323765 * anInt7738) {
	    Class318 class318
		= interface18.method107(56323765 * anInt7738, 1504047109);
	    if (class318 != null)
		i = interface20.method119(class318, -1032480555);
	} else if (-1235168485 * anInt7739 != -1) {
	    Class150 class150
		= interface18.method108(Class453.aClass453_4946,
					anInt7739 * -1235168485, 1686853277);
	    if (class150 != null)
		i = interface20.method120(class150, (byte) -93);
	}
	if (i < 0 || i >= anIntArray7716.length - 1) {
	    int i_11_ = anIntArray7716[anIntArray7716.length - 1];
	    if (-1 != i_11_)
		return ((Class578)
			anInterface14_7719.method91(i_11_, 352386304));
	    return null;
	}
	if (anIntArray7716[i] == -1)
	    return null;
	return ((Class578)
		anInterface14_7719.method91(anIntArray7716[i], -798169542));
    }
    
    Class578(int i, Class586 class586, Interface14 interface14) {
	anInt7723 = 1577737303;
	anInt7724 = 1866303766;
	anInt7714 = 1213463151;
	anInt7722 = -978834779;
	anInt7727 = -603706763;
	anInt7728 = 1421348389;
	anInt7729 = 0;
	anInt7715 = 0;
	anInt7731 = 1256435453;
	aString7718 = "";
	anInt7733 = 1185770227;
	anInt7734 = 0;
	anInt7737 = 0;
	anInt7736 = 0;
	anInt7738 = -2105446301;
	anInt7739 = 525250797;
	anInt7740 = -1925512347;
	anInt7741 = -1889864929;
	aClass586_7725 = class586;
	anInterface14_7719 = interface14;
    }
    
    public Class163 method9787(Class185 class185) {
	if (237460337 * anInt7714 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7714
								* 237460337)));
	if (null == class163) {
	    method9783(class185, 1632848260);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (237460337 * anInt7714));
	}
	return class163;
    }
    
    public Class163 method9788(Class185 class185) {
	if (237460337 * anInt7714 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7714
								* 237460337)));
	if (null == class163) {
	    method9783(class185, -550310314);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (237460337 * anInt7714));
	}
	return class163;
    }
    
    public Class163 method9789(Class185 class185) {
	if (237460337 * anInt7714 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7714
								* 237460337)));
	if (null == class163) {
	    method9783(class185, -1286174257);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (237460337 * anInt7714));
	}
	return class163;
    }
    
    public Class163 method9790(Class185 class185) {
	if (anInt7722 * 84693715 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7722
								* 84693715)));
	if (null == class163) {
	    method9783(class185, 1042738550);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (84693715 * anInt7722));
	}
	return class163;
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1838108426);
	    if (i == 0)
		break;
	    method9776(class534_sub40, i, -1722724416);
	}
    }
    
    public Class163 method9791(Class185 class185) {
	if (anInt7722 * 84693715 < 0)
	    return null;
	Class163 class163
	    = ((Class163)
	       aClass586_7725.aClass203_7801.method3871((long) (anInt7722
								* 84693715)));
	if (null == class163) {
	    method9783(class185, 876845002);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (84693715 * anInt7722));
	}
	return class163;
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(981550496);
	    if (i == 0)
		break;
	    method9776(class534_sub40, i, -1533415249);
	}
    }
    
    public Class163 method9792(Class185 class185) {
	if (anInt7728 * 1138795091 < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass586_7725.aClass203_7801
			     .method3871((long) (anInt7728 * 1138795091));
	if (null == class163) {
	    method9783(class185, -805103375);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (1138795091 * anInt7728));
	}
	return class163;
    }
    
    public Class163 method9793(Class185 class185) {
	if (anInt7728 * 1138795091 < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass586_7725.aClass203_7801
			     .method3871((long) (anInt7728 * 1138795091));
	if (null == class163) {
	    method9783(class185, 1414668885);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (1138795091 * anInt7728));
	}
	return class163;
    }
    
    public Class163 method9794(Class185 class185) {
	if (anInt7728 * 1138795091 < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass586_7725.aClass203_7801
			     .method3871((long) (anInt7728 * 1138795091));
	if (null == class163) {
	    method9783(class185, 2105089387);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (1138795091 * anInt7728));
	}
	return class163;
    }
    
    public Class163 method9795(Class185 class185) {
	if (anInt7728 * 1138795091 < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass586_7725.aClass203_7801
			     .method3871((long) (anInt7728 * 1138795091));
	if (null == class163) {
	    method9783(class185, -686849065);
	    class163
		= (Class163) aClass586_7725.aClass203_7801
				 .method3871((long) (1138795091 * anInt7728));
	}
	return class163;
    }
    
    static final void method9796(Class669 class669, int i) {
	Class458.method7438((Class654_Sub1) class669.anInterface62_8603,
			    (class669.anIntArray8595
			     [(class669.anInt8600 -= 308999563) * 2088438307]),
			    true, -1644148534);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (int) client.aFloatArray11167[0];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (int) client.aFloatArray11167[1];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (int) client.aFloatArray11167[2];
    }
    
    static final void method9797(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_12_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	boolean bool = 1 == (class669.anIntArray8595
			     [1 + class669.anInt8600 * 2088438307]);
	if (!Class94.method1765(i_12_, bool, (byte) 103))
	    throw new RuntimeException();
    }
    
    static char method9798(char c, int i) {
	switch (c) {
	case '\u00d9':
	case '\u00da':
	case '\u00db':
	case '\u00dc':
	case '\u00f9':
	case '\u00fa':
	case '\u00fb':
	case '\u00fc':
	    return 'u';
	default:
	    return Character.toLowerCase(c);
	case '\u00c7':
	case '\u00e7':
	    return 'c';
	case '\u00df':
	    return 'b';
	case ' ':
	case '-':
	case '_':
	case '\u00a0':
	    return '_';
	case '\u00ff':
	case '\u0178':
	    return 'y';
	case '\u00c0':
	case '\u00c1':
	case '\u00c2':
	case '\u00c3':
	case '\u00c4':
	case '\u00e0':
	case '\u00e1':
	case '\u00e2':
	case '\u00e3':
	case '\u00e4':
	    return 'a';
	case '\u00cd':
	case '\u00ce':
	case '\u00cf':
	case '\u00ed':
	case '\u00ee':
	case '\u00ef':
	    return 'i';
	case '\u00c8':
	case '\u00c9':
	case '\u00ca':
	case '\u00cb':
	case '\u00e8':
	case '\u00e9':
	case '\u00ea':
	case '\u00eb':
	    return 'e';
	case '\u00d1':
	case '\u00f1':
	    return 'n';
	case '\u00d2':
	case '\u00d3':
	case '\u00d4':
	case '\u00d5':
	case '\u00d6':
	case '\u00f2':
	case '\u00f3':
	case '\u00f4':
	case '\u00f5':
	case '\u00f6':
	    return 'o';
	case '#':
	case '[':
	case ']':
	    return c;
	}
    }
    
    public static final Class247 method9799(Class243 class243,
					    Class247 class247, int i) {
	if (-1 != class247.anInt2472 * -742015869)
	    return class243.method4476(class247.anInt2472 * -742015869,
				       -82873306);
	if (!class243.aBool2413) {
	    int i_13_ = class247.anInt2454 * -1278450723 >>> 16;
	    Class18 class18 = new Class18(client.aClass9_11224);
	    for (Class534_Sub37 class534_sub37
		     = (Class534_Sub37) class18.method793(-642673515);
		 class534_sub37 != null;
		 class534_sub37 = (Class534_Sub37) class18.next()) {
		if (1225863589 * class534_sub37.anInt10803 == i_13_)
		    return Class112.method2017((int) (8258869577519436579L
						      * (class534_sub37
							 .aLong7158)),
					       1479143853);
	    }
	}
	return null;
    }
}
