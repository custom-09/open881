/* Class594 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;
import java.util.Map;

public class Class594
{
    static Class594 aClass594_7824;
    static Class594 aClass594_7825;
    static Class594 aClass594_7826 = new Class594(0);
    int anInt7827;
    public static Map aMap7828;
    
    static Class594[] method9901() {
	return (new Class594[]
		{ aClass594_7825, aClass594_7826, aClass594_7824 });
    }
    
    static {
	aClass594_7825 = new Class594(1);
	aClass594_7824 = new Class594(2);
    }
    
    static Class594[] method9902() {
	return (new Class594[]
		{ aClass594_7825, aClass594_7826, aClass594_7824 });
    }
    
    Class594(int i) {
	anInt7827 = -1232506795 * i;
    }
    
    static Class594[] method9903() {
	return (new Class594[]
		{ aClass594_7825, aClass594_7826, aClass594_7824 });
    }
    
    static Class594[] method9904() {
	return (new Class594[]
		{ aClass594_7825, aClass594_7826, aClass594_7824 });
    }
    
    static Class594 method9905(int i) {
	Class594[] class594s = Class102.method1905((byte) 87);
	for (int i_0_ = 0; i_0_ < class594s.length; i_0_++) {
	    Class594 class594 = class594s[i_0_];
	    if (1296567549 * class594.anInt7827 == i)
		return class594;
	}
	return null;
    }
    
    static Class594 method9906(int i) {
	Class594[] class594s = Class102.method1905((byte) 16);
	for (int i_1_ = 0; i_1_ < class594s.length; i_1_++) {
	    Class594 class594 = class594s[i_1_];
	    if (1296567549 * class594.anInt7827 == i)
		return class594;
	}
	return null;
    }
    
    static final void method9907(Class669 class669, int i) {
	Class599.aClass298_Sub1_7871.method5376((byte) 14);
    }
    
    static Class175_Sub2_Sub1 method9908(Class185_Sub2 class185_sub2,
					 Canvas canvas, int i, int i_2_,
					 short i_3_) {
	Class175_Sub2_Sub1_Sub1 class175_sub2_sub1_sub1
	    = new Class175_Sub2_Sub1_Sub1(class185_sub2, canvas, i, i_2_);
	return class175_sub2_sub1_sub1;
    }
}
