/* Class600 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class600 implements Interface70
{
    Interface69 anInterface69_7872;
    Class592 aClass592_7873 = Class592.aClass592_7817;
    int anInt7874;
    Class472 aClass472_7875;
    Class367 aClass367_7876;
    
    public void method475(boolean bool) {
	/* empty */
    }
    
    public void method446(byte i) {
	if (aClass592_7873 == Class592.aClass592_7818) {
	    method9948(870860472);
	    aClass592_7873 = Class592.aClass592_7816;
	    if (anInterface69_7872 != null)
		anInterface69_7872.method441(this, 0, anInt7874 * 1371532667,
					     true, -1186343192);
	}
    }
    
    public Class491 method444(byte i) {
	Class491 class491 = new Class491(aClass367_7876);
	class491.method8010(this, 1045939075);
	return class491;
    }
    
    public void method469(Class534_Sub40 class534_sub40) {
	/* empty */
    }
    
    public boolean method445(byte i) {
	return true;
    }
    
    void method9948(int i) {
	/* empty */
    }
    
    public int method456(byte i) {
	return 0;
    }
    
    public int method448() {
	return 0;
    }
    
    public boolean method453(int i) {
	return false;
    }
    
    public boolean method454(int i, byte i_0_) {
	return false;
    }
    
    public byte[] method447(int i, byte i_1_) {
	int i_2_ = i;
	if (0 == i_2_)
	    i_2_ = anInt7874 * 1371532667;
	return aClass472_7875.method7738(i_2_, (byte) -61);
    }
    
    public Class534_Sub40 method458(int i, int i_3_) {
	return null;
    }
    
    public int method222() {
	return 0;
    }
    
    public void method451(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    public int method452(byte i) {
	return 0;
    }
    
    public int method30(int i) {
	return anInt7874 * 1371532667;
    }
    
    public Class592 method470(int i) {
	return aClass592_7873;
    }
    
    public Class496 method463(int i) {
	return Class496.aClass496_5540;
    }
    
    public void method226() {
	if (aClass592_7873 == Class592.aClass592_7818) {
	    method9948(870860472);
	    aClass592_7873 = Class592.aClass592_7816;
	    if (anInterface69_7872 != null)
		anInterface69_7872.method441(this, 0, anInt7874 * 1371532667,
					     true, -1186343192);
	}
    }
    
    public Class491 method459() {
	Class491 class491 = new Class491(aClass367_7876);
	class491.method8010(this, 1045939075);
	return class491;
    }
    
    public Class491 method481() {
	Class491 class491 = new Class491(aClass367_7876);
	class491.method8010(this, 1045939075);
	return class491;
    }
    
    public int method225() {
	return anInt7874 * 1371532667;
    }
    
    public Class491 method461() {
	Class491 class491 = new Class491(aClass367_7876);
	class491.method8010(this, 1045939075);
	return class491;
    }
    
    public Class592 method457() {
	return aClass592_7873;
    }
    
    public boolean method423() {
	return true;
    }
    
    public boolean method462() {
	return true;
    }
    
    public byte[] method464(int i) {
	int i_4_ = i;
	if (0 == i_4_)
	    i_4_ = anInt7874 * 1371532667;
	return aClass472_7875.method7738(i_4_, (byte) -51);
    }
    
    public byte[] method465(int i) {
	int i_5_ = i;
	if (0 == i_5_)
	    i_5_ = anInt7874 * 1371532667;
	return aClass472_7875.method7738(i_5_, (byte) -40);
    }
    
    public void method472(boolean bool, int i) {
	/* empty */
    }
    
    public void method482(Class534_Sub40 class534_sub40) {
	/* empty */
    }
    
    public int method223() {
	return 0;
    }
    
    public Class496 method455() {
	return Class496.aClass496_5540;
    }
    
    public Class600(Class472 class472, int i, Class367 class367,
		    Interface69 interface69) {
	aClass472_7875 = class472;
	anInt7874 = -1569134157 * i;
	aClass367_7876 = class367;
	anInterface69_7872 = interface69;
	aClass592_7873 = Class592.aClass592_7818;
    }
    
    public void method229() {
	if (aClass592_7873 == Class592.aClass592_7818) {
	    method9948(870860472);
	    aClass592_7873 = Class592.aClass592_7816;
	    if (anInterface69_7872 != null)
		anInterface69_7872.method441(this, 0, anInt7874 * 1371532667,
					     true, -1186343192);
	}
    }
    
    public int method449() {
	return anInt7874 * 1371532667;
    }
    
    public Class534_Sub40 method476(int i) {
	return null;
    }
    
    public boolean method471(int i) {
	return false;
    }
    
    public boolean method466(int i) {
	return false;
    }
    
    public boolean method473() {
	return false;
    }
    
    public boolean method273() {
	return false;
    }
    
    public Class491 method460() {
	Class491 class491 = new Class491(aClass367_7876);
	class491.method8010(this, 1045939075);
	return class491;
    }
    
    public boolean method467(int i) {
	return false;
    }
    
    public boolean method474() {
	return false;
    }
    
    public boolean method477() {
	return false;
    }
    
    public void method478(Class534_Sub40 class534_sub40) {
	/* empty */
    }
    
    public void method479(Class534_Sub40 class534_sub40) {
	/* empty */
    }
    
    public void method480(Class534_Sub40 class534_sub40) {
	/* empty */
    }
    
    public int method450() {
	return 0;
    }
    
    public int method468() {
	return anInt7874 * 1371532667;
    }
    
    void method9949() {
	/* empty */
    }
    
    static byte[] method9950(Class472 class472, Class649 class649, int i,
			     short i_6_) {
	if (class649.method10707((byte) 50) > 1)
	    return class472.method7743(class649.method10711(i, -1187599554),
				       class649.method10708(i, -2084138253),
				       -1970450004);
	return class472.method7743(1570156841 * class649.anInt8385, i,
				   -1624846134);
    }
}
