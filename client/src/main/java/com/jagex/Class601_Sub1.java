/* Class601_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class601_Sub1 extends Class601 implements Interface6
{
    Class472 aClass472_10876;
    public static Class472 aClass472_10877;
    
    byte[] method9959(int i) {
	synchronized (aClass472_10876) {
	    byte[] is = aClass472_10876.method7743(i, 0, -2048744874);
	    return is;
	}
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class602(i, this, interface14);
    }
    
    public Class method59(short i) {
	return com.jagex.Class602.class;
    }
    
    boolean method9961(int i) {
	synchronized (aClass472_10876) {
	    boolean bool = aClass472_10876.method7669(i, 0, 1804616883);
	    return bool;
	}
    }
    
    boolean method9958(int i, int i_1_) {
	synchronized (aClass472_10876) {
	    boolean bool = aClass472_10876.method7669(i, 0, 1804616883);
	    return bool;
	}
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class602(i, this, interface14);
    }
    
    public Class method61() {
	return com.jagex.Class602.class;
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class602(i, this, interface14);
    }
    
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class602(i, this, interface14);
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class602(i, this, interface14);
    }
    
    byte[] method9966(int i) {
	synchronized (aClass472_10876) {
	    byte[] is = aClass472_10876.method7743(i, 0, -695064367);
	    return is;
	}
    }
    
    Class601_Sub1(boolean bool, Class472 class472, Class672 class672,
		  Class675 class675) {
	super(bool, class672, class675);
	aClass472_10876 = class472;
    }
    
    byte[] method9953(int i) {
	synchronized (aClass472_10876) {
	    byte[] is = aClass472_10876.method7743(i, 0, -960186957);
	    return is;
	}
    }
    
    boolean method9962(int i) {
	synchronized (aClass472_10876) {
	    boolean bool = aClass472_10876.method7669(i, 0, 1804616883);
	    return bool;
	}
    }
    
    byte[] method9957(int i, int i_2_) {
	synchronized (aClass472_10876) {
	    byte[] is = aClass472_10876.method7743(i, 0, -887313152);
	    return is;
	}
    }
    
    public static void method16965(byte i) {
	Class247.aClass203_2552.method3884((byte) -44);
	Class247.aClass203_2504.method3884((byte) -11);
	Class247.aClass203_2523.method3884((byte) -113);
	Class247.aClass203_2449.method3884((byte) -59);
    }
}
