/* Class602 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Arrays;

public class Class602 implements Interface13
{
    short[] aShortArray7885;
    public static short[] aShortArray7886 = new short[256];
    public int anInt7887;
    Class601 aClass601_7888;
    static final int anInt7889 = 127007;
    public int anInt7890;
    int[][] anIntArrayArray7891;
    public String aString7892 = "null";
    short[] aShortArray7893;
    short[] aShortArray7894;
    byte[] aByteArray7895;
    public boolean aBool7896;
    short[] aShortArray7897;
    int anInt7898;
    byte[] aByteArray7899;
    byte aByte7900;
    public int anInt7901;
    byte aByte7902;
    int anInt7903;
    public int anInt7904;
    int anInt7905;
    byte aByte7906 = 0;
    public int anInt7907;
    byte aByte7908;
    int anInt7909;
    public boolean aBool7910;
    public int anInt7911;
    int anInt7912;
    public int anInt7913;
    int[] anIntArray7914;
    public int anInt7915;
    public boolean aBool7916;
    public int anInt7917;
    int anInt7918;
    int anInt7919;
    public String[] aStringArray7920;
    byte[] aByteArray7921;
    int[] anIntArray7922;
    public int anInt7923;
    public boolean aBool7924;
    public int anInt7925;
    byte[] aByteArray7926;
    public boolean aBool7927;
    public int anInt7928;
    int anInt7929;
    int anInt7930;
    int anInt7931;
    int anInt7932;
    int anInt7933;
    Interface14 anInterface14_7934;
    int anInt7935;
    int anInt7936;
    static final int anInt7937 = 6;
    public boolean aBool7938;
    boolean aBool7939;
    public int anInt7940;
    public int anInt7941;
    int[] anIntArray7942;
    public int[] anIntArray7943;
    public boolean aBool7944;
    public int anInt7945;
    public int anInt7946;
    public int anInt7947;
    public int anInt7948;
    public int anInt7949;
    public int anInt7950;
    public int[] anIntArray7951;
    Class9 aClass9_7952;
    public boolean aBool7953;
    public int anInt7954;
    public boolean aBool7955;
    public Class432 aClass432_7956;
    public boolean aBool7957;
    public int[] anIntArray7958;
    public boolean aBool7959;
    public int anInt7960;
    public boolean aBool7961;
    public boolean aBool7962;
    public Class593 aClass593_7963;
    public boolean aBool7964;
    byte aByte7965;
    
    public int method9979(int i) {
	if (null == anIntArray7922)
	    return -1;
	return anIntArray7922[i];
    }
    
    public boolean method9980(int i) {
	if (anIntArray7914 != null && -1 != i) {
	    for (int i_0_ = 0; i_0_ < anIntArray7914.length; i_0_++) {
		if (anIntArray7914[i_0_] == i)
		    return true;
	    }
	}
	return false;
    }
    
    public void method82(int i) {
	method9982(1853799027);
	if (aBool7939)
	    anInt7923 = 0;
	if (!aClass601_7888.aBool7877 && aBool7964) {
	    aStringArray7920 = null;
	    anIntArray7958 = null;
	}
    }
    
    public final boolean method9981(int i) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i_1_ = 0; i_1_ < aByteArray7926.length; i_1_++) {
	    if (i == aByteArray7926[i_1_]) {
		for (int i_2_ = 0; i_2_ < anIntArrayArray7891[i_1_].length;
		     i_2_++) {
		    if (!aClass601_7888.method9958((anIntArrayArray7891[i_1_]
						    [i_2_]),
						   1189319673))
			bool = false;
		}
	    }
	}
	return bool;
    }
    
    void method9982(int i) {
	if (anInt7907 * -2134171963 == -1) {
	    anInt7907 = 0;
	    if (aByteArray7926 != null && aByteArray7926.length == 1
		&& (847393323 * Class595.aClass595_7836.anInt7852
		    == aByteArray7926[0]))
		anInt7907 = 384348173;
	    for (int i_3_ = 0; i_3_ < 5; i_3_++) {
		if (aStringArray7920[i_3_] != null) {
		    anInt7907 = 384348173;
		    break;
		}
	    }
	}
	if (-1 == 1288889595 * anInt7940)
	    anInt7940 = (0 != anInt7923 * -499459421 ? 1 : 0) * -694974925;
	if (method10015((byte) -14) || aBool7957 || anIntArray7943 != null)
	    aBool7961 = true;
	if (-2134171963 * anInt7907 <= 0
	    && aClass593_7963 != Class593.aClass593_7821) {
	    /* empty */
	}
    }
    
    public boolean method9983(short i) {
	if (anIntArray7943 == null)
	    return 352154885 * anInt7945 != -1 || null != anIntArray7951;
	for (int i_4_ = 0; i_4_ < anIntArray7943.length; i_4_++) {
	    if (anIntArray7943[i_4_] != -1) {
		Class602 class602_5_
		    = ((Class602)
		       anInterface14_7934.method91(anIntArray7943[i_4_],
						   -710743151));
		if (-1 != class602_5_.anInt7945 * 352154885
		    || class602_5_.anIntArray7951 != null)
		    return true;
	    }
	}
	return false;
    }
    
    public final boolean method9984(Class647 class647, int i) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i_6_ = 0; i_6_ < anIntArrayArray7891.length; i_6_++) {
	    for (int i_7_ = 0; i_7_ < anIntArrayArray7891[i_6_].length;
		 i_7_++) {
		boolean bool_8_
		    = aClass601_7888.method9958((anIntArrayArray7891[i_6_]
						 [i_7_]),
						1189319673);
		bool &= bool_8_;
		if (!bool_8_)
		    class647.anInt8375
			= anIntArrayArray7891[i_6_][i_7_] * 511701673;
	    }
	}
	return bool;
    }
    
    public int method9985(int i, int i_9_) {
	if (null == anIntArray7922)
	    return -1;
	return anIntArray7922[i];
    }
    
    public final synchronized Class183 method9986
	(Class185 class185, int i, int i_10_, int i_11_, Class151 class151,
	 Class151 class151_12_, int i_13_, int i_14_, int i_15_,
	 Class711 class711, Class596 class596, int i_16_) {
	if (Class459.method7439(i_10_, (byte) -35))
	    i_10_ = 847393323 * Class595.aClass595_7834.anInt7852;
	long l
	    = (long) ((i_10_ << 3) + (-1562722583 * anInt7887 << 10) + i_11_);
	int i_17_ = i;
	l |= (long) (2098753835 * class185.anInt2001 << 29);
	if (null != class596)
	    l |= 5594098338423607855L * class596.aLong7858 << 32;
	if (class711 != null)
	    i |= class711.method14360((short) -31144);
	if (aByte7908 == 3)
	    i |= 0x7;
	else {
	    if (0 != aByte7908 || 0 != 1273961079 * anInt7918)
		i |= 0x2;
	    if (0 != -1393558471 * anInt7935)
		i |= 0x1;
	    if (0 != 763951193 * anInt7936)
		i |= 0x4;
	}
	if (i_10_ == Class595.aClass595_7836.anInt7852 * 847393323
	    && i_11_ > 3)
	    i |= 0x5;
	Class183 class183;
	synchronized (aClass601_7888.aClass203_7884) {
	    class183 = (Class183) aClass601_7888.aClass203_7884.method3871(l);
	}
	if (null == class183
	    || class185.method3330(class183.method3101(), i) != 0) {
	    if (class183 != null)
		i = class185.method3331(i, class183.method3101());
	    class183
		= method9987(class185, i, i_10_, i_11_, class596, 2113275141);
	    if (class183 == null)
		return null;
	    synchronized (aClass601_7888.aClass203_7884) {
		aClass601_7888.aClass203_7884.method3893(class183, l);
	    }
	}
	boolean bool = false;
	if (null != class711) {
	    class183 = class183.method3011((byte) 1, i, true);
	    bool = true;
	    class711.method14399(class183, i_11_ & 0x3, 2143573169);
	}
	if (847393323 * Class595.aClass595_7836.anInt7852 == i_10_
	    && i_11_ > 3) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3015(2048);
	}
	if (aByte7908 != 0) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3022(aByte7908, anInt7909 * 1064510741, class151,
				class151_12_, i_13_, i_14_, i_15_);
	}
	if (-1393558471 * anInt7935 != 0 || 0 != 1273961079 * anInt7918
	    || 0 != anInt7936 * 763951193) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3098(-1393558471 * anInt7935,
				1273961079 * anInt7918, 763951193 * anInt7936);
	}
	if (bool)
	    class183.method3012(i_17_);
	return class183;
    }
    
    Class183 method9987(Class185 class185, int i, int i_18_, int i_19_,
			Class596 class596, int i_20_) {
	int i_21_ = 64 + anInt7898 * 57540739;
	int i_22_ = 850 + anInt7919 * -1886190073;
	int i_23_ = i;
	boolean bool
	    = (aBool7927
	       || (847393323 * Class595.aClass595_7829.anInt7852 == i_18_
		   && i_19_ > 3));
	if (bool)
	    i |= 0x10;
	if (0 == i_19_) {
	    if (128 != 1087546699 * anInt7929 || -507159513 * anInt7932 != 0)
		i |= 0x1;
	    if (128 != anInt7931 * -1406965279 || 0 != 552542037 * anInt7905)
		i |= 0x4;
	} else
	    i |= 0xd;
	if (anInt7930 * 855774647 != 128 || 0 != anInt7933 * 1699166449)
	    i |= 0x2;
	if (null != aShortArray7893)
	    i |= 0x4000;
	if (null != aShortArray7885)
	    i |= 0x8000;
	if (0 != aByte7906)
	    i |= 0x80000;
	Class183 class183 = null;
	if (aByteArray7926 != null) {
	    int i_24_ = -1;
	    for (int i_25_ = 0; i_25_ < aByteArray7926.length; i_25_++) {
		if (i_18_ == aByteArray7926[i_25_]) {
		    i_24_ = i_25_;
		    break;
		}
	    }
	    if (i_24_ == -1)
		return null;
	    int[] is
		= (class596 != null && class596.anIntArray7856 != null
		   ? class596.anIntArray7856 : anIntArrayArray7891[i_24_]);
	    int i_26_ = is.length;
	    if (i_26_ > 0) {
		long l = (long) (class185.anInt2001 * 2098753835);
		for (int i_27_ = 0; i_27_ < i_26_; i_27_++)
		    l = 67783L * l + (long) is[i_27_];
		synchronized (aClass601_7888.aClass203_7878) {
		    class183 = ((Class183)
				aClass601_7888.aClass203_7878.method3871(l));
		}
		if (null != class183) {
		    if (i_21_ != class183.method3053())
			i |= 0x1000;
		    if (i_22_ != class183.method3054())
			i |= 0x2000;
		}
		if (null == class183
		    || class185.method3330(class183.method3101(), i) != 0) {
		    int i_28_ = i | 0x1f01f;
		    if (null != class183)
			i_28_ = class185.method3331(i_28_,
						    class183.method3101());
		    Class187 class187 = null;
		    synchronized (aClass601_7888.aClass187Array7883) {
			for (int i_29_ = 0; i_29_ < i_26_; i_29_++) {
			    byte[] is_30_
				= aClass601_7888.method9957(is[i_29_],
							    -990304944);
			    if (null == is_30_) {
				Class183 class183_31_ = null;
				return class183_31_;
			    }
			    class187 = new Class187(is_30_);
			    if (class187.anInt2082 < 13)
				class187.method3723(2);
			    if (i_26_ > 1)
				aClass601_7888.aClass187Array7883[i_29_]
				    = class187;
			}
			if (i_26_ > 1)
			    class187 = new Class187((aClass601_7888
						     .aClass187Array7883),
						    i_26_);
		    }
		    class183 = class185.method3329(class187, i_28_,
						   (aClass601_7888.anInt7880
						    * -1707528565),
						   i_21_, i_22_);
		    synchronized (aClass601_7888.aClass203_7878) {
			aClass601_7888.aClass203_7878.method3893(class183, l);
		    }
		}
	    }
	}
	if (null == class183)
	    return null;
	Class183 class183_32_ = class183.method3011((byte) 0, i, true);
	if (i_21_ != class183.method3053())
	    class183_32_.method3051(i_21_);
	if (i_22_ != class183.method3054())
	    class183_32_.method3120(i_22_);
	if (bool)
	    class183_32_.method3020();
	if (i_18_ == Class595.aClass595_7834.anInt7852 * 847393323
	    && i_19_ > 3) {
	    class183_32_.method3016(2048);
	    class183_32_.method3098(180, 0, -180);
	}
	i_19_ &= 0x3;
	if (1 == i_19_)
	    class183_32_.method3016(4096);
	else if (i_19_ == 2)
	    class183_32_.method3016(8192);
	else if (3 == i_19_)
	    class183_32_.method3016(12288);
	if (aShortArray7893 != null) {
	    short[] is;
	    if (null != class596 && null != class596.aShortArray7857)
		is = class596.aShortArray7857;
	    else
		is = aShortArray7894;
	    for (int i_33_ = 0; i_33_ < aShortArray7893.length; i_33_++) {
		if (aByteArray7895 != null && i_33_ < aByteArray7895.length)
		    class183_32_.method3056(aShortArray7893[i_33_],
					    (aShortArray7886
					     [aByteArray7895[i_33_] & 0xff]));
		else
		    class183_32_.method3056(aShortArray7893[i_33_], is[i_33_]);
	    }
	}
	if (aShortArray7885 != null) {
	    short[] is;
	    if (null != class596 && null != class596.aShortArray7855)
		is = class596.aShortArray7855;
	    else
		is = aShortArray7897;
	    for (int i_34_ = 0; i_34_ < aShortArray7885.length; i_34_++)
		class183_32_.method3118(aShortArray7885[i_34_], is[i_34_]);
	}
	if (0 != aByte7906)
	    class183_32_.method3058(aByte7900, aByte7965, aByte7902,
				    aByte7906 & 0xff);
	if (1087546699 * anInt7929 != 128 || anInt7930 * 855774647 != 128
	    || 128 != anInt7931 * -1406965279)
	    class183_32_.method3021(1087546699 * anInt7929,
				    855774647 * anInt7930,
				    -1406965279 * anInt7931);
	if (-507159513 * anInt7932 != 0 || anInt7933 * 1699166449 != 0
	    || 0 != anInt7905 * 552542037)
	    class183_32_.method3098(anInt7932 * -507159513,
				    anInt7933 * 1699166449,
				    552542037 * anInt7905);
	class183_32_.method3012(i_23_);
	return class183_32_;
    }
    
    public final Class602 method9988(Interface18 interface18,
				     Interface20 interface20, int i) {
	int i_35_ = -1;
	if (1089951497 * anInt7912 != -1) {
	    Class318 class318
		= interface18.method107(anInt7912 * 1089951497, 1504047109);
	    if (null != class318)
		i_35_ = interface20.method119(class318, -1857388223);
	} else if (anInt7903 * -950236515 != -1) {
	    Class150 class150
		= interface18.method108(Class453.aClass453_4946,
					-950236515 * anInt7903, 1672356647);
	    if (null != class150)
		i_35_ = interface20.method120(class150, (byte) -44);
	}
	if (i_35_ < 0 || i_35_ >= anIntArray7943.length - 1) {
	    int i_36_ = anIntArray7943[anIntArray7943.length - 1];
	    if (-1 != i_36_)
		return ((Class602)
			anInterface14_7934.method91(i_36_, -363401816));
	    return null;
	}
	if (-1 == anIntArray7943[i_35_])
	    return null;
	return (Class602) anInterface14_7934.method91(anIntArray7943[i_35_],
						      -877189112);
    }
    
    public int method9989(int i, int i_37_, byte i_38_) {
	if (null == aClass9_7952)
	    return i_37_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_7952.method579((long) i);
	if (class534_sub39 == null)
	    return i_37_;
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public String method9990(int i, String string, byte i_39_) {
	if (null == aClass9_7952)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_7952.method579((long) i);
	if (class534_sub6 == null)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    void method9991() {
	if (anInt7907 * -2134171963 == -1) {
	    anInt7907 = 0;
	    if (aByteArray7926 != null && aByteArray7926.length == 1
		&& (847393323 * Class595.aClass595_7836.anInt7852
		    == aByteArray7926[0]))
		anInt7907 = 384348173;
	    for (int i = 0; i < 5; i++) {
		if (aStringArray7920[i] != null) {
		    anInt7907 = 384348173;
		    break;
		}
	    }
	}
	if (-1 == 1288889595 * anInt7940)
	    anInt7940 = (0 != anInt7923 * -499459421 ? 1 : 0) * -694974925;
	if (method10015((byte) -101) || aBool7957 || anIntArray7943 != null)
	    aBool7961 = true;
	if (-2134171963 * anInt7907 <= 0
	    && aClass593_7963 != Class593.aClass593_7821) {
	    /* empty */
	}
    }
    
    Class183 method9992(Class185 class185, int i, int i_40_, int i_41_,
			Class596 class596) {
	int i_42_ = 64 + anInt7898 * 57540739;
	int i_43_ = 850 + anInt7919 * -1886190073;
	int i_44_ = i;
	boolean bool
	    = (aBool7927
	       || (847393323 * Class595.aClass595_7829.anInt7852 == i_40_
		   && i_41_ > 3));
	if (bool)
	    i |= 0x10;
	if (0 == i_41_) {
	    if (128 != 1087546699 * anInt7929 || -507159513 * anInt7932 != 0)
		i |= 0x1;
	    if (128 != anInt7931 * -1406965279 || 0 != 552542037 * anInt7905)
		i |= 0x4;
	} else
	    i |= 0xd;
	if (anInt7930 * 855774647 != 128 || 0 != anInt7933 * 1699166449)
	    i |= 0x2;
	if (null != aShortArray7893)
	    i |= 0x4000;
	if (null != aShortArray7885)
	    i |= 0x8000;
	if (0 != aByte7906)
	    i |= 0x80000;
	Class183 class183 = null;
	if (aByteArray7926 != null) {
	    int i_45_ = -1;
	    for (int i_46_ = 0; i_46_ < aByteArray7926.length; i_46_++) {
		if (i_40_ == aByteArray7926[i_46_]) {
		    i_45_ = i_46_;
		    break;
		}
	    }
	    if (i_45_ == -1)
		return null;
	    int[] is
		= (class596 != null && class596.anIntArray7856 != null
		   ? class596.anIntArray7856 : anIntArrayArray7891[i_45_]);
	    int i_47_ = is.length;
	    if (i_47_ > 0) {
		long l = (long) (class185.anInt2001 * 2098753835);
		for (int i_48_ = 0; i_48_ < i_47_; i_48_++)
		    l = 67783L * l + (long) is[i_48_];
		synchronized (aClass601_7888.aClass203_7878) {
		    class183 = ((Class183)
				aClass601_7888.aClass203_7878.method3871(l));
		}
		if (null != class183) {
		    if (i_42_ != class183.method3053())
			i |= 0x1000;
		    if (i_43_ != class183.method3054())
			i |= 0x2000;
		}
		if (null == class183
		    || class185.method3330(class183.method3101(), i) != 0) {
		    int i_49_ = i | 0x1f01f;
		    if (null != class183)
			i_49_ = class185.method3331(i_49_,
						    class183.method3101());
		    Class187 class187 = null;
		    synchronized (aClass601_7888.aClass187Array7883) {
			for (int i_50_ = 0; i_50_ < i_47_; i_50_++) {
			    byte[] is_51_
				= aClass601_7888.method9957(is[i_50_],
							    1332600626);
			    if (null == is_51_) {
				Class183 class183_52_ = null;
				return class183_52_;
			    }
			    class187 = new Class187(is_51_);
			    if (class187.anInt2082 < 13)
				class187.method3723(2);
			    if (i_47_ > 1)
				aClass601_7888.aClass187Array7883[i_50_]
				    = class187;
			}
			if (i_47_ > 1)
			    class187 = new Class187((aClass601_7888
						     .aClass187Array7883),
						    i_47_);
		    }
		    class183 = class185.method3329(class187, i_49_,
						   (aClass601_7888.anInt7880
						    * -1707528565),
						   i_42_, i_43_);
		    synchronized (aClass601_7888.aClass203_7878) {
			aClass601_7888.aClass203_7878.method3893(class183, l);
		    }
		}
	    }
	}
	if (null == class183)
	    return null;
	Class183 class183_53_ = class183.method3011((byte) 0, i, true);
	if (i_42_ != class183.method3053())
	    class183_53_.method3051(i_42_);
	if (i_43_ != class183.method3054())
	    class183_53_.method3120(i_43_);
	if (bool)
	    class183_53_.method3020();
	if (i_40_ == Class595.aClass595_7834.anInt7852 * 847393323
	    && i_41_ > 3) {
	    class183_53_.method3016(2048);
	    class183_53_.method3098(180, 0, -180);
	}
	i_41_ &= 0x3;
	if (1 == i_41_)
	    class183_53_.method3016(4096);
	else if (i_41_ == 2)
	    class183_53_.method3016(8192);
	else if (3 == i_41_)
	    class183_53_.method3016(12288);
	if (aShortArray7893 != null) {
	    short[] is;
	    if (null != class596 && null != class596.aShortArray7857)
		is = class596.aShortArray7857;
	    else
		is = aShortArray7894;
	    for (int i_54_ = 0; i_54_ < aShortArray7893.length; i_54_++) {
		if (aByteArray7895 != null && i_54_ < aByteArray7895.length)
		    class183_53_.method3056(aShortArray7893[i_54_],
					    (aShortArray7886
					     [aByteArray7895[i_54_] & 0xff]));
		else
		    class183_53_.method3056(aShortArray7893[i_54_], is[i_54_]);
	    }
	}
	if (aShortArray7885 != null) {
	    short[] is;
	    if (null != class596 && null != class596.aShortArray7855)
		is = class596.aShortArray7855;
	    else
		is = aShortArray7897;
	    for (int i_55_ = 0; i_55_ < aShortArray7885.length; i_55_++)
		class183_53_.method3118(aShortArray7885[i_55_], is[i_55_]);
	}
	if (0 != aByte7906)
	    class183_53_.method3058(aByte7900, aByte7965, aByte7902,
				    aByte7906 & 0xff);
	if (1087546699 * anInt7929 != 128 || anInt7930 * 855774647 != 128
	    || 128 != anInt7931 * -1406965279)
	    class183_53_.method3021(1087546699 * anInt7929,
				    855774647 * anInt7930,
				    -1406965279 * anInt7931);
	if (-507159513 * anInt7932 != 0 || anInt7933 * 1699166449 != 0
	    || 0 != anInt7905 * 552542037)
	    class183_53_.method3098(anInt7932 * -507159513,
				    anInt7933 * 1699166449,
				    552542037 * anInt7905);
	class183_53_.method3012(i_44_);
	return class183_53_;
    }
    
    public boolean method9993(int i) {
	return anIntArray7914 != null && anIntArray7914.length > 1;
    }
    
    public int method9994(byte i) {
	if (anIntArray7914 != null) {
	    if (anIntArray7914.length > 1) {
		int i_56_ = (int) (Math.random() * 65535.0);
		for (int i_57_ = 0; i_57_ < anIntArray7914.length; i_57_++) {
		    if (i_56_ <= anIntArray7942[i_57_])
			return anIntArray7914[i_57_];
		    i_56_ -= anIntArray7942[i_57_];
		}
	    } else
		return anIntArray7914[0];
	}
	return -1;
    }
    
    public final Class602 method9995(Interface18 interface18,
				     Interface20 interface20) {
	int i = -1;
	if (1089951497 * anInt7912 != -1) {
	    Class318 class318
		= interface18.method107(anInt7912 * 1089951497, 1504047109);
	    if (null != class318)
		i = interface20.method119(class318, -1694304409);
	} else if (anInt7903 * -950236515 != -1) {
	    Class150 class150
		= interface18.method108(Class453.aClass453_4946,
					-950236515 * anInt7903, 1633435765);
	    if (null != class150)
		i = interface20.method120(class150, (byte) -39);
	}
	if (i < 0 || i >= anIntArray7943.length - 1) {
	    int i_58_ = anIntArray7943[anIntArray7943.length - 1];
	    if (-1 != i_58_)
		return ((Class602)
			anInterface14_7934.method91(i_58_, 757696887));
	    return null;
	}
	if (-1 == anIntArray7943[i])
	    return null;
	return ((Class602)
		anInterface14_7934.method91(anIntArray7943[i], -1548620309));
    }
    
    public boolean method9996(int i, int i_59_) {
	if (anIntArray7914 != null && -1 != i) {
	    for (int i_60_ = 0; i_60_ < anIntArray7914.length; i_60_++) {
		if (anIntArray7914[i_60_] == i)
		    return true;
	    }
	}
	return false;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_61_ = class534_sub40.method16527(-1513715726);
	    if (i_61_ == 0)
		break;
	    method10004(class534_sub40, i_61_, (byte) 24);
	}
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-27221109);
	    if (i == 0)
		break;
	    method10004(class534_sub40, i, (byte) 24);
	}
    }
    
    public final synchronized Class7 method9997
	(Class185 class185, int i, int i_62_, int i_63_, Class151 class151,
	 Class151 class151_64_, int i_65_, int i_66_, int i_67_, boolean bool,
	 Class596 class596, int i_68_) {
	if (Class459.method7439(i_62_, (byte) -88))
	    i_62_ = 847393323 * Class595.aClass595_7834.anInt7852;
	long l
	    = (long) ((i_62_ << 3) + (-1562722583 * anInt7887 << 10) + i_63_);
	l |= (long) (class185.anInt2001 * 2098753835 << 29);
	if (class596 != null)
	    l |= 5594098338423607855L * class596.aLong7858 << 32;
	int i_69_ = i;
	if (aByte7908 == 3)
	    i_69_ |= 0x7;
	else {
	    if (aByte7908 != 0 || 0 != 1273961079 * anInt7918)
		i_69_ |= 0x2;
	    if (0 != anInt7935 * -1393558471)
		i_69_ |= 0x1;
	    if (0 != 763951193 * anInt7936)
		i_69_ |= 0x4;
	}
	if (bool)
	    i_69_ |= 0x40000;
	boolean bool_70_
	    = aByte7908 != 0 && (null != class151 || null != class151_64_);
	boolean bool_71_
	    = (0 != -1393558471 * anInt7935 || anInt7918 * 1273961079 != 0
	       || 0 != 763951193 * anInt7936);
	Class7 class7;
	synchronized (aClass601_7888.aClass203_7879) {
	    class7 = (Class7) aClass601_7888.aClass203_7879.method3871(l);
	}
	Class183 class183
	    = (Class183) (null != class7 ? class7.anObject58 : null);
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	if (null == class183
	    || class185.method3330(class183.method3101(), i_69_) != 0) {
	    if (null != class183)
		i_69_ = class185.method3331(i_69_, class183.method3101());
	    int i_72_ = i_69_;
	    if (847393323 * Class595.aClass595_7836.anInt7852 == i_62_
		&& i_63_ > 3)
		i_72_ |= 0x5;
	    class183 = method9987(class185, i_72_, i_62_, i_63_, class596,
				  2113275141);
	    if (class183 == null)
		return null;
	    if (i_62_ == 847393323 * Class595.aClass595_7836.anInt7852
		&& i_63_ > 3)
		class183.method3015(2048);
	    if (bool && !bool_70_ && !bool_71_)
		class534_sub18_sub16 = class183.method3040(null);
	    class183.method3012(i_69_);
	    class7 = new Class7(class183, class534_sub18_sub16);
	    synchronized (aClass601_7888.aClass203_7879) {
		aClass601_7888.aClass203_7879.method3893(class7, l);
	    }
	} else {
	    class534_sub18_sub16 = (Class534_Sub18_Sub16) class7.anObject59;
	    if (bool && class534_sub18_sub16 == null && !bool_70_ && !bool_71_)
		class534_sub18_sub16
		    = (Class534_Sub18_Sub16) (class7.anObject59
					      = class183.method3040(null));
	}
	if (bool_70_ || bool_71_) {
	    class183 = class183.method3011((byte) 0, i_69_, true);
	    if (bool_70_)
		class183.method3022(aByte7908, 1064510741 * anInt7909,
				    class151, class151_64_, i_65_, i_66_,
				    i_67_);
	    if (bool_71_)
		class183.method3098(anInt7935 * -1393558471,
				    anInt7918 * 1273961079,
				    anInt7936 * 763951193);
	    if (bool)
		class534_sub18_sub16 = class183.method3040(null);
	    class183.method3012(i);
	} else
	    class183 = class183.method3011((byte) 0, i, true);
	aClass601_7888.aClass7_7881.anObject58 = class183;
	aClass601_7888.aClass7_7881.anObject59 = class534_sub18_sub16;
	return aClass601_7888.aClass7_7881;
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1862749583);
	    if (i == 0)
		break;
	    method10004(class534_sub40, i, (byte) 24);
	}
    }
    
    public void method83() {
	method9982(1464032327);
	if (aBool7939)
	    anInt7923 = 0;
	if (!aClass601_7888.aBool7877 && aBool7964) {
	    aStringArray7920 = null;
	    anIntArray7958 = null;
	}
    }
    
    public final synchronized Class7 method9998
	(Class185 class185, int i, int i_73_, int i_74_, Class151 class151,
	 Class151 class151_75_, int i_76_, int i_77_, int i_78_, boolean bool,
	 Class596 class596) {
	if (Class459.method7439(i_73_, (byte) -91))
	    i_73_ = 847393323 * Class595.aClass595_7834.anInt7852;
	long l
	    = (long) ((i_73_ << 3) + (-1562722583 * anInt7887 << 10) + i_74_);
	l |= (long) (class185.anInt2001 * 2098753835 << 29);
	if (class596 != null)
	    l |= 5594098338423607855L * class596.aLong7858 << 32;
	int i_79_ = i;
	if (aByte7908 == 3)
	    i_79_ |= 0x7;
	else {
	    if (aByte7908 != 0 || 0 != 1273961079 * anInt7918)
		i_79_ |= 0x2;
	    if (0 != anInt7935 * -1393558471)
		i_79_ |= 0x1;
	    if (0 != 763951193 * anInt7936)
		i_79_ |= 0x4;
	}
	if (bool)
	    i_79_ |= 0x40000;
	boolean bool_80_
	    = aByte7908 != 0 && (null != class151 || null != class151_75_);
	boolean bool_81_
	    = (0 != -1393558471 * anInt7935 || anInt7918 * 1273961079 != 0
	       || 0 != 763951193 * anInt7936);
	Class7 class7;
	synchronized (aClass601_7888.aClass203_7879) {
	    class7 = (Class7) aClass601_7888.aClass203_7879.method3871(l);
	}
	Class183 class183
	    = (Class183) (null != class7 ? class7.anObject58 : null);
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	if (null == class183
	    || class185.method3330(class183.method3101(), i_79_) != 0) {
	    if (null != class183)
		i_79_ = class185.method3331(i_79_, class183.method3101());
	    int i_82_ = i_79_;
	    if (847393323 * Class595.aClass595_7836.anInt7852 == i_73_
		&& i_74_ > 3)
		i_82_ |= 0x5;
	    class183 = method9987(class185, i_82_, i_73_, i_74_, class596,
				  2113275141);
	    if (class183 == null)
		return null;
	    if (i_73_ == 847393323 * Class595.aClass595_7836.anInt7852
		&& i_74_ > 3)
		class183.method3015(2048);
	    if (bool && !bool_80_ && !bool_81_)
		class534_sub18_sub16 = class183.method3040(null);
	    class183.method3012(i_79_);
	    class7 = new Class7(class183, class534_sub18_sub16);
	    synchronized (aClass601_7888.aClass203_7879) {
		aClass601_7888.aClass203_7879.method3893(class7, l);
	    }
	} else {
	    class534_sub18_sub16 = (Class534_Sub18_Sub16) class7.anObject59;
	    if (bool && class534_sub18_sub16 == null && !bool_80_ && !bool_81_)
		class534_sub18_sub16
		    = (Class534_Sub18_Sub16) (class7.anObject59
					      = class183.method3040(null));
	}
	if (bool_80_ || bool_81_) {
	    class183 = class183.method3011((byte) 0, i_79_, true);
	    if (bool_80_)
		class183.method3022(aByte7908, 1064510741 * anInt7909,
				    class151, class151_75_, i_76_, i_77_,
				    i_78_);
	    if (bool_81_)
		class183.method3098(anInt7935 * -1393558471,
				    anInt7918 * 1273961079,
				    anInt7936 * 763951193);
	    if (bool)
		class534_sub18_sub16 = class183.method3040(null);
	    class183.method3012(i);
	} else
	    class183 = class183.method3011((byte) 0, i, true);
	aClass601_7888.aClass7_7881.anObject58 = class183;
	aClass601_7888.aClass7_7881.anObject59 = class534_sub18_sub16;
	return aClass601_7888.aClass7_7881;
    }
    
    void method9999(Class534_Sub40 class534_sub40, int i) {
	if (1 == i) {
	    int i_83_ = class534_sub40.method16527(-1504753711);
	    aByteArray7926 = new byte[i_83_];
	    anIntArrayArray7891 = new int[i_83_][];
	    for (int i_84_ = 0; i_84_ < i_83_; i_84_++) {
		aByteArray7926[i_84_] = class534_sub40.method16586((byte) 1);
		int i_85_ = class534_sub40.method16527(-1532207207);
		anIntArrayArray7891[i_84_] = new int[i_85_];
		for (int i_86_ = 0; i_86_ < i_85_; i_86_++)
		    anIntArrayArray7891[i_84_][i_86_]
			= class534_sub40.method16550((byte) -18);
	    }
	} else if (i == 2)
	    aString7892 = class534_sub40.method16541((byte) -28).intern();
	else if (i == 14)
	    anInt7904 = class534_sub40.method16527(-1119828415) * 1578780151;
	else if (i == 15)
	    anInt7928 = class534_sub40.method16527(646248928) * -742973319;
	else if (i == 17)
	    anInt7923 = 0;
	else if (18 != i) {
	    if (i == 19)
		anInt7907 = class534_sub40.method16527(-238789496) * 384348173;
	    else if (i == 21)
		aByte7908 = (byte) 1;
	    else if (22 == i)
		aBool7910 = true;
	    else if (23 == i)
		anInt7911 = 353749263;
	    else if (24 == i) {
		int i_87_ = class534_sub40.method16550((byte) -62);
		if (-1 != i_87_)
		    anIntArray7914 = new int[] { i_87_ };
	    } else if (i == 27)
		anInt7923 = -880695029;
	    else if (28 == i)
		anInt7917 = ((class534_sub40.method16527(-1412458220) << 2)
			     * -513186995);
	    else if (29 == i)
		anInt7898 = class534_sub40.method16586((byte) 1) * -1412805077;
	    else if (39 == i)
		anInt7919 = class534_sub40.method16586((byte) 1) * -1354914157;
	    else if (i >= 30 && i < 35)
		aStringArray7920[i - 30]
		    = class534_sub40.method16541((byte) -29).intern();
	    else if (40 == i) {
		int i_88_ = class534_sub40.method16527(-1505222222);
		aShortArray7893 = new short[i_88_];
		aShortArray7894 = new short[i_88_];
		for (int i_89_ = 0; i_89_ < i_88_; i_89_++) {
		    aShortArray7893[i_89_]
			= (short) class534_sub40.method16529((byte) 1);
		    aShortArray7894[i_89_]
			= (short) class534_sub40.method16529((byte) 1);
		}
	    } else if (41 == i) {
		int i_90_ = class534_sub40.method16527(-1774698187);
		aShortArray7885 = new short[i_90_];
		aShortArray7897 = new short[i_90_];
		for (int i_91_ = 0; i_91_ < i_90_; i_91_++) {
		    aShortArray7885[i_91_]
			= (short) class534_sub40.method16529((byte) 1);
		    aShortArray7897[i_91_]
			= (short) class534_sub40.method16529((byte) 1);
		}
	    } else if (i == 42) {
		int i_92_ = class534_sub40.method16527(1280290149);
		aByteArray7895 = new byte[i_92_];
		for (int i_93_ = 0; i_93_ < i_92_; i_93_++)
		    aByteArray7895[i_93_]
			= class534_sub40.method16586((byte) 1);
	    } else if (44 == i) {
		int i_94_ = class534_sub40.method16529((byte) 1);
		int i_95_ = 0;
		for (int i_96_ = i_94_; i_96_ > 0; i_96_ >>= 1)
		    i_95_++;
		aByteArray7921 = new byte[i_95_];
		byte i_97_ = 0;
		for (int i_98_ = 0; i_98_ < i_95_; i_98_++) {
		    if ((i_94_ & 1 << i_98_) > 0) {
			aByteArray7921[i_98_] = i_97_;
			i_97_++;
		    } else
			aByteArray7921[i_98_] = (byte) -1;
		}
	    } else if (i == 45) {
		int i_99_ = class534_sub40.method16529((byte) 1);
		int i_100_ = 0;
		for (int i_101_ = i_99_; i_101_ > 0; i_101_ >>= 1)
		    i_100_++;
		aByteArray7899 = new byte[i_100_];
		byte i_102_ = 0;
		for (int i_103_ = 0; i_103_ < i_100_; i_103_++) {
		    if ((i_99_ & 1 << i_103_) > 0) {
			aByteArray7899[i_103_] = i_102_;
			i_102_++;
		    } else
			aByteArray7899[i_103_] = (byte) -1;
		}
	    } else if (i == 62)
		aBool7927 = true;
	    else if (64 == i)
		aBool7959 = false;
	    else if (i == 65)
		anInt7929 = class534_sub40.method16529((byte) 1) * 1242938467;
	    else if (i == 66)
		anInt7930 = class534_sub40.method16529((byte) 1) * 341432327;
	    else if (67 == i)
		anInt7931 = class534_sub40.method16529((byte) 1) * 92183073;
	    else if (69 == i)
		class534_sub40.method16527(1243776350);
	    else if (i == 70)
		anInt7932 = ((class534_sub40.method16530((byte) -2) << 2)
			     * 1046107031);
	    else if (i == 71)
		anInt7933 = ((class534_sub40.method16530((byte) -80) << 2)
			     * -1428637679);
	    else if (72 == i)
		anInt7905 = ((class534_sub40.method16530((byte) -92) << 2)
			     * 1678501373);
	    else if (i == 73)
		aBool7938 = true;
	    else if (74 == i)
		aBool7939 = true;
	    else if (75 == i)
		anInt7940
		    = class534_sub40.method16527(-1390449624) * -694974925;
	    else if (77 == i || i == 92) {
		anInt7912 = class534_sub40.method16529((byte) 1) * -689871047;
		if (1089951497 * anInt7912 == 65535)
		    anInt7912 = 689871047;
		anInt7903 = class534_sub40.method16529((byte) 1) * 99503029;
		if (65535 == anInt7903 * -950236515)
		    anInt7903 = -99503029;
		int i_104_ = -1;
		if (i == 92)
		    i_104_ = class534_sub40.method16550((byte) -27);
		int i_105_ = class534_sub40.method16527(-1942340456);
		anIntArray7943 = new int[i_105_ + 2];
		for (int i_106_ = 0; i_106_ <= i_105_; i_106_++)
		    anIntArray7943[i_106_]
			= class534_sub40.method16550((byte) 54);
		anIntArray7943[i_105_ + 1] = i_104_;
	    } else if (i == 78) {
		anInt7945 = class534_sub40.method16529((byte) 1) * -2116677683;
		anInt7946
		    = class534_sub40.method16527(-14227273) * -1680210341;
	    } else if (79 == i) {
		anInt7915 = class534_sub40.method16529((byte) 1) * -1894995015;
		anInt7950 = class534_sub40.method16529((byte) 1) * 1008959683;
		anInt7946
		    = class534_sub40.method16527(787002282) * -1680210341;
		int i_107_ = class534_sub40.method16527(-1929094569);
		anIntArray7951 = new int[i_107_];
		for (int i_108_ = 0; i_108_ < i_107_; i_108_++)
		    anIntArray7951[i_108_]
			= class534_sub40.method16529((byte) 1);
	    } else if (i == 81) {
		aByte7908 = (byte) 2;
		anInt7909
		    = class534_sub40.method16527(1415130085) * -1375585024;
	    } else if (82 == i)
		aBool7953 = true;
	    else if (i == 88)
		aBool7896 = false;
	    else if (89 == i)
		aBool7944 = false;
	    else if (i == 91)
		aBool7964 = true;
	    else if (i == 93) {
		aByte7908 = (byte) 3;
		anInt7909 = class534_sub40.method16529((byte) 1) * 1806565949;
	    } else if (i == 94)
		aByte7908 = (byte) 4;
	    else if (i == 95) {
		aByte7908 = (byte) 5;
		anInt7909
		    = class534_sub40.method16530((byte) -92) * 1806565949;
	    } else if (97 == i)
		aBool7924 = true;
	    else if (98 == i)
		aBool7957 = true;
	    else if (99 == i || 100 == i) {
		class534_sub40.method16527(-1001338849);
		class534_sub40.method16529((byte) 1);
	    } else if (i == 101)
		anInt7925
		    = class534_sub40.method16527(-141265158) * -1148718285;
	    else if (102 == i)
		anInt7890 = class534_sub40.method16529((byte) 1) * 1279885027;
	    else if (i == 103)
		anInt7911 = 0;
	    else if (104 == i)
		anInt7948
		    = class534_sub40.method16527(873009481) * -1951164827;
	    else if (i == 105)
		aBool7955 = true;
	    else if (i == 106) {
		int i_109_ = class534_sub40.method16527(-1108836411);
		int i_110_ = 0;
		anIntArray7914 = new int[i_109_];
		anIntArray7942 = new int[i_109_];
		for (int i_111_ = 0; i_111_ < i_109_; i_111_++) {
		    anIntArray7914[i_111_]
			= class534_sub40.method16550((byte) 21);
		    i_110_ += anIntArray7942[i_111_]
			= class534_sub40.method16527(157100680);
		}
		for (int i_112_ = 0; i_112_ < i_109_; i_112_++)
		    anIntArray7942[i_112_]
			= anIntArray7942[i_112_] * 65535 / i_110_;
	    } else if (107 == i)
		anInt7949 = class534_sub40.method16529((byte) 1) * 1042084459;
	    else if (i >= 150 && i < 155) {
		aStringArray7920[i - 150]
		    = class534_sub40.method16541((byte) -102).intern();
		if (!aClass601_7888.aBool7877)
		    aStringArray7920[i - 150] = null;
	    } else if (160 == i) {
		int i_113_ = class534_sub40.method16527(2119012384);
		anIntArray7958 = new int[i_113_];
		for (int i_114_ = 0; i_114_ < i_113_; i_114_++)
		    anIntArray7958[i_114_]
			= class534_sub40.method16529((byte) 1);
	    } else if (162 == i) {
		aByte7908 = (byte) 3;
		anInt7909
		    = class534_sub40.method16533(-258848859) * 1806565949;
	    } else if (163 == i) {
		aByte7900 = class534_sub40.method16586((byte) 1);
		aByte7965 = class534_sub40.method16586((byte) 1);
		aByte7902 = class534_sub40.method16586((byte) 1);
		aByte7906 = class534_sub40.method16586((byte) 1);
	    } else if (164 == i)
		anInt7935
		    = class534_sub40.method16530((byte) -107) * 2025140745;
	    else if (i == 165)
		anInt7918
		    = class534_sub40.method16530((byte) -63) * -487661753;
	    else if (166 == i)
		anInt7936
		    = class534_sub40.method16530((byte) -71) * 1761005545;
	    else if (i == 167)
		anInt7941 = class534_sub40.method16529((byte) 1) * 1246204931;
	    else if (168 != i && i != 169) {
		if (i == 170)
		    anInt7954
			= class534_sub40.method16546(-1706829710) * -921364721;
		else if (171 == i)
		    anInt7913
			= class534_sub40.method16546(-1706829710) * 658564109;
		else if (173 == i) {
		    anInt7901
			= class534_sub40.method16529((byte) 1) * -1566027831;
		    anInt7960
			= class534_sub40.method16529((byte) 1) * 1803353737;
		} else if (i == 177)
		    aBool7961 = true;
		else if (178 == i)
		    anInt7947
			= class534_sub40.method16527(-84680621) * 878557669;
		else if (186 == i)
		    aClass593_7963
			= ((Class593)
			   Class448.method7319(Class658.method10914((byte) 15),
					       class534_sub40
						   .method16527(-2084035661),
					       2088438307));
		else if (188 != i) {
		    if (i == 189)
			aBool7962 = true;
		    else if (i >= 190 && i < 196) {
			if (anIntArray7922 == null) {
			    anIntArray7922 = new int[6];
			    Arrays.fill(anIntArray7922, -1);
			}
			anIntArray7922[i - 190]
			    = class534_sub40.method16529((byte) 1);
		    } else if (i == 196)
			Class448.method7319(Class191.method3769(2139882933),
					    class534_sub40
						.method16527(-1555665357),
					    2088438307);
		    else if (197 == i)
			Class448.method7319(Class62.method1262((byte) 127),
					    class534_sub40
						.method16527(786634688),
					    2088438307);
		    else if (198 != i && 199 != i) {
			if (i == 200)
			    aBool7916 = true;
			else if (201 == i) {
			    aClass432_7956 = new Class432();
			    aClass432_7956.aFloat4847
				= (float) class534_sub40
					      .method16545((byte) -69);
			    aClass432_7956.aFloat4845
				= (float) class534_sub40
					      .method16545((byte) -5);
			    aClass432_7956.aFloat4844
				= (float) class534_sub40
					      .method16545((byte) -30);
			    aClass432_7956.aFloat4848
				= (float) class534_sub40
					      .method16545((byte) -81);
			    aClass432_7956.aFloat4850
				= (float) class534_sub40
					      .method16545((byte) -109);
			    aClass432_7956.aFloat4849
				= (float) class534_sub40
					      .method16545((byte) -78);
			} else if (i == 249) {
			    int i_115_
				= class534_sub40.method16527(-1348715750);
			    if (null == aClass9_7952) {
				int i_116_
				    = Class162.method2640(i_115_, (byte) 51);
				aClass9_7952 = new Class9(i_116_);
			    }
			    for (int i_117_ = 0; i_117_ < i_115_; i_117_++) {
				boolean bool
				    = (class534_sub40.method16527(-297072647)
				       == 1);
				int i_118_
				    = class534_sub40.method16531(896821403);
				Class534 class534;
				if (bool)
				    class534
					= new Class534_Sub6(class534_sub40
								.method16541
								((byte) -124)
								.intern());
				else
				    class534
					= new Class534_Sub39(class534_sub40
								 .method16533
							     (-258848859));
				aClass9_7952.method581(class534,
						       (long) i_118_);
			    }
			}
		    }
		}
	    }
	}
    }
    
    public final boolean method10000(int i, byte i_119_) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i_120_ = 0; i_120_ < aByteArray7926.length; i_120_++) {
	    if (i == aByteArray7926[i_120_]) {
		for (int i_121_ = 0;
		     i_121_ < anIntArrayArray7891[i_120_].length; i_121_++) {
		    if (!aClass601_7888.method9958((anIntArrayArray7891[i_120_]
						    [i_121_]),
						   1189319673))
			bool = false;
		}
	    }
	}
	return bool;
    }
    
    void method10001() {
	if (anInt7907 * -2134171963 == -1) {
	    anInt7907 = 0;
	    if (aByteArray7926 != null && aByteArray7926.length == 1
		&& (847393323 * Class595.aClass595_7836.anInt7852
		    == aByteArray7926[0]))
		anInt7907 = 384348173;
	    for (int i = 0; i < 5; i++) {
		if (aStringArray7920[i] != null) {
		    anInt7907 = 384348173;
		    break;
		}
	    }
	}
	if (-1 == 1288889595 * anInt7940)
	    anInt7940 = (0 != anInt7923 * -499459421 ? 1 : 0) * -694974925;
	if (method10015((byte) -33) || aBool7957 || anIntArray7943 != null)
	    aBool7961 = true;
	if (-2134171963 * anInt7907 <= 0
	    && aClass593_7963 != Class593.aClass593_7821) {
	    /* empty */
	}
    }
    
    void method10002() {
	if (anInt7907 * -2134171963 == -1) {
	    anInt7907 = 0;
	    if (aByteArray7926 != null && aByteArray7926.length == 1
		&& (847393323 * Class595.aClass595_7836.anInt7852
		    == aByteArray7926[0]))
		anInt7907 = 384348173;
	    for (int i = 0; i < 5; i++) {
		if (aStringArray7920[i] != null) {
		    anInt7907 = 384348173;
		    break;
		}
	    }
	}
	if (-1 == 1288889595 * anInt7940)
	    anInt7940 = (0 != anInt7923 * -499459421 ? 1 : 0) * -694974925;
	if (method10015((byte) -28) || aBool7957 || anIntArray7943 != null)
	    aBool7961 = true;
	if (-2134171963 * anInt7907 <= 0
	    && aClass593_7963 != Class593.aClass593_7821) {
	    /* empty */
	}
    }
    
    public final boolean method10003(int i) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i_122_ = 0; i_122_ < aByteArray7926.length; i_122_++) {
	    if (i == aByteArray7926[i_122_]) {
		for (int i_123_ = 0;
		     i_123_ < anIntArrayArray7891[i_122_].length; i_123_++) {
		    if (!aClass601_7888.method9958((anIntArrayArray7891[i_122_]
						    [i_123_]),
						   1189319673))
			bool = false;
		}
	    }
	}
	return bool;
    }
    
    void method10004(Class534_Sub40 class534_sub40, int i, byte i_124_) {
	if (1 == i) {
	    int i_125_ = class534_sub40.method16527(1945113435);
	    aByteArray7926 = new byte[i_125_];
	    anIntArrayArray7891 = new int[i_125_][];
	    for (int i_126_ = 0; i_126_ < i_125_; i_126_++) {
		aByteArray7926[i_126_] = class534_sub40.method16586((byte) 1);
		int i_127_ = class534_sub40.method16527(-950458135);
		anIntArrayArray7891[i_126_] = new int[i_127_];
		for (int i_128_ = 0; i_128_ < i_127_; i_128_++)
		    anIntArrayArray7891[i_126_][i_128_]
			= class534_sub40.method16550((byte) 37);
	    }
	} else if (i == 2)
	    aString7892 = class534_sub40.method16541((byte) -18).intern();
	else if (i == 14)
	    anInt7904 = class534_sub40.method16527(-1649729804) * 1578780151;
	else if (i == 15)
	    anInt7928 = class534_sub40.method16527(1391228154) * -742973319;
	else if (i == 17)
	    anInt7923 = 0;
	else if (18 != i) {
	    if (i == 19)
		anInt7907 = class534_sub40.method16527(-887933963) * 384348173;
	    else if (i == 21)
		aByte7908 = (byte) 1;
	    else if (22 == i)
		aBool7910 = true;
	    else if (23 == i)
		anInt7911 = 353749263;
	    else if (24 == i) {
		int i_129_ = class534_sub40.method16550((byte) 73);
		if (-1 != i_129_)
		    anIntArray7914 = new int[] { i_129_ };
	    } else if (i == 27)
		anInt7923 = -880695029;
	    else if (28 == i)
		anInt7917 = ((class534_sub40.method16527(-285109150) << 2)
			     * -513186995);
	    else if (29 == i)
		anInt7898 = class534_sub40.method16586((byte) 1) * -1412805077;
	    else if (39 == i)
		anInt7919 = class534_sub40.method16586((byte) 1) * -1354914157;
	    else if (i >= 30 && i < 35)
		aStringArray7920[i - 30]
		    = class534_sub40.method16541((byte) -109).intern();
	    else if (40 == i) {
		int i_130_ = class534_sub40.method16527(978469647);
		aShortArray7893 = new short[i_130_];
		aShortArray7894 = new short[i_130_];
		for (int i_131_ = 0; i_131_ < i_130_; i_131_++) {
		    aShortArray7893[i_131_]
			= (short) class534_sub40.method16529((byte) 1);
		    aShortArray7894[i_131_]
			= (short) class534_sub40.method16529((byte) 1);
		}
	    } else if (41 == i) {
		int i_132_ = class534_sub40.method16527(-369028364);
		aShortArray7885 = new short[i_132_];
		aShortArray7897 = new short[i_132_];
		for (int i_133_ = 0; i_133_ < i_132_; i_133_++) {
		    aShortArray7885[i_133_]
			= (short) class534_sub40.method16529((byte) 1);
		    aShortArray7897[i_133_]
			= (short) class534_sub40.method16529((byte) 1);
		}
	    } else if (i == 42) {
		int i_134_ = class534_sub40.method16527(759031564);
		aByteArray7895 = new byte[i_134_];
		for (int i_135_ = 0; i_135_ < i_134_; i_135_++)
		    aByteArray7895[i_135_]
			= class534_sub40.method16586((byte) 1);
	    } else if (44 == i) {
		int i_136_ = class534_sub40.method16529((byte) 1);
		int i_137_ = 0;
		for (int i_138_ = i_136_; i_138_ > 0; i_138_ >>= 1)
		    i_137_++;
		aByteArray7921 = new byte[i_137_];
		byte i_139_ = 0;
		for (int i_140_ = 0; i_140_ < i_137_; i_140_++) {
		    if ((i_136_ & 1 << i_140_) > 0) {
			aByteArray7921[i_140_] = i_139_;
			i_139_++;
		    } else
			aByteArray7921[i_140_] = (byte) -1;
		}
	    } else if (i == 45) {
		int i_141_ = class534_sub40.method16529((byte) 1);
		int i_142_ = 0;
		for (int i_143_ = i_141_; i_143_ > 0; i_143_ >>= 1)
		    i_142_++;
		aByteArray7899 = new byte[i_142_];
		byte i_144_ = 0;
		for (int i_145_ = 0; i_145_ < i_142_; i_145_++) {
		    if ((i_141_ & 1 << i_145_) > 0) {
			aByteArray7899[i_145_] = i_144_;
			i_144_++;
		    } else
			aByteArray7899[i_145_] = (byte) -1;
		}
	    } else if (i == 62)
		aBool7927 = true;
	    else if (64 == i)
		aBool7959 = false;
	    else if (i == 65)
		anInt7929 = class534_sub40.method16529((byte) 1) * 1242938467;
	    else if (i == 66)
		anInt7930 = class534_sub40.method16529((byte) 1) * 341432327;
	    else if (67 == i)
		anInt7931 = class534_sub40.method16529((byte) 1) * 92183073;
	    else if (69 == i)
		class534_sub40.method16527(-146709398);
	    else if (i == 70)
		anInt7932 = ((class534_sub40.method16530((byte) -8) << 2)
			     * 1046107031);
	    else if (i == 71)
		anInt7933 = ((class534_sub40.method16530((byte) -75) << 2)
			     * -1428637679);
	    else if (72 == i)
		anInt7905 = ((class534_sub40.method16530((byte) -125) << 2)
			     * 1678501373);
	    else if (i == 73)
		aBool7938 = true;
	    else if (74 == i)
		aBool7939 = true;
	    else if (75 == i)
		anInt7940
		    = class534_sub40.method16527(-1875981915) * -694974925;
	    else if (77 == i || i == 92) {
		anInt7912 = class534_sub40.method16529((byte) 1) * -689871047;
		if (1089951497 * anInt7912 == 65535)
		    anInt7912 = 689871047;
		anInt7903 = class534_sub40.method16529((byte) 1) * 99503029;
		if (65535 == anInt7903 * -950236515)
		    anInt7903 = -99503029;
		int i_146_ = -1;
		if (i == 92)
		    i_146_ = class534_sub40.method16550((byte) -86);
		int i_147_ = class534_sub40.method16527(357522864);
		anIntArray7943 = new int[i_147_ + 2];
		for (int i_148_ = 0; i_148_ <= i_147_; i_148_++)
		    anIntArray7943[i_148_]
			= class534_sub40.method16550((byte) 39);
		anIntArray7943[i_147_ + 1] = i_146_;
	    } else if (i == 78) {
		anInt7945 = class534_sub40.method16529((byte) 1) * -2116677683;
		anInt7946 = class534_sub40.method16527(7832912) * -1680210341;
	    } else if (79 == i) {
		anInt7915 = class534_sub40.method16529((byte) 1) * -1894995015;
		anInt7950 = class534_sub40.method16529((byte) 1) * 1008959683;
		anInt7946
		    = class534_sub40.method16527(-947719366) * -1680210341;
		int i_149_ = class534_sub40.method16527(-673852400);
		anIntArray7951 = new int[i_149_];
		for (int i_150_ = 0; i_150_ < i_149_; i_150_++)
		    anIntArray7951[i_150_]
			= class534_sub40.method16529((byte) 1);
	    } else if (i == 81) {
		aByte7908 = (byte) 2;
		anInt7909 = class534_sub40.method16527(20941901) * -1375585024;
	    } else if (82 == i)
		aBool7953 = true;
	    else if (i == 88)
		aBool7896 = false;
	    else if (89 == i)
		aBool7944 = false;
	    else if (i == 91)
		aBool7964 = true;
	    else if (i == 93) {
		aByte7908 = (byte) 3;
		anInt7909 = class534_sub40.method16529((byte) 1) * 1806565949;
	    } else if (i == 94)
		aByte7908 = (byte) 4;
	    else if (i == 95) {
		aByte7908 = (byte) 5;
		anInt7909
		    = class534_sub40.method16530((byte) -128) * 1806565949;
	    } else if (97 == i)
		aBool7924 = true;
	    else if (98 == i)
		aBool7957 = true;
	    else if (99 == i || 100 == i) {
		class534_sub40.method16527(-1424630971);
		class534_sub40.method16529((byte) 1);
	    } else if (i == 101)
		anInt7925
		    = class534_sub40.method16527(635714391) * -1148718285;
	    else if (102 == i)
		anInt7890 = class534_sub40.method16529((byte) 1) * 1279885027;
	    else if (i == 103)
		anInt7911 = 0;
	    else if (104 == i)
		anInt7948 = class534_sub40.method16527(63726714) * -1951164827;
	    else if (i == 105)
		aBool7955 = true;
	    else if (i == 106) {
		int i_151_ = class534_sub40.method16527(174402360);
		int i_152_ = 0;
		anIntArray7914 = new int[i_151_];
		anIntArray7942 = new int[i_151_];
		for (int i_153_ = 0; i_153_ < i_151_; i_153_++) {
		    anIntArray7914[i_153_]
			= class534_sub40.method16550((byte) 47);
		    i_152_ += anIntArray7942[i_153_]
			= class534_sub40.method16527(1149883195);
		}
		for (int i_154_ = 0; i_154_ < i_151_; i_154_++)
		    anIntArray7942[i_154_]
			= anIntArray7942[i_154_] * 65535 / i_152_;
	    } else if (107 == i)
		anInt7949 = class534_sub40.method16529((byte) 1) * 1042084459;
	    else if (i >= 150 && i < 155) {
		aStringArray7920[i - 150]
		    = class534_sub40.method16541((byte) -100).intern();
		if (!aClass601_7888.aBool7877)
		    aStringArray7920[i - 150] = null;
	    } else if (160 == i) {
		int i_155_ = class534_sub40.method16527(-104033897);
		anIntArray7958 = new int[i_155_];
		for (int i_156_ = 0; i_156_ < i_155_; i_156_++)
		    anIntArray7958[i_156_]
			= class534_sub40.method16529((byte) 1);
	    } else if (162 == i) {
		aByte7908 = (byte) 3;
		anInt7909
		    = class534_sub40.method16533(-258848859) * 1806565949;
	    } else if (163 == i) {
		aByte7900 = class534_sub40.method16586((byte) 1);
		aByte7965 = class534_sub40.method16586((byte) 1);
		aByte7902 = class534_sub40.method16586((byte) 1);
		aByte7906 = class534_sub40.method16586((byte) 1);
	    } else if (164 == i)
		anInt7935
		    = class534_sub40.method16530((byte) -10) * 2025140745;
	    else if (i == 165)
		anInt7918
		    = class534_sub40.method16530((byte) -29) * -487661753;
	    else if (166 == i)
		anInt7936
		    = class534_sub40.method16530((byte) -113) * 1761005545;
	    else if (i == 167)
		anInt7941 = class534_sub40.method16529((byte) 1) * 1246204931;
	    else if (168 != i && i != 169) {
		if (i == 170)
		    anInt7954
			= class534_sub40.method16546(-1706829710) * -921364721;
		else if (171 == i)
		    anInt7913
			= class534_sub40.method16546(-1706829710) * 658564109;
		else if (173 == i) {
		    anInt7901
			= class534_sub40.method16529((byte) 1) * -1566027831;
		    anInt7960
			= class534_sub40.method16529((byte) 1) * 1803353737;
		} else if (i == 177)
		    aBool7961 = true;
		else if (178 == i)
		    anInt7947
			= class534_sub40.method16527(-1456166945) * 878557669;
		else if (186 == i)
		    aClass593_7963
			= ((Class593)
			   Class448.method7319(Class658.method10914((byte) 74),
					       class534_sub40
						   .method16527(-1317636560),
					       2088438307));
		else if (188 != i) {
		    if (i == 189)
			aBool7962 = true;
		    else if (i >= 190 && i < 196) {
			if (anIntArray7922 == null) {
			    anIntArray7922 = new int[6];
			    Arrays.fill(anIntArray7922, -1);
			}
			anIntArray7922[i - 190]
			    = class534_sub40.method16529((byte) 1);
		    } else if (i == 196)
			Class448.method7319(Class191.method3769(2139882933),
					    class534_sub40
						.method16527(257384279),
					    2088438307);
		    else if (197 == i)
			Class448.method7319(Class62.method1262((byte) 21),
					    class534_sub40
						.method16527(-1044406494),
					    2088438307);
		    else if (198 != i && 199 != i) {
			if (i == 200)
			    aBool7916 = true;
			else if (201 == i) {
			    aClass432_7956 = new Class432();
			    aClass432_7956.aFloat4847
				= (float) class534_sub40
					      .method16545((byte) -5);
			    aClass432_7956.aFloat4845
				= (float) class534_sub40
					      .method16545((byte) -6);
			    aClass432_7956.aFloat4844
				= (float) class534_sub40
					      .method16545((byte) -34);
			    aClass432_7956.aFloat4848
				= (float) class534_sub40
					      .method16545((byte) -41);
			    aClass432_7956.aFloat4850
				= (float) class534_sub40
					      .method16545((byte) -43);
			    aClass432_7956.aFloat4849
				= (float) class534_sub40
					      .method16545((byte) -40);
			} else if (i == 249) {
			    int i_157_
				= class534_sub40.method16527(1360691207);
			    if (null == aClass9_7952) {
				int i_158_
				    = Class162.method2640(i_157_, (byte) 30);
				aClass9_7952 = new Class9(i_158_);
			    }
			    for (int i_159_ = 0; i_159_ < i_157_; i_159_++) {
				boolean bool
				    = (class534_sub40.method16527(-1324027324)
				       == 1);
				int i_160_
				    = class534_sub40.method16531(893825797);
				Class534 class534;
				if (bool)
				    class534
					= new Class534_Sub6(class534_sub40
								.method16541
								((byte) -108)
								.intern());
				else
				    class534
					= new Class534_Sub39(class534_sub40
								 .method16533
							     (-258848859));
				aClass9_7952.method581(class534,
						       (long) i_160_);
			    }
			}
		    }
		}
	    }
	}
    }
    
    public final boolean method10005(int i) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i_161_ = 0; i_161_ < aByteArray7926.length; i_161_++) {
	    if (i == aByteArray7926[i_161_]) {
		for (int i_162_ = 0;
		     i_162_ < anIntArrayArray7891[i_161_].length; i_162_++) {
		    if (!aClass601_7888.method9958((anIntArrayArray7891[i_161_]
						    [i_162_]),
						   1189319673))
			bool = false;
		}
	    }
	}
	return bool;
    }
    
    public final boolean method10006(Class647 class647) {
	if (null == anIntArrayArray7891)
	    return true;
	boolean bool = true;
	for (int i = 0; i < anIntArrayArray7891.length; i++) {
	    for (int i_163_ = 0; i_163_ < anIntArrayArray7891[i].length;
		 i_163_++) {
		boolean bool_164_
		    = aClass601_7888.method9958(anIntArrayArray7891[i][i_163_],
						1189319673);
		bool &= bool_164_;
		if (!bool_164_)
		    class647.anInt8375
			= anIntArrayArray7891[i][i_163_] * 511701673;
	    }
	}
	return bool;
    }
    
    public boolean method10007(int i) {
	if (anIntArray7914 != null && -1 != i) {
	    for (int i_165_ = 0; i_165_ < anIntArray7914.length; i_165_++) {
		if (anIntArray7914[i_165_] == i)
		    return true;
	    }
	}
	return false;
    }
    
    public final synchronized Class183 method10008
	(Class185 class185, int i, int i_166_, int i_167_, Class151 class151,
	 Class151 class151_168_, int i_169_, int i_170_, int i_171_,
	 Class711 class711, Class596 class596) {
	if (Class459.method7439(i_166_, (byte) -58))
	    i_166_ = 847393323 * Class595.aClass595_7834.anInt7852;
	long l = (long) ((i_166_ << 3) + (-1562722583 * anInt7887 << 10)
			 + i_167_);
	int i_172_ = i;
	l |= (long) (2098753835 * class185.anInt2001 << 29);
	if (null != class596)
	    l |= 5594098338423607855L * class596.aLong7858 << 32;
	if (class711 != null)
	    i |= class711.method14360((short) -19085);
	if (aByte7908 == 3)
	    i |= 0x7;
	else {
	    if (0 != aByte7908 || 0 != 1273961079 * anInt7918)
		i |= 0x2;
	    if (0 != -1393558471 * anInt7935)
		i |= 0x1;
	    if (0 != 763951193 * anInt7936)
		i |= 0x4;
	}
	if (i_166_ == Class595.aClass595_7836.anInt7852 * 847393323
	    && i_167_ > 3)
	    i |= 0x5;
	Class183 class183;
	synchronized (aClass601_7888.aClass203_7884) {
	    class183 = (Class183) aClass601_7888.aClass203_7884.method3871(l);
	}
	if (null == class183
	    || class185.method3330(class183.method3101(), i) != 0) {
	    if (class183 != null)
		i = class185.method3331(i, class183.method3101());
	    class183 = method9987(class185, i, i_166_, i_167_, class596,
				  2113275141);
	    if (class183 == null)
		return null;
	    synchronized (aClass601_7888.aClass203_7884) {
		aClass601_7888.aClass203_7884.method3893(class183, l);
	    }
	}
	boolean bool = false;
	if (null != class711) {
	    class183 = class183.method3011((byte) 1, i, true);
	    bool = true;
	    class711.method14399(class183, i_167_ & 0x3, 1734734876);
	}
	if (847393323 * Class595.aClass595_7836.anInt7852 == i_166_
	    && i_167_ > 3) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3015(2048);
	}
	if (aByte7908 != 0) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3022(aByte7908, anInt7909 * 1064510741, class151,
				class151_168_, i_169_, i_170_, i_171_);
	}
	if (-1393558471 * anInt7935 != 0 || 0 != 1273961079 * anInt7918
	    || 0 != anInt7936 * 763951193) {
	    if (!bool) {
		class183 = class183.method3011((byte) 3, i, true);
		bool = true;
	    }
	    class183.method3098(-1393558471 * anInt7935,
				1273961079 * anInt7918, 763951193 * anInt7936);
	}
	if (bool)
	    class183.method3012(i_172_);
	return class183;
    }
    
    public int method10009(int i) {
	if (null == anIntArray7922)
	    return -1;
	return anIntArray7922[i];
    }
    
    Class602(int i, Class601 class601, Interface14 interface14) {
	anInt7904 = 1578780151;
	anInt7928 = -742973319;
	anInt7923 = -1761390058;
	anInt7907 = -384348173;
	aByte7908 = (byte) 0;
	anInt7909 = -1806565949;
	aBool7910 = false;
	anInt7911 = -353749263;
	anInt7954 = 253130816;
	anInt7913 = 0;
	anIntArray7914 = null;
	anIntArray7942 = null;
	aBool7916 = false;
	anInt7917 = 1515770688;
	anInt7898 = 0;
	anInt7919 = 0;
	anInt7949 = -1042084459;
	anInt7890 = -1279885027;
	aBool7924 = false;
	anInt7925 = 0;
	aBool7955 = false;
	aBool7927 = false;
	aBool7959 = true;
	anInt7929 = 182333824;
	anInt7930 = 753664896;
	anInt7931 = -1085468544;
	anInt7932 = 0;
	anInt7933 = 0;
	anInt7905 = 0;
	anInt7935 = 0;
	anInt7918 = 0;
	anInt7936 = 0;
	aBool7938 = false;
	aBool7939 = false;
	anInt7940 = 694974925;
	anInt7941 = 0;
	anInt7912 = 689871047;
	anInt7903 = -99503029;
	anInt7945 = 2116677683;
	anInt7946 = 0;
	anInt7947 = 0;
	anInt7948 = 669175451;
	anInt7915 = 0;
	anInt7950 = 0;
	aBool7944 = true;
	aBool7953 = false;
	aBool7896 = true;
	aBool7964 = false;
	aBool7957 = false;
	anInt7901 = -1471166208;
	anInt7960 = 2097056000;
	aBool7961 = false;
	aBool7962 = false;
	aClass593_7963 = Class593.aClass593_7823;
	anInt7887 = 810285401 * i;
	aClass601_7888 = class601;
	anInterface14_7934 = interface14;
	aStringArray7920 = (String[]) aClass601_7888.aStringArray7882.clone();
    }
    
    public int method10010(int i, int i_173_) {
	if (null == aClass9_7952)
	    return i_173_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_7952.method579((long) i);
	if (class534_sub39 == null)
	    return i_173_;
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public int method10011(int i, int i_174_) {
	if (null == aClass9_7952)
	    return i_174_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_7952.method579((long) i);
	if (class534_sub39 == null)
	    return i_174_;
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public String method10012(int i, String string) {
	if (null == aClass9_7952)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_7952.method579((long) i);
	if (class534_sub6 == null)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public boolean method10013() {
	if (anIntArray7943 == null)
	    return 352154885 * anInt7945 != -1 || null != anIntArray7951;
	for (int i = 0; i < anIntArray7943.length; i++) {
	    if (anIntArray7943[i] != -1) {
		Class602 class602_175_
		    = (Class602) anInterface14_7934.method91(anIntArray7943[i],
							     539058255);
		if (-1 != class602_175_.anInt7945 * 352154885
		    || class602_175_.anIntArray7951 != null)
		    return true;
	    }
	}
	return false;
    }
    
    public boolean method10014() {
	return anIntArray7914 != null;
    }
    
    public boolean method10015(byte i) {
	return anIntArray7914 != null;
    }
    
    public int[] method10016() {
	return anIntArray7914;
    }
    
    public int[] method10017() {
	return anIntArray7914;
    }
    
    public int[] method10018() {
	return anIntArray7914;
    }
    
    public boolean method10019() {
	return anIntArray7914 != null && anIntArray7914.length > 1;
    }
    
    public int[] method10020(int i) {
	return anIntArray7914;
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-287990929);
	    if (i == 0)
		break;
	    method10004(class534_sub40, i, (byte) 24);
	}
    }
    
    public int method10021(int i) {
	if (null == anIntArray7922)
	    return -1;
	return anIntArray7922[i];
    }
    
    public void method84() {
	method9982(1004985670);
	if (aBool7939)
	    anInt7923 = 0;
	if (!aClass601_7888.aBool7877 && aBool7964) {
	    aStringArray7920 = null;
	    anIntArray7958 = null;
	}
    }
    
    static final void method10022(Class669 class669, int i) {
	int i_176_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass219_11338.method4120(i_176_, (byte) 61)
		  .method4347(1214342961);
    }
    
    static final void method10023(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class275.method5150(class247, class243, class669, 2141837149);
    }
    
    static final void method10024(Class669 class669, int i) {
	int i_177_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class526.method8759((char) i_177_, -1923805618) ? 1 : 0;
    }
}
