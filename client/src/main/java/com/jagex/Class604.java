/* Class604 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class604
{
    public static final int anInt7967 = 8;
    public static final int anInt7968 = 9;
    public static final int anInt7969 = 2;
    public static final int anInt7970 = 511;
    public static final int anInt7971 = 512;
    public static final int anInt7972 = 1024;
    public static final int anInt7973 = 256;
    public static final int anInt7974 = 128;
    public static final int anInt7975 = 7;
    
    static {
	Math.sqrt(131072.0);
    }
    
    Class604() throws Throwable {
	throw new Error();
    }
    
    static final void method10029(Class669 class669, int i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
		  .method18542(1993360886);
    }
    
    static final void method10030(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4026(-1484235725).size();
    }
    
    public static boolean method10031(char c, int i) {
	return (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z'
		|| c >= 'a' && c <= 'z');
    }
}
