/* Class612 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class612 implements Interface19
{
    int[] anIntArray8015;
    static final long aLong8016 = 4611686018427387904L;
    static final long aLong8017 = 1L;
    static final long aLong8018 = 500L;
    int[] anIntArray8019;
    static final long aLong8020 = 4611686018427387903L;
    Class9 aClass9_8021 = new Class9(128);
    
    public void method130(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public void method10097(Class150 class150, int i, int i_0_) {
	anIntArray8019[class150.anInt1694 * -1270946121] = i;
	Class534_Sub31 class534_sub31
	    = ((Class534_Sub31)
	       aClass9_8021.method579((long) (-1270946121
					      * class150.anInt1694)));
	if (null != class534_sub31) {
	    if (class534_sub31.aLong10695 * -4291396730769792553L
		!= 4611686018427387905L)
		class534_sub31.aLong10695
		    = (Class250.method4604((byte) -7) + 500L
		       | 0x4000000000000000L) * 1934962372971061735L;
	} else {
	    class534_sub31 = new Class534_Sub31(4611686018427387905L);
	    aClass9_8021.method581(class534_sub31,
				   (long) (class150.anInt1694 * -1270946121));
	}
    }
    
    public void method10098(int i) {
	for (int i_1_ = 0;
	     i_1_ < Class562.aClass110_Sub1_Sub1_7560.method90((byte) 70);
	     i_1_++) {
	    Class150_Sub2 class150_sub2
		= ((Class150_Sub2)
		   Class562.aClass110_Sub1_Sub1_7560.method91(i_1_,
							      -1802539585));
	    if (null != class150_sub2) {
		anIntArray8019[i_1_] = 0;
		anIntArray8015[i_1_] = 0;
	    }
	}
	aClass9_8021 = new Class9(128);
    }
    
    public int method117(Class150 class150) {
	return anIntArray8015[class150.anInt1694 * -1270946121];
    }
    
    public void method114(Class150 class150, int i, int i_2_) {
	anIntArray8015[class150.anInt1694 * -1270946121] = i;
	Class534_Sub31 class534_sub31
	    = ((Class534_Sub31)
	       aClass9_8021.method579((long) (-1270946121
					      * class150.anInt1694)));
	if (class534_sub31 != null)
	    class534_sub31.aLong10695 = (Class250.method4604((byte) -111)
					 + 500L) * 1934962372971061735L;
	else {
	    class534_sub31
		= new Class534_Sub31(Class250.method4604((byte) -119) + 500L);
	    aClass9_8021.method581(class534_sub31,
				   (long) (-1270946121 * class150.anInt1694));
	}
    }
    
    public long method129(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public int method119(Class318 class318, int i) {
	return class318.method5750((anIntArray8015
				    [(-1270946121
				      * class318.aClass150_3392.anInt1694)]),
				   1032886751);
    }
    
    public long method132(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public void method10099(Class318 class318, int i, int i_3_) {
	try {
	    int i_4_ = class318.method5748((anIntArray8019
					    [(class318.aClass150_3392.anInt1694
					      * -1270946121)]),
					   i, (byte) -71);
	    method10097(class318.aClass150_3392, i_4_, 1867722130);
	} catch (Exception_Sub6 exception_sub6) {
	    /* empty */
	}
    }
    
    public long method127(Class150 class150, byte i) {
	throw new UnsupportedOperationException();
    }
    
    public void method140(Class318 class318, int i) throws Exception_Sub6 {
	int i_5_
	    = class318.method5748(anIntArray8015[(class318.aClass150_3392
						  .anInt1694) * -1270946121],
				  i, (byte) -95);
	method114(class318.aClass150_3392, i_5_, -2098058189);
    }
    
    public Object method124(Class150 class150, int i) {
	throw new UnsupportedOperationException();
    }
    
    public void method118(Class150 class150, Object object, byte i) {
	throw new UnsupportedOperationException();
    }
    
    public void method128(Class150 class150, int i) {
	anIntArray8015[class150.anInt1694 * -1270946121] = i;
	Class534_Sub31 class534_sub31
	    = ((Class534_Sub31)
	       aClass9_8021.method579((long) (-1270946121
					      * class150.anInt1694)));
	if (class534_sub31 != null)
	    class534_sub31.aLong10695 = (Class250.method4604((byte) -14)
					 + 500L) * 1934962372971061735L;
	else {
	    class534_sub31
		= new Class534_Sub31(Class250.method4604((byte) -58) + 500L);
	    aClass9_8021.method581(class534_sub31,
				   (long) (-1270946121 * class150.anInt1694));
	}
    }
    
    public int method136(Class150 class150) {
	return anIntArray8015[class150.anInt1694 * -1270946121];
    }
    
    public int method123(Class318 class318) {
	return class318.method5750((anIntArray8015
				    [(-1270946121
				      * class318.aClass150_3392.anInt1694)]),
				   1879906188);
    }
    
    public int method135(Class318 class318) {
	return class318.method5750((anIntArray8015
				    [(-1270946121
				      * class318.aClass150_3392.anInt1694)]),
				   1172656022);
    }
    
    public int method125(Class318 class318) {
	return class318.method5750((anIntArray8015
				    [(-1270946121
				      * class318.aClass150_3392.anInt1694)]),
				   1153683734);
    }
    
    public void method10100(Class318 class318, int i) {
	try {
	    int i_6_ = class318.method5748((anIntArray8019
					    [(class318.aClass150_3392.anInt1694
					      * -1270946121)]),
					   i, (byte) 100);
	    method10097(class318.aClass150_3392, i_6_, 2079722165);
	} catch (Exception_Sub6 exception_sub6) {
	    /* empty */
	}
    }
    
    public void method121(Class150 class150, int i) {
	anIntArray8015[class150.anInt1694 * -1270946121] = i;
	Class534_Sub31 class534_sub31
	    = ((Class534_Sub31)
	       aClass9_8021.method579((long) (-1270946121
					      * class150.anInt1694)));
	if (class534_sub31 != null)
	    class534_sub31.aLong10695 = (Class250.method4604((byte) -110)
					 + 500L) * 1934962372971061735L;
	else {
	    class534_sub31
		= new Class534_Sub31(Class250.method4604((byte) -60) + 500L);
	    aClass9_8021.method581(class534_sub31,
				   (long) (-1270946121 * class150.anInt1694));
	}
    }
    
    public int method10101(boolean bool, int i) {
	long l = Class250.method4604((byte) -27);
	for (Class534_Sub31 class534_sub31
		 = (Class534_Sub31) (bool ? aClass9_8021.method583(-1933948810)
				     : aClass9_8021.method584((byte) -32));
	     null != class534_sub31;
	     class534_sub31
		 = (Class534_Sub31) aClass9_8021.method584((byte) -78)) {
	    if ((-4291396730769792553L * class534_sub31.aLong10695
		 & 0x3fffffffffffffffL)
		< l) {
		if (0L != (-4291396730769792553L * class534_sub31.aLong10695
			   & 0x4000000000000000L)) {
		    int i_7_ = (int) (8258869577519436579L
				      * class534_sub31.aLong7158);
		    anIntArray8015[i_7_] = anIntArray8019[i_7_];
		    class534_sub31.method8892((byte) 1);
		    return i_7_;
		}
		class534_sub31.method8892((byte) 1);
	    }
	}
	return -1;
    }
    
    public int method126(Class318 class318) {
	return class318.method5750((anIntArray8015
				    [(-1270946121
				      * class318.aClass150_3392.anInt1694)]),
				   1194698142);
    }
    
    public long method133(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public long method131(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public int method120(Class150 class150, byte i) {
	return anIntArray8015[class150.anInt1694 * -1270946121];
    }
    
    public void method115(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public void method113(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public void method122(Class318 class318, int i, byte i_8_)
	throws Exception_Sub6 {
	int i_9_
	    = class318.method5748(anIntArray8015[(class318.aClass150_3392
						  .anInt1694) * -1270946121],
				  i, (byte) 78);
	method114(class318.aClass150_3392, i_9_, -834861946);
    }
    
    public Object method134(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public Object method137(Class150 class150) {
	throw new UnsupportedOperationException();
    }
    
    public void method138(Class150 class150, Object object) {
	throw new UnsupportedOperationException();
    }
    
    public void method139(Class318 class318, int i) throws Exception_Sub6 {
	int i_10_
	    = class318.method5748(anIntArray8015[(class318.aClass150_3392
						  .anInt1694) * -1270946121],
				  i, (byte) -18);
	method114(class318.aClass150_3392, i_10_, -596858893);
    }
    
    public Class612() {
	anIntArray8019
	    = new int[Class562.aClass110_Sub1_Sub1_7560.method90((byte) 85)];
	anIntArray8015
	    = new int[Class562.aClass110_Sub1_Sub1_7560.method90((byte) 74)];
    }
    
    public int method10102(boolean bool) {
	long l = Class250.method4604((byte) -85);
	for (Class534_Sub31 class534_sub31
		 = (Class534_Sub31) (bool ? aClass9_8021.method583(-1705471127)
				     : aClass9_8021.method584((byte) -101));
	     null != class534_sub31;
	     class534_sub31
		 = (Class534_Sub31) aClass9_8021.method584((byte) -40)) {
	    if ((-4291396730769792553L * class534_sub31.aLong10695
		 & 0x3fffffffffffffffL)
		< l) {
		if (0L != (-4291396730769792553L * class534_sub31.aLong10695
			   & 0x4000000000000000L)) {
		    int i = (int) (8258869577519436579L
				   * class534_sub31.aLong7158);
		    anIntArray8015[i] = anIntArray8019[i];
		    class534_sub31.method8892((byte) 1);
		    return i;
		}
		class534_sub31.method8892((byte) 1);
	    }
	}
	return -1;
    }
    
    public int method10103(boolean bool) {
	long l = Class250.method4604((byte) -92);
	for (Class534_Sub31 class534_sub31
		 = (Class534_Sub31) (bool ? aClass9_8021.method583(-1837914783)
				     : aClass9_8021.method584((byte) -114));
	     null != class534_sub31;
	     class534_sub31
		 = (Class534_Sub31) aClass9_8021.method584((byte) -116)) {
	    if ((-4291396730769792553L * class534_sub31.aLong10695
		 & 0x3fffffffffffffffL)
		< l) {
		if (0L != (-4291396730769792553L * class534_sub31.aLong10695
			   & 0x4000000000000000L)) {
		    int i = (int) (8258869577519436579L
				   * class534_sub31.aLong7158);
		    anIntArray8015[i] = anIntArray8019[i];
		    class534_sub31.method8892((byte) 1);
		    return i;
		}
		class534_sub31.method8892((byte) 1);
	    }
	}
	return -1;
    }
    
    public void method116(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    static final void method10104(Class669 class669, int i) {
	Class589.method9875(-2025682400);
    }
    
    static final void method10105(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1;
    }
    
    public static final void method10106(Class183 class183, Class711 class711,
					 Class711 class711_11_, byte i) {
	if (class711.method14350(147946534)
	    && class711_11_.method14350(-2144733548)) {
	    Class205 class205 = class711.aClass205_8864;
	    Class205 class205_12_ = class711_11_.aClass205_8864;
	    class183.method3028
		(class711.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711.aClass693_8874.anInt8769 * 1321112311,
		 class711.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711.aClass693_8874.anInt8766,
		 -1237559381 * class711.anInt8869,
		 class205.anIntArray2211[class711.anInt8871 * -1485754087],
		 class711_11_.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711_11_.aClass693_8874.anInt8769 * 1321112311,
		 class711_11_.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711_11_.aClass693_8874.anInt8766,
		 -1237559381 * class711_11_.anInt8869,
		 (class205_12_.anIntArray2211
		  [-1485754087 * class711_11_.anInt8871]),
		 (null != class205.aClass208_2213
		  ? class205.aClass208_2213.aBoolArray2240 : null),
		 class205.aBool2214 | class205_12_.aBool2214);
	}
    }
    
    public static void method10107
	(Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2, int i) {
	Class534_Sub16 class534_sub16
	    = ((Class534_Sub16)
	       (Class534_Sub16.aClass9_10470.method579
		((long) (class654_sub1_sub5_sub1_sub2.anInt11922
			 * 1126388985))));
	if (class534_sub16 == null)
	    Class449.method7330(class654_sub1_sub5_sub1_sub2.aByte10854,
				(class654_sub1_sub5_sub1_sub2.anIntArray11977
				 [0]),
				(class654_sub1_sub5_sub1_sub2.anIntArray11978
				 [0]),
				0, null, null, class654_sub1_sub5_sub1_sub2,
				-42557415);
	else
	    class534_sub16.method16151(-1516674708);
    }
    
    static void method10108(int i) {
	if (204700261 * Class151.anInt1705 < 0) {
	    Class151.anInt1705 = 0;
	    Class554_Sub1.anInt10672 = 1421361991;
	    Class554_Sub1.anInt10673 = -2017528667;
	}
	if (204700261 * Class151.anInt1705 > Class554_Sub1.anInt7370) {
	    Class151.anInt1705 = 704227181 * Class554_Sub1.anInt7370;
	    Class554_Sub1.anInt10672 = 1421361991;
	    Class554_Sub1.anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 < 0) {
	    Class328.anInt3479 = 0;
	    Class554_Sub1.anInt10672 = 1421361991;
	    Class554_Sub1.anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 > Class554_Sub1.anInt7371) {
	    Class328.anInt3479 = Class554_Sub1.anInt7371 * 980626489;
	    Class554_Sub1.anInt10672 = 1421361991;
	    Class554_Sub1.anInt10673 = -2017528667;
	}
    }
}
