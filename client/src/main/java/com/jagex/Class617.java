/* Class617 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class617
{
    public static Class617 aClass617_8086 = new Class617(1);
    static Class617 aClass617_8087;
    public static Class617 aClass617_8088;
    static Class617 aClass617_8089 = new Class617(2);
    static Class617 aClass617_8090;
    static Class617 aClass617_8091;
    static Class617 aClass617_8092;
    static Class617 aClass617_8093 = new Class617(3);
    static Class617 aClass617_8094;
    static Class617 aClass617_8095;
    public int anInt8096;
    
    static {
	aClass617_8088 = new Class617(4);
	aClass617_8090 = new Class617(5);
	aClass617_8091 = new Class617(6);
	aClass617_8092 = new Class617(7);
	aClass617_8087 = new Class617(8);
	aClass617_8094 = new Class617(9);
	aClass617_8095 = new Class617(10);
    }
    
    Class617(int i) {
	anInt8096 = -1872296101 * i;
    }
    
    public static Class409[] method10231(int i) {
	return (new Class409[]
		{ Class409.aClass409_4465, Class409.aClass409_4406,
		  Class409.aClass409_4538, Class409.aClass409_4401,
		  Class409.aClass409_4370, Class409.aClass409_4371,
		  Class409.aClass409_4372, Class409.aClass409_4373,
		  Class409.aClass409_4374, Class409.aClass409_4375,
		  Class409.aClass409_4402, Class409.aClass409_4461,
		  Class409.aClass409_4464, Class409.aClass409_4379,
		  Class409.aClass409_4377, Class409.aClass409_4381,
		  Class409.aClass409_4382, Class409.aClass409_4383,
		  Class409.aClass409_4395, Class409.aClass409_4385,
		  Class409.aClass409_4386, Class409.aClass409_4387,
		  Class409.aClass409_4388, Class409.aClass409_4559,
		  Class409.aClass409_4390, Class409.aClass409_4391,
		  Class409.aClass409_4495, Class409.aClass409_4393,
		  Class409.aClass409_4394, Class409.aClass409_4549,
		  Class409.aClass409_4507, Class409.aClass409_4423,
		  Class409.aClass409_4504, Class409.aClass409_4399,
		  Class409.aClass409_4400, Class409.aClass409_4459,
		  Class409.aClass409_4378, Class409.aClass409_4403,
		  Class409.aClass409_4404, Class409.aClass409_4405,
		  Class409.aClass409_4554, Class409.aClass409_4407,
		  Class409.aClass409_4452, Class409.aClass409_4408,
		  Class409.aClass409_4410, Class409.aClass409_4411,
		  Class409.aClass409_4412, Class409.aClass409_4413,
		  Class409.aClass409_4414, Class409.aClass409_4441,
		  Class409.aClass409_4416, Class409.aClass409_4417,
		  Class409.aClass409_4418, Class409.aClass409_4419,
		  Class409.aClass409_4420, Class409.aClass409_4421,
		  Class409.aClass409_4506, Class409.aClass409_4558,
		  Class409.aClass409_4449, Class409.aClass409_4425,
		  Class409.aClass409_4426, Class409.aClass409_4427,
		  Class409.aClass409_4428, Class409.aClass409_4429,
		  Class409.aClass409_4536, Class409.aClass409_4431,
		  Class409.aClass409_4432, Class409.aClass409_4486,
		  Class409.aClass409_4434, Class409.aClass409_4435,
		  Class409.aClass409_4367, Class409.aClass409_4437,
		  Class409.aClass409_4448, Class409.aClass409_4439,
		  Class409.aClass409_4440, Class409.aClass409_4397,
		  Class409.aClass409_4442, Class409.aClass409_4369,
		  Class409.aClass409_4376, Class409.aClass409_4396,
		  Class409.aClass409_4446, Class409.aClass409_4447,
		  Class409.aClass409_4492, Class409.aClass409_4438,
		  Class409.aClass409_4368, Class409.aClass409_4451,
		  Class409.aClass409_4409, Class409.aClass409_4498,
		  Class409.aClass409_4454, Class409.aClass409_4455,
		  Class409.aClass409_4456, Class409.aClass409_4457,
		  Class409.aClass409_4458, Class409.aClass409_4489,
		  Class409.aClass409_4460, Class409.aClass409_4430,
		  Class409.aClass409_4422, Class409.aClass409_4463,
		  Class409.aClass409_4542, Class409.aClass409_4547,
		  Class409.aClass409_4466, Class409.aClass409_4467,
		  Class409.aClass409_4468, Class409.aClass409_4469,
		  Class409.aClass409_4392, Class409.aClass409_4471,
		  Class409.aClass409_4472, Class409.aClass409_4473,
		  Class409.aClass409_4474, Class409.aClass409_4475,
		  Class409.aClass409_4380, Class409.aClass409_4477,
		  Class409.aClass409_4478, Class409.aClass409_4398,
		  Class409.aClass409_4480, Class409.aClass409_4481,
		  Class409.aClass409_4482, Class409.aClass409_4483,
		  Class409.aClass409_4484, Class409.aClass409_4485,
		  Class409.aClass409_4470, Class409.aClass409_4487,
		  Class409.aClass409_4488, Class409.aClass409_4424,
		  Class409.aClass409_4490, Class409.aClass409_4491,
		  Class409.aClass409_4493, Class409.aClass409_4384,
		  Class409.aClass409_4433, Class409.aClass409_4415,
		  Class409.aClass409_4496, Class409.aClass409_4497,
		  Class409.aClass409_4453, Class409.aClass409_4499,
		  Class409.aClass409_4494, Class409.aClass409_4540,
		  Class409.aClass409_4502, Class409.aClass409_4503,
		  Class409.aClass409_4544, Class409.aClass409_4443,
		  Class409.aClass409_4476, Class409.aClass409_4518,
		  Class409.aClass409_4508, Class409.aClass409_4509,
		  Class409.aClass409_4555, Class409.aClass409_4541,
		  Class409.aClass409_4512, Class409.aClass409_4436,
		  Class409.aClass409_4514, Class409.aClass409_4515,
		  Class409.aClass409_4516, Class409.aClass409_4510,
		  Class409.aClass409_4479, Class409.aClass409_4519,
		  Class409.aClass409_4520, Class409.aClass409_4521,
		  Class409.aClass409_4522, Class409.aClass409_4523,
		  Class409.aClass409_4524, Class409.aClass409_4525,
		  Class409.aClass409_4501, Class409.aClass409_4527,
		  Class409.aClass409_4528, Class409.aClass409_4529,
		  Class409.aClass409_4530, Class409.aClass409_4531,
		  Class409.aClass409_4532, Class409.aClass409_4533,
		  Class409.aClass409_4513, Class409.aClass409_4535,
		  Class409.aClass409_4526, Class409.aClass409_4537,
		  Class409.aClass409_4366, Class409.aClass409_4539,
		  Class409.aClass409_4445, Class409.aClass409_4462,
		  Class409.aClass409_4389, Class409.aClass409_4543,
		  Class409.aClass409_4534, Class409.aClass409_4444,
		  Class409.aClass409_4546, Class409.aClass409_4560,
		  Class409.aClass409_4548, Class409.aClass409_4545,
		  Class409.aClass409_4550, Class409.aClass409_4551,
		  Class409.aClass409_4552, Class409.aClass409_4553,
		  Class409.aClass409_4517, Class409.aClass409_4505,
		  Class409.aClass409_4556, Class409.aClass409_4557,
		  Class409.aClass409_4450, Class409.aClass409_4500 });
    }
    
    static void method10232(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class571.method9628(-483141397);
    }
    
    static final void method10233(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742
		  .method17059(664677824) ? 1 : 0;
    }
    
    static final void method10234(Class669 class669, byte i) {
	Class103.method1920(class669.aClass654_Sub1_Sub5_Sub1_8604, class669,
			    (byte) -4);
    }
    
    static final void method10235(int i, int i_0_, int i_1_, int i_2_,
				  boolean bool, int i_3_) {
	if (client.aClass512_11100.method8424((byte) 126) == null)
	    Class254.aClass185_2683.method3292(i, i_0_, i_1_, i_2_, -16777216,
					       -1740340602);
	else {
	    Class438 class438
		= (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method10807()
		   .aClass438_4885);
	    boolean bool_4_ = false;
	    if (client.anInt11155 * -1468443459 != 1) {
		if (!client.aBool11109)
		    bool_4_ = true;
	    } else {
		if ((int) class438.aFloat4864 < 0
		    || ((int) class438.aFloat4864
			>= client.aClass512_11100.method8417(-560458472) * 512)
		    || (int) class438.aFloat4865 < 0
		    || ((int) class438.aFloat4865
			>= (client.aClass512_11100.method8418(-1533611049)
			    * 512)))
		    bool_4_ = true;
		if (3 == -891094135 * Class10.anInt75
		    && !Class599.aClass298_Sub1_7871.method5435(-1869743926))
		    bool_4_ = true;
	    }
	    if (bool_4_)
		Class254.aClass185_2683.method3292(i, i_0_, i_1_, i_2_,
						   -16777216, -1598255704);
	    else {
		client.anInt11161 += -1243241841;
		if (null != Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
		    && ((int) class438.aFloat4864
			- (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			       .method18545((byte) 1)
			   - 1) * 256) >> 9 == -1254538725 * Class113.anInt1375
		    && (((int) class438.aFloat4865
			 - (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
				.method18545((byte) 1)
			    - 1) * 256) >> 9
			== -246444497 * Class113.anInt1376)) {
		    Class113.anInt1375 = 1660827629;
		    Class113.anInt1376 = -517634255;
		    Class453_Sub3.method15985(-357385394);
		}
		Class273.method5101(1710441723);
		if (!bool)
		    Class458.method7437((byte) 40);
		Class499.method8272(-1366746737);
		for (int i_5_ = 0; i_5_ < client.aClass530Array11054.length;
		     i_5_++) {
		    if (null != client.aClass530Array11054[i_5_]
			&& !client.aClass530Array11054[i_5_]
				.method8828(2000056608)
			&& (client.aClass530Array11054[i_5_].method8830
			    (Class254.aClass185_2683, (byte) -38)))
			client.aClass530Array11054[i_5_].method8825
			    (client.aClass512_11100.method8424((byte) 122),
			     1874684782);
		}
		Class292.method5306(i, i_0_, i_1_, i_2_, true, (byte) 35);
		i = client.anInt11319 * -288443379;
		i_0_ = 1082702717 * client.anInt11320;
		i_1_ = -610622155 * client.anInt11234;
		i_2_ = 608745565 * client.anInt11190;
		Class70.method1409(i, i_0_, 825817068);
		if (-891094135 * Class10.anInt75 == 2) {
		    int i_6_ = (int) client.aFloat11302;
		    if (client.anInt11019 * 544044433 >> 8 > i_6_)
			i_6_ = client.anInt11019 * 544044433 >> 8;
		    if (client.aBoolArray11172[4]
			&& 128 + client.anIntArray11308[4] > i_6_)
			i_6_ = 128 + client.anIntArray11308[4];
		    int i_7_ = (((int) client.aFloat11140
				 + -61292849 * client.anInt11127)
				& 0x3fff);
		    Class678.method11152
			(Class588.anInt7808 * 1490134939,
			 (Class247.method4595((int) class438.aFloat4864,
					      (int) class438.aFloat4865,
					      Class674.anInt8633 * -878424575,
					      -1943046495)
			  - client.anInt11146 * -231820249),
			 -1293868227 * Class453.anInt4958, i_6_, i_7_,
			 600 + (i_6_ >> 3) * 3 << 2, i_2_, 2086572687);
		} else if (Class10.anInt75 * -891094135 == 5) {
		    int i_8_ = (int) client.aFloat11302;
		    if (544044433 * client.anInt11019 >> 8 > i_8_)
			i_8_ = client.anInt11019 * 544044433 >> 8;
		    if (client.aBoolArray11172[4]
			&& 128 + client.anIntArray11308[4] > i_8_)
			i_8_ = 128 + client.anIntArray11308[4];
		    int i_9_ = (int) client.aFloat11140 & 0x3fff;
		    Class678.method11152
			(Class588.anInt7808 * 1490134939,
			 (Class247.method4595(1196989073 * client.anInt11137,
					      -823660049 * client.anInt11138,
					      -878424575 * Class674.anInt8633,
					      994223971)
			  - client.anInt11146 * -231820249),
			 -1293868227 * Class453.anInt4958, i_8_, i_9_,
			 600 + (i_8_ >> 3) * 3 << 2, i_2_, 2086572687);
		} else if (6 == Class10.anInt75 * -891094135)
		    Class635.method10541(i_2_, (byte) 92);
		int i_10_ = -116109187 * Class200_Sub13.anInt9937;
		int i_11_ = 1529694271 * Class677.anInt8654;
		int i_12_ = Class636.anInt8305 * -1098179003;
		int i_13_ = -864938791 * Class566.anInt7589;
		int i_14_ = Class641.anInt8341 * -890112543;
		for (int i_15_ = 0; i_15_ < 5; i_15_++) {
		    if (client.aBoolArray11172[i_15_]) {
			int i_16_
			    = (int) ((Math.random()
				      * (double) (2 * (client.anIntArray11307
						       [i_15_])
						  + 1))
				     - (double) client.anIntArray11307[i_15_]
				     + (Math.sin((double) (client
							   .anIntArray11099
							   [i_15_])
						 * ((double) (client
							      .anIntArray11309
							      [i_15_])
						    / 100.0))
					* (double) (client.anIntArray11308
						    [i_15_])));
			if (i_15_ == 0)
			    Class200_Sub13.anInt9937
				+= -2079086379 * (i_16_ << 2);
			if (1 == i_15_)
			    Class677.anInt8654 += 996845503 * (i_16_ << 2);
			if (2 == i_15_)
			    Class636.anInt8305 += -866237299 * (i_16_ << 2);
			if (3 == i_15_)
			    Class641.anInt8341
				= (i_16_ + -890112543 * Class641.anInt8341
				   & 0x3fff) * 486346273;
			if (4 == i_15_) {
			    Class566.anInt7589 += i_16_ * -1539495063;
			    if (Class566.anInt7589 * -864938791 < 1024)
				Class566.anInt7589 = -189946880;
			    else if (Class566.anInt7589 * -864938791 > 3072)
				Class566.anInt7589 = -569840640;
			}
		    }
		}
		if (Class200_Sub13.anInt9937 * -116109187 < 0)
		    Class200_Sub13.anInt9937 = 0;
		if (-116109187 * Class200_Sub13.anInt9937
		    > ((client.aClass512_11100.method8424((byte) 14).anInt7435
			* 1183912005)
		       << 9) - 1)
		    Class200_Sub13.anInt9937
			= (((client.aClass512_11100.method8424((byte) 26)
			     .anInt7435) * 1183912005
			    << 9)
			   - 1) * -2079086379;
		if (Class636.anInt8305 * -1098179003 < 0)
		    Class636.anInt8305 = 0;
		if (-1098179003 * Class636.anInt8305
		    > ((client.aClass512_11100.method8424((byte) 123).anInt7470
			* 60330541)
		       << 9) - 1)
		    Class636.anInt8305 = (((client.aClass512_11100.method8424
					    ((byte) 2).anInt7470) * 60330541
					   << 9)
					  - 1) * -866237299;
		if (Class44_Sub6.aClass534_Sub35_10989
			.aClass690_Sub17_10759.method17035((byte) 2)
		    == 2)
		    Class352.method6260(1657486937);
		else if (Class44_Sub6.aClass534_Sub35_10989
			     .aClass690_Sub17_10759.method17035((byte) 2)
			 == 3)
		    Class106.method1946(-1175696248);
		Class254.aClass185_2683.method3318(i, i_0_, i_1_, i_2_);
		Class254.aClass185_2683.method3237(true);
		Class254.aClass185_2683.method3373(i, i_0_, i + i_1_,
						   i_2_ + i_0_);
		Class634 class634 = client.aClass512_11100.method8501
					((byte) 4).method10145((byte) -122);
		int i_17_ = class634.method10511(-34604748);
		Class287 class287 = new Class287();
		Class597 class597
		    = client.aClass512_11100.method8416((byte) 27);
		if (Class242.method4471((byte) -9))
		    Class51.aClass298_Sub1_412.method5350
			(class287, client.aClass446_11041,
			 client.aClass433_11040,
			 class597.anInt7859 * -424199969 << 9,
			 class597.anInt7861 * -1166289421 << 9, -46561689);
		else if (3 == Class10.anInt75 * -891094135)
		    Class599.aClass298_Sub1_7871.method5350
			(class287, client.aClass446_11041,
			 client.aClass433_11040,
			 -424199969 * class597.anInt7859 << 9,
			 class597.anInt7861 * -1166289421 << 9, -1576229831);
		else {
		    client.aClass446_11041.method7250
			((float) -(-116109187 * Class200_Sub13.anInt9937),
			 (float) -(1529694271 * Class677.anInt8654),
			 (float) -(Class636.anInt8305 * -1098179003));
		    client.aClass446_11041.method7245
			(0.0F, -1.0F, 0.0F,
			 Class427.method6799(-(-890112543 * Class641.anInt8341)
					     & 0x3fff));
		    client.aClass446_11041.method7245
			(-1.0F, 0.0F, 0.0F,
			 Class427.method6799(-(-864938791 * Class566.anInt7589)
					     & 0x3fff));
		    client.aClass446_11041.method7245
			(0.0F, 0.0F, -1.0F,
			 Class427.method6799(-(1208803031 * Class2.anInt22)
					     & 0x3fff));
		    Class200_Sub23.method15658
			(client.aClass433_11040, true, (float) (i_1_ / 2),
			 (float) (i_2_ / 2),
			 (float) (client.anInt11323 * -1439382607 << 1),
			 (float) (-1439382607 * client.anInt11323 << 1), i_1_,
			 i_2_, -476597316);
		}
		Class254.aClass185_2683.method3335(client.aClass446_11041);
		Class254.aClass185_2683.method3338(client.aClass433_11040);
		if (class634.method10512(-1861028078) != null) {
		    Class254.aClass185_2683.method3489(1.0F);
		    Class254.aClass185_2683.method3341(16777215, 0.0F, 0.0F,
						       1.0F, 0.0F, 0.0F);
		    if (-891094135 * Class10.anInt75 == 3) {
			int i_18_
			    = (int) ((double) Class599.aClass298_Sub1_7871
						  .method5399(160919413)
				     * 2607.5945876176133);
			int i_19_
			    = (int) ((double) Class599.aClass298_Sub1_7871
						  .method5389(-1032269943)
				     * 2607.5945876176133);
			class634.method10512(-2107971449).method8336
			    (Class254.aClass185_2683,
			     -1178262641 * Class12.anInt92 << 3, i, i_0_, i_1_,
			     i_2_, i_18_, i_19_, 0, i_17_, true, false,
			     -1027959407);
		    } else
			class634.method10512(-1756342720).method8336
			    (Class254.aClass185_2683,
			     -1178262641 * Class12.anInt92 << 3, i, i_0_, i_1_,
			     i_2_, Class566.anInt7589 * -864938791,
			     Class641.anInt8341 * -890112543,
			     1208803031 * Class2.anInt22, i_17_, true, false,
			     -1326751498);
		    Class254.aClass185_2683.method3581();
		} else
		    Class254.aClass185_2683.method3340(3, i_17_);
		Class254.aClass185_2683.method3237(false);
		Class437.method6988(client.aClass446_11041,
				    client.aClass433_11040, i_1_, i_2_,
				    (short) -652);
		client.aClass512_11100.method8501((byte) 29)
		    .method10141(client.aClass512_11100, (byte) 124);
		int i_20_ = Class44_Sub6.aClass534_Sub35_10989
				.aClass690_Sub17_10759.method17035((byte) 2);
		byte i_21_;
		if (i_20_ == 2)
		    i_21_ = (byte) (1373322351 * client.anInt11161);
		else if (i_20_ == 3)
		    i_21_ = (byte) (client.aBool11340 ? 1 : -1);
		else
		    i_21_ = (byte) 1;
		if (Class242.method4471((byte) -87)
		    || 3 == -891094135 * Class10.anInt75)
		    client.aClass512_11100.method8424((byte) 49).method9226
			(client.anInt11101, 1135682509 * class287.anInt3082,
			 -1018779427 * class287.anInt3083,
			 975339373 * class287.anInt3081,
			 client.aClass512_11100.method8411((byte) -27),
			 client.anIntArray11177, client.anIntArray11123,
			 client.anIntArray11179, client.anIntArray11135,
			 client.anIntArray11181,
			 (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			  .aByte10854) + 1,
			 i_21_, (int) class438.aFloat4864 >> 9,
			 (int) class438.aFloat4865 >> 9,
			 (Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub9_10748.method16958(-1858514400)
			  == 0),
			 true, 0, true);
		else
		    client.aClass512_11100.method8424((byte) 96).method9226
			(client.anInt11101,
			 Class200_Sub13.anInt9937 * -116109187,
			 1529694271 * Class677.anInt8654,
			 Class636.anInt8305 * -1098179003,
			 client.aClass512_11100.method8411((byte) -5),
			 client.anIntArray11177, client.anIntArray11123,
			 client.anIntArray11179, client.anIntArray11135,
			 client.anIntArray11181,
			 (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			  .aByte10854) + 1,
			 i_21_, (int) class438.aFloat4864 >> 9,
			 (int) class438.aFloat4865 >> 9,
			 (Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub9_10748.method16958(-2038481961)
			  == 0),
			 true, 0, true);
		client.anInt11061 += 1831602931;
		if (!Class254.aClass185_2683.method3352()
		    && 16 == client.anInt11039 * -1850530127)
		    Class223.method4160(i, i_0_, i_1_, i_2_, (byte) -6);
		client.aClass512_11100.method8424((byte) 7)
		    .method9248(1076682868);
		Class200_Sub13.anInt9937 = i_10_ * -2079086379;
		Class677.anInt8654 = 996845503 * i_11_;
		Class636.anInt8305 = i_12_ * -866237299;
		Class566.anInt7589 = i_13_ * -1539495063;
		Class641.anInt8341 = 486346273 * i_14_;
		if (client.aBool11352
		    && Class6.aClass450_56.method7336(-1789258921) == 0)
		    client.aBool11352 = false;
		if (client.aBool11352) {
		    Class254.aClass185_2683.method3292(i, i_0_, i_1_, i_2_,
						       -16777216, -1621762922);
		    Class689.method14015
			(Class58.aClass58_460
			     .method1245(Class539.aClass672_7171, (byte) -58),
			 false, Class254.aClass185_2683,
			 Class539_Sub1.aClass171_10327, Class67.aClass16_720,
			 (byte) -99);
		}
		Class200_Sub23.method15658
		    (client.aClass433_11040, false, (float) (i + i_1_ / 2),
		     (float) (i_0_ + i_2_ / 2),
		     (float) (-1439382607 * client.anInt11323 << 1),
		     (float) (client.anInt11323 * -1439382607 << 1), i_1_,
		     i_2_, 2023433186);
		Class254.aClass185_2683.method3338(client.aClass433_11040);
		Class587.method9866(client.aClass433_11040, (byte) -38);
	    }
	}
    }
}
