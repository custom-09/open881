/* Class635 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class635 implements Interface76
{
    public static Class635 aClass635_8298;
    public static Class635 aClass635_8299;
    static Class635 aClass635_8300 = new Class635(3, 0);
    public int anInt8301;
    public static Class635 aClass635_8302;
    int anInt8303;
    
    public static Class635[] method10533(byte i) {
	return new Class635[] { aClass635_8299, aClass635_8302, aClass635_8298,
				aClass635_8300 };
    }
    
    Class635(int i, int i_0_) {
	anInt8301 = -847218451 * i;
	anInt8303 = i_0_ * -363110699;
    }
    
    public int method93() {
	return -1142625667 * anInt8303;
    }
    
    public static Class635[] method10534() {
	return new Class635[] { aClass635_8299, aClass635_8302, aClass635_8298,
				aClass635_8300 };
    }
    
    public int method22() {
	return -1142625667 * anInt8303;
    }
    
    public int method53() {
	return -1142625667 * anInt8303;
    }
    
    static {
	aClass635_8298 = new Class635(2, 1);
	aClass635_8302 = new Class635(1, 2);
	aClass635_8299 = new Class635(0, 3);
    }
    
    public static Class455[] method10535(int i) {
	return (new Class455[]
		{ Class455.aClass455_4961, Class455.aClass455_4960,
		  Class455.aClass455_4959 });
    }
    
    public static void method10536(boolean bool, int i) {
	if (Class574.aBool7697 != bool) {
	    Class535.method8896(99481101);
	    Class574.aBool7697 = bool;
	}
    }
    
    static final void method10537
	(Class515 class515, Class669 class669, int i)
	throws Exception_Sub2, Exception_Sub6 {
	switch (1391779877 * class515.anInt6326) {
	case 24:
	    Class390.method6537(class669, (byte) 79);
	    break;
	case 771:
	    Class437.method6987(class669, -1059380133);
	    break;
	case 303:
	    Class507.method8368(class669, 755396313);
	    break;
	case 123:
	    Class612.method10104(class669, 1662028923);
	    break;
	case 320:
	    Class616.method10230(class669, (byte) -81);
	    break;
	case 40:
	    Class531.method8859(class669, 1257583761);
	    break;
	case 1233:
	    Class598.method9941(class669, (byte) 1);
	    break;
	case 458:
	    Class90.method1726(class669, 256269439);
	    break;
	case 113:
	    Class592.method9886(class669, 509681673);
	    break;
	case 508:
	    Class613.method10109(class669, 1142712355);
	    break;
	case 185:
	    Class191.method3770(class669, (byte) -50);
	    break;
	case 739:
	    Class159.method2609(class669, (byte) 21);
	    break;
	case 367:
	    Class567.method9565(class669, (byte) -86);
	    break;
	case 1000:
	    Class11.method610(class669, -83121708);
	    break;
	case 180:
	    Class545.method8963(class669, 1746344101);
	    break;
	case 1258:
	    Class259.method4800(class669, (byte) 13);
	    break;
	case 106:
	    Class146.method2453(class669, (short) 10559);
	    break;
	case 860:
	    Class40.method1036(class669, 1136520021);
	    break;
	case 796:
	    Class45.method1101(class669, (byte) -102);
	    break;
	case 1095:
	    Class537.method8912(class669, 994581042);
	    break;
	case 443:
	    Class634.method10529(class669, (short) 28687);
	    break;
	case 1235:
	    Class188.method3757(class669, 69677822);
	    break;
	case 589:
	    Class675.method11128(class669, (byte) -54);
	    break;
	case 66:
	    Class200_Sub18.method15631(class669, 715824362);
	    break;
	case 187:
	    Class242.method4467(class669, (byte) -1);
	    break;
	case 785:
	    Class238.method4424(class669, (byte) -80);
	    break;
	case 404:
	    Class37.method939(class669, -608084040);
	    break;
	case 11:
	    Class214.method4100(class669, -1150834559);
	    break;
	case 520:
	    Class473.method7754(class669, (byte) -73);
	    break;
	case 822:
	    Class403.method6614(class669, 16711935);
	    break;
	case 705:
	    Class654_Sub1_Sub5_Sub1_Sub1.method18861(class669, (byte) 0);
	    break;
	case 1108:
	    Class179_Sub1.method14980(class669, -889643001);
	    break;
	case 500:
	    Class220.method4147(class669, -1323947690);
	    break;
	case 27:
	    Class411.method6718(class669, (byte) 37);
	    break;
	case 13:
	    Class276.method5157(class669, -1929814355);
	    break;
	case 1130:
	    Class367_Sub1.method15780(class669, (byte) 70);
	    break;
	case 335:
	    Class637.method10555(class669, -2027308270);
	    break;
	case 401:
	    Class575.method9757(class669, 1667835248);
	    break;
	case 1146:
	    Class34.method924(class669, -925393954);
	    break;
	case 1058:
	    Class74.method1573(class669, 640001538);
	    break;
	case 253:
	    Class90.method1727(class669, (byte) 61);
	    break;
	case 872:
	    Class173.method2907(class669, 106530626);
	    break;
	case 685:
	    Class480.method7922(class669, -687204854);
	    break;
	case 713:
	    Class586.method9859(class669, -1979811423);
	    break;
	case 14:
	    Class534_Sub32.method16392(class669, (byte) -106);
	    break;
	case 491:
	    Class606.method10049(class669, 2006333851);
	    break;
	case 17:
	    Class652.method10738(class669, 668694443);
	    break;
	case 644:
	    Class555.method9222(class669, 1643028634);
	    break;
	case 57:
	    Class171_Sub4.method15602(class669, -1790073230);
	    break;
	case 1213:
	    Class485.method7962(class669, (byte) -2);
	    break;
	case 628:
	    Class489.method8004(class669, -833592845);
	    break;
	case 136:
	    Class289.method5282(class669, 308999563);
	    break;
	case 1060:
	    Class352.method6259(class669, (byte) -101);
	    break;
	case 228:
	    Class267.method4873(class669, 478724485);
	    break;
	case 87:
	    Class34.method926(class669, -1098179003);
	    break;
	case 1113:
	    Class559.method9425(class669, (byte) -66);
	    break;
	case 1062:
	    Class577.method9773(class669, (short) -2452);
	    break;
	case 545:
	    Class683.method13924(class669, (short) -26703);
	    break;
	case 354:
	    Class190.method3767(class669, 751231354);
	    break;
	case 973:
	    Class482.method7929(class669, 1831870632);
	    break;
	case 255:
	    Class578.method9797(class669, (byte) 1);
	    break;
	case 1234:
	    Class662.method10988(class669, 565100473);
	    break;
	case 482:
	    Class461.method7512(class669, 1273577115);
	    break;
	case 711:
	    Class410.method6712(class669, -445782853);
	    break;
	case 430:
	    Class534_Sub41.method16767(class669, -582052241);
	    break;
	case 96:
	    Class184.method3222(class669, (byte) 71);
	    break;
	case 946:
	    Class8.method576(class669, (byte) -115);
	    break;
	case 505:
	    Class317.method5743(class669, 1543519364);
	    break;
	case 784:
	    Class224.method4162(class669, -501316579);
	    break;
	case 970:
	    Class237.method4423(class669, 1973574827);
	    break;
	case 103:
	    Class488.method7998(class669, -53931884);
	    break;
	case 493:
	    Class171.method2883(class669, 1080574910);
	    break;
	case 201:
	    Class628.method10380(class669, -316262604);
	    break;
	case 928:
	    Class534_Sub12_Sub1.method18252(class669, (byte) 4);
	    break;
	case 34:
	    Class484.method7957(class669, (byte) 47);
	    break;
	case 375:
	    Class440.method7096(class669, (byte) -90);
	    break;
	case 585:
	    Class504.method8320(class669, 1499690625);
	    break;
	case 159:
	    Class494.method8126(class669, (byte) 24);
	    break;
	case 206:
	    Class50.method1162(class669, (byte) -14);
	    break;
	case 1090:
	    Class559.method9427(class669, 105425183);
	    break;
	case 853:
	    Class265.method4857(class669, 1455147980);
	    break;
	case 1063:
	    Class253.method4633(class669, -1041827961);
	    break;
	case 791:
	    Class409.method6707(class669, -1986546511);
	    break;
	case 32:
	    Class330.method5853(class669, 1133869925);
	    break;
	case 849:
	    Class610.method10072(class669, (byte) -74);
	    break;
	case 896:
	    Class68.method1386(class669, (byte) 1);
	    break;
	case 1021:
	    Class376.method6415(class669, 1874593113);
	    break;
	case 558:
	    Class392.method6548(class669, (byte) 67);
	    break;
	case 1087:
	    Class397.method6575(class669, -1676720539);
	    break;
	case 991:
	    Class469.method7638(class669, 1701859415);
	    break;
	case 1059:
	    Class683.method13926(class669, 1100137856);
	    break;
	case 541:
	    Class540.method8941(class669, (short) 8192);
	    break;
	case 8:
	    Class534_Sub40.method16764(class669, 952190922);
	    break;
	case 53:
	    Class57.method1242(class669, -1768973317);
	    break;
	case 1004:
	    Class202.method3866(class669, (byte) -98);
	    break;
	case 726:
	    Class106.method1948(class669, (byte) -4);
	    break;
	case 709:
	    Class468.method7625(class669, -1287905174);
	    break;
	case 927:
	    Class485.method7961(class669, (byte) -57);
	    break;
	case 1172:
	    Class303.method5575(class669, -1852221718);
	    break;
	case 842:
	    Class421.method6781(class669, (byte) 49);
	    break;
	case 1024:
	    Class203.method3904(class669, (byte) -24);
	    break;
	case 252:
	    Class87.method1708(class669, (byte) -24);
	    break;
	case 1252:
	    Class30.method883(class669, -1984841505);
	    break;
	case 353:
	    Class408.method6693(class669, 787722176);
	    break;
	case 537:
	    Class419.method6769(class669, -819848360);
	    break;
	case 998:
	    Class214.method4103(class669, 43024496);
	    break;
	case 30:
	    Class545.method8966(class669, 65536);
	    break;
	case 138:
	    Class219.method4140(class669, 2041131940);
	    break;
	case 242:
	    Class697.method14118(class669, (byte) -74);
	    break;
	case 982:
	    Class200_Sub15.method15604(class669, 310704475);
	    break;
	case 987:
	    Class597.method9933(class669, -524585953);
	    break;
	case 340:
	    Class645.method10685(class669, (byte) -86);
	    break;
	case 329:
	    Class529.method8822(class669, (byte) -72);
	    break;
	case 1080:
	    Class146.method2455(class669, (byte) 1);
	    break;
	case 42:
	    Class310.method5663(class669, 1164120609);
	    break;
	case 295:
	    Class392.method6549(class669, (byte) 86);
	    break;
	case 1079:
	    Class273.method5100(class669, 1502868732);
	    break;
	case 217:
	    Class513.method8580(class669, -468426711);
	    break;
	case 1259:
	    Class92.method1745(class669, (byte) 2);
	    break;
	case 1174:
	    Class246.method4508(class669, (byte) 0);
	    break;
	case 861:
	    Class159.method2607(class669, -2058212731);
	    break;
	case 553:
	    Class404.method6620(class669, -1633827690);
	    break;
	case 309:
	    Class680.method13864(class669, -700619503);
	    break;
	case 157:
	    Class398.method6577(class669, 1008462924);
	    break;
	case 414:
	    Class484.method7954(class669, -1569757211);
	    break;
	case 101:
	    Class255.method4652(class669, -2022189992);
	    break;
	case 1271:
	    Class582.method9835(class669, 681616307);
	    break;
	case 1008:
	    Class480.method7921(class669, (byte) -18);
	    break;
	case 1131:
	    Class412.method6730(class669, 2140063890);
	    break;
	case 676:
	    Class540.method8940(class669, -152089884);
	    break;
	case 597:
	    Class467.method7601(class669, (byte) 119);
	    break;
	case 1074:
	    Class199.method3840(class669, (byte) 10);
	    break;
	case 1184:
	    Class526.method8758(class669, -897584116);
	    break;
	case 716:
	    Class287.method5270(class669, -2026026895);
	    break;
	case 821:
	    Class624.method10311(class669, (byte) -72);
	    break;
	case 1092:
	    Class568.method9570(class669, 1176344707);
	    break;
	case 192:
	    Class577.method9769(class669, 1774602948);
	    break;
	case 601:
	    Class90.method1724(class669, -1620280544);
	    break;
	case 882:
	    Class572.method9636(class669, (byte) 25);
	    break;
	case 1017:
	    Class386.method6498(class669, -85903323);
	    break;
	case 1239:
	    Class294.method5320(class669, (byte) 127);
	    break;
	case 393:
	    Class273.method5099(class669, 1906912163);
	    break;
	case 455:
	    Class575.method9756(class669, -865010507);
	    break;
	case 410:
	    Class302.method5567(class669, -2107916264);
	    break;
	case 695:
	    Class388.method6526(class669, (byte) 103);
	    break;
	case 609:
	    Class299.method5517(class669, (byte) 104);
	    break;
	case 1137:
	    Class511.method8405(class669, 986979318);
	    break;
	case 898:
	    Class618.method10247(class669, 1144992167);
	    break;
	case 307:
	    Class511.method8404(class669, 218276768);
	    break;
	case 1139:
	    Class449.method7326(class669, (byte) -51);
	    break;
	case 645:
	    Class158.method2592(class669, -1601096419);
	    break;
	case 1078:
	    Class322.method5781(class669, 476390806);
	    break;
	case 18:
	    Class200_Sub22.method15648(class669, -417545057);
	    break;
	case 499:
	    Class367.method6365(class669, -2109959895);
	    break;
	case 239:
	    Class223.method4157(class669, -1871834967);
	    break;
	case 834:
	    Class118.method2130(class669, 827696568);
	    break;
	case 76:
	    Class283.method5249(class669, (byte) 2);
	    break;
	case 665:
	    Class581.method9827(class669, 195159761);
	    break;
	case 148:
	    Class175.method2922(class669, (byte) 57);
	    break;
	case 171:
	    Class172.method2903(class669, 43241364);
	    break;
	case 448:
	    Class408.method6698(class669, (short) -10313);
	    break;
	case 1064:
	    Class639.method10588(class669, -2110992874);
	    break;
	case 133:
	    Class660.method10945(class669, 1125571744);
	    break;
	case 701:
	    Class567.method9566(class669, 242877115);
	    break;
	case 447:
	    Class408.method6694(class669, (byte) 71);
	    break;
	case 583:
	    Class188.method3754(class669, (byte) -22);
	    break;
	case 763:
	    Class464.method7563(class669, -2044484027);
	    break;
	case 475:
	    Class556.method9400(class669, -1596186770);
	    break;
	case 298:
	    Class18.method795(class669, (byte) 42);
	    break;
	case 486:
	    Class492.method8097(class669, 1327821691);
	    break;
	case 156:
	    Class40.method1035(false, class669, 1981070618);
	    break;
	case 485:
	    Class224.method4164(class669, -1153301080);
	    break;
	case 321:
	    Class100.method1890(class669, (byte) -87);
	    break;
	case 43:
	    Class408.method6697(class669, -1922454448);
	    break;
	case 1055:
	    Class484.method7953(class669, 11835086);
	    break;
	case 271:
	    Class36.method936(class669, (byte) 74);
	    break;
	case 189:
	    Class75.method1588(class669, -1184425351);
	    break;
	case 815:
	    Class534_Sub36.method16490(class669, -508210585);
	    break;
	case 643:
	    Class333.method5870(class669, 935140216);
	    break;
	case 958:
	    Class304.method5595(class669, 291172585);
	    break;
	case 650:
	    Class331.method5858(class669, (byte) 42);
	    break;
	case 464:
	    Class206.method3936(class669, (byte) -2);
	    break;
	case 291:
	    Class594.method9907(class669, 610588140);
	    break;
	case 863:
	    Class619.method10261(class669, (byte) -89);
	    break;
	case 463:
	    Exception_Sub4.method17945(class669, (byte) 5);
	    break;
	case 423:
	    Class560.method9430(class669, -2124525229);
	    break;
	case 728:
	    Class528.method8818(class669, 396493061);
	    break;
	case 810:
	    Class61.method1260(class669, (byte) -79);
	    break;
	case 633:
	    Class323_Sub2.method15694(class669, -2061661728);
	    break;
	case 511:
	    Class260.method4810(class669, (byte) 26);
	    break;
	case 630:
	    Class100.method1887(class669, -1366139551);
	    break;
	case 747:
	    Class203.method3902(class669, (byte) 1);
	    break;
	case 990:
	    Class622.method10287(class669, -590455953);
	    break;
	case 58:
	    Class375.method6412(class669, (byte) -112);
	    break;
	case 248:
	    Class176.method2927(class669, -1052435547);
	    break;
	case 661:
	    Class117.method2123(true, class669, -2138620071);
	    break;
	case 333:
	    Class625.method10329(class669, 488785447);
	    break;
	case 691:
	    Class390.method6538(class669, 959938765);
	    break;
	case 593:
	    Class251.method4620(class669, -894905854);
	    break;
	case 876:
	    Class230.method4209(class669, -2106999262);
	    break;
	case 311:
	    Class464.method7560(class669, (byte) -31);
	    break;
	case 390:
	    Class302.method5568(class669, (byte) 75);
	    break;
	case 477:
	    Class534_Sub35.method16459(class669, -766470114);
	    break;
	case 912:
	    Class599.method9946(class669, false, 1316243697);
	    break;
	case 913:
	    Class456.method7427(class669, -922467902);
	    break;
	case 132:
	    Class503.method8308(class669, (byte) 59);
	    break;
	case 828:
	    Class251.method4621(class669, (byte) 78);
	    break;
	case 403:
	    Class383.method6460(class669, (byte) -38);
	    break;
	case 352:
	    Class487.method7991(class669, -1026872332);
	    break;
	case 801:
	    Class400.method6585(class669, 705831646);
	    break;
	case 720:
	    Class636.method10552(class669, 1239937575);
	    break;
	case 165:
	    Class412.method6728(class669, 1769278275);
	    break;
	case 641:
	    Class393.method6557(class669, (byte) -8);
	    break;
	case 540:
	    Class195.method3816(class669, 860790173);
	    break;
	case 350:
	    Class419.method6767(class669, -510914230);
	    break;
	case 757:
	    Class22.method814(class669, -1467179273);
	    break;
	case 174:
	    Class34.method922(class669, -1274072612);
	    break;
	case 1085:
	    Class309.method5658(class669, (byte) 14);
	    break;
	case 431:
	    Class640.method10595(class669, -1495101509);
	    break;
	case 230:
	    Class171_Sub6.method15629(class669, -1846202585);
	    break;
	case 453:
	    Class302.method5569(class669, -1042530518);
	    break;
	case 160:
	    OutputStream_Sub1.method17370(class669, 1639347219);
	    break;
	case 422:
	    Class82.method1650(class669, -1284649919);
	    break;
	case 355:
	    Class643.method10679(class669, (byte) -12);
	    break;
	case 445:
	    Class614.method10129(class669, (byte) -75);
	    break;
	case 1028:
	    Class551.method9050(class669, (byte) 67);
	    break;
	case 1054:
	    Class235.method4407(class669, 1767190562);
	    break;
	case 196:
	    Class89.method1717(class669, (byte) 0);
	    break;
	case 181:
	    Class513.method8577(class669, (byte) -105);
	    break;
	case 519:
	    Class662.method10993(class669, -719747585);
	    break;
	case 465:
	    Class681.method13868(class669, 768998485);
	    break;
	case 812:
	    Class198.method3828(class669, -1723760517);
	    break;
	case 653:
	    Class503.method8309(class669, -1769314326);
	    break;
	case 917:
	    Class645.method10684(class669, (byte) 39);
	    break;
	case 1269:
	    Class550.method9031(class669, (byte) 60);
	    break;
	case 559:
	    Class105.method1939(class669, (byte) -37);
	    break;
	case 1207:
	    Class534_Sub1_Sub1.method18224(class669, (byte) -63);
	    break;
	case 865:
	    Class309.method5659(class669, -1129844175);
	    break;
	case 107:
	    Class235.method4405(class669, (byte) -114);
	    break;
	case 1052:
	    Class280.method5232(class669, (byte) 39);
	    break;
	case 891:
	    Class210.method3955(class669, (byte) 12);
	    break;
	case 910:
	    Class19.method797(class669, -1033079634);
	    break;
	case 93:
	    Class562.method9466(class669, (byte) -5);
	    break;
	case 378:
	    Class179_Sub1.method14981(class669, (byte) 75);
	    break;
	case 71:
	    Class29.method876(class669, true, 2094208568);
	    break;
	case 967:
	    Class653.method10800(class669, (byte) -112);
	    break;
	case 943:
	    Class712.method14424(class669, -1020785483);
	    break;
	case 933:
	    Class616.method10229(class669, (byte) -26);
	    break;
	case 903:
	    Class369.method6379(class669, 153675458);
	    break;
	case 452:
	    Class195.method3815(class669, -1463699610);
	    break;
	case 1142:
	    Class382.method6446(class669, -915317810);
	    break;
	case 826:
	    Class627.method10376(class669, 1664147795);
	    break;
	case 915:
	    Class519.method8648(class669, -241954802);
	    break;
	case 331:
	    Class665.method11009(class669, 16711935);
	    break;
	case 1023:
	    Class388.method6528(class669, -10979974);
	    break;
	case 105:
	    Class204.method3909(class669, 1134156857);
	    break;
	case 359:
	    Class172.method2896(class669, 1468345157);
	    break;
	case 146:
	    Class658.method10909(class669, 592927069);
	    break;
	case 1068:
	    Class19.method799(class669, 2128762655);
	    break;
	case 827:
	    Class388.method6527(class669, (short) -3139);
	    break;
	case 554:
	    Class568.method9571(class669, 443071802);
	    break;
	case 382:
	    Class49.method1152(class669, -1418358694);
	    break;
	case 1039:
	    Class530.method8846(class669, 87841020);
	    break;
	case 64:
	    Class670_Sub1.method16903(class669, 268435455);
	    break;
	case 52:
	    Class219.method4138(class669, (byte) -100);
	    break;
	case 1065:
	    Class303.method5574(class669, -1665141556);
	    break;
	case 147:
	    Class92.method1746(class669, 608275859);
	    break;
	case 37:
	    Class601.method9977(class669, -424523114);
	    break;
	case 809:
	    Class102.method1904(class669, 1991358999);
	    break;
	case 454:
	    Class457.method7428(class669, -1768883392);
	    break;
	case 663:
	    Class172.method2898(class669, (byte) 81);
	    break;
	case 1236:
	    Class510.method8403(class669, (byte) -1);
	    break;
	case 780:
	    Class130.method2303(class669, (byte) 25);
	    break;
	case 715:
	    Class494.method8124(class669, (byte) 29);
	    break;
	case 417:
	    Class25.method856(class669, -574613427);
	    break;
	case 124:
	    Class311.method5667(class669, 1505886085);
	    break;
	case 409:
	    Class351.method6182(class669, (byte) 7);
	    break;
	case 462:
	    Class211.method3957(class669, -793125541);
	    break;
	case 552:
	    Class223.method4158(class669, -1194024402);
	    break;
	case 921:
	    Class575.method9755(class669, 721017477);
	    break;
	case 102:
	    Class419.method6766(class669, -1676898991);
	    break;
	case 98:
	    Class170.method2822(class669, 1137981403);
	    break;
	case 209:
	    Class287.method5271(class669, -880507120);
	    break;
	case 978:
	    Class402.method6593(class669, true, true, 1784278896);
	    break;
	case 618:
	    Class614.method10124(class669, -363758032);
	    break;
	case 1179:
	    Class521.method8690(class669, (byte) 22);
	    break;
	case 1278:
	    Class253.method4637(class669, 580386668);
	    break;
	case 1035:
	    Class300.method5554(class669, 942253394);
	    break;
	case 744:
	    Class690_Sub38.method17205(class669, -1047324209);
	    break;
	case 467:
	    Class548.method9015(class669, 1987269380);
	    break;
	case 1229:
	    Class234.method4343(class669, 2043886654);
	    break;
	case 532:
	    Class403.method6613(class669, (short) -30163);
	    break;
	case 1152:
	    Class206.method3935(class669, 957166262);
	    break;
	case 247:
	    Class504.method8324(class669, (byte) -43);
	    break;
	case 994:
	    Class403.method6617(class669, (byte) -1);
	    break;
	case 251:
	    Class270.method5036(class669, 1824680564);
	    break;
	case 929:
	    Class571.method9631(class669, 757111527);
	    break;
	case 4:
	    Class665.method11002(class669, (byte) 94);
	    break;
	case 850:
	    Class587.method9864(class669, 1409815235);
	    break;
	case 1119:
	    Class298.method5507(class669, (byte) 30);
	    break;
	case 636:
	    Class382.method6448(class669, 1745803383);
	    break;
	case 1163:
	    Class673.method11109(class669, 1984155648);
	    break;
	case 302:
	    Class563.method9509(class669, (byte) 0);
	    break;
	case 887:
	    Class636.method10550(class669, 505684448);
	    break;
	case 306:
	    Class395.method6564(class669, -1973192882);
	    break;
	case 114:
	    Class215.method4108(class669, (byte) 2);
	    break;
	case 1178:
	    Class325.method5816(class669, (short) 2625);
	    break;
	case 1003:
	    Class33.method903(class669, 1303062614);
	    break;
	case 515:
	    Class170.method2823(class669, (byte) 19);
	    break;
	case 1277:
	    Class593.method9899(class669, (short) 23284);
	    break;
	case 1044:
	    Class487.method7990(class669, 914888907);
	    break;
	case 360:
	    Class617.method10234(class669, (byte) -35);
	    break;
	case 338:
	    Class276.method5156(class669, 837949621);
	    break;
	case 788:
	    Class584.method9839(class669, 1680510718);
	    break;
	case 534:
	    Class204.method3911(class669, -758754553);
	    break;
	case 300:
	    Class584.method9840(class669, (byte) 117);
	    break;
	case 15:
	    Class369.method6376(class669, (byte) -16);
	    break;
	case 62:
	    Class604.method10030(class669, (byte) -57);
	    break;
	case 1170:
	    Class613.method10110(class669, -1228959953);
	    break;
	case 441:
	    Class672.method11081(class669, 1812823912);
	    break;
	case 1255:
	    Class692.method14046(class669, (byte) 25);
	    break;
	case 377:
	    Class412.method6729(class669, 1062667594);
	    break;
	case 934:
	    Class159.method2610(class669, (byte) 2);
	    break;
	case 416:
	    Class547.method9012(class669, (byte) -101);
	    break;
	case 115:
	    Class226.method4176(class669, 280183475);
	    break;
	case 1097:
	    Class466.method7584(class669, (byte) 98);
	    break;
	case 221:
	    Class200_Sub22.method15647(class669, -810727915);
	    break;
	case 845:
	    Class177.method2935(class669, -1100842399);
	    break;
	case 843:
	    Class175_Sub1.method15089(class669, 2050369294);
	    break;
	case 666:
	    Class429.method6809(class669, (byte) 4);
	    break;
	case 1211:
	    Class235.method4406(class669, -1095234703);
	    break;
	case 1116:
	    Class260.method4811(class669, -953649595);
	    break;
	case 5:
	    Class647.method10691(class669, -531354212);
	    break;
	case 881:
	    Class301.method5560(class669, 1139058921);
	    break;
	case 610:
	    Class44_Sub4.method17265(class669, -553890369);
	    break;
	case 94:
	    Class110_Sub1_Sub1.method17866(class669, (byte) 58);
	    break;
	case 570:
	    Class205.method3924(class669, 1948402418);
	    break;
	case 1279:
	    Class422.method6784(class669, (byte) 92);
	    break;
	case 698:
	    Class473.method7755(class669, (byte) 26);
	    break;
	case 374:
	    Class675.method11125(class669, 934776929);
	    break;
	case 215:
	    Class351.method6185(class669, -1252783650);
	    break;
	case 581:
	    Class471.method7656(class669, (byte) 0);
	    break;
	case 316:
	    Exception_Sub4.method17944(class669, 1577626719);
	    break;
	case 637:
	    Class497.method8139(class669, -966526014);
	    break;
	case 223:
	    Class47.method1124(class669, 2093684895);
	    break;
	case 212:
	    Class98.method1838(class669, (byte) 7);
	    break;
	case 582:
	    Class488.method7997(class669, -1792556064);
	    break;
	case 274:
	    Class607.method10059(class669, -1796285136);
	    break;
	case 1185:
	    Class262.method4821(class669, (byte) 16);
	    break;
	case 104:
	    Class409.method6708(class669, (byte) 16);
	    break;
	case 16:
	    Class630.method10432(class669, -178779416);
	    break;
	case 219:
	    Class34.method923(class669, 429908584);
	    break;
	case 855:
	    Class561.method9445(class669, (short) 23118);
	    break;
	case 1103:
	    Class53.method1192(class669, -1913610185);
	    break;
	case 1081:
	    Class278.method5225(class669, (byte) -54);
	    break;
	case 20:
	    Class40.method1033(class669, -958927812);
	    break;
	case 1167:
	    Class293.method5314(class669, (byte) 19);
	    break;
	case 229:
	    Class73.method1568(class669, 745533709);
	    break;
	case 293:
	    Class224.method4163(class669, -825230399);
	    break;
	case 254:
	    Class222.method4153(class669, -339829428);
	    break;
	case 687:
	    Class281_Sub1.method15655(class669, (byte) -16);
	    break;
	case 951:
	    Class690_Sub15.method17025(class669, (byte) 105);
	    break;
	case 989:
	    Class555.method9221(class669, 2107005154);
	    break;
	case 846:
	    Class539.method8925(class669, 253641390);
	    break;
	case 207:
	    Class524.method8732(class669, (byte) 21);
	    break;
	case 799:
	    Class283.method5250(class669, -112470898);
	    break;
	case 322:
	    Class78.method1619(class669, (byte) 6);
	    break;
	case 767:
	    Class251.method4619(class669, 1355170127);
	    break;
	case 231:
	    Class317.method5745(class669, -1700482941);
	    break;
	case 899:
	    Class117.method2122(class669, -1393477938);
	    break;
	case 857:
	    Class411.method6717(class669, 1992695988);
	    break;
	case 1195:
	    Class597.method9934(class669, 2063813083);
	    break;
	case 38:
	    Class262.method4820(class669, (byte) -14);
	    break;
	case 261:
	    Class104.method1931(class669, (short) -988);
	    break;
	case 1183:
	    Class209.method3952(class669, 1438017593);
	    break;
	case 830:
	    Class313.method5690(class669, -1965079276);
	    break;
	case 948:
	    Class572.method9637(class669, -589051127);
	    break;
	case 200:
	    Class557.method9408(class669, 39708084);
	    break;
	case 775:
	    Class487.method7995(class669, 1831545406);
	    break;
	case 397:
	    Class28.method864(class669, -1621309166);
	    break;
	case 461:
	    Class584.method9842(class669, 1228257455);
	    break;
	case 383:
	    Class565.method9528(class669, 1528448711);
	    break;
	case 23:
	    Class590.method9878(class669, 1212032348);
	    break;
	case 587:
	    Class618.method10248(class669, -1546062853);
	    break;
	case 802:
	    Class51.method1177(class669, -520563133);
	    break;
	case 97:
	    Class288.method5274(class669, 2042693911);
	    break;
	case 939:
	    Class200_Sub11.method15585(class669, (byte) 95);
	    break;
	case 122:
	    Class246.method4506(class669, 310539082);
	    break;
	case 442:
	    Class549.method9019(class669, -814918558);
	    break;
	case 74:
	    Class493.method8119(class669, 253995311);
	    break;
	case 356:
	    Class16.method766(class669, 478918071);
	    break;
	case 565:
	    Class418.method6758(class669, -354866169);
	    break;
	case 599:
	    Class404.method6621(class669, -1033503214);
	    break;
	case 287:
	    Class610.method10073(class669, (byte) 37);
	    break;
	case 128:
	    Class561.method9449(class669, (byte) -56);
	    break;
	case 735:
	    Class475.method7765(class669, 437751179);
	    break;
	case 214:
	    Class17.method776(class669, (byte) 0);
	    break;
	case 808:
	    Class375.method6413(class669, (short) 17393);
	    break;
	case 92:
	    Class319.method5762(class669, 1934629109);
	    break;
	case 1270:
	    Class617.method10233(class669, 12021633);
	    break;
	case 44:
	    Class69.method1398(class669, -1816702648);
	    break;
	case 10:
	    Class117.method2124(class669, (byte) 92);
	    break;
	case 1120:
	    Class711.method14417(class669, -1140632790);
	    break;
	case 90:
	    Class421.method6780(class669, -1984912900);
	    break;
	case 856:
	    Class450.method7369(class669, -943215643);
	    break;
	case 1006:
	    Class229.method4204(class669, -1782296223);
	    break;
	case 875:
	    Class261.method4819(class669, 551631815);
	    break;
	case 473:
	    Class387.method6502(class669, (byte) 5);
	    break;
	case 137:
	    Class551.method9051(class669, 707934674);
	    break;
	case 690:
	    Class200_Sub8.method15580(class669, (short) -24331);
	    break;
	case 831:
	    Class700.method14201(class669, 1916398893);
	    break;
	case 965:
	    Class177.method2937(class669, -441344943);
	    break;
	case 1210:
	    Class619.method10260(class669, -1196058216);
	    break;
	case 264:
	    Class672.method11093(class669, 1049962594);
	    break;
	case 504:
	    Class538.method8924(class669, (byte) 0);
	    break;
	case 339:
	    Class337.method5905(class669, (short) -1360);
	    break;
	case 897:
	    Class566.method9542(class669, (short) 512);
	    break;
	case 1202:
	    Class583.method9838(class669, 904846403);
	    break;
	case 697:
	    Class28.method861(class669, (byte) 20);
	    break;
	case 952:
	    Class547.method9011(class669, -244466032);
	    break;
	case 327:
	    Class618.method10244(class669, -142062505);
	    break;
	case 886:
	    Class200.method3854(class669, (byte) -22);
	    break;
	case 195:
	    Class519.method8650(class669, (byte) -60);
	    break;
	case 749:
	    Class679.method13859(class669, 2131554374);
	    break;
	case 1200:
	    Class200_Sub7.method15577(class669, (byte) -42);
	    break;
	case 1147:
	    Class654_Sub1_Sub1_Sub1.method18619(class669, 1796656139);
	    break;
	case 979:
	    Class616.method10225(class669, (byte) -11);
	    break;
	case 1215:
	    Class633.method10496(class669, (short) -20197);
	    break;
	case 222:
	    Class288.method5275(class669, -1631846435);
	    break;
	case 275:
	    Class92.method1747(class669, (byte) 42);
	    break;
	case 682:
	    Class645.method10686(class669, (byte) 47);
	    break;
	case 777:
	    Class212.method4012(class669, (byte) 13);
	    break;
	case 625:
	    Class200_Sub15.method15605(class669, (byte) 74);
	    break;
	case 1014:
	    Class592.method9888(class669, 1472935656);
	    break;
	case 466:
	    Class397.method6574(class669, -401867256);
	    break;
	case 877:
	    Class646.method10689(class669, 1116457382);
	    break;
	case 177:
	    Class42.method1068(class669, -682253919);
	    break;
	case 514:
	    Class319.method5763(class669, (byte) 5);
	    break;
	case 1169:
	    Class242.method4468(class669, 296921695);
	    break;
	case 820:
	    Class390.method6541(class669, -2022120433);
	    break;
	case 21:
	    Class293.method5316(class669, (short) 630);
	    break;
	case 572:
	    Class508.method8370(class669, -1323526876);
	    break;
	case 1106:
	    Class654_Sub1_Sub5_Sub2.method18631(class669, 787076805);
	    break;
	case 33:
	    Class706_Sub4.method17304(class669, (byte) 1);
	    break;
	case 564:
	    Class215.method4110(class669, -705401752);
	    break;
	case 1136:
	    Class417.method6753(class669, 346731323);
	    break;
	case 642:
	    Class173.method2904(class669, 65535);
	    break;
	case 1267:
	    Class539.method8926(class669, (byte) -66);
	    break;
	case 249:
	    Class44_Sub2.method17248(class669, -1117459804);
	    break;
	case 259:
	    Class530.method8848(class669, (byte) -37);
	    break;
	case 1019:
	    Class499.method8271(class669, (short) 12342);
	    break;
	case 748:
	    Class455.method7420(class669, -1293810188);
	    break;
	case 1025:
	    Class221.method4148(class669, -1668824491);
	    break;
	case 638:
	    Class569.method9595(class669, -788786548);
	    break;
	case 237:
	    Class546.method8992(class669, -1828366178);
	    break;
	case 451:
	    Class58.method1247(class669, (byte) 4);
	    break;
	case 703:
	    Class429.method6810(class669, (byte) 1);
	    break;
	case 54:
	    Class389.method6532(class669, 411350722);
	    break;
	case 926:
	    Class58.method1251(class669, -333805937);
	    break;
	case 902:
	    Class460.method7510(class669, (byte) -105);
	    break;
	case 794:
	    Class118.method2129(class669, -1877606492);
	    break;
	case 1186:
	    Class166.method2752(class669, -123391537);
	    break;
	case 1244:
	    Class69.method1399(class669, (byte) 57);
	    break;
	case 782:
	    Class534_Sub1.method16017(class669, 1165844833);
	    break;
	case 874:
	    Class586.method9860(class669, -795783546);
	    break;
	case 386:
	    Class619.method10257(class669, (short) 16383);
	    break;
	case 391:
	    Class467.method7603(class669, 60955732);
	    break;
	case 424:
	    Class483.method7952(class669, -739744756);
	    break;
	case 218:
	    Class602.method10023(class669, (byte) 0);
	    break;
	case 681:
	    Class687.method13980(class669, -1193171438);
	    break;
	case 449:
	    Class189.method3762(class669, -1448663928);
	    break;
	case 622:
	    Class180.method2977(class669, 418311763);
	    break;
	case 513:
	    Class640.method10596(class669, (byte) 50);
	    break;
	case 1101:
	    Class259.method4795(class669, 559504715);
	    break;
	case 282:
	    Class108.method1968(class669, -1685908679);
	    break;
	case 1031:
	    Class195.method3813(class669, (byte) 95);
	    break;
	case 7:
	    Class181.method2982(class669, -1480257569);
	    break;
	case 879:
	    Class689.method14016(class669, (byte) 44);
	    break;
	case 937:
	    Class553.method9103(class669, -390427439);
	    break;
	case 1230:
	    Class207.method3939(class669, 1323700880);
	    break;
	case 437:
	    Class204.method3908(class669, (byte) 2);
	    break;
	case 818:
	    Class347_Sub1.method15772(class669, 1888770397);
	    break;
	case 1112:
	    Class105.method1941(class669, (byte) -13);
	    break;
	case 429:
	    Class382.method6447(class669, (byte) 40);
	    break;
	case 848:
	    Class311.method5670(class669, (short) 27965);
	    break;
	case 920:
	    Class672.method11097(class669, -1636434263);
	    break;
	case 484:
	    Class130.method2302(class669, (byte) -7);
	    break;
	case 683:
	    Class661.method10974(class669, (byte) 1);
	    break;
	case 1098:
	    Class464.method7561(class669, (byte) 19);
	    break;
	case 385:
	    Class597.method9935(class669, -213445509);
	    break;
	case 154:
	    Class577.method9770(class669, (byte) -18);
	    break;
	case 1043:
	    Class448.method7318(class669, (byte) -42);
	    break;
	case 1203:
	    Class632.method10473(class669, (byte) 109);
	    break;
	case 525:
	    Class319.method5761(class669, -1882267686);
	    break;
	case 413:
	    Class175.method2923(class669, 2102872681);
	    break;
	case 99:
	    Class509.method8392(class669, -1503469776);
	    break;
	case 489:
	    Class527.method8777(class669, -1524460875);
	    break;
	case 244:
	    Class260.method4808(class669, -1162996084);
	    break;
	case 1109:
	    Class376.method6417(class669, (byte) 0);
	    break;
	case 781:
	    Class616.method10227(class669, -371528432);
	    break;
	case 278:
	    Class555.method9223(class669, 1535293047);
	    break;
	case 925:
	    Class396.method6572(class669, (byte) 119);
	    break;
	case 297:
	    Class563_Sub1.method16404(class669, -1114506987);
	    break;
	case 1138:
	    Class453.method7404(class669, (short) 355);
	    break;
	case 562:
	    Class512.method8572(class669, -1678587583);
	    break;
	case 573:
	    Class659.method10915(class669, 131243213);
	    break;
	case 824:
	    Class130.method2301(class669, (byte) -82);
	    break;
	case 369:
	    Class334.method5893(class669, -1510752731);
	    break;
	case 1104:
	    Class705.method14232(class669, 1482687929);
	    break;
	case 550:
	    Class394.method6561(class669, 807863796);
	    break;
	case 1272:
	    Class162.method2638(class669, (byte) -48);
	    break;
	case 129:
	    Class534_Sub29.method16318(class669, 644802798);
	    break;
	case 574:
	    Class618.method10246(class669, (byte) 36);
	    break;
	case 155:
	    Class11.method615(class669, -515680258);
	    break;
	case 349:
	    Class515.method8588(class669, (byte) -54);
	    break;
	case 844:
	    Class464.method7562(class669, 1996356501);
	    break;
	case 89:
	    Class422.method6787(class669, 716251788);
	    break;
	case 556:
	    Class459.method7441(class669, (byte) 73);
	    break;
	case 578:
	    Class686.method13971(class669, (byte) 62);
	    break;
	case 1010:
	    Class451.method7389(class669, -458355624);
	    break;
	case 1114:
	    Class74.method1572(class669, 1121607925);
	    break;
	case 620:
	    Class556.method9397(class669, 308999563);
	    break;
	case 754:
	    Class184.method3225(class669, (byte) -11);
	    break;
	case 364:
	    Class559.method9428(class669, 1980361931);
	    break;
	case 1268:
	    Class198.method3830(class669, 912092868);
	    break;
	case 1222:
	    Class30.method885(class669, -1450795044);
	    break;
	case 31:
	    Class52.method1186(class669, 2028749454);
	    break;
	case 405:
	    Class200_Sub6.method15574(class669, -2023653173);
	    break;
	case 702:
	    Class553.method9104(class669, -1687545264);
	    break;
	case 586:
	    Class184.method3224(class669, 1226697136);
	    break;
	case 73:
	    Class328.method5829(class669, 591460272);
	    break;
	case 1226:
	    Class197.method3819(class669, -872496979);
	    break;
	case 80:
	    Class572.method9639(class669, (byte) 93);
	    break;
	case 672:
	    Class513.method8576(class669, 1768746357);
	    break;
	case 732:
	    Class238.method4425(class669, (byte) 88);
	    break;
	case 26:
	    Class198.method3831(class669, 791789604);
	    break;
	case 36:
	    Class523.method8724(class669, -95006648);
	    break;
	case 631:
	    Class44_Sub2.method17247(class669, (byte) 58);
	    break;
	case 867:
	    Class69.method1401(class669, -1130744008);
	    break;
	case 1129:
	    Class229.method4206(class669, -1411380580);
	    break;
	case 543:
	    Canvas_Sub1.method18492(class669, -1924295585);
	    break;
	case 677:
	    Class384.method6474(class669, 746217751);
	    break;
	case 759:
	    Class568.method9572(class669, (byte) 3);
	    break;
	case 743:
	    Class175_Sub2_Sub1.method17934(class669, (short) 23461);
	    break;
	case 864:
	    Class494.method8127(class669, 712890920);
	    break;
	case 1160:
	    Class532.method8879(class669, -610301340);
	    break;
	case 1007:
	    Class661.method10977(class669, -190847794);
	    break;
	case 203:
	    Class151.method2545(class669, (byte) 55);
	    break;
	case 186:
	    Class184.method3226(class669, (byte) 1);
	    break;
	case 907:
	    Class202.method3867(class669, (byte) 53);
	    break;
	case 1141:
	    Class193.method3792(class669, (short) -482);
	    break;
	case 1201:
	    Class402.method6594(class669, (byte) 110);
	    break;
	case 366:
	    Class227.method4180(class669, -832330600);
	    break;
	case 1011:
	    Class690_Sub42.method17232(class669, (byte) -14);
	    break;
	case 652:
	    Class649.method10716(class669, -1192292957);
	    break;
	case 169:
	    Class29.method876(class669, false, 1648632228);
	    break;
	case 786:
	    Class242.method4470(class669, (byte) -44);
	    break;
	case 1231:
	    Class172.method2899(class669, 2084757314);
	    break;
	case 706:
	    Class462.method7517(class669, 1884949535);
	    break;
	case 273:
	    Class285.method5259(class669, (byte) 19);
	    break;
	case 654:
	    Class645.method10687(class669, 1661787304);
	    break;
	case 1069:
	    Class464.method7568(class669, 572065441);
	    break;
	case 305:
	    Class259.method4797(class669, (byte) -67);
	    break;
	case 1099:
	    Class575.method9753(class669, -2025883819);
	    break;
	case 368:
	    Class233.method4340(class669, 744805591);
	    break;
	case 813:
	    Class535.method8899(class669, 732514046);
	    break;
	case 233:
	    Class193.method3797(class669, 1913168747);
	    break;
	case 1194:
	    Class468.method7626(class669, 1367846880);
	    break;
	case 755:
	    Class410.method6710(class669, 1955060796);
	    break;
	case 972:
	    Class658.method10908(class669, 1511538416);
	    break;
	case 1002:
	    Class578.method9796(class669, 1189271583);
	    break;
	case 1251:
	    Class285.method5261(class669, -316133416);
	    break;
	case 1117:
	    Class608.method10068(class669, -1161207937);
	    break;
	case 851:
	    Class301.method5559(class669, -514868111);
	    break;
	case 1227:
	    Class45.method1103(class669, 2017536816);
	    break;
	case 1066:
	    Class549.method9018(class669, -686496367);
	    break;
	case 901:
	    Class581_Sub1.method16305(class669, 1060684989);
	    break;
	case 805:
	    Class497.method8138(class669, -962046250);
	    break;
	case 234:
	    Class628.method10381(class669, 2088438307);
	    break;
	case 170:
	    Class55.method1216(class669, -1957565737);
	    break;
	case 940:
	    Class300.method5557(class669, 1410800796);
	    break;
	case 577:
	    Class571.method9629(class669, 2130763198);
	    break;
	case 1048:
	    Class677.method11141(class669, -997680990);
	    break;
	case 145:
	    Class625.method10326(class669, (byte) 21);
	    break;
	case 199:
	    Class508.method8369(class669, (byte) 61);
	    break;
	case 617:
	    Class117.method2123(false, class669, -2134519740);
	    break;
	case 804:
	    Class385.method6491(class669, -1410120886);
	    break;
	case 772:
	    Class451_Sub1.method15922(class669, 1037206032);
	    break;
	case 1049:
	    Class108.method1966(class669, (byte) -36);
	    break;
	case 1261:
	    Class560.method9429(class669, -228812539);
	    break;
	case 55:
	    Class661.method10973(class669, (byte) -107);
	    break;
	case 662:
	    Class49.method1151(class669, (byte) 122);
	    break;
	case 134:
	    Class502.method8303(class669, -227639145);
	    break;
	case 279:
	    Class70.method1410(class669, -2037498533);
	    break;
	case 507:
	    Class177.method2936(class669, (byte) -20);
	    break;
	case 1161:
	    Class597.method9937(class669, (byte) 1);
	    break;
	case 531:
	    Class383.method6458(class669, (byte) -19);
	    break;
	case 226:
	    Class197.method3820(class669, (byte) -35);
	    break;
	case 1151:
	    Class299.method5514(class669, (byte) 103);
	    break;
	case 343:
	    Class556.method9395(class669, 948199663);
	    break;
	case 789:
	    Class347_Sub3.method15870(class669, -948474127);
	    break;
	case 1240:
	    Class110.method1973(class669, -1508571943);
	    break;
	case 1083:
	    Class572.method9638(class669, (byte) 42);
	    break;
	case 918:
	    Class222.method4150(class669, -252725459);
	    break;
	case 521:
	    Class459.method7440(class669, 196563595);
	    break;
	case 793:
	    Class461.method7511(class669, -1996100533);
	    break;
	case 110:
	    Class389.method6533(class669, -1189686698);
	    break;
	case 911:
	    Class201.method3863(class669, (byte) 0);
	    break;
	case 1094:
	    Class637.method10556(class669, (short) -3506);
	    break;
	case 1224:
	    Class5.method563(class669, (byte) 74);
	    break;
	case 1192:
	    Class312.method5681(class669, 677726012);
	    break;
	case 840:
	    Class607.method10063(class669, -2050588046);
	    break;
	case 607:
	    Class56.method1227(class669, -1082914252);
	    break;
	case 627:
	    Class116.method2117(class669, -168560958);
	    break;
	case 619:
	    Class598.method9940(class669, -116815718);
	    break;
	case 512:
	    Class489.method8002(class669, (short) 877);
	    break;
	case 328:
	    Class561.method9448(class669, 838495023);
	    break;
	case 712:
	    Class160.method2618(class669, -653673153);
	    break;
	case 173:
	    Class665.method11008(class669, 1008610128);
	    break;
	case 1145:
	    Class236.method4412(class669, (byte) -71);
	    break;
	case 694:
	    Class497.method8142(class669, (byte) -100);
	    break;
	case 580:
	    Class259.method4799(class669, (byte) 1);
	    break;
	case 1193:
	    Class534_Sub18_Sub2.method17847(class669, -2013150476);
	    break;
	case 126:
	    Class698.method14122(class669, (byte) 14);
	    break;
	case 516:
	    Class565.method9529(class669, (byte) -65);
	    break;
	case 621:
	    Class621.method10279(class669, (byte) 44);
	    break;
	case 1253:
	    Class650.method10720(class669, -620482084);
	    break;
	case 296:
	    Class64.method1290(class669, 484584995);
	    break;
	case 696:
	    Class431.method6836(class669, (byte) 54);
	    break;
	case 758:
	    Class25.method855(class669, (byte) -125);
	    break;
	case 983:
	    Class602.method10024(class669, 926784052);
	    break;
	case 12:
	    Class243.method4481(class669, -672888615);
	    break;
	case 492:
	    Class294.method5322(class669, 614193231);
	    break;
	case 63:
	    Class309.method5660(class669, (short) -21951);
	    break;
	case 567:
	    Class40.method1035(true, class669, 2037422562);
	    break;
	case 736:
	    Class173.method2905(class669, (short) 324);
	    break;
	case 387:
	    Class625.method10325(class669, (byte) 4);
	    break;
	case 78:
	    Class391.method6547(class669, (byte) -18);
	    break;
	case 1223:
	    Class605.method10036(class669, -637162661);
	    break;
	case 1082:
	    Class322.method5780(class669, -72623893);
	    break;
	case 664:
	    Class403.method6612(class669, -464989676);
	    break;
	case 727:
	    Class393.method6553(class669, -1623816431);
	    break;
	case 100:
	    Class621.method10281(class669, -825554671);
	    break;
	case 420:
	    Class295.method5326(class669, 452606295);
	    break;
	case 427:
	    Class522.method8716(class669, 1942642852);
	    break;
	case 266:
	    Class245.method4501(class669, (byte) 0);
	    break;
	case 29:
	    Class242.method4472(class669, -1204170473);
	    break;
	case 481:
	    Class515.method8587(class669, -902661093);
	    break;
	case 798:
	    Class89.method1715(class669, 189295939);
	    break;
	case 524:
	    Class396.method6573(class669, -1769864908);
	    break;
	case 1046:
	    Class652.method10737(class669, (short) 255);
	    break;
	case 179:
	    Class534_Sub18_Sub12.method18380(class669, (byte) 4);
	    break;
	case 594:
	    Class408.method6695(class669, 843180954);
	    break;
	case 1072:
	    Class612.method10105(class669, -464888486);
	    break;
	case 197:
	    Class534_Sub21.method16239(class669, (short) -4378);
	    break;
	case 522:
	    Class332.method5862(class669, 299717409);
	    break;
	case 919:
	    Class419.method6768(class669, -1664625522);
	    break;
	case 498:
	    Class630.method10431(class669, (byte) 100);
	    break;
	case 530:
	    Class246.method4511(class669, -999365920);
	    break;
	case 142:
	    Class571.method9632(class669, -922821565);
	    break;
	case 817:
	    Class616.method10228(class669, 1365170584);
	    break;
	case 528:
	    Class415.method6742(class669, -625881771);
	    break;
	case 216:
	    Class270.method5034(class669, 53425196);
	    break;
	case 457:
	    Class621.method10280(class669, (short) 10232);
	    break;
	case 490:
	    Class171_Sub5.method15614(class669, -2043165283);
	    break;
	case 257:
	    Class403.method6619(class669, 952039894);
	    break;
	case 1176:
	    Class42.method1064(class669, (byte) 58);
	    break;
	case 950:
	    Class274.method5142(class669, (short) 1234);
	    break;
	case 871:
	    Class511.method8409(class669, -1682340394);
	    break;
	case 68:
	    Class57.method1243(class669, (byte) -34);
	    break;
	case 779:
	    Class504.method8322(class669, (byte) -28);
	    break;
	case 193:
	    Class98.method1836(class669, 1215372012);
	    break;
	case 976:
	    Class575.method9759(class669, 774855081);
	    break;
	case 267:
	    Class474.method7760(class669, (byte) 83);
	    break;
	case 1042:
	    Class209.method3951(class669, 1829189220);
	    break;
	case 825:
	    Class48.method1137(class669, (byte) 72);
	    break;
	case 1243:
	    Class633.method10497(class669, -1689299442);
	    break;
	case 995:
	    Class384.method6475(class669, (byte) 30);
	    break;
	case 227:
	    Class44_Sub2.method17246(class669, 119689634);
	    break;
	case 1084:
	    Class614.method10126(class669, (byte) 10);
	    break;
	case 852:
	    Class613.method10112(class669, -77074570);
	    break;
	case 526:
	    IcmpService_Sub1.method17015(class669, -1371604503);
	    break;
	case 395:
	    Class527.method8776(class669, (byte) 38);
	    break;
	case 931:
	    Class522.method8714(class669, (byte) -44);
	    break;
	case 1248:
	    Class110_Sub1.method14505(class669, 471032570);
	    break;
	case 1262:
	    Class536_Sub1.method15936(class669, 1138874191);
	    break;
	case 194:
	    Class266.method4860(class669, 2049748206);
	    break;
	case 1091:
	    Class252.method4630(class669, -636940167);
	    break;
	case 1260:
	    Class314_Sub1.method15643(class669, 1132105270);
	    break;
	case 833:
	    Class685.method13959(class669, 1721343821);
	    break;
	case 888:
	    Class211.method3959(class669, 1557236664);
	    break;
	case 205:
	    Class517.method8633(class669, 330009361);
	    break;
	case 6:
	    Class686.method13966(class669, -506201215);
	    break;
	case 1026:
	    Class227.method4179(class669, 692306868);
	    break;
	case 1168:
	    Class301.method5561(class669, -646407075);
	    break;
	case 1047:
	    Class687.method13979(class669, -150900843);
	    break;
	case 474:
	    Class269.method5021(class669, (byte) -122);
	    break;
	case 535:
	    Class145_Sub2.method15496(class669, (byte) 23);
	    break;
	case 394:
	    Class155_Sub1.method15472(class669, 1402894341);
	    break;
	case 1242:
	    Class318.method5756(class669, (byte) -110);
	    break;
	case 428:
	    Class543.method8959(class669, -58668644);
	    break;
	case 569:
	    Class328.method5830(class669, 584914825);
	    break;
	case 841:
	    Class321.method5772(class669, -657804467);
	    break;
	case 623:
	    Class494.method8125(class669, -1805975754);
	    break;
	case 649:
	    Class219.method4141(class669, -866036587);
	    break;
	case 1057:
	    Class449.method7329(class669, (short) 215);
	    break;
	case 737:
	    Class639.method10590(class669, 279569723);
	    break;
	case 611:
	    Class650.method10723(class669, 1192875751);
	    break;
	case 816:
	    Class310.method5665(class669, -2009416100);
	    break;
	case 3:
	    Class662.method10989(class669, (byte) 63);
	    break;
	case 280:
	    Class513.method8581(class669, (byte) 0);
	    break;
	case 668:
	    Class327_Sub2.method15704(class669, -1952919816);
	    break;
	case 584:
	    Class271.method5041(class669, -2101637403);
	    break;
	case 432:
	    Class279.method5227(class669, -704731866);
	    break;
	case 947:
	    Class679.method13861(class669, 1483377985);
	    break;
	case 494:
	    Class171.method2885(class669, -1495004017);
	    break;
	case 733:
	    Class250.method4610(class669, (byte) 6);
	    break;
	case 1157:
	    Class198.method3829(class669, 322760217);
	    break;
	case 1125:
	    Class457.method7430(class669, -1096544745);
	    break;
	case 347:
	    Class46.method1113(class669, (byte) -15);
	    break;
	case 79:
	    Class288.method5273(class669, (byte) -69);
	    break;
	case 1124:
	    Class531.method8861(class669, (byte) -69);
	    break;
	case 914:
	    Class199.method3841(class669, -1381612249);
	    break;
	case 468:
	    Class481.method7925(class669, 1512696892);
	    break;
	case 258:
	    Class457.method7431(class669, (byte) -9);
	    break;
	case 806:
	    Class259.method4798(class669, (byte) 36);
	    break;
	case 616:
	    Class534_Sub24.method16261(class669, 79369030);
	    break;
	case 974:
	    Class75_Sub1.method17365(class669, (byte) -112);
	    break;
	case 1088:
	    Class411.method6719(class669, -1445027643);
	    break;
	case 342:
	    Class521.method8689(class669, (byte) 1);
	    break;
	case 669:
	    Class585.method9847(class669, 331693517);
	    break;
	case 980:
	    Class655.method10853(class669, (byte) -33);
	    break;
	case 707:
	    Class653.method10799(class669, 54915007);
	    break;
	case 152:
	    Class471.method7658(class669, 1046269871);
	    break;
	case 292:
	    Class195.method3814(class669, (byte) 69);
	    break;
	case 283:
	    Class473.method7756(class669, (byte) 1);
	    break;
	case 1187:
	    Class624.method10312(class669, 897537681);
	    break;
	case 315:
	    Class103.method1919(class669, -503686790);
	    break;
	case 1188:
	    Class537.method8909(class669, 823049952);
	    break;
	case 1219:
	    Class234.method4344(class669, (byte) 78);
	    break;
	case 1061:
	    Class206_Sub1.method15705(class669, 490303723);
	    break;
	default:
	    throw new RuntimeException();
	case 646:
	    Class204.method3910(class669, (short) -17830);
	    break;
	case 245:
	    Class48.method1139(class669, (byte) -110);
	    break;
	case 981:
	    Class587.method9867(class669, (byte) -81);
	    break;
	case 48:
	    Class276.method5158(class669, (byte) 8);
	    break;
	case 797:
	    Class534_Sub18_Sub10.method18286(class669, (byte) 42);
	    break;
	case 396:
	    Class351.method6184(class669, (byte) 82);
	    break;
	case 1191:
	    Class30.method881(class669, -1974469502);
	    break;
	case 225:
	    Class93.method1762(class669, (short) -23051);
	    break;
	case 269:
	    Class690_Sub14.method17009(class669, -496904101);
	    break;
	case 751:
	    Class318.method5757(class669, (byte) 127);
	    break;
	case 1034:
	    Class449.method7328(class669, (byte) 42);
	    break;
	case 606:
	    Class637.method10558(class669, (byte) -76);
	    break;
	case 319:
	    Class393.method6552(class669, 667151382);
	    break;
	case 944:
	    Class188.method3756(class669, 1682553718);
	    break;
	case 1121:
	    Class332.method5860(class669, (byte) 1);
	    break;
	case 518:
	    Class534_Sub21.method16240(class669, 919999391);
	    break;
	case 576:
	    Class439.method7095(class669, (byte) -5);
	    break;
	case 1009:
	    Class565.method9527(class669, -1224990025);
	    break;
	case 272:
	    Class174.method2909(class669, (byte) -41);
	    break;
	case 285:
	    Class534_Sub8.method16068(class669, -925096913);
	    break;
	case 1153:
	    Class483.method7949(class669, (short) -7970);
	    break;
	case 544:
	    Class268.method4906(class669, (byte) -22);
	    break;
	case 439:
	    Class48.method1138(class669, 131973935);
	    break;
	case 719:
	    Class557.method9409(class669, -388040117);
	    break;
	case 721:
	    Class534_Sub19.method16191(class669, 237747886);
	    break;
	case 869:
	    Class536_Sub2.method15978(class669, (byte) 49);
	    break;
	case 880:
	    Class348_Sub1_Sub1.method17980(class669, (byte) 2);
	    break;
	case 210:
	    Class421.method6782(class669, -984204153);
	    break;
	case 440:
	    Class710.method14324(class669, 16711935);
	    break;
	case 538:
	    Class567.method9558(class669, 1288359491);
	    break;
	case 1076:
	    Class383.method6461(class669, (short) 22108);
	    break;
	case 1071:
	    Class571.method9633(class669, -874630312);
	    break;
	case 546:
	    Class63.method1278(class669, -1303704760);
	    break;
	case 1216:
	    Class618.method10245(class669, -316296773);
	    break;
	case 418:
	    Class62.method1265(class669, 137233038);
	    break;
	case 614:
	    Class110_Sub1_Sub1.method17867(class669, -1285415288);
	    break;
	case 178:
	    Class547.method9013(class669, 100663296);
	    break;
	case 884:
	    Class676.method11134(class669, -1254652754);
	    break;
	case 904:
	    Class519.method8651(class669, -1892372132);
	    break;
	case 168:
	    Class680.method13863(class669, (byte) 1);
	    break;
	case 883:
	    Class287.method5268(class669, -1256129294);
	    break;
	case 9:
	    Class172.method2901(class669, 57243683);
	    break;
	case 671:
	    Class10.method608(class669, -321377060);
	    break;
	case 399:
	    Class295.method5327(class669, 1012015072);
	    break;
	case 150:
	    Class232.method4338(class669, (short) -2984);
	    break;
	case 379:
	    Class246.method4510(class669, (byte) -26);
	    break;
	case 120:
	    Class534_Sub16.method16179(class669, (byte) 54);
	    break;
	case 1033:
	    Class94.method1763(class669, 1174330691);
	    break;
	case 19:
	    Class295.method5324(class669, -1201797937);
	    break;
	case 1156:
	    Class563.method9511(class669, 1933731628);
	    break;
	case 470:
	    Class389.method6534(class669, (short) 712);
	    break;
	case 1012:
	    Class71.method1415(class669, (byte) 6);
	    break;
	case 612:
	    Class534_Sub42.method16819(class669, -1894389632);
	    break;
	case 190:
	    Class222.method4151(class669, 1883506113);
	    break;
	case 238:
	    Class214.method4102(class669, 1817172891);
	    break;
	case 1022:
	    Class616.method10226(class669, -861823081);
	    break;
	case 220:
	    Class626.method10367(class669, (byte) -106);
	    break;
	case 1045:
	    Class236.method4410(class669, 698706077);
	    break;
	case 722:
	    Class455.method7424(class669, 2131918041);
	    break;
	case 139:
	    Class316.method5721(class669, -426609471);
	    break;
	case 835:
	    Class561.method9447(class669, (byte) -58);
	    break;
	case 86:
	    Class1.method510(class669, 1248333305);
	    break;
	case 330:
	    Class647.method10692(class669, (byte) -8);
	    break;
	case 575:
	    Class654_Sub1_Sub3_Sub1.method18643(class669, 1754668900);
	    break;
	case 373:
	    Class158.method2593(class669, -406923290);
	    break;
	case 792:
	    Class616.method10224(class669, -1703607233);
	    break;
	case 963:
	    Class224.method4165(class669, (byte) -107);
	    break;
	case 376:
	    Class477.method7781(class669, (byte) 35);
	    break;
	case 1217:
	    Exception_Sub1.method17925(class669, -1951882507);
	    break;
	case 172:
	    Class617.method10232(class669, (byte) -1);
	    break;
	case 61:
	    Class179.method2974(class669, (byte) 78);
	    break;
	case 956:
	    Class555.method9224(class669, 984973778);
	    break;
	case 323:
	    Class593.method9897(class669, 2043411478);
	    break;
	case 380:
	    Class286.method5267(class669, (byte) 1);
	    break;
	case 224:
	    Class700.method14200(class669, (byte) 42);
	    break;
	case 345:
	    Class590.method9877(class669, 1513551912);
	    break;
	case 60:
	    Class321.method5773(class669, (byte) 60);
	    break;
	case 1148:
	    Class334.method5894(class669, -1095230001);
	    break;
	case 412:
	    Class534_Sub18_Sub3.method17877(class669, 269244320);
	    break;
	case 65:
	    Class607.method10060(class669, (byte) 18);
	    break;
	case 734:
	    Class16.method768(class669, (byte) 7);
	    break;
	case 75:
	    Class507.method8367(class669, (short) -5362);
	    break;
	case 862:
	    Class449.method7325(class669, -1214759298);
	    break;
	case 679:
	    Class44_Sub3.method17256(class669, -862799708);
	    break;
	case 370:
	    Class571.method9630(class669, -2009860273);
	    break;
	case 629:
	    Class197.method3818(class669, 706823674);
	    break;
	case 372:
	    Class565.method9526(class669, 544374084);
	    break;
	case 776:
	    Class402.method6593(class669, false, false, 1784278896);
	    break;
	case 684:
	    Class227.method4186(class669, 1022573795);
	    break;
	case 332:
	    Class394_Sub3.method15850(class669, -652546224);
	    break;
	case 213:
	    Class263.method4836(class669, -320237349);
	    break;
	case 288:
	    Class658.method10910(class669, (byte) -77);
	    break;
	case 909:
	    Class390.method6536(class669, 462308082);
	    break;
	case 1107:
	    Class236.method4411(class669, -1596540284);
	    break;
	case 766:
	    Class478.method7803(class669, (short) -4103);
	    break;
	case 389:
	    Class263.method4834(class669, (byte) 1);
	    break;
	case 277:
	    Class514.method8583(class669, (short) 10784);
	    break;
	case 723:
	    Class60.method1256(class669, (short) 11075);
	    break;
	case 496:
	    Class464.method7564(class669, 879958593);
	    break;
	case 460:
	    Class492.method8098(class669, 308999563);
	    break;
	case 109:
	    Class681.method13866(class669, 1980953670);
	    break;
	case 153:
	    Class649.method10715(class669, -43070897);
	    break;
	case 962:
	    Class686.method13972(class669, (byte) -95);
	    break;
	case 591:
	    Class502_Sub2.method15984(class669, (short) -11570);
	    break;
	case 411:
	    Class341.method5944(class669, (byte) 96);
	    break;
	case 111:
	    Class536_Sub1.method15935(class669, (byte) -29);
	    break;
	case 1214:
	    Class333.method5868(class669, 1753126853);
	    break;
	case 975:
	    Class189.method3759(class669, 1155533090);
	    break;
	case 954:
	    Class557.method9410(class669, (byte) 95);
	    break;
	case 560:
	    Class572.method9635(class669, -1910144205);
	    break;
	case 310:
	    Class522.method8712(class669, (byte) 0);
	    break;
	case 1030:
	    Class172.method2897(class669, 1676901714);
	    break;
	case 35:
	    Class352.method6258(class669, (byte) -116);
	    break;
	case 932:
	    Class402.method6593(class669, true, false, 1784278896);
	    break;
	case 1110:
	    Class386.method6492(class669, 2145130997);
	    break;
	case 175:
	    Class337.method5904(class669, -1251622999);
	    break;
	case 69:
	    Class540.method8939(class669, 638004337);
	    break;
	case 1005:
	    Class189.method3758(class669, (byte) -19);
	    break;
	case 1264:
	    Class203.method3903(class669, 1957531378);
	    break;
	case 648:
	    Class547.method9010(class669, -1398077659);
	    break;
	case 634:
	    Class699.method14127(class669, 1817888743);
	    break;
	case 459:
	    Class575.method9754(class669, 624530162);
	    break;
	case 1276:
	    Class553.method9102(class669, 65535);
	    break;
	case 823:
	    Class301.method5558(class669, 1431006983);
	    break;
	case 51:
	    Class159.method2614(class669, -1024904969);
	    break;
	case 384:
	    Class690_Sub11.method16984(class669, 892052256);
	    break;
	case 640:
	    Class311.method5669(class669, -451867190);
	    break;
	case 984:
	    Class638.method10566(class669, 1204392335);
	    break;
	case 434:
	    Class448.method7320(class669, (short) 21371);
	    break;
	case 829:
	    Class546.method8990(class669, -1114260217);
	    break;
	case 1144:
	    Class654_Sub1_Sub1_Sub2.method18635(class669, (byte) 31);
	    break;
	case 1020:
	    Class705.method14230(class669, 477351428);
	    break;
	case 714:
	    Class539.method8929(class669, (byte) 14);
	    break;
	case 135:
	    Class290.method5284(class669, (byte) 18);
	    break;
	case 1164:
	    Class440.method7099(class669, -1900618053);
	    break;
	case 659:
	    Class367.method6367(class669, 198839010);
	    break;
	case 955:
	    Class536_Sub4.method15995(class669, -240649804);
	    break;
	case 673:
	    Class213.method4022(class669, 2103919497);
	    break;
	case 986:
	    Class458.method7435(class669, (byte) -99);
	    break;
	case 527:
	    Class68.method1388(class669, (byte) 1);
	    break;
	case 590:
	    Class16.method767(class669, 1396106295);
	    break;
	case 1237:
	    Class186.method3702(class669, (byte) 13);
	    break;
	case 549:
	    Class599.method9946(class669, true, -1551974061);
	    break;
	case 655:
	    Class277.method5162(class669, (byte) 15);
	    break;
	case 164:
	    Class86.method1699(class669, -1964021424);
	    break;
	case 480:
	    Class422.method6788(class669, (byte) 15);
	    break;
	case 667:
	    Class292.method5303(class669, 392246188);
	    break;
	case 1111:
	    Class566.method9543(class669, 1896261791);
	    break;
	case 182:
	    Class403.method6618(class669, (byte) -115);
	    break;
	case 517:
	    Class45.method1102(class669, -1436286377);
	    break;
	case 892:
	    Class489.method8001(class669, 2112597205);
	    break;
	case 1275:
	    Class511.method8406(class669, -1167082670);
	    break;
	case 957:
	    Class551.method9049(class669, (byte) -22);
	    break;
	case 260:
	    Class312_Sub3.method15675(class669, -1492241038);
	    break;
	case 692:
	    Class200_Sub9.method15584(class669, (byte) 0);
	    break;
	case 313:
	    Class293.method5313(class669, -1315232671);
	    break;
	case 596:
	    Class341.method5945(class669, 607340662);
	    break;
	case 1027:
	    Class267.method4869(class669, -1758682918);
	    break;
	case 444:
	    Class385.method6490(class669, 16711935);
	    break;
	case 436:
	    Class244.method4487(class669, (byte) -123);
	    break;
	case 191:
	    Class646.method10688(class669, -1546930663);
	    break;
	case 324:
	    Class484.method7956(class669, 2073326999);
	    break;
	case 1189:
	    Class210.method3954(class669, -264847972);
	    break;
	case 325:
	    Class599.method9945(class669, (byte) -94);
	    break;
	case 710:
	    Class155.method2580(class669, -1203728391);
	    break;
	case 885:
	    Class200.method3855(class669, -857077847);
	    break;
	case 1089:
	    Class437.method6990(class669, 319168010);
	    break;
	case 1197:
	    Class577.method9772(class669, 270265477);
	    break;
	case 151:
	    Class534_Sub4.method16038(class669, (byte) 12);
	    break;
	case 602:
	    Class62.method1263(class669, (byte) -104);
	    break;
	case 426:
	    Class651.method10730(class669, 2088348520);
	    break;
	case 603:
	    Class690_Sub3.method16844(class669, -59123689);
	    break;
	case 773:
	    Class566.method9544(class669, 1009859566);
	    break;
	case 1190:
	    Class200_Sub18.method15630(class669, -2028521702);
	    break;
	case 162:
	    Class614.method10122(class669, 617999126);
	    break;
	case 557:
	    Class259.method4796(class669, (byte) 12);
	    break;
	case 140:
	    Class312_Sub2.method15670(class669, (short) -32270);
	    break;
	case 183:
	    Class546.method8993(class669, 157605406);
	    break;
	case 1206:
	    Class608.method10067(class669, -1518148391);
	    break;
	case 836:
	    Class28.method866(class669, -881188269);
	    break;
	case 365:
	    Class567.method9564(class669, (byte) 107);
	    break;
	case 381:
	    Class535.method8897(class669, 617999126);
	    break;
	case 95:
	    Class28.method860(class669, 367776947);
	    break;
	case 506:
	    Class200_Sub1.method15566(class669, (short) 7538);
	    break;
	case 938:
	    Class455.method7421(class669, 509542726);
	    break;
	case 281:
	    Class253.method4636(class669, 502264292);
	    break;
	case 1155:
	    Class534_Sub6.method16067(class669, 31645619);
	    break;
	case 942:
	    Class534_Sub11_Sub14.method18231(class669, 1906298477);
	    break;
	case 680:
	    Class691.method14042(class669, 773846728);
	    break;
	case 265:
	    Class511.method8407(class669, (byte) 79);
	    break;
	case 922:
	    Class670.method11041(class669, (byte) 13);
	    break;
	case 502:
	    Class539.method8927(class669, -2044438582);
	    break;
	case 595:
	    Class219.method4139(class669, 1251685998);
	    break;
	case 563:
	    Class630.method10430(class669, 1129207420);
	    break;
	case 753:
	    Class429.method6806(class669, (byte) -9);
	    break;
	case 997:
	    Class693.method14054(class669, 1221206332);
	    break;
	case 985:
	    Class608.method10069(class669, 1485266147);
	    break;
	case 800:
	    Class621.method10283(class669, 220112950);
	    break;
	case 949:
	    Class639.method10587(class669, (byte) -70);
	    break;
	case 336:
	    Class386.method6493(class669, (short) 1814);
	    break;
	case 421:
	    Class110.method1972(class669, (byte) -36);
	    break;
	case 961:
	    Class192.method3791(class669, 1903171565);
	    break;
	case 924:
	    Class98.method1835(class669, 2016794288);
	    break;
	case 783:
	    Class280.method5231(class669, 1498836187);
	    break;
	case 363:
	    Class11.method613(class669, -1707993844);
	    break;
	case 1126:
	    Class262.method4822(class669, (byte) -37);
	    break;
	case 977:
	    Class395.method6566(class669, (byte) -1);
	    break;
	case 119:
	    Class535.method8898(class669, -382310878);
	    break;
	case 993:
	    Class28.method869(class669, (byte) 27);
	    break;
	case 1204:
	    Class517.method8634(class669, (byte) -12);
	    break;
	case 487:
	    Class587.method9868(class669, (byte) 1);
	    break;
	case 1249:
	    Class440.method7097(class669, 229264954);
	    break;
	case 84:
	    Class218.method4117(class669, -2030169851);
	    break;
	case 108:
	    Class482.method7933(class669, (byte) -7);
	    break;
	case 358:
	    Class531.method8858(class669, 895692310);
	    break;
	case 334:
	    Class387.method6501(class669, -444124714);
	    break;
	case 130:
	    Class608.method10066(class669, 2106942012);
	    break;
	case 270:
	    Class114.method2109(class669, -1821465447);
	    break;
	case 1162:
	    Class465.method7569(class669, -413752399);
	    break;
	case 588:
	    Class171.method2884(class669, (short) 404);
	    break;
	case 688:
	    Class582.method9833(class669, 1670070052);
	    break;
	case 571:
	    Class117.method2125(class669, (short) 9219);
	    break;
	case 760:
	    Class44_Sub18.method17360(class669, (byte) -126);
	    break;
	case 85:
	    Class651.method10731(class669, 1330817033);
	    break;
	case 731:
	    Class539_Sub1.method15924(class669, (byte) 16);
	    break;
	case 600:
	    Class453_Sub3.method15987(class669, (short) -8569);
	    break;
	case 964:
	    Class573.method9642(class669, (byte) 102);
	    break;
	case 718:
	    Class213.method4019(class669, (byte) 7);
	    break;
	case 149:
	    Class491.method8083(class669, 1625235324);
	    break;
	case 1228:
	    Class200_Sub15.method15606(class669, -145417917);
	    break;
	case 398:
	    Class592.method9885(class669, -1744247759);
	    break;
	case 1040:
	    Class623.method10308(class669, (byte) -25);
	    break;
	case 1018:
	    Class429.method6807(class669, -180658326);
	    break;
	case 495:
	    Class534_Sub16.method16178(class669, -1783466193);
	    break;
	case 77:
	    Class628.method10382(class669, (byte) 112);
	    break;
	case 1175:
	    Class526.method8757(class669, 1121084118);
	    break;
	case 605:
	    Class63.method1279(class669, -1977339141);
	    break;
	case 745:
	    Class623.method10309(class669, -1322300601);
	    break;
	case 50:
	    Class173.method2906(class669, 1190447801);
	    break;
	case 166:
	    Class28.method867(class669, 1397863462);
	    break;
	case 503:
	    Class559.method9423(class669, -2140816019);
	    break;
	case 953:
	    Class523.method8725(class669, -643037592);
	    break;
	case 433:
	    Class30.method880(class669, -678419600);
	    break;
	case 1029:
	    Class415.method6741(class669, (byte) 95);
	    break;
	case 308:
	    Class281.method5239(class669, (byte) 79);
	    break;
	case 670:
	    Class58.method1252(class669, (byte) -12);
	    break;
	case 1041:
	    Class484.method7958(class669, (byte) 66);
	    break;
	case 969:
	    Class330.method5852(class669, 326646494);
	    break;
	case 900:
	    Class556.method9401(class669, 271329503);
	    break;
	case 1165:
	    Class234.method4342(class669, (byte) 0);
	    break;
	case 647:
	    Class674.method11121(class669, (byte) 97);
	    break;
	case 497:
	    Class534_Sub41.method16765(class669, 384635331);
	    break;
	case 276:
	    Class480.method7923(class669, 1741832875);
	    break;
	case 960:
	    Class522.method8719(class669, -665834243);
	    break;
	case 406:
	    Class406.method6680(class669, 1007357703);
	    break;
	case 286:
	    Class165.method2742(class669, -769203796);
	    break;
	case 536:
	    Class417.method6756(class669, (byte) -65);
	    break;
	case 608:
	    Class67.method1384(class669, -656539242);
	    break;
	case 82:
	    Class222.method4152(class669, -142859585);
	    break;
	case 750:
	    Class534_Sub18_Sub18_Sub1.method18767(class669, 355802670);
	    break;
	case 246:
	    Class197.method3823(class669, -377026457);
	    break;
	case 613:
	    Class288.method5278(class669, (byte) 11);
	    break;
	case 866:
	    Class690_Sub16.method17033(class669, -816798109);
	    break;
	case 501:
	    Class543.method8957(class669, (byte) 0);
	    break;
	case 59:
	    Class703.method14220(class669, (byte) 74);
	    break;
	case 1127:
	    Class257.method4676(class669, 524703491);
	    break;
	case 46:
	    Class200_Sub2.method15569(class669, (byte) 1);
	    break;
	case 204:
	    Class214.method4101(class669, 160131578);
	    break;
	case 765:
	    Class655.method10857(class669, 1395446813);
	    break;
	case 346:
	    Class492.method8095(class669, (byte) -102);
	    break;
	case 348:
	    Class235.method4404(class669, (byte) 11);
	    break;
	case 419:
	    Class622.method10288(class669, -1505546417);
	    break;
	case 361:
	    Class168.method2758(class669, (byte) 10);
	    break;
	case 678:
	    Class454.method7415(class669, 1256525053);
	    break;
	case 770:
	    Class695.method14073(class669, 1796624426);
	    break;
	case 304:
	    Class291.method5298(class669, -1366907897);
	    break;
	case 1208:
	    Class184.method3223(class669, (byte) -8);
	    break;
	case 392:
	    Class25.method857(class669, 2121781252);
	    break;
	case 738:
	    Class192.method3790(class669, (short) 9186);
	    break;
	case 651:
	    Class507.method8365(class669, -48904906);
	    break;
	case 469:
	    Class559.method9424(class669, (byte) 55);
	    break;
	case 144:
	    Class236.method4409(class669, (byte) -13);
	    break;
	case 125:
	    Class130.method2304(class669, (short) 30162);
	    break;
	case 729:
	    Class498.method8259(class669, -734095561);
	    break;
	case 819:
	    Class238.method4427(class669, -2106712377);
	    break;
	case 930:
	    Class654_Sub1_Sub5.method18488(class669, 1865726184);
	    break;
	case 762:
	    Class390.method6540(class669, (byte) 0);
	    break;
	case 1067:
	    Class604.method10029(class669, -1370243166);
	    break;
	case 116:
	    Class429.method6805(class669, 1460193483);
	    break;
	case 1182:
	    Class32.method897(class669, -2044563335);
	    break;
	case 1001:
	    Class580.method9814(class669, (byte) -97);
	    break;
	case 407:
	    Class633.method10495(class669, 1414042615);
	    break;
	case 208:
	    Class489.method8006(class669, 83007835);
	    break;
	case 72:
	    Class200_Sub4.method15572(class669, (short) -16855);
	    break;
	case 158:
	    Class658.method10907(class669, -1983931113);
	    break;
	case 45:
	    Class227.method4187(class669, -1811684878);
	    break;
	case 483:
	    Class577.method9768(class669, -1638128734);
	    break;
	case 435:
	    Class453.method7406(class669, 2124800009);
	    break;
	case 1274:
	    Class492.method8093(class669, -1445458344);
	    break;
	case 787:
	    Class642.method10620(class669, (byte) 39);
	    break;
	case 740:
	    Class162.method2639(class669, -1956341595);
	    break;
	case 357:
	    Class189.method3760(class669, -1709472547);
	    break;
	case 1199:
	    Class207.method3940(class669, (byte) 2);
	    break;
	case 1073:
	    Class41.method1061(class669, -2144261722);
	    break;
	case 476:
	    Class706_Sub2.method17260(class669, -165023352);
	    break;
	case 262:
	    Class457.method7429(class669, 810139985);
	    break;
	case 388:
	    Class593.method9896(class669, -1107210791);
	    break;
	case 1205:
	    Class487.method7994(class669, 1348839374);
	    break;
	case 70:
	    Class97.method1830(class669, (byte) 8);
	    break;
	case 626:
	    Class534_Sub35.method16458(class669, (byte) -5);
	    break;
	case 1171:
	    Class285.method5263(class669, (byte) 0);
	    break;
	case 1086:
	    Class704.method14221(class669, -1657339689);
	    break;
	case 686:
	    Class63.method1277(class669, -1581216505);
	    break;
	case 832:
	    Class57.method1244(class669, 1976891991);
	    break;
	case 161:
	    Class22.method815(class669, (byte) 5);
	    break;
	case 529:
	    Class660.method10946(class669, 149950356);
	    break;
	case 438:
	    Class333.method5869(class669, (byte) -15);
	    break;
	case 1123:
	    Class504.method8323(class669, 785568531);
	    break;
	case 746:
	    Class60.method1259(class669, (byte) 89);
	    break;
	case 916:
	    Class319.method5760(class669, -2040258753);
	    break;
	case 988:
	    Class567.method9562(class669, 373906399);
	    break;
	case 945:
	    Class376.method6416(class669, (byte) 9);
	    break;
	case 67:
	    Class36.method938(class669, (byte) 114);
	    break;
	case 870:
	    Class299.method5515(class669, -993629849);
	    break;
	case 1196:
	    Class243.method4482(class669, (byte) -28);
	    break;
	case 790:
	    Class186.method3703(class669, (byte) -15);
	    break;
	case 1266:
	    Class551.method9048(class669, (byte) 41);
	    break;
	case 408:
	    Class290.method5285(class669, -1754001179);
	    break;
	case 167:
	    Class54.method1211(class669, (byte) 18);
	    break;
	case 966:
	    Class501.method8277(class669, -1175744987);
	    break;
	case 598:
	    Class44_Sub14.method17356(class669, -752231216);
	    break;
	case 143:
	    Class625.method10327(class669, (byte) 125);
	    break;
	case 56:
	    Class34.method927(class669, (byte) -65);
	    break;
	case 752:
	    Class63.method1276(class669, 969152601);
	    break;
	case 1241:
	    Class589.method9873(class669, (byte) -83);
	    break;
	case 425:
	    Class274.method5141(class669, 611491253);
	    break;
	case 579:
	    Class49.method1150(class669, -1039983391);
	    break;
	case 471:
	    Class237.method4422(class669, (byte) 0);
	    break;
	case 198:
	    Class114.method2107(class669, -163243029);
	    break;
	case 1218:
	    Class390.method6539(class669, -2117514169);
	    break;
	case 724:
	    Class534_Sub5.method16066(class669, 397045959);
	    break;
	case 2:
	    Class184.method3229(class669, -1400360447);
	    break;
	case 894:
	    Class534_Sub37.method16501(class669, 231975715);
	    break;
	case 1056:
	    Class587.method9865(class669, (byte) 30);
	    break;
	case 725:
	    Class528.method8819(class669, 476358815);
	    break;
	case 312:
	    Class624.method10310(class669, (byte) 26);
	    break;
	case 811:
	    Class42.method1065(class669, (byte) 0);
	    break;
	case 456:
	    Class83.method1652(class669, 1782451037);
	    break;
	case 873:
	    Class268.method4907(class669, 2146657585);
	    break;
	case 141:
	    Class37.method942(class669, 1432561717);
	    break;
	case 268:
	    Class279.method5226(class669, -983592604);
	    break;
	case 1015:
	    Class502.method8305(class669, (byte) 106);
	    break;
	case 542:
	    Class639.method10589(class669, -974087831);
	    break;
	case 1013:
	    Class28.method870(class669, -1526109181);
	    break;
	case 889:
	    Class394_Sub3.method15849(class669, (byte) 112);
	    break;
	case 533:
	    Class491.method8081(class669, 1541820282);
	    break;
	case 566:
	    Class71.method1414(class669, (byte) 1);
	    break;
	case 446:
	    Class650.method10721(class669, (byte) -19);
	    break;
	case 1181:
	    Class333.method5867(class669, 729810524);
	    break;
	case 241:
	    Class607.method10062(class669, -1337486755);
	    break;
	case 935:
	    Class598.method9942(class669, -383204796);
	    break;
	case 184:
	    Class529.method8821(class669, (short) 5920);
	    break;
	case 1037:
	    Class252.method4627(class669, 1597766522);
	    break;
	case 968:
	    Class106.method1947(class669, (byte) 34);
	    break;
	case 657:
	    Exception_Sub2.method17928(class669, (byte) 39);
	    break;
	case 700:
	    Class99.method1856(class669, 437884758);
	    break;
	case 88:
	    Class672.method11092(class669, 2082668032);
	    break;
	case 1032:
	    Class341.method5946(class669, -669781252);
	    break;
	case 317:
	    Class636.method10553(class669, 1988982006);
	    break;
	case 402:
	    Class261.method4818(class669, -1991257227);
	    break;
	case 235:
	    Class509.method8394(class669, -46163713);
	    break;
	case 121:
	    Class219.method4144(class669, 435417434);
	    break;
	case 240:
	    Class384.method6476(class669, -211661978);
	    break;
	case 301:
	    Class231.method4210(class669, -298673683);
	    break;
	case 362:
	    Class260.method4807(class669, -1305198429);
	    break;
	case 1254:
	    Class656.method10894(class669, (byte) -8);
	    break;
	case 959:
	    Class540.method8937(class669, -1359147665);
	    break;
	case 1093:
	    Class237.method4421(class669, -737029249);
	    break;
	case 1135:
	    Class648.method10703(class669, (byte) 82);
	    break;
	case 1053:
	    Class524.method8731(class669, -280817281);
	    break;
	case 1257:
	    method10540(class669, -1940457524);
	    break;
	case 1016:
	    Class429.method6808(class669, 835914838);
	    break;
	case 478:
	    Class110_Sub1_Sub1.method17864(class669, -1612000828);
	    break;
	case 112:
	    Class521.method8691(class669, 1588483897);
	    break;
	case 256:
	    Class450_Sub1.method15930(class669, -1140726636);
	    break;
	case 211:
	    Class503.method8307(class669, -1985843025);
	    break;
	case 756:
	    Class146.method2454(class669, -313553575);
	    break;
	case 936:
	    Class494.method8129(class669, 1894168001);
	    break;
	case 1050:
	    Class267.method4867(class669, 251477447);
	    break;
	case 878:
	    Class294.method5321(class669, (short) 255);
	    break;
	case 1166:
	    Class483.method7951(class669, 2074840783);
	    break;
	case 1150:
	    Class245.method4502(class669, 101547349);
	    break;
	case 294:
	    Class186.method3701(class669, 2090168189);
	    break;
	case 479:
	    Class643.method10678(class669, -888319977);
	    break;
	case 660:
	    Class51.method1178(class669, -15489536);
	    break;
	case 232:
	    Class593.method9898(class669, (byte) -97);
	    break;
	case 854:
	    Class33.method904(class669, -6823369);
	    break;
	case 1051:
	    Class591.method9882(class669, -1152925167);
	    break;
	case 1221:
	    Class65.method1356(class669, 1309201590);
	    break;
	case 693:
	    Class38.method973(class669, -1375416956);
	    break;
	case 1198:
	    Class81.method1638(class669, 758745918);
	    break;
	case 708:
	    Class117.method2127(class669, 1952500642);
	    break;
	case 118:
	    Class690_Sub21.method17073(class669, -238825126);
	    break;
	case 1105:
	    Class242.method4469(class669, 1346856688);
	    break;
	case 1250:
	    Class557.method9411(class669, 1555508355);
	    break;
	case 1273:
	    Class474.method7761(class669, 1423759592);
	    break;
	case 674:
	    Class536.method8906(class669, 658625911);
	    break;
	case 22:
	    Class297.method5348(class669, (byte) 21);
	    break;
	case 28:
	    Class383.method6459(class669, (short) -435);
	    break;
	case 769:
	    Class498.method8260(class669, 1765338066);
	    break;
	case 1128:
	    Class238.method4429(class669, -714702541);
	    break;
	case 604:
	    Class305.method5606(class669, (byte) -32);
	    break;
	case 472:
	    Class543.method8960(class669, (byte) -70);
	    break;
	case 41:
	    Class567.method9563(class669, 1939933136);
	    break;
	case 1140:
	    Class312_Sub3.method15676(class669, -135935594);
	    break;
	case 890:
	    Class661.method10975(class669, (byte) 0);
	    break;
	case 344:
	    Class200_Sub23.method15661(class669, (short) 16066);
	    break;
	case 689:
	    Class507.method8366(class669, (byte) -94);
	    break;
	case 1177:
	    client.method17836(class669, -2109666705);
	    break;
	case 1100:
	    Class580.method9815(class669, (byte) -16);
	    break;
	case 1246:
	    Class642.method10619(class669, 687856325);
	    break;
	case 127:
	    Class534_Sub18_Sub2.method17850(class669, (byte) 68);
	    break;
	case 510:
	    Class158.method2595(class669, 343766897);
	    break;
	case 996:
	    Class492.method8094(class669, (byte) 60);
	    break;
	case 39:
	    Class534_Sub20.method16193(class669, (byte) -113);
	    break;
	case 1133:
	    Class200.method3853(class669, -2094847057);
	    break;
	case 250:
	    Class614.method10125(class669, -1279903791);
	    break;
	case 523:
	    Class38.method975(class669, (byte) 0);
	    break;
	case 999:
	    Class389.method6530(class669, 1980495156);
	    break;
	case 992:
	    Class453_Sub2.method15972(class669, (byte) -28);
	    break;
	case 1265:
	    Class351.method6183(class669, (byte) 49);
	    break;
	case 656:
	    Class55.method1214(class669, -1833707988);
	    break;
	case 1263:
	    Class100.method1889(class669, -173712978);
	    break;
	case 561:
	    Class289.method5280(class669, 65535);
	    break;
	case 905:
	    Class58.method1250(class669, (byte) -47);
	    break;
	case 778:
	    Class260.method4809(class669, 613671380);
	    break;
	case 284:
	    Class327_Sub2.method15703(class669, (byte) 8);
	    break;
	case 847:
	    Class513.method8578(class669, (byte) 0);
	    break;
	case 859:
	    Class602.method10022(class669, -2136130038);
	    break;
	case 314:
	    Class4.method535(class669, -122713985);
	    break;
	case 1212:
	    Class193.method3796(class669, 1946135041);
	    break;
	case 337:
	    Class288.method5277(class669, -537050366);
	    break;
	case 807:
	    Class275.method5149(class669, -2130626221);
	    break;
	case 1077:
	    Class561.method9446(class669, 2104039303);
	    break;
	case 1232:
	    Class513.method8579(class669, -1145206757);
	    break;
	case 1159:
	    Class556.method9399(class669, 1246253456);
	    break;
	case 236:
	    Class569.method9594(class669, (byte) 1);
	    break;
	case 908:
	    Class184.method3221(class669, 2127653727);
	    break;
	case 289:
	    Class37.method941(class669, (byte) 0);
	    break;
	case 1102:
	    Class341.method5947(class669, (byte) 93);
	    break;
	case 803:
	    Class369.method6380(class669, -1805099104);
	    break;
	case 326:
	    Class522.method8713(class669, (short) 19063);
	    break;
	case 568:
	    Class166.method2750(class669, 2063436477);
	    break;
	case 1075:
	    Class415.method6743(class669, 1190929153);
	    break;
	case 1180:
	    Class534.method8894(class669, (byte) 0);
	    break;
	case 1209:
	    Class657.method10895(class669, -1476456587);
	    break;
	case 1115:
	    Class653.method10804(class669, (byte) 14);
	    break;
	case 839:
	    Class577.method9771(class669, -1797869129);
	    break;
	case 0:
	    Class20.method802(class669, (byte) 74);
	    break;
	case 539:
	    Class271.method5039(class669, -448241069);
	    break;
	case 906:
	    Class406.method6678(class669, 306347159);
	    break;
	case 551:
	    Class482.method7931(class669, (byte) -67);
	    break;
	case 923:
	    Class327.method5826(class669, (byte) 3);
	    break;
	case 1245:
	    Class254.method4641(class669, 1917531102);
	    break;
	case 1143:
	    Class245.method4500(class669, 1632119987);
	    break;
	case 371:
	    Class33.method902(class669, 1893551920);
	    break;
	case 1122:
	    Class19.method800(class669, 533666618);
	    break;
	case 704:
	    Class37.method940(class669, (byte) 3);
	    break;
	case 1118:
	    Class69.method1397(class669, (byte) 8);
	    break;
	case 188:
	    Class417.method6757(class669, (byte) 70);
	    break;
	case 615:
	    Class11.method614(class669, -176729267);
	    break;
	case 699:
	    Class530.method8853(class669, (byte) -124);
	    break;
	case 868:
	    Class401.method6592(class669, -1754063002);
	    break;
	case 1238:
	    Class581.method9826(class669, -1173784165);
	    break;
	case 176:
	    Class534_Sub32.method16391(class669, -1510194595);
	    break;
	case 1220:
	    Class330.method5854(class669, 1838218641);
	    break;
	case 49:
	    Class255.method4649(class669, 16777472);
	    break;
	case 509:
	    Class71_Sub1.method16306(class669, 2108964227);
	    break;
	case 624:
	    Class559.method9426(class669, -957038250);
	    break;
	case 632:
	    Class605.method10035(class669, 1273171322);
	    break;
	case 768:
	    Class28.method868(class669, 780208366);
	    break;
	case 555:
	    Class587.method9863(class669, 2023757382);
	    break;
	case 1225:
	    Class300.method5555(class669, -1843929329);
	    break;
	case 1154:
	    Class583.method9837(class669, (byte) 126);
	    break;
	case 1038:
	    Class101.method1902(class669, 1614128413);
	    break;
	case 1158:
	    Class440.method7098(class669, 287382820);
	    break;
	case 761:
	    Class254.method4642(class669, 1634126000);
	    break;
	case 895:
	    Class104.method1930(class669, 1387034206);
	    break;
	case 488:
	    Class479.method7917(class669, -999494825);
	    break;
	case 117:
	    Class706_Sub4.method17303(class669, 917713820);
	    break;
	case 91:
	    Class200_Sub12.method15587(class669, 9823353);
	    break;
	case 351:
	    Class478.method7802(class669, (short) -694);
	    break;
	case 1149:
	    Class252.method4628(class669, (byte) -104);
	    break;
	case 1:
	    Class614.method10123(class669, -1522951045);
	    break;
	case 290:
	    Class173.method2908(class669, (byte) -62);
	    break;
	case 1256:
	    Class553.method9106(class669, 1773154325);
	    break;
	case 299:
	    Class175_Sub1.method15088(class669, (byte) -38);
	    break;
	case 1247:
	    Class526.method8760(class669, (byte) -99);
	    break;
	case 243:
	    Class660.method10947(class669, (byte) 0);
	    break;
	case 592:
	    Class690_Sub7.method16945(class669, (byte) 114);
	    break;
	case 838:
	    Class14.method665(class669, -1966877743);
	    break;
	case 941:
	    Class54.method1210(class669, (byte) 81);
	    break;
	case 675:
	    Class677.method11143(class669, 1151318967);
	    break;
	case 25:
	    Class406.method6679(class669, 1868517860);
	    break;
	case 341:
	    Class401.method6591(class669, 1194702163);
	    break;
	case 81:
	    Class690_Sub27.method17127(class669, -1849305406);
	    break;
	case 795:
	    Class634.method10530(class669, (byte) 17);
	    break;
	case 858:
	    Class150.method2489(class669, (byte) 42);
	    break;
	case 837:
	    Class44.method1099(class669, (byte) -69);
	    break;
	case 163:
	    Class526.method8763(class669, (byte) -21);
	    break;
	case 717:
	    Class709.method14304(class669, -1110883229);
	    break;
	case 415:
	    Class707.method14259(class669, 1027308039);
	    break;
	case 263:
	    Class463.method7543(class669, (byte) 15);
	    break;
	case 730:
	    Class540.method8938(class669, (byte) -113);
	    break;
	case 1036:
	    Class97.method1831(class669, -2143612761);
	    break;
	case 774:
	    Class55.method1215(class669, (byte) 35);
	    break;
	case 547:
	    Class303.method5573(class669, -2045854647);
	    break;
	case 1096:
	    Class99.method1858(class669, 1894056297);
	    break;
	case 764:
	    Class312_Sub3.method15677(class669, -849091632);
	    break;
	case 1173:
	    Class100.method1888(class669, 1631270585);
	    break;
	case 742:
	    Class524.method8730(class669, 1748944533);
	    break;
	case 1134:
	    Class293.method5315(class669, -773482286);
	    break;
	case 658:
	    Class202.method3868(class669, 1357646500);
	    break;
	case 971:
	    Class221.method4149(class669, -1232435203);
	    break;
	case 1070:
	    Class573.method9641(class669, 1745719160);
	    break;
	case 202:
	    Class492.method8096(class669, (byte) -23);
	    break;
	case 131:
	    Class58.method1246(class669, -2099950826);
	    break;
	case 400:
	    Class200_Sub20.method15636(class669, -792286348);
	    break;
	case 318:
	    Class110_Sub1.method14507(class669, -2067433237);
	    break;
	case 741:
	    Class483.method7950(class669, 27049437);
	    break;
	case 639:
	    Class640.method10594(class669, 1306056438);
	    break;
	case 548:
	    Class534_Sub1.method16016(class669, 2119198504);
	    break;
	case 47:
	    Class398.method6579(class669, (byte) 2);
	    break;
	case 1132:
	    Class200_Sub9.method15583(class669, -181906079);
	}
    }
    
    public static void method10538(int i) {
	int i_1_ = 0;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680)
	    == 1) {
	    i_1_ |= 0x1;
	    i_1_ |= 0x10;
	    i_1_ |= 0x20;
	    i_1_ |= 0x2;
	    i_1_ |= 0x4;
	}
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -29)
	    == 0)
	    i_1_ |= 0x40;
	Class42.method1062(i_1_, 1094034484);
	client.aClass512_11100.method8428(-1486655428)
	    .method17349(i_1_, -1648304551);
	Class159.aClass509_1754.method8390((byte) -14).method8428
	    (-1486655428).method17349(i_1_, -1593891334);
	Class531.aClass44_Sub7_7135.method17309(i_1_, (byte) 37);
	Class578.aClass44_Sub3_7743.method17250(i_1_, (byte) -80);
	Class55.aClass44_Sub4_447.method17263(i_1_, 674157543);
	Class484.method7959(i_1_, -2066017421);
	Class72.method1561(i_1_, 221777407);
	Class407.method6689(i_1_, (short) 8682);
	Class273.method5098(i_1_, (byte) 1);
	Class660.method10950(i_1_, -130124997);
	client.aClass512_11100.method8441(19286677);
    }
    
    static String method10539(Class534_Sub18_Sub11 class534_sub18_sub11,
			      int i) {
	return new StringBuilder().append
		   (class534_sub18_sub11.aString11793).append
		   (Class154.method2575(16777215, 1582720997)).append
		   (" >").toString();
    }
    
    static final void method10540(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class16 class16
	    = Class351.aClass406_3620.method6666(client.anInterface52_11081,
						 i_2_, (byte) 20);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class16.method748(string, Class658.aClass163Array8541,
				(byte) 22);
    }
    
    static final void method10541(int i, byte i_3_) {
	int i_4_ = client.anInt11101 - 712767803 * client.anInt11303;
	if (i_4_ >= 100) {
	    Class10.anInt75
		= Class200_Sub5.method15573((byte) 66) * -1822037319;
	    Class106.anInt1312 = 309821991;
	    Class93.anInt901 = 1899572639;
	} else {
	    float f = 1.0F - ((float) ((100 - i_4_)
				       * ((100 - i_4_) * (100 - i_4_)))
			      / 1000000.0F);
	    int i_5_;
	    if (Class200_Sub5.method15573((byte) -14) == 3) {
		Class534_Sub36 class534_sub36
		    = Class599.aClass298_Sub1_7871.method5380((byte) -100)
			  .method6133((byte) -33);
		Class597 class597
		    = client.aClass512_11100.method8416((byte) 76);
		Class566.anInt7589
		    = ((int) ((double) Class599.aClass298_Sub1_7871
					   .method5399(281790951)
			      * 2607.5945876176133)
		       & 0x3fff) * -1539495063;
		Class641.anInt8341
		    = ((int) ((double) Class599.aClass298_Sub1_7871
					   .method5389(-963898005)
			      * -2607.5945876176133)
		       & 0x3fff) * 486346273;
		Class2.anInt22 = 0;
		client.anInt11323
		    = (int) ((float) (-272423665 * Class26.anInt248)
			     + (float) ((int) ((double) (-610622155
							 * client.anInt11234)
					       / (4.0
						  * (Math.tan
						     ((double) ((Class599
								     .aClass298_Sub1_7871
								     .method5480
								 ((byte) 0))
								/ 2.0F)))))
					- (-272423665
					   * (Class26
					      .anInt248))) * f) * 574465361;
		Class200_Sub13.anInt9937
		    = ((int) ((float) (599803149 * class534_sub36.anInt10797
				       - class597.anInt7859 * 1852947968
				       - Class559.anInt7501 * 962345669) * f
			      + (float) (962345669 * Class559.anInt7501))
		       * -2079086379);
		Class677.anInt8654
		    = ((int) ((float) (-(class534_sub36.anInt10798
					 * -1203728391)
				       - Class18.anInt208 * 341531539) * f
			      + (float) (341531539 * Class18.anInt208))
		       * 996845503);
		Class636.anInt8305
		    = (int) ((float) (-703095931 * Class226.anInt2318)
			     + (f
				* (float) (33298755 * class534_sub36.anInt10799
					   - -139729408 * class597.anInt7861
					   - (Class226.anInt2318
					      * -703095931)))) * -866237299;
		i_5_ = (-(Class641.anInt8341 * -890112543)
			- 904023967 * Class78.anInt827) & 0x3fff;
		if (i_5_ > 8192)
		    i_5_ -= 16384;
		else if (i_5_ < -8192)
		    i_5_ += 16384;
	    } else {
		int i_6_ = (int) client.aFloat11302;
		if (544044433 * client.anInt11019 >> 8 > i_6_)
		    i_6_ = client.anInt11019 * 544044433 >> 8;
		if (client.aBoolArray11172[4]
		    && client.anIntArray11308[4] + 128 > i_6_)
		    i_6_ = 128 + client.anIntArray11308[4];
		int i_7_
		    = (-61292849 * client.anInt11127 + (int) client.aFloat11140
		       & 0x3fff);
		Class438 class438
		    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method10807
		       ().aClass438_4885);
		Class678.method11152
		    (Class588.anInt7808 * 1490134939,
		     (Class247.method4595((int) class438.aFloat4864,
					  (int) class438.aFloat4865,
					  Class674.anInt8633 * -878424575,
					  205929643)
		      - -231820249 * client.anInt11146),
		     -1293868227 * Class453.anInt4958, i_6_, i_7_,
		     600 + (i_6_ >> 3) * 3 << 2, i, 2086572687);
		Class200_Sub13.anInt9937
		    = (int) (f * (float) (-116109187 * Class200_Sub13.anInt9937
					  - Class559.anInt7501 * 962345669)
			     + (float) (Class559.anInt7501
					* 962345669)) * -2079086379;
		Class677.anInt8654
		    = ((int) ((float) (Class677.anInt8654 * 1529694271
				       - 341531539 * Class18.anInt208) * f
			      + (float) (341531539 * Class18.anInt208))
		       * 996845503);
		Class636.anInt8305
		    = (-866237299
		       * (int) ((float) (Class226.anInt2318 * -703095931)
				+ f * (float) (Class636.anInt8305 * -1098179003
					       - (Class226.anInt2318
						  * -703095931))));
		Class566.anInt7589
		    = (-1539495063
		       * (int) ((float) (Class566.anInt7589 * -864938791
					 - -927757145 * Class479.anInt5250) * f
				+ (float) (Class479.anInt5250 * -927757145)));
		i_5_ = (-890112543 * Class641.anInt8341
			- 904023967 * Class78.anInt827);
		if (i_5_ > 8192)
		    i_5_ -= 16384;
		else if (i_5_ < -8192)
		    i_5_ += 16384;
	    }
	    Class641.anInt8341
		= 486346273 * (int) ((float) (904023967 * Class78.anInt827)
				     + f * (float) i_5_);
	    Class641.anInt8341
		= 486346273 * (Class641.anInt8341 * -890112543 & 0x3fff);
	    client.anInt11323
		= (574465361
		   * (int) (f * (float) (-1439382607 * client.anInt11323
					 - Class26.anInt248 * -272423665)
			    + (float) (Class26.anInt248 * -272423665)));
	}
    }
}
