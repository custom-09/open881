/* Class651 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class651 implements Interface13, Interface7
{
    public int anInt8465;
    public int anInt8466;
    public int anInt8467 = 0;
    public int anInt8468 = 1122033033;
    public boolean aBool8469 = true;
    public int anInt8470 = 1553306891;
    public int anInt8471 = -385271296;
    public boolean aBool8472 = true;
    public int anInt8473 = 729422904;
    public boolean aBool8474 = false;
    public int anInt8475 = -1648355787;
    public int anInt8476 = -836957504;
    static final int anInt8477 = 1190717;
    int anInt8478;
    public int anInt8479;
    public int anInt8480;
    
    static int method10725(int i) {
	if (16711935 == i)
	    return -1;
	return Class466.method7583(i, 1447452631);
    }
    
    public void method66(int i) {
	anInt8478 = i * -2118077015;
    }
    
    void method10726(Class534_Sub40 class534_sub40, int i, int i_0_) {
	if (1 == i)
	    anInt8467
		= Class521.method8687(class534_sub40.method16531(1559109180),
				      (byte) 43) * -1446740851;
	else if (i == 2)
	    anInt8468 = class534_sub40.method16527(2092357153) * -1122033033;
	else if (3 == i) {
	    anInt8468 = class534_sub40.method16529((byte) 1) * -1122033033;
	    if (1884378951 * anInt8468 == 65535)
		anInt8468 = 1122033033;
	} else if (5 == i)
	    aBool8469 = false;
	else if (7 == i)
	    anInt8470
		= Class521.method8687(class534_sub40.method16531(751166677),
				      (byte) 40) * -1553306891;
	else if (8 != i) {
	    if (i == 9)
		anInt8471 = ((class534_sub40.method16529((byte) 1) << 2)
			     * -1418427235);
	    else if (i == 10)
		aBool8472 = false;
	    else if (11 == i)
		anInt8473
		    = class534_sub40.method16527(-1245227043) * -1519434873;
	    else if (i == 12)
		aBool8474 = true;
	    else if (13 == i)
		anInt8475 = class534_sub40.method16531(347666520) * -648796583;
	    else if (14 == i)
		anInt8476
		    = (class534_sub40.method16527(-335021608) << 2) * 54031403;
	    else if (16 == i)
		anInt8466
		    = class534_sub40.method16527(-188441965) * 1265937407;
	    else if (20 == i)
		anInt8465 = class534_sub40.method16529((byte) 1) * 1409815235;
	    else if (i == 21)
		anInt8479 = class534_sub40.method16527(259203237) * -701138217;
	    else if (i == 22)
		anInt8480 = class534_sub40.method16529((byte) 1) * 1949890743;
	}
    }
    
    public void method82(int i) {
	anInt8473 = -1519434873 * (anInt8473 * 589238839 << 8
				   | anInt8478 * 620805785);
    }
    
    public void method67(int i, int i_1_) {
	anInt8478 = i * -2118077015;
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1381362643);
	    if (0 == i)
		break;
	    method10726(class534_sub40, i, -648702397);
	}
    }
    
    public void method65(int i) {
	anInt8478 = i * -2118077015;
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1869132105);
	    if (0 == i)
		break;
	    method10726(class534_sub40, i, 1237542785);
	}
    }
    
    public void method83() {
	anInt8473 = -1519434873 * (anInt8473 * 589238839 << 8
				   | anInt8478 * 620805785);
    }
    
    public void method84() {
	anInt8473 = -1519434873 * (anInt8473 * 589238839 << 8
				   | anInt8478 * 620805785);
    }
    
    Class651() {
	anInt8466 = 691491585;
	anInt8465 = -1375953411;
	anInt8479 = 0;
	anInt8480 = 238955968;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_2_ = class534_sub40.method16527(-1448504428);
	    if (0 == i_2_)
		break;
	    method10726(class534_sub40, i_2_, 242109209);
	}
    }
    
    void method10727(Class534_Sub40 class534_sub40, int i) {
	if (1 == i)
	    anInt8467
		= Class521.method8687(class534_sub40.method16531(1203642468),
				      (byte) 18) * -1446740851;
	else if (i == 2)
	    anInt8468 = class534_sub40.method16527(-1889766638) * -1122033033;
	else if (3 == i) {
	    anInt8468 = class534_sub40.method16529((byte) 1) * -1122033033;
	    if (1884378951 * anInt8468 == 65535)
		anInt8468 = 1122033033;
	} else if (5 == i)
	    aBool8469 = false;
	else if (7 == i)
	    anInt8470
		= Class521.method8687(class534_sub40.method16531(72826798),
				      (byte) 44) * -1553306891;
	else if (8 != i) {
	    if (i == 9)
		anInt8471 = ((class534_sub40.method16529((byte) 1) << 2)
			     * -1418427235);
	    else if (i == 10)
		aBool8472 = false;
	    else if (11 == i)
		anInt8473
		    = class534_sub40.method16527(1890274343) * -1519434873;
	    else if (i == 12)
		aBool8474 = true;
	    else if (13 == i)
		anInt8475
		    = class534_sub40.method16531(1444130817) * -648796583;
	    else if (14 == i)
		anInt8476
		    = (class534_sub40.method16527(582357298) << 2) * 54031403;
	    else if (16 == i)
		anInt8466
		    = class534_sub40.method16527(1493590694) * 1265937407;
	    else if (20 == i)
		anInt8465 = class534_sub40.method16529((byte) 1) * 1409815235;
	    else if (i == 21)
		anInt8479
		    = class534_sub40.method16527(1529506572) * -701138217;
	    else if (i == 22)
		anInt8480 = class534_sub40.method16529((byte) 1) * 1949890743;
	}
    }
    
    void method10728(Class534_Sub40 class534_sub40, int i) {
	if (1 == i)
	    anInt8467
		= Class521.method8687(class534_sub40.method16531(1929006409),
				      (byte) 100) * -1446740851;
	else if (i == 2)
	    anInt8468 = class534_sub40.method16527(1185915684) * -1122033033;
	else if (3 == i) {
	    anInt8468 = class534_sub40.method16529((byte) 1) * -1122033033;
	    if (1884378951 * anInt8468 == 65535)
		anInt8468 = 1122033033;
	} else if (5 == i)
	    aBool8469 = false;
	else if (7 == i)
	    anInt8470
		= Class521.method8687(class534_sub40.method16531(122861233),
				      (byte) 64) * -1553306891;
	else if (8 != i) {
	    if (i == 9)
		anInt8471 = ((class534_sub40.method16529((byte) 1) << 2)
			     * -1418427235);
	    else if (i == 10)
		aBool8472 = false;
	    else if (11 == i)
		anInt8473
		    = class534_sub40.method16527(-204022906) * -1519434873;
	    else if (i == 12)
		aBool8474 = true;
	    else if (13 == i)
		anInt8475 = class534_sub40.method16531(912650925) * -648796583;
	    else if (14 == i)
		anInt8476
		    = (class534_sub40.method16527(1887193808) << 2) * 54031403;
	    else if (16 == i)
		anInt8466
		    = class534_sub40.method16527(-435993461) * 1265937407;
	    else if (20 == i)
		anInt8465 = class534_sub40.method16529((byte) 1) * 1409815235;
	    else if (i == 21)
		anInt8479
		    = class534_sub40.method16527(1540958692) * -701138217;
	    else if (i == 22)
		anInt8480 = class534_sub40.method16529((byte) 1) * 1949890743;
	}
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-290027908);
	    if (0 == i)
		break;
	    method10726(class534_sub40, i, 633767966);
	}
    }
    
    static int method10729(int i) {
	if (16711935 == i)
	    return -1;
	return Class466.method7583(i, 1447452631);
    }
    
    static final void method10730(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class213.method4021(968273173).method93();
    }
    
    static final void method10731(Class669 class669, int i) {
	class669.anInt8594 -= 1460193483;
    }
    
    static void method10732(int i, int i_3_, int i_4_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(910664110);
	class534_sub18_sub6.anInt11666 = i_3_ * 517206857;
    }
}
