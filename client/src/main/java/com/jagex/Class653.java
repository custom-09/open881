/* Class653 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class653
{
    float aFloat8485;
    float aFloat8486;
    int[] anIntArray8487 = new int[3];
    int anInt8488;
    Class438 aClass438_8489;
    int anInt8490;
    int anInt8491;
    float aFloat8492;
    float aFloat8493;
    int anInt8494;
    int anInt8495;
    int anInt8496;
    int anInt8497;
    float aFloat8498;
    int anInt8499;
    int anInt8500;
    float aFloat8501;
    float[] aFloatArray8502 = new float[3];
    int anInt8503;
    
    public float method10739() {
	return aFloat8486;
    }
    
    public boolean method10740(Class534_Sub40 class534_sub40, int i) {
	int i_0_ = class534_sub40.method16529((byte) 1);
	if (0 == i_0_) {
	    anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	    return false;
	}
	if (0 != (i_0_ & Class644.aClass644_8353.anInt8354 * 522594405))
	    anInt8494 = class534_sub40.method16533(-258848859) * -1689948605;
	if (0 != (i_0_ & Class644.aClass644_8349.anInt8354 * 522594405))
	    aFloat8486 = class534_sub40.method16539(-1055953289);
	if ((i_0_ & 522594405 * Class644.aClass644_8350.anInt8354) != 0)
	    aFloat8498 = class534_sub40.method16539(-1952466507);
	if (0 != (i_0_ & Class644.aClass644_8351.anInt8354 * 522594405))
	    aFloat8485 = class534_sub40.method16539(-1393139046);
	if ((i_0_ & Class644.aClass644_8352.anInt8354 * 522594405) != 0)
	    aClass438_8489 = Class438.method7025(class534_sub40);
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8490 = class534_sub40.method16533(-258848859) * -673554397;
	if (0 != (i_0_ & Class644.aClass644_8359.anInt8354 * 522594405))
	    anInt8491 = class534_sub40.method16529((byte) 1) * 1162994453;
	if ((i_0_ & Class644.aClass644_8355.anInt8354 * 522594405) != 0)
	    anInt8488 = class534_sub40.method16529((byte) 1) * -1175870517;
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8356.anInt8354))
	    aFloat8492 = class534_sub40.method16539(-1647045117);
	if ((i_0_ & Class644.aClass644_8357.anInt8354 * 522594405) != 0)
	    aFloat8493 = class534_sub40.method16539(-1538685848);
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8369.anInt8354))
	    aFloat8501 = class534_sub40.method16539(-1471038875);
	if ((i_0_ & Class644.aClass644_8366.anInt8354 * 522594405) != 0) {
	    anInt8495 = class534_sub40.method16529((byte) 1) * 1149517419;
	    anInt8496 = class534_sub40.method16530((byte) -115) * -669073583;
	    anInt8497 = class534_sub40.method16530((byte) -45) * -1754948975;
	    anInt8499 = class534_sub40.method16530((byte) -24) * 758504603;
	    anInt8500 = class534_sub40.method16529((byte) 1) * -1063533015;
	}
	if (0 != (i_0_ & Class644.aClass644_8372.anInt8354 * 522594405)) {
	    anIntArray8487[0] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[0] = class534_sub40.method16539(-1422702050);
	}
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8361.anInt8354)) {
	    anIntArray8487[1] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[1] = class534_sub40.method16539(-1772976625);
	}
	if ((i_0_ & Class644.aClass644_8373.anInt8354 * 522594405) != 0) {
	    anIntArray8487[2] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[2] = class534_sub40.method16539(-1698120743);
	}
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8363.anInt8354))
	    class534_sub40.method16539(-1161472052);
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8364.anInt8354))
	    class534_sub40.method16539(-1390992703);
	if ((i_0_ & Class644.aClass644_8365.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1298943944);
	if (0 != (i_0_ & Class644.aClass644_8371.anInt8354 * 522594405))
	    class534_sub40.method16539(-1774618842);
	if (0 != (i_0_ & Class644.aClass644_8367.anInt8354 * 522594405))
	    class534_sub40.method16539(-1988933877);
	if (0 != (i_0_ & Class644.aClass644_8348.anInt8354 * 522594405))
	    class534_sub40.method16539(-1709093753);
	if ((i_0_ & Class644.aClass644_8368.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1660607862);
	if ((i_0_ & 522594405 * Class644.aClass644_8370.anInt8354) != 0)
	    class534_sub40.method16539(-1840771245);
	if (0 != (i_0_ & Class644.aClass644_8358.anInt8354 * 522594405))
	    class534_sub40.method16533(-258848859);
	if (0 != (i_0_ & 522594405 * Class644.aClass644_8362.anInt8354))
	    class534_sub40.method16533(-258848859);
	anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	return true;
    }
    
    public int method10741() {
	return 304860779 * anInt8494;
    }
    
    public int method10742(int i) {
	return 304860779 * anInt8494;
    }
    
    public boolean method10743(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16529((byte) 1);
	if (0 == i) {
	    anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	    return false;
	}
	if (0 != (i & Class644.aClass644_8353.anInt8354 * 522594405))
	    anInt8494 = class534_sub40.method16533(-258848859) * -1689948605;
	if (0 != (i & Class644.aClass644_8349.anInt8354 * 522594405))
	    aFloat8486 = class534_sub40.method16539(-1358242097);
	if ((i & 522594405 * Class644.aClass644_8350.anInt8354) != 0)
	    aFloat8498 = class534_sub40.method16539(-1417346761);
	if (0 != (i & Class644.aClass644_8351.anInt8354 * 522594405))
	    aFloat8485 = class534_sub40.method16539(-2007145054);
	if ((i & Class644.aClass644_8352.anInt8354 * 522594405) != 0)
	    aClass438_8489 = Class438.method7025(class534_sub40);
	if (0 != (i & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8490 = class534_sub40.method16533(-258848859) * -673554397;
	if (0 != (i & Class644.aClass644_8359.anInt8354 * 522594405))
	    anInt8491 = class534_sub40.method16529((byte) 1) * 1162994453;
	if ((i & Class644.aClass644_8355.anInt8354 * 522594405) != 0)
	    anInt8488 = class534_sub40.method16529((byte) 1) * -1175870517;
	if (0 != (i & 522594405 * Class644.aClass644_8356.anInt8354))
	    aFloat8492 = class534_sub40.method16539(-1661538244);
	if ((i & Class644.aClass644_8357.anInt8354 * 522594405) != 0)
	    aFloat8493 = class534_sub40.method16539(-1966443187);
	if (0 != (i & 522594405 * Class644.aClass644_8369.anInt8354))
	    aFloat8501 = class534_sub40.method16539(-978046355);
	if ((i & Class644.aClass644_8366.anInt8354 * 522594405) != 0) {
	    anInt8495 = class534_sub40.method16529((byte) 1) * 1149517419;
	    anInt8496 = class534_sub40.method16530((byte) -33) * -669073583;
	    anInt8497 = class534_sub40.method16530((byte) -119) * -1754948975;
	    anInt8499 = class534_sub40.method16530((byte) -78) * 758504603;
	    anInt8500 = class534_sub40.method16529((byte) 1) * -1063533015;
	}
	if (0 != (i & Class644.aClass644_8372.anInt8354 * 522594405)) {
	    anIntArray8487[0] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[0] = class534_sub40.method16539(-2017025726);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8361.anInt8354)) {
	    anIntArray8487[1] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[1] = class534_sub40.method16539(-1738954895);
	}
	if ((i & Class644.aClass644_8373.anInt8354 * 522594405) != 0) {
	    anIntArray8487[2] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[2] = class534_sub40.method16539(-1859283327);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8363.anInt8354))
	    class534_sub40.method16539(-2102951397);
	if (0 != (i & 522594405 * Class644.aClass644_8364.anInt8354))
	    class534_sub40.method16539(-1208110024);
	if ((i & Class644.aClass644_8365.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1835808833);
	if (0 != (i & Class644.aClass644_8371.anInt8354 * 522594405))
	    class534_sub40.method16539(-2074542081);
	if (0 != (i & Class644.aClass644_8367.anInt8354 * 522594405))
	    class534_sub40.method16539(-1584342440);
	if (0 != (i & Class644.aClass644_8348.anInt8354 * 522594405))
	    class534_sub40.method16539(-1437729048);
	if ((i & Class644.aClass644_8368.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1626731351);
	if ((i & 522594405 * Class644.aClass644_8370.anInt8354) != 0)
	    class534_sub40.method16539(-1866700310);
	if (0 != (i & Class644.aClass644_8358.anInt8354 * 522594405))
	    class534_sub40.method16533(-258848859);
	if (0 != (i & 522594405 * Class644.aClass644_8362.anInt8354))
	    class534_sub40.method16533(-258848859);
	anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	return true;
    }
    
    public float method10744(short i) {
	return aFloat8498;
    }
    
    public Class438 method10745(byte i) {
	return aClass438_8489;
    }
    
    public float method10746() {
	return aFloat8486;
    }
    
    public int method10747(byte i) {
	return 249646987 * anInt8490;
    }
    
    public int method10748(byte i) {
	return 1871860797 * anInt8491;
    }
    
    public float method10749() {
	return aFloat8492;
    }
    
    public float method10750(int i) {
	return aFloat8493;
    }
    
    public float method10751() {
	return aFloat8493;
    }
    
    public int method10752(byte i) {
	return 121694691 * anInt8488;
    }
    
    public int method10753(byte i) {
	return -993952189 * anInt8495;
    }
    
    public int method10754(int i) {
	return anInt8496 * 126740913;
    }
    
    public float method10755(int i, int i_1_) {
	return aFloatArray8502[i];
    }
    
    public int method10756(byte i) {
	return anInt8499 * -783242861;
    }
    
    public int method10757(int i) {
	return anInt8500 * -1430964711;
    }
    
    public int method10758(int i, int i_2_) {
	return anIntArray8487[i];
    }
    
    public int method10759(int i) {
	return -1241422223 * anInt8497;
    }
    
    public int method10760(byte i) {
	return anInt8503 * 1777623545;
    }
    
    void method10761() {
	anInt8494 = 1689948605;
	aFloat8486 = -1.0F;
	aFloat8498 = -1.0F;
	aFloat8485 = -1.0F;
	aClass438_8489 = null;
	anInt8490 = 673554397;
	anInt8491 = -1162994453;
	aFloat8492 = -1.0F;
	aFloat8493 = -1.0F;
	aFloat8501 = -1.0F;
	anInt8488 = 1175870517;
	anInt8495 = -1149517419;
	anInt8496 = 669073583;
	anInt8497 = 1754948975;
	anInt8499 = -758504603;
	anInt8500 = 1063533015;
	for (int i = 0; i < anIntArray8487.length; i++) {
	    anIntArray8487[i] = -1;
	    aFloatArray8502[i] = 0.0F;
	}
	anInt8503 = -1214115384;
    }
    
    public boolean method10762(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16529((byte) 1);
	if (0 == i) {
	    anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	    return false;
	}
	if (0 != (i & Class644.aClass644_8353.anInt8354 * 522594405))
	    anInt8494 = class534_sub40.method16533(-258848859) * -1689948605;
	if (0 != (i & Class644.aClass644_8349.anInt8354 * 522594405))
	    aFloat8486 = class534_sub40.method16539(-1616675382);
	if ((i & 522594405 * Class644.aClass644_8350.anInt8354) != 0)
	    aFloat8498 = class534_sub40.method16539(-2144516734);
	if (0 != (i & Class644.aClass644_8351.anInt8354 * 522594405))
	    aFloat8485 = class534_sub40.method16539(-1959759546);
	if ((i & Class644.aClass644_8352.anInt8354 * 522594405) != 0)
	    aClass438_8489 = Class438.method7025(class534_sub40);
	if (0 != (i & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8490 = class534_sub40.method16533(-258848859) * -673554397;
	if (0 != (i & Class644.aClass644_8359.anInt8354 * 522594405))
	    anInt8491 = class534_sub40.method16529((byte) 1) * 1162994453;
	if ((i & Class644.aClass644_8355.anInt8354 * 522594405) != 0)
	    anInt8488 = class534_sub40.method16529((byte) 1) * -1175870517;
	if (0 != (i & 522594405 * Class644.aClass644_8356.anInt8354))
	    aFloat8492 = class534_sub40.method16539(-2134470873);
	if ((i & Class644.aClass644_8357.anInt8354 * 522594405) != 0)
	    aFloat8493 = class534_sub40.method16539(-1829517775);
	if (0 != (i & 522594405 * Class644.aClass644_8369.anInt8354))
	    aFloat8501 = class534_sub40.method16539(-1999721020);
	if ((i & Class644.aClass644_8366.anInt8354 * 522594405) != 0) {
	    anInt8495 = class534_sub40.method16529((byte) 1) * 1149517419;
	    anInt8496 = class534_sub40.method16530((byte) -15) * -669073583;
	    anInt8497 = class534_sub40.method16530((byte) -46) * -1754948975;
	    anInt8499 = class534_sub40.method16530((byte) -46) * 758504603;
	    anInt8500 = class534_sub40.method16529((byte) 1) * -1063533015;
	}
	if (0 != (i & Class644.aClass644_8372.anInt8354 * 522594405)) {
	    anIntArray8487[0] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[0] = class534_sub40.method16539(-1695858297);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8361.anInt8354)) {
	    anIntArray8487[1] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[1] = class534_sub40.method16539(-2079520579);
	}
	if ((i & Class644.aClass644_8373.anInt8354 * 522594405) != 0) {
	    anIntArray8487[2] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[2] = class534_sub40.method16539(-1726850609);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8363.anInt8354))
	    class534_sub40.method16539(-1303484636);
	if (0 != (i & 522594405 * Class644.aClass644_8364.anInt8354))
	    class534_sub40.method16539(-1870178764);
	if ((i & Class644.aClass644_8365.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1367281852);
	if (0 != (i & Class644.aClass644_8371.anInt8354 * 522594405))
	    class534_sub40.method16539(-1807153393);
	if (0 != (i & Class644.aClass644_8367.anInt8354 * 522594405))
	    class534_sub40.method16539(-2009335532);
	if (0 != (i & Class644.aClass644_8348.anInt8354 * 522594405))
	    class534_sub40.method16539(-1369413474);
	if ((i & Class644.aClass644_8368.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1345971934);
	if ((i & 522594405 * Class644.aClass644_8370.anInt8354) != 0)
	    class534_sub40.method16539(-1785539310);
	if (0 != (i & Class644.aClass644_8358.anInt8354 * 522594405))
	    class534_sub40.method16533(-258848859);
	if (0 != (i & 522594405 * Class644.aClass644_8362.anInt8354))
	    class534_sub40.method16533(-258848859);
	anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	return true;
    }
    
    public int method10763() {
	return anInt8499 * -783242861;
    }
    
    public boolean method10764(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16529((byte) 1);
	if (0 == i) {
	    anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	    return false;
	}
	if (0 != (i & Class644.aClass644_8353.anInt8354 * 522594405))
	    anInt8494 = class534_sub40.method16533(-258848859) * -1689948605;
	if (0 != (i & Class644.aClass644_8349.anInt8354 * 522594405))
	    aFloat8486 = class534_sub40.method16539(-1613771771);
	if ((i & 522594405 * Class644.aClass644_8350.anInt8354) != 0)
	    aFloat8498 = class534_sub40.method16539(-1558220141);
	if (0 != (i & Class644.aClass644_8351.anInt8354 * 522594405))
	    aFloat8485 = class534_sub40.method16539(-1755906620);
	if ((i & Class644.aClass644_8352.anInt8354 * 522594405) != 0)
	    aClass438_8489 = Class438.method7025(class534_sub40);
	if (0 != (i & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8490 = class534_sub40.method16533(-258848859) * -673554397;
	if (0 != (i & Class644.aClass644_8359.anInt8354 * 522594405))
	    anInt8491 = class534_sub40.method16529((byte) 1) * 1162994453;
	if ((i & Class644.aClass644_8355.anInt8354 * 522594405) != 0)
	    anInt8488 = class534_sub40.method16529((byte) 1) * -1175870517;
	if (0 != (i & 522594405 * Class644.aClass644_8356.anInt8354))
	    aFloat8492 = class534_sub40.method16539(-2041382864);
	if ((i & Class644.aClass644_8357.anInt8354 * 522594405) != 0)
	    aFloat8493 = class534_sub40.method16539(-1490328151);
	if (0 != (i & 522594405 * Class644.aClass644_8369.anInt8354))
	    aFloat8501 = class534_sub40.method16539(-1903923579);
	if ((i & Class644.aClass644_8366.anInt8354 * 522594405) != 0) {
	    anInt8495 = class534_sub40.method16529((byte) 1) * 1149517419;
	    anInt8496 = class534_sub40.method16530((byte) -34) * -669073583;
	    anInt8497 = class534_sub40.method16530((byte) -59) * -1754948975;
	    anInt8499 = class534_sub40.method16530((byte) -49) * 758504603;
	    anInt8500 = class534_sub40.method16529((byte) 1) * -1063533015;
	}
	if (0 != (i & Class644.aClass644_8372.anInt8354 * 522594405)) {
	    anIntArray8487[0] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[0] = class534_sub40.method16539(-1399407939);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8361.anInt8354)) {
	    anIntArray8487[1] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[1] = class534_sub40.method16539(-1941288217);
	}
	if ((i & Class644.aClass644_8373.anInt8354 * 522594405) != 0) {
	    anIntArray8487[2] = class534_sub40.method16529((byte) 1);
	    aFloatArray8502[2] = class534_sub40.method16539(-1925744732);
	}
	if (0 != (i & 522594405 * Class644.aClass644_8363.anInt8354))
	    class534_sub40.method16539(-1395376429);
	if (0 != (i & 522594405 * Class644.aClass644_8364.anInt8354))
	    class534_sub40.method16539(-1171241162);
	if ((i & Class644.aClass644_8365.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1021032788);
	if (0 != (i & Class644.aClass644_8371.anInt8354 * 522594405))
	    class534_sub40.method16539(-1889963662);
	if (0 != (i & Class644.aClass644_8367.anInt8354 * 522594405))
	    class534_sub40.method16539(-1422410163);
	if (0 != (i & Class644.aClass644_8348.anInt8354 * 522594405))
	    class534_sub40.method16539(-1229741368);
	if ((i & Class644.aClass644_8368.anInt8354 * 522594405) != 0)
	    class534_sub40.method16539(-1755996617);
	if ((i & 522594405 * Class644.aClass644_8370.anInt8354) != 0)
	    class534_sub40.method16539(-1181121662);
	if (0 != (i & Class644.aClass644_8358.anInt8354 * 522594405))
	    class534_sub40.method16533(-258848859);
	if (0 != (i & 522594405 * Class644.aClass644_8362.anInt8354))
	    class534_sub40.method16533(-258848859);
	anInt8503 = class534_sub40.method16529((byte) 1) * -1328246711;
	return true;
    }
    
    public int method10765() {
	return 304860779 * anInt8494;
    }
    
    public int method10766() {
	return 304860779 * anInt8494;
    }
    
    public int method10767() {
	return 304860779 * anInt8494;
    }
    
    public float method10768(int i) {
	return aFloatArray8502[i];
    }
    
    public float method10769() {
	return aFloat8493;
    }
    
    public float method10770(int i) {
	return aFloat8501;
    }
    
    public float method10771() {
	return aFloat8486;
    }
    
    public float method10772() {
	return aFloat8498;
    }
    
    public float method10773() {
	return aFloat8498;
    }
    
    public int method10774(int i) {
	return anIntArray8487[i];
    }
    
    public Class438 method10775() {
	return aClass438_8489;
    }
    
    public int method10776() {
	return 249646987 * anInt8490;
    }
    
    public int method10777() {
	return 249646987 * anInt8490;
    }
    
    public float method10778(int i) {
	return aFloatArray8502[i];
    }
    
    public int method10779() {
	return 1871860797 * anInt8491;
    }
    
    public float method10780() {
	return aFloat8492;
    }
    
    public float method10781(int i) {
	return aFloat8492;
    }
    
    public float method10782() {
	return aFloat8493;
    }
    
    void method10783(int i) {
	anInt8494 = 1689948605;
	aFloat8486 = -1.0F;
	aFloat8498 = -1.0F;
	aFloat8485 = -1.0F;
	aClass438_8489 = null;
	anInt8490 = 673554397;
	anInt8491 = -1162994453;
	aFloat8492 = -1.0F;
	aFloat8493 = -1.0F;
	aFloat8501 = -1.0F;
	anInt8488 = 1175870517;
	anInt8495 = -1149517419;
	anInt8496 = 669073583;
	anInt8497 = 1754948975;
	anInt8499 = -758504603;
	anInt8500 = 1063533015;
	for (int i_3_ = 0; i_3_ < anIntArray8487.length; i_3_++) {
	    anIntArray8487[i_3_] = -1;
	    aFloatArray8502[i_3_] = 0.0F;
	}
	anInt8503 = -1214115384;
    }
    
    public int method10784() {
	return 249646987 * anInt8490;
    }
    
    public float method10785() {
	return aFloat8501;
    }
    
    public int method10786() {
	return 121694691 * anInt8488;
    }
    
    public int method10787() {
	return 121694691 * anInt8488;
    }
    
    public float method10788(int i) {
	return aFloat8486;
    }
    
    public int method10789() {
	return anInt8496 * 126740913;
    }
    
    public int method10790() {
	return -1241422223 * anInt8497;
    }
    
    public int method10791() {
	return -1241422223 * anInt8497;
    }
    
    public int method10792() {
	return -1241422223 * anInt8497;
    }
    
    public int method10793() {
	return anInt8499 * -783242861;
    }
    
    public int method10794() {
	return -993952189 * anInt8495;
    }
    
    public Class653() {
	method10783(132797279);
    }
    
    public float method10795(byte i) {
	return aFloat8485;
    }
    
    public float method10796(int i) {
	return aFloatArray8502[i];
    }
    
    public int method10797() {
	return 304860779 * anInt8494;
    }
    
    public int method10798() {
	return anInt8503 * 1777623545;
    }
    
    static final void method10799(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class589.method9872(class247, class243, class669, -160818505);
    }
    
    static final void method10800(Class669 class669, byte i) {
	float f
	    = Class427.method6799(class669.anIntArray8595[((class669.anInt8600
							    -= 308999563)
							   * 2088438307)]);
	Class534_Sub18_Sub13_Sub1 class534_sub18_sub13_sub1
	    = new Class534_Sub18_Sub13_Sub1(Class599.aClass298_Sub1_7871
						.method15678(-288224045),
					    f);
	Class599.aClass298_Sub1_7871.method5373(class534_sub18_sub13_sub1,
						356365251);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class534_sub18_sub13_sub1.anInt11802 * -997430305;
    }
    
    static boolean method10801(byte i) {
	return Class72.anInt765 * 324852453 > 0;
    }
    
    public static void method10802(byte i) {
	Class86.aClass700_846 = new Class700();
    }
    
    static Class536_Sub3 method10803(int i) {
	Class536_Sub3 class536_sub3
	    = ((Class536_Sub3)
	       Class536_Sub3.aClass709_10362.method14290(544815340));
	if (null != class536_sub3) {
	    Class536_Sub3.anInt10357 -= 816205627;
	    return class536_sub3;
	}
	return new Class536_Sub3();
    }
    
    static void method10804(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class253.method4634(-1294122672);
    }
    
    static void method10805(int i, int i_4_, int i_5_, byte i_6_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(9, (long) i);
	class534_sub18_sub6.method18121(1014277405);
	class534_sub18_sub6.anInt11666 = i_4_ * 517206857;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_5_;
    }
    
    public static void method10806(int i, int i_7_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
}
