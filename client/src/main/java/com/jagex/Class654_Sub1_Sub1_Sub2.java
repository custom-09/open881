/* Class654_Sub1_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub1_Sub2 extends Class654_Sub1_Sub1
    implements Interface62
{
    static int[] anIntArray12017;
    boolean aBool12018;
    public Class528 aClass528_12019;
    static int[] anIntArray12020 = { 1, 2, 4, 8 };
    Class564 aClass564_12021;
    boolean aBool12022 = true;
    
    public int method16866() {
	return aClass528_12019.method8805((byte) 82);
    }
    
    boolean method16864() {
	return false;
    }
    
    boolean method16850(int i) {
	return aBool12022;
    }
    
    void method16868(Class185 class185, int i) {
	Class183 class183 = aClass528_12019.method8787(class185, 262144, true,
						       true, 2025292367);
	if (null != class183) {
	    Class446 class446 = method10834();
	    Class444 class444 = method10807();
	    int i_0_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	    int i_1_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	    aClass528_12019.method8788(class185, class183, class446, i_0_,
				       i_0_, i_1_, i_1_, false, (byte) -81);
	}
    }
    
    public int method16876(int i) {
	return aClass528_12019.method8805((byte) 62);
    }
    
    public int method16897(int i) {
	return aClass528_12019.method8789((byte) 1);
    }
    
    public void method18632(Class596 class596, byte i) {
	aClass528_12019.method8783(class596, (byte) -91);
    }
    
    Class550 method16853(Class185 class185, int i) {
	Class183 class183 = aClass528_12019.method8787(class185, 2048, false,
						       true, 1400010270);
	if (null == class183)
	    return null;
	Class446 class446 = method10834();
	Class444 class444 = method10807();
	Class550 class550 = Class322.method5779(aBool12018, -174127692);
	int i_2_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_3_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	aClass528_12019.method8788(class185, class183, class446, i_2_, i_2_,
				   i_3_, i_3_, true, (byte) -73);
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (aClass528_12019.aClass629_7105 != null) {
	    Class174 class174 = aClass528_12019.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12022
	    = class183.method3027() || null != aClass528_12019.aClass629_7105;
	if (aClass564_12021 == null)
	    aClass564_12021
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 109);
	else
	    Class274.method5144(aClass564_12021,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, -1044561869);
	return class550;
    }
    
    final void method16877(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_4_, int i_5_, boolean bool) {
	throw new IllegalStateException();
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return aClass564_12021;
    }
    
    final boolean method16848(byte i) {
	return false;
    }
    
    public void method414(Class185 class185, int i) {
	aClass528_12019.method8791(class185, 198005613);
    }
    
    public Class654_Sub1_Sub1_Sub2
	(Class556 class556, Class185 class185, Class44_Sub13 class44_sub13,
	 Class602 class602, int i, int i_6_, int i_7_, int i_8_, int i_9_,
	 boolean bool, int i_10_, int i_11_, int i_12_, int i_13_) {
	super(class556, i_7_, i_8_, i_9_, i, i_6_,
	      Class415.method6744(i_10_, i_11_, 1779928515));
	aClass528_12019
	    = new Class528(class185, class44_sub13, class602, i_10_, i_11_,
			   i_6_, this, bool, i_12_, i_13_);
	aBool12018 = 0 != -2134171963 * class602.anInt7907 && !bool;
	method16862(1, -134493483);
    }
    
    public int method16854() {
	return aClass528_12019.method8789((byte) 1);
    }
    
    public int method409(int i) {
	return -1932952217 * aClass528_12019.anInt7104;
    }
    
    public int method410(int i) {
	return aClass528_12019.anInt7111 * -2129482149;
    }
    
    boolean method16880(Class185 class185, int i, int i_14_) {
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null)
	    return class185.method3309(i, i_14_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12019.method8787(class185, 131072, false,
						       false, 1691946738);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_14_, method10834(), false, 0);
    }
    
    public boolean method413(byte i) {
	return true;
    }
    
    public boolean method419(byte i) {
	return aClass528_12019.method8809((byte) 77);
    }
    
    final boolean method16861() {
	return false;
    }
    
    public int method56(int i) {
	return aClass528_12019.anInt7103 * 1626333597;
    }
    
    static {
	anIntArray12017 = new int[] { 16, 32, 64, 128 };
    }
    
    public int method9() {
	return -1932952217 * aClass528_12019.anInt7104;
    }
    
    public int method145() {
	return aClass528_12019.anInt7111 * -2129482149;
    }
    
    public int method181() {
	return aClass528_12019.anInt7111 * -2129482149;
    }
    
    public void method141() {
	/* empty */
    }
    
    public void method144() {
	/* empty */
    }
    
    public boolean method416() {
	return aClass528_12019.method8809((byte) 77);
    }
    
    public void method417(Class185 class185) {
	aClass528_12019.method8790(class185, -2097306107);
    }
    
    public void method422(Class185 class185) {
	aClass528_12019.method8790(class185, -1062300476);
    }
    
    public void method418(Class185 class185) {
	aClass528_12019.method8790(class185, -400300576);
    }
    
    public void method421(Class185 class185) {
	aClass528_12019.method8791(class185, 198005613);
    }
    
    final void method16883(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_15_, int i_16_, boolean bool) {
	throw new IllegalStateException();
    }
    
    boolean method16895() {
	return false;
    }
    
    final void method16851(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_17_, int i_18_, boolean bool,
			   int i_19_) {
	throw new IllegalStateException();
    }
    
    final void method16852(int i) {
	throw new IllegalStateException();
    }
    
    public boolean method423() {
	return true;
    }
    
    public int method16867() {
	return aClass528_12019.method8805((byte) 16);
    }
    
    public boolean method260() {
	return true;
    }
    
    public Class564 method16872(Class185 class185) {
	return aClass564_12021;
    }
    
    public Class564 method16870(Class185 class185) {
	return aClass564_12021;
    }
    
    void method16871(Class185 class185) {
	Class183 class183 = aClass528_12019.method8787(class185, 262144, true,
						       true, 1851970585);
	if (null != class183) {
	    Class446 class446 = method10834();
	    Class444 class444 = method10807();
	    int i = (int) class444.aClass438_4885.aFloat4864 >> 9;
	    int i_20_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	    aClass528_12019.method8788(class185, class183, class446, i, i,
				       i_20_, i_20_, false, (byte) -110);
	}
    }
    
    public void method420(Class185 class185) {
	aClass528_12019.method8791(class185, 198005613);
    }
    
    boolean method16846(Class185 class185, int i, int i_21_, byte i_22_) {
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null)
	    return class185.method3309(i, i_21_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12019.method8787(class185, 131072, false,
						       false, 1764281011);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_21_, method10834(), false, 0);
    }
    
    boolean method16874(Class185 class185, int i, int i_23_) {
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null)
	    return class185.method3309(i, i_23_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12019.method8787(class185, 131072, false,
						       false, 1525691865);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_23_, method10834(), false, 0);
    }
    
    boolean method16882(Class185 class185, int i, int i_24_) {
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null)
	    return class185.method3309(i, i_24_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12019.method8787(class185, 131072, false,
						       false, 1897544618);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_24_, method10834(), false, 0);
    }
    
    boolean method16879() {
	return aBool12022;
    }
    
    boolean method16873(Class185 class185, int i, int i_25_) {
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null)
	    return class185.method3309(i, i_25_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12019.method8787(class185, 131072, false,
						       false, 1956810654);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_25_, method10834(), false, 0);
    }
    
    boolean method16869() {
	return aBool12022;
    }
    
    public int method16875() {
	return aClass528_12019.method8789((byte) 1);
    }
    
    final void method16865() {
	throw new IllegalStateException();
    }
    
    final void method16881() {
	throw new IllegalStateException();
    }
    
    public int method252() {
	return aClass528_12019.anInt7103 * 1626333597;
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    public int method254() {
	return aClass528_12019.anInt7103 * 1626333597;
    }
    
    public boolean method412() {
	return true;
    }
    
    public boolean method415() {
	return true;
    }
    
    public void method408(Class185 class185, byte i) {
	aClass528_12019.method8790(class185, -1151963887);
    }
    
    final void method16845() {
	throw new IllegalStateException();
    }
    
    public int method253() {
	return aClass528_12019.anInt7103 * 1626333597;
    }
    
    public void method411(int i) {
	/* empty */
    }
    
    Class550 method16884(Class185 class185) {
	Class183 class183 = aClass528_12019.method8787(class185, 2048, false,
						       true, 2095540046);
	if (null == class183)
	    return null;
	Class446 class446 = method10834();
	Class444 class444 = method10807();
	Class550 class550 = Class322.method5779(aBool12018, 2088641680);
	int i = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_26_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	aClass528_12019.method8788(class185, class183, class446, i, i, i_26_,
				   i_26_, true, (byte) -45);
	Class602 class602 = aClass528_12019.method8786(1460193483);
	if (class602.aClass432_7956 != null) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (aClass528_12019.aClass629_7105 != null) {
	    Class174 class174 = aClass528_12019.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12022
	    = class183.method3027() || null != aClass528_12019.aClass629_7105;
	if (aClass564_12021 == null)
	    aClass564_12021
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 109);
	else
	    Class274.method5144(aClass564_12021,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, 1374646724);
	return class550;
    }
    
    static int method18633(int i, int i_27_) {
	if (i == Class595.aClass595_7830.anInt7852 * 847393323
	    || i == 847393323 * Class595.aClass595_7832.anInt7852)
	    return anIntArray12017[i_27_ & 0x3];
	return anIntArray12020[i_27_ & 0x3];
    }
    
    public void method18634(Class596 class596) {
	aClass528_12019.method8783(class596, (byte) -68);
    }
    
    static final void method18635(Class669 class669, byte i) {
	int i_28_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass33Array11299[i_28_].aBool272 ? 1 : 0;
    }
}
