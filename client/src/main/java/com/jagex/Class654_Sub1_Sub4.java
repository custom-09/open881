/* Class654_Sub1_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class654_Sub1_Sub4 extends Class654_Sub1
{
    final void method16851(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_0_, int i_1_, boolean bool, int i_2_) {
	throw new IllegalStateException();
    }
    
    Class654_Sub1_Sub4(Class556 class556, int i, int i_3_, int i_4_, int i_5_,
		       int i_6_) {
	super(class556);
	aByte10854 = (byte) i_5_;
	aByte10853 = (byte) i_6_;
	method10809(new Class438((float) i, (float) i_3_, (float) i_4_));
    }
    
    boolean method16857(Class185 class185, int i) {
	Class438 class438 = method10807().aClass438_4885;
	Class559 class559
	    = aClass556_10855.method9263(aByte10853,
					 ((int) class438.aFloat4864
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 ((int) class438.aFloat4865
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 -796106445);
	if (null != class559 && class559.aClass654_Sub1_Sub5_7500.aBool11902)
	    return (aClass556_10855.aClass552_7427.method9064
		    (aByte10853,
		     ((int) class438.aFloat4864
		      >> aClass556_10855.anInt7422 * 941710601),
		     ((int) class438.aFloat4865
		      >> aClass556_10855.anInt7422 * 941710601),
		     (class559.aClass654_Sub1_Sub5_7500
			  .method16876(-1807951113)
		      + method16876(-1692485349))));
	return (aClass556_10855.aClass552_7427.method9062
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> aClass556_10855.anInt7422 * 941710601),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422)));
    }
    
    boolean method16858(byte i) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(((int) class438.aFloat4864
		   >> 941710601 * aClass556_10855.anInt7422)
		  - -380604831 * aClass556_10855.anInt7455
		  + -1213435377 * aClass556_10855.anInt7461)]
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4865
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7459 * -1709472547))]);
    }
    
    int method16856(Class534_Sub21[] class534_sub21s, int i) {
	Class438 class438 = method10807().aClass438_4885;
	return method16860(((int) class438.aFloat4864
			    >> aClass556_10855.anInt7422 * 941710601),
			   ((int) class438.aFloat4865
			    >> aClass556_10855.anInt7422 * 941710601),
			   class534_sub21s, -1148195433);
    }
    
    final void method16845() {
	throw new IllegalStateException();
    }
    
    final boolean method16848(byte i) {
	return false;
    }
    
    final boolean method16861() {
	return false;
    }
    
    final void method16877(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_7_, int i_8_, boolean bool) {
	throw new IllegalStateException();
    }
    
    final void method16852(int i) {
	throw new IllegalStateException();
    }
    
    boolean method16893() {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(((int) class438.aFloat4864
		   >> 941710601 * aClass556_10855.anInt7422)
		  - -380604831 * aClass556_10855.anInt7455
		  + -1213435377 * aClass556_10855.anInt7461)]
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4865
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7459 * -1709472547))]);
    }
    
    final void method16865() {
	throw new IllegalStateException();
    }
    
    final void method16881() {
	throw new IllegalStateException();
    }
    
    int method16888(Class534_Sub21[] class534_sub21s) {
	Class438 class438 = method10807().aClass438_4885;
	return method16860(((int) class438.aFloat4864
			    >> aClass556_10855.anInt7422 * 941710601),
			   ((int) class438.aFloat4865
			    >> aClass556_10855.anInt7422 * 941710601),
			   class534_sub21s, -104797918);
    }
    
    boolean method16889(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	Class559 class559
	    = aClass556_10855.method9263(aByte10853,
					 ((int) class438.aFloat4864
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 ((int) class438.aFloat4865
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 -27349877);
	if (null != class559 && class559.aClass654_Sub1_Sub5_7500.aBool11902)
	    return (aClass556_10855.aClass552_7427.method9064
		    (aByte10853,
		     ((int) class438.aFloat4864
		      >> aClass556_10855.anInt7422 * 941710601),
		     ((int) class438.aFloat4865
		      >> aClass556_10855.anInt7422 * 941710601),
		     (class559.aClass654_Sub1_Sub5_7500
			  .method16876(-2081834304)
		      + method16876(-1446297081))));
	return (aClass556_10855.aClass552_7427.method9062
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> aClass556_10855.anInt7422 * 941710601),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422)));
    }
    
    boolean method16890(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	Class559 class559
	    = aClass556_10855.method9263(aByte10853,
					 ((int) class438.aFloat4864
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 ((int) class438.aFloat4865
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 -1779987435);
	if (null != class559 && class559.aClass654_Sub1_Sub5_7500.aBool11902)
	    return (aClass556_10855.aClass552_7427.method9064
		    (aByte10853,
		     ((int) class438.aFloat4864
		      >> aClass556_10855.anInt7422 * 941710601),
		     ((int) class438.aFloat4865
		      >> aClass556_10855.anInt7422 * 941710601),
		     (class559.aClass654_Sub1_Sub5_7500.method16876(-922469065)
		      + method16876(-1797049915))));
	return (aClass556_10855.aClass552_7427.method9062
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> aClass556_10855.anInt7422 * 941710601),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422)));
    }
    
    boolean method16891(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	Class559 class559
	    = aClass556_10855.method9263(aByte10853,
					 ((int) class438.aFloat4864
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 ((int) class438.aFloat4865
					  >> (941710601
					      * aClass556_10855.anInt7422)),
					 125212570);
	if (null != class559 && class559.aClass654_Sub1_Sub5_7500.aBool11902)
	    return (aClass556_10855.aClass552_7427.method9064
		    (aByte10853,
		     ((int) class438.aFloat4864
		      >> aClass556_10855.anInt7422 * 941710601),
		     ((int) class438.aFloat4865
		      >> aClass556_10855.anInt7422 * 941710601),
		     (class559.aClass654_Sub1_Sub5_7500.method16876(-991403778)
		      + method16876(-1926847332))));
	return (aClass556_10855.aClass552_7427.method9062
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> aClass556_10855.anInt7422 * 941710601),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422)));
    }
    
    boolean method16885() {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(((int) class438.aFloat4864
		   >> 941710601 * aClass556_10855.anInt7422)
		  - -380604831 * aClass556_10855.anInt7455
		  + -1213435377 * aClass556_10855.anInt7461)]
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4865
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7459 * -1709472547))]);
    }
    
    final void method16883(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_9_, int i_10_, boolean bool) {
	throw new IllegalStateException();
    }
}
