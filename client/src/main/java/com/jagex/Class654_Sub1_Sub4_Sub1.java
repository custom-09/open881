/* Class654_Sub1_Sub4_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub4_Sub1 extends Class654_Sub1_Sub4
{
    public Class446 aClass446_12134;
    public int anInt12135 = -420367235;
    public int anInt12136;
    boolean aBool12137;
    public int anInt12138;
    public int anInt12139 = 172960067;
    public int anInt12140;
    public int anInt12141;
    static Class446 aClass446_12142 = new Class446();
    public Class446 aClass446_12143 = null;
    public int anInt12144;
    int anInt12145;
    int anInt12146;
    int anInt12147;
    public Class446 aClass446_12148 = null;
    int anInt12149;
    int anInt12150;
    
    boolean method16882(Class185 class185, int i, int i_0_) {
	Class446 class446 = class185.method3665();
	class446.method7237(method10807());
	class446.method7287(0.0F, -10.0F, 0.0F);
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(1392959229 * anInt12141,
						     -548167859))
		   .method689
	       (class185, 131072, anInt12136 * -1769733337, null, null, 0, 0,
		0, 0, 2137245355));
	if (class183 != null) {
	    if (aClass446_12143 == null)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    if (class183.method3039(i, i_0_, aClass446_12142, true, 0))
		return true;
	}
	if (382442283 * anInt12135 != -1) {
	    class183
		= (((Class15) Class531.aClass44_Sub7_7135
				  .method91(382442283 * anInt12135, 293113798))
		       .method689
		   (class185, 131072, -2065912723 * anInt12138, null, null, 0,
		    0, 0, 0, 2125634822));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_0_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	if (-1 != anInt12139 * -866036587) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(anInt12139 * -866036587, -1485942266))
			    .method689
			(class185, 131072, anInt12140 * 10971137, null, null,
			 0, 0, 0, 0, 693394572));
	    if (class183 != null) {
		if (null == aClass446_12134)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_0_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	return false;
    }
    
    void method18768(int i) {
	anInt12147 = (32 + (int) (Math.random() * 4.0)) * -1971656505;
	anInt12146 = (3 + (int) (Math.random() * 2.0)) * -2044038625;
	anInt12149 = (16 + (int) (Math.random() * 3.0)) * -234113251;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -32)
	    == 1)
	    anInt12150 = (int) (Math.random() * 10.0) * -1499301503;
	else
	    anInt12150 = (int) (Math.random() * 20.0) * -1499301503;
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    boolean method16850(int i) {
	return aBool12137;
    }
    
    public static Class446 method18769() {
	Class446 class446 = new Class446();
	class446.method7244(0.0F, 1.0F, 0.0F,
			    (float) (Math.random() * 3.141592653589793 * 2.0));
	float f = (float) (0.5 + 0.5 * Math.random()) * 256.0F;
	class446.method7287(f - 128.0F, 0.0F, 0.0F);
	class446.method7245(0.0F, 1.0F, 0.0F,
			    (float) (Math.random() * 3.141592653589793 * 2.0));
	return class446;
    }
    
    public int method16876(int i) {
	return -10;
    }
    
    void method16871(Class185 class185) {
	/* empty */
    }
    
    void method16868(Class185 class185, int i) {
	/* empty */
    }
    
    public Class654_Sub1_Sub4_Sub1(Class556 class556, int i, int i_1_,
				   int i_2_, int i_3_, int i_4_) {
	super(class556, i, i_1_, i_2_, i_3_, i_4_);
	aClass446_12134 = null;
	anInt12144 = 0;
	anInt12145 = 0;
	aBool12137 = false;
	method16862(3, -1852871983);
	method18768(1433527933);
    }
    
    boolean method16846(Class185 class185, int i, int i_5_, byte i_6_) {
	Class446 class446 = class185.method3665();
	class446.method7237(method10807());
	class446.method7287(0.0F, -10.0F, 0.0F);
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(1392959229 * anInt12141,
						     -1420887653))
		   .method689
	       (class185, 131072, anInt12136 * -1769733337, null, null, 0, 0,
		0, 0, 2088550356));
	if (class183 != null) {
	    if (aClass446_12143 == null)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    if (class183.method3039(i, i_5_, aClass446_12142, true, 0))
		return true;
	}
	if (382442283 * anInt12135 != -1) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(382442283 * anInt12135, -1334249037))
			    .method689
			(class185, 131072, -2065912723 * anInt12138, null,
			 null, 0, 0, 0, 0, 1712295209));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_5_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	if (-1 != anInt12139 * -866036587) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(anInt12139 * -866036587, 298673174))
			    .method689
			(class185, 131072, anInt12140 * 10971137, null, null,
			 0, 0, 0, 0, 767182753));
	    if (class183 != null) {
		if (null == aClass446_12134)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_5_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	return false;
    }
    
    public int method16897(int i) {
	return anInt12144 * 725519827 - method16876(-1943641213);
    }
    
    boolean method16895() {
	return false;
    }
    
    boolean method16864() {
	return false;
    }
    
    boolean method16879() {
	return aBool12137;
    }
    
    boolean method16869() {
	return aBool12137;
    }
    
    public int method16867() {
	return -10;
    }
    
    public int method16866() {
	return -10;
    }
    
    int method16847(int i) {
	Class15 class15
	    = ((Class15)
	       Class531.aClass44_Sub7_7135.method91(anInt12141 * 1392959229,
						    -2007901696));
	int i_7_ = class15.anInt178 * 2041587899;
	if (382442283 * anInt12135 != -1) {
	    Class15 class15_8_
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91(382442283 * anInt12135,
							705458542));
	    if (2041587899 * class15_8_.anInt178 > i_7_)
		i_7_ = class15_8_.anInt178 * 2041587899;
	}
	if (anInt12139 * -866036587 != -1) {
	    Class15 class15_9_
		= ((Class15)
		   Class531.aClass44_Sub7_7135
		       .method91(anInt12139 * -866036587, -1997457661));
	    if (2041587899 * class15_9_.anInt178 > i_7_)
		i_7_ = 2041587899 * class15_9_.anInt178;
	}
	return i_7_;
    }
    
    public Class564 method16870(Class185 class185) {
	return null;
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return null;
    }
    
    boolean method16880(Class185 class185, int i, int i_10_) {
	Class446 class446 = class185.method3665();
	class446.method7237(method10807());
	class446.method7287(0.0F, -10.0F, 0.0F);
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(1392959229 * anInt12141,
						     -1027761019))
		   .method689
	       (class185, 131072, anInt12136 * -1769733337, null, null, 0, 0,
		0, 0, 1812425519));
	if (class183 != null) {
	    if (aClass446_12143 == null)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    if (class183.method3039(i, i_10_, aClass446_12142, true, 0))
		return true;
	}
	if (382442283 * anInt12135 != -1) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(382442283 * anInt12135, -836020327))
			    .method689
			(class185, 131072, -2065912723 * anInt12138, null,
			 null, 0, 0, 0, 0, 1848063046));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_10_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	if (-1 != anInt12139 * -866036587) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(anInt12139 * -866036587, -223371652))
			    .method689
			(class185, 131072, anInt12140 * 10971137, null, null,
			 0, 0, 0, 0, 966513459));
	    if (class183 != null) {
		if (null == aClass446_12134)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_10_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	return false;
    }
    
    Class550 method16853(Class185 class185, int i) {
	Class438 class438 = Class438.method6994(method10807().aClass438_4885);
	Class559 class559
	    = aClass556_10855.method9263(aByte10854,
					 (int) class438.aFloat4864 >> 9,
					 (int) class438.aFloat4865 >> 9,
					 13475567);
	Class654_Sub1_Sub2 class654_sub1_sub2
	    = aClass556_10855.method9264(aByte10854,
					 (int) class438.aFloat4864 >> 9,
					 (int) class438.aFloat4865 >> 9,
					 (byte) 28);
	int i_11_ = 0;
	for (/**/; null != class559; class559 = class559.aClass559_7497) {
	    if (class559.aClass654_Sub1_Sub5_7500.aBool11902
		&& (class559.aClass654_Sub1_Sub5_7500.method16876(-1346414067)
		    < i_11_))
		i_11_ = class559.aClass654_Sub1_Sub5_7500
			    .method16876(-988284496);
	}
	if (class654_sub1_sub2 != null
	    && class654_sub1_sub2.aShort11864 > -i_11_)
	    i_11_ = -class654_sub1_sub2.aShort11864;
	if (i_11_ != anInt12144 * 725519827) {
	    class438.aFloat4863 += (float) (i_11_ - anInt12144 * 725519827);
	    method10809(class438);
	    anInt12144 = i_11_ * 1469207131;
	}
	Class446 class446 = class185.method3665();
	class446.method7254();
	if (0 == anInt12144 * 725519827) {
	    boolean bool = false;
	    boolean bool_12_ = false;
	    boolean bool_13_ = false;
	    Class151 class151 = aClass556_10855.aClass151Array7432[aByte10853];
	    int i_14_ = 604626939 * anInt12145 << 1;
	    int i_15_ = i_14_;
	    int i_16_ = -i_14_ / 2;
	    int i_17_ = -i_15_ / 2;
	    int i_18_ = class151.method2498(i_16_ + (int) class438.aFloat4864,
					    i_17_ + (int) class438.aFloat4865,
					    -2891647);
	    int i_19_ = i_14_ / 2;
	    int i_20_ = -i_15_ / 2;
	    int i_21_ = class151.method2498((int) class438.aFloat4864 + i_19_,
					    (int) class438.aFloat4865 + i_20_,
					    -2052100904);
	    int i_22_ = -i_14_ / 2;
	    int i_23_ = i_15_ / 2;
	    int i_24_ = class151.method2498(i_22_ + (int) class438.aFloat4864,
					    (int) class438.aFloat4865 + i_23_,
					    1042081045);
	    int i_25_ = i_14_ / 2;
	    int i_26_ = i_15_ / 2;
	    int i_27_ = class151.method2498(i_25_ + (int) class438.aFloat4864,
					    i_26_ + (int) class438.aFloat4865,
					    -1544138455);
	    int i_28_ = i_18_ < i_21_ ? i_18_ : i_21_;
	    int i_29_ = i_24_ < i_27_ ? i_24_ : i_27_;
	    int i_30_ = i_21_ < i_27_ ? i_21_ : i_27_;
	    int i_31_ = i_18_ < i_24_ ? i_18_ : i_24_;
	    if (0 != i_15_) {
		int i_32_ = ((int) (Math.atan2((double) (i_28_ - i_29_),
					       (double) i_15_)
				    * 2607.5945876176133)
			     & 0x3fff);
		if (i_32_ != 0)
		    class446.method7245(1.0F, 0.0F, 0.0F,
					Class427.method6799(i_32_));
	    }
	    if (0 != i_14_) {
		int i_33_ = ((int) (Math.atan2((double) (i_31_ - i_30_),
					       (double) i_14_)
				    * 2607.5945876176133)
			     & 0x3fff);
		if (0 != i_33_)
		    class446.method7245(0.0F, 0.0F, 1.0F,
					Class427.method6799(-i_33_));
	    }
	    int i_34_ = i_18_ + i_27_;
	    if (i_21_ + i_24_ < i_34_)
		i_34_ = i_21_ + i_24_;
	    i_34_ = (i_34_ >> 1) - (int) class438.aFloat4863;
	    if (i_34_ != 0)
		class446.method7287(0.0F, (float) i_34_, 0.0F);
	}
	class438.method7074();
	Class438 class438_35_ = method10807().aClass438_4885;
	class446.method7287(class438_35_.aFloat4864,
			    class438_35_.aFloat4863 - 10.0F,
			    class438_35_.aFloat4865);
	Class550 class550 = Class322.method5779(true, 700080350);
	aBool12137 = false;
	anInt12145 = 0;
	if (-1 != -866036587 * anInt12139) {
	    Class183 class183
		= (((Class15)
		    Class531.aClass44_Sub7_7135
			.method91(anInt12139 * -866036587, -206481185))
		       .method689
		   (class185, 526336, 10971137 * anInt12140, null, null,
		    anInt12147 * -562865929, anInt12146 * -544437793,
		    1878573877 * anInt12149, -1683503487 * anInt12150,
		    1046979740));
	    if (null != class183) {
		if (aClass446_12134 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		class183.method3034(aClass446_12142, aClass194Array10852[2],
				    0);
		aBool12137 |= class183.method3027();
		anInt12145 = class183.method3042() * -316779725;
	    }
	}
	if (anInt12135 * 382442283 != -1) {
	    Class183 class183
		= (((Class15)
		    Class531.aClass44_Sub7_7135
			.method91(anInt12135 * 382442283, -1748018957))
		       .method689
		   (class185, 526336, -2065912723 * anInt12138, null, null,
		    -562865929 * anInt12147, anInt12146 * -544437793,
		    anInt12149 * 1878573877, anInt12150 * -1683503487,
		    2087411149));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		class183.method3034(aClass446_12142, aClass194Array10852[1],
				    0);
		aBool12137 |= class183.method3027();
		if (class183.method3042() > anInt12145 * 604626939)
		    anInt12145 = class183.method3042() * -316779725;
	    }
	}
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(anInt12141 * 1392959229,
						     1317121991))
		   .method689
	       (class185, 526336, anInt12136 * -1769733337, null, null,
		anInt12147 * -562865929, anInt12146 * -544437793,
		anInt12149 * 1878573877, -1683503487 * anInt12150, 792804926));
	if (class183 != null) {
	    if (null == aClass446_12143)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    class183.method3034(aClass446_12142, aClass194Array10852[0], 0);
	    aBool12137 |= class183.method3027();
	    if (class183.method3042() > anInt12145 * 604626939)
		anInt12145 = class183.method3042() * -316779725;
	}
	return class550;
    }
    
    boolean method16874(Class185 class185, int i, int i_36_) {
	Class446 class446 = class185.method3665();
	class446.method7237(method10807());
	class446.method7287(0.0F, -10.0F, 0.0F);
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(1392959229 * anInt12141,
						     1523061991))
		   .method689
	       (class185, 131072, anInt12136 * -1769733337, null, null, 0, 0,
		0, 0, 1518707948));
	if (class183 != null) {
	    if (aClass446_12143 == null)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    if (class183.method3039(i, i_36_, aClass446_12142, true, 0))
		return true;
	}
	if (382442283 * anInt12135 != -1) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(382442283 * anInt12135, -1997867356))
			    .method689
			(class185, 131072, -2065912723 * anInt12138, null,
			 null, 0, 0, 0, 0, 1933267723));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_36_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	if (-1 != anInt12139 * -866036587) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(anInt12139 * -866036587, 1112941373))
			    .method689
			(class185, 131072, anInt12140 * 10971137, null, null,
			 0, 0, 0, 0, 2125012031));
	    if (class183 != null) {
		if (null == aClass446_12134)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_36_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	return false;
    }
    
    void method18770() {
	anInt12147 = (32 + (int) (Math.random() * 4.0)) * -1971656505;
	anInt12146 = (3 + (int) (Math.random() * 2.0)) * -2044038625;
	anInt12149 = (16 + (int) (Math.random() * 3.0)) * -234113251;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) 13)
	    == 1)
	    anInt12150 = (int) (Math.random() * 10.0) * -1499301503;
	else
	    anInt12150 = (int) (Math.random() * 20.0) * -1499301503;
    }
    
    boolean method16873(Class185 class185, int i, int i_37_) {
	Class446 class446 = class185.method3665();
	class446.method7237(method10807());
	class446.method7287(0.0F, -10.0F, 0.0F);
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(1392959229 * anInt12141,
						     1252248534))
		   .method689
	       (class185, 131072, anInt12136 * -1769733337, null, null, 0, 0,
		0, 0, 2018025407));
	if (class183 != null) {
	    if (aClass446_12143 == null)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    if (class183.method3039(i, i_37_, aClass446_12142, true, 0))
		return true;
	}
	if (382442283 * anInt12135 != -1) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(382442283 * anInt12135, -639433738))
			    .method689
			(class185, 131072, -2065912723 * anInt12138, null,
			 null, 0, 0, 0, 0, 1657480627));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_37_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	if (-1 != anInt12139 * -866036587) {
	    class183 = (((Class15)
			 Class531.aClass44_Sub7_7135
			     .method91(anInt12139 * -866036587, 262843495))
			    .method689
			(class185, 131072, anInt12140 * 10971137, null, null,
			 0, 0, 0, 0, 1310423663));
	    if (class183 != null) {
		if (null == aClass446_12134)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		if (class183.method3039(i, i_37_, aClass446_12142, true, 0))
		    return true;
	    }
	}
	return false;
    }
    
    public Class564 method16872(Class185 class185) {
	return null;
    }
    
    void method18771() {
	anInt12147 = (32 + (int) (Math.random() * 4.0)) * -1971656505;
	anInt12146 = (3 + (int) (Math.random() * 2.0)) * -2044038625;
	anInt12149 = (16 + (int) (Math.random() * 3.0)) * -234113251;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -13)
	    == 1)
	    anInt12150 = (int) (Math.random() * 10.0) * -1499301503;
	else
	    anInt12150 = (int) (Math.random() * 20.0) * -1499301503;
    }
    
    Class550 method16884(Class185 class185) {
	Class438 class438 = Class438.method6994(method10807().aClass438_4885);
	Class559 class559
	    = aClass556_10855.method9263(aByte10854,
					 (int) class438.aFloat4864 >> 9,
					 (int) class438.aFloat4865 >> 9,
					 -2065380251);
	Class654_Sub1_Sub2 class654_sub1_sub2
	    = aClass556_10855.method9264(aByte10854,
					 (int) class438.aFloat4864 >> 9,
					 (int) class438.aFloat4865 >> 9,
					 (byte) 69);
	int i = 0;
	for (/**/; null != class559; class559 = class559.aClass559_7497) {
	    if (class559.aClass654_Sub1_Sub5_7500.aBool11902
		&& (class559.aClass654_Sub1_Sub5_7500.method16876(-1822502700)
		    < i))
		i = class559.aClass654_Sub1_Sub5_7500.method16876(-1954528053);
	}
	if (class654_sub1_sub2 != null && class654_sub1_sub2.aShort11864 > -i)
	    i = -class654_sub1_sub2.aShort11864;
	if (i != anInt12144 * 725519827) {
	    class438.aFloat4863 += (float) (i - anInt12144 * 725519827);
	    method10809(class438);
	    anInt12144 = i * 1469207131;
	}
	Class446 class446 = class185.method3665();
	class446.method7254();
	if (0 == anInt12144 * 725519827) {
	    boolean bool = false;
	    boolean bool_38_ = false;
	    boolean bool_39_ = false;
	    Class151 class151 = aClass556_10855.aClass151Array7432[aByte10853];
	    int i_40_ = 604626939 * anInt12145 << 1;
	    int i_41_ = i_40_;
	    int i_42_ = -i_40_ / 2;
	    int i_43_ = -i_41_ / 2;
	    int i_44_ = class151.method2498(i_42_ + (int) class438.aFloat4864,
					    i_43_ + (int) class438.aFloat4865,
					    -1271455011);
	    int i_45_ = i_40_ / 2;
	    int i_46_ = -i_41_ / 2;
	    int i_47_ = class151.method2498((int) class438.aFloat4864 + i_45_,
					    (int) class438.aFloat4865 + i_46_,
					    783261291);
	    int i_48_ = -i_40_ / 2;
	    int i_49_ = i_41_ / 2;
	    int i_50_ = class151.method2498(i_48_ + (int) class438.aFloat4864,
					    (int) class438.aFloat4865 + i_49_,
					    -1797212665);
	    int i_51_ = i_40_ / 2;
	    int i_52_ = i_41_ / 2;
	    int i_53_ = class151.method2498(i_51_ + (int) class438.aFloat4864,
					    i_52_ + (int) class438.aFloat4865,
					    1735168898);
	    int i_54_ = i_44_ < i_47_ ? i_44_ : i_47_;
	    int i_55_ = i_50_ < i_53_ ? i_50_ : i_53_;
	    int i_56_ = i_47_ < i_53_ ? i_47_ : i_53_;
	    int i_57_ = i_44_ < i_50_ ? i_44_ : i_50_;
	    if (0 != i_41_) {
		int i_58_ = ((int) (Math.atan2((double) (i_54_ - i_55_),
					       (double) i_41_)
				    * 2607.5945876176133)
			     & 0x3fff);
		if (i_58_ != 0)
		    class446.method7245(1.0F, 0.0F, 0.0F,
					Class427.method6799(i_58_));
	    }
	    if (0 != i_40_) {
		int i_59_ = ((int) (Math.atan2((double) (i_57_ - i_56_),
					       (double) i_40_)
				    * 2607.5945876176133)
			     & 0x3fff);
		if (0 != i_59_)
		    class446.method7245(0.0F, 0.0F, 1.0F,
					Class427.method6799(-i_59_));
	    }
	    int i_60_ = i_44_ + i_53_;
	    if (i_47_ + i_50_ < i_60_)
		i_60_ = i_47_ + i_50_;
	    i_60_ = (i_60_ >> 1) - (int) class438.aFloat4863;
	    if (i_60_ != 0)
		class446.method7287(0.0F, (float) i_60_, 0.0F);
	}
	class438.method7074();
	Class438 class438_61_ = method10807().aClass438_4885;
	class446.method7287(class438_61_.aFloat4864,
			    class438_61_.aFloat4863 - 10.0F,
			    class438_61_.aFloat4865);
	Class550 class550 = Class322.method5779(true, 483623998);
	aBool12137 = false;
	anInt12145 = 0;
	if (-1 != -866036587 * anInt12139) {
	    Class183 class183
		= (((Class15)
		    Class531.aClass44_Sub7_7135
			.method91(anInt12139 * -866036587, 282762449))
		       .method689
		   (class185, 526336, 10971137 * anInt12140, null, null,
		    anInt12147 * -562865929, anInt12146 * -544437793,
		    1878573877 * anInt12149, -1683503487 * anInt12150,
		    1198256530));
	    if (null != class183) {
		if (aClass446_12134 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12134);
		aClass446_12142.method7253(class446);
		class183.method3034(aClass446_12142, aClass194Array10852[2],
				    0);
		aBool12137 |= class183.method3027();
		anInt12145 = class183.method3042() * -316779725;
	    }
	}
	if (anInt12135 * 382442283 != -1) {
	    Class183 class183
		= (((Class15)
		    Class531.aClass44_Sub7_7135
			.method91(anInt12135 * 382442283, -1632679485))
		       .method689
		   (class185, 526336, -2065912723 * anInt12138, null, null,
		    -562865929 * anInt12147, anInt12146 * -544437793,
		    anInt12149 * 1878573877, anInt12150 * -1683503487,
		    1193635015));
	    if (class183 != null) {
		if (aClass446_12148 == null)
		    aClass446_12142.method7254();
		else
		    aClass446_12142.method7236(aClass446_12148);
		aClass446_12142.method7253(class446);
		class183.method3034(aClass446_12142, aClass194Array10852[1],
				    0);
		aBool12137 |= class183.method3027();
		if (class183.method3042() > anInt12145 * 604626939)
		    anInt12145 = class183.method3042() * -316779725;
	    }
	}
	Class183 class183
	    = (((Class15)
		Class531.aClass44_Sub7_7135.method91(anInt12141 * 1392959229,
						     -43772348))
		   .method689
	       (class185, 526336, anInt12136 * -1769733337, null, null,
		anInt12147 * -562865929, anInt12146 * -544437793,
		anInt12149 * 1878573877, -1683503487 * anInt12150, 856671218));
	if (class183 != null) {
	    if (null == aClass446_12143)
		aClass446_12142.method7254();
	    else
		aClass446_12142.method7236(aClass446_12143);
	    aClass446_12142.method7253(class446);
	    class183.method3034(aClass446_12142, aClass194Array10852[0], 0);
	    aBool12137 |= class183.method3027();
	    if (class183.method3042() > anInt12145 * 604626939)
		anInt12145 = class183.method3042() * -316779725;
	}
	return class550;
    }
    
    void method18772() {
	anInt12147 = (32 + (int) (Math.random() * 4.0)) * -1971656505;
	anInt12146 = (3 + (int) (Math.random() * 2.0)) * -2044038625;
	anInt12149 = (16 + (int) (Math.random() * 3.0)) * -234113251;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -40)
	    == 1)
	    anInt12150 = (int) (Math.random() * 10.0) * -1499301503;
	else
	    anInt12150 = (int) (Math.random() * 20.0) * -1499301503;
    }
    
    public int method16875() {
	return anInt12144 * 725519827 - method16876(-1261652756);
    }
    
    public int method16854() {
	return anInt12144 * 725519827 - method16876(-1153001774);
    }
    
    int method16863() {
	Class15 class15
	    = ((Class15)
	       Class531.aClass44_Sub7_7135.method91(anInt12141 * 1392959229,
						    919163068));
	int i = class15.anInt178 * 2041587899;
	if (382442283 * anInt12135 != -1) {
	    Class15 class15_62_
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91(382442283 * anInt12135,
							-79796662));
	    if (2041587899 * class15_62_.anInt178 > i)
		i = class15_62_.anInt178 * 2041587899;
	}
	if (anInt12139 * -866036587 != -1) {
	    Class15 class15_63_
		= (Class15) Class531.aClass44_Sub7_7135
				.method91(anInt12139 * -866036587, 44468525);
	    if (2041587899 * class15_63_.anInt178 > i)
		i = 2041587899 * class15_63_.anInt178;
	}
	return i;
    }
    
    void method18773() {
	anInt12147 = (32 + (int) (Math.random() * 4.0)) * -1971656505;
	anInt12146 = (3 + (int) (Math.random() * 2.0)) * -2044038625;
	anInt12149 = (16 + (int) (Math.random() * 3.0)) * -234113251;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -91)
	    == 1)
	    anInt12150 = (int) (Math.random() * 10.0) * -1499301503;
	else
	    anInt12150 = (int) (Math.random() * 20.0) * -1499301503;
    }
    
    public static Class446 method18774() {
	Class446 class446 = new Class446();
	class446.method7244(0.0F, 1.0F, 0.0F,
			    (float) (Math.random() * 3.141592653589793 * 2.0));
	float f = (float) (0.5 + 0.5 * Math.random()) * 256.0F;
	class446.method7287(f - 128.0F, 0.0F, 0.0F);
	class446.method7245(0.0F, 1.0F, 0.0F,
			    (float) (Math.random() * 3.141592653589793 * 2.0));
	return class446;
    }
}
