/* Class654_Sub1_Sub5_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub5_Sub5 extends Class654_Sub1_Sub5
{
    boolean aBool12151;
    int anInt12152 = 0;
    Class711 aClass711_12153;
    int anInt12154 = 0;
    int anInt12155;
    Class629 aClass629_12156;
    int anInt12157;
    
    final void method16883(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_0_, int i_1_, boolean bool) {
	throw new IllegalStateException();
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return null;
    }
    
    public int method16876(int i) {
	return anInt12154 * 1008835189;
    }
    
    void method16868(Class185 class185, int i) {
	Class183 class183
	    = method18779(class185, 0, 2002081797 * anInt12155, (byte) -99);
	if (null != class183)
	    method18780(class185, class183, method10834(), (byte) -5);
    }
    
    public final boolean method18775() {
	return (null == aClass711_12153
		|| aClass711_12153.method14343((byte) 1));
    }
    
    public final void method18776(int i, byte i_2_) {
	if (null != aClass711_12153 && !aClass711_12153.method14343((byte) 1))
	    aClass711_12153.method14393(i, (byte) 1);
    }
    
    public final boolean method18777() {
	return (null == aClass711_12153
		|| aClass711_12153.method14343((byte) 1));
    }
    
    public final boolean method18778(int i) {
	return (null == aClass711_12153
		|| aClass711_12153.method14343((byte) 1));
    }
    
    Class183 method18779(Class185 class185, int i, int i_3_, byte i_4_) {
	Class684 class684
	    = (Class684) Class55.aClass44_Sub4_447.method91(i_3_, -151823260);
	Class151 class151 = aClass556_10855.aClass151Array7434[aByte10854];
	Class151 class151_5_
	    = (aByte10853 < 3
	       ? aClass556_10855.aClass151Array7434[1 + aByte10853] : null);
	Class438 class438 = method10807().aClass438_4885;
	return ((null != aClass711_12153
		 && !aClass711_12153.method14343((byte) 1))
		? class684.method13929(class185, i, anInt12152 * -1680906240,
				       class151, class151_5_,
				       (int) class438.aFloat4864,
				       (int) class438.aFloat4863,
				       (int) class438.aFloat4865,
				       aClass711_12153, (byte) 2, (byte) 65)
		: class684.method13929(class185, i, -1680906240 * anInt12152,
				       class151, class151_5_,
				       (int) class438.aFloat4864,
				       (int) class438.aFloat4863,
				       (int) class438.aFloat4865, null,
				       (byte) 2, (byte) 108));
    }
    
    Class550 method16853(Class185 class185, int i) {
	Class183 class183
	    = method18779(class185,
			  0x800 | (0 != anInt12152 * -1842120211 ? 5 : 0),
			  anInt12155 * 2002081797, (byte) -96);
	if (class183 == null)
	    return null;
	Class446 class446 = method10834();
	method18780(class185, class183, class446, (byte) -31);
	Class550 class550 = Class322.method5779(false, 1656760594);
	class183.method3034(class446, aClass194Array10852[0], 0);
	if (null != aClass629_12156) {
	    Class174 class174 = aClass629_12156.method10405();
	    class185.method3334(class174);
	}
	aBool12151 = class183.method3027();
	anInt12154 = class183.method3045() * -2144868899;
	class183.method3042();
	return class550;
    }
    
    boolean method16850(int i) {
	return aBool12151;
    }
    
    void method18780(Class185 class185, Class183 class183, Class446 class446,
		     byte i) {
	class183.method3073(class446);
	Class172[] class172s = class183.method3168();
	Class158[] class158s = class183.method3065();
	if ((aClass629_12156 == null || aClass629_12156.aBool8189)
	    && (class172s != null || class158s != null))
	    aClass629_12156 = Class629.method10422(client.anInt11101, true);
	if (null != aClass629_12156) {
	    aClass629_12156.method10393(class185, (long) client.anInt11101,
					class172s, class158s, false);
	    aClass629_12156.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
	}
    }
    
    public void method18781(byte i) {
	if (null != aClass629_12156)
	    aClass629_12156.method10389();
    }
    
    boolean method16846(Class185 class185, int i, int i_6_, byte i_7_) {
	return false;
    }
    
    final void method16877(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_8_, int i_9_, boolean bool) {
	throw new IllegalStateException();
    }
    
    boolean method16879() {
	return aBool12151;
    }
    
    final void method16852(int i) {
	throw new IllegalStateException();
    }
    
    boolean method16895() {
	return false;
    }
    
    boolean method16864() {
	return false;
    }
    
    boolean method16882(Class185 class185, int i, int i_10_) {
	return false;
    }
    
    boolean method16869() {
	return aBool12151;
    }
    
    public int method16867() {
	return anInt12154 * 1008835189;
    }
    
    public Class654_Sub1_Sub5_Sub5
	(Class556 class556, int i, int i_11_, int i_12_, int i_13_, int i_14_,
	 int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_,
	 int i_21_, boolean bool, int i_22_) {
	super(class556, i_12_, i_13_, i_14_, i_15_, i_16_, i_17_, i_18_, i_19_,
	      i_20_, false, (byte) 0);
	aBool12151 = true;
	anInt12157 = 0;
	anInt12155 = i * 935441613;
	anInt12152 = 851037157 * i_21_;
	anInt12157 = i_22_ * -844175579;
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(anInt12155 * 2002081797,
						  -2052311363));
	int i_23_ = class684.anInt8688 * -811043807;
	if (i_23_ != -1) {
	    aClass711_12153 = new Class711_Sub3(this, false);
	    int i_24_ = class684.aBool8691 ? 0 : 2;
	    if (bool)
		i_24_ = 1;
	    aClass711_12153.method14334(i_23_, i_11_, i_24_, false,
					(byte) -68);
	}
	method16862(1, -1377085818);
    }
    
    final void method16865() {
	throw new IllegalStateException();
    }
    
    public Class564 method16870(Class185 class185) {
	return null;
    }
    
    void method16871(Class185 class185) {
	Class183 class183
	    = method18779(class185, 0, 2002081797 * anInt12155, (byte) -111);
	if (null != class183)
	    method18780(class185, class183, method10834(), (byte) -43);
    }
    
    boolean method16880(Class185 class185, int i, int i_25_) {
	return false;
    }
    
    boolean method16873(Class185 class185, int i, int i_26_) {
	return false;
    }
    
    boolean method16874(Class185 class185, int i, int i_27_) {
	return false;
    }
    
    public void method18782() {
	if (null != aClass629_12156)
	    aClass629_12156.method10389();
    }
    
    final boolean method16861() {
	return false;
    }
    
    public int method18783(short i) {
	return 2120408237 * anInt12157;
    }
    
    public int method16866() {
	return anInt12154 * 1008835189;
    }
    
    final void method16845() {
	throw new IllegalStateException();
    }
    
    public void method18784() {
	if (null != aClass629_12156)
	    aClass629_12156.method10389();
    }
    
    final void method16851(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_28_, int i_29_, boolean bool,
			   int i_30_) {
	throw new IllegalStateException();
    }
    
    Class550 method16884(Class185 class185) {
	Class183 class183
	    = method18779(class185,
			  0x800 | (0 != anInt12152 * -1842120211 ? 5 : 0),
			  anInt12155 * 2002081797, (byte) -25);
	if (class183 == null)
	    return null;
	Class446 class446 = method10834();
	method18780(class185, class183, class446, (byte) 4);
	Class550 class550 = Class322.method5779(false, 694337896);
	class183.method3034(class446, aClass194Array10852[0], 0);
	if (null != aClass629_12156) {
	    Class174 class174 = aClass629_12156.method10405();
	    class185.method3334(class174);
	}
	aBool12151 = class183.method3027();
	anInt12154 = class183.method3045() * -2144868899;
	class183.method3042();
	return class550;
    }
    
    public int method18785() {
	return 2120408237 * anInt12157;
    }
    
    public int method18786() {
	return 2120408237 * anInt12157;
    }
    
    public final boolean method18787(int i) {
	return (aClass711_12153 != null
		&& !aClass711_12153.method14336(1611829927));
    }
    
    public final void method18788(int i) {
	if (null != aClass711_12153 && !aClass711_12153.method14343((byte) 1))
	    aClass711_12153.method14393(i, (byte) 1);
    }
    
    public final boolean method18789() {
	return (aClass711_12153 != null
		&& !aClass711_12153.method14336(1698706644));
    }
    
    public final boolean method18790() {
	return (null == aClass711_12153
		|| aClass711_12153.method14343((byte) 1));
    }
    
    public final void method18791(int i) {
	if (null != aClass711_12153 && !aClass711_12153.method14343((byte) 1))
	    aClass711_12153.method14393(i, (byte) 1);
    }
    
    public final boolean method18792() {
	return (null == aClass711_12153
		|| aClass711_12153.method14343((byte) 1));
    }
    
    final void method16881() {
	throw new IllegalStateException();
    }
    
    void method18793(Class185 class185, Class183 class183, Class446 class446) {
	class183.method3073(class446);
	Class172[] class172s = class183.method3168();
	Class158[] class158s = class183.method3065();
	if ((aClass629_12156 == null || aClass629_12156.aBool8189)
	    && (class172s != null || class158s != null))
	    aClass629_12156 = Class629.method10422(client.anInt11101, true);
	if (null != aClass629_12156) {
	    aClass629_12156.method10393(class185, (long) client.anInt11101,
					class172s, class158s, false);
	    aClass629_12156.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
	}
    }
    
    final boolean method16848(byte i) {
	return false;
    }
    
    void method18794(Class185 class185, Class183 class183, Class446 class446) {
	class183.method3073(class446);
	Class172[] class172s = class183.method3168();
	Class158[] class158s = class183.method3065();
	if ((aClass629_12156 == null || aClass629_12156.aBool8189)
	    && (class172s != null || class158s != null))
	    aClass629_12156 = Class629.method10422(client.anInt11101, true);
	if (null != aClass629_12156) {
	    aClass629_12156.method10393(class185, (long) client.anInt11101,
					class172s, class158s, false);
	    aClass629_12156.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
	}
    }
    
    void method18795(Class185 class185, Class183 class183, Class446 class446) {
	class183.method3073(class446);
	Class172[] class172s = class183.method3168();
	Class158[] class158s = class183.method3065();
	if ((aClass629_12156 == null || aClass629_12156.aBool8189)
	    && (class172s != null || class158s != null))
	    aClass629_12156 = Class629.method10422(client.anInt11101, true);
	if (null != aClass629_12156) {
	    aClass629_12156.method10393(class185, (long) client.anInt11101,
					class172s, class158s, false);
	    aClass629_12156.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
	}
    }
    
    void method18796(Class185 class185, Class183 class183, Class446 class446) {
	class183.method3073(class446);
	Class172[] class172s = class183.method3168();
	Class158[] class158s = class183.method3065();
	if ((aClass629_12156 == null || aClass629_12156.aBool8189)
	    && (class172s != null || class158s != null))
	    aClass629_12156 = Class629.method10422(client.anInt11101, true);
	if (null != aClass629_12156) {
	    aClass629_12156.method10393(class185, (long) client.anInt11101,
					class172s, class158s, false);
	    aClass629_12156.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
	}
    }
    
    public void method18797() {
	if (null != aClass629_12156)
	    aClass629_12156.method10389();
    }
    
    public Class564 method16872(Class185 class185) {
	return null;
    }
}
