/* Class656_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class656_Sub2 extends Class656
{
    void method10883(byte i) {
	Class534_Sub42 class534_sub42 = method17048(-960397513);
	if (null != class534_sub42) {
	    int i_0_ = method10862(class534_sub42, 32767, (byte) -57);
	    int i_1_ = class534_sub42.method16800(-1527463643);
	    if (i_1_ < 0)
		i_1_ = 0;
	    else if (i_1_ > 65535)
		i_1_ = 65535;
	    int i_2_ = class534_sub42.method16799((byte) 81);
	    if (i_2_ < 0)
		i_2_ = 0;
	    else if (i_2_ > 65535)
		i_2_ = 65535;
	    int i_3_ = 0;
	    if (class534_sub42.method16798((byte) 48) == 2)
		i_3_ = 1;
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4186,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16568(i_0_ | i_3_ << 15, -1703099548);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16578(i_2_ | i_1_ << 16, -1230333862);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 59);
	}
    }
    
    void method10864(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42, byte i) {
	/* empty */
    }
    
    boolean method10866(byte i) {
	return (method17048(-960397513) != null
		|| (aLong8522 * 1164426520149525653L
		    < Class250.method4604((byte) -14) - 2000L));
    }
    
    Class534_Sub42 method17048(int i) {
	return (Class534_Sub42) client.aClass700_11063.method14135((byte) -1);
    }
    
    Class534_Sub19 method10887(int i) {
	return Class346.method6128(Class404.aClass404_4228,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    Class534_Sub19 method10872() {
	return Class346.method6128(Class404.aClass404_4228,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    Class534_Sub42 method17049() {
	return (Class534_Sub42) client.aClass700_11063.method14135((byte) -1);
    }
    
    void method10868() {
	Class534_Sub42 class534_sub42 = method17048(-960397513);
	if (null != class534_sub42) {
	    int i = method10862(class534_sub42, 32767, (byte) -19);
	    int i_4_ = class534_sub42.method16800(-264417412);
	    if (i_4_ < 0)
		i_4_ = 0;
	    else if (i_4_ > 65535)
		i_4_ = 65535;
	    int i_5_ = class534_sub42.method16799((byte) 64);
	    if (i_5_ < 0)
		i_5_ = 0;
	    else if (i_5_ > 65535)
		i_5_ = 65535;
	    int i_6_ = 0;
	    if (class534_sub42.method16798((byte) 14) == 2)
		i_6_ = 1;
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4186,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16568(i | i_6_ << 15, -1896390349);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16578(i_5_ | i_4_ << 16, 562120965);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 26);
	}
    }
    
    void method10878(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42) {
	/* empty */
    }
    
    void method10870() {
	Class534_Sub42 class534_sub42 = method17048(-960397513);
	if (null != class534_sub42) {
	    int i = method10862(class534_sub42, 32767, (byte) -126);
	    int i_7_ = class534_sub42.method16800(-1431975146);
	    if (i_7_ < 0)
		i_7_ = 0;
	    else if (i_7_ > 65535)
		i_7_ = 65535;
	    int i_8_ = class534_sub42.method16799((byte) 26);
	    if (i_8_ < 0)
		i_8_ = 0;
	    else if (i_8_ > 65535)
		i_8_ = 65535;
	    int i_9_ = 0;
	    if (class534_sub42.method16798((byte) 101) == 2)
		i_9_ = 1;
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4186,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16568(i | i_9_ << 15, 386237562);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16578(i_8_ | i_7_ << 16, 2107117789);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 103);
	}
    }
    
    boolean method10871() {
	return (method17048(-960397513) != null
		|| (aLong8522 * 1164426520149525653L
		    < Class250.method4604((byte) -33) - 2000L));
    }
    
    int method10882(int i) {
	return 0;
    }
    
    Class656_Sub2() {
	/* empty */
    }
    
    Class534_Sub19 method10874() {
	return Class346.method6128(Class404.aClass404_4228,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    int method10867() {
	return 0;
    }
    
    int method10876() {
	return 0;
    }
    
    void method10861() {
	Class534_Sub42 class534_sub42 = method17048(-960397513);
	if (null != class534_sub42) {
	    int i = method10862(class534_sub42, 32767, (byte) -114);
	    int i_10_ = class534_sub42.method16800(-2034909056);
	    if (i_10_ < 0)
		i_10_ = 0;
	    else if (i_10_ > 65535)
		i_10_ = 65535;
	    int i_11_ = class534_sub42.method16799((byte) 45);
	    if (i_11_ < 0)
		i_11_ = 0;
	    else if (i_11_ > 65535)
		i_11_ = 65535;
	    int i_12_ = 0;
	    if (class534_sub42.method16798((byte) -21) == 2)
		i_12_ = 1;
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4186,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16568(i | i_12_ << 15, -248238263);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16578(i_11_ | i_10_ << 16, -1014527152);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 117);
	}
    }
    
    Class534_Sub19 method10873() {
	return Class346.method6128(Class404.aClass404_4228,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    void method10879(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42) {
	/* empty */
    }
    
    Class534_Sub42 method17050() {
	return (Class534_Sub42) client.aClass700_11063.method14135((byte) -1);
    }
    
    int method10877() {
	return 0;
    }
}
