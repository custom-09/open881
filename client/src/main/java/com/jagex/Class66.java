/* Class66 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class66
{
    public static Class464 aClass464_713;
    public static byte aByte714;
    
    Class66() throws Throwable {
	throw new Error();
    }
    
    static byte[] method1357(Class472 class472, Class649 class649, int i) {
	if (class649.method10707((byte) 11) > 1)
	    return class472.method7743(class649.method10711(i, -526438800),
				       class649.method10708(i, -1980884066),
				       -2131926971);
	return class472.method7743(1570156841 * class649.anInt8385, i,
				   -1297359898);
    }
    
    static byte[] method1358(Class472 class472, Class649 class649, int i) {
	if (class649.method10707((byte) 34) > 1)
	    return class472.method7743(class649.method10711(i, -1542960883),
				       class649.method10708(i, -1861361414),
				       -517492598);
	return class472.method7743(1570156841 * class649.anInt8385, i,
				   -1351697267);
    }
    
    static int method1359(Class472 class472, Class649 class649) {
	if (class472 != null) {
	    if (class649.method10707((byte) -58) > 1) {
		int i = class472.method7679(1554434211) - 1;
		return (i * class649.method10707((byte) -97)
			+ class472.method7726(i, (byte) 15));
	    }
	    return class472.method7726(class649.anInt8385 * 1570156841,
				       (byte) 89);
	}
	return 0;
    }
    
    static byte[] method1360(Class472 class472, Class649 class649, int i) {
	if (class649.method10707((byte) -43) > 1)
	    return class472.method7743(class649.method10711(i, -716833087),
				       class649.method10708(i, -2123672722),
				       -1227587359);
	return class472.method7743(1570156841 * class649.anInt8385, i,
				   -1051915281);
    }
    
    static void method1361(Class100 class100, int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4169,
				  class100.aClass13_1183, 1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(Class63.method1280(658613901), 674341188);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(1771907305 * Class706_Sub4.anInt10994, 1761291349);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(Class18.anInt205 * -1091172141, 1372099180);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757
		 .method17119((byte) 107),
	     2027824340);
	class100.method1863(class534_sub19, (byte) 96);
    }
    
    public static Class703[] method1362(byte i) {
	return (new Class703[]
		{ Class703.aClass703_8820, Class703.aClass703_8818,
		  Class703.aClass703_8817, Class703.aClass703_8822,
		  Class703.aClass703_8816, Class703.aClass703_8819 });
    }
}
