/* Class670 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class670 implements Interface6
{
    Class472 aClass472_8617;
    Class203 aClass203_8618 = new Class203(60);
    int anInt8619;
    
    void method11032(int i, int i_0_) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3876(i, (byte) 0);
	}
    }
    
    void method11033(int i, byte i_1_) {
	anInt8619 = i * 933504141;
	synchronized (aClass203_8618) {
	    aClass203_8618.method3877(-3911454);
	}
    }
    
    void method11034(int i) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3877(1004853237);
	}
    }
    
    void method11035() {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3884((byte) -119);
	}
    }
    
    void method11036(byte i) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3884((byte) -50);
	}
    }
    
    void method11037() {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3877(-1933413470);
	}
    }
    
    void method11038(int i) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3876(i, (byte) 0);
	}
    }
    
    void method11039(int i) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3876(i, (byte) 0);
	}
    }
    
    void method11040(int i) {
	synchronized (aClass203_8618) {
	    aClass203_8618.method3876(i, (byte) 0);
	}
    }
    
    Class670(Class472 class472) {
	aClass472_8617 = class472;
    }
    
    static final void method11041(Class669 class669, byte i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_2_, 1350916029);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_2_ >> 16];
	Class154.method2576(class247, class243, class669, (byte) -122);
    }
    
    static Class534_Sub19 method11042(int i) {
	if (0 == Class534_Sub19.anInt10515 * -1174729025)
	    return new Class534_Sub19();
	return (Class534_Sub19.aClass534_Sub19Array10514
		[(Class534_Sub19.anInt10515 -= -1902157505) * -1174729025]);
    }
    
    public static void method11043(Interface60 interface60, int i) {
	if (null != Class510.anInterface60_5677)
	    throw new IllegalStateException("");
	Class510.anInterface60_5677 = interface60;
    }
}
