/* Class681 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class681 implements Interface76
{
    public static Class681 aClass681_8670 = new Class681(0, "");
    public static Class681 aClass681_8671 = new Class681(1, "");
    int anInt8672;
    
    public int method93() {
	return 1177294667 * anInt8672;
    }
    
    public int method53() {
	return 1177294667 * anInt8672;
    }
    
    public int method22() {
	return 1177294667 * anInt8672;
    }
    
    Class681(int i, String string) {
	anInt8672 = i * 2005154403;
    }
    
    public static Class392 method13865(Class534_Sub40 class534_sub40, byte i) {
	int i_0_ = class534_sub40.method16550((byte) 43);
	Class401 class401 = (Class72.method1560(1999381759)
			     [class534_sub40.method16527(2043191332)]);
	Class391 class391 = (Class705.method14234(608893757)
			     [class534_sub40.method16527(-554879592)]);
	int i_1_ = class534_sub40.method16530((byte) -102);
	int i_2_ = class534_sub40.method16530((byte) -35);
	return new Class392(i_0_, class401, class391, i_1_, i_2_);
    }
    
    static final void method13866(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_3_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_4_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (client.aClass492ArrayArray11027[i_4_][i_3_].anInt5339
	       * 818084131);
    }
    
    public static Class394 method13867(Class534_Sub40 class534_sub40, int i) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -1509764639);
	int i_5_ = class534_sub40.method16533(-258848859);
	int i_6_ = class534_sub40.method16533(-258848859);
	int i_7_ = class534_sub40.method16550((byte) 52);
	return new Class394_Sub2(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 class394.anInt4093 * -1055236307,
				 class394.anInt4097 * -1607607997,
				 -228886179 * class394.anInt4098,
				 -81046249 * class394.anInt4099,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i_5_, i_6_,
				 i_7_);
    }
    
    static final void method13868(Class669 class669, int i) {
	Class166.method2751((class669.anIntArray8595
			     [(class669.anInt8600 -= 308999563) * 2088438307]),
			    1637029150);
    }
}
