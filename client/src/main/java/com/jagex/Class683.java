/* Class683 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Container;
import java.awt.Insets;

public class Class683
{
    Class597 aClass597_8674;
    int anInt8675;
    int anInt8676;
    
    public Class683(Class597 class597, int i, int i_0_, int i_1_) {
	aClass597_8674 = class597;
	anInt8675 = 1832602797 * i;
	anInt8676 = i_1_ * -583340817;
    }
    
    int method13915(int i) {
	return client.anIntArray11136[-526387419 * anInt8675];
    }
    
    public Interface62 method13916(byte i) {
	Interface62 interface62 = method13920(1993692343);
	if (null == interface62)
	    return null;
	if (interface62.method56(-1949000604) != anInt8676 * -68042225)
	    return null;
	return interface62;
    }
    
    public Interface62 method13917() {
	Interface62 interface62 = method13920(636022031);
	if (null == interface62)
	    return null;
	if (interface62.method56(711653551) != anInt8676 * -68042225)
	    return null;
	return interface62;
    }
    
    int method13918() {
	return client.anIntArray11136[-526387419 * anInt8675];
    }
    
    int method13919() {
	return client.anIntArray11136[-526387419 * anInt8675];
    }
    
    Interface62 method13920(int i) {
	int i_2_ = -217302955 * aClass597_8674.anInt7860;
	Class597 class597 = client.aClass512_11100.method8416((byte) 78);
	int i_3_ = (-424199969 * aClass597_8674.anInt7859
		    - class597.anInt7859 * -424199969);
	int i_4_ = (-1166289421 * aClass597_8674.anInt7861
		    - -1166289421 * class597.anInt7861);
	if (i_3_ < 0 || i_4_ < 0
	    || i_3_ >= client.aClass512_11100.method8417(963895565)
	    || i_4_ >= client.aClass512_11100.method8418(-1533611049)
	    || client.aClass512_11100.method8424((byte) 41) == null)
	    return null;
	switch (method13915(-1366116355)) {
	case 0:
	    return ((Interface62)
		    client.aClass512_11100.method8424((byte) 54)
			.method9258(i_2_, i_3_, i_4_, (byte) -111));
	case 3:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 62)
				     .method9264(i_2_, i_3_, i_4_, (byte) 24);
	default:
	    return null;
	case 2:
	    return ((Interface62)
		    (client.aClass512_11100.method8424((byte) 36).method9262
		     (i_2_, i_3_, i_4_, client.anInterface64_11333,
		      (byte) 117)));
	case 1:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 5)
				     .method9357(i_2_, i_3_, i_4_, (byte) -56);
	}
    }
    
    public Interface62 method13921() {
	Interface62 interface62 = method13920(268976057);
	if (null == interface62)
	    return null;
	if (interface62.method56(475139655) != anInt8676 * -68042225)
	    return null;
	return interface62;
    }
    
    Interface62 method13922() {
	int i = -217302955 * aClass597_8674.anInt7860;
	Class597 class597 = client.aClass512_11100.method8416((byte) 115);
	int i_5_ = (-424199969 * aClass597_8674.anInt7859
		    - class597.anInt7859 * -424199969);
	int i_6_ = (-1166289421 * aClass597_8674.anInt7861
		    - -1166289421 * class597.anInt7861);
	if (i_5_ < 0 || i_6_ < 0
	    || i_5_ >= client.aClass512_11100.method8417(-592611191)
	    || i_6_ >= client.aClass512_11100.method8418(-1533611049)
	    || client.aClass512_11100.method8424((byte) 53) == null)
	    return null;
	switch (method13915(-1533582495)) {
	case 0:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 100)
				     .method9258(i, i_5_, i_6_, (byte) -19);
	case 3:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 18)
				     .method9264(i, i_5_, i_6_, (byte) 43);
	default:
	    return null;
	case 2:
	    return ((Interface62)
		    (client.aClass512_11100.method8424((byte) 7).method9262
		     (i, i_5_, i_6_, client.anInterface64_11333, (byte) 97)));
	case 1:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 48)
				     .method9357(i, i_5_, i_6_, (byte) 51);
	}
    }
    
    Interface62 method13923() {
	int i = -217302955 * aClass597_8674.anInt7860;
	Class597 class597 = client.aClass512_11100.method8416((byte) 83);
	int i_7_ = (-424199969 * aClass597_8674.anInt7859
		    - class597.anInt7859 * -424199969);
	int i_8_ = (-1166289421 * aClass597_8674.anInt7861
		    - -1166289421 * class597.anInt7861);
	if (i_7_ < 0 || i_8_ < 0
	    || i_7_ >= client.aClass512_11100.method8417(82785924)
	    || i_8_ >= client.aClass512_11100.method8418(-1533611049)
	    || client.aClass512_11100.method8424((byte) 72) == null)
	    return null;
	switch (method13915(-491947523)) {
	case 0:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 126)
				     .method9258(i, i_7_, i_8_, (byte) -65);
	case 3:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 80)
				     .method9264(i, i_7_, i_8_, (byte) 68);
	default:
	    return null;
	case 2:
	    return ((Interface62)
		    (client.aClass512_11100.method8424((byte) 89).method9262
		     (i, i_7_, i_8_, client.anInterface64_11333, (byte) 37)));
	case 1:
	    return (Interface62) client.aClass512_11100.method8424
				     ((byte) 24)
				     .method9357(i, i_7_, i_8_, (byte) -20);
	}
    }
    
    static final void method13924(Class669 class669, short i)
	throws Exception_Sub2 {
	class669.anInt8600 -= 1235998252;
	int i_9_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_10_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	int i_11_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 2];
	int i_12_
	    = class669.anIntArray8595[3 + class669.anInt8600 * 2088438307];
	Class438 class438
	    = Class438.method6996((float) i_9_, (float) i_10_, (float) i_11_);
	if (class438.aFloat4864 == -1.0F)
	    class438.aFloat4864 = Float.POSITIVE_INFINITY;
	if (class438.aFloat4863 == -1.0F)
	    class438.aFloat4863 = Float.POSITIVE_INFINITY;
	if (class438.aFloat4865 == -1.0F)
	    class438.aFloat4865 = Float.POSITIVE_INFINITY;
	Class599.aClass298_Sub1_7871.method5454(class438, -1127503483);
	Class599.aClass298_Sub1_7871.method5365((float) i_12_ / 1000.0F,
						31645619);
	class438.method7074();
    }
    
    public static void method13925(int i) {
	Container container = Class171.method2880((short) -1880);
	int i_13_ = container.getSize().width;
	int i_14_ = container.getSize().height;
	if (container == Class452.aFrame4943) {
	    Insets insets = Class452.aFrame4943.getInsets();
	    i_13_ -= insets.right + insets.left;
	    i_14_ -= insets.top + insets.bottom;
	}
	if (-166028671 * Class391.anInt4076 != i_13_
	    || i_14_ != 8272787 * client.anInt5561 || client.aBool11059) {
	    if (Class254.aClass185_2683 == null
		|| Class254.aClass185_2683.method3242())
		Class244.method4485(-2066945729);
	    else {
		Class391.anInt4076 = 1199809921 * i_13_;
		client.anInt5561 = i_14_ * 226957979;
	    }
	    client.aLong11182 = ((Class250.method4604((byte) -101) + 500L)
				 * -92619617144807657L);
	    client.aBool11059 = false;
	}
    }
    
    static final void method13926(Class669 class669, int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 0,
	     1605614742);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
}
