/* Class690_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub16 extends Class690
{
    public static final int anInt10894 = 1;
    static final int anInt10895 = 2;
    public static final int anInt10896 = 0;
    
    public int method17026() {
	return 189295939 * anInt8753;
    }
    
    public Class690_Sub16(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method17027(int i) {
	if (aClass534_Sub35_8752.method16441(-38392842)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 2)
		anInt8753 = method14017(2094812109) * 1823770475;
	} else if (anInt8753 * 189295939 != 0 && 189295939 * anInt8753 != 1)
	    anInt8753 = method14017(2146691598) * 1823770475;
    }
    
    int method14017(int i) {
	return 1;
    }
    
    public boolean method17028(int i) {
	if (aClass534_Sub35_8752.method16441(1133080911)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public int method14026(int i, int i_0_) {
	if (aClass534_Sub35_8752.method16441(-135958514)
	    == Class675.aClass675_8634) {
	    if (i == 0 || aClass534_Sub35_8752.aClass690_Sub25_10750
			      .method17102((byte) -93) == 1)
		return 1;
	    return 2;
	}
	return 3;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    public void method17029() {
	if (aClass534_Sub35_8752.method16441(-129658794)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 2)
		anInt8753 = method14017(2130350954) * 1823770475;
	} else if (anInt8753 * 189295939 != 0 && 189295939 * anInt8753 != 1)
	    anInt8753 = method14017(2092980309) * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method17030(byte i) {
	return 189295939 * anInt8753;
    }
    
    int method14018() {
	return 1;
    }
    
    public boolean method17031() {
	if (aClass534_Sub35_8752.method16441(-389313637)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    int method14021() {
	return 1;
    }
    
    public Class690_Sub16(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.method16441(-263947388)
	    == Class675.aClass675_8634) {
	    if (i == 0 || aClass534_Sub35_8752.aClass690_Sub25_10750
			      .method17102((byte) -100) == 1)
		return 1;
	    return 2;
	}
	return 3;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.method16441(-1532455303)
	    == Class675.aClass675_8634) {
	    if (i == 0 || aClass534_Sub35_8752.aClass690_Sub25_10750
			      .method17102((byte) -42) == 1)
		return 1;
	    return 2;
	}
	return 3;
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.method16441(-142377266)
	    == Class675.aClass675_8634) {
	    if (i == 0 || aClass534_Sub35_8752.aClass690_Sub25_10750
			      .method17102((byte) -20) == 1)
		return 1;
	    return 2;
	}
	return 3;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.method16441(-1459122778)
	    == Class675.aClass675_8634) {
	    if (i == 0 || aClass534_Sub35_8752.aClass690_Sub25_10750
			      .method17102((byte) -77) == 1)
		return 1;
	    return 2;
	}
	return 3;
    }
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public void method17032() {
	if (aClass534_Sub35_8752.method16441(-493638896)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 2)
		anInt8753 = method14017(2110855006) * 1823770475;
	} else if (anInt8753 * 189295939 != 0 && 189295939 * anInt8753 != 1)
	    anInt8753 = method14017(2094811892) * 1823770475;
    }
    
    int method14022() {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    static final void method17033(Class669 class669, int i) {
	System.out.println(class669.anObjectArray8593
			   [(class669.anInt8594 -= 1460193483) * 1485266147]);
    }
}
