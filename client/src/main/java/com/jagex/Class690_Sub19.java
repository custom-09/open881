/* Class690_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub19 extends Class690
{
    public static final int anInt10905 = 0;
    public static final int anInt10906 = 1;
    static final int anInt10907 = 3;
    
    int method14022() {
	return 0;
    }
    
    public Class690_Sub19(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public int method14029(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1410574098))
	    return 3;
	return 1;
    }
    
    int method14017(int i) {
	return 0;
    }
    
    public boolean method17051(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1401660058))
	    return false;
	return true;
    }
    
    public int method14026(int i, int i_0_) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1654690064))
	    return 3;
	return 1;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub19(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14021() {
	return 0;
    }
    
    public void method17052() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   -1305433062))
	    anInt8753 = 0;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 3)
		anInt8753 = method14017(2110829170) * 1823770475;
	} else if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2104128998) * 1823770475;
    }
    
    int method14018() {
	return 0;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17053(byte i) {
	return 189295939 * anInt8753;
    }
    
    public int method14027(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1563783403))
	    return 3;
	return 1;
    }
    
    public int method14028(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1632675005))
	    return 3;
	return 1;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method14030(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-280911555))
	    return 3;
	return 1;
    }
    
    public void method17054() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   875419687))
	    anInt8753 = 0;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 3)
		anInt8753 = method14017(2141948683) * 1823770475;
	} else if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2127537549) * 1823770475;
    }
    
    public void method17055(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   1976797367))
	    anInt8753 = 0;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485)) {
	    if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 3)
		anInt8753 = method14017(2103301543) * 1823770475;
	} else if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2128083338) * 1823770475;
    }
    
    public boolean method17056() {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					39043239))
	    return false;
	return true;
    }
}
