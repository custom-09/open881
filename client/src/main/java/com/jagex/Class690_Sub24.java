/* Class690_Sub24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.File;

public class Class690_Sub24 extends Class690
{
    static final int anInt10920 = 2;
    public static final int anInt10921 = 1;
    public static final int anInt10922 = 0;
    
    int method14029(int i) {
	return 1;
    }
    
    public Class690_Sub24(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public void method17093(int i) {
	if (aClass534_Sub35_8752.method16441(-622016967)
	    == Class675.aClass675_8635)
	    anInt8753 = -647426346;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2094127389) * 1823770475;
    }
    
    int method14028(int i) {
	return 1;
    }
    
    public Class690_Sub24(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    int method14022() {
	return 1;
    }
    
    public int method17094(int i) {
	return anInt8753 * 189295939;
    }
    
    int method14021() {
	return 1;
    }
    
    public void method17095() {
	if (aClass534_Sub35_8752.method16441(-1955686743)
	    == Class675.aClass675_8635)
	    anInt8753 = -647426346;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2133066324) * 1823770475;
    }
    
    int method14018() {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    int method14027(int i) {
	return 1;
    }
    
    public void method17096() {
	if (aClass534_Sub35_8752.method16441(838431712)
	    == Class675.aClass675_8635)
	    anInt8753 = -647426346;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2110291738) * 1823770475;
    }
    
    public void method17097() {
	if (aClass534_Sub35_8752.method16441(-399890146)
	    == Class675.aClass675_8635)
	    anInt8753 = -647426346;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2134189218) * 1823770475;
    }
    
    int method14030(int i) {
	return 1;
    }
    
    void method14020(int i, int i_0_) {
	anInt8753 = i * 1823770475;
    }
    
    int method14026(int i, int i_1_) {
	return 1;
    }
    
    int method14017(int i) {
	return 1;
    }
    
    public int method17098() {
	return anInt8753 * 189295939;
    }
    
    public static byte[] method17099(File file, int i) {
	return Class113.method2073(file, (int) file.length(), (byte) 14);
    }
    
    static final void method17100(Class247 class247, Class243 class243,
				  Class669 class669, int i) {
	class247.anInt2496 = -642498273;
	class247.anInt2497 = client.anInt11037 * 2118781167;
	class247.anInt2550 = 0;
	if (-1 == class247.anInt2580 * 1365669833 && !class243.aBool2413)
	    Class626.method10366(-1278450723 * class247.anInt2454, 1774966668);
    }
}
