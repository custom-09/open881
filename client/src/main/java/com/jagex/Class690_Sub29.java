/* Class690_Sub29 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub29 extends Class690
{
    static final int anInt10931 = -3;
    static final int anInt10932 = -2;
    
    public int method17134() {
	return 189295939 * anInt8753;
    }
    
    public Class690_Sub29(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method17135(byte i) {
	if (anInt8753 * 189295939 < -3)
	    anInt8753 = method14017(2130940184) * 1823770475;
    }
    
    int method14017(int i) {
	return -2;
    }
    
    int method14026(int i, int i_0_) {
	return 3;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14022() {
	return -2;
    }
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public void method17136() {
	if (anInt8753 * 189295939 < -3)
	    anInt8753 = method14017(2105479075) * 1823770475;
    }
    
    int method14018() {
	return -2;
    }
    
    int method14028(int i) {
	return 3;
    }
    
    public void method17137() {
	if (anInt8753 * 189295939 < -3)
	    anInt8753 = method14017(2096587411) * 1823770475;
    }
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14027(int i) {
	return 3;
    }
    
    public Class690_Sub29(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14029(int i) {
	return 3;
    }
    
    public int method17138(int i) {
	return 189295939 * anInt8753;
    }
    
    int method14030(int i) {
	return 3;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14021() {
	return -2;
    }
    
    static final boolean method17139(Class44_Sub13 class44_sub13, int i,
				     int i_2_, byte i_3_) {
	Class602 class602 = (Class602) class44_sub13.method91(i, 453245712);
	if (i_2_ == 11)
	    i_2_ = 10;
	if (i_2_ >= 5 && i_2_ <= 8)
	    i_2_ = 4;
	return class602.method10000(i_2_, (byte) 118);
    }
}
