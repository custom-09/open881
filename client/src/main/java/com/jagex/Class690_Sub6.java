/* Class690_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub6 extends Class690
{
    public static final int anInt10865 = 2;
    public static final int anInt10866 = 1;
    public static final int anInt10867 = 0;
    
    public int method16918() {
	return anInt8753 * 189295939;
    }
    
    public Class690_Sub6(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method16919(byte i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 67)
	    == 0)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2117773843) * 1823770475;
    }
    
    public int method16920() {
	return anInt8753 * 189295939;
    }
    
    public boolean method16921(byte i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) -87)
	    == 0)
	    return false;
	return true;
    }
    
    public int method14026(int i, int i_0_) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 48)
	    == 0)
	    return 3;
	return 1;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 12)
	    == 0)
	    return 3;
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    int method14022() {
	return 2;
    }
    
    int method14018() {
	return 2;
    }
    
    public boolean method16922() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) -49)
	    == 0)
	    return false;
	return true;
    }
    
    public int method16923(byte i) {
	return anInt8753 * 189295939;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    int method14021() {
	return 2;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 79)
	    == 0)
	    return 3;
	return 1;
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) -47)
	    == 0)
	    return 3;
	return 1;
    }
    
    int method14017(int i) {
	return 2;
    }
    
    public void method16924() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 24)
	    == 0)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2111191474) * 1823770475;
    }
    
    public void method16925() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 23)
	    == 0)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2099858474) * 1823770475;
    }
    
    public void method16926() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 29)
	    == 0)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2109312582) * 1823770475;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 8)
	    == 0)
	    return 3;
	return 1;
    }
    
    public boolean method16927() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) 4)
	    == 0)
	    return false;
	return true;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public boolean method16928() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) -2)
	    == 0)
	    return false;
	return true;
    }
    
    public int method16929() {
	return anInt8753 * 189295939;
    }
    
    public Class690_Sub6(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public boolean method16930() {
	if (aClass534_Sub35_8752.aClass690_Sub16_10763.method17030((byte) -26)
	    == 0)
	    return false;
	return true;
    }
    
    public int method16931() {
	return anInt8753 * 189295939;
    }
    
    public int method16932() {
	return anInt8753 * 189295939;
    }
}
