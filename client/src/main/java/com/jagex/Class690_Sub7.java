/* Class690_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub7 extends Class690
{
    boolean aBool10868 = true;
    public boolean aBool10869 = false;
    
    int method14017(int i) {
	aBool10869 = true;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14116(1976779752))
	    return 3;
	return 1;
    }
    
    public boolean method16933(byte i) {
	return true;
    }
    
    boolean method16934() {
	return aBool10868;
    }
    
    public Class690_Sub7(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public int method14030(int i) {
	if (i == 3 && !Class112.method2018(-1327590673).method397("jagdx",
								  1283709043))
	    return 3;
	return 2;
    }
    
    public int method14026(int i, int i_0_) {
	if (i == 3 && !Class112.method2018(-1327590673).method397("jagdx",
								  1251791245))
	    return 3;
	return 2;
    }
    
    void method14020(int i, int i_1_) {
	aBool10869 = false;
	anInt8753 = 1823770475 * i;
    }
    
    public int method16935(int i) {
	return anInt8753 * 189295939;
    }
    
    public void method16936(byte i) {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 5
	    || 2 == anInt8753 * 189295939)
	    anInt8753 = method14017(2091866440) * 1823770475;
    }
    
    public void method16937(boolean bool, int i) {
	aBool10868 = bool;
    }
    
    int method14021() {
	aBool10869 = true;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14116(1942010095))
	    return 3;
	return 1;
    }
    
    public Class690_Sub7(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public int method14027(int i) {
	if (i == 3
	    && !Class112.method2018(-1327590673).method397("jagdx", 935180245))
	    return 3;
	return 2;
    }
    
    void method14024(int i) {
	aBool10869 = false;
	anInt8753 = 1823770475 * i;
    }
    
    boolean method16938(int i) {
	return aBool10868;
    }
    
    void method14023(int i) {
	aBool10869 = false;
	anInt8753 = 1823770475 * i;
    }
    
    void method14025(int i) {
	aBool10869 = false;
	anInt8753 = 1823770475 * i;
    }
    
    public int method14028(int i) {
	if (i == 3 && !Class112.method2018(-1327590673).method397("jagdx",
								  1962300226))
	    return 3;
	return 2;
    }
    
    public int method14029(int i) {
	if (i == 3 && !Class112.method2018(-1327590673).method397("jagdx",
								  1011075230))
	    return 3;
	return 2;
    }
    
    int method14018() {
	aBool10869 = true;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14116(2061311592))
	    return 3;
	return 1;
    }
    
    public void method16939() {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 5
	    || 2 == anInt8753 * 189295939)
	    anInt8753 = method14017(2102185773) * 1823770475;
    }
    
    public boolean method16940() {
	return true;
    }
    
    public int method16941() {
	return anInt8753 * 189295939;
    }
    
    public int method16942() {
	return anInt8753 * 189295939;
    }
    
    int method14022() {
	aBool10869 = true;
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14116(1929824453))
	    return 3;
	return 1;
    }
    
    public void method16943(boolean bool) {
	aBool10868 = bool;
    }
    
    public void method16944(boolean bool) {
	aBool10868 = bool;
    }
    
    static final void method16945(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_2_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_3_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (Class618.aClass458_8101.method7433(i_2_, -1203343574)
	       .anIntArray11893[i_3_]);
    }
    
    public static String method16946(long l, int i, int i_4_) {
	Class699.method14128(l);
	int i_5_ = Class84.aCalendar838.get(11);
	int i_6_ = Class84.aCalendar838.get(12);
	int i_7_ = Class84.aCalendar838.get(13);
	return new StringBuilder().append(Integer.toString(i_5_ / 10)).append
		   (i_5_ % 10).append
		   (":").append
		   (i_6_ / 10).append
		   (i_6_ % 10).append
		   (":").append
		   (i_7_ / 10).append
		   (i_7_ % 10).toString();
    }
}
