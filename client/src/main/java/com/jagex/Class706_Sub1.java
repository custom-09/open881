/* Class706_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class706_Sub1 extends Class706
{
    Class438 aClass438_10917 = new Class438(Float.NaN, Float.NaN, Float.NaN);
    Class438 aClass438_10918 = new Class438(Float.NaN, Float.NaN, Float.NaN);
    Class438 aClass438_10919 = new Class438();
    
    public Class438 method14247() {
	return Class438.method6994(aClass438_10917);
    }
    
    public void method17090(Class534_Sub36 class534_sub36, int i) {
	aClass438_10918.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10918.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10918.aFloat4865
	    = (float) (33298755 * class534_sub36.anInt10799);
	if (Float.isNaN(aClass438_10917.aFloat4864)) {
	    aClass438_10917.method6992(aClass438_10918);
	    aClass438_10919.method6999();
	}
    }
    
    public void method14239(float f, int i) {
	aClass298_8844.method5352(false, f, aClass438_10917,
				  aClass298_8844.method5391((byte) 62),
				  aClass438_10918, aClass438_10919,
				  (byte) -72);
    }
    
    public boolean method14237(byte i) {
	return !Float.isNaN(aClass438_10917.aFloat4864);
    }
    
    public void method14240(Class287 class287, Class446 class446, int i,
			    int i_0_, float f, int i_1_) {
	Class438 class438
	    = Class438.method6994(aClass298_8844.method5385((byte) 124));
	class438.aFloat4864 -= (float) i;
	class438.aFloat4865 -= (float) i_0_;
	class438.aFloat4863 *= -1.0F;
	Class438 class438_2_ = Class438.method6994(aClass438_10917);
	class438_2_.aFloat4864 -= (float) i;
	class438_2_.aFloat4865 -= (float) i_0_;
	class438_2_.aFloat4863 *= -1.0F;
	class446.method7241((double) class438.aFloat4864,
			    (double) class438.aFloat4863,
			    (double) class438.aFloat4865,
			    (double) class438_2_.aFloat4864,
			    (double) class438_2_.aFloat4863,
			    (double) class438_2_.aFloat4865, 0.0F, 1.0F, 0.0F);
	class438.method7074();
	class438_2_.method7074();
    }
    
    public Class706_Sub1(Class298 class298) {
	super(class298);
    }
    
    public Class438 method14248(int i) {
	return Class438.method6994(aClass438_10918);
    }
    
    public void method14235(Class534_Sub40 class534_sub40, int i) {
	aClass438_10918.method7080(class534_sub40);
    }
    
    public Class438 method14249() {
	return Class438.method6994(aClass438_10918);
    }
    
    public void method17091(Class534_Sub36 class534_sub36) {
	aClass438_10918.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10918.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10918.aFloat4865
	    = (float) (33298755 * class534_sub36.anInt10799);
	if (Float.isNaN(aClass438_10917.aFloat4864)) {
	    aClass438_10917.method6992(aClass438_10918);
	    aClass438_10919.method6999();
	}
    }
    
    public void method14251(Class534_Sub40 class534_sub40) {
	aClass438_10918.method7080(class534_sub40);
    }
    
    public void method14238(float f) {
	aClass298_8844.method5352(false, f, aClass438_10917,
				  aClass298_8844.method5391((byte) 54),
				  aClass438_10918, aClass438_10919,
				  (byte) -37);
    }
    
    public void method14244(float f) {
	aClass298_8844.method5352(false, f, aClass438_10917,
				  aClass298_8844.method5391((byte) 9),
				  aClass438_10918, aClass438_10919,
				  (byte) -101);
    }
    
    public boolean method14245() {
	return !Float.isNaN(aClass438_10917.aFloat4864);
    }
    
    public void method14246(Class287 class287, Class446 class446, int i,
			    int i_3_, float f) {
	Class438 class438
	    = Class438.method6994(aClass298_8844.method5385((byte) 120));
	class438.aFloat4864 -= (float) i;
	class438.aFloat4865 -= (float) i_3_;
	class438.aFloat4863 *= -1.0F;
	Class438 class438_4_ = Class438.method6994(aClass438_10917);
	class438_4_.aFloat4864 -= (float) i;
	class438_4_.aFloat4865 -= (float) i_3_;
	class438_4_.aFloat4863 *= -1.0F;
	class446.method7241((double) class438.aFloat4864,
			    (double) class438.aFloat4863,
			    (double) class438.aFloat4865,
			    (double) class438_4_.aFloat4864,
			    (double) class438_4_.aFloat4863,
			    (double) class438_4_.aFloat4865, 0.0F, 1.0F, 0.0F);
	class438.method7074();
	class438_4_.method7074();
    }
    
    public void method14242(float f) {
	aClass298_8844.method5352(false, f, aClass438_10917,
				  aClass298_8844.method5391((byte) 106),
				  aClass438_10918, aClass438_10919, (byte) 2);
    }
    
    public Class438 method14243() {
	return Class438.method6994(aClass438_10917);
    }
    
    public Class438 method14241() {
	return Class438.method6994(aClass438_10917);
    }
    
    public Class438 method14236(int i) {
	return Class438.method6994(aClass438_10917);
    }
    
    public Class438 method14250() {
	return Class438.method6994(aClass438_10918);
    }
    
    public void method17092(Class534_Sub36 class534_sub36) {
	aClass438_10918.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10918.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10918.aFloat4865
	    = (float) (33298755 * class534_sub36.anInt10799);
	if (Float.isNaN(aClass438_10917.aFloat4864)) {
	    aClass438_10917.method6992(aClass438_10918);
	    aClass438_10919.method6999();
	}
    }
}
