/* Class706_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class706_Sub2 extends Class706
{
    Class347_Sub3 aClass347_Sub3_10973;
    Class434 aClass434_10974 = null;
    
    public Class438 method14241() {
	Class438 class438 = Class438.method7061();
	double[] ds
	    = aClass434_10974
		  .method6971(aClass347_Sub3_10973.method15861((byte) -117));
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public Class706_Sub2(Class298 class298) {
	super(class298);
	aClass347_Sub3_10973 = null;
    }
    
    public boolean method14237(byte i) {
	return (null != aClass434_10974 && aClass347_Sub3_10973 != null
		&& aClass347_Sub3_10973.method6131(334646705));
    }
    
    public Class438 method14236(int i) {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434_10974.method6971(aClass347_Sub3_10973
						     .method15861((byte) -2));
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    double[] method17258(byte i) {
	return aClass434_10974
		   .method6971(aClass347_Sub3_10973.method15861((byte) -30));
    }
    
    public Class438 method14248(int i) {
	return method14236(308999563);
    }
    
    public Class438 method14247() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434_10974.method6971(aClass347_Sub3_10973
						     .method15861((byte) -55));
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public void method14235(Class534_Sub40 class534_sub40, int i) {
	aClass434_10974 = new Class434(class534_sub40);
    }
    
    public void method14242(float f) {
	/* empty */
    }
    
    public Class438 method14249() {
	return method14236(308999563);
    }
    
    public void method14244(float f) {
	/* empty */
    }
    
    public boolean method14245() {
	return (null != aClass434_10974 && aClass347_Sub3_10973 != null
		&& aClass347_Sub3_10973.method6131(-1799597759));
    }
    
    public void method14246(Class287 class287, Class446 class446, int i,
			    int i_0_, float f) {
	double[] ds = aClass298_8844.method5386(-885556439);
	ds[0] -= (double) i;
	ds[2] -= (double) i_0_;
	double[] ds_1_ = method17258((byte) -73);
	ds_1_[0] -= (double) i;
	ds_1_[2] -= (double) i_0_;
	ds[1] *= -1.0;
	ds_1_[1] *= -1.0;
	Class438 class438 = Class438.method7061();
	class438.aFloat4864 = (float) (ds_1_[0] - ds[0]);
	class438.aFloat4863 = (float) (ds_1_[1] - ds[1]);
	class438.aFloat4865 = (float) (ds_1_[2] - ds[2]);
	class438.method7002();
	Class443 class443 = new Class443();
	class443.method7146(class438, f);
	Class438 class438_2_ = Class438.method6996(0.0F, 1.0F, 0.0F);
	Class438 class438_3_ = Class438.method7019(class438, class438_2_);
	class438_2_ = Class438.method7019(class438_3_, class438);
	class438_2_.method7021(class443);
	class446.method7241(ds[0], ds[1], ds[2], ds_1_[0], ds_1_[1], ds_1_[2],
			    class438_2_.aFloat4864, class438_2_.aFloat4863,
			    class438_2_.aFloat4865);
	class438_2_.method7074();
    }
    
    public void method14240(Class287 class287, Class446 class446, int i,
			    int i_4_, float f, int i_5_) {
	double[] ds = aClass298_8844.method5386(-671235389);
	ds[0] -= (double) i;
	ds[2] -= (double) i_4_;
	double[] ds_6_ = method17258((byte) -119);
	ds_6_[0] -= (double) i;
	ds_6_[2] -= (double) i_4_;
	ds[1] *= -1.0;
	ds_6_[1] *= -1.0;
	Class438 class438 = Class438.method7061();
	class438.aFloat4864 = (float) (ds_6_[0] - ds[0]);
	class438.aFloat4863 = (float) (ds_6_[1] - ds[1]);
	class438.aFloat4865 = (float) (ds_6_[2] - ds[2]);
	class438.method7002();
	Class443 class443 = new Class443();
	class443.method7146(class438, f);
	Class438 class438_7_ = Class438.method6996(0.0F, 1.0F, 0.0F);
	Class438 class438_8_ = Class438.method7019(class438, class438_7_);
	class438_7_ = Class438.method7019(class438_8_, class438);
	class438_7_.method7021(class443);
	class446.method7241(ds[0], ds[1], ds[2], ds_6_[0], ds_6_[1], ds_6_[2],
			    class438_7_.aFloat4864, class438_7_.aFloat4863,
			    class438_7_.aFloat4865);
	class438_7_.method7074();
    }
    
    public Class438 method14243() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434_10974.method6971(aClass347_Sub3_10973
						     .method15861((byte) -54));
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public void method14238(float f) {
	/* empty */
    }
    
    public void method14239(float f, int i) {
	/* empty */
    }
    
    public Class438 method14250() {
	return method14236(308999563);
    }
    
    public void method14251(Class534_Sub40 class534_sub40) {
	aClass434_10974 = new Class434(class534_sub40);
    }
    
    double[] method17259() {
	return aClass434_10974
		   .method6971(aClass347_Sub3_10973.method15861((byte) -85));
    }
    
    static final void method17260(Class669 class669, int i)
	throws Exception_Sub2 {
	class669.anInt8600 -= 617999126;
	int i_9_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	Class438 class438
	    = Class438.method6996((float) i_9_, (float) i_9_, (float) i_9_);
	Class599.aClass298_Sub1_7871.method5410(class438, -1301917969);
	class438.method7074();
    }
}
