/* Class709 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class709
{
    Class536 aClass536_8855;
    Class536 aClass536_8856 = new Class536();
    public static int anInt8857;
    
    public Class536 method14282(int i) {
	Class536 class536 = aClass536_8855;
	if (aClass536_8856 == class536) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public void method14283(int i) {
	for (;;) {
	    Class536 class536 = aClass536_8856.aClass536_7164;
	    if (class536 == aClass536_8856)
		break;
	    class536.method8900(-1015196452);
	}
	aClass536_8855 = null;
    }
    
    public void method14284() {
	for (;;) {
	    Class536 class536 = aClass536_8856.aClass536_7164;
	    if (class536 == aClass536_8856)
		break;
	    class536.method8900(1709839284);
	}
	aClass536_8855 = null;
    }
    
    public void method14285(Class536 class536, byte i) {
	if (class536.aClass536_7163 != null)
	    class536.method8900(660520691);
	class536.aClass536_7163 = aClass536_8856;
	class536.aClass536_7164 = aClass536_8856.aClass536_7164;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public Class709() {
	aClass536_8856.aClass536_7164 = aClass536_8856;
	aClass536_8856.aClass536_7163 = aClass536_8856;
    }
    
    public Class536 method14286() {
	Class536 class536 = aClass536_8855;
	if (aClass536_8856 == class536) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public void method14287(Class536 class536, int i) {
	if (null != class536.aClass536_7163)
	    class536.method8900(124341651);
	class536.aClass536_7163 = aClass536_8856.aClass536_7163;
	class536.aClass536_7164 = aClass536_8856;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public boolean method14288(int i) {
	return aClass536_8856 == aClass536_8856.aClass536_7164;
    }
    
    public void method14289() {
	for (;;) {
	    Class536 class536 = aClass536_8856.aClass536_7164;
	    if (class536 == aClass536_8856)
		break;
	    class536.method8900(1398811570);
	}
	aClass536_8855 = null;
    }
    
    public Class536 method14290(int i) {
	Class536 class536 = aClass536_8856.aClass536_7164;
	if (aClass536_8856 == class536)
	    return null;
	class536.method8900(-1381820434);
	return class536;
    }
    
    public Class536 method14291() {
	Class536 class536 = aClass536_8856.aClass536_7164;
	if (aClass536_8856 == class536)
	    return null;
	class536.method8900(1086850612);
	return class536;
    }
    
    public void method14292(Class536 class536) {
	if (null != class536.aClass536_7163)
	    class536.method8900(-1877941841);
	class536.aClass536_7163 = aClass536_8856.aClass536_7163;
	class536.aClass536_7164 = aClass536_8856;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public void method14293(Class536 class536) {
	if (null != class536.aClass536_7163)
	    class536.method8900(-589421169);
	class536.aClass536_7163 = aClass536_8856.aClass536_7163;
	class536.aClass536_7164 = aClass536_8856;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public void method14294(Class536 class536) {
	if (class536.aClass536_7163 != null)
	    class536.method8900(-1576352805);
	class536.aClass536_7163 = aClass536_8856;
	class536.aClass536_7164 = aClass536_8856.aClass536_7164;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public static void method14295(Class536 class536, Class536 class536_0_) {
	if (class536.aClass536_7163 != null)
	    class536.method8900(-1800631431);
	class536.aClass536_7163 = class536_0_;
	class536.aClass536_7164 = class536_0_.aClass536_7164;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public static void method14296(Class536 class536, Class536 class536_1_) {
	if (class536.aClass536_7163 != null)
	    class536.method8900(1913784020);
	class536.aClass536_7163 = class536_1_;
	class536.aClass536_7164 = class536_1_.aClass536_7164;
	class536.aClass536_7163.aClass536_7164 = class536;
	class536.aClass536_7164.aClass536_7163 = class536;
    }
    
    public Class536 method14297() {
	Class536 class536 = aClass536_8855;
	if (aClass536_8856 == class536) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public Class536 method14298() {
	Class536 class536 = aClass536_8856.aClass536_7164;
	if (class536 == aClass536_8856) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public Class536 method14299() {
	Class536 class536 = aClass536_8856.aClass536_7164;
	if (class536 == aClass536_8856) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public Class536 method14300() {
	Class536 class536 = aClass536_8855;
	if (aClass536_8856 == class536) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public Class536 method14301(int i) {
	Class536 class536 = aClass536_8856.aClass536_7164;
	if (class536 == aClass536_8856) {
	    aClass536_8855 = null;
	    return null;
	}
	aClass536_8855 = class536.aClass536_7164;
	return class536;
    }
    
    public void method14302() {
	for (;;) {
	    Class536 class536 = aClass536_8856.aClass536_7164;
	    if (class536 == aClass536_8856)
		break;
	    class536.method8900(2125418453);
	}
	aClass536_8855 = null;
    }
    
    public boolean method14303() {
	return aClass536_8856 == aClass536_8856.aClass536_7164;
    }
    
    static final void method14304(Class669 class669, int i) {
	String string;
	if (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419 != null
	    && (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aString12228
		!= null))
	    string = Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			 .method18882(false, -743416097);
	else
	    string = "";
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = string;
    }
    
    public static int method14305(int i) {
	if (Class534_Sub25.aTwitchEventLiveStreams10576 == null
	    || (-297069345 * Class574.anInt7703
		>= (Class534_Sub25.aTwitchEventLiveStreams10576.streamCount
		    - 1)))
	    return -1;
	return (Class574.anInt7703 += 202745631) * -297069345;
    }
}
