/* Class711_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class711_Sub1 extends Class711
{
    public boolean aBool10971 = false;
    Class654_Sub1_Sub5_Sub1 aClass654_Sub1_Sub5_Sub1_10972;
    
    void method14352(Class205 class205, int i) {
	if (!aBool10971
	    || !aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		    .method14338((byte) 0)
	    || aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		   .method14336(1907912825))
	    Class171_Sub4.aClass232_9944.method4212
		(class205, i, aClass654_Sub1_Sub5_Sub1_10972, 1146579524);
    }
    
    Class711_Sub1(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1,
		  boolean bool) {
	super(bool);
	aClass654_Sub1_Sub5_Sub1_10972 = class654_sub1_sub5_sub1;
    }
    
    void method14365(Class205 class205, int i) {
	if (!aBool10971
	    || !aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		    .method14338((byte) 0)
	    || aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		   .method14336(1656369027))
	    Class171_Sub4.aClass232_9944.method4212
		(class205, i, aClass654_Sub1_Sub5_Sub1_10972, 861811592);
    }
    
    void method14353(Class205 class205, int i) {
	if (!aBool10971
	    || !aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		    .method14338((byte) 0)
	    || aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		   .method14336(1916190024))
	    Class171_Sub4.aClass232_9944.method4212
		(class205, i, aClass654_Sub1_Sub5_Sub1_10972, 1747335161);
    }
    
    void method14349(Class205 class205, int i, int i_0_) {
	if (!aBool10971
	    || !aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		    .method14338((byte) 0)
	    || aClass654_Sub1_Sub5_Sub1_10972.aClass711_11948
		   .method14336(2139567468))
	    Class171_Sub4.aClass232_9944.method4212
		(class205, i, aClass654_Sub1_Sub5_Sub1_10972, -897632196);
    }
}
