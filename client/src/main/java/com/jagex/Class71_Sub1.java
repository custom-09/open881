/* Class71_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class71_Sub1 extends Class71 implements Interface68
{
    public void method356(int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4207,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(anInt751 * 1458967645, 945877101);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt745 * -1933466331, 438132545);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt748 * -1597989303, -286544616);
	int i_0_ = (aBool746 ? 2 : 0) | (aBool750 ? 1 : 0);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16560(i_0_,
							      -1810311640);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(2029722599 * anInt744, 2071590599);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(-1844398003 * anInt747, 1364030803);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(-1721981813 * anInt752, 1256450971);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(anInt749 * -1689693861, 2090017387);
	client.aClass100_11264.method1863(class534_sub19, (byte) 117);
    }
    
    public void method142() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4207,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(anInt751 * 1458967645, -712776987);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt745 * -1933466331, 756008826);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt748 * -1597989303, -986023789);
	int i = (aBool746 ? 2 : 0) | (aBool750 ? 1 : 0);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16560(i, 216052571);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(2029722599 * anInt744, 150698216);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(-1844398003 * anInt747, 34347801);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(-1721981813 * anInt752, 176564877);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(anInt749 * -1689693861, 1078492595);
	client.aClass100_11264.method1863(class534_sub19, (byte) 15);
    }
    
    public void method286() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4207,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(anInt751 * 1458967645, 2061160817);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt745 * -1933466331, -803182682);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt748 * -1597989303, 73032739);
	int i = (aBool746 ? 2 : 0) | (aBool750 ? 1 : 0);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16560(i, 819460479);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(2029722599 * anInt744, 296906324);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(-1844398003 * anInt747, 1002485346);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(-1721981813 * anInt752, 475401014);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(anInt749 * -1689693861, 1881147797);
	client.aClass100_11264.method1863(class534_sub19, (byte) 106);
    }
    
    public void method203() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4207,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(anInt751 * 1458967645, 1616937446);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt745 * -1933466331, 307639729);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt748 * -1597989303, -823934606);
	int i = (aBool746 ? 2 : 0) | (aBool750 ? 1 : 0);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16560(i, 604117297);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(2029722599 * anInt744, 1091106662);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(-1844398003 * anInt747, 1680365347);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(-1721981813 * anInt752, 867385608);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(anInt749 * -1689693861, 1082702500);
	client.aClass100_11264.method1863(class534_sub19, (byte) 59);
    }
    
    public void method439() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4207,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(anInt751 * 1458967645, -1720714526);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt745 * -1933466331, -565791498);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16579(anInt748 * -1597989303, 1457425895);
	int i = (aBool746 ? 2 : 0) | (aBool750 ? 1 : 0);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16560(i, 824185793);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(2029722599 * anInt744, 535141750);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16568(-1844398003 * anInt747, -417712119);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16560(-1721981813 * anInt752, -610310911);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16635(anInt749 * -1689693861, 988496949);
	client.aClass100_11264.method1863(class534_sub19, (byte) 70);
    }
    
    Class71_Sub1(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
		 boolean bool, boolean bool_6_, int i_7_) {
	super(i, i_1_, i_2_ > 65535 ? 65535 : i_2_, i_3_,
	      i_4_ > 255 ? 255 : i_4_, i_5_, bool, bool_6_,
	      i_7_ > 255 ? 255 : i_7_);
    }
    
    static final void method16306(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_8_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	boolean bool
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600 + 1]
	       == 1);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub7_10764),
						       i_8_, -1062574356);
	if (!bool)
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 0,
		 -377149727);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
}
