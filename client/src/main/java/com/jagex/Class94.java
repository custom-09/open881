/* Class94 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class94
{
    public int anInt902;
    public int anInt903;
    public int anInt904;
    
    Class94(int i, int i_0_, int i_1_) {
	anInt904 = 1072993355 * i;
	anInt903 = i_0_ * 1862182055;
	anInt902 = i_1_ * -594256357;
    }
    
    static final void method1763(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_2_, 1728697605);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_2_ >> 16];
	Class534_Sub20.method16195(class247, class243, true, 2, class669,
				   1107253284);
    }
    
    public static void method1764(Class534_Sub41 class534_sub41, int i) {
	Class690_Sub14.method17010(class534_sub41, 500000, 480487195);
    }
    
    public static boolean method1765(int i, boolean bool, byte i_3_) {
	if (i == Class554_Sub1.anInt10683 * -363511917)
	    Class554_Sub1.aBool10675 = bool;
	else if (i == 1272099037 * Class554_Sub1.anInt10666)
	    Class554_Sub1.aBool10667 = bool;
	else if (-1565977311 * Class554_Sub1.anInt10692 == i)
	    Class554_Sub1.aBool10676 = bool;
	else
	    return false;
	return true;
    }
}
