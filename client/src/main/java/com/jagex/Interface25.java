/* Interface25 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface25
{
    public float[] method148(Class598 class598, int i, float f, int i_0_,
			     int i_1_, boolean bool);
    
    public int[] method149(Class598 class598, int i, float f, int i_2_,
			   int i_3_, boolean bool, int i_4_);
    
    public int[] method150(Class598 class598, int i, float f, int i_5_,
			   int i_6_, boolean bool, int i_7_);
    
    public float[] method151(Class598 class598, int i, float f, int i_8_,
			     int i_9_, boolean bool, int i_10_);
    
    public void method33(int i);
    
    public boolean method152(Class598 class598, int i, Class613 class613,
			     float f, int i_11_, int i_12_, boolean bool);
    
    public int[] method153(Class598 class598, int i, float f, int i_13_,
			   int i_14_, boolean bool);
    
    public int[] method154(Class598 class598, int i, float f, int i_15_,
			   int i_16_, boolean bool);
    
    public boolean method155(Class598 class598, int i, Class613 class613,
			     float f, int i_17_, int i_18_, boolean bool,
			     int i_19_);
    
    public int[] method156(Class598 class598, int i, float f, int i_20_,
			   int i_21_, boolean bool);
    
    public boolean method157(Class598 class598, int i, Class613 class613,
			     float f, int i_22_, int i_23_, boolean bool);
    
    public int[] method158(Class598 class598, int i, float f, int i_24_,
			   int i_25_, boolean bool);
    
    public void method159();
}
