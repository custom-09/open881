/* Interface38 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface38 extends Interface43
{
    public float method239();
    
    public void method240(int i, int i_0_, int i_1_, int i_2_, int i_3_,
			  int i_4_);
    
    public float method241(float f);
    
    public void method242(boolean bool, boolean bool_5_);
    
    public boolean method204();
    
    public int method93();
    
    public void method243(int i, int i_6_, int i_7_, int i_8_, int[] is,
			  int i_9_);
    
    public void method244(int i, int i_10_, int i_11_, int i_12_, byte[] is,
			  Class181 class181, int i_13_, int i_14_);
    
    public float method245(float f);
    
    public void method246(int i, int i_15_, int i_16_, int i_17_, int[] is,
			  int i_18_);
    
    public void method247(int i, int i_19_, int i_20_, int i_21_, int i_22_,
			  int i_23_);
    
    public float method248();
    
    public float method249();
    
    public float method250();
    
    public int method251();
    
    public int method252();
    
    public int method253();
    
    public int method254();
    
    public float method255(float f);
    
    public float method256(float f);
    
    public void method257(boolean bool, boolean bool_24_);
    
    public void method258(int i, int i_25_, int i_26_, int i_27_, byte[] is,
			  Class181 class181, int i_28_, int i_29_);
    
    public void method259(int i, int i_30_, int i_31_, int i_32_, int[] is,
			  int i_33_, int i_34_);
    
    public boolean method260();
    
    public void method261(int i, int i_35_, int i_36_, int i_37_, byte[] is,
			  Class181 class181, int i_38_, int i_39_);
    
    public void method262(int i, int i_40_, int i_41_, int i_42_, int[] is,
			  int[] is_43_, int i_44_);
    
    public void method263(int i, int i_45_, int i_46_, int i_47_, byte[] is,
			  Class181 class181, int i_48_, int i_49_);
    
    public void method264(int i, int i_50_, int i_51_, int i_52_, byte[] is,
			  Class181 class181, int i_53_, int i_54_);
    
    public void method265(int i, int i_55_, int i_56_, int i_57_, byte[] is,
			  Class181 class181, int i_58_, int i_59_);
    
    public void method266(int i, int i_60_, int i_61_, int i_62_, int[] is,
			  int i_63_);
    
    public void method267(int i, int i_64_, int i_65_, int i_66_, int[] is,
			  int i_67_);
    
    public void method268(int i, int i_68_, int i_69_, int i_70_, int[] is,
			  int[] is_71_, int i_72_);
    
    public void method269(int i, int i_73_, int i_74_, int i_75_, int[] is,
			  int i_76_, int i_77_);
    
    public boolean method270();
    
    public int method1();
    
    public boolean method271();
    
    public float method272();
    
    public boolean method273();
    
    public void method274(int i, int i_78_, int i_79_, int i_80_, int i_81_,
			  int i_82_);
    
    public boolean method275();
}
