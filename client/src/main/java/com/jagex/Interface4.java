/* Interface4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface4 extends Iterable
{
    public int method24(int i);
    
    public void method25(int i);
    
    public long method26(int i, int i_0_);
    
    public void method27(int i, long l);
    
    public void method28(int i, int i_1_, byte i_2_);
    
    public void method29(int i, Object object, short i_3_);
    
    public int method30(int i);
    
    public void method31(int i, int i_4_);
    
    public int method32(int i, int i_5_);
    
    public void method33(int i);
    
    public int method34(int i);
    
    public void method35(int i, int i_6_);
    
    public void method36(int i, int i_7_);
    
    public long method37(int i);
    
    public void method38(int i, long l);
    
    public void method39(int i, long l);
    
    public void method40(int i, long l);
    
    public Object method41(int i);
    
    public void method42(int i, Object object);
    
    public void method43(int i, Object object);
    
    public void method44();
    
    public Object method45(int i, int i_8_);
    
    public void method46(int i);
    
    public void method47(int i);
    
    public void method48(int i);
    
    public void method49(int i);
}
