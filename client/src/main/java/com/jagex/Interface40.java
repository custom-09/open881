/* Interface40 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface40
{
    public int method280();
    
    public Class386 method281();
    
    public void method282(Interface49 interface49);
    
    public void method70(Class534_Sub40 class534_sub40);
    
    public void method84();
    
    public Class382 method283();
    
    public Class375 method284();
    
    public void method285(boolean bool, int i, int i_0_, int i_1_);
    
    public void method286();
    
    public void method287(int i, Class382 class382, Class386 class386,
			  int i_2_);
    
    public void method288(boolean bool);
    
    public int method24(int i);
    
    public int method289(int i);
    
    public boolean method290();
    
    public void method291(Class534_Sub40 class534_sub40);
    
    public void method292(boolean bool, int i, int i_3_, int i_4_);
    
    public boolean method293();
    
    public void method294(boolean bool);
    
    public void method295(int i, Class382 class382, Class386 class386,
			  int i_5_);
    
    public void method296();
    
    public Class534_Sub40 method297(int i);
    
    public void method298();
    
    public void method299();
    
    public void method300();
    
    public void method301(boolean bool);
    
    public Class375 method302();
    
    public void method303(Interface49 interface49);
    
    public void method304(Interface49 interface49);
    
    public void method305(Interface49 interface49);
    
    public void method306();
    
    public Class375 method307();
    
    public void method308(boolean bool);
    
    public void method309();
    
    public Class534_Sub40 method310(int i);
    
    public int method9();
    
    public Class534_Sub40 method311(int i);
    
    public Class534_Sub40 method312(int i);
    
    public Class534_Sub40 method313(int i);
    
    public int method314();
    
    public int method315();
    
    public int method316();
    
    public int method317(int i);
    
    public Class534_Sub40 method318(int i);
    
    public int method319(int i);
    
    public Class382 method320();
    
    public int method321();
    
    public boolean method322();
    
    public void method323(Class534_Sub40 class534_sub40);
    
    public void method324(boolean bool, int i, int i_6_, int i_7_);
    
    public void method325(boolean bool, int i, int i_8_, int i_9_);
    
    public void method326(boolean bool);
    
    public int method327(int i);
    
    public Class386 method328();
    
    public Class386 method329();
}
