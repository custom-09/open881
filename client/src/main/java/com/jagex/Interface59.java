/* Interface59 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.applet.Applet;
import java.awt.Graphics;

public interface Interface59
{
    public void paint(Graphics graphics);
    
    public void method382(Graphics graphics);
    
    public void method383();
    
    public void init();
    
    public void method384(Graphics graphics);
    
    public void update(Graphics graphics);
    
    public void stop();
    
    public void method385(Applet applet);
    
    public void method386();
    
    public void method387();
    
    public void method388();
    
    public void method389();
    
    public void supplyApplet(Applet applet);
    
    public void method390();
    
    public void method391();
    
    public void method392();
    
    public void method393(Graphics graphics);
    
    public void method394(Applet applet);
    
    public void start();
    
    public void method395(Graphics graphics);
    
    public void destroy();
    
    public void method396();
}
