/* Interface62 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface62
{
    public void method408(Class185 class185, byte i);
    
    public int method409(int i);
    
    public int method410(int i);
    
    public void method411(int i);
    
    public int method56(int i);
    
    public boolean method412();
    
    public boolean method413(byte i);
    
    public void method414(Class185 class185, int i);
    
    public int method9();
    
    public int method145();
    
    public boolean method415();
    
    public int method252();
    
    public void method144();
    
    public boolean method416();
    
    public void method417(Class185 class185);
    
    public void method418(Class185 class185);
    
    public boolean method419(byte i);
    
    public int method181();
    
    public void method420(Class185 class185);
    
    public void method421(Class185 class185);
    
    public int method253();
    
    public int method254();
    
    public void method422(Class185 class185);
    
    public void method141();
    
    public boolean method260();
    
    public boolean method423();
}
