/* TwitchEventResult - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex.twitchtv;
import com.jagex.Class590;

public class TwitchEventResult extends TwitchEvent
{
    public int result;
    
    public TwitchEventResult(int i, int i_0_) {
	super(i);
	result = i_0_;
    }
    
    public void method6699(int[] is, long[] ls, Object[] objects) {
	is[0] = eventType;
	is[1] = result;
    }
    
    public Class590 method6700() {
	return Class590.aClass590_7810;
    }
    
    public Class590 method6701() {
	return Class590.aClass590_7810;
    }
    
    public Class590 method6702() {
	return Class590.aClass590_7810;
    }
    
    public void method6703(int[] is, long[] ls, Object[] objects) {
	is[0] = eventType;
	is[1] = result;
    }
    
    public void method6704(int[] is, long[] ls, Object[] objects) {
	is[0] = eventType;
	is[1] = result;
    }
    
    public void method6705(int[] is, long[] ls, Object[] objects) {
	is[0] = eventType;
	is[1] = result;
    }
}
