/* Buffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory;

public interface Buffer
{
    public int method1();
    
    public long method2();
    
    public void method3(byte[] is, int i, int i_0_, int i_1_);
    
    public void method4(byte[] is, int i, int i_2_, int i_3_);
    
    public void method5(byte[] is, int i, int i_4_, int i_5_);
    
    public void method6(byte[] is, int i, int i_6_, int i_7_);
    
    public void method7(byte[] is, int i, int i_8_, int i_9_);
    
    public int method8();
    
    public int method9();
    
    public long method10();
    
    public long method11();
    
    public long method12();
    
    public long method13();
}
