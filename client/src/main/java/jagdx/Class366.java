/* Class366 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;

public class Class366
{
    public static final int anInt3841 = 0;
    public static final int anInt3842 = 21;
    public static final int anInt3843 = 22;
    public static final int anInt3844 = 23;
    public static final int anInt3845 = 28;
    public static final int anInt3846 = 50;
    public static final int anInt3847 = 51;
    public static final int anInt3848 = 77;
    public static final int anInt3849 = 80;
    public static final int anInt3850 = 101;
    public static final int anInt3851 = 102;
    public static final int anInt3852 = 113;
    public static final int anInt3853 = 116;
    public static int anInt3854 = method6331('D', 'X', 'T', '1') * 1274045821;
    public static int anInt3855 = method6331('D', 'X', 'T', '5') * -1673986609;
    
    private Class366() throws Throwable {
	throw new Error();
    }
    
    private static int method6331(char c, char c_0_, char c_1_, char c_2_) {
	return ((c & 0xff) << 0 | (c_0_ & 0xff) << 8 | (c_1_ & 0xff) << 16
		| (c_2_ & 0xff) << 24);
    }
    
    private static int method6332(char c, char c_3_, char c_4_, char c_5_) {
	return ((c & 0xff) << 0 | (c_3_ & 0xff) << 8 | (c_4_ & 0xff) << 16
		| (c_5_ & 0xff) << 24);
    }
    
    private static int method6333(char c, char c_6_, char c_7_, char c_8_) {
	return ((c & 0xff) << 0 | (c_6_ & 0xff) << 8 | (c_7_ & 0xff) << 16
		| (c_8_ & 0xff) << 24);
    }
}
