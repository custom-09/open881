package com.rs.game.constants;

public class Shops {

    public static int General_Store = 1;
    public static int THESSALIAS_CLOTHING = 2;
    public static int ZAFFS_SUPIOUR_STAVES = 3;
    public static int VARROCK_SWORD_SHOP = 4;
    public static int TEA_SHOP = 5;
    public static int AUBURYS_RUNE_SHOP = 6;
    public static int LOWES_ARCHERY_EMPORIUM = 7;
    public static int HORVICKS_ARMOUR_SHOP = 8;
    public static int CRATE = 9;
    public static int HARRYS_FISHING_SHOP = 10;
    public static int AUTHENTIC_THROWING_WEAPONS = 11;
    public static int DOMMICKS_CRAFTING_SHOP = 13;
    public static int MAGICIANS_ROBE_SHOP = 14;
    public static int ARDY_BAKER_SHOP = 23;
    public static int ARDY_SILVER_SHOP = 24;
    public static int ARDY_GEM_SHOP = 25;
    public static int ARDY_FUR_SHOP = 26;
    public static int ARDY_SPICE_SHOP = 27;
    public static int ZENESHAS_PLATEBODY_SHOP = 28;
    public static int AEMADS_ADVENTURING_SUPPLIES = 29;
    public static int WYDINS_FOOD_STORE = 30;
    public static int GERRANTS_FISHY_BUSINESS = 31;
    public static int GRUMS_GOLD_EXCHANGE = 32;
    public static int BRIANS_BATTLE_AXE_BAZAAR = 33;
    public static int BETTYS_MAGIC_EMPORIUM = 34;
    public static int GEM_TRADER = 35;
    public static int ZEKES_SUPERIOR_SCIMITARS = 36;
    public static int Louies_ARMOURED_LEGS_BAZAAR = 37;
    public static int DRAYNORS_SEED_MARKET = 99;
    public static int VANESSAS_FARMING_SHOP = 188;



    }
